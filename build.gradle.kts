plugins {
    java
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

group = "org.example"
version = "1.0-SNAPSHOT"
var mapstructVersion = "1.3.1.Final"
repositories {
    mavenCentral()
    jcenter()
}


dependencies {
  implementation(fileTree("./libs"))
  implementation("ch.obermuhlner:big-math:2.3.0")
  implementation ("org.mapstruct:mapstruct:1.3.1.Final")
  annotationProcessor ("org.mapstruct:mapstruct-processor:1.3.1.Final")
  implementation("org.projectlombok:lombok:1.18.12")
  annotationProcessor("org.projectlombok:lombok:1.18.12")
  testCompile("junit", "junit", "4.12")
  implementation ("org.apache.commons:commons-lang3:3.12.0")
  implementation ("com.google.code.gson:gson:2.8.0")
 }



tasks.register<Jar>("runTask") {
        dependsOn(":DEMOJar")
        dependsOn(":pgmsvgJar")
        dependsOn(":dtoJar")
    }


tasks.register<Jar>("DEMOJar") {
    from(sourceSets.main.get().output) {
        include("com/tmax/openframe/trans/DEMO/**")
    }
    destinationDir = file("../PipeLine/proobject20/application/OF21/servicegroup/DEMO")
    archiveName = "DEMO-transaction.jar"
}

tasks.register<Jar>("pgmsvgJar") {
     from(sourceSets.main.get().output) {
         exclude("com/tmax/openframe/trans/**")
         exclude("com/tmax/openframe/dto/**")
     }
    destinationDir = file("../PipeLine/proobject20/application/OF21/servicegroup/pgmsvg")
    archiveName = "pgmsvg-transaction.jar"
}

tasks.register<Jar>("dtoJar") {
     from(sourceSets.main.get().output) {
        include("com/tmax/openframe/dto/**")
     }
     destinationDir = file("../PipeLine/proobject20/application/OF21/common/dto")
     archiveName = "openframe-online-runtime-dto.jar"
}

configure< JavaPluginConvention > {
     sourceCompatibility = JavaVersion.VERSION_1_8
}