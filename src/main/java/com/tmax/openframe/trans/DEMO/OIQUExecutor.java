package com.tmax.openframe.trans.DEMO;

import com.tmax.proobject.engine.service.executor.ServiceExecutor;

public class OIQUExecutor extends ServiceExecutor {

    public OIQUExecutor() {serviceObject = new OIQU();}

    @Override
    public Object execute(Object serviceInput, String serviceExecutionMethod) throws Throwable {
        return serviceObject.service(serviceInput);
    }

    public String getRendezvousMethodName(String service){return null;}
}
