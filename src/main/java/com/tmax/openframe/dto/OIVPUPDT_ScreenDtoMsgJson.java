package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPUPDT_ScreenDtoMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPUPDT_ScreenDto _OIVPUPDT_ScreenDto = (com.tmax.openframe.dto.OIVPUPDT_ScreenDto)obj;
    	
    	if(_OIVPUPDT_ScreenDto == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPUPDT_ScreenDto, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPUPDT_ScreenDto _OIVPUPDT_ScreenDto, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM05_OIVPM05_dataMsgJson __OIVPM05_OIVPM05_data = new com.tmax.openframe.dto.OIVPM05_OIVPM05_dataMsgJson();	
    	writer.name("OIVPM05_OIVPM05_data");
    	if(_OIVPUPDT_ScreenDto.getOIVPM05_OIVPM05_data() != null) {
    	writer.beginObject();
    	__OIVPM05_OIVPM05_data.marshal((com.tmax.openframe.dto.OIVPM05_OIVPM05_data)_OIVPUPDT_ScreenDto.getOIVPM05_OIVPM05_data(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.OIVPM05_OIVPM05_metaMsgJson __OIVPM05_OIVPM05_meta = new com.tmax.openframe.dto.OIVPM05_OIVPM05_metaMsgJson();	
    	writer.name("OIVPM05_OIVPM05_meta");
    	if(_OIVPUPDT_ScreenDto.getOIVPM05_OIVPM05_meta() != null) {
    	writer.beginObject();
    	__OIVPM05_OIVPM05_meta.marshal((com.tmax.openframe.dto.OIVPM05_OIVPM05_meta)_OIVPUPDT_ScreenDto.getOIVPM05_OIVPM05_meta(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapName"); 
    	if (_OIVPUPDT_ScreenDto.getMapName() != null) {
    		writer.value(_OIVPUPDT_ScreenDto.getMapName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapSetName"); 
    	if (_OIVPUPDT_ScreenDto.getMapSetName() != null) {
    		writer.value(_OIVPUPDT_ScreenDto.getMapSetName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("text"); 
    	if (_OIVPUPDT_ScreenDto.getText() != null) {
    		writer.value(_OIVPUPDT_ScreenDto.getText());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPUPDT_ScreenDto _OIVPUPDT_ScreenDto = new com.tmax.openframe.dto.OIVPUPDT_ScreenDto();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPUPDT_ScreenDto();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPUPDT_ScreenDto = (com.tmax.openframe.dto.OIVPUPDT_ScreenDto)unmarshal( jr,  _OIVPUPDT_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPUPDT_ScreenDto;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPUPDT_ScreenDto _OIVPUPDT_ScreenDto = (com.tmax.openframe.dto.OIVPUPDT_ScreenDto) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPUPDT_ScreenDto();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPUPDT_ScreenDto = (com.tmax.openframe.dto.OIVPUPDT_ScreenDto)unmarshal( jr,  _OIVPUPDT_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPUPDT_ScreenDto;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPUPDT_ScreenDto dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPUPDT_ScreenDto dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "OIVPM05_OIVPM05_data" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM05_OIVPM05_dataMsgJson __OIVPM05_OIVPM05_data = new com.tmax.openframe.dto.OIVPM05_OIVPM05_dataMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM05_OIVPM05_data ___OIVPM05_OIVPM05_data = new com.tmax.openframe.dto.OIVPM05_OIVPM05_data();
    				reader.beginObject();
    				dto.setOIVPM05_OIVPM05_data((com.tmax.openframe.dto.OIVPM05_OIVPM05_data)__OIVPM05_OIVPM05_data.unmarshal( reader, ___OIVPM05_OIVPM05_data ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "OIVPM05_OIVPM05_meta" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM05_OIVPM05_metaMsgJson __OIVPM05_OIVPM05_meta = new com.tmax.openframe.dto.OIVPM05_OIVPM05_metaMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM05_OIVPM05_meta ___OIVPM05_OIVPM05_meta = new com.tmax.openframe.dto.OIVPM05_OIVPM05_meta();
    				reader.beginObject();
    				dto.setOIVPM05_OIVPM05_meta((com.tmax.openframe.dto.OIVPM05_OIVPM05_meta)__OIVPM05_OIVPM05_meta.unmarshal( reader, ___OIVPM05_OIVPM05_meta ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "mapName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "mapSetName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapSetName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "text" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setText( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

