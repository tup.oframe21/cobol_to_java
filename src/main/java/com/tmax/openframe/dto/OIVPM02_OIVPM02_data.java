package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM02_OIVPM02_data extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM02_OIVPM02_data";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM02_OIVPM02_data() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE
     * Comments    : 
     */	
    private String DATE = null;
    
    private transient boolean DATE_nullable = true;
    
    public boolean isNullableDATE() {
    	return this.DATE_nullable;
    }
    
    private transient boolean DATE_invalidation = false;
    	
    public void setInvalidationDATE(boolean invalidation) { 
    	this.DATE_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE() {
    	return this.DATE_invalidation;
    }
    	
    private transient boolean DATE_modified = false;
    
    public boolean isModifiedDATE() {
    	return this.DATE_modified;
    }
    	
    public void unModifiedDATE() {
    	this.DATE_modified = false;
    }
    public FieldProperty getDATEFieldProperty() {
    	return fieldPropertyMap.get("DATE");
    }
    
    public String getDATE() {
    	return DATE;
    }	
    public String getNvlDATE() {
    	if(getDATE() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE();
    	}
    }
    public void setDATE(String DATE) {
    	if(DATE == null) {
    		this.DATE = null;
    	} else {
    		this.DATE = DATE;
    	}
    	this.DATE_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM
     * Comments    : 
     */	
    private String TERM = null;
    
    private transient boolean TERM_nullable = true;
    
    public boolean isNullableTERM() {
    	return this.TERM_nullable;
    }
    
    private transient boolean TERM_invalidation = false;
    	
    public void setInvalidationTERM(boolean invalidation) { 
    	this.TERM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM() {
    	return this.TERM_invalidation;
    }
    	
    private transient boolean TERM_modified = false;
    
    public boolean isModifiedTERM() {
    	return this.TERM_modified;
    }
    	
    public void unModifiedTERM() {
    	this.TERM_modified = false;
    }
    public FieldProperty getTERMFieldProperty() {
    	return fieldPropertyMap.get("TERM");
    }
    
    public String getTERM() {
    	return TERM;
    }	
    public String getNvlTERM() {
    	if(getTERM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM();
    	}
    }
    public void setTERM(String TERM) {
    	if(TERM == null) {
    		this.TERM = null;
    	} else {
    		this.TERM = TERM;
    	}
    	this.TERM_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME
     * Comments    : 
     */	
    private String TIME = null;
    
    private transient boolean TIME_nullable = true;
    
    public boolean isNullableTIME() {
    	return this.TIME_nullable;
    }
    
    private transient boolean TIME_invalidation = false;
    	
    public void setInvalidationTIME(boolean invalidation) { 
    	this.TIME_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME() {
    	return this.TIME_invalidation;
    }
    	
    private transient boolean TIME_modified = false;
    
    public boolean isModifiedTIME() {
    	return this.TIME_modified;
    }
    	
    public void unModifiedTIME() {
    	this.TIME_modified = false;
    }
    public FieldProperty getTIMEFieldProperty() {
    	return fieldPropertyMap.get("TIME");
    }
    
    public String getTIME() {
    	return TIME;
    }	
    public String getNvlTIME() {
    	if(getTIME() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME();
    	}
    }
    public void setTIME(String TIME) {
    	if(TIME == null) {
    		this.TIME = null;
    	} else {
    		this.TIME = TIME;
    	}
    	this.TIME_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1
     * Comments    : 
     */	
    private String IDEN1 = null;
    
    private transient boolean IDEN1_nullable = true;
    
    public boolean isNullableIDEN1() {
    	return this.IDEN1_nullable;
    }
    
    private transient boolean IDEN1_invalidation = false;
    	
    public void setInvalidationIDEN1(boolean invalidation) { 
    	this.IDEN1_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1() {
    	return this.IDEN1_invalidation;
    }
    	
    private transient boolean IDEN1_modified = false;
    
    public boolean isModifiedIDEN1() {
    	return this.IDEN1_modified;
    }
    	
    public void unModifiedIDEN1() {
    	this.IDEN1_modified = false;
    }
    public FieldProperty getIDEN1FieldProperty() {
    	return fieldPropertyMap.get("IDEN1");
    }
    
    public String getIDEN1() {
    	return IDEN1;
    }	
    public String getNvlIDEN1() {
    	if(getIDEN1() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1();
    	}
    }
    public void setIDEN1(String IDEN1) {
    	if(IDEN1 == null) {
    		this.IDEN1 = null;
    	} else {
    		this.IDEN1 = IDEN1;
    	}
    	this.IDEN1_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1
     * Comments    : 
     */	
    private String NAME1 = null;
    
    private transient boolean NAME1_nullable = true;
    
    public boolean isNullableNAME1() {
    	return this.NAME1_nullable;
    }
    
    private transient boolean NAME1_invalidation = false;
    	
    public void setInvalidationNAME1(boolean invalidation) { 
    	this.NAME1_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1() {
    	return this.NAME1_invalidation;
    }
    	
    private transient boolean NAME1_modified = false;
    
    public boolean isModifiedNAME1() {
    	return this.NAME1_modified;
    }
    	
    public void unModifiedNAME1() {
    	this.NAME1_modified = false;
    }
    public FieldProperty getNAME1FieldProperty() {
    	return fieldPropertyMap.get("NAME1");
    }
    
    public String getNAME1() {
    	return NAME1;
    }	
    public String getNvlNAME1() {
    	if(getNAME1() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1();
    	}
    }
    public void setNAME1(String NAME1) {
    	if(NAME1 == null) {
    		this.NAME1 = null;
    	} else {
    		this.NAME1 = NAME1;
    	}
    	this.NAME1_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1
     * Comments    : 
     */	
    private String DEPT1 = null;
    
    private transient boolean DEPT1_nullable = true;
    
    public boolean isNullableDEPT1() {
    	return this.DEPT1_nullable;
    }
    
    private transient boolean DEPT1_invalidation = false;
    	
    public void setInvalidationDEPT1(boolean invalidation) { 
    	this.DEPT1_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1() {
    	return this.DEPT1_invalidation;
    }
    	
    private transient boolean DEPT1_modified = false;
    
    public boolean isModifiedDEPT1() {
    	return this.DEPT1_modified;
    }
    	
    public void unModifiedDEPT1() {
    	this.DEPT1_modified = false;
    }
    public FieldProperty getDEPT1FieldProperty() {
    	return fieldPropertyMap.get("DEPT1");
    }
    
    public String getDEPT1() {
    	return DEPT1;
    }	
    public String getNvlDEPT1() {
    	if(getDEPT1() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1();
    	}
    }
    public void setDEPT1(String DEPT1) {
    	if(DEPT1 == null) {
    		this.DEPT1 = null;
    	} else {
    		this.DEPT1 = DEPT1;
    	}
    	this.DEPT1_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1
     * Comments    : 
     */	
    private String PHON1 = null;
    
    private transient boolean PHON1_nullable = true;
    
    public boolean isNullablePHON1() {
    	return this.PHON1_nullable;
    }
    
    private transient boolean PHON1_invalidation = false;
    	
    public void setInvalidationPHON1(boolean invalidation) { 
    	this.PHON1_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1() {
    	return this.PHON1_invalidation;
    }
    	
    private transient boolean PHON1_modified = false;
    
    public boolean isModifiedPHON1() {
    	return this.PHON1_modified;
    }
    	
    public void unModifiedPHON1() {
    	this.PHON1_modified = false;
    }
    public FieldProperty getPHON1FieldProperty() {
    	return fieldPropertyMap.get("PHON1");
    }
    
    public String getPHON1() {
    	return PHON1;
    }	
    public String getNvlPHON1() {
    	if(getPHON1() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1();
    	}
    }
    public void setPHON1(String PHON1) {
    	if(PHON1 == null) {
    		this.PHON1 = null;
    	} else {
    		this.PHON1 = PHON1;
    	}
    	this.PHON1_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2
     * Comments    : 
     */	
    private String IDEN2 = null;
    
    private transient boolean IDEN2_nullable = true;
    
    public boolean isNullableIDEN2() {
    	return this.IDEN2_nullable;
    }
    
    private transient boolean IDEN2_invalidation = false;
    	
    public void setInvalidationIDEN2(boolean invalidation) { 
    	this.IDEN2_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2() {
    	return this.IDEN2_invalidation;
    }
    	
    private transient boolean IDEN2_modified = false;
    
    public boolean isModifiedIDEN2() {
    	return this.IDEN2_modified;
    }
    	
    public void unModifiedIDEN2() {
    	this.IDEN2_modified = false;
    }
    public FieldProperty getIDEN2FieldProperty() {
    	return fieldPropertyMap.get("IDEN2");
    }
    
    public String getIDEN2() {
    	return IDEN2;
    }	
    public String getNvlIDEN2() {
    	if(getIDEN2() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2();
    	}
    }
    public void setIDEN2(String IDEN2) {
    	if(IDEN2 == null) {
    		this.IDEN2 = null;
    	} else {
    		this.IDEN2 = IDEN2;
    	}
    	this.IDEN2_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2
     * Comments    : 
     */	
    private String NAME2 = null;
    
    private transient boolean NAME2_nullable = true;
    
    public boolean isNullableNAME2() {
    	return this.NAME2_nullable;
    }
    
    private transient boolean NAME2_invalidation = false;
    	
    public void setInvalidationNAME2(boolean invalidation) { 
    	this.NAME2_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2() {
    	return this.NAME2_invalidation;
    }
    	
    private transient boolean NAME2_modified = false;
    
    public boolean isModifiedNAME2() {
    	return this.NAME2_modified;
    }
    	
    public void unModifiedNAME2() {
    	this.NAME2_modified = false;
    }
    public FieldProperty getNAME2FieldProperty() {
    	return fieldPropertyMap.get("NAME2");
    }
    
    public String getNAME2() {
    	return NAME2;
    }	
    public String getNvlNAME2() {
    	if(getNAME2() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2();
    	}
    }
    public void setNAME2(String NAME2) {
    	if(NAME2 == null) {
    		this.NAME2 = null;
    	} else {
    		this.NAME2 = NAME2;
    	}
    	this.NAME2_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2
     * Comments    : 
     */	
    private String DEPT2 = null;
    
    private transient boolean DEPT2_nullable = true;
    
    public boolean isNullableDEPT2() {
    	return this.DEPT2_nullable;
    }
    
    private transient boolean DEPT2_invalidation = false;
    	
    public void setInvalidationDEPT2(boolean invalidation) { 
    	this.DEPT2_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2() {
    	return this.DEPT2_invalidation;
    }
    	
    private transient boolean DEPT2_modified = false;
    
    public boolean isModifiedDEPT2() {
    	return this.DEPT2_modified;
    }
    	
    public void unModifiedDEPT2() {
    	this.DEPT2_modified = false;
    }
    public FieldProperty getDEPT2FieldProperty() {
    	return fieldPropertyMap.get("DEPT2");
    }
    
    public String getDEPT2() {
    	return DEPT2;
    }	
    public String getNvlDEPT2() {
    	if(getDEPT2() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2();
    	}
    }
    public void setDEPT2(String DEPT2) {
    	if(DEPT2 == null) {
    		this.DEPT2 = null;
    	} else {
    		this.DEPT2 = DEPT2;
    	}
    	this.DEPT2_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2
     * Comments    : 
     */	
    private String PHON2 = null;
    
    private transient boolean PHON2_nullable = true;
    
    public boolean isNullablePHON2() {
    	return this.PHON2_nullable;
    }
    
    private transient boolean PHON2_invalidation = false;
    	
    public void setInvalidationPHON2(boolean invalidation) { 
    	this.PHON2_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2() {
    	return this.PHON2_invalidation;
    }
    	
    private transient boolean PHON2_modified = false;
    
    public boolean isModifiedPHON2() {
    	return this.PHON2_modified;
    }
    	
    public void unModifiedPHON2() {
    	this.PHON2_modified = false;
    }
    public FieldProperty getPHON2FieldProperty() {
    	return fieldPropertyMap.get("PHON2");
    }
    
    public String getPHON2() {
    	return PHON2;
    }	
    public String getNvlPHON2() {
    	if(getPHON2() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2();
    	}
    }
    public void setPHON2(String PHON2) {
    	if(PHON2 == null) {
    		this.PHON2 = null;
    	} else {
    		this.PHON2 = PHON2;
    	}
    	this.PHON2_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3
     * Comments    : 
     */	
    private String IDEN3 = null;
    
    private transient boolean IDEN3_nullable = true;
    
    public boolean isNullableIDEN3() {
    	return this.IDEN3_nullable;
    }
    
    private transient boolean IDEN3_invalidation = false;
    	
    public void setInvalidationIDEN3(boolean invalidation) { 
    	this.IDEN3_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3() {
    	return this.IDEN3_invalidation;
    }
    	
    private transient boolean IDEN3_modified = false;
    
    public boolean isModifiedIDEN3() {
    	return this.IDEN3_modified;
    }
    	
    public void unModifiedIDEN3() {
    	this.IDEN3_modified = false;
    }
    public FieldProperty getIDEN3FieldProperty() {
    	return fieldPropertyMap.get("IDEN3");
    }
    
    public String getIDEN3() {
    	return IDEN3;
    }	
    public String getNvlIDEN3() {
    	if(getIDEN3() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3();
    	}
    }
    public void setIDEN3(String IDEN3) {
    	if(IDEN3 == null) {
    		this.IDEN3 = null;
    	} else {
    		this.IDEN3 = IDEN3;
    	}
    	this.IDEN3_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3
     * Comments    : 
     */	
    private String NAME3 = null;
    
    private transient boolean NAME3_nullable = true;
    
    public boolean isNullableNAME3() {
    	return this.NAME3_nullable;
    }
    
    private transient boolean NAME3_invalidation = false;
    	
    public void setInvalidationNAME3(boolean invalidation) { 
    	this.NAME3_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3() {
    	return this.NAME3_invalidation;
    }
    	
    private transient boolean NAME3_modified = false;
    
    public boolean isModifiedNAME3() {
    	return this.NAME3_modified;
    }
    	
    public void unModifiedNAME3() {
    	this.NAME3_modified = false;
    }
    public FieldProperty getNAME3FieldProperty() {
    	return fieldPropertyMap.get("NAME3");
    }
    
    public String getNAME3() {
    	return NAME3;
    }	
    public String getNvlNAME3() {
    	if(getNAME3() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3();
    	}
    }
    public void setNAME3(String NAME3) {
    	if(NAME3 == null) {
    		this.NAME3 = null;
    	} else {
    		this.NAME3 = NAME3;
    	}
    	this.NAME3_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3
     * Comments    : 
     */	
    private String DEPT3 = null;
    
    private transient boolean DEPT3_nullable = true;
    
    public boolean isNullableDEPT3() {
    	return this.DEPT3_nullable;
    }
    
    private transient boolean DEPT3_invalidation = false;
    	
    public void setInvalidationDEPT3(boolean invalidation) { 
    	this.DEPT3_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3() {
    	return this.DEPT3_invalidation;
    }
    	
    private transient boolean DEPT3_modified = false;
    
    public boolean isModifiedDEPT3() {
    	return this.DEPT3_modified;
    }
    	
    public void unModifiedDEPT3() {
    	this.DEPT3_modified = false;
    }
    public FieldProperty getDEPT3FieldProperty() {
    	return fieldPropertyMap.get("DEPT3");
    }
    
    public String getDEPT3() {
    	return DEPT3;
    }	
    public String getNvlDEPT3() {
    	if(getDEPT3() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3();
    	}
    }
    public void setDEPT3(String DEPT3) {
    	if(DEPT3 == null) {
    		this.DEPT3 = null;
    	} else {
    		this.DEPT3 = DEPT3;
    	}
    	this.DEPT3_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3
     * Comments    : 
     */	
    private String PHON3 = null;
    
    private transient boolean PHON3_nullable = true;
    
    public boolean isNullablePHON3() {
    	return this.PHON3_nullable;
    }
    
    private transient boolean PHON3_invalidation = false;
    	
    public void setInvalidationPHON3(boolean invalidation) { 
    	this.PHON3_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3() {
    	return this.PHON3_invalidation;
    }
    	
    private transient boolean PHON3_modified = false;
    
    public boolean isModifiedPHON3() {
    	return this.PHON3_modified;
    }
    	
    public void unModifiedPHON3() {
    	this.PHON3_modified = false;
    }
    public FieldProperty getPHON3FieldProperty() {
    	return fieldPropertyMap.get("PHON3");
    }
    
    public String getPHON3() {
    	return PHON3;
    }	
    public String getNvlPHON3() {
    	if(getPHON3() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3();
    	}
    }
    public void setPHON3(String PHON3) {
    	if(PHON3 == null) {
    		this.PHON3 = null;
    	} else {
    		this.PHON3 = PHON3;
    	}
    	this.PHON3_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4
     * Comments    : 
     */	
    private String IDEN4 = null;
    
    private transient boolean IDEN4_nullable = true;
    
    public boolean isNullableIDEN4() {
    	return this.IDEN4_nullable;
    }
    
    private transient boolean IDEN4_invalidation = false;
    	
    public void setInvalidationIDEN4(boolean invalidation) { 
    	this.IDEN4_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4() {
    	return this.IDEN4_invalidation;
    }
    	
    private transient boolean IDEN4_modified = false;
    
    public boolean isModifiedIDEN4() {
    	return this.IDEN4_modified;
    }
    	
    public void unModifiedIDEN4() {
    	this.IDEN4_modified = false;
    }
    public FieldProperty getIDEN4FieldProperty() {
    	return fieldPropertyMap.get("IDEN4");
    }
    
    public String getIDEN4() {
    	return IDEN4;
    }	
    public String getNvlIDEN4() {
    	if(getIDEN4() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4();
    	}
    }
    public void setIDEN4(String IDEN4) {
    	if(IDEN4 == null) {
    		this.IDEN4 = null;
    	} else {
    		this.IDEN4 = IDEN4;
    	}
    	this.IDEN4_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4
     * Comments    : 
     */	
    private String NAME4 = null;
    
    private transient boolean NAME4_nullable = true;
    
    public boolean isNullableNAME4() {
    	return this.NAME4_nullable;
    }
    
    private transient boolean NAME4_invalidation = false;
    	
    public void setInvalidationNAME4(boolean invalidation) { 
    	this.NAME4_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4() {
    	return this.NAME4_invalidation;
    }
    	
    private transient boolean NAME4_modified = false;
    
    public boolean isModifiedNAME4() {
    	return this.NAME4_modified;
    }
    	
    public void unModifiedNAME4() {
    	this.NAME4_modified = false;
    }
    public FieldProperty getNAME4FieldProperty() {
    	return fieldPropertyMap.get("NAME4");
    }
    
    public String getNAME4() {
    	return NAME4;
    }	
    public String getNvlNAME4() {
    	if(getNAME4() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4();
    	}
    }
    public void setNAME4(String NAME4) {
    	if(NAME4 == null) {
    		this.NAME4 = null;
    	} else {
    		this.NAME4 = NAME4;
    	}
    	this.NAME4_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4
     * Comments    : 
     */	
    private String DEPT4 = null;
    
    private transient boolean DEPT4_nullable = true;
    
    public boolean isNullableDEPT4() {
    	return this.DEPT4_nullable;
    }
    
    private transient boolean DEPT4_invalidation = false;
    	
    public void setInvalidationDEPT4(boolean invalidation) { 
    	this.DEPT4_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4() {
    	return this.DEPT4_invalidation;
    }
    	
    private transient boolean DEPT4_modified = false;
    
    public boolean isModifiedDEPT4() {
    	return this.DEPT4_modified;
    }
    	
    public void unModifiedDEPT4() {
    	this.DEPT4_modified = false;
    }
    public FieldProperty getDEPT4FieldProperty() {
    	return fieldPropertyMap.get("DEPT4");
    }
    
    public String getDEPT4() {
    	return DEPT4;
    }	
    public String getNvlDEPT4() {
    	if(getDEPT4() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4();
    	}
    }
    public void setDEPT4(String DEPT4) {
    	if(DEPT4 == null) {
    		this.DEPT4 = null;
    	} else {
    		this.DEPT4 = DEPT4;
    	}
    	this.DEPT4_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4
     * Comments    : 
     */	
    private String PHON4 = null;
    
    private transient boolean PHON4_nullable = true;
    
    public boolean isNullablePHON4() {
    	return this.PHON4_nullable;
    }
    
    private transient boolean PHON4_invalidation = false;
    	
    public void setInvalidationPHON4(boolean invalidation) { 
    	this.PHON4_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4() {
    	return this.PHON4_invalidation;
    }
    	
    private transient boolean PHON4_modified = false;
    
    public boolean isModifiedPHON4() {
    	return this.PHON4_modified;
    }
    	
    public void unModifiedPHON4() {
    	this.PHON4_modified = false;
    }
    public FieldProperty getPHON4FieldProperty() {
    	return fieldPropertyMap.get("PHON4");
    }
    
    public String getPHON4() {
    	return PHON4;
    }	
    public String getNvlPHON4() {
    	if(getPHON4() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4();
    	}
    }
    public void setPHON4(String PHON4) {
    	if(PHON4 == null) {
    		this.PHON4 = null;
    	} else {
    		this.PHON4 = PHON4;
    	}
    	this.PHON4_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5
     * Comments    : 
     */	
    private String IDEN5 = null;
    
    private transient boolean IDEN5_nullable = true;
    
    public boolean isNullableIDEN5() {
    	return this.IDEN5_nullable;
    }
    
    private transient boolean IDEN5_invalidation = false;
    	
    public void setInvalidationIDEN5(boolean invalidation) { 
    	this.IDEN5_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5() {
    	return this.IDEN5_invalidation;
    }
    	
    private transient boolean IDEN5_modified = false;
    
    public boolean isModifiedIDEN5() {
    	return this.IDEN5_modified;
    }
    	
    public void unModifiedIDEN5() {
    	this.IDEN5_modified = false;
    }
    public FieldProperty getIDEN5FieldProperty() {
    	return fieldPropertyMap.get("IDEN5");
    }
    
    public String getIDEN5() {
    	return IDEN5;
    }	
    public String getNvlIDEN5() {
    	if(getIDEN5() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5();
    	}
    }
    public void setIDEN5(String IDEN5) {
    	if(IDEN5 == null) {
    		this.IDEN5 = null;
    	} else {
    		this.IDEN5 = IDEN5;
    	}
    	this.IDEN5_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5
     * Comments    : 
     */	
    private String NAME5 = null;
    
    private transient boolean NAME5_nullable = true;
    
    public boolean isNullableNAME5() {
    	return this.NAME5_nullable;
    }
    
    private transient boolean NAME5_invalidation = false;
    	
    public void setInvalidationNAME5(boolean invalidation) { 
    	this.NAME5_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5() {
    	return this.NAME5_invalidation;
    }
    	
    private transient boolean NAME5_modified = false;
    
    public boolean isModifiedNAME5() {
    	return this.NAME5_modified;
    }
    	
    public void unModifiedNAME5() {
    	this.NAME5_modified = false;
    }
    public FieldProperty getNAME5FieldProperty() {
    	return fieldPropertyMap.get("NAME5");
    }
    
    public String getNAME5() {
    	return NAME5;
    }	
    public String getNvlNAME5() {
    	if(getNAME5() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5();
    	}
    }
    public void setNAME5(String NAME5) {
    	if(NAME5 == null) {
    		this.NAME5 = null;
    	} else {
    		this.NAME5 = NAME5;
    	}
    	this.NAME5_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5
     * Comments    : 
     */	
    private String DEPT5 = null;
    
    private transient boolean DEPT5_nullable = true;
    
    public boolean isNullableDEPT5() {
    	return this.DEPT5_nullable;
    }
    
    private transient boolean DEPT5_invalidation = false;
    	
    public void setInvalidationDEPT5(boolean invalidation) { 
    	this.DEPT5_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5() {
    	return this.DEPT5_invalidation;
    }
    	
    private transient boolean DEPT5_modified = false;
    
    public boolean isModifiedDEPT5() {
    	return this.DEPT5_modified;
    }
    	
    public void unModifiedDEPT5() {
    	this.DEPT5_modified = false;
    }
    public FieldProperty getDEPT5FieldProperty() {
    	return fieldPropertyMap.get("DEPT5");
    }
    
    public String getDEPT5() {
    	return DEPT5;
    }	
    public String getNvlDEPT5() {
    	if(getDEPT5() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5();
    	}
    }
    public void setDEPT5(String DEPT5) {
    	if(DEPT5 == null) {
    		this.DEPT5 = null;
    	} else {
    		this.DEPT5 = DEPT5;
    	}
    	this.DEPT5_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5
     * Comments    : 
     */	
    private String PHON5 = null;
    
    private transient boolean PHON5_nullable = true;
    
    public boolean isNullablePHON5() {
    	return this.PHON5_nullable;
    }
    
    private transient boolean PHON5_invalidation = false;
    	
    public void setInvalidationPHON5(boolean invalidation) { 
    	this.PHON5_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5() {
    	return this.PHON5_invalidation;
    }
    	
    private transient boolean PHON5_modified = false;
    
    public boolean isModifiedPHON5() {
    	return this.PHON5_modified;
    }
    	
    public void unModifiedPHON5() {
    	this.PHON5_modified = false;
    }
    public FieldProperty getPHON5FieldProperty() {
    	return fieldPropertyMap.get("PHON5");
    }
    
    public String getPHON5() {
    	return PHON5;
    }	
    public String getNvlPHON5() {
    	if(getPHON5() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5();
    	}
    }
    public void setPHON5(String PHON5) {
    	if(PHON5 == null) {
    		this.PHON5 = null;
    	} else {
    		this.PHON5 = PHON5;
    	}
    	this.PHON5_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX
     * Comments    : 
     */	
    private String BIDX = null;
    
    private transient boolean BIDX_nullable = true;
    
    public boolean isNullableBIDX() {
    	return this.BIDX_nullable;
    }
    
    private transient boolean BIDX_invalidation = false;
    	
    public void setInvalidationBIDX(boolean invalidation) { 
    	this.BIDX_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX() {
    	return this.BIDX_invalidation;
    }
    	
    private transient boolean BIDX_modified = false;
    
    public boolean isModifiedBIDX() {
    	return this.BIDX_modified;
    }
    	
    public void unModifiedBIDX() {
    	this.BIDX_modified = false;
    }
    public FieldProperty getBIDXFieldProperty() {
    	return fieldPropertyMap.get("BIDX");
    }
    
    public String getBIDX() {
    	return BIDX;
    }	
    public String getNvlBIDX() {
    	if(getBIDX() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX();
    	}
    }
    public void setBIDX(String BIDX) {
    	if(BIDX == null) {
    		this.BIDX = null;
    	} else {
    		this.BIDX = BIDX;
    	}
    	this.BIDX_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX
     * Comments    : 
     */	
    private String FIDX = null;
    
    private transient boolean FIDX_nullable = true;
    
    public boolean isNullableFIDX() {
    	return this.FIDX_nullable;
    }
    
    private transient boolean FIDX_invalidation = false;
    	
    public void setInvalidationFIDX(boolean invalidation) { 
    	this.FIDX_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX() {
    	return this.FIDX_invalidation;
    }
    	
    private transient boolean FIDX_modified = false;
    
    public boolean isModifiedFIDX() {
    	return this.FIDX_modified;
    }
    	
    public void unModifiedFIDX() {
    	this.FIDX_modified = false;
    }
    public FieldProperty getFIDXFieldProperty() {
    	return fieldPropertyMap.get("FIDX");
    }
    
    public String getFIDX() {
    	return FIDX;
    }	
    public String getNvlFIDX() {
    	if(getFIDX() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX();
    	}
    }
    public void setFIDX(String FIDX) {
    	if(FIDX == null) {
    		this.FIDX = null;
    	} else {
    		this.FIDX = FIDX;
    	}
    	this.FIDX_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG
     * Comments    : 
     */	
    private String MSG = null;
    
    private transient boolean MSG_nullable = true;
    
    public boolean isNullableMSG() {
    	return this.MSG_nullable;
    }
    
    private transient boolean MSG_invalidation = false;
    	
    public void setInvalidationMSG(boolean invalidation) { 
    	this.MSG_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG() {
    	return this.MSG_invalidation;
    }
    	
    private transient boolean MSG_modified = false;
    
    public boolean isModifiedMSG() {
    	return this.MSG_modified;
    }
    	
    public void unModifiedMSG() {
    	this.MSG_modified = false;
    }
    public FieldProperty getMSGFieldProperty() {
    	return fieldPropertyMap.get("MSG");
    }
    
    public String getMSG() {
    	return MSG;
    }	
    public String getNvlMSG() {
    	if(getMSG() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG();
    	}
    }
    public void setMSG(String MSG) {
    	if(MSG == null) {
    		this.MSG = null;
    	} else {
    		this.MSG = MSG;
    	}
    	this.MSG_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_modified = false;
    	this.TERM_modified = false;
    	this.TIME_modified = false;
    	this.IDEN1_modified = false;
    	this.NAME1_modified = false;
    	this.DEPT1_modified = false;
    	this.PHON1_modified = false;
    	this.IDEN2_modified = false;
    	this.NAME2_modified = false;
    	this.DEPT2_modified = false;
    	this.PHON2_modified = false;
    	this.IDEN3_modified = false;
    	this.NAME3_modified = false;
    	this.DEPT3_modified = false;
    	this.PHON3_modified = false;
    	this.IDEN4_modified = false;
    	this.NAME4_modified = false;
    	this.DEPT4_modified = false;
    	this.PHON4_modified = false;
    	this.IDEN5_modified = false;
    	this.NAME5_modified = false;
    	this.DEPT5_modified = false;
    	this.PHON5_modified = false;
    	this.BIDX_modified = false;
    	this.FIDX_modified = false;
    	this.MSG_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_modified == true)
    		modifiedFields.add("DATE");
    	if(this.TERM_modified == true)
    		modifiedFields.add("TERM");
    	if(this.TIME_modified == true)
    		modifiedFields.add("TIME");
    	if(this.IDEN1_modified == true)
    		modifiedFields.add("IDEN1");
    	if(this.NAME1_modified == true)
    		modifiedFields.add("NAME1");
    	if(this.DEPT1_modified == true)
    		modifiedFields.add("DEPT1");
    	if(this.PHON1_modified == true)
    		modifiedFields.add("PHON1");
    	if(this.IDEN2_modified == true)
    		modifiedFields.add("IDEN2");
    	if(this.NAME2_modified == true)
    		modifiedFields.add("NAME2");
    	if(this.DEPT2_modified == true)
    		modifiedFields.add("DEPT2");
    	if(this.PHON2_modified == true)
    		modifiedFields.add("PHON2");
    	if(this.IDEN3_modified == true)
    		modifiedFields.add("IDEN3");
    	if(this.NAME3_modified == true)
    		modifiedFields.add("NAME3");
    	if(this.DEPT3_modified == true)
    		modifiedFields.add("DEPT3");
    	if(this.PHON3_modified == true)
    		modifiedFields.add("PHON3");
    	if(this.IDEN4_modified == true)
    		modifiedFields.add("IDEN4");
    	if(this.NAME4_modified == true)
    		modifiedFields.add("NAME4");
    	if(this.DEPT4_modified == true)
    		modifiedFields.add("DEPT4");
    	if(this.PHON4_modified == true)
    		modifiedFields.add("PHON4");
    	if(this.IDEN5_modified == true)
    		modifiedFields.add("IDEN5");
    	if(this.NAME5_modified == true)
    		modifiedFields.add("NAME5");
    	if(this.DEPT5_modified == true)
    		modifiedFields.add("DEPT5");
    	if(this.PHON5_modified == true)
    		modifiedFields.add("PHON5");
    	if(this.BIDX_modified == true)
    		modifiedFields.add("BIDX");
    	if(this.FIDX_modified == true)
    		modifiedFields.add("FIDX");
    	if(this.MSG_modified == true)
    		modifiedFields.add("MSG");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM02_OIVPM02_data copyObj = new OIVPM02_OIVPM02_data();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM02_OIVPM02_data) {
    	if(this == _oIVPM02_OIVPM02_data) return;
    	OIVPM02_OIVPM02_data __oIVPM02_OIVPM02_data = (OIVPM02_OIVPM02_data) _oIVPM02_OIVPM02_data;
    	this.setDATE(__oIVPM02_OIVPM02_data.getDATE());
    	this.setTERM(__oIVPM02_OIVPM02_data.getTERM());
    	this.setTIME(__oIVPM02_OIVPM02_data.getTIME());
    	this.setIDEN1(__oIVPM02_OIVPM02_data.getIDEN1());
    	this.setNAME1(__oIVPM02_OIVPM02_data.getNAME1());
    	this.setDEPT1(__oIVPM02_OIVPM02_data.getDEPT1());
    	this.setPHON1(__oIVPM02_OIVPM02_data.getPHON1());
    	this.setIDEN2(__oIVPM02_OIVPM02_data.getIDEN2());
    	this.setNAME2(__oIVPM02_OIVPM02_data.getNAME2());
    	this.setDEPT2(__oIVPM02_OIVPM02_data.getDEPT2());
    	this.setPHON2(__oIVPM02_OIVPM02_data.getPHON2());
    	this.setIDEN3(__oIVPM02_OIVPM02_data.getIDEN3());
    	this.setNAME3(__oIVPM02_OIVPM02_data.getNAME3());
    	this.setDEPT3(__oIVPM02_OIVPM02_data.getDEPT3());
    	this.setPHON3(__oIVPM02_OIVPM02_data.getPHON3());
    	this.setIDEN4(__oIVPM02_OIVPM02_data.getIDEN4());
    	this.setNAME4(__oIVPM02_OIVPM02_data.getNAME4());
    	this.setDEPT4(__oIVPM02_OIVPM02_data.getDEPT4());
    	this.setPHON4(__oIVPM02_OIVPM02_data.getPHON4());
    	this.setIDEN5(__oIVPM02_OIVPM02_data.getIDEN5());
    	this.setNAME5(__oIVPM02_OIVPM02_data.getNAME5());
    	this.setDEPT5(__oIVPM02_OIVPM02_data.getDEPT5());
    	this.setPHON5(__oIVPM02_OIVPM02_data.getPHON5());
    	this.setBIDX(__oIVPM02_OIVPM02_data.getBIDX());
    	this.setFIDX(__oIVPM02_OIVPM02_data.getFIDX());
    	this.setMSG(__oIVPM02_OIVPM02_data.getMSG());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE : ").append(DATE).append("\n");	
    	buffer.append("TERM : ").append(TERM).append("\n");	
    	buffer.append("TIME : ").append(TIME).append("\n");	
    	buffer.append("IDEN1 : ").append(IDEN1).append("\n");	
    	buffer.append("NAME1 : ").append(NAME1).append("\n");	
    	buffer.append("DEPT1 : ").append(DEPT1).append("\n");	
    	buffer.append("PHON1 : ").append(PHON1).append("\n");	
    	buffer.append("IDEN2 : ").append(IDEN2).append("\n");	
    	buffer.append("NAME2 : ").append(NAME2).append("\n");	
    	buffer.append("DEPT2 : ").append(DEPT2).append("\n");	
    	buffer.append("PHON2 : ").append(PHON2).append("\n");	
    	buffer.append("IDEN3 : ").append(IDEN3).append("\n");	
    	buffer.append("NAME3 : ").append(NAME3).append("\n");	
    	buffer.append("DEPT3 : ").append(DEPT3).append("\n");	
    	buffer.append("PHON3 : ").append(PHON3).append("\n");	
    	buffer.append("IDEN4 : ").append(IDEN4).append("\n");	
    	buffer.append("NAME4 : ").append(NAME4).append("\n");	
    	buffer.append("DEPT4 : ").append(DEPT4).append("\n");	
    	buffer.append("PHON4 : ").append(PHON4).append("\n");	
    	buffer.append("IDEN5 : ").append(IDEN5).append("\n");	
    	buffer.append("NAME5 : ").append(NAME5).append("\n");	
    	buffer.append("DEPT5 : ").append(DEPT5).append("\n");	
    	buffer.append("PHON5 : ").append(PHON5).append("\n");	
    	buffer.append("BIDX : ").append(BIDX).append("\n");	
    	buffer.append("FIDX : ").append(FIDX).append("\n");	
    	buffer.append("MSG : ").append(MSG).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(26);
    	fieldPropertyMap.put("DATE", FieldProperty.builder()
    	              .setPhysicalName("DATE")
    	              .setLogicalName("DATE")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM", FieldProperty.builder()
    	              .setPhysicalName("TERM")
    	              .setLogicalName("TERM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(4)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME", FieldProperty.builder()
    	              .setPhysicalName("TIME")
    	              .setLogicalName("TIME")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1", FieldProperty.builder()
    	              .setPhysicalName("IDEN1")
    	              .setLogicalName("IDEN1")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1", FieldProperty.builder()
    	              .setPhysicalName("NAME1")
    	              .setLogicalName("NAME1")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1", FieldProperty.builder()
    	              .setPhysicalName("DEPT1")
    	              .setLogicalName("DEPT1")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1", FieldProperty.builder()
    	              .setPhysicalName("PHON1")
    	              .setLogicalName("PHON1")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2", FieldProperty.builder()
    	              .setPhysicalName("IDEN2")
    	              .setLogicalName("IDEN2")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2", FieldProperty.builder()
    	              .setPhysicalName("NAME2")
    	              .setLogicalName("NAME2")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2", FieldProperty.builder()
    	              .setPhysicalName("DEPT2")
    	              .setLogicalName("DEPT2")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2", FieldProperty.builder()
    	              .setPhysicalName("PHON2")
    	              .setLogicalName("PHON2")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3", FieldProperty.builder()
    	              .setPhysicalName("IDEN3")
    	              .setLogicalName("IDEN3")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3", FieldProperty.builder()
    	              .setPhysicalName("NAME3")
    	              .setLogicalName("NAME3")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3", FieldProperty.builder()
    	              .setPhysicalName("DEPT3")
    	              .setLogicalName("DEPT3")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3", FieldProperty.builder()
    	              .setPhysicalName("PHON3")
    	              .setLogicalName("PHON3")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4", FieldProperty.builder()
    	              .setPhysicalName("IDEN4")
    	              .setLogicalName("IDEN4")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4", FieldProperty.builder()
    	              .setPhysicalName("NAME4")
    	              .setLogicalName("NAME4")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4", FieldProperty.builder()
    	              .setPhysicalName("DEPT4")
    	              .setLogicalName("DEPT4")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4", FieldProperty.builder()
    	              .setPhysicalName("PHON4")
    	              .setLogicalName("PHON4")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5", FieldProperty.builder()
    	              .setPhysicalName("IDEN5")
    	              .setLogicalName("IDEN5")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5", FieldProperty.builder()
    	              .setPhysicalName("NAME5")
    	              .setLogicalName("NAME5")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5", FieldProperty.builder()
    	              .setPhysicalName("DEPT5")
    	              .setLogicalName("DEPT5")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5", FieldProperty.builder()
    	              .setPhysicalName("PHON5")
    	              .setLogicalName("PHON5")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(15)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX", FieldProperty.builder()
    	              .setPhysicalName("BIDX")
    	              .setLogicalName("BIDX")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX", FieldProperty.builder()
    	              .setPhysicalName("FIDX")
    	              .setLogicalName("FIDX")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG", FieldProperty.builder()
    	              .setPhysicalName("MSG")
    	              .setLogicalName("MSG")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(76)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : return getDATE();
    		case "TERM" : return getTERM();
    		case "TIME" : return getTIME();
    		case "IDEN1" : return getIDEN1();
    		case "NAME1" : return getNAME1();
    		case "DEPT1" : return getDEPT1();
    		case "PHON1" : return getPHON1();
    		case "IDEN2" : return getIDEN2();
    		case "NAME2" : return getNAME2();
    		case "DEPT2" : return getDEPT2();
    		case "PHON2" : return getPHON2();
    		case "IDEN3" : return getIDEN3();
    		case "NAME3" : return getNAME3();
    		case "DEPT3" : return getDEPT3();
    		case "PHON3" : return getPHON3();
    		case "IDEN4" : return getIDEN4();
    		case "NAME4" : return getNAME4();
    		case "DEPT4" : return getDEPT4();
    		case "PHON4" : return getPHON4();
    		case "IDEN5" : return getIDEN5();
    		case "NAME5" : return getNAME5();
    		case "DEPT5" : return getDEPT5();
    		case "PHON5" : return getPHON5();
    		case "BIDX" : return getBIDX();
    		case "FIDX" : return getFIDX();
    		case "MSG" : return getMSG();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : setDATE((String)arg); break;
    		case "TERM" : setTERM((String)arg); break;
    		case "TIME" : setTIME((String)arg); break;
    		case "IDEN1" : setIDEN1((String)arg); break;
    		case "NAME1" : setNAME1((String)arg); break;
    		case "DEPT1" : setDEPT1((String)arg); break;
    		case "PHON1" : setPHON1((String)arg); break;
    		case "IDEN2" : setIDEN2((String)arg); break;
    		case "NAME2" : setNAME2((String)arg); break;
    		case "DEPT2" : setDEPT2((String)arg); break;
    		case "PHON2" : setPHON2((String)arg); break;
    		case "IDEN3" : setIDEN3((String)arg); break;
    		case "NAME3" : setNAME3((String)arg); break;
    		case "DEPT3" : setDEPT3((String)arg); break;
    		case "PHON3" : setPHON3((String)arg); break;
    		case "IDEN4" : setIDEN4((String)arg); break;
    		case "NAME4" : setNAME4((String)arg); break;
    		case "DEPT4" : setDEPT4((String)arg); break;
    		case "PHON4" : setPHON4((String)arg); break;
    		case "IDEN5" : setIDEN5((String)arg); break;
    		case "NAME5" : setNAME5((String)arg); break;
    		case "DEPT5" : setDEPT5((String)arg); break;
    		case "PHON5" : setPHON5((String)arg); break;
    		case "BIDX" : setBIDX((String)arg); break;
    		case "FIDX" : setFIDX((String)arg); break;
    		case "MSG" : setMSG((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM02_OIVPM02_data _OIVPM02_OIVPM02_data = (OIVPM02_OIVPM02_data) obj;
    	if(this.DATE == null) {
    	if(_OIVPM02_OIVPM02_data.getDATE() != null)
    		return false;
    	} else if(!this.DATE.equals(_OIVPM02_OIVPM02_data.getDATE()))
    		return false;
    	if(this.TERM == null) {
    	if(_OIVPM02_OIVPM02_data.getTERM() != null)
    		return false;
    	} else if(!this.TERM.equals(_OIVPM02_OIVPM02_data.getTERM()))
    		return false;
    	if(this.TIME == null) {
    	if(_OIVPM02_OIVPM02_data.getTIME() != null)
    		return false;
    	} else if(!this.TIME.equals(_OIVPM02_OIVPM02_data.getTIME()))
    		return false;
    	if(this.IDEN1 == null) {
    	if(_OIVPM02_OIVPM02_data.getIDEN1() != null)
    		return false;
    	} else if(!this.IDEN1.equals(_OIVPM02_OIVPM02_data.getIDEN1()))
    		return false;
    	if(this.NAME1 == null) {
    	if(_OIVPM02_OIVPM02_data.getNAME1() != null)
    		return false;
    	} else if(!this.NAME1.equals(_OIVPM02_OIVPM02_data.getNAME1()))
    		return false;
    	if(this.DEPT1 == null) {
    	if(_OIVPM02_OIVPM02_data.getDEPT1() != null)
    		return false;
    	} else if(!this.DEPT1.equals(_OIVPM02_OIVPM02_data.getDEPT1()))
    		return false;
    	if(this.PHON1 == null) {
    	if(_OIVPM02_OIVPM02_data.getPHON1() != null)
    		return false;
    	} else if(!this.PHON1.equals(_OIVPM02_OIVPM02_data.getPHON1()))
    		return false;
    	if(this.IDEN2 == null) {
    	if(_OIVPM02_OIVPM02_data.getIDEN2() != null)
    		return false;
    	} else if(!this.IDEN2.equals(_OIVPM02_OIVPM02_data.getIDEN2()))
    		return false;
    	if(this.NAME2 == null) {
    	if(_OIVPM02_OIVPM02_data.getNAME2() != null)
    		return false;
    	} else if(!this.NAME2.equals(_OIVPM02_OIVPM02_data.getNAME2()))
    		return false;
    	if(this.DEPT2 == null) {
    	if(_OIVPM02_OIVPM02_data.getDEPT2() != null)
    		return false;
    	} else if(!this.DEPT2.equals(_OIVPM02_OIVPM02_data.getDEPT2()))
    		return false;
    	if(this.PHON2 == null) {
    	if(_OIVPM02_OIVPM02_data.getPHON2() != null)
    		return false;
    	} else if(!this.PHON2.equals(_OIVPM02_OIVPM02_data.getPHON2()))
    		return false;
    	if(this.IDEN3 == null) {
    	if(_OIVPM02_OIVPM02_data.getIDEN3() != null)
    		return false;
    	} else if(!this.IDEN3.equals(_OIVPM02_OIVPM02_data.getIDEN3()))
    		return false;
    	if(this.NAME3 == null) {
    	if(_OIVPM02_OIVPM02_data.getNAME3() != null)
    		return false;
    	} else if(!this.NAME3.equals(_OIVPM02_OIVPM02_data.getNAME3()))
    		return false;
    	if(this.DEPT3 == null) {
    	if(_OIVPM02_OIVPM02_data.getDEPT3() != null)
    		return false;
    	} else if(!this.DEPT3.equals(_OIVPM02_OIVPM02_data.getDEPT3()))
    		return false;
    	if(this.PHON3 == null) {
    	if(_OIVPM02_OIVPM02_data.getPHON3() != null)
    		return false;
    	} else if(!this.PHON3.equals(_OIVPM02_OIVPM02_data.getPHON3()))
    		return false;
    	if(this.IDEN4 == null) {
    	if(_OIVPM02_OIVPM02_data.getIDEN4() != null)
    		return false;
    	} else if(!this.IDEN4.equals(_OIVPM02_OIVPM02_data.getIDEN4()))
    		return false;
    	if(this.NAME4 == null) {
    	if(_OIVPM02_OIVPM02_data.getNAME4() != null)
    		return false;
    	} else if(!this.NAME4.equals(_OIVPM02_OIVPM02_data.getNAME4()))
    		return false;
    	if(this.DEPT4 == null) {
    	if(_OIVPM02_OIVPM02_data.getDEPT4() != null)
    		return false;
    	} else if(!this.DEPT4.equals(_OIVPM02_OIVPM02_data.getDEPT4()))
    		return false;
    	if(this.PHON4 == null) {
    	if(_OIVPM02_OIVPM02_data.getPHON4() != null)
    		return false;
    	} else if(!this.PHON4.equals(_OIVPM02_OIVPM02_data.getPHON4()))
    		return false;
    	if(this.IDEN5 == null) {
    	if(_OIVPM02_OIVPM02_data.getIDEN5() != null)
    		return false;
    	} else if(!this.IDEN5.equals(_OIVPM02_OIVPM02_data.getIDEN5()))
    		return false;
    	if(this.NAME5 == null) {
    	if(_OIVPM02_OIVPM02_data.getNAME5() != null)
    		return false;
    	} else if(!this.NAME5.equals(_OIVPM02_OIVPM02_data.getNAME5()))
    		return false;
    	if(this.DEPT5 == null) {
    	if(_OIVPM02_OIVPM02_data.getDEPT5() != null)
    		return false;
    	} else if(!this.DEPT5.equals(_OIVPM02_OIVPM02_data.getDEPT5()))
    		return false;
    	if(this.PHON5 == null) {
    	if(_OIVPM02_OIVPM02_data.getPHON5() != null)
    		return false;
    	} else if(!this.PHON5.equals(_OIVPM02_OIVPM02_data.getPHON5()))
    		return false;
    	if(this.BIDX == null) {
    	if(_OIVPM02_OIVPM02_data.getBIDX() != null)
    		return false;
    	} else if(!this.BIDX.equals(_OIVPM02_OIVPM02_data.getBIDX()))
    		return false;
    	if(this.FIDX == null) {
    	if(_OIVPM02_OIVPM02_data.getFIDX() != null)
    		return false;
    	} else if(!this.FIDX.equals(_OIVPM02_OIVPM02_data.getFIDX()))
    		return false;
    	if(this.MSG == null) {
    	if(_OIVPM02_OIVPM02_data.getMSG() != null)
    		return false;
    	} else if(!this.MSG.equals(_OIVPM02_OIVPM02_data.getMSG()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DATE == null) ? 0 : this.DATE.hashCode());
    	result = prime * result + ((this.TERM == null) ? 0 : this.TERM.hashCode());
    	result = prime * result + ((this.TIME == null) ? 0 : this.TIME.hashCode());
    	result = prime * result + ((this.IDEN1 == null) ? 0 : this.IDEN1.hashCode());
    	result = prime * result + ((this.NAME1 == null) ? 0 : this.NAME1.hashCode());
    	result = prime * result + ((this.DEPT1 == null) ? 0 : this.DEPT1.hashCode());
    	result = prime * result + ((this.PHON1 == null) ? 0 : this.PHON1.hashCode());
    	result = prime * result + ((this.IDEN2 == null) ? 0 : this.IDEN2.hashCode());
    	result = prime * result + ((this.NAME2 == null) ? 0 : this.NAME2.hashCode());
    	result = prime * result + ((this.DEPT2 == null) ? 0 : this.DEPT2.hashCode());
    	result = prime * result + ((this.PHON2 == null) ? 0 : this.PHON2.hashCode());
    	result = prime * result + ((this.IDEN3 == null) ? 0 : this.IDEN3.hashCode());
    	result = prime * result + ((this.NAME3 == null) ? 0 : this.NAME3.hashCode());
    	result = prime * result + ((this.DEPT3 == null) ? 0 : this.DEPT3.hashCode());
    	result = prime * result + ((this.PHON3 == null) ? 0 : this.PHON3.hashCode());
    	result = prime * result + ((this.IDEN4 == null) ? 0 : this.IDEN4.hashCode());
    	result = prime * result + ((this.NAME4 == null) ? 0 : this.NAME4.hashCode());
    	result = prime * result + ((this.DEPT4 == null) ? 0 : this.DEPT4.hashCode());
    	result = prime * result + ((this.PHON4 == null) ? 0 : this.PHON4.hashCode());
    	result = prime * result + ((this.IDEN5 == null) ? 0 : this.IDEN5.hashCode());
    	result = prime * result + ((this.NAME5 == null) ? 0 : this.NAME5.hashCode());
    	result = prime * result + ((this.DEPT5 == null) ? 0 : this.DEPT5.hashCode());
    	result = prime * result + ((this.PHON5 == null) ? 0 : this.PHON5.hashCode());
    	result = prime * result + ((this.BIDX == null) ? 0 : this.BIDX.hashCode());
    	result = prime * result + ((this.FIDX == null) ? 0 : this.FIDX.hashCode());
    	result = prime * result + ((this.MSG == null) ? 0 : this.MSG.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE = null;
    	TERM = null;
    	TIME = null;
    	IDEN1 = null;
    	NAME1 = null;
    	DEPT1 = null;
    	PHON1 = null;
    	IDEN2 = null;
    	NAME2 = null;
    	DEPT2 = null;
    	PHON2 = null;
    	IDEN3 = null;
    	NAME3 = null;
    	DEPT3 = null;
    	PHON3 = null;
    	IDEN4 = null;
    	NAME4 = null;
    	DEPT4 = null;
    	PHON4 = null;
    	IDEN5 = null;
    	NAME5 = null;
    	DEPT5 = null;
    	PHON5 = null;
    	BIDX = null;
    	FIDX = null;
    	MSG = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

