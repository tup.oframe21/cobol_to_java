	
package com.tmax.openframe.dto;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.dataobject.DataObject;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@com.tmax.proobject.core.DataObject
public class OIVPMAIN_PEOPLE_INFO_GET_DO extends DataObject {
	private static final String DTO_LOGICAL_NAME = "OIVPMAIN_PEOPLE_INFO_GET_DO";

	public String getDtoLogicalName() {
		return DTO_LOGICAL_NAME;
	}

	private static final long serialVersionUID= 1L;

	public OIVPMAIN_PEOPLE_INFO_GET_DO() {
			super();
			
	}
	
	private transient boolean isModified = false;

	/**
	 * LogicalName : PEOPLE_INFO
	 * Comments    : 
	 */		

	private String PEOPLE_INFO = null;
	
	private transient boolean PEOPLE_INFO_nullable = true;
	
	public boolean isNullablePEOPLE_INFO() {
		return this.PEOPLE_INFO_nullable;
	}
	
	private transient boolean PEOPLE_INFO_invalidation = false;
	
	public void setInvalidationPEOPLE_INFO(boolean invalidation) { 
		this.PEOPLE_INFO_invalidation = invalidation;
	}
	
	public boolean isInvalidationPEOPLE_INFO() {
		return this.PEOPLE_INFO_invalidation;
	}
	
	private transient boolean PEOPLE_INFO_modified = false;
	

	public boolean isModifiedPEOPLE_INFO() {
		return this.PEOPLE_INFO_modified;
	}
	
	public void unModifiedPEOPLE_INFO() {
		this.PEOPLE_INFO_modified = false;
	}

	public FieldProperty getPEOPLE_INFOFieldProperty() {
		return fieldPropertyMap.get("PEOPLE_INFO");
	}

	public String getPEOPLE_INFO() {
		return PEOPLE_INFO;
	}
	public String getNvlPEOPLE_INFO() {
		if(getPEOPLE_INFO() == null) {
			return EMPTY_STRING;
		} else {
			return getPEOPLE_INFO();
		}
	}
	
	public void setPEOPLE_INFO(String PEOPLE_INFO) {
		if(PEOPLE_INFO == null) {
			this.PEOPLE_INFO = null;
		} else {
			this.PEOPLE_INFO = PEOPLE_INFO;
		}
		this.PEOPLE_INFO_modified = true;
		this.isModified = true;
	}


	/**
	 * LogicalName : WS_KEY
	 * Comments    : 
	 */

	private int WS_KEY = 0;

	private transient boolean WS_KEY_nullable = false;

	public boolean isNullableWS_KEY() {
		return this.WS_KEY_nullable;
	}

	private transient boolean WS_KEY_invalidation = false;

	public void setInvalidationWS_KEY(boolean invalidation) {
		this.WS_KEY_invalidation = invalidation;
	}

	public boolean isInvalidationWS_KEY() {
		return this.WS_KEY_invalidation;
	}

	private transient boolean WS_KEY_modified = false;


	public boolean isModifiedWS_KEY() {
		return this.WS_KEY_modified;
	}

	public void unModifiedWS_KEY() {
		this.WS_KEY_modified = false;
	}

	public FieldProperty getWS_KEYFieldProperty() {
		return fieldPropertyMap.get("WS_KEY");
	}

	public int getWS_KEY() {
		return WS_KEY;
	}

	public void setWS_KEY(int WS_KEY) {
		this.WS_KEY = WS_KEY;
		this.WS_KEY_modified = true;
		this.isModified = true;
	}
	public void setWS_KEY(Integer WS_KEY) {
		if( WS_KEY == null) {
			this.WS_KEY = 0;
		} else{
			this.WS_KEY = WS_KEY.intValue();
		}
		this.WS_KEY_modified = true;
		this.isModified = true;
	}
	public void setWS_KEY(String WS_KEY) {
		if  (WS_KEY==null || WS_KEY.length() == 0) {
			this.WS_KEY = 0;
		} else {
			this.WS_KEY = Integer.parseInt(WS_KEY);
		}
		this.WS_KEY_modified = true;
		this.isModified = true;
	}



	/**
	 * LogicalName : WS_DATA
	 * Comments    : 
	 */		

	private String WS_DATA = null;
	
	private transient boolean WS_DATA_nullable = true;
	
	public boolean isNullableWS_DATA() {
		return this.WS_DATA_nullable;
	}
	
	private transient boolean WS_DATA_invalidation = false;
	
	public void setInvalidationWS_DATA(boolean invalidation) { 
		this.WS_DATA_invalidation = invalidation;
	}
	
	public boolean isInvalidationWS_DATA() {
		return this.WS_DATA_invalidation;
	}
	
	private transient boolean WS_DATA_modified = false;
	

	public boolean isModifiedWS_DATA() {
		return this.WS_DATA_modified;
	}
	
	public void unModifiedWS_DATA() {
		this.WS_DATA_modified = false;
	}

	public FieldProperty getWS_DATAFieldProperty() {
		return fieldPropertyMap.get("WS_DATA");
	}

	public String getWS_DATA() {
		return WS_DATA;
	}
	public String getNvlWS_DATA() {
		if(getWS_DATA() == null) {
			return EMPTY_STRING;
		} else {
			return getWS_DATA();
		}
	}
	
	public void setWS_DATA(String WS_DATA) {
		if(WS_DATA == null) {
			this.WS_DATA = null;
		} else {
			this.WS_DATA = WS_DATA;
		}
		this.WS_DATA_modified = true;
		this.isModified = true;
	}		


	/**
	 * LogicalName : WS_NAME
	 * Comments    : 
	 */		

	private String WS_NAME = null;
	
	private transient boolean WS_NAME_nullable = true;
	
	public boolean isNullableWS_NAME() {
		return this.WS_NAME_nullable;
	}
	
	private transient boolean WS_NAME_invalidation = false;
	
	public void setInvalidationWS_NAME(boolean invalidation) { 
		this.WS_NAME_invalidation = invalidation;
	}
	
	public boolean isInvalidationWS_NAME() {
		return this.WS_NAME_invalidation;
	}
	
	private transient boolean WS_NAME_modified = false;
	

	public boolean isModifiedWS_NAME() {
		return this.WS_NAME_modified;
	}
	
	public void unModifiedWS_NAME() {
		this.WS_NAME_modified = false;
	}

	public FieldProperty getWS_NAMEFieldProperty() {
		return fieldPropertyMap.get("WS_NAME");
	}

	public String getWS_NAME() {
		return WS_NAME;
	}
	public String getNvlWS_NAME() {
		if(getWS_NAME() == null) {
			return EMPTY_STRING;
		} else {
			return getWS_NAME();
		}
	}
	
	public void setWS_NAME(String WS_NAME) {
		if(WS_NAME == null) {
			this.WS_NAME = null;
		} else {
			this.WS_NAME = WS_NAME;
		}
		this.WS_NAME_modified = true;
		this.isModified = true;
	}		


	/**
	 * LogicalName : WS_DEPARTMENT
	 * Comments    : 
	 */		

	private String WS_DEPARTMENT = null;
	
	private transient boolean WS_DEPARTMENT_nullable = true;
	
	public boolean isNullableWS_DEPARTMENT() {
		return this.WS_DEPARTMENT_nullable;
	}
	
	private transient boolean WS_DEPARTMENT_invalidation = false;
	
	public void setInvalidationWS_DEPARTMENT(boolean invalidation) { 
		this.WS_DEPARTMENT_invalidation = invalidation;
	}
	
	public boolean isInvalidationWS_DEPARTMENT() {
		return this.WS_DEPARTMENT_invalidation;
	}
	
	private transient boolean WS_DEPARTMENT_modified = false;
	

	public boolean isModifiedWS_DEPARTMENT() {
		return this.WS_DEPARTMENT_modified;
	}
	
	public void unModifiedWS_DEPARTMENT() {
		this.WS_DEPARTMENT_modified = false;
	}

	public FieldProperty getWS_DEPARTMENTFieldProperty() {
		return fieldPropertyMap.get("WS_DEPARTMENT");
	}

	public String getWS_DEPARTMENT() {
		return WS_DEPARTMENT;
	}
	public String getNvlWS_DEPARTMENT() {
		if(getWS_DEPARTMENT() == null) {
			return EMPTY_STRING;
		} else {
			return getWS_DEPARTMENT();
		}
	}
	
	public void setWS_DEPARTMENT(String WS_DEPARTMENT) {
		if(WS_DEPARTMENT == null) {
			this.WS_DEPARTMENT = null;
		} else {
			this.WS_DEPARTMENT = WS_DEPARTMENT;
		}
		this.WS_DEPARTMENT_modified = true;
		this.isModified = true;
	}		


	/**
	 * LogicalName : WS_PHONE
	 * Comments    : 
	 */		

	private String WS_PHONE = null;
	
	private transient boolean WS_PHONE_nullable = true;
	
	public boolean isNullableWS_PHONE() {
		return this.WS_PHONE_nullable;
	}
	
	private transient boolean WS_PHONE_invalidation = false;
	
	public void setInvalidationWS_PHONE(boolean invalidation) { 
		this.WS_PHONE_invalidation = invalidation;
	}
	
	public boolean isInvalidationWS_PHONE() {
		return this.WS_PHONE_invalidation;
	}
	
	private transient boolean WS_PHONE_modified = false;
	

	public boolean isModifiedWS_PHONE() {
		return this.WS_PHONE_modified;
	}
	
	public void unModifiedWS_PHONE() {
		this.WS_PHONE_modified = false;
	}

	public FieldProperty getWS_PHONEFieldProperty() {
		return fieldPropertyMap.get("WS_PHONE");
	}

	public String getWS_PHONE() {
		return WS_PHONE;
	}
	public String getNvlWS_PHONE() {
		if(getWS_PHONE() == null) {
			return EMPTY_STRING;
		} else {
			return getWS_PHONE();
		}
	}
	
	public void setWS_PHONE(String WS_PHONE) {
		if(WS_PHONE == null) {
			this.WS_PHONE = null;
		} else {
			this.WS_PHONE = WS_PHONE;
		}
		this.WS_PHONE_modified = true;
		this.isModified = true;
	}		


	/**
	 * LogicalName : WS_EMAIL
	 * Comments    : 
	 */		

	private String WS_EMAIL = null;
	
	private transient boolean WS_EMAIL_nullable = true;
	
	public boolean isNullableWS_EMAIL() {
		return this.WS_EMAIL_nullable;
	}
	
	private transient boolean WS_EMAIL_invalidation = false;
	
	public void setInvalidationWS_EMAIL(boolean invalidation) { 
		this.WS_EMAIL_invalidation = invalidation;
	}
	
	public boolean isInvalidationWS_EMAIL() {
		return this.WS_EMAIL_invalidation;
	}
	
	private transient boolean WS_EMAIL_modified = false;
	

	public boolean isModifiedWS_EMAIL() {
		return this.WS_EMAIL_modified;
	}
	
	public void unModifiedWS_EMAIL() {
		this.WS_EMAIL_modified = false;
	}

	public FieldProperty getWS_EMAILFieldProperty() {
		return fieldPropertyMap.get("WS_EMAIL");
	}

	public String getWS_EMAIL() {
		return WS_EMAIL;
	}
	public String getNvlWS_EMAIL() {
		if(getWS_EMAIL() == null) {
			return EMPTY_STRING;
		} else {
			return getWS_EMAIL();
		}
	}
	
	public void setWS_EMAIL(String WS_EMAIL) {
		if(WS_EMAIL == null) {
			this.WS_EMAIL = null;
		} else {
			this.WS_EMAIL = WS_EMAIL;
		}
		this.WS_EMAIL_modified = true;
		this.isModified = true;
	}		


	/**
	 * LogicalName : WS_ADDRESS
	 * Comments    : 
	 */		

	private String WS_ADDRESS = null;
	
	private transient boolean WS_ADDRESS_nullable = true;
	
	public boolean isNullableWS_ADDRESS() {
		return this.WS_ADDRESS_nullable;
	}
	
	private transient boolean WS_ADDRESS_invalidation = false;
	
	public void setInvalidationWS_ADDRESS(boolean invalidation) { 
		this.WS_ADDRESS_invalidation = invalidation;
	}
	
	public boolean isInvalidationWS_ADDRESS() {
		return this.WS_ADDRESS_invalidation;
	}
	
	private transient boolean WS_ADDRESS_modified = false;
	

	public boolean isModifiedWS_ADDRESS() {
		return this.WS_ADDRESS_modified;
	}
	
	public void unModifiedWS_ADDRESS() {
		this.WS_ADDRESS_modified = false;
	}

	public FieldProperty getWS_ADDRESSFieldProperty() {
		return fieldPropertyMap.get("WS_ADDRESS");
	}

	public String getWS_ADDRESS() {
		return WS_ADDRESS;
	}
	public String getNvlWS_ADDRESS() {
		if(getWS_ADDRESS() == null) {
			return EMPTY_STRING;
		} else {
			return getWS_ADDRESS();
		}
	}
	
	public void setWS_ADDRESS(String WS_ADDRESS) {
		if(WS_ADDRESS == null) {
			this.WS_ADDRESS = null;
		} else {
			this.WS_ADDRESS = WS_ADDRESS;
		}
		this.WS_ADDRESS_modified = true;
		this.isModified = true;
	}		


	@Override
	public void clearAllIsModified() {
		this.PEOPLE_INFO_modified = false;
		this.WS_KEY_modified = false;
		this.WS_DATA_modified = false;
		this.WS_NAME_modified = false;
		this.WS_DEPARTMENT_modified = false;
		this.WS_PHONE_modified = false;
		this.WS_EMAIL_modified = false;
		this.WS_ADDRESS_modified = false;
		this.isModified = false;
	}
	
	@Override
	public List<String> getIsModifiedField() {
		List<String> modifiedFields = new ArrayList<>();
		if(this.PEOPLE_INFO_modified == true)
			modifiedFields.add("PEOPLE_INFO");
		if(this.WS_KEY_modified == true)
			modifiedFields.add("WS_KEY");
		if(this.WS_DATA_modified == true)
			modifiedFields.add("WS_DATA");
		if(this.WS_NAME_modified == true)
			modifiedFields.add("WS_NAME");
		if(this.WS_DEPARTMENT_modified == true)
			modifiedFields.add("WS_DEPARTMENT");
		if(this.WS_PHONE_modified == true)
			modifiedFields.add("WS_PHONE");
		if(this.WS_EMAIL_modified == true)
			modifiedFields.add("WS_EMAIL");
		if(this.WS_ADDRESS_modified == true)
			modifiedFields.add("WS_ADDRESS");
		return modifiedFields;
	}
	
	@Override
	public boolean isModified() {
	    return isModified;
	}
	 
	public Object clone() {
		OIVPMAIN_PEOPLE_INFO_GET_DO copyObj = new OIVPMAIN_PEOPLE_INFO_GET_DO();
		copyObj.clone(this);
		return copyObj;
	}
	
	public void clone(DataObject _oIVPTST_PEOPLE_INFO_DO) {
		if(this == _oIVPTST_PEOPLE_INFO_DO) return;
		OIVPMAIN_PEOPLE_INFO_GET_DO __oIVPTST_PEOPLE_INFO_DO = (OIVPMAIN_PEOPLE_INFO_GET_DO) _oIVPTST_PEOPLE_INFO_DO;
		
		this.setPEOPLE_INFO(__oIVPTST_PEOPLE_INFO_DO.getPEOPLE_INFO());
		this.setWS_KEY(__oIVPTST_PEOPLE_INFO_DO.getWS_KEY());
		this.setWS_DATA(__oIVPTST_PEOPLE_INFO_DO.getWS_DATA());
		this.setWS_NAME(__oIVPTST_PEOPLE_INFO_DO.getWS_NAME());
		this.setWS_DEPARTMENT(__oIVPTST_PEOPLE_INFO_DO.getWS_DEPARTMENT());
		this.setWS_PHONE(__oIVPTST_PEOPLE_INFO_DO.getWS_PHONE());
		this.setWS_EMAIL(__oIVPTST_PEOPLE_INFO_DO.getWS_EMAIL());
		this.setWS_ADDRESS(__oIVPTST_PEOPLE_INFO_DO.getWS_ADDRESS());
		
	}

	public String toString() {
		StringBuilder buffer = new StringBuilder();
				
		buffer.append("PEOPLE_INFO : ").append(PEOPLE_INFO).append("\n");				
		buffer.append("WS_KEY : ").append(WS_KEY).append("\n");				
		buffer.append("WS_DATA : ").append(WS_DATA).append("\n");				
		buffer.append("WS_NAME : ").append(WS_NAME).append("\n");				
		buffer.append("WS_DEPARTMENT : ").append(WS_DEPARTMENT).append("\n");				
		buffer.append("WS_PHONE : ").append(WS_PHONE).append("\n");				
		buffer.append("WS_EMAIL : ").append(WS_EMAIL).append("\n");				
		buffer.append("WS_ADDRESS : ").append(WS_ADDRESS).append("\n");		
		return buffer.toString();
	}
	
	private static final Map<String,FieldProperty> fieldPropertyMap;
	
	static {
		fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(8);
		fieldPropertyMap.put("PEOPLE_INFO", FieldProperty.builder()
              .setPhysicalName("PEOPLE_INFO")
              .setLogicalName("PEOPLE_INFO")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(104)
              .setDecimal(-1)
              .setColumnName("PEOPLE_INFO")
              .build());
		fieldPropertyMap.put("WS_KEY", FieldProperty.builder()
              .setPhysicalName("WS_KEY")
              .setLogicalName("WS_KEY")
              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
              .setLength(4)
              .setDecimal(-1)
              .setColumnName("WS_KEY")
              .setIskey(true)
              .build());
		fieldPropertyMap.put("WS_DATA", FieldProperty.builder()
              .setPhysicalName("WS_DATA")
              .setLogicalName("WS_DATA")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(100)
              .setDecimal(-1)
              .setColumnName("WS_DATA")
              .build());
		fieldPropertyMap.put("WS_NAME", FieldProperty.builder()
              .setPhysicalName("WS_NAME")
              .setLogicalName("WS_NAME")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(10)
              .setDecimal(-1)
              .setColumnName("WS_NAME")
              .build());
		fieldPropertyMap.put("WS_DEPARTMENT", FieldProperty.builder()
              .setPhysicalName("WS_DEPARTMENT")
              .setLogicalName("WS_DEPARTMENT")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(15)
              .setDecimal(-1)
              .setColumnName("WS_DEPARTMENT")
              .build());
		fieldPropertyMap.put("WS_PHONE", FieldProperty.builder()
              .setPhysicalName("WS_PHONE")
              .setLogicalName("WS_PHONE")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(15)
              .setDecimal(-1)
              .setColumnName("WS_PHONE")
              .build());
		fieldPropertyMap.put("WS_EMAIL", FieldProperty.builder()
              .setPhysicalName("WS_EMAIL")
              .setLogicalName("WS_EMAIL")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(20)
              .setDecimal(-1)
              .setColumnName("WS_EMAIL")
              .build());
		fieldPropertyMap.put("WS_ADDRESS", FieldProperty.builder()
              .setPhysicalName("WS_ADDRESS")
              .setLogicalName("WS_ADDRESS")
              .setType(FieldProperty.TYPE_OBJECT_STRING)
              .setLength(40)
              .setDecimal(-1)
              .setColumnName("WS_ADDRESS")
              .build());
	}

	public Map<String,FieldProperty> getFieldPropertyMap() {
		return Collections.unmodifiableMap(fieldPropertyMap);
	}
	
	public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
		return Collections.unmodifiableMap(fieldPropertyMap);
	}	

	@SuppressWarnings("unchecked")
	public Object get(String fieldName) throws FieldNotFoundException {
		switch(fieldName) {
			case "PEOPLE_INFO" : return getPEOPLE_INFO();
			case "WS_KEY" : return getWS_KEY();
			case "WS_DATA" : return getWS_DATA();
			case "WS_NAME" : return getWS_NAME();
			case "WS_DEPARTMENT" : return getWS_DEPARTMENT();
			case "WS_PHONE" : return getWS_PHONE();
			case "WS_EMAIL" : return getWS_EMAIL();
			case "WS_ADDRESS" : return getWS_ADDRESS();
			default : throw new FieldNotFoundException(fieldName);		
		}
	}

	public void set(String fieldName, Object arg) throws FieldNotFoundException {
		switch(fieldName) {
			case "PEOPLE_INFO" : setPEOPLE_INFO((String)arg);break;
			case "WS_KEY" : setWS_KEY((int)arg);break;
			case "WS_DATA" : setWS_DATA((String)arg);break;
			case "WS_NAME" : setWS_NAME((String)arg);break;
			case "WS_DEPARTMENT" : setWS_DEPARTMENT((String)arg);break;
			case "WS_PHONE" : setWS_PHONE((String)arg);break;
			case "WS_EMAIL" : setWS_EMAIL((String)arg);break;
			case "WS_ADDRESS" : setWS_ADDRESS((String)arg);break;
			default : throw new FieldNotFoundException(fieldName);		
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		OIVPMAIN_PEOPLE_INFO_GET_DO _OIVPDS_DO = (OIVPMAIN_PEOPLE_INFO_GET_DO) obj;
		if(this.PEOPLE_INFO == null) {
			if(_OIVPDS_DO.getPEOPLE_INFO() != null)
				return false;
		} else if(!this.PEOPLE_INFO.equals(_OIVPDS_DO.getPEOPLE_INFO()))
			return false;				
		if(this.WS_KEY != _OIVPDS_DO.getWS_KEY()) return false;
		if(this.WS_DATA == null) {
			if(_OIVPDS_DO.getWS_DATA() != null)
				return false;
		} else if(!this.WS_DATA.equals(_OIVPDS_DO.getWS_DATA()))
			return false;
		if(this.WS_NAME == null) {
			if(_OIVPDS_DO.getWS_NAME() != null)
				return false;
		} else if(!this.WS_NAME.equals(_OIVPDS_DO.getWS_NAME()))
			return false;
		if(this.WS_DEPARTMENT == null) {
			if(_OIVPDS_DO.getWS_DEPARTMENT() != null)
				return false;
		} else if(!this.WS_DEPARTMENT.equals(_OIVPDS_DO.getWS_DEPARTMENT()))
			return false;
		if(this.WS_PHONE == null) {
			if(_OIVPDS_DO.getWS_PHONE() != null)
				return false;
		} else if(!this.WS_PHONE.equals(_OIVPDS_DO.getWS_PHONE()))
			return false;
		if(this.WS_EMAIL == null) {
			if(_OIVPDS_DO.getWS_EMAIL() != null)
				return false;
		} else if(!this.WS_EMAIL.equals(_OIVPDS_DO.getWS_EMAIL()))
			return false;
		if(this.WS_ADDRESS == null) {
			if(_OIVPDS_DO.getWS_ADDRESS() != null)
				return false;
		} else if(!this.WS_ADDRESS.equals(_OIVPDS_DO.getWS_ADDRESS()))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int prime  = 31;
		int result = 1;					
		result     = prime * result + ((this.PEOPLE_INFO == null) ? 0 : this.PEOPLE_INFO.hashCode());
		result     = prime * result + this.WS_KEY;
		result     = prime * result + ((this.WS_DATA == null) ? 0 : this.WS_DATA.hashCode());					
		result     = prime * result + ((this.WS_NAME == null) ? 0 : this.WS_NAME.hashCode());					
		result     = prime * result + ((this.WS_DEPARTMENT == null) ? 0 : this.WS_DEPARTMENT.hashCode());					
		result     = prime * result + ((this.WS_PHONE == null) ? 0 : this.WS_PHONE.hashCode());					
		result     = prime * result + ((this.WS_EMAIL == null) ? 0 : this.WS_EMAIL.hashCode());					
		result     = prime * result + ((this.WS_ADDRESS == null) ? 0 : this.WS_ADDRESS.hashCode());
		return result;
	}
	
	@Override
	public void clear() {
		PEOPLE_INFO = null;
		WS_KEY = 0;
		WS_DATA = null;
		WS_NAME = null;
		WS_DEPARTMENT = null;
		WS_PHONE = null;
		WS_EMAIL = null;
		WS_ADDRESS = null;	
		clearAllIsModified();
	}

	private void writeObject(java.io.ObjectOutputStream stream)throws IOException {
		stream.defaultWriteObject();
	}
	
	
}
