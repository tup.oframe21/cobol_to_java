package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPINSR_ScreenDtoMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPINSR_ScreenDto _OIVPINSR_ScreenDto = (com.tmax.openframe.dto.OIVPINSR_ScreenDto)obj;
    	
    	if(_OIVPINSR_ScreenDto == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPINSR_ScreenDto, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPINSR_ScreenDto _OIVPINSR_ScreenDto, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM04_OIVPM04_dataMsgJson __OIVPM04_OIVPM04_data = new com.tmax.openframe.dto.OIVPM04_OIVPM04_dataMsgJson();	
    	writer.name("OIVPM04_OIVPM04_data");
    	if(_OIVPINSR_ScreenDto.getOIVPM04_OIVPM04_data() != null) {
    	writer.beginObject();
    	__OIVPM04_OIVPM04_data.marshal((com.tmax.openframe.dto.OIVPM04_OIVPM04_data)_OIVPINSR_ScreenDto.getOIVPM04_OIVPM04_data(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.OIVPM04_OIVPM04_metaMsgJson __OIVPM04_OIVPM04_meta = new com.tmax.openframe.dto.OIVPM04_OIVPM04_metaMsgJson();	
    	writer.name("OIVPM04_OIVPM04_meta");
    	if(_OIVPINSR_ScreenDto.getOIVPM04_OIVPM04_meta() != null) {
    	writer.beginObject();
    	__OIVPM04_OIVPM04_meta.marshal((com.tmax.openframe.dto.OIVPM04_OIVPM04_meta)_OIVPINSR_ScreenDto.getOIVPM04_OIVPM04_meta(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapName"); 
    	if (_OIVPINSR_ScreenDto.getMapName() != null) {
    		writer.value(_OIVPINSR_ScreenDto.getMapName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapSetName"); 
    	if (_OIVPINSR_ScreenDto.getMapSetName() != null) {
    		writer.value(_OIVPINSR_ScreenDto.getMapSetName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("text"); 
    	if (_OIVPINSR_ScreenDto.getText() != null) {
    		writer.value(_OIVPINSR_ScreenDto.getText());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPINSR_ScreenDto _OIVPINSR_ScreenDto = new com.tmax.openframe.dto.OIVPINSR_ScreenDto();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPINSR_ScreenDto();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPINSR_ScreenDto = (com.tmax.openframe.dto.OIVPINSR_ScreenDto)unmarshal( jr,  _OIVPINSR_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPINSR_ScreenDto;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPINSR_ScreenDto _OIVPINSR_ScreenDto = (com.tmax.openframe.dto.OIVPINSR_ScreenDto) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPINSR_ScreenDto();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPINSR_ScreenDto = (com.tmax.openframe.dto.OIVPINSR_ScreenDto)unmarshal( jr,  _OIVPINSR_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPINSR_ScreenDto;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPINSR_ScreenDto dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPINSR_ScreenDto dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "OIVPM04_OIVPM04_data" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM04_OIVPM04_dataMsgJson __OIVPM04_OIVPM04_data = new com.tmax.openframe.dto.OIVPM04_OIVPM04_dataMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM04_OIVPM04_data ___OIVPM04_OIVPM04_data = new com.tmax.openframe.dto.OIVPM04_OIVPM04_data();
    				reader.beginObject();
    				dto.setOIVPM04_OIVPM04_data((com.tmax.openframe.dto.OIVPM04_OIVPM04_data)__OIVPM04_OIVPM04_data.unmarshal( reader, ___OIVPM04_OIVPM04_data ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "OIVPM04_OIVPM04_meta" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM04_OIVPM04_metaMsgJson __OIVPM04_OIVPM04_meta = new com.tmax.openframe.dto.OIVPM04_OIVPM04_metaMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM04_OIVPM04_meta ___OIVPM04_OIVPM04_meta = new com.tmax.openframe.dto.OIVPM04_OIVPM04_meta();
    				reader.beginObject();
    				dto.setOIVPM04_OIVPM04_meta((com.tmax.openframe.dto.OIVPM04_OIVPM04_meta)__OIVPM04_OIVPM04_meta.unmarshal( reader, ___OIVPM04_OIVPM04_meta ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "mapName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "mapSetName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapSetName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "text" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setText( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

