package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPM07_OIVPM07_dataMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPM07_OIVPM07_data _OIVPM07_OIVPM07_data = (com.tmax.openframe.dto.OIVPM07_OIVPM07_data)obj;
    	
    	if(_OIVPM07_OIVPM07_data == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPM07_OIVPM07_data, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPM07_OIVPM07_data _OIVPM07_OIVPM07_data, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DATE"); 
    	if (_OIVPM07_OIVPM07_data.getDATE() != null) {
    		writer.value(_OIVPM07_OIVPM07_data.getDATE());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM"); 
    	if (_OIVPM07_OIVPM07_data.getTERM() != null) {
    		writer.value(_OIVPM07_OIVPM07_data.getTERM());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME"); 
    	if (_OIVPM07_OIVPM07_data.getTIME() != null) {
    		writer.value(_OIVPM07_OIVPM07_data.getTIME());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ENDM"); 
    	if (_OIVPM07_OIVPM07_data.getENDM() != null) {
    		writer.value(_OIVPM07_OIVPM07_data.getENDM());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG"); 
    	if (_OIVPM07_OIVPM07_data.getMSG() != null) {
    		writer.value(_OIVPM07_OIVPM07_data.getMSG());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM07_OIVPM07_data _OIVPM07_OIVPM07_data = new com.tmax.openframe.dto.OIVPM07_OIVPM07_data();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM07_OIVPM07_data();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM07_OIVPM07_data = (com.tmax.openframe.dto.OIVPM07_OIVPM07_data)unmarshal( jr,  _OIVPM07_OIVPM07_data);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPM07_OIVPM07_data;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM07_OIVPM07_data _OIVPM07_OIVPM07_data = (com.tmax.openframe.dto.OIVPM07_OIVPM07_data) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM07_OIVPM07_data();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM07_OIVPM07_data = (com.tmax.openframe.dto.OIVPM07_OIVPM07_data)unmarshal( jr,  _OIVPM07_OIVPM07_data);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPM07_OIVPM07_data;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPM07_OIVPM07_data dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPM07_OIVPM07_data dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DATE" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ENDM" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setENDM( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

