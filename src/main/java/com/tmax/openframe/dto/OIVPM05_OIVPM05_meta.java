package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM05_OIVPM05_meta extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM05_OIVPM05_meta";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM05_OIVPM05_meta() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE_length
     * Comments    : 
     */	
    private int DATE_length = 0;
    
    private transient boolean DATE_length_nullable = false;
    
    public boolean isNullableDATE_length() {
    	return this.DATE_length_nullable;
    }
    
    private transient boolean DATE_length_invalidation = false;
    	
    public void setInvalidationDATE_length(boolean invalidation) { 
    	this.DATE_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_length() {
    	return this.DATE_length_invalidation;
    }
    	
    private transient boolean DATE_length_modified = false;
    
    public boolean isModifiedDATE_length() {
    	return this.DATE_length_modified;
    }
    	
    public void unModifiedDATE_length() {
    	this.DATE_length_modified = false;
    }
    public FieldProperty getDATE_lengthFieldProperty() {
    	return fieldPropertyMap.get("DATE_length");
    }
    
    public int getDATE_length() {
    	return DATE_length;
    }	
    public void setDATE_length(int DATE_length) {
    	this.DATE_length = DATE_length;
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(Integer DATE_length) {
    	if( DATE_length == null) {
    		this.DATE_length = 0;
    	} else{
    		this.DATE_length = DATE_length.intValue();
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(String DATE_length) {
    	if  (DATE_length==null || DATE_length.length() == 0) {
    		this.DATE_length = 0;
    	} else {
    		this.DATE_length = Integer.parseInt(DATE_length);
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_color
     * Comments    : 
     */	
    private String DATE_color = null;
    
    private transient boolean DATE_color_nullable = true;
    
    public boolean isNullableDATE_color() {
    	return this.DATE_color_nullable;
    }
    
    private transient boolean DATE_color_invalidation = false;
    	
    public void setInvalidationDATE_color(boolean invalidation) { 
    	this.DATE_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_color() {
    	return this.DATE_color_invalidation;
    }
    	
    private transient boolean DATE_color_modified = false;
    
    public boolean isModifiedDATE_color() {
    	return this.DATE_color_modified;
    }
    	
    public void unModifiedDATE_color() {
    	this.DATE_color_modified = false;
    }
    public FieldProperty getDATE_colorFieldProperty() {
    	return fieldPropertyMap.get("DATE_color");
    }
    
    public String getDATE_color() {
    	return DATE_color;
    }	
    public String getNvlDATE_color() {
    	if(getDATE_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_color();
    	}
    }
    public void setDATE_color(String DATE_color) {
    	if(DATE_color == null) {
    		this.DATE_color = null;
    	} else {
    		this.DATE_color = DATE_color;
    	}
    	this.DATE_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_hilight
     * Comments    : 
     */	
    private String DATE_hilight = null;
    
    private transient boolean DATE_hilight_nullable = true;
    
    public boolean isNullableDATE_hilight() {
    	return this.DATE_hilight_nullable;
    }
    
    private transient boolean DATE_hilight_invalidation = false;
    	
    public void setInvalidationDATE_hilight(boolean invalidation) { 
    	this.DATE_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_hilight() {
    	return this.DATE_hilight_invalidation;
    }
    	
    private transient boolean DATE_hilight_modified = false;
    
    public boolean isModifiedDATE_hilight() {
    	return this.DATE_hilight_modified;
    }
    	
    public void unModifiedDATE_hilight() {
    	this.DATE_hilight_modified = false;
    }
    public FieldProperty getDATE_hilightFieldProperty() {
    	return fieldPropertyMap.get("DATE_hilight");
    }
    
    public String getDATE_hilight() {
    	return DATE_hilight;
    }	
    public String getNvlDATE_hilight() {
    	if(getDATE_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_hilight();
    	}
    }
    public void setDATE_hilight(String DATE_hilight) {
    	if(DATE_hilight == null) {
    		this.DATE_hilight = null;
    	} else {
    		this.DATE_hilight = DATE_hilight;
    	}
    	this.DATE_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_outline
     * Comments    : 
     */	
    private String DATE_outline = null;
    
    private transient boolean DATE_outline_nullable = true;
    
    public boolean isNullableDATE_outline() {
    	return this.DATE_outline_nullable;
    }
    
    private transient boolean DATE_outline_invalidation = false;
    	
    public void setInvalidationDATE_outline(boolean invalidation) { 
    	this.DATE_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_outline() {
    	return this.DATE_outline_invalidation;
    }
    	
    private transient boolean DATE_outline_modified = false;
    
    public boolean isModifiedDATE_outline() {
    	return this.DATE_outline_modified;
    }
    	
    public void unModifiedDATE_outline() {
    	this.DATE_outline_modified = false;
    }
    public FieldProperty getDATE_outlineFieldProperty() {
    	return fieldPropertyMap.get("DATE_outline");
    }
    
    public String getDATE_outline() {
    	return DATE_outline;
    }	
    public String getNvlDATE_outline() {
    	if(getDATE_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_outline();
    	}
    }
    public void setDATE_outline(String DATE_outline) {
    	if(DATE_outline == null) {
    		this.DATE_outline = null;
    	} else {
    		this.DATE_outline = DATE_outline;
    	}
    	this.DATE_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_transp
     * Comments    : 
     */	
    private String DATE_transp = null;
    
    private transient boolean DATE_transp_nullable = true;
    
    public boolean isNullableDATE_transp() {
    	return this.DATE_transp_nullable;
    }
    
    private transient boolean DATE_transp_invalidation = false;
    	
    public void setInvalidationDATE_transp(boolean invalidation) { 
    	this.DATE_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_transp() {
    	return this.DATE_transp_invalidation;
    }
    	
    private transient boolean DATE_transp_modified = false;
    
    public boolean isModifiedDATE_transp() {
    	return this.DATE_transp_modified;
    }
    	
    public void unModifiedDATE_transp() {
    	this.DATE_transp_modified = false;
    }
    public FieldProperty getDATE_transpFieldProperty() {
    	return fieldPropertyMap.get("DATE_transp");
    }
    
    public String getDATE_transp() {
    	return DATE_transp;
    }	
    public String getNvlDATE_transp() {
    	if(getDATE_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_transp();
    	}
    }
    public void setDATE_transp(String DATE_transp) {
    	if(DATE_transp == null) {
    		this.DATE_transp = null;
    	} else {
    		this.DATE_transp = DATE_transp;
    	}
    	this.DATE_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_validn
     * Comments    : 
     */	
    private String DATE_validn = null;
    
    private transient boolean DATE_validn_nullable = true;
    
    public boolean isNullableDATE_validn() {
    	return this.DATE_validn_nullable;
    }
    
    private transient boolean DATE_validn_invalidation = false;
    	
    public void setInvalidationDATE_validn(boolean invalidation) { 
    	this.DATE_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_validn() {
    	return this.DATE_validn_invalidation;
    }
    	
    private transient boolean DATE_validn_modified = false;
    
    public boolean isModifiedDATE_validn() {
    	return this.DATE_validn_modified;
    }
    	
    public void unModifiedDATE_validn() {
    	this.DATE_validn_modified = false;
    }
    public FieldProperty getDATE_validnFieldProperty() {
    	return fieldPropertyMap.get("DATE_validn");
    }
    
    public String getDATE_validn() {
    	return DATE_validn;
    }	
    public String getNvlDATE_validn() {
    	if(getDATE_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_validn();
    	}
    }
    public void setDATE_validn(String DATE_validn) {
    	if(DATE_validn == null) {
    		this.DATE_validn = null;
    	} else {
    		this.DATE_validn = DATE_validn;
    	}
    	this.DATE_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_sosi
     * Comments    : 
     */	
    private String DATE_sosi = null;
    
    private transient boolean DATE_sosi_nullable = true;
    
    public boolean isNullableDATE_sosi() {
    	return this.DATE_sosi_nullable;
    }
    
    private transient boolean DATE_sosi_invalidation = false;
    	
    public void setInvalidationDATE_sosi(boolean invalidation) { 
    	this.DATE_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_sosi() {
    	return this.DATE_sosi_invalidation;
    }
    	
    private transient boolean DATE_sosi_modified = false;
    
    public boolean isModifiedDATE_sosi() {
    	return this.DATE_sosi_modified;
    }
    	
    public void unModifiedDATE_sosi() {
    	this.DATE_sosi_modified = false;
    }
    public FieldProperty getDATE_sosiFieldProperty() {
    	return fieldPropertyMap.get("DATE_sosi");
    }
    
    public String getDATE_sosi() {
    	return DATE_sosi;
    }	
    public String getNvlDATE_sosi() {
    	if(getDATE_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_sosi();
    	}
    }
    public void setDATE_sosi(String DATE_sosi) {
    	if(DATE_sosi == null) {
    		this.DATE_sosi = null;
    	} else {
    		this.DATE_sosi = DATE_sosi;
    	}
    	this.DATE_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_ps
     * Comments    : 
     */	
    private String DATE_ps = null;
    
    private transient boolean DATE_ps_nullable = true;
    
    public boolean isNullableDATE_ps() {
    	return this.DATE_ps_nullable;
    }
    
    private transient boolean DATE_ps_invalidation = false;
    	
    public void setInvalidationDATE_ps(boolean invalidation) { 
    	this.DATE_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_ps() {
    	return this.DATE_ps_invalidation;
    }
    	
    private transient boolean DATE_ps_modified = false;
    
    public boolean isModifiedDATE_ps() {
    	return this.DATE_ps_modified;
    }
    	
    public void unModifiedDATE_ps() {
    	this.DATE_ps_modified = false;
    }
    public FieldProperty getDATE_psFieldProperty() {
    	return fieldPropertyMap.get("DATE_ps");
    }
    
    public String getDATE_ps() {
    	return DATE_ps;
    }	
    public String getNvlDATE_ps() {
    	if(getDATE_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_ps();
    	}
    }
    public void setDATE_ps(String DATE_ps) {
    	if(DATE_ps == null) {
    		this.DATE_ps = null;
    	} else {
    		this.DATE_ps = DATE_ps;
    	}
    	this.DATE_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_length
     * Comments    : 
     */	
    private int TERM_length = 0;
    
    private transient boolean TERM_length_nullable = false;
    
    public boolean isNullableTERM_length() {
    	return this.TERM_length_nullable;
    }
    
    private transient boolean TERM_length_invalidation = false;
    	
    public void setInvalidationTERM_length(boolean invalidation) { 
    	this.TERM_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_length() {
    	return this.TERM_length_invalidation;
    }
    	
    private transient boolean TERM_length_modified = false;
    
    public boolean isModifiedTERM_length() {
    	return this.TERM_length_modified;
    }
    	
    public void unModifiedTERM_length() {
    	this.TERM_length_modified = false;
    }
    public FieldProperty getTERM_lengthFieldProperty() {
    	return fieldPropertyMap.get("TERM_length");
    }
    
    public int getTERM_length() {
    	return TERM_length;
    }	
    public void setTERM_length(int TERM_length) {
    	this.TERM_length = TERM_length;
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(Integer TERM_length) {
    	if( TERM_length == null) {
    		this.TERM_length = 0;
    	} else{
    		this.TERM_length = TERM_length.intValue();
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(String TERM_length) {
    	if  (TERM_length==null || TERM_length.length() == 0) {
    		this.TERM_length = 0;
    	} else {
    		this.TERM_length = Integer.parseInt(TERM_length);
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_color
     * Comments    : 
     */	
    private String TERM_color = null;
    
    private transient boolean TERM_color_nullable = true;
    
    public boolean isNullableTERM_color() {
    	return this.TERM_color_nullable;
    }
    
    private transient boolean TERM_color_invalidation = false;
    	
    public void setInvalidationTERM_color(boolean invalidation) { 
    	this.TERM_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_color() {
    	return this.TERM_color_invalidation;
    }
    	
    private transient boolean TERM_color_modified = false;
    
    public boolean isModifiedTERM_color() {
    	return this.TERM_color_modified;
    }
    	
    public void unModifiedTERM_color() {
    	this.TERM_color_modified = false;
    }
    public FieldProperty getTERM_colorFieldProperty() {
    	return fieldPropertyMap.get("TERM_color");
    }
    
    public String getTERM_color() {
    	return TERM_color;
    }	
    public String getNvlTERM_color() {
    	if(getTERM_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_color();
    	}
    }
    public void setTERM_color(String TERM_color) {
    	if(TERM_color == null) {
    		this.TERM_color = null;
    	} else {
    		this.TERM_color = TERM_color;
    	}
    	this.TERM_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_hilight
     * Comments    : 
     */	
    private String TERM_hilight = null;
    
    private transient boolean TERM_hilight_nullable = true;
    
    public boolean isNullableTERM_hilight() {
    	return this.TERM_hilight_nullable;
    }
    
    private transient boolean TERM_hilight_invalidation = false;
    	
    public void setInvalidationTERM_hilight(boolean invalidation) { 
    	this.TERM_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_hilight() {
    	return this.TERM_hilight_invalidation;
    }
    	
    private transient boolean TERM_hilight_modified = false;
    
    public boolean isModifiedTERM_hilight() {
    	return this.TERM_hilight_modified;
    }
    	
    public void unModifiedTERM_hilight() {
    	this.TERM_hilight_modified = false;
    }
    public FieldProperty getTERM_hilightFieldProperty() {
    	return fieldPropertyMap.get("TERM_hilight");
    }
    
    public String getTERM_hilight() {
    	return TERM_hilight;
    }	
    public String getNvlTERM_hilight() {
    	if(getTERM_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_hilight();
    	}
    }
    public void setTERM_hilight(String TERM_hilight) {
    	if(TERM_hilight == null) {
    		this.TERM_hilight = null;
    	} else {
    		this.TERM_hilight = TERM_hilight;
    	}
    	this.TERM_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_outline
     * Comments    : 
     */	
    private String TERM_outline = null;
    
    private transient boolean TERM_outline_nullable = true;
    
    public boolean isNullableTERM_outline() {
    	return this.TERM_outline_nullable;
    }
    
    private transient boolean TERM_outline_invalidation = false;
    	
    public void setInvalidationTERM_outline(boolean invalidation) { 
    	this.TERM_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_outline() {
    	return this.TERM_outline_invalidation;
    }
    	
    private transient boolean TERM_outline_modified = false;
    
    public boolean isModifiedTERM_outline() {
    	return this.TERM_outline_modified;
    }
    	
    public void unModifiedTERM_outline() {
    	this.TERM_outline_modified = false;
    }
    public FieldProperty getTERM_outlineFieldProperty() {
    	return fieldPropertyMap.get("TERM_outline");
    }
    
    public String getTERM_outline() {
    	return TERM_outline;
    }	
    public String getNvlTERM_outline() {
    	if(getTERM_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_outline();
    	}
    }
    public void setTERM_outline(String TERM_outline) {
    	if(TERM_outline == null) {
    		this.TERM_outline = null;
    	} else {
    		this.TERM_outline = TERM_outline;
    	}
    	this.TERM_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_transp
     * Comments    : 
     */	
    private String TERM_transp = null;
    
    private transient boolean TERM_transp_nullable = true;
    
    public boolean isNullableTERM_transp() {
    	return this.TERM_transp_nullable;
    }
    
    private transient boolean TERM_transp_invalidation = false;
    	
    public void setInvalidationTERM_transp(boolean invalidation) { 
    	this.TERM_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_transp() {
    	return this.TERM_transp_invalidation;
    }
    	
    private transient boolean TERM_transp_modified = false;
    
    public boolean isModifiedTERM_transp() {
    	return this.TERM_transp_modified;
    }
    	
    public void unModifiedTERM_transp() {
    	this.TERM_transp_modified = false;
    }
    public FieldProperty getTERM_transpFieldProperty() {
    	return fieldPropertyMap.get("TERM_transp");
    }
    
    public String getTERM_transp() {
    	return TERM_transp;
    }	
    public String getNvlTERM_transp() {
    	if(getTERM_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_transp();
    	}
    }
    public void setTERM_transp(String TERM_transp) {
    	if(TERM_transp == null) {
    		this.TERM_transp = null;
    	} else {
    		this.TERM_transp = TERM_transp;
    	}
    	this.TERM_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_validn
     * Comments    : 
     */	
    private String TERM_validn = null;
    
    private transient boolean TERM_validn_nullable = true;
    
    public boolean isNullableTERM_validn() {
    	return this.TERM_validn_nullable;
    }
    
    private transient boolean TERM_validn_invalidation = false;
    	
    public void setInvalidationTERM_validn(boolean invalidation) { 
    	this.TERM_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_validn() {
    	return this.TERM_validn_invalidation;
    }
    	
    private transient boolean TERM_validn_modified = false;
    
    public boolean isModifiedTERM_validn() {
    	return this.TERM_validn_modified;
    }
    	
    public void unModifiedTERM_validn() {
    	this.TERM_validn_modified = false;
    }
    public FieldProperty getTERM_validnFieldProperty() {
    	return fieldPropertyMap.get("TERM_validn");
    }
    
    public String getTERM_validn() {
    	return TERM_validn;
    }	
    public String getNvlTERM_validn() {
    	if(getTERM_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_validn();
    	}
    }
    public void setTERM_validn(String TERM_validn) {
    	if(TERM_validn == null) {
    		this.TERM_validn = null;
    	} else {
    		this.TERM_validn = TERM_validn;
    	}
    	this.TERM_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_sosi
     * Comments    : 
     */	
    private String TERM_sosi = null;
    
    private transient boolean TERM_sosi_nullable = true;
    
    public boolean isNullableTERM_sosi() {
    	return this.TERM_sosi_nullable;
    }
    
    private transient boolean TERM_sosi_invalidation = false;
    	
    public void setInvalidationTERM_sosi(boolean invalidation) { 
    	this.TERM_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_sosi() {
    	return this.TERM_sosi_invalidation;
    }
    	
    private transient boolean TERM_sosi_modified = false;
    
    public boolean isModifiedTERM_sosi() {
    	return this.TERM_sosi_modified;
    }
    	
    public void unModifiedTERM_sosi() {
    	this.TERM_sosi_modified = false;
    }
    public FieldProperty getTERM_sosiFieldProperty() {
    	return fieldPropertyMap.get("TERM_sosi");
    }
    
    public String getTERM_sosi() {
    	return TERM_sosi;
    }	
    public String getNvlTERM_sosi() {
    	if(getTERM_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_sosi();
    	}
    }
    public void setTERM_sosi(String TERM_sosi) {
    	if(TERM_sosi == null) {
    		this.TERM_sosi = null;
    	} else {
    		this.TERM_sosi = TERM_sosi;
    	}
    	this.TERM_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_ps
     * Comments    : 
     */	
    private String TERM_ps = null;
    
    private transient boolean TERM_ps_nullable = true;
    
    public boolean isNullableTERM_ps() {
    	return this.TERM_ps_nullable;
    }
    
    private transient boolean TERM_ps_invalidation = false;
    	
    public void setInvalidationTERM_ps(boolean invalidation) { 
    	this.TERM_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_ps() {
    	return this.TERM_ps_invalidation;
    }
    	
    private transient boolean TERM_ps_modified = false;
    
    public boolean isModifiedTERM_ps() {
    	return this.TERM_ps_modified;
    }
    	
    public void unModifiedTERM_ps() {
    	this.TERM_ps_modified = false;
    }
    public FieldProperty getTERM_psFieldProperty() {
    	return fieldPropertyMap.get("TERM_ps");
    }
    
    public String getTERM_ps() {
    	return TERM_ps;
    }	
    public String getNvlTERM_ps() {
    	if(getTERM_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_ps();
    	}
    }
    public void setTERM_ps(String TERM_ps) {
    	if(TERM_ps == null) {
    		this.TERM_ps = null;
    	} else {
    		this.TERM_ps = TERM_ps;
    	}
    	this.TERM_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_length
     * Comments    : 
     */	
    private int TIME_length = 0;
    
    private transient boolean TIME_length_nullable = false;
    
    public boolean isNullableTIME_length() {
    	return this.TIME_length_nullable;
    }
    
    private transient boolean TIME_length_invalidation = false;
    	
    public void setInvalidationTIME_length(boolean invalidation) { 
    	this.TIME_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_length() {
    	return this.TIME_length_invalidation;
    }
    	
    private transient boolean TIME_length_modified = false;
    
    public boolean isModifiedTIME_length() {
    	return this.TIME_length_modified;
    }
    	
    public void unModifiedTIME_length() {
    	this.TIME_length_modified = false;
    }
    public FieldProperty getTIME_lengthFieldProperty() {
    	return fieldPropertyMap.get("TIME_length");
    }
    
    public int getTIME_length() {
    	return TIME_length;
    }	
    public void setTIME_length(int TIME_length) {
    	this.TIME_length = TIME_length;
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(Integer TIME_length) {
    	if( TIME_length == null) {
    		this.TIME_length = 0;
    	} else{
    		this.TIME_length = TIME_length.intValue();
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(String TIME_length) {
    	if  (TIME_length==null || TIME_length.length() == 0) {
    		this.TIME_length = 0;
    	} else {
    		this.TIME_length = Integer.parseInt(TIME_length);
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_color
     * Comments    : 
     */	
    private String TIME_color = null;
    
    private transient boolean TIME_color_nullable = true;
    
    public boolean isNullableTIME_color() {
    	return this.TIME_color_nullable;
    }
    
    private transient boolean TIME_color_invalidation = false;
    	
    public void setInvalidationTIME_color(boolean invalidation) { 
    	this.TIME_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_color() {
    	return this.TIME_color_invalidation;
    }
    	
    private transient boolean TIME_color_modified = false;
    
    public boolean isModifiedTIME_color() {
    	return this.TIME_color_modified;
    }
    	
    public void unModifiedTIME_color() {
    	this.TIME_color_modified = false;
    }
    public FieldProperty getTIME_colorFieldProperty() {
    	return fieldPropertyMap.get("TIME_color");
    }
    
    public String getTIME_color() {
    	return TIME_color;
    }	
    public String getNvlTIME_color() {
    	if(getTIME_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_color();
    	}
    }
    public void setTIME_color(String TIME_color) {
    	if(TIME_color == null) {
    		this.TIME_color = null;
    	} else {
    		this.TIME_color = TIME_color;
    	}
    	this.TIME_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_hilight
     * Comments    : 
     */	
    private String TIME_hilight = null;
    
    private transient boolean TIME_hilight_nullable = true;
    
    public boolean isNullableTIME_hilight() {
    	return this.TIME_hilight_nullable;
    }
    
    private transient boolean TIME_hilight_invalidation = false;
    	
    public void setInvalidationTIME_hilight(boolean invalidation) { 
    	this.TIME_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_hilight() {
    	return this.TIME_hilight_invalidation;
    }
    	
    private transient boolean TIME_hilight_modified = false;
    
    public boolean isModifiedTIME_hilight() {
    	return this.TIME_hilight_modified;
    }
    	
    public void unModifiedTIME_hilight() {
    	this.TIME_hilight_modified = false;
    }
    public FieldProperty getTIME_hilightFieldProperty() {
    	return fieldPropertyMap.get("TIME_hilight");
    }
    
    public String getTIME_hilight() {
    	return TIME_hilight;
    }	
    public String getNvlTIME_hilight() {
    	if(getTIME_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_hilight();
    	}
    }
    public void setTIME_hilight(String TIME_hilight) {
    	if(TIME_hilight == null) {
    		this.TIME_hilight = null;
    	} else {
    		this.TIME_hilight = TIME_hilight;
    	}
    	this.TIME_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_outline
     * Comments    : 
     */	
    private String TIME_outline = null;
    
    private transient boolean TIME_outline_nullable = true;
    
    public boolean isNullableTIME_outline() {
    	return this.TIME_outline_nullable;
    }
    
    private transient boolean TIME_outline_invalidation = false;
    	
    public void setInvalidationTIME_outline(boolean invalidation) { 
    	this.TIME_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_outline() {
    	return this.TIME_outline_invalidation;
    }
    	
    private transient boolean TIME_outline_modified = false;
    
    public boolean isModifiedTIME_outline() {
    	return this.TIME_outline_modified;
    }
    	
    public void unModifiedTIME_outline() {
    	this.TIME_outline_modified = false;
    }
    public FieldProperty getTIME_outlineFieldProperty() {
    	return fieldPropertyMap.get("TIME_outline");
    }
    
    public String getTIME_outline() {
    	return TIME_outline;
    }	
    public String getNvlTIME_outline() {
    	if(getTIME_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_outline();
    	}
    }
    public void setTIME_outline(String TIME_outline) {
    	if(TIME_outline == null) {
    		this.TIME_outline = null;
    	} else {
    		this.TIME_outline = TIME_outline;
    	}
    	this.TIME_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_transp
     * Comments    : 
     */	
    private String TIME_transp = null;
    
    private transient boolean TIME_transp_nullable = true;
    
    public boolean isNullableTIME_transp() {
    	return this.TIME_transp_nullable;
    }
    
    private transient boolean TIME_transp_invalidation = false;
    	
    public void setInvalidationTIME_transp(boolean invalidation) { 
    	this.TIME_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_transp() {
    	return this.TIME_transp_invalidation;
    }
    	
    private transient boolean TIME_transp_modified = false;
    
    public boolean isModifiedTIME_transp() {
    	return this.TIME_transp_modified;
    }
    	
    public void unModifiedTIME_transp() {
    	this.TIME_transp_modified = false;
    }
    public FieldProperty getTIME_transpFieldProperty() {
    	return fieldPropertyMap.get("TIME_transp");
    }
    
    public String getTIME_transp() {
    	return TIME_transp;
    }	
    public String getNvlTIME_transp() {
    	if(getTIME_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_transp();
    	}
    }
    public void setTIME_transp(String TIME_transp) {
    	if(TIME_transp == null) {
    		this.TIME_transp = null;
    	} else {
    		this.TIME_transp = TIME_transp;
    	}
    	this.TIME_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_validn
     * Comments    : 
     */	
    private String TIME_validn = null;
    
    private transient boolean TIME_validn_nullable = true;
    
    public boolean isNullableTIME_validn() {
    	return this.TIME_validn_nullable;
    }
    
    private transient boolean TIME_validn_invalidation = false;
    	
    public void setInvalidationTIME_validn(boolean invalidation) { 
    	this.TIME_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_validn() {
    	return this.TIME_validn_invalidation;
    }
    	
    private transient boolean TIME_validn_modified = false;
    
    public boolean isModifiedTIME_validn() {
    	return this.TIME_validn_modified;
    }
    	
    public void unModifiedTIME_validn() {
    	this.TIME_validn_modified = false;
    }
    public FieldProperty getTIME_validnFieldProperty() {
    	return fieldPropertyMap.get("TIME_validn");
    }
    
    public String getTIME_validn() {
    	return TIME_validn;
    }	
    public String getNvlTIME_validn() {
    	if(getTIME_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_validn();
    	}
    }
    public void setTIME_validn(String TIME_validn) {
    	if(TIME_validn == null) {
    		this.TIME_validn = null;
    	} else {
    		this.TIME_validn = TIME_validn;
    	}
    	this.TIME_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_sosi
     * Comments    : 
     */	
    private String TIME_sosi = null;
    
    private transient boolean TIME_sosi_nullable = true;
    
    public boolean isNullableTIME_sosi() {
    	return this.TIME_sosi_nullable;
    }
    
    private transient boolean TIME_sosi_invalidation = false;
    	
    public void setInvalidationTIME_sosi(boolean invalidation) { 
    	this.TIME_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_sosi() {
    	return this.TIME_sosi_invalidation;
    }
    	
    private transient boolean TIME_sosi_modified = false;
    
    public boolean isModifiedTIME_sosi() {
    	return this.TIME_sosi_modified;
    }
    	
    public void unModifiedTIME_sosi() {
    	this.TIME_sosi_modified = false;
    }
    public FieldProperty getTIME_sosiFieldProperty() {
    	return fieldPropertyMap.get("TIME_sosi");
    }
    
    public String getTIME_sosi() {
    	return TIME_sosi;
    }	
    public String getNvlTIME_sosi() {
    	if(getTIME_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_sosi();
    	}
    }
    public void setTIME_sosi(String TIME_sosi) {
    	if(TIME_sosi == null) {
    		this.TIME_sosi = null;
    	} else {
    		this.TIME_sosi = TIME_sosi;
    	}
    	this.TIME_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_ps
     * Comments    : 
     */	
    private String TIME_ps = null;
    
    private transient boolean TIME_ps_nullable = true;
    
    public boolean isNullableTIME_ps() {
    	return this.TIME_ps_nullable;
    }
    
    private transient boolean TIME_ps_invalidation = false;
    	
    public void setInvalidationTIME_ps(boolean invalidation) { 
    	this.TIME_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_ps() {
    	return this.TIME_ps_invalidation;
    }
    	
    private transient boolean TIME_ps_modified = false;
    
    public boolean isModifiedTIME_ps() {
    	return this.TIME_ps_modified;
    }
    	
    public void unModifiedTIME_ps() {
    	this.TIME_ps_modified = false;
    }
    public FieldProperty getTIME_psFieldProperty() {
    	return fieldPropertyMap.get("TIME_ps");
    }
    
    public String getTIME_ps() {
    	return TIME_ps;
    }	
    public String getNvlTIME_ps() {
    	if(getTIME_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_ps();
    	}
    }
    public void setTIME_ps(String TIME_ps) {
    	if(TIME_ps == null) {
    		this.TIME_ps = null;
    	} else {
    		this.TIME_ps = TIME_ps;
    	}
    	this.TIME_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_length
     * Comments    : 
     */	
    private int IDEN_length = 0;
    
    private transient boolean IDEN_length_nullable = false;
    
    public boolean isNullableIDEN_length() {
    	return this.IDEN_length_nullable;
    }
    
    private transient boolean IDEN_length_invalidation = false;
    	
    public void setInvalidationIDEN_length(boolean invalidation) { 
    	this.IDEN_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_length() {
    	return this.IDEN_length_invalidation;
    }
    	
    private transient boolean IDEN_length_modified = false;
    
    public boolean isModifiedIDEN_length() {
    	return this.IDEN_length_modified;
    }
    	
    public void unModifiedIDEN_length() {
    	this.IDEN_length_modified = false;
    }
    public FieldProperty getIDEN_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN_length");
    }
    
    public int getIDEN_length() {
    	return IDEN_length;
    }	
    public void setIDEN_length(int IDEN_length) {
    	this.IDEN_length = IDEN_length;
    	this.IDEN_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN_length(Integer IDEN_length) {
    	if( IDEN_length == null) {
    		this.IDEN_length = 0;
    	} else{
    		this.IDEN_length = IDEN_length.intValue();
    	}
    	this.IDEN_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN_length(String IDEN_length) {
    	if  (IDEN_length==null || IDEN_length.length() == 0) {
    		this.IDEN_length = 0;
    	} else {
    		this.IDEN_length = Integer.parseInt(IDEN_length);
    	}
    	this.IDEN_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_color
     * Comments    : 
     */	
    private String IDEN_color = null;
    
    private transient boolean IDEN_color_nullable = true;
    
    public boolean isNullableIDEN_color() {
    	return this.IDEN_color_nullable;
    }
    
    private transient boolean IDEN_color_invalidation = false;
    	
    public void setInvalidationIDEN_color(boolean invalidation) { 
    	this.IDEN_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_color() {
    	return this.IDEN_color_invalidation;
    }
    	
    private transient boolean IDEN_color_modified = false;
    
    public boolean isModifiedIDEN_color() {
    	return this.IDEN_color_modified;
    }
    	
    public void unModifiedIDEN_color() {
    	this.IDEN_color_modified = false;
    }
    public FieldProperty getIDEN_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN_color");
    }
    
    public String getIDEN_color() {
    	return IDEN_color;
    }	
    public String getNvlIDEN_color() {
    	if(getIDEN_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_color();
    	}
    }
    public void setIDEN_color(String IDEN_color) {
    	if(IDEN_color == null) {
    		this.IDEN_color = null;
    	} else {
    		this.IDEN_color = IDEN_color;
    	}
    	this.IDEN_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_hilight
     * Comments    : 
     */	
    private String IDEN_hilight = null;
    
    private transient boolean IDEN_hilight_nullable = true;
    
    public boolean isNullableIDEN_hilight() {
    	return this.IDEN_hilight_nullable;
    }
    
    private transient boolean IDEN_hilight_invalidation = false;
    	
    public void setInvalidationIDEN_hilight(boolean invalidation) { 
    	this.IDEN_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_hilight() {
    	return this.IDEN_hilight_invalidation;
    }
    	
    private transient boolean IDEN_hilight_modified = false;
    
    public boolean isModifiedIDEN_hilight() {
    	return this.IDEN_hilight_modified;
    }
    	
    public void unModifiedIDEN_hilight() {
    	this.IDEN_hilight_modified = false;
    }
    public FieldProperty getIDEN_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN_hilight");
    }
    
    public String getIDEN_hilight() {
    	return IDEN_hilight;
    }	
    public String getNvlIDEN_hilight() {
    	if(getIDEN_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_hilight();
    	}
    }
    public void setIDEN_hilight(String IDEN_hilight) {
    	if(IDEN_hilight == null) {
    		this.IDEN_hilight = null;
    	} else {
    		this.IDEN_hilight = IDEN_hilight;
    	}
    	this.IDEN_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_outline
     * Comments    : 
     */	
    private String IDEN_outline = null;
    
    private transient boolean IDEN_outline_nullable = true;
    
    public boolean isNullableIDEN_outline() {
    	return this.IDEN_outline_nullable;
    }
    
    private transient boolean IDEN_outline_invalidation = false;
    	
    public void setInvalidationIDEN_outline(boolean invalidation) { 
    	this.IDEN_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_outline() {
    	return this.IDEN_outline_invalidation;
    }
    	
    private transient boolean IDEN_outline_modified = false;
    
    public boolean isModifiedIDEN_outline() {
    	return this.IDEN_outline_modified;
    }
    	
    public void unModifiedIDEN_outline() {
    	this.IDEN_outline_modified = false;
    }
    public FieldProperty getIDEN_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN_outline");
    }
    
    public String getIDEN_outline() {
    	return IDEN_outline;
    }	
    public String getNvlIDEN_outline() {
    	if(getIDEN_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_outline();
    	}
    }
    public void setIDEN_outline(String IDEN_outline) {
    	if(IDEN_outline == null) {
    		this.IDEN_outline = null;
    	} else {
    		this.IDEN_outline = IDEN_outline;
    	}
    	this.IDEN_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_transp
     * Comments    : 
     */	
    private String IDEN_transp = null;
    
    private transient boolean IDEN_transp_nullable = true;
    
    public boolean isNullableIDEN_transp() {
    	return this.IDEN_transp_nullable;
    }
    
    private transient boolean IDEN_transp_invalidation = false;
    	
    public void setInvalidationIDEN_transp(boolean invalidation) { 
    	this.IDEN_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_transp() {
    	return this.IDEN_transp_invalidation;
    }
    	
    private transient boolean IDEN_transp_modified = false;
    
    public boolean isModifiedIDEN_transp() {
    	return this.IDEN_transp_modified;
    }
    	
    public void unModifiedIDEN_transp() {
    	this.IDEN_transp_modified = false;
    }
    public FieldProperty getIDEN_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN_transp");
    }
    
    public String getIDEN_transp() {
    	return IDEN_transp;
    }	
    public String getNvlIDEN_transp() {
    	if(getIDEN_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_transp();
    	}
    }
    public void setIDEN_transp(String IDEN_transp) {
    	if(IDEN_transp == null) {
    		this.IDEN_transp = null;
    	} else {
    		this.IDEN_transp = IDEN_transp;
    	}
    	this.IDEN_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_validn
     * Comments    : 
     */	
    private String IDEN_validn = null;
    
    private transient boolean IDEN_validn_nullable = true;
    
    public boolean isNullableIDEN_validn() {
    	return this.IDEN_validn_nullable;
    }
    
    private transient boolean IDEN_validn_invalidation = false;
    	
    public void setInvalidationIDEN_validn(boolean invalidation) { 
    	this.IDEN_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_validn() {
    	return this.IDEN_validn_invalidation;
    }
    	
    private transient boolean IDEN_validn_modified = false;
    
    public boolean isModifiedIDEN_validn() {
    	return this.IDEN_validn_modified;
    }
    	
    public void unModifiedIDEN_validn() {
    	this.IDEN_validn_modified = false;
    }
    public FieldProperty getIDEN_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN_validn");
    }
    
    public String getIDEN_validn() {
    	return IDEN_validn;
    }	
    public String getNvlIDEN_validn() {
    	if(getIDEN_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_validn();
    	}
    }
    public void setIDEN_validn(String IDEN_validn) {
    	if(IDEN_validn == null) {
    		this.IDEN_validn = null;
    	} else {
    		this.IDEN_validn = IDEN_validn;
    	}
    	this.IDEN_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_sosi
     * Comments    : 
     */	
    private String IDEN_sosi = null;
    
    private transient boolean IDEN_sosi_nullable = true;
    
    public boolean isNullableIDEN_sosi() {
    	return this.IDEN_sosi_nullable;
    }
    
    private transient boolean IDEN_sosi_invalidation = false;
    	
    public void setInvalidationIDEN_sosi(boolean invalidation) { 
    	this.IDEN_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_sosi() {
    	return this.IDEN_sosi_invalidation;
    }
    	
    private transient boolean IDEN_sosi_modified = false;
    
    public boolean isModifiedIDEN_sosi() {
    	return this.IDEN_sosi_modified;
    }
    	
    public void unModifiedIDEN_sosi() {
    	this.IDEN_sosi_modified = false;
    }
    public FieldProperty getIDEN_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN_sosi");
    }
    
    public String getIDEN_sosi() {
    	return IDEN_sosi;
    }	
    public String getNvlIDEN_sosi() {
    	if(getIDEN_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_sosi();
    	}
    }
    public void setIDEN_sosi(String IDEN_sosi) {
    	if(IDEN_sosi == null) {
    		this.IDEN_sosi = null;
    	} else {
    		this.IDEN_sosi = IDEN_sosi;
    	}
    	this.IDEN_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_ps
     * Comments    : 
     */	
    private String IDEN_ps = null;
    
    private transient boolean IDEN_ps_nullable = true;
    
    public boolean isNullableIDEN_ps() {
    	return this.IDEN_ps_nullable;
    }
    
    private transient boolean IDEN_ps_invalidation = false;
    	
    public void setInvalidationIDEN_ps(boolean invalidation) { 
    	this.IDEN_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_ps() {
    	return this.IDEN_ps_invalidation;
    }
    	
    private transient boolean IDEN_ps_modified = false;
    
    public boolean isModifiedIDEN_ps() {
    	return this.IDEN_ps_modified;
    }
    	
    public void unModifiedIDEN_ps() {
    	this.IDEN_ps_modified = false;
    }
    public FieldProperty getIDEN_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN_ps");
    }
    
    public String getIDEN_ps() {
    	return IDEN_ps;
    }	
    public String getNvlIDEN_ps() {
    	if(getIDEN_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_ps();
    	}
    }
    public void setIDEN_ps(String IDEN_ps) {
    	if(IDEN_ps == null) {
    		this.IDEN_ps = null;
    	} else {
    		this.IDEN_ps = IDEN_ps;
    	}
    	this.IDEN_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_length
     * Comments    : 
     */	
    private int NAME_length = 0;
    
    private transient boolean NAME_length_nullable = false;
    
    public boolean isNullableNAME_length() {
    	return this.NAME_length_nullable;
    }
    
    private transient boolean NAME_length_invalidation = false;
    	
    public void setInvalidationNAME_length(boolean invalidation) { 
    	this.NAME_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_length() {
    	return this.NAME_length_invalidation;
    }
    	
    private transient boolean NAME_length_modified = false;
    
    public boolean isModifiedNAME_length() {
    	return this.NAME_length_modified;
    }
    	
    public void unModifiedNAME_length() {
    	this.NAME_length_modified = false;
    }
    public FieldProperty getNAME_lengthFieldProperty() {
    	return fieldPropertyMap.get("NAME_length");
    }
    
    public int getNAME_length() {
    	return NAME_length;
    }	
    public void setNAME_length(int NAME_length) {
    	this.NAME_length = NAME_length;
    	this.NAME_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME_length(Integer NAME_length) {
    	if( NAME_length == null) {
    		this.NAME_length = 0;
    	} else{
    		this.NAME_length = NAME_length.intValue();
    	}
    	this.NAME_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME_length(String NAME_length) {
    	if  (NAME_length==null || NAME_length.length() == 0) {
    		this.NAME_length = 0;
    	} else {
    		this.NAME_length = Integer.parseInt(NAME_length);
    	}
    	this.NAME_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_color
     * Comments    : 
     */	
    private String NAME_color = null;
    
    private transient boolean NAME_color_nullable = true;
    
    public boolean isNullableNAME_color() {
    	return this.NAME_color_nullable;
    }
    
    private transient boolean NAME_color_invalidation = false;
    	
    public void setInvalidationNAME_color(boolean invalidation) { 
    	this.NAME_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_color() {
    	return this.NAME_color_invalidation;
    }
    	
    private transient boolean NAME_color_modified = false;
    
    public boolean isModifiedNAME_color() {
    	return this.NAME_color_modified;
    }
    	
    public void unModifiedNAME_color() {
    	this.NAME_color_modified = false;
    }
    public FieldProperty getNAME_colorFieldProperty() {
    	return fieldPropertyMap.get("NAME_color");
    }
    
    public String getNAME_color() {
    	return NAME_color;
    }	
    public String getNvlNAME_color() {
    	if(getNAME_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_color();
    	}
    }
    public void setNAME_color(String NAME_color) {
    	if(NAME_color == null) {
    		this.NAME_color = null;
    	} else {
    		this.NAME_color = NAME_color;
    	}
    	this.NAME_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_hilight
     * Comments    : 
     */	
    private String NAME_hilight = null;
    
    private transient boolean NAME_hilight_nullable = true;
    
    public boolean isNullableNAME_hilight() {
    	return this.NAME_hilight_nullable;
    }
    
    private transient boolean NAME_hilight_invalidation = false;
    	
    public void setInvalidationNAME_hilight(boolean invalidation) { 
    	this.NAME_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_hilight() {
    	return this.NAME_hilight_invalidation;
    }
    	
    private transient boolean NAME_hilight_modified = false;
    
    public boolean isModifiedNAME_hilight() {
    	return this.NAME_hilight_modified;
    }
    	
    public void unModifiedNAME_hilight() {
    	this.NAME_hilight_modified = false;
    }
    public FieldProperty getNAME_hilightFieldProperty() {
    	return fieldPropertyMap.get("NAME_hilight");
    }
    
    public String getNAME_hilight() {
    	return NAME_hilight;
    }	
    public String getNvlNAME_hilight() {
    	if(getNAME_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_hilight();
    	}
    }
    public void setNAME_hilight(String NAME_hilight) {
    	if(NAME_hilight == null) {
    		this.NAME_hilight = null;
    	} else {
    		this.NAME_hilight = NAME_hilight;
    	}
    	this.NAME_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_outline
     * Comments    : 
     */	
    private String NAME_outline = null;
    
    private transient boolean NAME_outline_nullable = true;
    
    public boolean isNullableNAME_outline() {
    	return this.NAME_outline_nullable;
    }
    
    private transient boolean NAME_outline_invalidation = false;
    	
    public void setInvalidationNAME_outline(boolean invalidation) { 
    	this.NAME_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_outline() {
    	return this.NAME_outline_invalidation;
    }
    	
    private transient boolean NAME_outline_modified = false;
    
    public boolean isModifiedNAME_outline() {
    	return this.NAME_outline_modified;
    }
    	
    public void unModifiedNAME_outline() {
    	this.NAME_outline_modified = false;
    }
    public FieldProperty getNAME_outlineFieldProperty() {
    	return fieldPropertyMap.get("NAME_outline");
    }
    
    public String getNAME_outline() {
    	return NAME_outline;
    }	
    public String getNvlNAME_outline() {
    	if(getNAME_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_outline();
    	}
    }
    public void setNAME_outline(String NAME_outline) {
    	if(NAME_outline == null) {
    		this.NAME_outline = null;
    	} else {
    		this.NAME_outline = NAME_outline;
    	}
    	this.NAME_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_transp
     * Comments    : 
     */	
    private String NAME_transp = null;
    
    private transient boolean NAME_transp_nullable = true;
    
    public boolean isNullableNAME_transp() {
    	return this.NAME_transp_nullable;
    }
    
    private transient boolean NAME_transp_invalidation = false;
    	
    public void setInvalidationNAME_transp(boolean invalidation) { 
    	this.NAME_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_transp() {
    	return this.NAME_transp_invalidation;
    }
    	
    private transient boolean NAME_transp_modified = false;
    
    public boolean isModifiedNAME_transp() {
    	return this.NAME_transp_modified;
    }
    	
    public void unModifiedNAME_transp() {
    	this.NAME_transp_modified = false;
    }
    public FieldProperty getNAME_transpFieldProperty() {
    	return fieldPropertyMap.get("NAME_transp");
    }
    
    public String getNAME_transp() {
    	return NAME_transp;
    }	
    public String getNvlNAME_transp() {
    	if(getNAME_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_transp();
    	}
    }
    public void setNAME_transp(String NAME_transp) {
    	if(NAME_transp == null) {
    		this.NAME_transp = null;
    	} else {
    		this.NAME_transp = NAME_transp;
    	}
    	this.NAME_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_validn
     * Comments    : 
     */	
    private String NAME_validn = null;
    
    private transient boolean NAME_validn_nullable = true;
    
    public boolean isNullableNAME_validn() {
    	return this.NAME_validn_nullable;
    }
    
    private transient boolean NAME_validn_invalidation = false;
    	
    public void setInvalidationNAME_validn(boolean invalidation) { 
    	this.NAME_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_validn() {
    	return this.NAME_validn_invalidation;
    }
    	
    private transient boolean NAME_validn_modified = false;
    
    public boolean isModifiedNAME_validn() {
    	return this.NAME_validn_modified;
    }
    	
    public void unModifiedNAME_validn() {
    	this.NAME_validn_modified = false;
    }
    public FieldProperty getNAME_validnFieldProperty() {
    	return fieldPropertyMap.get("NAME_validn");
    }
    
    public String getNAME_validn() {
    	return NAME_validn;
    }	
    public String getNvlNAME_validn() {
    	if(getNAME_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_validn();
    	}
    }
    public void setNAME_validn(String NAME_validn) {
    	if(NAME_validn == null) {
    		this.NAME_validn = null;
    	} else {
    		this.NAME_validn = NAME_validn;
    	}
    	this.NAME_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_sosi
     * Comments    : 
     */	
    private String NAME_sosi = null;
    
    private transient boolean NAME_sosi_nullable = true;
    
    public boolean isNullableNAME_sosi() {
    	return this.NAME_sosi_nullable;
    }
    
    private transient boolean NAME_sosi_invalidation = false;
    	
    public void setInvalidationNAME_sosi(boolean invalidation) { 
    	this.NAME_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_sosi() {
    	return this.NAME_sosi_invalidation;
    }
    	
    private transient boolean NAME_sosi_modified = false;
    
    public boolean isModifiedNAME_sosi() {
    	return this.NAME_sosi_modified;
    }
    	
    public void unModifiedNAME_sosi() {
    	this.NAME_sosi_modified = false;
    }
    public FieldProperty getNAME_sosiFieldProperty() {
    	return fieldPropertyMap.get("NAME_sosi");
    }
    
    public String getNAME_sosi() {
    	return NAME_sosi;
    }	
    public String getNvlNAME_sosi() {
    	if(getNAME_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_sosi();
    	}
    }
    public void setNAME_sosi(String NAME_sosi) {
    	if(NAME_sosi == null) {
    		this.NAME_sosi = null;
    	} else {
    		this.NAME_sosi = NAME_sosi;
    	}
    	this.NAME_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME_ps
     * Comments    : 
     */	
    private String NAME_ps = null;
    
    private transient boolean NAME_ps_nullable = true;
    
    public boolean isNullableNAME_ps() {
    	return this.NAME_ps_nullable;
    }
    
    private transient boolean NAME_ps_invalidation = false;
    	
    public void setInvalidationNAME_ps(boolean invalidation) { 
    	this.NAME_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME_ps() {
    	return this.NAME_ps_invalidation;
    }
    	
    private transient boolean NAME_ps_modified = false;
    
    public boolean isModifiedNAME_ps() {
    	return this.NAME_ps_modified;
    }
    	
    public void unModifiedNAME_ps() {
    	this.NAME_ps_modified = false;
    }
    public FieldProperty getNAME_psFieldProperty() {
    	return fieldPropertyMap.get("NAME_ps");
    }
    
    public String getNAME_ps() {
    	return NAME_ps;
    }	
    public String getNvlNAME_ps() {
    	if(getNAME_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME_ps();
    	}
    }
    public void setNAME_ps(String NAME_ps) {
    	if(NAME_ps == null) {
    		this.NAME_ps = null;
    	} else {
    		this.NAME_ps = NAME_ps;
    	}
    	this.NAME_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_length
     * Comments    : 
     */	
    private int DEPT_length = 0;
    
    private transient boolean DEPT_length_nullable = false;
    
    public boolean isNullableDEPT_length() {
    	return this.DEPT_length_nullable;
    }
    
    private transient boolean DEPT_length_invalidation = false;
    	
    public void setInvalidationDEPT_length(boolean invalidation) { 
    	this.DEPT_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_length() {
    	return this.DEPT_length_invalidation;
    }
    	
    private transient boolean DEPT_length_modified = false;
    
    public boolean isModifiedDEPT_length() {
    	return this.DEPT_length_modified;
    }
    	
    public void unModifiedDEPT_length() {
    	this.DEPT_length_modified = false;
    }
    public FieldProperty getDEPT_lengthFieldProperty() {
    	return fieldPropertyMap.get("DEPT_length");
    }
    
    public int getDEPT_length() {
    	return DEPT_length;
    }	
    public void setDEPT_length(int DEPT_length) {
    	this.DEPT_length = DEPT_length;
    	this.DEPT_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT_length(Integer DEPT_length) {
    	if( DEPT_length == null) {
    		this.DEPT_length = 0;
    	} else{
    		this.DEPT_length = DEPT_length.intValue();
    	}
    	this.DEPT_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT_length(String DEPT_length) {
    	if  (DEPT_length==null || DEPT_length.length() == 0) {
    		this.DEPT_length = 0;
    	} else {
    		this.DEPT_length = Integer.parseInt(DEPT_length);
    	}
    	this.DEPT_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_color
     * Comments    : 
     */	
    private String DEPT_color = null;
    
    private transient boolean DEPT_color_nullable = true;
    
    public boolean isNullableDEPT_color() {
    	return this.DEPT_color_nullable;
    }
    
    private transient boolean DEPT_color_invalidation = false;
    	
    public void setInvalidationDEPT_color(boolean invalidation) { 
    	this.DEPT_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_color() {
    	return this.DEPT_color_invalidation;
    }
    	
    private transient boolean DEPT_color_modified = false;
    
    public boolean isModifiedDEPT_color() {
    	return this.DEPT_color_modified;
    }
    	
    public void unModifiedDEPT_color() {
    	this.DEPT_color_modified = false;
    }
    public FieldProperty getDEPT_colorFieldProperty() {
    	return fieldPropertyMap.get("DEPT_color");
    }
    
    public String getDEPT_color() {
    	return DEPT_color;
    }	
    public String getNvlDEPT_color() {
    	if(getDEPT_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_color();
    	}
    }
    public void setDEPT_color(String DEPT_color) {
    	if(DEPT_color == null) {
    		this.DEPT_color = null;
    	} else {
    		this.DEPT_color = DEPT_color;
    	}
    	this.DEPT_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_hilight
     * Comments    : 
     */	
    private String DEPT_hilight = null;
    
    private transient boolean DEPT_hilight_nullable = true;
    
    public boolean isNullableDEPT_hilight() {
    	return this.DEPT_hilight_nullable;
    }
    
    private transient boolean DEPT_hilight_invalidation = false;
    	
    public void setInvalidationDEPT_hilight(boolean invalidation) { 
    	this.DEPT_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_hilight() {
    	return this.DEPT_hilight_invalidation;
    }
    	
    private transient boolean DEPT_hilight_modified = false;
    
    public boolean isModifiedDEPT_hilight() {
    	return this.DEPT_hilight_modified;
    }
    	
    public void unModifiedDEPT_hilight() {
    	this.DEPT_hilight_modified = false;
    }
    public FieldProperty getDEPT_hilightFieldProperty() {
    	return fieldPropertyMap.get("DEPT_hilight");
    }
    
    public String getDEPT_hilight() {
    	return DEPT_hilight;
    }	
    public String getNvlDEPT_hilight() {
    	if(getDEPT_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_hilight();
    	}
    }
    public void setDEPT_hilight(String DEPT_hilight) {
    	if(DEPT_hilight == null) {
    		this.DEPT_hilight = null;
    	} else {
    		this.DEPT_hilight = DEPT_hilight;
    	}
    	this.DEPT_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_outline
     * Comments    : 
     */	
    private String DEPT_outline = null;
    
    private transient boolean DEPT_outline_nullable = true;
    
    public boolean isNullableDEPT_outline() {
    	return this.DEPT_outline_nullable;
    }
    
    private transient boolean DEPT_outline_invalidation = false;
    	
    public void setInvalidationDEPT_outline(boolean invalidation) { 
    	this.DEPT_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_outline() {
    	return this.DEPT_outline_invalidation;
    }
    	
    private transient boolean DEPT_outline_modified = false;
    
    public boolean isModifiedDEPT_outline() {
    	return this.DEPT_outline_modified;
    }
    	
    public void unModifiedDEPT_outline() {
    	this.DEPT_outline_modified = false;
    }
    public FieldProperty getDEPT_outlineFieldProperty() {
    	return fieldPropertyMap.get("DEPT_outline");
    }
    
    public String getDEPT_outline() {
    	return DEPT_outline;
    }	
    public String getNvlDEPT_outline() {
    	if(getDEPT_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_outline();
    	}
    }
    public void setDEPT_outline(String DEPT_outline) {
    	if(DEPT_outline == null) {
    		this.DEPT_outline = null;
    	} else {
    		this.DEPT_outline = DEPT_outline;
    	}
    	this.DEPT_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_transp
     * Comments    : 
     */	
    private String DEPT_transp = null;
    
    private transient boolean DEPT_transp_nullable = true;
    
    public boolean isNullableDEPT_transp() {
    	return this.DEPT_transp_nullable;
    }
    
    private transient boolean DEPT_transp_invalidation = false;
    	
    public void setInvalidationDEPT_transp(boolean invalidation) { 
    	this.DEPT_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_transp() {
    	return this.DEPT_transp_invalidation;
    }
    	
    private transient boolean DEPT_transp_modified = false;
    
    public boolean isModifiedDEPT_transp() {
    	return this.DEPT_transp_modified;
    }
    	
    public void unModifiedDEPT_transp() {
    	this.DEPT_transp_modified = false;
    }
    public FieldProperty getDEPT_transpFieldProperty() {
    	return fieldPropertyMap.get("DEPT_transp");
    }
    
    public String getDEPT_transp() {
    	return DEPT_transp;
    }	
    public String getNvlDEPT_transp() {
    	if(getDEPT_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_transp();
    	}
    }
    public void setDEPT_transp(String DEPT_transp) {
    	if(DEPT_transp == null) {
    		this.DEPT_transp = null;
    	} else {
    		this.DEPT_transp = DEPT_transp;
    	}
    	this.DEPT_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_validn
     * Comments    : 
     */	
    private String DEPT_validn = null;
    
    private transient boolean DEPT_validn_nullable = true;
    
    public boolean isNullableDEPT_validn() {
    	return this.DEPT_validn_nullable;
    }
    
    private transient boolean DEPT_validn_invalidation = false;
    	
    public void setInvalidationDEPT_validn(boolean invalidation) { 
    	this.DEPT_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_validn() {
    	return this.DEPT_validn_invalidation;
    }
    	
    private transient boolean DEPT_validn_modified = false;
    
    public boolean isModifiedDEPT_validn() {
    	return this.DEPT_validn_modified;
    }
    	
    public void unModifiedDEPT_validn() {
    	this.DEPT_validn_modified = false;
    }
    public FieldProperty getDEPT_validnFieldProperty() {
    	return fieldPropertyMap.get("DEPT_validn");
    }
    
    public String getDEPT_validn() {
    	return DEPT_validn;
    }	
    public String getNvlDEPT_validn() {
    	if(getDEPT_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_validn();
    	}
    }
    public void setDEPT_validn(String DEPT_validn) {
    	if(DEPT_validn == null) {
    		this.DEPT_validn = null;
    	} else {
    		this.DEPT_validn = DEPT_validn;
    	}
    	this.DEPT_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_sosi
     * Comments    : 
     */	
    private String DEPT_sosi = null;
    
    private transient boolean DEPT_sosi_nullable = true;
    
    public boolean isNullableDEPT_sosi() {
    	return this.DEPT_sosi_nullable;
    }
    
    private transient boolean DEPT_sosi_invalidation = false;
    	
    public void setInvalidationDEPT_sosi(boolean invalidation) { 
    	this.DEPT_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_sosi() {
    	return this.DEPT_sosi_invalidation;
    }
    	
    private transient boolean DEPT_sosi_modified = false;
    
    public boolean isModifiedDEPT_sosi() {
    	return this.DEPT_sosi_modified;
    }
    	
    public void unModifiedDEPT_sosi() {
    	this.DEPT_sosi_modified = false;
    }
    public FieldProperty getDEPT_sosiFieldProperty() {
    	return fieldPropertyMap.get("DEPT_sosi");
    }
    
    public String getDEPT_sosi() {
    	return DEPT_sosi;
    }	
    public String getNvlDEPT_sosi() {
    	if(getDEPT_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_sosi();
    	}
    }
    public void setDEPT_sosi(String DEPT_sosi) {
    	if(DEPT_sosi == null) {
    		this.DEPT_sosi = null;
    	} else {
    		this.DEPT_sosi = DEPT_sosi;
    	}
    	this.DEPT_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT_ps
     * Comments    : 
     */	
    private String DEPT_ps = null;
    
    private transient boolean DEPT_ps_nullable = true;
    
    public boolean isNullableDEPT_ps() {
    	return this.DEPT_ps_nullable;
    }
    
    private transient boolean DEPT_ps_invalidation = false;
    	
    public void setInvalidationDEPT_ps(boolean invalidation) { 
    	this.DEPT_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT_ps() {
    	return this.DEPT_ps_invalidation;
    }
    	
    private transient boolean DEPT_ps_modified = false;
    
    public boolean isModifiedDEPT_ps() {
    	return this.DEPT_ps_modified;
    }
    	
    public void unModifiedDEPT_ps() {
    	this.DEPT_ps_modified = false;
    }
    public FieldProperty getDEPT_psFieldProperty() {
    	return fieldPropertyMap.get("DEPT_ps");
    }
    
    public String getDEPT_ps() {
    	return DEPT_ps;
    }	
    public String getNvlDEPT_ps() {
    	if(getDEPT_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT_ps();
    	}
    }
    public void setDEPT_ps(String DEPT_ps) {
    	if(DEPT_ps == null) {
    		this.DEPT_ps = null;
    	} else {
    		this.DEPT_ps = DEPT_ps;
    	}
    	this.DEPT_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_length
     * Comments    : 
     */	
    private int PHON_length = 0;
    
    private transient boolean PHON_length_nullable = false;
    
    public boolean isNullablePHON_length() {
    	return this.PHON_length_nullable;
    }
    
    private transient boolean PHON_length_invalidation = false;
    	
    public void setInvalidationPHON_length(boolean invalidation) { 
    	this.PHON_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_length() {
    	return this.PHON_length_invalidation;
    }
    	
    private transient boolean PHON_length_modified = false;
    
    public boolean isModifiedPHON_length() {
    	return this.PHON_length_modified;
    }
    	
    public void unModifiedPHON_length() {
    	this.PHON_length_modified = false;
    }
    public FieldProperty getPHON_lengthFieldProperty() {
    	return fieldPropertyMap.get("PHON_length");
    }
    
    public int getPHON_length() {
    	return PHON_length;
    }	
    public void setPHON_length(int PHON_length) {
    	this.PHON_length = PHON_length;
    	this.PHON_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON_length(Integer PHON_length) {
    	if( PHON_length == null) {
    		this.PHON_length = 0;
    	} else{
    		this.PHON_length = PHON_length.intValue();
    	}
    	this.PHON_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON_length(String PHON_length) {
    	if  (PHON_length==null || PHON_length.length() == 0) {
    		this.PHON_length = 0;
    	} else {
    		this.PHON_length = Integer.parseInt(PHON_length);
    	}
    	this.PHON_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_color
     * Comments    : 
     */	
    private String PHON_color = null;
    
    private transient boolean PHON_color_nullable = true;
    
    public boolean isNullablePHON_color() {
    	return this.PHON_color_nullable;
    }
    
    private transient boolean PHON_color_invalidation = false;
    	
    public void setInvalidationPHON_color(boolean invalidation) { 
    	this.PHON_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_color() {
    	return this.PHON_color_invalidation;
    }
    	
    private transient boolean PHON_color_modified = false;
    
    public boolean isModifiedPHON_color() {
    	return this.PHON_color_modified;
    }
    	
    public void unModifiedPHON_color() {
    	this.PHON_color_modified = false;
    }
    public FieldProperty getPHON_colorFieldProperty() {
    	return fieldPropertyMap.get("PHON_color");
    }
    
    public String getPHON_color() {
    	return PHON_color;
    }	
    public String getNvlPHON_color() {
    	if(getPHON_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_color();
    	}
    }
    public void setPHON_color(String PHON_color) {
    	if(PHON_color == null) {
    		this.PHON_color = null;
    	} else {
    		this.PHON_color = PHON_color;
    	}
    	this.PHON_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_hilight
     * Comments    : 
     */	
    private String PHON_hilight = null;
    
    private transient boolean PHON_hilight_nullable = true;
    
    public boolean isNullablePHON_hilight() {
    	return this.PHON_hilight_nullable;
    }
    
    private transient boolean PHON_hilight_invalidation = false;
    	
    public void setInvalidationPHON_hilight(boolean invalidation) { 
    	this.PHON_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_hilight() {
    	return this.PHON_hilight_invalidation;
    }
    	
    private transient boolean PHON_hilight_modified = false;
    
    public boolean isModifiedPHON_hilight() {
    	return this.PHON_hilight_modified;
    }
    	
    public void unModifiedPHON_hilight() {
    	this.PHON_hilight_modified = false;
    }
    public FieldProperty getPHON_hilightFieldProperty() {
    	return fieldPropertyMap.get("PHON_hilight");
    }
    
    public String getPHON_hilight() {
    	return PHON_hilight;
    }	
    public String getNvlPHON_hilight() {
    	if(getPHON_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_hilight();
    	}
    }
    public void setPHON_hilight(String PHON_hilight) {
    	if(PHON_hilight == null) {
    		this.PHON_hilight = null;
    	} else {
    		this.PHON_hilight = PHON_hilight;
    	}
    	this.PHON_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_outline
     * Comments    : 
     */	
    private String PHON_outline = null;
    
    private transient boolean PHON_outline_nullable = true;
    
    public boolean isNullablePHON_outline() {
    	return this.PHON_outline_nullable;
    }
    
    private transient boolean PHON_outline_invalidation = false;
    	
    public void setInvalidationPHON_outline(boolean invalidation) { 
    	this.PHON_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_outline() {
    	return this.PHON_outline_invalidation;
    }
    	
    private transient boolean PHON_outline_modified = false;
    
    public boolean isModifiedPHON_outline() {
    	return this.PHON_outline_modified;
    }
    	
    public void unModifiedPHON_outline() {
    	this.PHON_outline_modified = false;
    }
    public FieldProperty getPHON_outlineFieldProperty() {
    	return fieldPropertyMap.get("PHON_outline");
    }
    
    public String getPHON_outline() {
    	return PHON_outline;
    }	
    public String getNvlPHON_outline() {
    	if(getPHON_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_outline();
    	}
    }
    public void setPHON_outline(String PHON_outline) {
    	if(PHON_outline == null) {
    		this.PHON_outline = null;
    	} else {
    		this.PHON_outline = PHON_outline;
    	}
    	this.PHON_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_transp
     * Comments    : 
     */	
    private String PHON_transp = null;
    
    private transient boolean PHON_transp_nullable = true;
    
    public boolean isNullablePHON_transp() {
    	return this.PHON_transp_nullable;
    }
    
    private transient boolean PHON_transp_invalidation = false;
    	
    public void setInvalidationPHON_transp(boolean invalidation) { 
    	this.PHON_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_transp() {
    	return this.PHON_transp_invalidation;
    }
    	
    private transient boolean PHON_transp_modified = false;
    
    public boolean isModifiedPHON_transp() {
    	return this.PHON_transp_modified;
    }
    	
    public void unModifiedPHON_transp() {
    	this.PHON_transp_modified = false;
    }
    public FieldProperty getPHON_transpFieldProperty() {
    	return fieldPropertyMap.get("PHON_transp");
    }
    
    public String getPHON_transp() {
    	return PHON_transp;
    }	
    public String getNvlPHON_transp() {
    	if(getPHON_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_transp();
    	}
    }
    public void setPHON_transp(String PHON_transp) {
    	if(PHON_transp == null) {
    		this.PHON_transp = null;
    	} else {
    		this.PHON_transp = PHON_transp;
    	}
    	this.PHON_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_validn
     * Comments    : 
     */	
    private String PHON_validn = null;
    
    private transient boolean PHON_validn_nullable = true;
    
    public boolean isNullablePHON_validn() {
    	return this.PHON_validn_nullable;
    }
    
    private transient boolean PHON_validn_invalidation = false;
    	
    public void setInvalidationPHON_validn(boolean invalidation) { 
    	this.PHON_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_validn() {
    	return this.PHON_validn_invalidation;
    }
    	
    private transient boolean PHON_validn_modified = false;
    
    public boolean isModifiedPHON_validn() {
    	return this.PHON_validn_modified;
    }
    	
    public void unModifiedPHON_validn() {
    	this.PHON_validn_modified = false;
    }
    public FieldProperty getPHON_validnFieldProperty() {
    	return fieldPropertyMap.get("PHON_validn");
    }
    
    public String getPHON_validn() {
    	return PHON_validn;
    }	
    public String getNvlPHON_validn() {
    	if(getPHON_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_validn();
    	}
    }
    public void setPHON_validn(String PHON_validn) {
    	if(PHON_validn == null) {
    		this.PHON_validn = null;
    	} else {
    		this.PHON_validn = PHON_validn;
    	}
    	this.PHON_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_sosi
     * Comments    : 
     */	
    private String PHON_sosi = null;
    
    private transient boolean PHON_sosi_nullable = true;
    
    public boolean isNullablePHON_sosi() {
    	return this.PHON_sosi_nullable;
    }
    
    private transient boolean PHON_sosi_invalidation = false;
    	
    public void setInvalidationPHON_sosi(boolean invalidation) { 
    	this.PHON_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_sosi() {
    	return this.PHON_sosi_invalidation;
    }
    	
    private transient boolean PHON_sosi_modified = false;
    
    public boolean isModifiedPHON_sosi() {
    	return this.PHON_sosi_modified;
    }
    	
    public void unModifiedPHON_sosi() {
    	this.PHON_sosi_modified = false;
    }
    public FieldProperty getPHON_sosiFieldProperty() {
    	return fieldPropertyMap.get("PHON_sosi");
    }
    
    public String getPHON_sosi() {
    	return PHON_sosi;
    }	
    public String getNvlPHON_sosi() {
    	if(getPHON_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_sosi();
    	}
    }
    public void setPHON_sosi(String PHON_sosi) {
    	if(PHON_sosi == null) {
    		this.PHON_sosi = null;
    	} else {
    		this.PHON_sosi = PHON_sosi;
    	}
    	this.PHON_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON_ps
     * Comments    : 
     */	
    private String PHON_ps = null;
    
    private transient boolean PHON_ps_nullable = true;
    
    public boolean isNullablePHON_ps() {
    	return this.PHON_ps_nullable;
    }
    
    private transient boolean PHON_ps_invalidation = false;
    	
    public void setInvalidationPHON_ps(boolean invalidation) { 
    	this.PHON_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON_ps() {
    	return this.PHON_ps_invalidation;
    }
    	
    private transient boolean PHON_ps_modified = false;
    
    public boolean isModifiedPHON_ps() {
    	return this.PHON_ps_modified;
    }
    	
    public void unModifiedPHON_ps() {
    	this.PHON_ps_modified = false;
    }
    public FieldProperty getPHON_psFieldProperty() {
    	return fieldPropertyMap.get("PHON_ps");
    }
    
    public String getPHON_ps() {
    	return PHON_ps;
    }	
    public String getNvlPHON_ps() {
    	if(getPHON_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON_ps();
    	}
    }
    public void setPHON_ps(String PHON_ps) {
    	if(PHON_ps == null) {
    		this.PHON_ps = null;
    	} else {
    		this.PHON_ps = PHON_ps;
    	}
    	this.PHON_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_length
     * Comments    : 
     */	
    private int EMAL_length = 0;
    
    private transient boolean EMAL_length_nullable = false;
    
    public boolean isNullableEMAL_length() {
    	return this.EMAL_length_nullable;
    }
    
    private transient boolean EMAL_length_invalidation = false;
    	
    public void setInvalidationEMAL_length(boolean invalidation) { 
    	this.EMAL_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_length() {
    	return this.EMAL_length_invalidation;
    }
    	
    private transient boolean EMAL_length_modified = false;
    
    public boolean isModifiedEMAL_length() {
    	return this.EMAL_length_modified;
    }
    	
    public void unModifiedEMAL_length() {
    	this.EMAL_length_modified = false;
    }
    public FieldProperty getEMAL_lengthFieldProperty() {
    	return fieldPropertyMap.get("EMAL_length");
    }
    
    public int getEMAL_length() {
    	return EMAL_length;
    }	
    public void setEMAL_length(int EMAL_length) {
    	this.EMAL_length = EMAL_length;
    	this.EMAL_length_modified = true;
    	this.isModified = true;
    }
    public void setEMAL_length(Integer EMAL_length) {
    	if( EMAL_length == null) {
    		this.EMAL_length = 0;
    	} else{
    		this.EMAL_length = EMAL_length.intValue();
    	}
    	this.EMAL_length_modified = true;
    	this.isModified = true;
    }
    public void setEMAL_length(String EMAL_length) {
    	if  (EMAL_length==null || EMAL_length.length() == 0) {
    		this.EMAL_length = 0;
    	} else {
    		this.EMAL_length = Integer.parseInt(EMAL_length);
    	}
    	this.EMAL_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_color
     * Comments    : 
     */	
    private String EMAL_color = null;
    
    private transient boolean EMAL_color_nullable = true;
    
    public boolean isNullableEMAL_color() {
    	return this.EMAL_color_nullable;
    }
    
    private transient boolean EMAL_color_invalidation = false;
    	
    public void setInvalidationEMAL_color(boolean invalidation) { 
    	this.EMAL_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_color() {
    	return this.EMAL_color_invalidation;
    }
    	
    private transient boolean EMAL_color_modified = false;
    
    public boolean isModifiedEMAL_color() {
    	return this.EMAL_color_modified;
    }
    	
    public void unModifiedEMAL_color() {
    	this.EMAL_color_modified = false;
    }
    public FieldProperty getEMAL_colorFieldProperty() {
    	return fieldPropertyMap.get("EMAL_color");
    }
    
    public String getEMAL_color() {
    	return EMAL_color;
    }	
    public String getNvlEMAL_color() {
    	if(getEMAL_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_color();
    	}
    }
    public void setEMAL_color(String EMAL_color) {
    	if(EMAL_color == null) {
    		this.EMAL_color = null;
    	} else {
    		this.EMAL_color = EMAL_color;
    	}
    	this.EMAL_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_hilight
     * Comments    : 
     */	
    private String EMAL_hilight = null;
    
    private transient boolean EMAL_hilight_nullable = true;
    
    public boolean isNullableEMAL_hilight() {
    	return this.EMAL_hilight_nullable;
    }
    
    private transient boolean EMAL_hilight_invalidation = false;
    	
    public void setInvalidationEMAL_hilight(boolean invalidation) { 
    	this.EMAL_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_hilight() {
    	return this.EMAL_hilight_invalidation;
    }
    	
    private transient boolean EMAL_hilight_modified = false;
    
    public boolean isModifiedEMAL_hilight() {
    	return this.EMAL_hilight_modified;
    }
    	
    public void unModifiedEMAL_hilight() {
    	this.EMAL_hilight_modified = false;
    }
    public FieldProperty getEMAL_hilightFieldProperty() {
    	return fieldPropertyMap.get("EMAL_hilight");
    }
    
    public String getEMAL_hilight() {
    	return EMAL_hilight;
    }	
    public String getNvlEMAL_hilight() {
    	if(getEMAL_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_hilight();
    	}
    }
    public void setEMAL_hilight(String EMAL_hilight) {
    	if(EMAL_hilight == null) {
    		this.EMAL_hilight = null;
    	} else {
    		this.EMAL_hilight = EMAL_hilight;
    	}
    	this.EMAL_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_outline
     * Comments    : 
     */	
    private String EMAL_outline = null;
    
    private transient boolean EMAL_outline_nullable = true;
    
    public boolean isNullableEMAL_outline() {
    	return this.EMAL_outline_nullable;
    }
    
    private transient boolean EMAL_outline_invalidation = false;
    	
    public void setInvalidationEMAL_outline(boolean invalidation) { 
    	this.EMAL_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_outline() {
    	return this.EMAL_outline_invalidation;
    }
    	
    private transient boolean EMAL_outline_modified = false;
    
    public boolean isModifiedEMAL_outline() {
    	return this.EMAL_outline_modified;
    }
    	
    public void unModifiedEMAL_outline() {
    	this.EMAL_outline_modified = false;
    }
    public FieldProperty getEMAL_outlineFieldProperty() {
    	return fieldPropertyMap.get("EMAL_outline");
    }
    
    public String getEMAL_outline() {
    	return EMAL_outline;
    }	
    public String getNvlEMAL_outline() {
    	if(getEMAL_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_outline();
    	}
    }
    public void setEMAL_outline(String EMAL_outline) {
    	if(EMAL_outline == null) {
    		this.EMAL_outline = null;
    	} else {
    		this.EMAL_outline = EMAL_outline;
    	}
    	this.EMAL_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_transp
     * Comments    : 
     */	
    private String EMAL_transp = null;
    
    private transient boolean EMAL_transp_nullable = true;
    
    public boolean isNullableEMAL_transp() {
    	return this.EMAL_transp_nullable;
    }
    
    private transient boolean EMAL_transp_invalidation = false;
    	
    public void setInvalidationEMAL_transp(boolean invalidation) { 
    	this.EMAL_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_transp() {
    	return this.EMAL_transp_invalidation;
    }
    	
    private transient boolean EMAL_transp_modified = false;
    
    public boolean isModifiedEMAL_transp() {
    	return this.EMAL_transp_modified;
    }
    	
    public void unModifiedEMAL_transp() {
    	this.EMAL_transp_modified = false;
    }
    public FieldProperty getEMAL_transpFieldProperty() {
    	return fieldPropertyMap.get("EMAL_transp");
    }
    
    public String getEMAL_transp() {
    	return EMAL_transp;
    }	
    public String getNvlEMAL_transp() {
    	if(getEMAL_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_transp();
    	}
    }
    public void setEMAL_transp(String EMAL_transp) {
    	if(EMAL_transp == null) {
    		this.EMAL_transp = null;
    	} else {
    		this.EMAL_transp = EMAL_transp;
    	}
    	this.EMAL_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_validn
     * Comments    : 
     */	
    private String EMAL_validn = null;
    
    private transient boolean EMAL_validn_nullable = true;
    
    public boolean isNullableEMAL_validn() {
    	return this.EMAL_validn_nullable;
    }
    
    private transient boolean EMAL_validn_invalidation = false;
    	
    public void setInvalidationEMAL_validn(boolean invalidation) { 
    	this.EMAL_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_validn() {
    	return this.EMAL_validn_invalidation;
    }
    	
    private transient boolean EMAL_validn_modified = false;
    
    public boolean isModifiedEMAL_validn() {
    	return this.EMAL_validn_modified;
    }
    	
    public void unModifiedEMAL_validn() {
    	this.EMAL_validn_modified = false;
    }
    public FieldProperty getEMAL_validnFieldProperty() {
    	return fieldPropertyMap.get("EMAL_validn");
    }
    
    public String getEMAL_validn() {
    	return EMAL_validn;
    }	
    public String getNvlEMAL_validn() {
    	if(getEMAL_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_validn();
    	}
    }
    public void setEMAL_validn(String EMAL_validn) {
    	if(EMAL_validn == null) {
    		this.EMAL_validn = null;
    	} else {
    		this.EMAL_validn = EMAL_validn;
    	}
    	this.EMAL_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_sosi
     * Comments    : 
     */	
    private String EMAL_sosi = null;
    
    private transient boolean EMAL_sosi_nullable = true;
    
    public boolean isNullableEMAL_sosi() {
    	return this.EMAL_sosi_nullable;
    }
    
    private transient boolean EMAL_sosi_invalidation = false;
    	
    public void setInvalidationEMAL_sosi(boolean invalidation) { 
    	this.EMAL_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_sosi() {
    	return this.EMAL_sosi_invalidation;
    }
    	
    private transient boolean EMAL_sosi_modified = false;
    
    public boolean isModifiedEMAL_sosi() {
    	return this.EMAL_sosi_modified;
    }
    	
    public void unModifiedEMAL_sosi() {
    	this.EMAL_sosi_modified = false;
    }
    public FieldProperty getEMAL_sosiFieldProperty() {
    	return fieldPropertyMap.get("EMAL_sosi");
    }
    
    public String getEMAL_sosi() {
    	return EMAL_sosi;
    }	
    public String getNvlEMAL_sosi() {
    	if(getEMAL_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_sosi();
    	}
    }
    public void setEMAL_sosi(String EMAL_sosi) {
    	if(EMAL_sosi == null) {
    		this.EMAL_sosi = null;
    	} else {
    		this.EMAL_sosi = EMAL_sosi;
    	}
    	this.EMAL_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL_ps
     * Comments    : 
     */	
    private String EMAL_ps = null;
    
    private transient boolean EMAL_ps_nullable = true;
    
    public boolean isNullableEMAL_ps() {
    	return this.EMAL_ps_nullable;
    }
    
    private transient boolean EMAL_ps_invalidation = false;
    	
    public void setInvalidationEMAL_ps(boolean invalidation) { 
    	this.EMAL_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL_ps() {
    	return this.EMAL_ps_invalidation;
    }
    	
    private transient boolean EMAL_ps_modified = false;
    
    public boolean isModifiedEMAL_ps() {
    	return this.EMAL_ps_modified;
    }
    	
    public void unModifiedEMAL_ps() {
    	this.EMAL_ps_modified = false;
    }
    public FieldProperty getEMAL_psFieldProperty() {
    	return fieldPropertyMap.get("EMAL_ps");
    }
    
    public String getEMAL_ps() {
    	return EMAL_ps;
    }	
    public String getNvlEMAL_ps() {
    	if(getEMAL_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL_ps();
    	}
    }
    public void setEMAL_ps(String EMAL_ps) {
    	if(EMAL_ps == null) {
    		this.EMAL_ps = null;
    	} else {
    		this.EMAL_ps = EMAL_ps;
    	}
    	this.EMAL_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_length
     * Comments    : 
     */	
    private int ADDR_length = 0;
    
    private transient boolean ADDR_length_nullable = false;
    
    public boolean isNullableADDR_length() {
    	return this.ADDR_length_nullable;
    }
    
    private transient boolean ADDR_length_invalidation = false;
    	
    public void setInvalidationADDR_length(boolean invalidation) { 
    	this.ADDR_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_length() {
    	return this.ADDR_length_invalidation;
    }
    	
    private transient boolean ADDR_length_modified = false;
    
    public boolean isModifiedADDR_length() {
    	return this.ADDR_length_modified;
    }
    	
    public void unModifiedADDR_length() {
    	this.ADDR_length_modified = false;
    }
    public FieldProperty getADDR_lengthFieldProperty() {
    	return fieldPropertyMap.get("ADDR_length");
    }
    
    public int getADDR_length() {
    	return ADDR_length;
    }	
    public void setADDR_length(int ADDR_length) {
    	this.ADDR_length = ADDR_length;
    	this.ADDR_length_modified = true;
    	this.isModified = true;
    }
    public void setADDR_length(Integer ADDR_length) {
    	if( ADDR_length == null) {
    		this.ADDR_length = 0;
    	} else{
    		this.ADDR_length = ADDR_length.intValue();
    	}
    	this.ADDR_length_modified = true;
    	this.isModified = true;
    }
    public void setADDR_length(String ADDR_length) {
    	if  (ADDR_length==null || ADDR_length.length() == 0) {
    		this.ADDR_length = 0;
    	} else {
    		this.ADDR_length = Integer.parseInt(ADDR_length);
    	}
    	this.ADDR_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_color
     * Comments    : 
     */	
    private String ADDR_color = null;
    
    private transient boolean ADDR_color_nullable = true;
    
    public boolean isNullableADDR_color() {
    	return this.ADDR_color_nullable;
    }
    
    private transient boolean ADDR_color_invalidation = false;
    	
    public void setInvalidationADDR_color(boolean invalidation) { 
    	this.ADDR_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_color() {
    	return this.ADDR_color_invalidation;
    }
    	
    private transient boolean ADDR_color_modified = false;
    
    public boolean isModifiedADDR_color() {
    	return this.ADDR_color_modified;
    }
    	
    public void unModifiedADDR_color() {
    	this.ADDR_color_modified = false;
    }
    public FieldProperty getADDR_colorFieldProperty() {
    	return fieldPropertyMap.get("ADDR_color");
    }
    
    public String getADDR_color() {
    	return ADDR_color;
    }	
    public String getNvlADDR_color() {
    	if(getADDR_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_color();
    	}
    }
    public void setADDR_color(String ADDR_color) {
    	if(ADDR_color == null) {
    		this.ADDR_color = null;
    	} else {
    		this.ADDR_color = ADDR_color;
    	}
    	this.ADDR_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_hilight
     * Comments    : 
     */	
    private String ADDR_hilight = null;
    
    private transient boolean ADDR_hilight_nullable = true;
    
    public boolean isNullableADDR_hilight() {
    	return this.ADDR_hilight_nullable;
    }
    
    private transient boolean ADDR_hilight_invalidation = false;
    	
    public void setInvalidationADDR_hilight(boolean invalidation) { 
    	this.ADDR_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_hilight() {
    	return this.ADDR_hilight_invalidation;
    }
    	
    private transient boolean ADDR_hilight_modified = false;
    
    public boolean isModifiedADDR_hilight() {
    	return this.ADDR_hilight_modified;
    }
    	
    public void unModifiedADDR_hilight() {
    	this.ADDR_hilight_modified = false;
    }
    public FieldProperty getADDR_hilightFieldProperty() {
    	return fieldPropertyMap.get("ADDR_hilight");
    }
    
    public String getADDR_hilight() {
    	return ADDR_hilight;
    }	
    public String getNvlADDR_hilight() {
    	if(getADDR_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_hilight();
    	}
    }
    public void setADDR_hilight(String ADDR_hilight) {
    	if(ADDR_hilight == null) {
    		this.ADDR_hilight = null;
    	} else {
    		this.ADDR_hilight = ADDR_hilight;
    	}
    	this.ADDR_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_outline
     * Comments    : 
     */	
    private String ADDR_outline = null;
    
    private transient boolean ADDR_outline_nullable = true;
    
    public boolean isNullableADDR_outline() {
    	return this.ADDR_outline_nullable;
    }
    
    private transient boolean ADDR_outline_invalidation = false;
    	
    public void setInvalidationADDR_outline(boolean invalidation) { 
    	this.ADDR_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_outline() {
    	return this.ADDR_outline_invalidation;
    }
    	
    private transient boolean ADDR_outline_modified = false;
    
    public boolean isModifiedADDR_outline() {
    	return this.ADDR_outline_modified;
    }
    	
    public void unModifiedADDR_outline() {
    	this.ADDR_outline_modified = false;
    }
    public FieldProperty getADDR_outlineFieldProperty() {
    	return fieldPropertyMap.get("ADDR_outline");
    }
    
    public String getADDR_outline() {
    	return ADDR_outline;
    }	
    public String getNvlADDR_outline() {
    	if(getADDR_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_outline();
    	}
    }
    public void setADDR_outline(String ADDR_outline) {
    	if(ADDR_outline == null) {
    		this.ADDR_outline = null;
    	} else {
    		this.ADDR_outline = ADDR_outline;
    	}
    	this.ADDR_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_transp
     * Comments    : 
     */	
    private String ADDR_transp = null;
    
    private transient boolean ADDR_transp_nullable = true;
    
    public boolean isNullableADDR_transp() {
    	return this.ADDR_transp_nullable;
    }
    
    private transient boolean ADDR_transp_invalidation = false;
    	
    public void setInvalidationADDR_transp(boolean invalidation) { 
    	this.ADDR_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_transp() {
    	return this.ADDR_transp_invalidation;
    }
    	
    private transient boolean ADDR_transp_modified = false;
    
    public boolean isModifiedADDR_transp() {
    	return this.ADDR_transp_modified;
    }
    	
    public void unModifiedADDR_transp() {
    	this.ADDR_transp_modified = false;
    }
    public FieldProperty getADDR_transpFieldProperty() {
    	return fieldPropertyMap.get("ADDR_transp");
    }
    
    public String getADDR_transp() {
    	return ADDR_transp;
    }	
    public String getNvlADDR_transp() {
    	if(getADDR_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_transp();
    	}
    }
    public void setADDR_transp(String ADDR_transp) {
    	if(ADDR_transp == null) {
    		this.ADDR_transp = null;
    	} else {
    		this.ADDR_transp = ADDR_transp;
    	}
    	this.ADDR_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_validn
     * Comments    : 
     */	
    private String ADDR_validn = null;
    
    private transient boolean ADDR_validn_nullable = true;
    
    public boolean isNullableADDR_validn() {
    	return this.ADDR_validn_nullable;
    }
    
    private transient boolean ADDR_validn_invalidation = false;
    	
    public void setInvalidationADDR_validn(boolean invalidation) { 
    	this.ADDR_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_validn() {
    	return this.ADDR_validn_invalidation;
    }
    	
    private transient boolean ADDR_validn_modified = false;
    
    public boolean isModifiedADDR_validn() {
    	return this.ADDR_validn_modified;
    }
    	
    public void unModifiedADDR_validn() {
    	this.ADDR_validn_modified = false;
    }
    public FieldProperty getADDR_validnFieldProperty() {
    	return fieldPropertyMap.get("ADDR_validn");
    }
    
    public String getADDR_validn() {
    	return ADDR_validn;
    }	
    public String getNvlADDR_validn() {
    	if(getADDR_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_validn();
    	}
    }
    public void setADDR_validn(String ADDR_validn) {
    	if(ADDR_validn == null) {
    		this.ADDR_validn = null;
    	} else {
    		this.ADDR_validn = ADDR_validn;
    	}
    	this.ADDR_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_sosi
     * Comments    : 
     */	
    private String ADDR_sosi = null;
    
    private transient boolean ADDR_sosi_nullable = true;
    
    public boolean isNullableADDR_sosi() {
    	return this.ADDR_sosi_nullable;
    }
    
    private transient boolean ADDR_sosi_invalidation = false;
    	
    public void setInvalidationADDR_sosi(boolean invalidation) { 
    	this.ADDR_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_sosi() {
    	return this.ADDR_sosi_invalidation;
    }
    	
    private transient boolean ADDR_sosi_modified = false;
    
    public boolean isModifiedADDR_sosi() {
    	return this.ADDR_sosi_modified;
    }
    	
    public void unModifiedADDR_sosi() {
    	this.ADDR_sosi_modified = false;
    }
    public FieldProperty getADDR_sosiFieldProperty() {
    	return fieldPropertyMap.get("ADDR_sosi");
    }
    
    public String getADDR_sosi() {
    	return ADDR_sosi;
    }	
    public String getNvlADDR_sosi() {
    	if(getADDR_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_sosi();
    	}
    }
    public void setADDR_sosi(String ADDR_sosi) {
    	if(ADDR_sosi == null) {
    		this.ADDR_sosi = null;
    	} else {
    		this.ADDR_sosi = ADDR_sosi;
    	}
    	this.ADDR_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR_ps
     * Comments    : 
     */	
    private String ADDR_ps = null;
    
    private transient boolean ADDR_ps_nullable = true;
    
    public boolean isNullableADDR_ps() {
    	return this.ADDR_ps_nullable;
    }
    
    private transient boolean ADDR_ps_invalidation = false;
    	
    public void setInvalidationADDR_ps(boolean invalidation) { 
    	this.ADDR_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR_ps() {
    	return this.ADDR_ps_invalidation;
    }
    	
    private transient boolean ADDR_ps_modified = false;
    
    public boolean isModifiedADDR_ps() {
    	return this.ADDR_ps_modified;
    }
    	
    public void unModifiedADDR_ps() {
    	this.ADDR_ps_modified = false;
    }
    public FieldProperty getADDR_psFieldProperty() {
    	return fieldPropertyMap.get("ADDR_ps");
    }
    
    public String getADDR_ps() {
    	return ADDR_ps;
    }	
    public String getNvlADDR_ps() {
    	if(getADDR_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR_ps();
    	}
    }
    public void setADDR_ps(String ADDR_ps) {
    	if(ADDR_ps == null) {
    		this.ADDR_ps = null;
    	} else {
    		this.ADDR_ps = ADDR_ps;
    	}
    	this.ADDR_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_length
     * Comments    : 
     */	
    private int MSG_length = 0;
    
    private transient boolean MSG_length_nullable = false;
    
    public boolean isNullableMSG_length() {
    	return this.MSG_length_nullable;
    }
    
    private transient boolean MSG_length_invalidation = false;
    	
    public void setInvalidationMSG_length(boolean invalidation) { 
    	this.MSG_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_length() {
    	return this.MSG_length_invalidation;
    }
    	
    private transient boolean MSG_length_modified = false;
    
    public boolean isModifiedMSG_length() {
    	return this.MSG_length_modified;
    }
    	
    public void unModifiedMSG_length() {
    	this.MSG_length_modified = false;
    }
    public FieldProperty getMSG_lengthFieldProperty() {
    	return fieldPropertyMap.get("MSG_length");
    }
    
    public int getMSG_length() {
    	return MSG_length;
    }	
    public void setMSG_length(int MSG_length) {
    	this.MSG_length = MSG_length;
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(Integer MSG_length) {
    	if( MSG_length == null) {
    		this.MSG_length = 0;
    	} else{
    		this.MSG_length = MSG_length.intValue();
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(String MSG_length) {
    	if  (MSG_length==null || MSG_length.length() == 0) {
    		this.MSG_length = 0;
    	} else {
    		this.MSG_length = Integer.parseInt(MSG_length);
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_color
     * Comments    : 
     */	
    private String MSG_color = null;
    
    private transient boolean MSG_color_nullable = true;
    
    public boolean isNullableMSG_color() {
    	return this.MSG_color_nullable;
    }
    
    private transient boolean MSG_color_invalidation = false;
    	
    public void setInvalidationMSG_color(boolean invalidation) { 
    	this.MSG_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_color() {
    	return this.MSG_color_invalidation;
    }
    	
    private transient boolean MSG_color_modified = false;
    
    public boolean isModifiedMSG_color() {
    	return this.MSG_color_modified;
    }
    	
    public void unModifiedMSG_color() {
    	this.MSG_color_modified = false;
    }
    public FieldProperty getMSG_colorFieldProperty() {
    	return fieldPropertyMap.get("MSG_color");
    }
    
    public String getMSG_color() {
    	return MSG_color;
    }	
    public String getNvlMSG_color() {
    	if(getMSG_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_color();
    	}
    }
    public void setMSG_color(String MSG_color) {
    	if(MSG_color == null) {
    		this.MSG_color = null;
    	} else {
    		this.MSG_color = MSG_color;
    	}
    	this.MSG_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_hilight
     * Comments    : 
     */	
    private String MSG_hilight = null;
    
    private transient boolean MSG_hilight_nullable = true;
    
    public boolean isNullableMSG_hilight() {
    	return this.MSG_hilight_nullable;
    }
    
    private transient boolean MSG_hilight_invalidation = false;
    	
    public void setInvalidationMSG_hilight(boolean invalidation) { 
    	this.MSG_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_hilight() {
    	return this.MSG_hilight_invalidation;
    }
    	
    private transient boolean MSG_hilight_modified = false;
    
    public boolean isModifiedMSG_hilight() {
    	return this.MSG_hilight_modified;
    }
    	
    public void unModifiedMSG_hilight() {
    	this.MSG_hilight_modified = false;
    }
    public FieldProperty getMSG_hilightFieldProperty() {
    	return fieldPropertyMap.get("MSG_hilight");
    }
    
    public String getMSG_hilight() {
    	return MSG_hilight;
    }	
    public String getNvlMSG_hilight() {
    	if(getMSG_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_hilight();
    	}
    }
    public void setMSG_hilight(String MSG_hilight) {
    	if(MSG_hilight == null) {
    		this.MSG_hilight = null;
    	} else {
    		this.MSG_hilight = MSG_hilight;
    	}
    	this.MSG_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_outline
     * Comments    : 
     */	
    private String MSG_outline = null;
    
    private transient boolean MSG_outline_nullable = true;
    
    public boolean isNullableMSG_outline() {
    	return this.MSG_outline_nullable;
    }
    
    private transient boolean MSG_outline_invalidation = false;
    	
    public void setInvalidationMSG_outline(boolean invalidation) { 
    	this.MSG_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_outline() {
    	return this.MSG_outline_invalidation;
    }
    	
    private transient boolean MSG_outline_modified = false;
    
    public boolean isModifiedMSG_outline() {
    	return this.MSG_outline_modified;
    }
    	
    public void unModifiedMSG_outline() {
    	this.MSG_outline_modified = false;
    }
    public FieldProperty getMSG_outlineFieldProperty() {
    	return fieldPropertyMap.get("MSG_outline");
    }
    
    public String getMSG_outline() {
    	return MSG_outline;
    }	
    public String getNvlMSG_outline() {
    	if(getMSG_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_outline();
    	}
    }
    public void setMSG_outline(String MSG_outline) {
    	if(MSG_outline == null) {
    		this.MSG_outline = null;
    	} else {
    		this.MSG_outline = MSG_outline;
    	}
    	this.MSG_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_transp
     * Comments    : 
     */	
    private String MSG_transp = null;
    
    private transient boolean MSG_transp_nullable = true;
    
    public boolean isNullableMSG_transp() {
    	return this.MSG_transp_nullable;
    }
    
    private transient boolean MSG_transp_invalidation = false;
    	
    public void setInvalidationMSG_transp(boolean invalidation) { 
    	this.MSG_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_transp() {
    	return this.MSG_transp_invalidation;
    }
    	
    private transient boolean MSG_transp_modified = false;
    
    public boolean isModifiedMSG_transp() {
    	return this.MSG_transp_modified;
    }
    	
    public void unModifiedMSG_transp() {
    	this.MSG_transp_modified = false;
    }
    public FieldProperty getMSG_transpFieldProperty() {
    	return fieldPropertyMap.get("MSG_transp");
    }
    
    public String getMSG_transp() {
    	return MSG_transp;
    }	
    public String getNvlMSG_transp() {
    	if(getMSG_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_transp();
    	}
    }
    public void setMSG_transp(String MSG_transp) {
    	if(MSG_transp == null) {
    		this.MSG_transp = null;
    	} else {
    		this.MSG_transp = MSG_transp;
    	}
    	this.MSG_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_validn
     * Comments    : 
     */	
    private String MSG_validn = null;
    
    private transient boolean MSG_validn_nullable = true;
    
    public boolean isNullableMSG_validn() {
    	return this.MSG_validn_nullable;
    }
    
    private transient boolean MSG_validn_invalidation = false;
    	
    public void setInvalidationMSG_validn(boolean invalidation) { 
    	this.MSG_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_validn() {
    	return this.MSG_validn_invalidation;
    }
    	
    private transient boolean MSG_validn_modified = false;
    
    public boolean isModifiedMSG_validn() {
    	return this.MSG_validn_modified;
    }
    	
    public void unModifiedMSG_validn() {
    	this.MSG_validn_modified = false;
    }
    public FieldProperty getMSG_validnFieldProperty() {
    	return fieldPropertyMap.get("MSG_validn");
    }
    
    public String getMSG_validn() {
    	return MSG_validn;
    }	
    public String getNvlMSG_validn() {
    	if(getMSG_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_validn();
    	}
    }
    public void setMSG_validn(String MSG_validn) {
    	if(MSG_validn == null) {
    		this.MSG_validn = null;
    	} else {
    		this.MSG_validn = MSG_validn;
    	}
    	this.MSG_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_sosi
     * Comments    : 
     */	
    private String MSG_sosi = null;
    
    private transient boolean MSG_sosi_nullable = true;
    
    public boolean isNullableMSG_sosi() {
    	return this.MSG_sosi_nullable;
    }
    
    private transient boolean MSG_sosi_invalidation = false;
    	
    public void setInvalidationMSG_sosi(boolean invalidation) { 
    	this.MSG_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_sosi() {
    	return this.MSG_sosi_invalidation;
    }
    	
    private transient boolean MSG_sosi_modified = false;
    
    public boolean isModifiedMSG_sosi() {
    	return this.MSG_sosi_modified;
    }
    	
    public void unModifiedMSG_sosi() {
    	this.MSG_sosi_modified = false;
    }
    public FieldProperty getMSG_sosiFieldProperty() {
    	return fieldPropertyMap.get("MSG_sosi");
    }
    
    public String getMSG_sosi() {
    	return MSG_sosi;
    }	
    public String getNvlMSG_sosi() {
    	if(getMSG_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_sosi();
    	}
    }
    public void setMSG_sosi(String MSG_sosi) {
    	if(MSG_sosi == null) {
    		this.MSG_sosi = null;
    	} else {
    		this.MSG_sosi = MSG_sosi;
    	}
    	this.MSG_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_ps
     * Comments    : 
     */	
    private String MSG_ps = null;
    
    private transient boolean MSG_ps_nullable = true;
    
    public boolean isNullableMSG_ps() {
    	return this.MSG_ps_nullable;
    }
    
    private transient boolean MSG_ps_invalidation = false;
    	
    public void setInvalidationMSG_ps(boolean invalidation) { 
    	this.MSG_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_ps() {
    	return this.MSG_ps_invalidation;
    }
    	
    private transient boolean MSG_ps_modified = false;
    
    public boolean isModifiedMSG_ps() {
    	return this.MSG_ps_modified;
    }
    	
    public void unModifiedMSG_ps() {
    	this.MSG_ps_modified = false;
    }
    public FieldProperty getMSG_psFieldProperty() {
    	return fieldPropertyMap.get("MSG_ps");
    }
    
    public String getMSG_ps() {
    	return MSG_ps;
    }	
    public String getNvlMSG_ps() {
    	if(getMSG_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_ps();
    	}
    }
    public void setMSG_ps(String MSG_ps) {
    	if(MSG_ps == null) {
    		this.MSG_ps = null;
    	} else {
    		this.MSG_ps = MSG_ps;
    	}
    	this.MSG_ps_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_length_modified = false;
    	this.DATE_color_modified = false;
    	this.DATE_hilight_modified = false;
    	this.DATE_outline_modified = false;
    	this.DATE_transp_modified = false;
    	this.DATE_validn_modified = false;
    	this.DATE_sosi_modified = false;
    	this.DATE_ps_modified = false;
    	this.TERM_length_modified = false;
    	this.TERM_color_modified = false;
    	this.TERM_hilight_modified = false;
    	this.TERM_outline_modified = false;
    	this.TERM_transp_modified = false;
    	this.TERM_validn_modified = false;
    	this.TERM_sosi_modified = false;
    	this.TERM_ps_modified = false;
    	this.TIME_length_modified = false;
    	this.TIME_color_modified = false;
    	this.TIME_hilight_modified = false;
    	this.TIME_outline_modified = false;
    	this.TIME_transp_modified = false;
    	this.TIME_validn_modified = false;
    	this.TIME_sosi_modified = false;
    	this.TIME_ps_modified = false;
    	this.IDEN_length_modified = false;
    	this.IDEN_color_modified = false;
    	this.IDEN_hilight_modified = false;
    	this.IDEN_outline_modified = false;
    	this.IDEN_transp_modified = false;
    	this.IDEN_validn_modified = false;
    	this.IDEN_sosi_modified = false;
    	this.IDEN_ps_modified = false;
    	this.NAME_length_modified = false;
    	this.NAME_color_modified = false;
    	this.NAME_hilight_modified = false;
    	this.NAME_outline_modified = false;
    	this.NAME_transp_modified = false;
    	this.NAME_validn_modified = false;
    	this.NAME_sosi_modified = false;
    	this.NAME_ps_modified = false;
    	this.DEPT_length_modified = false;
    	this.DEPT_color_modified = false;
    	this.DEPT_hilight_modified = false;
    	this.DEPT_outline_modified = false;
    	this.DEPT_transp_modified = false;
    	this.DEPT_validn_modified = false;
    	this.DEPT_sosi_modified = false;
    	this.DEPT_ps_modified = false;
    	this.PHON_length_modified = false;
    	this.PHON_color_modified = false;
    	this.PHON_hilight_modified = false;
    	this.PHON_outline_modified = false;
    	this.PHON_transp_modified = false;
    	this.PHON_validn_modified = false;
    	this.PHON_sosi_modified = false;
    	this.PHON_ps_modified = false;
    	this.EMAL_length_modified = false;
    	this.EMAL_color_modified = false;
    	this.EMAL_hilight_modified = false;
    	this.EMAL_outline_modified = false;
    	this.EMAL_transp_modified = false;
    	this.EMAL_validn_modified = false;
    	this.EMAL_sosi_modified = false;
    	this.EMAL_ps_modified = false;
    	this.ADDR_length_modified = false;
    	this.ADDR_color_modified = false;
    	this.ADDR_hilight_modified = false;
    	this.ADDR_outline_modified = false;
    	this.ADDR_transp_modified = false;
    	this.ADDR_validn_modified = false;
    	this.ADDR_sosi_modified = false;
    	this.ADDR_ps_modified = false;
    	this.MSG_length_modified = false;
    	this.MSG_color_modified = false;
    	this.MSG_hilight_modified = false;
    	this.MSG_outline_modified = false;
    	this.MSG_transp_modified = false;
    	this.MSG_validn_modified = false;
    	this.MSG_sosi_modified = false;
    	this.MSG_ps_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_length_modified == true)
    		modifiedFields.add("DATE_length");
    	if(this.DATE_color_modified == true)
    		modifiedFields.add("DATE_color");
    	if(this.DATE_hilight_modified == true)
    		modifiedFields.add("DATE_hilight");
    	if(this.DATE_outline_modified == true)
    		modifiedFields.add("DATE_outline");
    	if(this.DATE_transp_modified == true)
    		modifiedFields.add("DATE_transp");
    	if(this.DATE_validn_modified == true)
    		modifiedFields.add("DATE_validn");
    	if(this.DATE_sosi_modified == true)
    		modifiedFields.add("DATE_sosi");
    	if(this.DATE_ps_modified == true)
    		modifiedFields.add("DATE_ps");
    	if(this.TERM_length_modified == true)
    		modifiedFields.add("TERM_length");
    	if(this.TERM_color_modified == true)
    		modifiedFields.add("TERM_color");
    	if(this.TERM_hilight_modified == true)
    		modifiedFields.add("TERM_hilight");
    	if(this.TERM_outline_modified == true)
    		modifiedFields.add("TERM_outline");
    	if(this.TERM_transp_modified == true)
    		modifiedFields.add("TERM_transp");
    	if(this.TERM_validn_modified == true)
    		modifiedFields.add("TERM_validn");
    	if(this.TERM_sosi_modified == true)
    		modifiedFields.add("TERM_sosi");
    	if(this.TERM_ps_modified == true)
    		modifiedFields.add("TERM_ps");
    	if(this.TIME_length_modified == true)
    		modifiedFields.add("TIME_length");
    	if(this.TIME_color_modified == true)
    		modifiedFields.add("TIME_color");
    	if(this.TIME_hilight_modified == true)
    		modifiedFields.add("TIME_hilight");
    	if(this.TIME_outline_modified == true)
    		modifiedFields.add("TIME_outline");
    	if(this.TIME_transp_modified == true)
    		modifiedFields.add("TIME_transp");
    	if(this.TIME_validn_modified == true)
    		modifiedFields.add("TIME_validn");
    	if(this.TIME_sosi_modified == true)
    		modifiedFields.add("TIME_sosi");
    	if(this.TIME_ps_modified == true)
    		modifiedFields.add("TIME_ps");
    	if(this.IDEN_length_modified == true)
    		modifiedFields.add("IDEN_length");
    	if(this.IDEN_color_modified == true)
    		modifiedFields.add("IDEN_color");
    	if(this.IDEN_hilight_modified == true)
    		modifiedFields.add("IDEN_hilight");
    	if(this.IDEN_outline_modified == true)
    		modifiedFields.add("IDEN_outline");
    	if(this.IDEN_transp_modified == true)
    		modifiedFields.add("IDEN_transp");
    	if(this.IDEN_validn_modified == true)
    		modifiedFields.add("IDEN_validn");
    	if(this.IDEN_sosi_modified == true)
    		modifiedFields.add("IDEN_sosi");
    	if(this.IDEN_ps_modified == true)
    		modifiedFields.add("IDEN_ps");
    	if(this.NAME_length_modified == true)
    		modifiedFields.add("NAME_length");
    	if(this.NAME_color_modified == true)
    		modifiedFields.add("NAME_color");
    	if(this.NAME_hilight_modified == true)
    		modifiedFields.add("NAME_hilight");
    	if(this.NAME_outline_modified == true)
    		modifiedFields.add("NAME_outline");
    	if(this.NAME_transp_modified == true)
    		modifiedFields.add("NAME_transp");
    	if(this.NAME_validn_modified == true)
    		modifiedFields.add("NAME_validn");
    	if(this.NAME_sosi_modified == true)
    		modifiedFields.add("NAME_sosi");
    	if(this.NAME_ps_modified == true)
    		modifiedFields.add("NAME_ps");
    	if(this.DEPT_length_modified == true)
    		modifiedFields.add("DEPT_length");
    	if(this.DEPT_color_modified == true)
    		modifiedFields.add("DEPT_color");
    	if(this.DEPT_hilight_modified == true)
    		modifiedFields.add("DEPT_hilight");
    	if(this.DEPT_outline_modified == true)
    		modifiedFields.add("DEPT_outline");
    	if(this.DEPT_transp_modified == true)
    		modifiedFields.add("DEPT_transp");
    	if(this.DEPT_validn_modified == true)
    		modifiedFields.add("DEPT_validn");
    	if(this.DEPT_sosi_modified == true)
    		modifiedFields.add("DEPT_sosi");
    	if(this.DEPT_ps_modified == true)
    		modifiedFields.add("DEPT_ps");
    	if(this.PHON_length_modified == true)
    		modifiedFields.add("PHON_length");
    	if(this.PHON_color_modified == true)
    		modifiedFields.add("PHON_color");
    	if(this.PHON_hilight_modified == true)
    		modifiedFields.add("PHON_hilight");
    	if(this.PHON_outline_modified == true)
    		modifiedFields.add("PHON_outline");
    	if(this.PHON_transp_modified == true)
    		modifiedFields.add("PHON_transp");
    	if(this.PHON_validn_modified == true)
    		modifiedFields.add("PHON_validn");
    	if(this.PHON_sosi_modified == true)
    		modifiedFields.add("PHON_sosi");
    	if(this.PHON_ps_modified == true)
    		modifiedFields.add("PHON_ps");
    	if(this.EMAL_length_modified == true)
    		modifiedFields.add("EMAL_length");
    	if(this.EMAL_color_modified == true)
    		modifiedFields.add("EMAL_color");
    	if(this.EMAL_hilight_modified == true)
    		modifiedFields.add("EMAL_hilight");
    	if(this.EMAL_outline_modified == true)
    		modifiedFields.add("EMAL_outline");
    	if(this.EMAL_transp_modified == true)
    		modifiedFields.add("EMAL_transp");
    	if(this.EMAL_validn_modified == true)
    		modifiedFields.add("EMAL_validn");
    	if(this.EMAL_sosi_modified == true)
    		modifiedFields.add("EMAL_sosi");
    	if(this.EMAL_ps_modified == true)
    		modifiedFields.add("EMAL_ps");
    	if(this.ADDR_length_modified == true)
    		modifiedFields.add("ADDR_length");
    	if(this.ADDR_color_modified == true)
    		modifiedFields.add("ADDR_color");
    	if(this.ADDR_hilight_modified == true)
    		modifiedFields.add("ADDR_hilight");
    	if(this.ADDR_outline_modified == true)
    		modifiedFields.add("ADDR_outline");
    	if(this.ADDR_transp_modified == true)
    		modifiedFields.add("ADDR_transp");
    	if(this.ADDR_validn_modified == true)
    		modifiedFields.add("ADDR_validn");
    	if(this.ADDR_sosi_modified == true)
    		modifiedFields.add("ADDR_sosi");
    	if(this.ADDR_ps_modified == true)
    		modifiedFields.add("ADDR_ps");
    	if(this.MSG_length_modified == true)
    		modifiedFields.add("MSG_length");
    	if(this.MSG_color_modified == true)
    		modifiedFields.add("MSG_color");
    	if(this.MSG_hilight_modified == true)
    		modifiedFields.add("MSG_hilight");
    	if(this.MSG_outline_modified == true)
    		modifiedFields.add("MSG_outline");
    	if(this.MSG_transp_modified == true)
    		modifiedFields.add("MSG_transp");
    	if(this.MSG_validn_modified == true)
    		modifiedFields.add("MSG_validn");
    	if(this.MSG_sosi_modified == true)
    		modifiedFields.add("MSG_sosi");
    	if(this.MSG_ps_modified == true)
    		modifiedFields.add("MSG_ps");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM05_OIVPM05_meta copyObj = new OIVPM05_OIVPM05_meta();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM05_OIVPM05_meta) {
    	if(this == _oIVPM05_OIVPM05_meta) return;
    	OIVPM05_OIVPM05_meta __oIVPM05_OIVPM05_meta = (OIVPM05_OIVPM05_meta) _oIVPM05_OIVPM05_meta;
    	this.setDATE_length(__oIVPM05_OIVPM05_meta.getDATE_length());
    	this.setDATE_color(__oIVPM05_OIVPM05_meta.getDATE_color());
    	this.setDATE_hilight(__oIVPM05_OIVPM05_meta.getDATE_hilight());
    	this.setDATE_outline(__oIVPM05_OIVPM05_meta.getDATE_outline());
    	this.setDATE_transp(__oIVPM05_OIVPM05_meta.getDATE_transp());
    	this.setDATE_validn(__oIVPM05_OIVPM05_meta.getDATE_validn());
    	this.setDATE_sosi(__oIVPM05_OIVPM05_meta.getDATE_sosi());
    	this.setDATE_ps(__oIVPM05_OIVPM05_meta.getDATE_ps());
    	this.setTERM_length(__oIVPM05_OIVPM05_meta.getTERM_length());
    	this.setTERM_color(__oIVPM05_OIVPM05_meta.getTERM_color());
    	this.setTERM_hilight(__oIVPM05_OIVPM05_meta.getTERM_hilight());
    	this.setTERM_outline(__oIVPM05_OIVPM05_meta.getTERM_outline());
    	this.setTERM_transp(__oIVPM05_OIVPM05_meta.getTERM_transp());
    	this.setTERM_validn(__oIVPM05_OIVPM05_meta.getTERM_validn());
    	this.setTERM_sosi(__oIVPM05_OIVPM05_meta.getTERM_sosi());
    	this.setTERM_ps(__oIVPM05_OIVPM05_meta.getTERM_ps());
    	this.setTIME_length(__oIVPM05_OIVPM05_meta.getTIME_length());
    	this.setTIME_color(__oIVPM05_OIVPM05_meta.getTIME_color());
    	this.setTIME_hilight(__oIVPM05_OIVPM05_meta.getTIME_hilight());
    	this.setTIME_outline(__oIVPM05_OIVPM05_meta.getTIME_outline());
    	this.setTIME_transp(__oIVPM05_OIVPM05_meta.getTIME_transp());
    	this.setTIME_validn(__oIVPM05_OIVPM05_meta.getTIME_validn());
    	this.setTIME_sosi(__oIVPM05_OIVPM05_meta.getTIME_sosi());
    	this.setTIME_ps(__oIVPM05_OIVPM05_meta.getTIME_ps());
    	this.setIDEN_length(__oIVPM05_OIVPM05_meta.getIDEN_length());
    	this.setIDEN_color(__oIVPM05_OIVPM05_meta.getIDEN_color());
    	this.setIDEN_hilight(__oIVPM05_OIVPM05_meta.getIDEN_hilight());
    	this.setIDEN_outline(__oIVPM05_OIVPM05_meta.getIDEN_outline());
    	this.setIDEN_transp(__oIVPM05_OIVPM05_meta.getIDEN_transp());
    	this.setIDEN_validn(__oIVPM05_OIVPM05_meta.getIDEN_validn());
    	this.setIDEN_sosi(__oIVPM05_OIVPM05_meta.getIDEN_sosi());
    	this.setIDEN_ps(__oIVPM05_OIVPM05_meta.getIDEN_ps());
    	this.setNAME_length(__oIVPM05_OIVPM05_meta.getNAME_length());
    	this.setNAME_color(__oIVPM05_OIVPM05_meta.getNAME_color());
    	this.setNAME_hilight(__oIVPM05_OIVPM05_meta.getNAME_hilight());
    	this.setNAME_outline(__oIVPM05_OIVPM05_meta.getNAME_outline());
    	this.setNAME_transp(__oIVPM05_OIVPM05_meta.getNAME_transp());
    	this.setNAME_validn(__oIVPM05_OIVPM05_meta.getNAME_validn());
    	this.setNAME_sosi(__oIVPM05_OIVPM05_meta.getNAME_sosi());
    	this.setNAME_ps(__oIVPM05_OIVPM05_meta.getNAME_ps());
    	this.setDEPT_length(__oIVPM05_OIVPM05_meta.getDEPT_length());
    	this.setDEPT_color(__oIVPM05_OIVPM05_meta.getDEPT_color());
    	this.setDEPT_hilight(__oIVPM05_OIVPM05_meta.getDEPT_hilight());
    	this.setDEPT_outline(__oIVPM05_OIVPM05_meta.getDEPT_outline());
    	this.setDEPT_transp(__oIVPM05_OIVPM05_meta.getDEPT_transp());
    	this.setDEPT_validn(__oIVPM05_OIVPM05_meta.getDEPT_validn());
    	this.setDEPT_sosi(__oIVPM05_OIVPM05_meta.getDEPT_sosi());
    	this.setDEPT_ps(__oIVPM05_OIVPM05_meta.getDEPT_ps());
    	this.setPHON_length(__oIVPM05_OIVPM05_meta.getPHON_length());
    	this.setPHON_color(__oIVPM05_OIVPM05_meta.getPHON_color());
    	this.setPHON_hilight(__oIVPM05_OIVPM05_meta.getPHON_hilight());
    	this.setPHON_outline(__oIVPM05_OIVPM05_meta.getPHON_outline());
    	this.setPHON_transp(__oIVPM05_OIVPM05_meta.getPHON_transp());
    	this.setPHON_validn(__oIVPM05_OIVPM05_meta.getPHON_validn());
    	this.setPHON_sosi(__oIVPM05_OIVPM05_meta.getPHON_sosi());
    	this.setPHON_ps(__oIVPM05_OIVPM05_meta.getPHON_ps());
    	this.setEMAL_length(__oIVPM05_OIVPM05_meta.getEMAL_length());
    	this.setEMAL_color(__oIVPM05_OIVPM05_meta.getEMAL_color());
    	this.setEMAL_hilight(__oIVPM05_OIVPM05_meta.getEMAL_hilight());
    	this.setEMAL_outline(__oIVPM05_OIVPM05_meta.getEMAL_outline());
    	this.setEMAL_transp(__oIVPM05_OIVPM05_meta.getEMAL_transp());
    	this.setEMAL_validn(__oIVPM05_OIVPM05_meta.getEMAL_validn());
    	this.setEMAL_sosi(__oIVPM05_OIVPM05_meta.getEMAL_sosi());
    	this.setEMAL_ps(__oIVPM05_OIVPM05_meta.getEMAL_ps());
    	this.setADDR_length(__oIVPM05_OIVPM05_meta.getADDR_length());
    	this.setADDR_color(__oIVPM05_OIVPM05_meta.getADDR_color());
    	this.setADDR_hilight(__oIVPM05_OIVPM05_meta.getADDR_hilight());
    	this.setADDR_outline(__oIVPM05_OIVPM05_meta.getADDR_outline());
    	this.setADDR_transp(__oIVPM05_OIVPM05_meta.getADDR_transp());
    	this.setADDR_validn(__oIVPM05_OIVPM05_meta.getADDR_validn());
    	this.setADDR_sosi(__oIVPM05_OIVPM05_meta.getADDR_sosi());
    	this.setADDR_ps(__oIVPM05_OIVPM05_meta.getADDR_ps());
    	this.setMSG_length(__oIVPM05_OIVPM05_meta.getMSG_length());
    	this.setMSG_color(__oIVPM05_OIVPM05_meta.getMSG_color());
    	this.setMSG_hilight(__oIVPM05_OIVPM05_meta.getMSG_hilight());
    	this.setMSG_outline(__oIVPM05_OIVPM05_meta.getMSG_outline());
    	this.setMSG_transp(__oIVPM05_OIVPM05_meta.getMSG_transp());
    	this.setMSG_validn(__oIVPM05_OIVPM05_meta.getMSG_validn());
    	this.setMSG_sosi(__oIVPM05_OIVPM05_meta.getMSG_sosi());
    	this.setMSG_ps(__oIVPM05_OIVPM05_meta.getMSG_ps());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE_length : ").append(DATE_length).append("\n");	
    	buffer.append("DATE_color : ").append(DATE_color).append("\n");	
    	buffer.append("DATE_hilight : ").append(DATE_hilight).append("\n");	
    	buffer.append("DATE_outline : ").append(DATE_outline).append("\n");	
    	buffer.append("DATE_transp : ").append(DATE_transp).append("\n");	
    	buffer.append("DATE_validn : ").append(DATE_validn).append("\n");	
    	buffer.append("DATE_sosi : ").append(DATE_sosi).append("\n");	
    	buffer.append("DATE_ps : ").append(DATE_ps).append("\n");	
    	buffer.append("TERM_length : ").append(TERM_length).append("\n");	
    	buffer.append("TERM_color : ").append(TERM_color).append("\n");	
    	buffer.append("TERM_hilight : ").append(TERM_hilight).append("\n");	
    	buffer.append("TERM_outline : ").append(TERM_outline).append("\n");	
    	buffer.append("TERM_transp : ").append(TERM_transp).append("\n");	
    	buffer.append("TERM_validn : ").append(TERM_validn).append("\n");	
    	buffer.append("TERM_sosi : ").append(TERM_sosi).append("\n");	
    	buffer.append("TERM_ps : ").append(TERM_ps).append("\n");	
    	buffer.append("TIME_length : ").append(TIME_length).append("\n");	
    	buffer.append("TIME_color : ").append(TIME_color).append("\n");	
    	buffer.append("TIME_hilight : ").append(TIME_hilight).append("\n");	
    	buffer.append("TIME_outline : ").append(TIME_outline).append("\n");	
    	buffer.append("TIME_transp : ").append(TIME_transp).append("\n");	
    	buffer.append("TIME_validn : ").append(TIME_validn).append("\n");	
    	buffer.append("TIME_sosi : ").append(TIME_sosi).append("\n");	
    	buffer.append("TIME_ps : ").append(TIME_ps).append("\n");	
    	buffer.append("IDEN_length : ").append(IDEN_length).append("\n");	
    	buffer.append("IDEN_color : ").append(IDEN_color).append("\n");	
    	buffer.append("IDEN_hilight : ").append(IDEN_hilight).append("\n");	
    	buffer.append("IDEN_outline : ").append(IDEN_outline).append("\n");	
    	buffer.append("IDEN_transp : ").append(IDEN_transp).append("\n");	
    	buffer.append("IDEN_validn : ").append(IDEN_validn).append("\n");	
    	buffer.append("IDEN_sosi : ").append(IDEN_sosi).append("\n");	
    	buffer.append("IDEN_ps : ").append(IDEN_ps).append("\n");	
    	buffer.append("NAME_length : ").append(NAME_length).append("\n");	
    	buffer.append("NAME_color : ").append(NAME_color).append("\n");	
    	buffer.append("NAME_hilight : ").append(NAME_hilight).append("\n");	
    	buffer.append("NAME_outline : ").append(NAME_outline).append("\n");	
    	buffer.append("NAME_transp : ").append(NAME_transp).append("\n");	
    	buffer.append("NAME_validn : ").append(NAME_validn).append("\n");	
    	buffer.append("NAME_sosi : ").append(NAME_sosi).append("\n");	
    	buffer.append("NAME_ps : ").append(NAME_ps).append("\n");	
    	buffer.append("DEPT_length : ").append(DEPT_length).append("\n");	
    	buffer.append("DEPT_color : ").append(DEPT_color).append("\n");	
    	buffer.append("DEPT_hilight : ").append(DEPT_hilight).append("\n");	
    	buffer.append("DEPT_outline : ").append(DEPT_outline).append("\n");	
    	buffer.append("DEPT_transp : ").append(DEPT_transp).append("\n");	
    	buffer.append("DEPT_validn : ").append(DEPT_validn).append("\n");	
    	buffer.append("DEPT_sosi : ").append(DEPT_sosi).append("\n");	
    	buffer.append("DEPT_ps : ").append(DEPT_ps).append("\n");	
    	buffer.append("PHON_length : ").append(PHON_length).append("\n");	
    	buffer.append("PHON_color : ").append(PHON_color).append("\n");	
    	buffer.append("PHON_hilight : ").append(PHON_hilight).append("\n");	
    	buffer.append("PHON_outline : ").append(PHON_outline).append("\n");	
    	buffer.append("PHON_transp : ").append(PHON_transp).append("\n");	
    	buffer.append("PHON_validn : ").append(PHON_validn).append("\n");	
    	buffer.append("PHON_sosi : ").append(PHON_sosi).append("\n");	
    	buffer.append("PHON_ps : ").append(PHON_ps).append("\n");	
    	buffer.append("EMAL_length : ").append(EMAL_length).append("\n");	
    	buffer.append("EMAL_color : ").append(EMAL_color).append("\n");	
    	buffer.append("EMAL_hilight : ").append(EMAL_hilight).append("\n");	
    	buffer.append("EMAL_outline : ").append(EMAL_outline).append("\n");	
    	buffer.append("EMAL_transp : ").append(EMAL_transp).append("\n");	
    	buffer.append("EMAL_validn : ").append(EMAL_validn).append("\n");	
    	buffer.append("EMAL_sosi : ").append(EMAL_sosi).append("\n");	
    	buffer.append("EMAL_ps : ").append(EMAL_ps).append("\n");	
    	buffer.append("ADDR_length : ").append(ADDR_length).append("\n");	
    	buffer.append("ADDR_color : ").append(ADDR_color).append("\n");	
    	buffer.append("ADDR_hilight : ").append(ADDR_hilight).append("\n");	
    	buffer.append("ADDR_outline : ").append(ADDR_outline).append("\n");	
    	buffer.append("ADDR_transp : ").append(ADDR_transp).append("\n");	
    	buffer.append("ADDR_validn : ").append(ADDR_validn).append("\n");	
    	buffer.append("ADDR_sosi : ").append(ADDR_sosi).append("\n");	
    	buffer.append("ADDR_ps : ").append(ADDR_ps).append("\n");	
    	buffer.append("MSG_length : ").append(MSG_length).append("\n");	
    	buffer.append("MSG_color : ").append(MSG_color).append("\n");	
    	buffer.append("MSG_hilight : ").append(MSG_hilight).append("\n");	
    	buffer.append("MSG_outline : ").append(MSG_outline).append("\n");	
    	buffer.append("MSG_transp : ").append(MSG_transp).append("\n");	
    	buffer.append("MSG_validn : ").append(MSG_validn).append("\n");	
    	buffer.append("MSG_sosi : ").append(MSG_sosi).append("\n");	
    	buffer.append("MSG_ps : ").append(MSG_ps).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(80);
    	fieldPropertyMap.put("DATE_length", FieldProperty.builder()
    	              .setPhysicalName("DATE_length")
    	              .setLogicalName("DATE_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DATE_color", FieldProperty.builder()
    	              .setPhysicalName("DATE_color")
    	              .setLogicalName("DATE_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_hilight", FieldProperty.builder()
    	              .setPhysicalName("DATE_hilight")
    	              .setLogicalName("DATE_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_outline", FieldProperty.builder()
    	              .setPhysicalName("DATE_outline")
    	              .setLogicalName("DATE_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_transp", FieldProperty.builder()
    	              .setPhysicalName("DATE_transp")
    	              .setLogicalName("DATE_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_validn", FieldProperty.builder()
    	              .setPhysicalName("DATE_validn")
    	              .setLogicalName("DATE_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_sosi", FieldProperty.builder()
    	              .setPhysicalName("DATE_sosi")
    	              .setLogicalName("DATE_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_ps", FieldProperty.builder()
    	              .setPhysicalName("DATE_ps")
    	              .setLogicalName("DATE_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_length", FieldProperty.builder()
    	              .setPhysicalName("TERM_length")
    	              .setLogicalName("TERM_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TERM_color", FieldProperty.builder()
    	              .setPhysicalName("TERM_color")
    	              .setLogicalName("TERM_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_hilight", FieldProperty.builder()
    	              .setPhysicalName("TERM_hilight")
    	              .setLogicalName("TERM_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_outline", FieldProperty.builder()
    	              .setPhysicalName("TERM_outline")
    	              .setLogicalName("TERM_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_transp", FieldProperty.builder()
    	              .setPhysicalName("TERM_transp")
    	              .setLogicalName("TERM_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_validn", FieldProperty.builder()
    	              .setPhysicalName("TERM_validn")
    	              .setLogicalName("TERM_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_sosi", FieldProperty.builder()
    	              .setPhysicalName("TERM_sosi")
    	              .setLogicalName("TERM_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_ps", FieldProperty.builder()
    	              .setPhysicalName("TERM_ps")
    	              .setLogicalName("TERM_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_length", FieldProperty.builder()
    	              .setPhysicalName("TIME_length")
    	              .setLogicalName("TIME_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TIME_color", FieldProperty.builder()
    	              .setPhysicalName("TIME_color")
    	              .setLogicalName("TIME_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_hilight", FieldProperty.builder()
    	              .setPhysicalName("TIME_hilight")
    	              .setLogicalName("TIME_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_outline", FieldProperty.builder()
    	              .setPhysicalName("TIME_outline")
    	              .setLogicalName("TIME_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_transp", FieldProperty.builder()
    	              .setPhysicalName("TIME_transp")
    	              .setLogicalName("TIME_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_validn", FieldProperty.builder()
    	              .setPhysicalName("TIME_validn")
    	              .setLogicalName("TIME_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_sosi", FieldProperty.builder()
    	              .setPhysicalName("TIME_sosi")
    	              .setLogicalName("TIME_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_ps", FieldProperty.builder()
    	              .setPhysicalName("TIME_ps")
    	              .setLogicalName("TIME_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN_length")
    	              .setLogicalName("IDEN_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN_color")
    	              .setLogicalName("IDEN_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN_hilight")
    	              .setLogicalName("IDEN_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN_outline")
    	              .setLogicalName("IDEN_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN_transp")
    	              .setLogicalName("IDEN_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN_validn")
    	              .setLogicalName("IDEN_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN_sosi")
    	              .setLogicalName("IDEN_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN_ps")
    	              .setLogicalName("IDEN_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_length", FieldProperty.builder()
    	              .setPhysicalName("NAME_length")
    	              .setLogicalName("NAME_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("NAME_color", FieldProperty.builder()
    	              .setPhysicalName("NAME_color")
    	              .setLogicalName("NAME_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_hilight", FieldProperty.builder()
    	              .setPhysicalName("NAME_hilight")
    	              .setLogicalName("NAME_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_outline", FieldProperty.builder()
    	              .setPhysicalName("NAME_outline")
    	              .setLogicalName("NAME_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_transp", FieldProperty.builder()
    	              .setPhysicalName("NAME_transp")
    	              .setLogicalName("NAME_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_validn", FieldProperty.builder()
    	              .setPhysicalName("NAME_validn")
    	              .setLogicalName("NAME_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_sosi", FieldProperty.builder()
    	              .setPhysicalName("NAME_sosi")
    	              .setLogicalName("NAME_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME_ps", FieldProperty.builder()
    	              .setPhysicalName("NAME_ps")
    	              .setLogicalName("NAME_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_length", FieldProperty.builder()
    	              .setPhysicalName("DEPT_length")
    	              .setLogicalName("DEPT_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DEPT_color", FieldProperty.builder()
    	              .setPhysicalName("DEPT_color")
    	              .setLogicalName("DEPT_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_hilight", FieldProperty.builder()
    	              .setPhysicalName("DEPT_hilight")
    	              .setLogicalName("DEPT_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_outline", FieldProperty.builder()
    	              .setPhysicalName("DEPT_outline")
    	              .setLogicalName("DEPT_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_transp", FieldProperty.builder()
    	              .setPhysicalName("DEPT_transp")
    	              .setLogicalName("DEPT_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_validn", FieldProperty.builder()
    	              .setPhysicalName("DEPT_validn")
    	              .setLogicalName("DEPT_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_sosi", FieldProperty.builder()
    	              .setPhysicalName("DEPT_sosi")
    	              .setLogicalName("DEPT_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT_ps", FieldProperty.builder()
    	              .setPhysicalName("DEPT_ps")
    	              .setLogicalName("DEPT_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_length", FieldProperty.builder()
    	              .setPhysicalName("PHON_length")
    	              .setLogicalName("PHON_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("PHON_color", FieldProperty.builder()
    	              .setPhysicalName("PHON_color")
    	              .setLogicalName("PHON_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_hilight", FieldProperty.builder()
    	              .setPhysicalName("PHON_hilight")
    	              .setLogicalName("PHON_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_outline", FieldProperty.builder()
    	              .setPhysicalName("PHON_outline")
    	              .setLogicalName("PHON_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_transp", FieldProperty.builder()
    	              .setPhysicalName("PHON_transp")
    	              .setLogicalName("PHON_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_validn", FieldProperty.builder()
    	              .setPhysicalName("PHON_validn")
    	              .setLogicalName("PHON_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_sosi", FieldProperty.builder()
    	              .setPhysicalName("PHON_sosi")
    	              .setLogicalName("PHON_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON_ps", FieldProperty.builder()
    	              .setPhysicalName("PHON_ps")
    	              .setLogicalName("PHON_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_length", FieldProperty.builder()
    	              .setPhysicalName("EMAL_length")
    	              .setLogicalName("EMAL_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("EMAL_color", FieldProperty.builder()
    	              .setPhysicalName("EMAL_color")
    	              .setLogicalName("EMAL_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_hilight", FieldProperty.builder()
    	              .setPhysicalName("EMAL_hilight")
    	              .setLogicalName("EMAL_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_outline", FieldProperty.builder()
    	              .setPhysicalName("EMAL_outline")
    	              .setLogicalName("EMAL_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_transp", FieldProperty.builder()
    	              .setPhysicalName("EMAL_transp")
    	              .setLogicalName("EMAL_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_validn", FieldProperty.builder()
    	              .setPhysicalName("EMAL_validn")
    	              .setLogicalName("EMAL_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_sosi", FieldProperty.builder()
    	              .setPhysicalName("EMAL_sosi")
    	              .setLogicalName("EMAL_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL_ps", FieldProperty.builder()
    	              .setPhysicalName("EMAL_ps")
    	              .setLogicalName("EMAL_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_length", FieldProperty.builder()
    	              .setPhysicalName("ADDR_length")
    	              .setLogicalName("ADDR_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("ADDR_color", FieldProperty.builder()
    	              .setPhysicalName("ADDR_color")
    	              .setLogicalName("ADDR_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_hilight", FieldProperty.builder()
    	              .setPhysicalName("ADDR_hilight")
    	              .setLogicalName("ADDR_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_outline", FieldProperty.builder()
    	              .setPhysicalName("ADDR_outline")
    	              .setLogicalName("ADDR_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_transp", FieldProperty.builder()
    	              .setPhysicalName("ADDR_transp")
    	              .setLogicalName("ADDR_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_validn", FieldProperty.builder()
    	              .setPhysicalName("ADDR_validn")
    	              .setLogicalName("ADDR_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_sosi", FieldProperty.builder()
    	              .setPhysicalName("ADDR_sosi")
    	              .setLogicalName("ADDR_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR_ps", FieldProperty.builder()
    	              .setPhysicalName("ADDR_ps")
    	              .setLogicalName("ADDR_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_length", FieldProperty.builder()
    	              .setPhysicalName("MSG_length")
    	              .setLogicalName("MSG_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("MSG_color", FieldProperty.builder()
    	              .setPhysicalName("MSG_color")
    	              .setLogicalName("MSG_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_hilight", FieldProperty.builder()
    	              .setPhysicalName("MSG_hilight")
    	              .setLogicalName("MSG_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_outline", FieldProperty.builder()
    	              .setPhysicalName("MSG_outline")
    	              .setLogicalName("MSG_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_transp", FieldProperty.builder()
    	              .setPhysicalName("MSG_transp")
    	              .setLogicalName("MSG_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_validn", FieldProperty.builder()
    	              .setPhysicalName("MSG_validn")
    	              .setLogicalName("MSG_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_sosi", FieldProperty.builder()
    	              .setPhysicalName("MSG_sosi")
    	              .setLogicalName("MSG_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_ps", FieldProperty.builder()
    	              .setPhysicalName("MSG_ps")
    	              .setLogicalName("MSG_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : return getDATE_length();
    		case "DATE_color" : return getDATE_color();
    		case "DATE_hilight" : return getDATE_hilight();
    		case "DATE_outline" : return getDATE_outline();
    		case "DATE_transp" : return getDATE_transp();
    		case "DATE_validn" : return getDATE_validn();
    		case "DATE_sosi" : return getDATE_sosi();
    		case "DATE_ps" : return getDATE_ps();
    		case "TERM_length" : return getTERM_length();
    		case "TERM_color" : return getTERM_color();
    		case "TERM_hilight" : return getTERM_hilight();
    		case "TERM_outline" : return getTERM_outline();
    		case "TERM_transp" : return getTERM_transp();
    		case "TERM_validn" : return getTERM_validn();
    		case "TERM_sosi" : return getTERM_sosi();
    		case "TERM_ps" : return getTERM_ps();
    		case "TIME_length" : return getTIME_length();
    		case "TIME_color" : return getTIME_color();
    		case "TIME_hilight" : return getTIME_hilight();
    		case "TIME_outline" : return getTIME_outline();
    		case "TIME_transp" : return getTIME_transp();
    		case "TIME_validn" : return getTIME_validn();
    		case "TIME_sosi" : return getTIME_sosi();
    		case "TIME_ps" : return getTIME_ps();
    		case "IDEN_length" : return getIDEN_length();
    		case "IDEN_color" : return getIDEN_color();
    		case "IDEN_hilight" : return getIDEN_hilight();
    		case "IDEN_outline" : return getIDEN_outline();
    		case "IDEN_transp" : return getIDEN_transp();
    		case "IDEN_validn" : return getIDEN_validn();
    		case "IDEN_sosi" : return getIDEN_sosi();
    		case "IDEN_ps" : return getIDEN_ps();
    		case "NAME_length" : return getNAME_length();
    		case "NAME_color" : return getNAME_color();
    		case "NAME_hilight" : return getNAME_hilight();
    		case "NAME_outline" : return getNAME_outline();
    		case "NAME_transp" : return getNAME_transp();
    		case "NAME_validn" : return getNAME_validn();
    		case "NAME_sosi" : return getNAME_sosi();
    		case "NAME_ps" : return getNAME_ps();
    		case "DEPT_length" : return getDEPT_length();
    		case "DEPT_color" : return getDEPT_color();
    		case "DEPT_hilight" : return getDEPT_hilight();
    		case "DEPT_outline" : return getDEPT_outline();
    		case "DEPT_transp" : return getDEPT_transp();
    		case "DEPT_validn" : return getDEPT_validn();
    		case "DEPT_sosi" : return getDEPT_sosi();
    		case "DEPT_ps" : return getDEPT_ps();
    		case "PHON_length" : return getPHON_length();
    		case "PHON_color" : return getPHON_color();
    		case "PHON_hilight" : return getPHON_hilight();
    		case "PHON_outline" : return getPHON_outline();
    		case "PHON_transp" : return getPHON_transp();
    		case "PHON_validn" : return getPHON_validn();
    		case "PHON_sosi" : return getPHON_sosi();
    		case "PHON_ps" : return getPHON_ps();
    		case "EMAL_length" : return getEMAL_length();
    		case "EMAL_color" : return getEMAL_color();
    		case "EMAL_hilight" : return getEMAL_hilight();
    		case "EMAL_outline" : return getEMAL_outline();
    		case "EMAL_transp" : return getEMAL_transp();
    		case "EMAL_validn" : return getEMAL_validn();
    		case "EMAL_sosi" : return getEMAL_sosi();
    		case "EMAL_ps" : return getEMAL_ps();
    		case "ADDR_length" : return getADDR_length();
    		case "ADDR_color" : return getADDR_color();
    		case "ADDR_hilight" : return getADDR_hilight();
    		case "ADDR_outline" : return getADDR_outline();
    		case "ADDR_transp" : return getADDR_transp();
    		case "ADDR_validn" : return getADDR_validn();
    		case "ADDR_sosi" : return getADDR_sosi();
    		case "ADDR_ps" : return getADDR_ps();
    		case "MSG_length" : return getMSG_length();
    		case "MSG_color" : return getMSG_color();
    		case "MSG_hilight" : return getMSG_hilight();
    		case "MSG_outline" : return getMSG_outline();
    		case "MSG_transp" : return getMSG_transp();
    		case "MSG_validn" : return getMSG_validn();
    		case "MSG_sosi" : return getMSG_sosi();
    		case "MSG_ps" : return getMSG_ps();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : setDATE_length((Integer)arg);break;
    		case "DATE_color" : setDATE_color((String)arg); break;
    		case "DATE_hilight" : setDATE_hilight((String)arg); break;
    		case "DATE_outline" : setDATE_outline((String)arg); break;
    		case "DATE_transp" : setDATE_transp((String)arg); break;
    		case "DATE_validn" : setDATE_validn((String)arg); break;
    		case "DATE_sosi" : setDATE_sosi((String)arg); break;
    		case "DATE_ps" : setDATE_ps((String)arg); break;
    		case "TERM_length" : setTERM_length((Integer)arg);break;
    		case "TERM_color" : setTERM_color((String)arg); break;
    		case "TERM_hilight" : setTERM_hilight((String)arg); break;
    		case "TERM_outline" : setTERM_outline((String)arg); break;
    		case "TERM_transp" : setTERM_transp((String)arg); break;
    		case "TERM_validn" : setTERM_validn((String)arg); break;
    		case "TERM_sosi" : setTERM_sosi((String)arg); break;
    		case "TERM_ps" : setTERM_ps((String)arg); break;
    		case "TIME_length" : setTIME_length((Integer)arg);break;
    		case "TIME_color" : setTIME_color((String)arg); break;
    		case "TIME_hilight" : setTIME_hilight((String)arg); break;
    		case "TIME_outline" : setTIME_outline((String)arg); break;
    		case "TIME_transp" : setTIME_transp((String)arg); break;
    		case "TIME_validn" : setTIME_validn((String)arg); break;
    		case "TIME_sosi" : setTIME_sosi((String)arg); break;
    		case "TIME_ps" : setTIME_ps((String)arg); break;
    		case "IDEN_length" : setIDEN_length((Integer)arg);break;
    		case "IDEN_color" : setIDEN_color((String)arg); break;
    		case "IDEN_hilight" : setIDEN_hilight((String)arg); break;
    		case "IDEN_outline" : setIDEN_outline((String)arg); break;
    		case "IDEN_transp" : setIDEN_transp((String)arg); break;
    		case "IDEN_validn" : setIDEN_validn((String)arg); break;
    		case "IDEN_sosi" : setIDEN_sosi((String)arg); break;
    		case "IDEN_ps" : setIDEN_ps((String)arg); break;
    		case "NAME_length" : setNAME_length((Integer)arg);break;
    		case "NAME_color" : setNAME_color((String)arg); break;
    		case "NAME_hilight" : setNAME_hilight((String)arg); break;
    		case "NAME_outline" : setNAME_outline((String)arg); break;
    		case "NAME_transp" : setNAME_transp((String)arg); break;
    		case "NAME_validn" : setNAME_validn((String)arg); break;
    		case "NAME_sosi" : setNAME_sosi((String)arg); break;
    		case "NAME_ps" : setNAME_ps((String)arg); break;
    		case "DEPT_length" : setDEPT_length((Integer)arg);break;
    		case "DEPT_color" : setDEPT_color((String)arg); break;
    		case "DEPT_hilight" : setDEPT_hilight((String)arg); break;
    		case "DEPT_outline" : setDEPT_outline((String)arg); break;
    		case "DEPT_transp" : setDEPT_transp((String)arg); break;
    		case "DEPT_validn" : setDEPT_validn((String)arg); break;
    		case "DEPT_sosi" : setDEPT_sosi((String)arg); break;
    		case "DEPT_ps" : setDEPT_ps((String)arg); break;
    		case "PHON_length" : setPHON_length((Integer)arg);break;
    		case "PHON_color" : setPHON_color((String)arg); break;
    		case "PHON_hilight" : setPHON_hilight((String)arg); break;
    		case "PHON_outline" : setPHON_outline((String)arg); break;
    		case "PHON_transp" : setPHON_transp((String)arg); break;
    		case "PHON_validn" : setPHON_validn((String)arg); break;
    		case "PHON_sosi" : setPHON_sosi((String)arg); break;
    		case "PHON_ps" : setPHON_ps((String)arg); break;
    		case "EMAL_length" : setEMAL_length((Integer)arg);break;
    		case "EMAL_color" : setEMAL_color((String)arg); break;
    		case "EMAL_hilight" : setEMAL_hilight((String)arg); break;
    		case "EMAL_outline" : setEMAL_outline((String)arg); break;
    		case "EMAL_transp" : setEMAL_transp((String)arg); break;
    		case "EMAL_validn" : setEMAL_validn((String)arg); break;
    		case "EMAL_sosi" : setEMAL_sosi((String)arg); break;
    		case "EMAL_ps" : setEMAL_ps((String)arg); break;
    		case "ADDR_length" : setADDR_length((Integer)arg);break;
    		case "ADDR_color" : setADDR_color((String)arg); break;
    		case "ADDR_hilight" : setADDR_hilight((String)arg); break;
    		case "ADDR_outline" : setADDR_outline((String)arg); break;
    		case "ADDR_transp" : setADDR_transp((String)arg); break;
    		case "ADDR_validn" : setADDR_validn((String)arg); break;
    		case "ADDR_sosi" : setADDR_sosi((String)arg); break;
    		case "ADDR_ps" : setADDR_ps((String)arg); break;
    		case "MSG_length" : setMSG_length((Integer)arg);break;
    		case "MSG_color" : setMSG_color((String)arg); break;
    		case "MSG_hilight" : setMSG_hilight((String)arg); break;
    		case "MSG_outline" : setMSG_outline((String)arg); break;
    		case "MSG_transp" : setMSG_transp((String)arg); break;
    		case "MSG_validn" : setMSG_validn((String)arg); break;
    		case "MSG_sosi" : setMSG_sosi((String)arg); break;
    		case "MSG_ps" : setMSG_ps((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM05_OIVPM05_meta _OIVPM05_OIVPM05_meta = (OIVPM05_OIVPM05_meta) obj;
    	if(this.DATE_length != _OIVPM05_OIVPM05_meta.getDATE_length()) return false;
    	if(this.DATE_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_color() != null)
    		return false;
    	} else if(!this.DATE_color.equals(_OIVPM05_OIVPM05_meta.getDATE_color()))
    		return false;
    	if(this.DATE_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_hilight() != null)
    		return false;
    	} else if(!this.DATE_hilight.equals(_OIVPM05_OIVPM05_meta.getDATE_hilight()))
    		return false;
    	if(this.DATE_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_outline() != null)
    		return false;
    	} else if(!this.DATE_outline.equals(_OIVPM05_OIVPM05_meta.getDATE_outline()))
    		return false;
    	if(this.DATE_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_transp() != null)
    		return false;
    	} else if(!this.DATE_transp.equals(_OIVPM05_OIVPM05_meta.getDATE_transp()))
    		return false;
    	if(this.DATE_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_validn() != null)
    		return false;
    	} else if(!this.DATE_validn.equals(_OIVPM05_OIVPM05_meta.getDATE_validn()))
    		return false;
    	if(this.DATE_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_sosi() != null)
    		return false;
    	} else if(!this.DATE_sosi.equals(_OIVPM05_OIVPM05_meta.getDATE_sosi()))
    		return false;
    	if(this.DATE_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getDATE_ps() != null)
    		return false;
    	} else if(!this.DATE_ps.equals(_OIVPM05_OIVPM05_meta.getDATE_ps()))
    		return false;
    	if(this.TERM_length != _OIVPM05_OIVPM05_meta.getTERM_length()) return false;
    	if(this.TERM_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_color() != null)
    		return false;
    	} else if(!this.TERM_color.equals(_OIVPM05_OIVPM05_meta.getTERM_color()))
    		return false;
    	if(this.TERM_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_hilight() != null)
    		return false;
    	} else if(!this.TERM_hilight.equals(_OIVPM05_OIVPM05_meta.getTERM_hilight()))
    		return false;
    	if(this.TERM_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_outline() != null)
    		return false;
    	} else if(!this.TERM_outline.equals(_OIVPM05_OIVPM05_meta.getTERM_outline()))
    		return false;
    	if(this.TERM_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_transp() != null)
    		return false;
    	} else if(!this.TERM_transp.equals(_OIVPM05_OIVPM05_meta.getTERM_transp()))
    		return false;
    	if(this.TERM_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_validn() != null)
    		return false;
    	} else if(!this.TERM_validn.equals(_OIVPM05_OIVPM05_meta.getTERM_validn()))
    		return false;
    	if(this.TERM_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_sosi() != null)
    		return false;
    	} else if(!this.TERM_sosi.equals(_OIVPM05_OIVPM05_meta.getTERM_sosi()))
    		return false;
    	if(this.TERM_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getTERM_ps() != null)
    		return false;
    	} else if(!this.TERM_ps.equals(_OIVPM05_OIVPM05_meta.getTERM_ps()))
    		return false;
    	if(this.TIME_length != _OIVPM05_OIVPM05_meta.getTIME_length()) return false;
    	if(this.TIME_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_color() != null)
    		return false;
    	} else if(!this.TIME_color.equals(_OIVPM05_OIVPM05_meta.getTIME_color()))
    		return false;
    	if(this.TIME_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_hilight() != null)
    		return false;
    	} else if(!this.TIME_hilight.equals(_OIVPM05_OIVPM05_meta.getTIME_hilight()))
    		return false;
    	if(this.TIME_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_outline() != null)
    		return false;
    	} else if(!this.TIME_outline.equals(_OIVPM05_OIVPM05_meta.getTIME_outline()))
    		return false;
    	if(this.TIME_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_transp() != null)
    		return false;
    	} else if(!this.TIME_transp.equals(_OIVPM05_OIVPM05_meta.getTIME_transp()))
    		return false;
    	if(this.TIME_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_validn() != null)
    		return false;
    	} else if(!this.TIME_validn.equals(_OIVPM05_OIVPM05_meta.getTIME_validn()))
    		return false;
    	if(this.TIME_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_sosi() != null)
    		return false;
    	} else if(!this.TIME_sosi.equals(_OIVPM05_OIVPM05_meta.getTIME_sosi()))
    		return false;
    	if(this.TIME_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getTIME_ps() != null)
    		return false;
    	} else if(!this.TIME_ps.equals(_OIVPM05_OIVPM05_meta.getTIME_ps()))
    		return false;
    	if(this.IDEN_length != _OIVPM05_OIVPM05_meta.getIDEN_length()) return false;
    	if(this.IDEN_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_color() != null)
    		return false;
    	} else if(!this.IDEN_color.equals(_OIVPM05_OIVPM05_meta.getIDEN_color()))
    		return false;
    	if(this.IDEN_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_hilight() != null)
    		return false;
    	} else if(!this.IDEN_hilight.equals(_OIVPM05_OIVPM05_meta.getIDEN_hilight()))
    		return false;
    	if(this.IDEN_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_outline() != null)
    		return false;
    	} else if(!this.IDEN_outline.equals(_OIVPM05_OIVPM05_meta.getIDEN_outline()))
    		return false;
    	if(this.IDEN_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_transp() != null)
    		return false;
    	} else if(!this.IDEN_transp.equals(_OIVPM05_OIVPM05_meta.getIDEN_transp()))
    		return false;
    	if(this.IDEN_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_validn() != null)
    		return false;
    	} else if(!this.IDEN_validn.equals(_OIVPM05_OIVPM05_meta.getIDEN_validn()))
    		return false;
    	if(this.IDEN_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_sosi() != null)
    		return false;
    	} else if(!this.IDEN_sosi.equals(_OIVPM05_OIVPM05_meta.getIDEN_sosi()))
    		return false;
    	if(this.IDEN_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getIDEN_ps() != null)
    		return false;
    	} else if(!this.IDEN_ps.equals(_OIVPM05_OIVPM05_meta.getIDEN_ps()))
    		return false;
    	if(this.NAME_length != _OIVPM05_OIVPM05_meta.getNAME_length()) return false;
    	if(this.NAME_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_color() != null)
    		return false;
    	} else if(!this.NAME_color.equals(_OIVPM05_OIVPM05_meta.getNAME_color()))
    		return false;
    	if(this.NAME_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_hilight() != null)
    		return false;
    	} else if(!this.NAME_hilight.equals(_OIVPM05_OIVPM05_meta.getNAME_hilight()))
    		return false;
    	if(this.NAME_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_outline() != null)
    		return false;
    	} else if(!this.NAME_outline.equals(_OIVPM05_OIVPM05_meta.getNAME_outline()))
    		return false;
    	if(this.NAME_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_transp() != null)
    		return false;
    	} else if(!this.NAME_transp.equals(_OIVPM05_OIVPM05_meta.getNAME_transp()))
    		return false;
    	if(this.NAME_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_validn() != null)
    		return false;
    	} else if(!this.NAME_validn.equals(_OIVPM05_OIVPM05_meta.getNAME_validn()))
    		return false;
    	if(this.NAME_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_sosi() != null)
    		return false;
    	} else if(!this.NAME_sosi.equals(_OIVPM05_OIVPM05_meta.getNAME_sosi()))
    		return false;
    	if(this.NAME_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getNAME_ps() != null)
    		return false;
    	} else if(!this.NAME_ps.equals(_OIVPM05_OIVPM05_meta.getNAME_ps()))
    		return false;
    	if(this.DEPT_length != _OIVPM05_OIVPM05_meta.getDEPT_length()) return false;
    	if(this.DEPT_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_color() != null)
    		return false;
    	} else if(!this.DEPT_color.equals(_OIVPM05_OIVPM05_meta.getDEPT_color()))
    		return false;
    	if(this.DEPT_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_hilight() != null)
    		return false;
    	} else if(!this.DEPT_hilight.equals(_OIVPM05_OIVPM05_meta.getDEPT_hilight()))
    		return false;
    	if(this.DEPT_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_outline() != null)
    		return false;
    	} else if(!this.DEPT_outline.equals(_OIVPM05_OIVPM05_meta.getDEPT_outline()))
    		return false;
    	if(this.DEPT_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_transp() != null)
    		return false;
    	} else if(!this.DEPT_transp.equals(_OIVPM05_OIVPM05_meta.getDEPT_transp()))
    		return false;
    	if(this.DEPT_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_validn() != null)
    		return false;
    	} else if(!this.DEPT_validn.equals(_OIVPM05_OIVPM05_meta.getDEPT_validn()))
    		return false;
    	if(this.DEPT_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_sosi() != null)
    		return false;
    	} else if(!this.DEPT_sosi.equals(_OIVPM05_OIVPM05_meta.getDEPT_sosi()))
    		return false;
    	if(this.DEPT_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getDEPT_ps() != null)
    		return false;
    	} else if(!this.DEPT_ps.equals(_OIVPM05_OIVPM05_meta.getDEPT_ps()))
    		return false;
    	if(this.PHON_length != _OIVPM05_OIVPM05_meta.getPHON_length()) return false;
    	if(this.PHON_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_color() != null)
    		return false;
    	} else if(!this.PHON_color.equals(_OIVPM05_OIVPM05_meta.getPHON_color()))
    		return false;
    	if(this.PHON_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_hilight() != null)
    		return false;
    	} else if(!this.PHON_hilight.equals(_OIVPM05_OIVPM05_meta.getPHON_hilight()))
    		return false;
    	if(this.PHON_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_outline() != null)
    		return false;
    	} else if(!this.PHON_outline.equals(_OIVPM05_OIVPM05_meta.getPHON_outline()))
    		return false;
    	if(this.PHON_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_transp() != null)
    		return false;
    	} else if(!this.PHON_transp.equals(_OIVPM05_OIVPM05_meta.getPHON_transp()))
    		return false;
    	if(this.PHON_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_validn() != null)
    		return false;
    	} else if(!this.PHON_validn.equals(_OIVPM05_OIVPM05_meta.getPHON_validn()))
    		return false;
    	if(this.PHON_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_sosi() != null)
    		return false;
    	} else if(!this.PHON_sosi.equals(_OIVPM05_OIVPM05_meta.getPHON_sosi()))
    		return false;
    	if(this.PHON_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getPHON_ps() != null)
    		return false;
    	} else if(!this.PHON_ps.equals(_OIVPM05_OIVPM05_meta.getPHON_ps()))
    		return false;
    	if(this.EMAL_length != _OIVPM05_OIVPM05_meta.getEMAL_length()) return false;
    	if(this.EMAL_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_color() != null)
    		return false;
    	} else if(!this.EMAL_color.equals(_OIVPM05_OIVPM05_meta.getEMAL_color()))
    		return false;
    	if(this.EMAL_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_hilight() != null)
    		return false;
    	} else if(!this.EMAL_hilight.equals(_OIVPM05_OIVPM05_meta.getEMAL_hilight()))
    		return false;
    	if(this.EMAL_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_outline() != null)
    		return false;
    	} else if(!this.EMAL_outline.equals(_OIVPM05_OIVPM05_meta.getEMAL_outline()))
    		return false;
    	if(this.EMAL_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_transp() != null)
    		return false;
    	} else if(!this.EMAL_transp.equals(_OIVPM05_OIVPM05_meta.getEMAL_transp()))
    		return false;
    	if(this.EMAL_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_validn() != null)
    		return false;
    	} else if(!this.EMAL_validn.equals(_OIVPM05_OIVPM05_meta.getEMAL_validn()))
    		return false;
    	if(this.EMAL_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_sosi() != null)
    		return false;
    	} else if(!this.EMAL_sosi.equals(_OIVPM05_OIVPM05_meta.getEMAL_sosi()))
    		return false;
    	if(this.EMAL_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getEMAL_ps() != null)
    		return false;
    	} else if(!this.EMAL_ps.equals(_OIVPM05_OIVPM05_meta.getEMAL_ps()))
    		return false;
    	if(this.ADDR_length != _OIVPM05_OIVPM05_meta.getADDR_length()) return false;
    	if(this.ADDR_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_color() != null)
    		return false;
    	} else if(!this.ADDR_color.equals(_OIVPM05_OIVPM05_meta.getADDR_color()))
    		return false;
    	if(this.ADDR_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_hilight() != null)
    		return false;
    	} else if(!this.ADDR_hilight.equals(_OIVPM05_OIVPM05_meta.getADDR_hilight()))
    		return false;
    	if(this.ADDR_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_outline() != null)
    		return false;
    	} else if(!this.ADDR_outline.equals(_OIVPM05_OIVPM05_meta.getADDR_outline()))
    		return false;
    	if(this.ADDR_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_transp() != null)
    		return false;
    	} else if(!this.ADDR_transp.equals(_OIVPM05_OIVPM05_meta.getADDR_transp()))
    		return false;
    	if(this.ADDR_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_validn() != null)
    		return false;
    	} else if(!this.ADDR_validn.equals(_OIVPM05_OIVPM05_meta.getADDR_validn()))
    		return false;
    	if(this.ADDR_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_sosi() != null)
    		return false;
    	} else if(!this.ADDR_sosi.equals(_OIVPM05_OIVPM05_meta.getADDR_sosi()))
    		return false;
    	if(this.ADDR_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getADDR_ps() != null)
    		return false;
    	} else if(!this.ADDR_ps.equals(_OIVPM05_OIVPM05_meta.getADDR_ps()))
    		return false;
    	if(this.MSG_length != _OIVPM05_OIVPM05_meta.getMSG_length()) return false;
    	if(this.MSG_color == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_color() != null)
    		return false;
    	} else if(!this.MSG_color.equals(_OIVPM05_OIVPM05_meta.getMSG_color()))
    		return false;
    	if(this.MSG_hilight == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_hilight() != null)
    		return false;
    	} else if(!this.MSG_hilight.equals(_OIVPM05_OIVPM05_meta.getMSG_hilight()))
    		return false;
    	if(this.MSG_outline == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_outline() != null)
    		return false;
    	} else if(!this.MSG_outline.equals(_OIVPM05_OIVPM05_meta.getMSG_outline()))
    		return false;
    	if(this.MSG_transp == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_transp() != null)
    		return false;
    	} else if(!this.MSG_transp.equals(_OIVPM05_OIVPM05_meta.getMSG_transp()))
    		return false;
    	if(this.MSG_validn == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_validn() != null)
    		return false;
    	} else if(!this.MSG_validn.equals(_OIVPM05_OIVPM05_meta.getMSG_validn()))
    		return false;
    	if(this.MSG_sosi == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_sosi() != null)
    		return false;
    	} else if(!this.MSG_sosi.equals(_OIVPM05_OIVPM05_meta.getMSG_sosi()))
    		return false;
    	if(this.MSG_ps == null) {
    	if(_OIVPM05_OIVPM05_meta.getMSG_ps() != null)
    		return false;
    	} else if(!this.MSG_ps.equals(_OIVPM05_OIVPM05_meta.getMSG_ps()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + this.DATE_length;
    	result = prime * result + ((this.DATE_color == null) ? 0 : this.DATE_color.hashCode());
    	result = prime * result + ((this.DATE_hilight == null) ? 0 : this.DATE_hilight.hashCode());
    	result = prime * result + ((this.DATE_outline == null) ? 0 : this.DATE_outline.hashCode());
    	result = prime * result + ((this.DATE_transp == null) ? 0 : this.DATE_transp.hashCode());
    	result = prime * result + ((this.DATE_validn == null) ? 0 : this.DATE_validn.hashCode());
    	result = prime * result + ((this.DATE_sosi == null) ? 0 : this.DATE_sosi.hashCode());
    	result = prime * result + ((this.DATE_ps == null) ? 0 : this.DATE_ps.hashCode());
    	result = prime * result + this.TERM_length;
    	result = prime * result + ((this.TERM_color == null) ? 0 : this.TERM_color.hashCode());
    	result = prime * result + ((this.TERM_hilight == null) ? 0 : this.TERM_hilight.hashCode());
    	result = prime * result + ((this.TERM_outline == null) ? 0 : this.TERM_outline.hashCode());
    	result = prime * result + ((this.TERM_transp == null) ? 0 : this.TERM_transp.hashCode());
    	result = prime * result + ((this.TERM_validn == null) ? 0 : this.TERM_validn.hashCode());
    	result = prime * result + ((this.TERM_sosi == null) ? 0 : this.TERM_sosi.hashCode());
    	result = prime * result + ((this.TERM_ps == null) ? 0 : this.TERM_ps.hashCode());
    	result = prime * result + this.TIME_length;
    	result = prime * result + ((this.TIME_color == null) ? 0 : this.TIME_color.hashCode());
    	result = prime * result + ((this.TIME_hilight == null) ? 0 : this.TIME_hilight.hashCode());
    	result = prime * result + ((this.TIME_outline == null) ? 0 : this.TIME_outline.hashCode());
    	result = prime * result + ((this.TIME_transp == null) ? 0 : this.TIME_transp.hashCode());
    	result = prime * result + ((this.TIME_validn == null) ? 0 : this.TIME_validn.hashCode());
    	result = prime * result + ((this.TIME_sosi == null) ? 0 : this.TIME_sosi.hashCode());
    	result = prime * result + ((this.TIME_ps == null) ? 0 : this.TIME_ps.hashCode());
    	result = prime * result + this.IDEN_length;
    	result = prime * result + ((this.IDEN_color == null) ? 0 : this.IDEN_color.hashCode());
    	result = prime * result + ((this.IDEN_hilight == null) ? 0 : this.IDEN_hilight.hashCode());
    	result = prime * result + ((this.IDEN_outline == null) ? 0 : this.IDEN_outline.hashCode());
    	result = prime * result + ((this.IDEN_transp == null) ? 0 : this.IDEN_transp.hashCode());
    	result = prime * result + ((this.IDEN_validn == null) ? 0 : this.IDEN_validn.hashCode());
    	result = prime * result + ((this.IDEN_sosi == null) ? 0 : this.IDEN_sosi.hashCode());
    	result = prime * result + ((this.IDEN_ps == null) ? 0 : this.IDEN_ps.hashCode());
    	result = prime * result + this.NAME_length;
    	result = prime * result + ((this.NAME_color == null) ? 0 : this.NAME_color.hashCode());
    	result = prime * result + ((this.NAME_hilight == null) ? 0 : this.NAME_hilight.hashCode());
    	result = prime * result + ((this.NAME_outline == null) ? 0 : this.NAME_outline.hashCode());
    	result = prime * result + ((this.NAME_transp == null) ? 0 : this.NAME_transp.hashCode());
    	result = prime * result + ((this.NAME_validn == null) ? 0 : this.NAME_validn.hashCode());
    	result = prime * result + ((this.NAME_sosi == null) ? 0 : this.NAME_sosi.hashCode());
    	result = prime * result + ((this.NAME_ps == null) ? 0 : this.NAME_ps.hashCode());
    	result = prime * result + this.DEPT_length;
    	result = prime * result + ((this.DEPT_color == null) ? 0 : this.DEPT_color.hashCode());
    	result = prime * result + ((this.DEPT_hilight == null) ? 0 : this.DEPT_hilight.hashCode());
    	result = prime * result + ((this.DEPT_outline == null) ? 0 : this.DEPT_outline.hashCode());
    	result = prime * result + ((this.DEPT_transp == null) ? 0 : this.DEPT_transp.hashCode());
    	result = prime * result + ((this.DEPT_validn == null) ? 0 : this.DEPT_validn.hashCode());
    	result = prime * result + ((this.DEPT_sosi == null) ? 0 : this.DEPT_sosi.hashCode());
    	result = prime * result + ((this.DEPT_ps == null) ? 0 : this.DEPT_ps.hashCode());
    	result = prime * result + this.PHON_length;
    	result = prime * result + ((this.PHON_color == null) ? 0 : this.PHON_color.hashCode());
    	result = prime * result + ((this.PHON_hilight == null) ? 0 : this.PHON_hilight.hashCode());
    	result = prime * result + ((this.PHON_outline == null) ? 0 : this.PHON_outline.hashCode());
    	result = prime * result + ((this.PHON_transp == null) ? 0 : this.PHON_transp.hashCode());
    	result = prime * result + ((this.PHON_validn == null) ? 0 : this.PHON_validn.hashCode());
    	result = prime * result + ((this.PHON_sosi == null) ? 0 : this.PHON_sosi.hashCode());
    	result = prime * result + ((this.PHON_ps == null) ? 0 : this.PHON_ps.hashCode());
    	result = prime * result + this.EMAL_length;
    	result = prime * result + ((this.EMAL_color == null) ? 0 : this.EMAL_color.hashCode());
    	result = prime * result + ((this.EMAL_hilight == null) ? 0 : this.EMAL_hilight.hashCode());
    	result = prime * result + ((this.EMAL_outline == null) ? 0 : this.EMAL_outline.hashCode());
    	result = prime * result + ((this.EMAL_transp == null) ? 0 : this.EMAL_transp.hashCode());
    	result = prime * result + ((this.EMAL_validn == null) ? 0 : this.EMAL_validn.hashCode());
    	result = prime * result + ((this.EMAL_sosi == null) ? 0 : this.EMAL_sosi.hashCode());
    	result = prime * result + ((this.EMAL_ps == null) ? 0 : this.EMAL_ps.hashCode());
    	result = prime * result + this.ADDR_length;
    	result = prime * result + ((this.ADDR_color == null) ? 0 : this.ADDR_color.hashCode());
    	result = prime * result + ((this.ADDR_hilight == null) ? 0 : this.ADDR_hilight.hashCode());
    	result = prime * result + ((this.ADDR_outline == null) ? 0 : this.ADDR_outline.hashCode());
    	result = prime * result + ((this.ADDR_transp == null) ? 0 : this.ADDR_transp.hashCode());
    	result = prime * result + ((this.ADDR_validn == null) ? 0 : this.ADDR_validn.hashCode());
    	result = prime * result + ((this.ADDR_sosi == null) ? 0 : this.ADDR_sosi.hashCode());
    	result = prime * result + ((this.ADDR_ps == null) ? 0 : this.ADDR_ps.hashCode());
    	result = prime * result + this.MSG_length;
    	result = prime * result + ((this.MSG_color == null) ? 0 : this.MSG_color.hashCode());
    	result = prime * result + ((this.MSG_hilight == null) ? 0 : this.MSG_hilight.hashCode());
    	result = prime * result + ((this.MSG_outline == null) ? 0 : this.MSG_outline.hashCode());
    	result = prime * result + ((this.MSG_transp == null) ? 0 : this.MSG_transp.hashCode());
    	result = prime * result + ((this.MSG_validn == null) ? 0 : this.MSG_validn.hashCode());
    	result = prime * result + ((this.MSG_sosi == null) ? 0 : this.MSG_sosi.hashCode());
    	result = prime * result + ((this.MSG_ps == null) ? 0 : this.MSG_ps.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE_length = 0;
    	DATE_color = null;
    	DATE_hilight = null;
    	DATE_outline = null;
    	DATE_transp = null;
    	DATE_validn = null;
    	DATE_sosi = null;
    	DATE_ps = null;
    	TERM_length = 0;
    	TERM_color = null;
    	TERM_hilight = null;
    	TERM_outline = null;
    	TERM_transp = null;
    	TERM_validn = null;
    	TERM_sosi = null;
    	TERM_ps = null;
    	TIME_length = 0;
    	TIME_color = null;
    	TIME_hilight = null;
    	TIME_outline = null;
    	TIME_transp = null;
    	TIME_validn = null;
    	TIME_sosi = null;
    	TIME_ps = null;
    	IDEN_length = 0;
    	IDEN_color = null;
    	IDEN_hilight = null;
    	IDEN_outline = null;
    	IDEN_transp = null;
    	IDEN_validn = null;
    	IDEN_sosi = null;
    	IDEN_ps = null;
    	NAME_length = 0;
    	NAME_color = null;
    	NAME_hilight = null;
    	NAME_outline = null;
    	NAME_transp = null;
    	NAME_validn = null;
    	NAME_sosi = null;
    	NAME_ps = null;
    	DEPT_length = 0;
    	DEPT_color = null;
    	DEPT_hilight = null;
    	DEPT_outline = null;
    	DEPT_transp = null;
    	DEPT_validn = null;
    	DEPT_sosi = null;
    	DEPT_ps = null;
    	PHON_length = 0;
    	PHON_color = null;
    	PHON_hilight = null;
    	PHON_outline = null;
    	PHON_transp = null;
    	PHON_validn = null;
    	PHON_sosi = null;
    	PHON_ps = null;
    	EMAL_length = 0;
    	EMAL_color = null;
    	EMAL_hilight = null;
    	EMAL_outline = null;
    	EMAL_transp = null;
    	EMAL_validn = null;
    	EMAL_sosi = null;
    	EMAL_ps = null;
    	ADDR_length = 0;
    	ADDR_color = null;
    	ADDR_hilight = null;
    	ADDR_outline = null;
    	ADDR_transp = null;
    	ADDR_validn = null;
    	ADDR_sosi = null;
    	ADDR_ps = null;
    	MSG_length = 0;
    	MSG_color = null;
    	MSG_hilight = null;
    	MSG_outline = null;
    	MSG_transp = null;
    	MSG_validn = null;
    	MSG_sosi = null;
    	MSG_ps = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

