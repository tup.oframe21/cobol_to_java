package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPMAIN_OutputDtoMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPMAIN_OutputDto _OIVPMAIN_OutputDto = (com.tmax.openframe.dto.OIVPMAIN_OutputDto)obj;
    	
    	if(_OIVPMAIN_OutputDto == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPMAIN_OutputDto, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPMAIN_OutputDto _OIVPMAIN_OutputDto, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("id"); 
    	if (_OIVPMAIN_OutputDto.getId() != null) {
    		writer.value(_OIVPMAIN_OutputDto.getId());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("tabId"); 
    	if (_OIVPMAIN_OutputDto.getTabId() != null) {
    		writer.value(_OIVPMAIN_OutputDto.getTabId());
		} else {
    		writer.nullValue();
    	}
    	writer.name("termId"); 
    	if (_OIVPMAIN_OutputDto.getTermId() != null) {
    		writer.value(_OIVPMAIN_OutputDto.getTermId());
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.SystemDtoMsgJson __systemDto = new com.tmax.openframe.dto.SystemDtoMsgJson();	
    	writer.name("systemDto");
    	if(_OIVPMAIN_OutputDto.getSystemDto() != null) {
    	writer.beginObject();
    	__systemDto.marshal((com.tmax.openframe.dto.SystemDto)_OIVPMAIN_OutputDto.getSystemDto(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.OIVPMAIN_ScreenDtoMsgJson __screenDto = new com.tmax.openframe.dto.OIVPMAIN_ScreenDtoMsgJson();	
    	writer.name("screenDto");
    	if(_OIVPMAIN_OutputDto.getScreenDto() != null) {
    	writer.beginObject();
    	__screenDto.marshal((com.tmax.openframe.dto.OIVPMAIN_ScreenDto)_OIVPMAIN_OutputDto.getScreenDto(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPMAIN_OutputDto _OIVPMAIN_OutputDto = new com.tmax.openframe.dto.OIVPMAIN_OutputDto();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPMAIN_OutputDto();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPMAIN_OutputDto = (com.tmax.openframe.dto.OIVPMAIN_OutputDto)unmarshal( jr,  _OIVPMAIN_OutputDto);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPMAIN_OutputDto;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPMAIN_OutputDto _OIVPMAIN_OutputDto = (com.tmax.openframe.dto.OIVPMAIN_OutputDto) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPMAIN_OutputDto();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPMAIN_OutputDto = (com.tmax.openframe.dto.OIVPMAIN_OutputDto)unmarshal( jr,  _OIVPMAIN_OutputDto);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPMAIN_OutputDto;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPMAIN_OutputDto dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPMAIN_OutputDto dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "id" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setId( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "tabId" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTabId( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "termId" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTermId( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "systemDto" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.SystemDtoMsgJson __systemDto = new com.tmax.openframe.dto.SystemDtoMsgJson();
    		
    				com.tmax.openframe.dto.SystemDto ___SystemDto = new com.tmax.openframe.dto.SystemDto();
    				reader.beginObject();
    				dto.setSystemDto((com.tmax.openframe.dto.SystemDto)__systemDto.unmarshal( reader, ___SystemDto ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "screenDto" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPMAIN_ScreenDtoMsgJson __screenDto = new com.tmax.openframe.dto.OIVPMAIN_ScreenDtoMsgJson();
    		
    				com.tmax.openframe.dto.OIVPMAIN_ScreenDto ___OIVPMAIN_ScreenDto = new com.tmax.openframe.dto.OIVPMAIN_ScreenDto();
    				reader.beginObject();
    				dto.setScreenDto((com.tmax.openframe.dto.OIVPMAIN_ScreenDto)__screenDto.unmarshal( reader, ___OIVPMAIN_ScreenDto ));
    				reader.endObject();
    			}
    			break;
    		}
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

