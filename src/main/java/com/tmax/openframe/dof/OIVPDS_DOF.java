package com.tmax.openframe.dof;


import com.tmax.openframe.dto.OIVPDS_DO;
import com.tmax.proobject.dataobject.factory.DBDataObjectFactory;
import com.tmax.proobject.dataobject.factory.Query;
import com.tmax.proobject.dataobject.model.MetaField;
import com.tmax.proobject.model.dataobject.DataObject;
import com.tmax.proobject.model.dataobject.Session;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OIVPDS_DOF extends DBDataObjectFactory<OIVPDS_DO>
{
	public OIVPDS_DOF(){
		super();
	}
	
	public OIVPDS_DOF(Session session){
		super(session);
	}
	
	public OIVPDS_DOF(String datasource){
		super(datasource);
	}
	
	public OIVPDS_DOF(String datasource, boolean autocommit){
			super(datasource, autocommit);
	}
	
	private Integer _WS_KEY;
	
	
	public static enum FULLQUERY  implements Query {
		READ_LT("SELECT * FROM OIVPDS WHERE  \n"
		                                  + "(WS_KEY) < (?) ORDER BY WS_KEY DESC"),
		REWRITE("UPDATE OIVPDS SET WS_KEY= ?,WS_NAME= ?,WS_DEPARTMENT= ?,WS_PHONE= ?,WS_EMAIL= ?,WS_ADDRESS= ? \n"
		                                  + " WHERE WS_KEY = ?"),
		DELETE("DELETE FROM OIVPDS \n"
		                                  + " WHERE WS_KEY = ?"),
		READ_LE("SELECT * FROM OIVPDS WHERE  \n"
		                                  + "(WS_KEY) <= (?) ORDER BY WS_KEY DESC"),
		READ_EQ("SELECT * FROM OIVPDS \n"
		                                  + " WHERE WS_KEY = ?"),
		READ_GE("SELECT * FROM OIVPDS WHERE  \n"
		                                  + "(WS_KEY) >= (?) ORDER BY WS_KEY ASC"),
		READ_GT("SELECT * FROM OIVPDS WHERE  \n"
		                                  + "(WS_KEY) > (?) ORDER BY WS_KEY ASC"),
		WRITE("INSERT INTO OIVPDS(WS_KEY,WS_NAME,WS_DEPARTMENT,WS_PHONE,WS_EMAIL,WS_ADDRESS) \n"
		                                  + "VALUES(?,?,?,?,?,?)")
	;
		private final String value;
	
		FULLQUERY(String v) {
			value = v;
		}
	
		public String value() {
			return value;
		}
	
		public static FULLQUERY fromValue(String v) {
			return valueOf(v);
		}
		
		public static Query getDefaultQuery(DBType type) {
			switch(type) {
				case SELECT :
					return FULLQUERY.READ_GT;
				case INSERT :
					return FULLQUERY.WRITE;
				case UPDATE :
					return FULLQUERY.REWRITE;
				case DELETE :
					return FULLQUERY.DELETE;
				default :
					return null;
			}
		}
	}
	
	
	 	
	private static String[] _QUERY_TABLES = {"OIVPDS", "OIVPDS", "OIVPDS", "OIVPDS", "OIVPDS", "OIVPDS", "OIVPDS", "OIVPDS"};
	private static List<MetaField> _FACTORY_FIELDS = new ArrayList<MetaField>();
	
	@Override
	public void init() {
		_WS_KEY = null;
		super.init();
	}
	
	@Override
	public String[] getQueryTables() {
	    return _QUERY_TABLES.clone();
	}      
	
	@Override
	public Query getDefaultQuery(DBType type) {
		return FULLQUERY.getDefaultQuery(type);
	}
	
	@Override
	public OIVPDS_DO getDataObject() {
		return new OIVPDS_DO();
	}
	
	public void setFullQuery(FULLQUERY query) {
		useFullQuery = true;
		this.query = query;
	}
	
	public void setFullQuery(String name) {
		FULLQUERY query = FULLQUERY.valueOf(name);
		setFullQuery(query);
	}
	
	@Override
	public List<MetaField> getFactoryFields() {
		return _FACTORY_FIELDS;
	}
	
	public Integer getWS_KEY() {
	    return _WS_KEY;
	}
	
	public void setWS_KEY(Integer _WS_KEY) {
	    this._WS_KEY = _WS_KEY;
	}
	
	
	
	@Override
	public Object getField(String fieldName) throws FieldNotFoundException {
		switch(fieldName) {
		case "WS_KEY" : return _WS_KEY;
		default : throw new FieldNotFoundException("unknown fieldName : "+fieldName+"("+fieldName.hashCode()+")");
		}
	}    
	
	@Override
	public void setField(String fieldName, Object value) throws FieldNotFoundException {
		switch(fieldName) {
		case "WS_KEY" : this._WS_KEY = ((Integer)value); break;
		default : throw new FieldNotFoundException("unknown fieldName : "+fieldName+"("+fieldName.hashCode()+")");
		}
	}
	
	@Override
	public Class<?> getFieldClassType(String fieldName) throws FieldNotFoundException {
		switch(fieldName) {
			case "WS_KEY" :
				return Integer.class;
			default : throw new FieldNotFoundException("unknown fieldName : "+fieldName+"("+fieldName.hashCode()+")");
		}
	}
	
	
	
	
	@Override
	public void onPrepareFullQuery(PreparedStatement ps, DataObject dataObject) {
		if(useFullQuery && query == FULLQUERY.READ_LT) {
			onPrepareFullQueryRead_lt(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.REWRITE) {
			onPrepareFullQueryRewrite(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.DELETE) {
			onPrepareFullQueryDelete(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.READ_LE) {
			onPrepareFullQueryRead_le(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.READ_EQ) {
			onPrepareFullQueryRead_eq(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.READ_GE) {
			onPrepareFullQueryRead_ge(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.READ_GT) {
			onPrepareFullQueryRead_gt(ps, (OIVPDS_DO)dataObject);
		} else if(useFullQuery && query == FULLQUERY.WRITE) {
			onPrepareFullQueryWrite(ps, (OIVPDS_DO)dataObject);
		}
	}
	
	public void onPrepareFullQueryRead_lt (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, this._WS_KEY, ps);
	}
	
	public void onPrepareFullQueryRewrite (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, dataObject.getWS_KEY(), "WS_KEY", ps);
		set(index++, String.class, dataObject.getWS_NAME(), "WS_NAME", ps);
		set(index++, String.class, dataObject.getWS_DEPARTMENT(), "WS_DEPARTMENT", ps);
		set(index++, String.class, dataObject.getWS_PHONE(), "WS_PHONE", ps);
		set(index++, String.class, dataObject.getWS_EMAIL(), "WS_EMAIL", ps);
		set(index++, String.class, dataObject.getWS_ADDRESS(), "WS_ADDRESS", ps);
		set(index++, Integer.class, dataObject.getWS_KEY(), "WS_KEY", ps);
	}
	
	public void onPrepareFullQueryDelete (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, this._WS_KEY, ps);
	}
	
	public void onPrepareFullQueryRead_le (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, this._WS_KEY, ps);
	}
	
	public void onPrepareFullQueryRead_eq (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, this._WS_KEY, ps);
	}
	
	public void onPrepareFullQueryRead_ge (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, this._WS_KEY, ps);
	}
	
	public void onPrepareFullQueryRead_gt (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, this._WS_KEY, ps);
	}
	
	public void onPrepareFullQueryWrite (PreparedStatement ps, OIVPDS_DO dataObject) {
		set(index++, Integer.class, dataObject.getWS_KEY(), "WS_KEY", ps);
		set(index++, String.class, dataObject.getWS_NAME(), "WS_NAME", ps);
		set(index++, String.class, dataObject.getWS_DEPARTMENT(), "WS_DEPARTMENT", ps);
		set(index++, String.class, dataObject.getWS_PHONE(), "WS_PHONE", ps);
		set(index++, String.class, dataObject.getWS_EMAIL(), "WS_EMAIL", ps);
		set(index++, String.class, dataObject.getWS_ADDRESS(), "WS_ADDRESS", ps);
	}
	
	static {
		_FACTORY_FIELDS.add(new MetaField("WS_KEY",Integer.class,"","","",false,false));
	
		_FACTORY_FIELDS = Collections.unmodifiableList(_FACTORY_FIELDS);
	}
	
	@Override
	public int add(DataObject insert, boolean immediate) {
		return super.add(insert, immediate);
	}
	@Override
	public int add(OIVPDS_DO insert, String... autoIncreasedFields) {
		return super.add(insert, autoIncreasedFields);
	}
	@Override
	public int update(DataObject update, boolean immediate) {
		return super.update(update, immediate);
	}
	@Override
	public int update(DataObject update, String... autoIncreasedColumns) {
		return super.update(update, autoIncreasedColumns);
	}
	    
	
	@Override
	public String toString() {
	    return "OIVPDS_DOF [" + super.toString()
	        + ", WS_KEY=" + _WS_KEY
	        + "]";
	}
}

