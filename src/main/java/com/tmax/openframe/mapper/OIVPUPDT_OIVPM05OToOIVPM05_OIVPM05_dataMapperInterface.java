package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPUPDT_OIVPM05OToOIVPM05_OIVPM05_dataMapperInterface {
OIVPUPDT_OIVPM05OToOIVPM05_OIVPM05_dataMapperInterface INSTANCE = Mappers.getMapper(OIVPUPDT_OIVPM05OToOIVPM05_OIVPM05_dataMapperInterface.class);


    @Mapping(target = "DATE", source = "DATEO")
    @Mapping(target = "TERM", source = "TERMO")
    @Mapping(target = "TIME", source = "TIMEO")
    @Mapping(target = "IDEN", source = "IDENO")
    @Mapping(target = "NAME", source = "NAMEO")
    @Mapping(target = "DEPT", source = "DEPTO")
    @Mapping(target = "PHON", source = "PHONO")
    @Mapping(target = "EMAL", source = "EMALO")
    @Mapping(target = "ADDR", source = "ADDRO")
    @Mapping(target = "MSG", source = "MSGO")
    void toTarget(OIVPUPDT_OIVPM05O source, @MappingTarget OIVPM05_OIVPM05_data target);

}
