package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPQUIT_OIVPM07OToOIVPM07_OIVPM07_metaMapperInterface {
OIVPQUIT_OIVPM07OToOIVPM07_OIVPM07_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPQUIT_OIVPM07OToOIVPM07_OIVPM07_metaMapperInterface.class);


    void toTarget(OIVPQUIT_OIVPM07O source, @MappingTarget OIVPM07_OIVPM07_meta target);

}
