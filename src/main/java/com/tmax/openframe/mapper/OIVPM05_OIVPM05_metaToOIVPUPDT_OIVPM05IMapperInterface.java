package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM05_OIVPM05_metaToOIVPUPDT_OIVPM05IMapperInterface {
OIVPM05_OIVPM05_metaToOIVPUPDT_OIVPM05IMapperInterface INSTANCE = Mappers.getMapper(OIVPM05_OIVPM05_metaToOIVPUPDT_OIVPM05IMapperInterface.class);


    @Mapping(target = "DATEL", source = "DATE_length")
    @Mapping(target = "TERML", source = "TERM_length")
    @Mapping(target = "TIMEL", source = "TIME_length")
    @Mapping(target = "IDENL", source = "IDEN_length")
    @Mapping(target = "NAMEL", source = "NAME_length")
    @Mapping(target = "DEPTL", source = "DEPT_length")
    @Mapping(target = "PHONL", source = "PHON_length")
    @Mapping(target = "EMALL", source = "EMAL_length")
    @Mapping(target = "ADDRL", source = "ADDR_length")
    @Mapping(target = "MSGL", source = "MSG_length")
    void toTarget(OIVPM05_OIVPM05_meta source, @MappingTarget OIVPUPDT_OIVPM05I target);

}
