package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM01_OIVPM01_dataToOIVPMAIN_OIVPM01IMapperInterface {
OIVPM01_OIVPM01_dataToOIVPMAIN_OIVPM01IMapperInterface INSTANCE = Mappers.getMapper(OIVPM01_OIVPM01_dataToOIVPMAIN_OIVPM01IMapperInterface.class);


    @Mapping(target = "DATEI", source = "DATE")
    @Mapping(target = "TERMI", source = "TERM")
    @Mapping(target = "TIMEI", source = "TIME")
    @Mapping(target = "CODEI", source = "CODE")
    @Mapping(target = "IDENI", source = "IDEN")
    @Mapping(target = "MSGI", source = "MSG")
    void toTarget(OIVPM01_OIVPM01_data source, @MappingTarget OIVPMAIN_OIVPM01I target);

}
