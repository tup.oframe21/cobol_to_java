package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM06_OIVPM06_dataToOIVPDELT_OIVPM06IMapperInterface {
OIVPM06_OIVPM06_dataToOIVPDELT_OIVPM06IMapperInterface INSTANCE = Mappers.getMapper(OIVPM06_OIVPM06_dataToOIVPDELT_OIVPM06IMapperInterface.class);


    @Mapping(target = "DATEI", source = "DATE")
    @Mapping(target = "TERMI", source = "TERM")
    @Mapping(target = "TIMEI", source = "TIME")
    @Mapping(target = "IDENI", source = "IDEN")
    @Mapping(target = "NAMEI", source = "NAME")
    @Mapping(target = "DEPTI", source = "DEPT")
    @Mapping(target = "PHONI", source = "PHON")
    @Mapping(target = "EMALI", source = "EMAL")
    @Mapping(target = "ADDRI", source = "ADDR")
    @Mapping(target = "MSGI", source = "MSG")
    void toTarget(OIVPM06_OIVPM06_data source, @MappingTarget OIVPDELT_OIVPM06I target);

}
