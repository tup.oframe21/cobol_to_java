package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM02_OIVPM02_dataToOIVPBROS_OIVPM02IMapperInterface {
OIVPM02_OIVPM02_dataToOIVPBROS_OIVPM02IMapperInterface INSTANCE = Mappers.getMapper(OIVPM02_OIVPM02_dataToOIVPBROS_OIVPM02IMapperInterface.class);


    @Mapping(target = "DATEI", source = "DATE")
    @Mapping(target = "TERMI", source = "TERM")
    @Mapping(target = "TIMEI", source = "TIME")
    @Mapping(target = "IDEN1I", source = "IDEN1")
    @Mapping(target = "NAME1I", source = "NAME1")
    @Mapping(target = "DEPT1I", source = "DEPT1")
    @Mapping(target = "PHON1I", source = "PHON1")
    @Mapping(target = "IDEN2I", source = "IDEN2")
    @Mapping(target = "NAME2I", source = "NAME2")
    @Mapping(target = "DEPT2I", source = "DEPT2")
    @Mapping(target = "PHON2I", source = "PHON2")
    @Mapping(target = "IDEN3I", source = "IDEN3")
    @Mapping(target = "NAME3I", source = "NAME3")
    @Mapping(target = "DEPT3I", source = "DEPT3")
    @Mapping(target = "PHON3I", source = "PHON3")
    @Mapping(target = "IDEN4I", source = "IDEN4")
    @Mapping(target = "NAME4I", source = "NAME4")
    @Mapping(target = "DEPT4I", source = "DEPT4")
    @Mapping(target = "PHON4I", source = "PHON4")
    @Mapping(target = "IDEN5I", source = "IDEN5")
    @Mapping(target = "NAME5I", source = "NAME5")
    @Mapping(target = "DEPT5I", source = "DEPT5")
    @Mapping(target = "PHON5I", source = "PHON5")
    @Mapping(target = "BIDXI", source = "BIDX")
    @Mapping(target = "FIDXI", source = "FIDX")
    @Mapping(target = "MSGI", source = "MSG")
    void toTarget(OIVPM02_OIVPM02_data source, @MappingTarget OIVPBROS_OIVPM02I target);

}
