package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPQUIT_OIVPM07OToOIVPM07_OIVPM07_dataMapperInterface {
OIVPQUIT_OIVPM07OToOIVPM07_OIVPM07_dataMapperInterface INSTANCE = Mappers.getMapper(OIVPQUIT_OIVPM07OToOIVPM07_OIVPM07_dataMapperInterface.class);


    @Mapping(target = "DATE", source = "DATEO")
    @Mapping(target = "TERM", source = "TERMO")
    @Mapping(target = "TIME", source = "TIMEO")
    @Mapping(target = "ENDM", source = "ENDMO")
    @Mapping(target = "MSG", source = "MSGO")
    void toTarget(OIVPQUIT_OIVPM07O source, @MappingTarget OIVPM07_OIVPM07_data target);

}
