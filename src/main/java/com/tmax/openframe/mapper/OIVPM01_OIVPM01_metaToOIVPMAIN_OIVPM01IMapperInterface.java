package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM01_OIVPM01_metaToOIVPMAIN_OIVPM01IMapperInterface {
OIVPM01_OIVPM01_metaToOIVPMAIN_OIVPM01IMapperInterface INSTANCE = Mappers.getMapper(OIVPM01_OIVPM01_metaToOIVPMAIN_OIVPM01IMapperInterface.class);


    @Mapping(target = "DATEL", source = "DATE_length")
    @Mapping(target = "TERML", source = "TERM_length")
    @Mapping(target = "TIMEL", source = "TIME_length")
    @Mapping(target = "CODEL", source = "CODE_length")
    @Mapping(target = "IDENL", source = "IDEN_length")
    @Mapping(target = "MSGL", source = "MSG_length")
    void toTarget(OIVPM01_OIVPM01_meta source, @MappingTarget OIVPMAIN_OIVPM01I target);

}
