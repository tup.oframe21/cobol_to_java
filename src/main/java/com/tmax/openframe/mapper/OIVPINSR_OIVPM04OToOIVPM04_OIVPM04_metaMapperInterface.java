package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPINSR_OIVPM04OToOIVPM04_OIVPM04_metaMapperInterface {
OIVPINSR_OIVPM04OToOIVPM04_OIVPM04_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPINSR_OIVPM04OToOIVPM04_OIVPM04_metaMapperInterface.class);


    void toTarget(OIVPINSR_OIVPM04O source, @MappingTarget OIVPM04_OIVPM04_meta target);

}
