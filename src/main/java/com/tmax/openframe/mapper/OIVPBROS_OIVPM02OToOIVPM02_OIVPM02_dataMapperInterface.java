package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPBROS_OIVPM02OToOIVPM02_OIVPM02_dataMapperInterface {
OIVPBROS_OIVPM02OToOIVPM02_OIVPM02_dataMapperInterface INSTANCE = Mappers.getMapper(OIVPBROS_OIVPM02OToOIVPM02_OIVPM02_dataMapperInterface.class);


    @Mapping(target = "DATE", source = "DATEO")
    @Mapping(target = "TERM", source = "TERMO")
    @Mapping(target = "TIME", source = "TIMEO")
    @Mapping(target = "IDEN1", source = "IDEN1O")
    @Mapping(target = "NAME1", source = "NAME1O")
    @Mapping(target = "DEPT1", source = "DEPT1O")
    @Mapping(target = "PHON1", source = "PHON1O")
    @Mapping(target = "IDEN2", source = "IDEN2O")
    @Mapping(target = "NAME2", source = "NAME2O")
    @Mapping(target = "DEPT2", source = "DEPT2O")
    @Mapping(target = "PHON2", source = "PHON2O")
    @Mapping(target = "IDEN3", source = "IDEN3O")
    @Mapping(target = "NAME3", source = "NAME3O")
    @Mapping(target = "DEPT3", source = "DEPT3O")
    @Mapping(target = "PHON3", source = "PHON3O")
    @Mapping(target = "IDEN4", source = "IDEN4O")
    @Mapping(target = "NAME4", source = "NAME4O")
    @Mapping(target = "DEPT4", source = "DEPT4O")
    @Mapping(target = "PHON4", source = "PHON4O")
    @Mapping(target = "IDEN5", source = "IDEN5O")
    @Mapping(target = "NAME5", source = "NAME5O")
    @Mapping(target = "DEPT5", source = "DEPT5O")
    @Mapping(target = "PHON5", source = "PHON5O")
    @Mapping(target = "BIDX", source = "BIDXO")
    @Mapping(target = "FIDX", source = "FIDXO")
    @Mapping(target = "MSG", source = "MSGO")
    void toTarget(OIVPBROS_OIVPM02O source, @MappingTarget OIVPM02_OIVPM02_data target);

}
