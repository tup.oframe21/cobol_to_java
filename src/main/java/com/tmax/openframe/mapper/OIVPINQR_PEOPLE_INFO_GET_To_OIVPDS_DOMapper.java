package com.tmax.openframe.mapper;

import com.tmax.openframe.dto.OIVPDS_DO;
import com.tmax.openframe.variable.group.OIVPINQR_PEOPLE_INFO_GET;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;


/**
 *  임시로 OIVP에 변환된 클래스와 패키지를 맞추려고
 */
@Mapper
public interface OIVPINQR_PEOPLE_INFO_GET_To_OIVPDS_DOMapper {
  OIVPINQR_PEOPLE_INFO_GET_To_OIVPDS_DOMapper INSTANCE = getMapper(OIVPINQR_PEOPLE_INFO_GET_To_OIVPDS_DOMapper.class);

  @Mapping(source = "WS_KEY", target = "WS_KEY")
  @Mapping(source = "WS_NAME", target = "WS_NAME")
  @Mapping(source = "WS_DEPARTMENT", target = "WS_DEPARTMENT")
  @Mapping(source = "WS_PHONE", target = "WS_PHONE")
  @Mapping(source = "WS_EMAIL", target = "WS_EMAIL")
  @Mapping(source = "WS_ADDRESS", target = "WS_ADDRESS")
  OIVPDS_DO toTarget(OIVPINQR_PEOPLE_INFO_GET peopleInfo);

  @Mapping(source = "WS_KEY", target = "WS_KEY")
  @Mapping(source = "WS_NAME", target = "WS_NAME")
  @Mapping(source = "WS_DEPARTMENT", target = "WS_DEPARTMENT")
  @Mapping(source = "WS_PHONE", target = "WS_PHONE")
  @Mapping(source = "WS_EMAIL", target = "WS_EMAIL")
  @Mapping(source = "WS_ADDRESS", target = "WS_ADDRESS")
  OIVPINQR_PEOPLE_INFO_GET toSource(OIVPDS_DO peopleInfo);
}
