package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPMAIN_OIVPM01OToOIVPM01_OIVPM01_metaMapperInterface {
OIVPMAIN_OIVPM01OToOIVPM01_OIVPM01_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPMAIN_OIVPM01OToOIVPM01_OIVPM01_metaMapperInterface.class);


    void toTarget(OIVPMAIN_OIVPM01O source, @MappingTarget OIVPM01_OIVPM01_meta target);

}
