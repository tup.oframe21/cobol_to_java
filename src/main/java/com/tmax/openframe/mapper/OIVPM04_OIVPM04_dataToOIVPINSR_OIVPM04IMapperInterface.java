package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM04_OIVPM04_dataToOIVPINSR_OIVPM04IMapperInterface {
OIVPM04_OIVPM04_dataToOIVPINSR_OIVPM04IMapperInterface INSTANCE = Mappers.getMapper(OIVPM04_OIVPM04_dataToOIVPINSR_OIVPM04IMapperInterface.class);


    @Mapping(target = "DATEI", source = "DATE")
    @Mapping(target = "TERMI", source = "TERM")
    @Mapping(target = "TIMEI", source = "TIME")
    @Mapping(target = "IDENI", source = "IDEN")
    @Mapping(target = "NAMEI", source = "NAME")
    @Mapping(target = "DEPTI", source = "DEPT")
    @Mapping(target = "PHONI", source = "PHON")
    @Mapping(target = "EMALI", source = "EMAL")
    @Mapping(target = "ADDRI", source = "ADDR")
    @Mapping(target = "MSGI", source = "MSG")
    void toTarget(OIVPM04_OIVPM04_data source, @MappingTarget OIVPINSR_OIVPM04I target);

}
