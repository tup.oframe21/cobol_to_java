package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPMAIN_OIVPM01OToOIVPM01_OIVPM01_dataMapperInterface {
OIVPMAIN_OIVPM01OToOIVPM01_OIVPM01_dataMapperInterface INSTANCE = Mappers.getMapper(OIVPMAIN_OIVPM01OToOIVPM01_OIVPM01_dataMapperInterface.class);


    @Mapping(target = "DATE", source = "DATEO")
    @Mapping(target = "TERM", source = "TERMO")
    @Mapping(target = "TIME", source = "TIMEO")
    @Mapping(target = "CODE", source = "CODEO")
    @Mapping(target = "IDEN", source = "IDENO")
    @Mapping(target = "MSG", source = "MSGO")
    void toTarget(OIVPMAIN_OIVPM01O source, @MappingTarget OIVPM01_OIVPM01_data target);

}
