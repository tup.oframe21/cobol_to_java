package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPDELT_OIVPM06OToOIVPM06_OIVPM06_metaMapperInterface {
OIVPDELT_OIVPM06OToOIVPM06_OIVPM06_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPDELT_OIVPM06OToOIVPM06_OIVPM06_metaMapperInterface.class);


    void toTarget(OIVPDELT_OIVPM06O source, @MappingTarget OIVPM06_OIVPM06_meta target);

}
