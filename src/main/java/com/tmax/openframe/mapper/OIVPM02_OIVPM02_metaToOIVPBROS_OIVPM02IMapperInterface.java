package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPM02_OIVPM02_metaToOIVPBROS_OIVPM02IMapperInterface {
OIVPM02_OIVPM02_metaToOIVPBROS_OIVPM02IMapperInterface INSTANCE = Mappers.getMapper(OIVPM02_OIVPM02_metaToOIVPBROS_OIVPM02IMapperInterface.class);


    @Mapping(target = "DATEL", source = "DATE_length")
    @Mapping(target = "TERML", source = "TERM_length")
    @Mapping(target = "TIMEL", source = "TIME_length")
    @Mapping(target = "IDEN1L", source = "IDEN1_length")
    @Mapping(target = "NAME1L", source = "NAME1_length")
    @Mapping(target = "DEPT1L", source = "DEPT1_length")
    @Mapping(target = "PHON1L", source = "PHON1_length")
    @Mapping(target = "IDEN2L", source = "IDEN2_length")
    @Mapping(target = "NAME2L", source = "NAME2_length")
    @Mapping(target = "DEPT2L", source = "DEPT2_length")
    @Mapping(target = "PHON2L", source = "PHON2_length")
    @Mapping(target = "IDEN3L", source = "IDEN3_length")
    @Mapping(target = "NAME3L", source = "NAME3_length")
    @Mapping(target = "DEPT3L", source = "DEPT3_length")
    @Mapping(target = "PHON3L", source = "PHON3_length")
    @Mapping(target = "IDEN4L", source = "IDEN4_length")
    @Mapping(target = "NAME4L", source = "NAME4_length")
    @Mapping(target = "DEPT4L", source = "DEPT4_length")
    @Mapping(target = "PHON4L", source = "PHON4_length")
    @Mapping(target = "IDEN5L", source = "IDEN5_length")
    @Mapping(target = "NAME5L", source = "NAME5_length")
    @Mapping(target = "DEPT5L", source = "DEPT5_length")
    @Mapping(target = "PHON5L", source = "PHON5_length")
    @Mapping(target = "BIDXL", source = "BIDX_length")
    @Mapping(target = "FIDXL", source = "FIDX_length")
    @Mapping(target = "MSGL", source = "MSG_length")
    void toTarget(OIVPM02_OIVPM02_meta source, @MappingTarget OIVPBROS_OIVPM02I target);

}
