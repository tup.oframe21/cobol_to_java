package com.tmax.openframe.mapper;

import com.tmax.openframe.dto.OIVPDS_DO;
import com.tmax.openframe.variable.group.OIVPUPDT_PEOPLE_INFO_TEMP;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;


/**
 *  임시로 OIVP에 변환된 클래스와 패키지를 맞추려고
 */
@Mapper
public interface OIVPUPDT_PEOPLE_INFO_TEMP_To_OIVPDS_DOMapper {
  OIVPUPDT_PEOPLE_INFO_TEMP_To_OIVPDS_DOMapper INSTANCE = getMapper(OIVPUPDT_PEOPLE_INFO_TEMP_To_OIVPDS_DOMapper.class);

  @Mapping(source = "OFWSKEY", target = "WS_KEY")
  @Mapping(source = "OFWS_NAME", target = "WS_NAME")
  @Mapping(source = "OFWS_DEPARTMENT", target = "WS_DEPARTMENT")
  @Mapping(source = "OFWS_PHONE", target = "WS_PHONE")
  @Mapping(source = "OFWS_EMAIL", target = "WS_EMAIL")
  @Mapping(source = "OFWS_ADDRESS", target = "WS_ADDRESS")
  OIVPDS_DO toTarget(OIVPUPDT_PEOPLE_INFO_TEMP peopleInfo);

  @Mapping(source = "WS_KEY", target = "OFWSKEY")
  @Mapping(source = "WS_NAME", target = "OFWS_NAME")
  @Mapping(source = "WS_DEPARTMENT", target = "OFWS_DEPARTMENT")
  @Mapping(source = "WS_PHONE", target = "OFWS_PHONE")
  @Mapping(source = "WS_EMAIL", target = "OFWS_EMAIL")
  @Mapping(source = "WS_ADDRESS", target = "OFWS_ADDRESS")
  OIVPUPDT_PEOPLE_INFO_TEMP toSource(OIVPDS_DO peopleInfo);
}
