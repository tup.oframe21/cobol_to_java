package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPINQR_OIVPM03OToOIVPM03_OIVPM03_metaMapperInterface {
OIVPINQR_OIVPM03OToOIVPM03_OIVPM03_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPINQR_OIVPM03OToOIVPM03_OIVPM03_metaMapperInterface.class);


    void toTarget(OIVPINQR_OIVPM03O source, @MappingTarget OIVPM03_OIVPM03_meta target);

}
