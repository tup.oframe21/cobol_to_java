package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPBROS_OIVPM02OToOIVPM02_OIVPM02_metaMapperInterface {
OIVPBROS_OIVPM02OToOIVPM02_OIVPM02_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPBROS_OIVPM02OToOIVPM02_OIVPM02_metaMapperInterface.class);


    void toTarget(OIVPBROS_OIVPM02O source, @MappingTarget OIVPM02_OIVPM02_meta target);

}
