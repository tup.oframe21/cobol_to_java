package com.tmax.openframe.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import com.tmax.openframe.dto.*;
import com.tmax.openframe.variable.group.*;


@Mapper
public interface OIVPUPDT_OIVPM05OToOIVPM05_OIVPM05_metaMapperInterface {
OIVPUPDT_OIVPM05OToOIVPM05_OIVPM05_metaMapperInterface INSTANCE = Mappers.getMapper(OIVPUPDT_OIVPM05OToOIVPM05_OIVPM05_metaMapperInterface.class);


    void toTarget(OIVPUPDT_OIVPM05O source, @MappingTarget OIVPM05_OIVPM05_meta target);

}
