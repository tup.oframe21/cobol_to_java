package com.tmax.openframe.mapper;

import com.tmax.openframe.dto.OIVPDS_DO;
import com.tmax.openframe.variable.group.OIVPUPDT_PEOPLE_INFO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.factory.Mappers.getMapper;


/**
 *  임시로 OIVP에 변환된 클래스와 패키지를 맞추려고
 */
@Mapper
public interface OIVPUPDT_PEOPLE_INFO_To_OIVPUPDT_PEOPLE_INFO_DOMapper {
  OIVPUPDT_PEOPLE_INFO_To_OIVPUPDT_PEOPLE_INFO_DOMapper INSTANCE = getMapper(OIVPUPDT_PEOPLE_INFO_To_OIVPUPDT_PEOPLE_INFO_DOMapper.class);

  @Mapping(source = "WSKEY", target = "WS_KEY")
  @Mapping(source = "WS_NAME", target = "WS_NAME")
  @Mapping(source = "WS_DEPARTMENT", target = "WS_DEPARTMENT")
  @Mapping(source = "WS_PHONE", target = "WS_PHONE")
  @Mapping(source = "WS_EMAIL", target = "WS_EMAIL")
  @Mapping(source = "WS_ADDRESS", target = "WS_ADDRESS")
  OIVPDS_DO toTarget(OIVPUPDT_PEOPLE_INFO peopleInfo);

  @Mapping(source = "WS_KEY", target = "WSKEY")
  @Mapping(source = "WS_NAME", target = "WS_NAME")
  @Mapping(source = "WS_DEPARTMENT", target = "WS_DEPARTMENT")
  @Mapping(source = "WS_PHONE", target = "WS_PHONE")
  @Mapping(source = "WS_EMAIL", target = "WS_EMAIL")
  @Mapping(source = "WS_ADDRESS", target = "WS_ADDRESS")
  OIVPUPDT_PEOPLE_INFO toSource(OIVPDS_DO peopleInfo);
}
