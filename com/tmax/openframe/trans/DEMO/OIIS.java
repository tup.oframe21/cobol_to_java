package com.tmax.openframe.trans.DEMO;

import com.tmax.proobject.core.ServiceObject;
import com.tmax.proobject.engine.logger.ServiceLogger;
import com.tmax.proobject.engine.servicemanager.ServiceManager;
import com.tmax.proobject.logger.ProObjectLogger;
import com.tmax.proobject.model.transaction.TransactionType;

@ServiceObject
public class OIIS implements com.tmax.proobject.model.service.ServiceObject < com.tmax.openframe.dto.OIVPINSR_InputDto, com.tmax.openframe.dto.OIVPINSR_OutputDto> {
    private ProObjectLogger logger = ServiceLogger.getLogger();
    public com.tmax.openframe.dto.OIVPINSR_InputDto input;
    public com.tmax.openframe.dto.OIVPINSR_OutputDto output;

    @Override
    public com.tmax.openframe.dto.OIVPINSR_OutputDto service (com.tmax.openframe.dto.OIVPINSR_InputDto arg0) throws Throwable {
        ServiceManager.forward("OF21.pgmsvg.OIVPINSR", arg0, TransactionType.JOIN);
        return output;
    }
}
