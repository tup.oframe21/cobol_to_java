package com.tmax.openframe.trans.DEMO;

import com.tmax.proobject.core.ServiceObject;
import com.tmax.proobject.engine.logger.ServiceLogger;
import com.tmax.proobject.engine.servicemanager.ServiceManager;
import com.tmax.proobject.logger.ProObjectLogger;
import com.tmax.proobject.model.transaction.TransactionType;

@ServiceObject
public class OIVP implements com.tmax.proobject.model.service.ServiceObject < com.tmax.openframe.dto.OIVPMAIN_InputDto, com.tmax.openframe.dto.OIVPMAIN_OutputDto> {
    private ProObjectLogger logger = ServiceLogger.getLogger();
    public com.tmax.openframe.dto.OIVPMAIN_InputDto input;
    public com.tmax.openframe.dto.OIVPMAIN_OutputDto output;

    @Override
    public com.tmax.openframe.dto.OIVPMAIN_OutputDto service (com.tmax.openframe.dto.OIVPMAIN_InputDto arg0) throws Throwable {
        ServiceManager.forward("OF21.pgmsvg.OIVPMAIN", arg0, TransactionType.JOIN);
        return output;
    }
}
