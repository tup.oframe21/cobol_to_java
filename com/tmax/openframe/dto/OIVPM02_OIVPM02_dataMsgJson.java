package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPM02_OIVPM02_dataMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPM02_OIVPM02_data _OIVPM02_OIVPM02_data = (com.tmax.openframe.dto.OIVPM02_OIVPM02_data)obj;
    	
    	if(_OIVPM02_OIVPM02_data == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPM02_OIVPM02_data, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPM02_OIVPM02_data _OIVPM02_OIVPM02_data, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DATE"); 
    	if (_OIVPM02_OIVPM02_data.getDATE() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getDATE());
    		writer.value(_OIVPM02_OIVPM02_data.getDATE());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM"); 
    	if (_OIVPM02_OIVPM02_data.getTERM() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getTERM());
    		writer.value(_OIVPM02_OIVPM02_data.getTERM());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME"); 
    	if (_OIVPM02_OIVPM02_data.getTIME() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getTIME());
    		writer.value(_OIVPM02_OIVPM02_data.getTIME());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1"); 
    	if (_OIVPM02_OIVPM02_data.getIDEN1() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN1());
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN1());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1"); 
    	if (_OIVPM02_OIVPM02_data.getNAME1() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getNAME1());
    		writer.value(_OIVPM02_OIVPM02_data.getNAME1());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1"); 
    	if (_OIVPM02_OIVPM02_data.getDEPT1() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT1());
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT1());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1"); 
    	if (_OIVPM02_OIVPM02_data.getPHON1() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getPHON1());
    		writer.value(_OIVPM02_OIVPM02_data.getPHON1());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2"); 
    	if (_OIVPM02_OIVPM02_data.getIDEN2() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN2());
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN2());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2"); 
    	if (_OIVPM02_OIVPM02_data.getNAME2() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getNAME2());
    		writer.value(_OIVPM02_OIVPM02_data.getNAME2());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2"); 
    	if (_OIVPM02_OIVPM02_data.getDEPT2() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT2());
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT2());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2"); 
    	if (_OIVPM02_OIVPM02_data.getPHON2() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getPHON2());
    		writer.value(_OIVPM02_OIVPM02_data.getPHON2());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3"); 
    	if (_OIVPM02_OIVPM02_data.getIDEN3() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN3());
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN3());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3"); 
    	if (_OIVPM02_OIVPM02_data.getNAME3() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getNAME3());
    		writer.value(_OIVPM02_OIVPM02_data.getNAME3());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3"); 
    	if (_OIVPM02_OIVPM02_data.getDEPT3() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT3());
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT3());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3"); 
    	if (_OIVPM02_OIVPM02_data.getPHON3() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getPHON3());
    		writer.value(_OIVPM02_OIVPM02_data.getPHON3());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4"); 
    	if (_OIVPM02_OIVPM02_data.getIDEN4() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN4());
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN4());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4"); 
    	if (_OIVPM02_OIVPM02_data.getNAME4() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getNAME4());
    		writer.value(_OIVPM02_OIVPM02_data.getNAME4());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4"); 
    	if (_OIVPM02_OIVPM02_data.getDEPT4() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT4());
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT4());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4"); 
    	if (_OIVPM02_OIVPM02_data.getPHON4() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getPHON4());
    		writer.value(_OIVPM02_OIVPM02_data.getPHON4());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5"); 
    	if (_OIVPM02_OIVPM02_data.getIDEN5() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN5());
    		writer.value(_OIVPM02_OIVPM02_data.getIDEN5());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5"); 
    	if (_OIVPM02_OIVPM02_data.getNAME5() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getNAME5());
    		writer.value(_OIVPM02_OIVPM02_data.getNAME5());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5"); 
    	if (_OIVPM02_OIVPM02_data.getDEPT5() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT5());
    		writer.value(_OIVPM02_OIVPM02_data.getDEPT5());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5"); 
    	if (_OIVPM02_OIVPM02_data.getPHON5() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getPHON5());
    		writer.value(_OIVPM02_OIVPM02_data.getPHON5());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX"); 
    	if (_OIVPM02_OIVPM02_data.getBIDX() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getBIDX());
    		writer.value(_OIVPM02_OIVPM02_data.getBIDX());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX"); 
    	if (_OIVPM02_OIVPM02_data.getFIDX() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getFIDX());
    		writer.value(_OIVPM02_OIVPM02_data.getFIDX());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG"); 
    	if (_OIVPM02_OIVPM02_data.getMSG() != null) {
    		writer.value(_OIVPM02_OIVPM02_data.getMSG());
    		writer.value(_OIVPM02_OIVPM02_data.getMSG());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM02_OIVPM02_data _OIVPM02_OIVPM02_data = new com.tmax.openframe.dto.OIVPM02_OIVPM02_data();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM02_OIVPM02_data();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM02_OIVPM02_data = (com.tmax.openframe.dto.OIVPM02_OIVPM02_data)unmarshal( jr,  _OIVPM02_OIVPM02_data);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPM02_OIVPM02_data;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM02_OIVPM02_data _OIVPM02_OIVPM02_data = (com.tmax.openframe.dto.OIVPM02_OIVPM02_data) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM02_OIVPM02_data();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM02_OIVPM02_data = (com.tmax.openframe.dto.OIVPM02_OIVPM02_data)unmarshal( jr,  _OIVPM02_OIVPM02_data);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPM02_OIVPM02_data;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPM02_OIVPM02_data dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPM02_OIVPM02_data dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DATE" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

