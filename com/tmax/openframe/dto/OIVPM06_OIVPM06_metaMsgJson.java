package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPM06_OIVPM06_metaMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPM06_OIVPM06_meta _OIVPM06_OIVPM06_meta = (com.tmax.openframe.dto.OIVPM06_OIVPM06_meta)obj;
    	
    	if(_OIVPM06_OIVPM06_meta == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPM06_OIVPM06_meta, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPM06_OIVPM06_meta _OIVPM06_OIVPM06_meta, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DATE_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getDATE_length());
    	writer.name("DATE_color"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getDATE_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getDATE_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getTERM_length());
    	writer.name("TERM_color"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getTERM_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getTERM_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getTIME_length());
    	writer.name("TIME_color"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getTIME_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getTIME_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getIDEN_length());
    	writer.name("IDEN_color"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getIDEN_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getIDEN_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getNAME_length());
    	writer.name("NAME_color"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getNAME_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getNAME_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getDEPT_length());
    	writer.name("DEPT_color"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getDEPT_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getDEPT_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getPHON_length());
    	writer.name("PHON_color"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getPHON_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getPHON_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getEMAL_length());
    	writer.name("EMAL_color"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getEMAL_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getEMAL_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getADDR_length());
    	writer.name("ADDR_color"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getADDR_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getADDR_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_length");
    	writer.value(_OIVPM06_OIVPM06_meta.getMSG_length());
    	writer.name("MSG_color"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_color() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_color());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_hilight"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_hilight() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_hilight());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_outline"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_outline() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_outline());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_transp"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_transp() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_transp());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_validn"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_validn() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_validn());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_sosi"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_sosi() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_sosi());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_ps"); 
    	if (_OIVPM06_OIVPM06_meta.getMSG_ps() != null) {
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_ps());
    		writer.value(_OIVPM06_OIVPM06_meta.getMSG_ps());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM06_OIVPM06_meta _OIVPM06_OIVPM06_meta = new com.tmax.openframe.dto.OIVPM06_OIVPM06_meta();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM06_OIVPM06_meta();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM06_OIVPM06_meta = (com.tmax.openframe.dto.OIVPM06_OIVPM06_meta)unmarshal( jr,  _OIVPM06_OIVPM06_meta);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPM06_OIVPM06_meta;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM06_OIVPM06_meta _OIVPM06_OIVPM06_meta = (com.tmax.openframe.dto.OIVPM06_OIVPM06_meta) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM06_OIVPM06_meta();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM06_OIVPM06_meta = (com.tmax.openframe.dto.OIVPM06_OIVPM06_meta)unmarshal( jr,  _OIVPM06_OIVPM06_meta);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPM06_OIVPM06_meta;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPM06_OIVPM06_meta dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPM06_OIVPM06_meta dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DATE_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

