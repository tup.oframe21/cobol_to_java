package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPINFO_DFHCOMMAREADtoMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto _OIVPINFO_DFHCOMMAREADto = (com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto)obj;
    	
    	if(_OIVPINFO_DFHCOMMAREADto == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPINFO_DFHCOMMAREADto, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto _OIVPINFO_DFHCOMMAREADto, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DCA_DATE"); 
    	if (_OIVPINFO_DFHCOMMAREADto.getDCA_DATE() != null) {
    		writer.value(_OIVPINFO_DFHCOMMAREADto.getDCA_DATE());
    		writer.value(_OIVPINFO_DFHCOMMAREADto.getDCA_DATE());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DCA_TERM"); 
    	if (_OIVPINFO_DFHCOMMAREADto.getDCA_TERM() != null) {
    		writer.value(_OIVPINFO_DFHCOMMAREADto.getDCA_TERM());
    		writer.value(_OIVPINFO_DFHCOMMAREADto.getDCA_TERM());
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDtoMsgJson __DCA_TIME = new com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDtoMsgJson();	
    	writer.name("DCA_TIME");
    	if(_OIVPINFO_DFHCOMMAREADto.getDCA_TIME() != null) {
    	writer.beginObject();
    	__DCA_TIME.marshal((com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto)_OIVPINFO_DFHCOMMAREADto.getDCA_TIME(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto _OIVPINFO_DFHCOMMAREADto = new com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPINFO_DFHCOMMAREADto = (com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto)unmarshal( jr,  _OIVPINFO_DFHCOMMAREADto);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPINFO_DFHCOMMAREADto;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto _OIVPINFO_DFHCOMMAREADto = (com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPINFO_DFHCOMMAREADto = (com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto)unmarshal( jr,  _OIVPINFO_DFHCOMMAREADto);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPINFO_DFHCOMMAREADto;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPINFO_DFHCOMMAREADto dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DCA_DATE" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDCA_DATE( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DCA_TERM" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDCA_TERM( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DCA_TIME" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDtoMsgJson __DCA_TIME = new com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDtoMsgJson();
    		
    				com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto ___OIVPINFO_DCA_TIMEDto = new com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto();
    				reader.beginObject();
    				dto.setDCA_TIME((com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto)__DCA_TIME.unmarshal( reader, ___OIVPINFO_DCA_TIMEDto ));
    				reader.endObject();
    			}
    			break;
    		}
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

