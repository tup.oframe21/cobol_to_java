package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPM02_OIVPM02_metaMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPM02_OIVPM02_meta _OIVPM02_OIVPM02_meta = (com.tmax.openframe.dto.OIVPM02_OIVPM02_meta)obj;
    	
    	if(_OIVPM02_OIVPM02_meta == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPM02_OIVPM02_meta, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPM02_OIVPM02_meta _OIVPM02_OIVPM02_meta, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DATE_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getDATE_length());
    	writer.name("DATE_color"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getDATE_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getDATE_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getTERM_length());
    	writer.name("TERM_color"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getTERM_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getTERM_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getTIME_length());
    	writer.name("TIME_color"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getTIME_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getTIME_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_length());
    	writer.name("IDEN1_color"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN1_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN1_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN1_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getNAME1_length());
    	writer.name("NAME1_color"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME1_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME1_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME1_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_length());
    	writer.name("DEPT1_color"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT1_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT1_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT1_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getPHON1_length());
    	writer.name("PHON1_color"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON1_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON1_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON1_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_length());
    	writer.name("IDEN2_color"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN2_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN2_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN2_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getNAME2_length());
    	writer.name("NAME2_color"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME2_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME2_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME2_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_length());
    	writer.name("DEPT2_color"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT2_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT2_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT2_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getPHON2_length());
    	writer.name("PHON2_color"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON2_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON2_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON2_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_length());
    	writer.name("IDEN3_color"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN3_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN3_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN3_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getNAME3_length());
    	writer.name("NAME3_color"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME3_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME3_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME3_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_length());
    	writer.name("DEPT3_color"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT3_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT3_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT3_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getPHON3_length());
    	writer.name("PHON3_color"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON3_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON3_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON3_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_length());
    	writer.name("IDEN4_color"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN4_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN4_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN4_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getNAME4_length());
    	writer.name("NAME4_color"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME4_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME4_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME4_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_length());
    	writer.name("DEPT4_color"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT4_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT4_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT4_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getPHON4_length());
    	writer.name("PHON4_color"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON4_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON4_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON4_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_length());
    	writer.name("IDEN5_color"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN5_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getIDEN5_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getIDEN5_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getNAME5_length());
    	writer.name("NAME5_color"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME5_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getNAME5_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getNAME5_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_length());
    	writer.name("DEPT5_color"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT5_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getDEPT5_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getDEPT5_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getPHON5_length());
    	writer.name("PHON5_color"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON5_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getPHON5_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getPHON5_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getBIDX_length());
    	writer.name("BIDX_color"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("BIDX_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getBIDX_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getBIDX_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getFIDX_length());
    	writer.name("FIDX_color"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("FIDX_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getFIDX_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getFIDX_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_length");
    	writer.value(_OIVPM02_OIVPM02_meta.getMSG_length());
    	writer.name("MSG_color"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_color() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_color());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_hilight"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_hilight() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_hilight());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_outline"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_outline() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_outline());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_transp"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_transp() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_transp());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_validn"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_validn() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_validn());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_sosi"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_sosi() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_sosi());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_ps"); 
    	if (_OIVPM02_OIVPM02_meta.getMSG_ps() != null) {
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_ps());
    		writer.value(_OIVPM02_OIVPM02_meta.getMSG_ps());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM02_OIVPM02_meta _OIVPM02_OIVPM02_meta = new com.tmax.openframe.dto.OIVPM02_OIVPM02_meta();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM02_OIVPM02_meta();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM02_OIVPM02_meta = (com.tmax.openframe.dto.OIVPM02_OIVPM02_meta)unmarshal( jr,  _OIVPM02_OIVPM02_meta);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPM02_OIVPM02_meta;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM02_OIVPM02_meta _OIVPM02_OIVPM02_meta = (com.tmax.openframe.dto.OIVPM02_OIVPM02_meta) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM02_OIVPM02_meta();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM02_OIVPM02_meta = (com.tmax.openframe.dto.OIVPM02_OIVPM02_meta)unmarshal( jr,  _OIVPM02_OIVPM02_meta);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPM02_OIVPM02_meta;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPM02_OIVPM02_meta dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPM02_OIVPM02_meta dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DATE_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN1_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN1_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME1_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME1_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT1_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT1_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON1_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON1_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN2_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN2_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME2_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME2_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT2_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT2_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON2_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON2_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN3_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN3_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME3_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME3_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT3_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT3_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON3_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON3_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN4_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN4_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME4_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME4_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT4_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT4_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON4_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON4_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN5_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN5_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME5_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME5_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT5_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT5_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON5_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON5_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "BIDX_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setBIDX_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "FIDX_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setFIDX_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

