package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPBROS_ScreenDto extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPBROS_ScreenDto";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPBROS_ScreenDto() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : OIVPM02_OIVPM02_data
     * Comments    : 
     */	
    private com.tmax.openframe.dto.OIVPM02_OIVPM02_data OIVPM02_OIVPM02_data = null;
    
    private transient boolean OIVPM02_OIVPM02_data_nullable = true;
    
    public boolean isNullableOIVPM02_OIVPM02_data() {
    	return this.OIVPM02_OIVPM02_data_nullable;
    }
    
    private transient boolean OIVPM02_OIVPM02_data_invalidation = false;
    	
    public void setInvalidationOIVPM02_OIVPM02_data(boolean invalidation) { 
    	this.OIVPM02_OIVPM02_data_invalidation = invalidation;
    }
    	
    public boolean isInvalidationOIVPM02_OIVPM02_data() {
    	return this.OIVPM02_OIVPM02_data_invalidation;
    }
    	
    private transient boolean OIVPM02_OIVPM02_data_modified = false;
    
    public boolean isModifiedOIVPM02_OIVPM02_data() {
    	if(this.OIVPM02_OIVPM02_data_modified) return true;
    	if(OIVPM02_OIVPM02_data.isModified()) return true;
    	return false;
    }
    	
    public void unModifiedOIVPM02_OIVPM02_data() {
    	this.OIVPM02_OIVPM02_data_modified = false;
    }
    public FieldProperty getOIVPM02_OIVPM02_dataFieldProperty() {
    	return fieldPropertyMap.get("OIVPM02_OIVPM02_data");
    }
    
    public com.tmax.openframe.dto.OIVPM02_OIVPM02_data getOIVPM02_OIVPM02_data() {
    	return OIVPM02_OIVPM02_data;
    }	
     	public void setOIVPM02_OIVPM02_data(com.tmax.openframe.dto.OIVPM02_OIVPM02_data OIVPM02_OIVPM02_data) {
    	if(OIVPM02_OIVPM02_data == null) {
    		this.OIVPM02_OIVPM02_data = null;
    	} else {
    		this.OIVPM02_OIVPM02_data = OIVPM02_OIVPM02_data;
    	}
    	this.OIVPM02_OIVPM02_data_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : OIVPM02_OIVPM02_meta
     * Comments    : 
     */	
    private com.tmax.openframe.dto.OIVPM02_OIVPM02_meta OIVPM02_OIVPM02_meta = null;
    
    private transient boolean OIVPM02_OIVPM02_meta_nullable = true;
    
    public boolean isNullableOIVPM02_OIVPM02_meta() {
    	return this.OIVPM02_OIVPM02_meta_nullable;
    }
    
    private transient boolean OIVPM02_OIVPM02_meta_invalidation = false;
    	
    public void setInvalidationOIVPM02_OIVPM02_meta(boolean invalidation) { 
    	this.OIVPM02_OIVPM02_meta_invalidation = invalidation;
    }
    	
    public boolean isInvalidationOIVPM02_OIVPM02_meta() {
    	return this.OIVPM02_OIVPM02_meta_invalidation;
    }
    	
    private transient boolean OIVPM02_OIVPM02_meta_modified = false;
    
    public boolean isModifiedOIVPM02_OIVPM02_meta() {
    	if(this.OIVPM02_OIVPM02_meta_modified) return true;
    	if(OIVPM02_OIVPM02_meta.isModified()) return true;
    	return false;
    }
    	
    public void unModifiedOIVPM02_OIVPM02_meta() {
    	this.OIVPM02_OIVPM02_meta_modified = false;
    }
    public FieldProperty getOIVPM02_OIVPM02_metaFieldProperty() {
    	return fieldPropertyMap.get("OIVPM02_OIVPM02_meta");
    }
    
    public com.tmax.openframe.dto.OIVPM02_OIVPM02_meta getOIVPM02_OIVPM02_meta() {
    	return OIVPM02_OIVPM02_meta;
    }	
     	public void setOIVPM02_OIVPM02_meta(com.tmax.openframe.dto.OIVPM02_OIVPM02_meta OIVPM02_OIVPM02_meta) {
    	if(OIVPM02_OIVPM02_meta == null) {
    		this.OIVPM02_OIVPM02_meta = null;
    	} else {
    		this.OIVPM02_OIVPM02_meta = OIVPM02_OIVPM02_meta;
    	}
    	this.OIVPM02_OIVPM02_meta_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : mapName
     * Comments    : 
     */	
    private String mapName = null;
    
    private transient boolean mapName_nullable = true;
    
    public boolean isNullableMapName() {
    	return this.mapName_nullable;
    }
    
    private transient boolean mapName_invalidation = false;
    	
    public void setInvalidationMapName(boolean invalidation) { 
    	this.mapName_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMapName() {
    	return this.mapName_invalidation;
    }
    	
    private transient boolean mapName_modified = false;
    
    public boolean isModifiedMapName() {
    	return this.mapName_modified;
    }
    	
    public void unModifiedMapName() {
    	this.mapName_modified = false;
    }
    public FieldProperty getMapNameFieldProperty() {
    	return fieldPropertyMap.get("mapName");
    }
    
    public String getMapName() {
    	return mapName;
    }	
    public String getNvlMapName() {
    	if(getMapName() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMapName();
    	}
    }
    public void setMapName(String mapName) {
    	if(mapName == null) {
    		this.mapName = null;
    	} else {
    		this.mapName = mapName;
    	}
    	this.mapName_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : mapSetName
     * Comments    : 
     */	
    private String mapSetName = null;
    
    private transient boolean mapSetName_nullable = true;
    
    public boolean isNullableMapSetName() {
    	return this.mapSetName_nullable;
    }
    
    private transient boolean mapSetName_invalidation = false;
    	
    public void setInvalidationMapSetName(boolean invalidation) { 
    	this.mapSetName_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMapSetName() {
    	return this.mapSetName_invalidation;
    }
    	
    private transient boolean mapSetName_modified = false;
    
    public boolean isModifiedMapSetName() {
    	return this.mapSetName_modified;
    }
    	
    public void unModifiedMapSetName() {
    	this.mapSetName_modified = false;
    }
    public FieldProperty getMapSetNameFieldProperty() {
    	return fieldPropertyMap.get("mapSetName");
    }
    
    public String getMapSetName() {
    	return mapSetName;
    }	
    public String getNvlMapSetName() {
    	if(getMapSetName() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMapSetName();
    	}
    }
    public void setMapSetName(String mapSetName) {
    	if(mapSetName == null) {
    		this.mapSetName = null;
    	} else {
    		this.mapSetName = mapSetName;
    	}
    	this.mapSetName_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : text
     * Comments    : 
     */	
    private String text = null;
    
    private transient boolean text_nullable = true;
    
    public boolean isNullableText() {
    	return this.text_nullable;
    }
    
    private transient boolean text_invalidation = false;
    	
    public void setInvalidationText(boolean invalidation) { 
    	this.text_invalidation = invalidation;
    }
    	
    public boolean isInvalidationText() {
    	return this.text_invalidation;
    }
    	
    private transient boolean text_modified = false;
    
    public boolean isModifiedText() {
    	return this.text_modified;
    }
    	
    public void unModifiedText() {
    	this.text_modified = false;
    }
    public FieldProperty getTextFieldProperty() {
    	return fieldPropertyMap.get("text");
    }
    
    public String getText() {
    	return text;
    }	
    public String getNvlText() {
    	if(getText() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getText();
    	}
    }
    public void setText(String text) {
    	if(text == null) {
    		this.text = null;
    	} else {
    		this.text = text;
    	}
    	this.text_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.OIVPM02_OIVPM02_data_modified = false;
    	if(this.OIVPM02_OIVPM02_data != null)
    		this.OIVPM02_OIVPM02_data.clearAllIsModified();
    	this.OIVPM02_OIVPM02_meta_modified = false;
    	if(this.OIVPM02_OIVPM02_meta != null)
    		this.OIVPM02_OIVPM02_meta.clearAllIsModified();
    	this.mapName_modified = false;
    	this.mapSetName_modified = false;
    	this.text_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.OIVPM02_OIVPM02_data_modified == true)
    		modifiedFields.add("OIVPM02_OIVPM02_data");
    	if(this.OIVPM02_OIVPM02_meta_modified == true)
    		modifiedFields.add("OIVPM02_OIVPM02_meta");
    	if(this.mapName_modified == true)
    		modifiedFields.add("mapName");
    	if(this.mapSetName_modified == true)
    		modifiedFields.add("mapSetName");
    	if(this.text_modified == true)
    		modifiedFields.add("text");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPBROS_ScreenDto copyObj = new OIVPBROS_ScreenDto();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPBROS_ScreenDto) {
    	if(this == _oIVPBROS_ScreenDto) return;
    	OIVPBROS_ScreenDto __oIVPBROS_ScreenDto = (OIVPBROS_ScreenDto) _oIVPBROS_ScreenDto;
    	com.tmax.openframe.dto.OIVPM02_OIVPM02_data _value0 = __oIVPBROS_ScreenDto.getOIVPM02_OIVPM02_data();
    		if(_value0 == null) {
    			this.setOIVPM02_OIVPM02_data(null);
    		} else {
    			this.setOIVPM02_OIVPM02_data((com.tmax.openframe.dto.OIVPM02_OIVPM02_data)_value0.clone());
    		}
    	com.tmax.openframe.dto.OIVPM02_OIVPM02_meta _value1 = __oIVPBROS_ScreenDto.getOIVPM02_OIVPM02_meta();
    		if(_value1 == null) {
    			this.setOIVPM02_OIVPM02_meta(null);
    		} else {
    			this.setOIVPM02_OIVPM02_meta((com.tmax.openframe.dto.OIVPM02_OIVPM02_meta)_value1.clone());
    		}
    	this.setMapName(__oIVPBROS_ScreenDto.getMapName());
    	this.setMapSetName(__oIVPBROS_ScreenDto.getMapSetName());
    	this.setText(__oIVPBROS_ScreenDto.getText());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("OIVPM02_OIVPM02_data : ").append(OIVPM02_OIVPM02_data).append("\n");	
    	buffer.append("OIVPM02_OIVPM02_meta : ").append(OIVPM02_OIVPM02_meta).append("\n");	
    	buffer.append("mapName : ").append(mapName).append("\n");	
    	buffer.append("mapSetName : ").append(mapSetName).append("\n");	
    	buffer.append("text : ").append(text).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(5);
    	fieldPropertyMap.put("OIVPM02_OIVPM02_data", FieldProperty.builder()
    	              .setPhysicalName("OIVPM02_OIVPM02_data")
    	              .setLogicalName("OIVPM02_OIVPM02_data")
    	              .setType(FieldProperty.TYPE_ABSTRACT_INCLUDE)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("com.tmax.openframe.dto.OIVPM02_OIVPM02_data")
    	              .build());
    	fieldPropertyMap.put("OIVPM02_OIVPM02_meta", FieldProperty.builder()
    	              .setPhysicalName("OIVPM02_OIVPM02_meta")
    	              .setLogicalName("OIVPM02_OIVPM02_meta")
    	              .setType(FieldProperty.TYPE_ABSTRACT_INCLUDE)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("com.tmax.openframe.dto.OIVPM02_OIVPM02_meta")
    	              .build());
    	fieldPropertyMap.put("mapName", FieldProperty.builder()
    	              .setPhysicalName("mapName")
    	              .setLogicalName("mapName")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("mapSetName", FieldProperty.builder()
    	              .setPhysicalName("mapSetName")
    	              .setLogicalName("mapSetName")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("text", FieldProperty.builder()
    	              .setPhysicalName("text")
    	              .setLogicalName("text")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "OIVPM02_OIVPM02_data" : return getOIVPM02_OIVPM02_data();
    		case "OIVPM02_OIVPM02_meta" : return getOIVPM02_OIVPM02_meta();
    		case "mapName" : return getMapName();
    		case "mapSetName" : return getMapSetName();
    		case "text" : return getText();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "OIVPM02_OIVPM02_data" : setOIVPM02_OIVPM02_data((com.tmax.openframe.dto.OIVPM02_OIVPM02_data)arg); break;
    		case "OIVPM02_OIVPM02_meta" : setOIVPM02_OIVPM02_meta((com.tmax.openframe.dto.OIVPM02_OIVPM02_meta)arg); break;
    		case "mapName" : setMapName((String)arg); break;
    		case "mapSetName" : setMapSetName((String)arg); break;
    		case "text" : setText((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPBROS_ScreenDto _OIVPBROS_ScreenDto = (OIVPBROS_ScreenDto) obj;
    	if(this.OIVPM02_OIVPM02_data == null) {
    	if(_OIVPBROS_ScreenDto.getOIVPM02_OIVPM02_data() != null)
    		return false;
    	} else if(!this.OIVPM02_OIVPM02_data.equals(_OIVPBROS_ScreenDto.getOIVPM02_OIVPM02_data()))
    		return false;
    	if(this.OIVPM02_OIVPM02_meta == null) {
    	if(_OIVPBROS_ScreenDto.getOIVPM02_OIVPM02_meta() != null)
    		return false;
    	} else if(!this.OIVPM02_OIVPM02_meta.equals(_OIVPBROS_ScreenDto.getOIVPM02_OIVPM02_meta()))
    		return false;
    	if(this.mapName == null) {
    	if(_OIVPBROS_ScreenDto.getMapName() != null)
    		return false;
    	} else if(!this.mapName.equals(_OIVPBROS_ScreenDto.getMapName()))
    		return false;
    	if(this.mapSetName == null) {
    	if(_OIVPBROS_ScreenDto.getMapSetName() != null)
    		return false;
    	} else if(!this.mapSetName.equals(_OIVPBROS_ScreenDto.getMapSetName()))
    		return false;
    	if(this.text == null) {
    	if(_OIVPBROS_ScreenDto.getText() != null)
    		return false;
    	} else if(!this.text.equals(_OIVPBROS_ScreenDto.getText()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.OIVPM02_OIVPM02_data == null) ? 0 : this.OIVPM02_OIVPM02_data.hashCode());
    	result = prime * result + ((this.OIVPM02_OIVPM02_meta == null) ? 0 : this.OIVPM02_OIVPM02_meta.hashCode());
    	result = prime * result + ((this.mapName == null) ? 0 : this.mapName.hashCode());
    	result = prime * result + ((this.mapSetName == null) ? 0 : this.mapSetName.hashCode());
    	result = prime * result + ((this.text == null) ? 0 : this.text.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	OIVPM02_OIVPM02_data = null;
    	OIVPM02_OIVPM02_meta = null;
    	mapName = null;
    	mapSetName = null;
    	text = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

