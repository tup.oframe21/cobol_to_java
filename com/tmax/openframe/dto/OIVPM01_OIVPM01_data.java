package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM01_OIVPM01_data extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM01_OIVPM01_data";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM01_OIVPM01_data() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE
     * Comments    : 
     */	
    private String DATE = null;
    
    private transient boolean DATE_nullable = true;
    
    public boolean isNullableDATE() {
    	return this.DATE_nullable;
    }
    
    private transient boolean DATE_invalidation = false;
    	
    public void setInvalidationDATE(boolean invalidation) { 
    	this.DATE_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE() {
    	return this.DATE_invalidation;
    }
    	
    private transient boolean DATE_modified = false;
    
    public boolean isModifiedDATE() {
    	return this.DATE_modified;
    }
    	
    public void unModifiedDATE() {
    	this.DATE_modified = false;
    }
    public FieldProperty getDATEFieldProperty() {
    	return fieldPropertyMap.get("DATE");
    }
    
    public String getDATE() {
    	return DATE;
    }	
    public String getNvlDATE() {
    	if(getDATE() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE();
    	}
    }
    public void setDATE(String DATE) {
    	if(DATE == null) {
    		this.DATE = null;
    	} else {
    		this.DATE = DATE;
    	}
    	this.DATE_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM
     * Comments    : 
     */	
    private String TERM = null;
    
    private transient boolean TERM_nullable = true;
    
    public boolean isNullableTERM() {
    	return this.TERM_nullable;
    }
    
    private transient boolean TERM_invalidation = false;
    	
    public void setInvalidationTERM(boolean invalidation) { 
    	this.TERM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM() {
    	return this.TERM_invalidation;
    }
    	
    private transient boolean TERM_modified = false;
    
    public boolean isModifiedTERM() {
    	return this.TERM_modified;
    }
    	
    public void unModifiedTERM() {
    	this.TERM_modified = false;
    }
    public FieldProperty getTERMFieldProperty() {
    	return fieldPropertyMap.get("TERM");
    }
    
    public String getTERM() {
    	return TERM;
    }	
    public String getNvlTERM() {
    	if(getTERM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM();
    	}
    }
    public void setTERM(String TERM) {
    	if(TERM == null) {
    		this.TERM = null;
    	} else {
    		this.TERM = TERM;
    	}
    	this.TERM_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME
     * Comments    : 
     */	
    private String TIME = null;
    
    private transient boolean TIME_nullable = true;
    
    public boolean isNullableTIME() {
    	return this.TIME_nullable;
    }
    
    private transient boolean TIME_invalidation = false;
    	
    public void setInvalidationTIME(boolean invalidation) { 
    	this.TIME_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME() {
    	return this.TIME_invalidation;
    }
    	
    private transient boolean TIME_modified = false;
    
    public boolean isModifiedTIME() {
    	return this.TIME_modified;
    }
    	
    public void unModifiedTIME() {
    	this.TIME_modified = false;
    }
    public FieldProperty getTIMEFieldProperty() {
    	return fieldPropertyMap.get("TIME");
    }
    
    public String getTIME() {
    	return TIME;
    }	
    public String getNvlTIME() {
    	if(getTIME() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME();
    	}
    }
    public void setTIME(String TIME) {
    	if(TIME == null) {
    		this.TIME = null;
    	} else {
    		this.TIME = TIME;
    	}
    	this.TIME_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE
     * Comments    : 
     */	
    private String CODE = null;
    
    private transient boolean CODE_nullable = true;
    
    public boolean isNullableCODE() {
    	return this.CODE_nullable;
    }
    
    private transient boolean CODE_invalidation = false;
    	
    public void setInvalidationCODE(boolean invalidation) { 
    	this.CODE_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE() {
    	return this.CODE_invalidation;
    }
    	
    private transient boolean CODE_modified = false;
    
    public boolean isModifiedCODE() {
    	return this.CODE_modified;
    }
    	
    public void unModifiedCODE() {
    	this.CODE_modified = false;
    }
    public FieldProperty getCODEFieldProperty() {
    	return fieldPropertyMap.get("CODE");
    }
    
    public String getCODE() {
    	return CODE;
    }	
    public String getNvlCODE() {
    	if(getCODE() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE();
    	}
    }
    public void setCODE(String CODE) {
    	if(CODE == null) {
    		this.CODE = null;
    	} else {
    		this.CODE = CODE;
    	}
    	this.CODE_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN
     * Comments    : 
     */	
    private String IDEN = null;
    
    private transient boolean IDEN_nullable = true;
    
    public boolean isNullableIDEN() {
    	return this.IDEN_nullable;
    }
    
    private transient boolean IDEN_invalidation = false;
    	
    public void setInvalidationIDEN(boolean invalidation) { 
    	this.IDEN_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN() {
    	return this.IDEN_invalidation;
    }
    	
    private transient boolean IDEN_modified = false;
    
    public boolean isModifiedIDEN() {
    	return this.IDEN_modified;
    }
    	
    public void unModifiedIDEN() {
    	this.IDEN_modified = false;
    }
    public FieldProperty getIDENFieldProperty() {
    	return fieldPropertyMap.get("IDEN");
    }
    
    public String getIDEN() {
    	return IDEN;
    }	
    public String getNvlIDEN() {
    	if(getIDEN() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN();
    	}
    }
    public void setIDEN(String IDEN) {
    	if(IDEN == null) {
    		this.IDEN = null;
    	} else {
    		this.IDEN = IDEN;
    	}
    	this.IDEN_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG
     * Comments    : 
     */	
    private String MSG = null;
    
    private transient boolean MSG_nullable = true;
    
    public boolean isNullableMSG() {
    	return this.MSG_nullable;
    }
    
    private transient boolean MSG_invalidation = false;
    	
    public void setInvalidationMSG(boolean invalidation) { 
    	this.MSG_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG() {
    	return this.MSG_invalidation;
    }
    	
    private transient boolean MSG_modified = false;
    
    public boolean isModifiedMSG() {
    	return this.MSG_modified;
    }
    	
    public void unModifiedMSG() {
    	this.MSG_modified = false;
    }
    public FieldProperty getMSGFieldProperty() {
    	return fieldPropertyMap.get("MSG");
    }
    
    public String getMSG() {
    	return MSG;
    }	
    public String getNvlMSG() {
    	if(getMSG() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG();
    	}
    }
    public void setMSG(String MSG) {
    	if(MSG == null) {
    		this.MSG = null;
    	} else {
    		this.MSG = MSG;
    	}
    	this.MSG_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_modified = false;
    	this.TERM_modified = false;
    	this.TIME_modified = false;
    	this.CODE_modified = false;
    	this.IDEN_modified = false;
    	this.MSG_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_modified == true)
    		modifiedFields.add("DATE");
    	if(this.TERM_modified == true)
    		modifiedFields.add("TERM");
    	if(this.TIME_modified == true)
    		modifiedFields.add("TIME");
    	if(this.CODE_modified == true)
    		modifiedFields.add("CODE");
    	if(this.IDEN_modified == true)
    		modifiedFields.add("IDEN");
    	if(this.MSG_modified == true)
    		modifiedFields.add("MSG");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM01_OIVPM01_data copyObj = new OIVPM01_OIVPM01_data();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM01_OIVPM01_data) {
    	if(this == _oIVPM01_OIVPM01_data) return;
    	OIVPM01_OIVPM01_data __oIVPM01_OIVPM01_data = (OIVPM01_OIVPM01_data) _oIVPM01_OIVPM01_data;
    	this.setDATE(__oIVPM01_OIVPM01_data.getDATE());
    	this.setTERM(__oIVPM01_OIVPM01_data.getTERM());
    	this.setTIME(__oIVPM01_OIVPM01_data.getTIME());
    	this.setCODE(__oIVPM01_OIVPM01_data.getCODE());
    	this.setIDEN(__oIVPM01_OIVPM01_data.getIDEN());
    	this.setMSG(__oIVPM01_OIVPM01_data.getMSG());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE : ").append(DATE).append("\n");	
    	buffer.append("TERM : ").append(TERM).append("\n");	
    	buffer.append("TIME : ").append(TIME).append("\n");	
    	buffer.append("CODE : ").append(CODE).append("\n");	
    	buffer.append("IDEN : ").append(IDEN).append("\n");	
    	buffer.append("MSG : ").append(MSG).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(6);
    	fieldPropertyMap.put("DATE", FieldProperty.builder()
    	              .setPhysicalName("DATE")
    	              .setLogicalName("DATE")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM", FieldProperty.builder()
    	              .setPhysicalName("TERM")
    	              .setLogicalName("TERM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(4)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME", FieldProperty.builder()
    	              .setPhysicalName("TIME")
    	              .setLogicalName("TIME")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE", FieldProperty.builder()
    	              .setPhysicalName("CODE")
    	              .setLogicalName("CODE")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(4)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN", FieldProperty.builder()
    	              .setPhysicalName("IDEN")
    	              .setLogicalName("IDEN")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG", FieldProperty.builder()
    	              .setPhysicalName("MSG")
    	              .setLogicalName("MSG")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(76)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : return getDATE();
    		case "TERM" : return getTERM();
    		case "TIME" : return getTIME();
    		case "CODE" : return getCODE();
    		case "IDEN" : return getIDEN();
    		case "MSG" : return getMSG();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : setDATE((String)arg); break;
    		case "TERM" : setTERM((String)arg); break;
    		case "TIME" : setTIME((String)arg); break;
    		case "CODE" : setCODE((String)arg); break;
    		case "IDEN" : setIDEN((String)arg); break;
    		case "MSG" : setMSG((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM01_OIVPM01_data _OIVPM01_OIVPM01_data = (OIVPM01_OIVPM01_data) obj;
    	if(this.DATE == null) {
    	if(_OIVPM01_OIVPM01_data.getDATE() != null)
    		return false;
    	} else if(!this.DATE.equals(_OIVPM01_OIVPM01_data.getDATE()))
    		return false;
    	if(this.TERM == null) {
    	if(_OIVPM01_OIVPM01_data.getTERM() != null)
    		return false;
    	} else if(!this.TERM.equals(_OIVPM01_OIVPM01_data.getTERM()))
    		return false;
    	if(this.TIME == null) {
    	if(_OIVPM01_OIVPM01_data.getTIME() != null)
    		return false;
    	} else if(!this.TIME.equals(_OIVPM01_OIVPM01_data.getTIME()))
    		return false;
    	if(this.CODE == null) {
    	if(_OIVPM01_OIVPM01_data.getCODE() != null)
    		return false;
    	} else if(!this.CODE.equals(_OIVPM01_OIVPM01_data.getCODE()))
    		return false;
    	if(this.IDEN == null) {
    	if(_OIVPM01_OIVPM01_data.getIDEN() != null)
    		return false;
    	} else if(!this.IDEN.equals(_OIVPM01_OIVPM01_data.getIDEN()))
    		return false;
    	if(this.MSG == null) {
    	if(_OIVPM01_OIVPM01_data.getMSG() != null)
    		return false;
    	} else if(!this.MSG.equals(_OIVPM01_OIVPM01_data.getMSG()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DATE == null) ? 0 : this.DATE.hashCode());
    	result = prime * result + ((this.TERM == null) ? 0 : this.TERM.hashCode());
    	result = prime * result + ((this.TIME == null) ? 0 : this.TIME.hashCode());
    	result = prime * result + ((this.CODE == null) ? 0 : this.CODE.hashCode());
    	result = prime * result + ((this.IDEN == null) ? 0 : this.IDEN.hashCode());
    	result = prime * result + ((this.MSG == null) ? 0 : this.MSG.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE = null;
    	TERM = null;
    	TIME = null;
    	CODE = null;
    	IDEN = null;
    	MSG = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

