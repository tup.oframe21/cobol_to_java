package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM03_OIVPM03_data extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM03_OIVPM03_data";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM03_OIVPM03_data() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE
     * Comments    : 
     */	
    private String DATE = null;
    
    private transient boolean DATE_nullable = true;
    
    public boolean isNullableDATE() {
    	return this.DATE_nullable;
    }
    
    private transient boolean DATE_invalidation = false;
    	
    public void setInvalidationDATE(boolean invalidation) { 
    	this.DATE_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE() {
    	return this.DATE_invalidation;
    }
    	
    private transient boolean DATE_modified = false;
    
    public boolean isModifiedDATE() {
    	return this.DATE_modified;
    }
    	
    public void unModifiedDATE() {
    	this.DATE_modified = false;
    }
    public FieldProperty getDATEFieldProperty() {
    	return fieldPropertyMap.get("DATE");
    }
    
    public String getDATE() {
    	return DATE;
    }	
    public String getNvlDATE() {
    	if(getDATE() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE();
    	}
    }
    public void setDATE(String DATE) {
    	if(DATE == null) {
    		this.DATE = null;
    	} else {
    		this.DATE = DATE;
    	}
    	this.DATE_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM
     * Comments    : 
     */	
    private String TERM = null;
    
    private transient boolean TERM_nullable = true;
    
    public boolean isNullableTERM() {
    	return this.TERM_nullable;
    }
    
    private transient boolean TERM_invalidation = false;
    	
    public void setInvalidationTERM(boolean invalidation) { 
    	this.TERM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM() {
    	return this.TERM_invalidation;
    }
    	
    private transient boolean TERM_modified = false;
    
    public boolean isModifiedTERM() {
    	return this.TERM_modified;
    }
    	
    public void unModifiedTERM() {
    	this.TERM_modified = false;
    }
    public FieldProperty getTERMFieldProperty() {
    	return fieldPropertyMap.get("TERM");
    }
    
    public String getTERM() {
    	return TERM;
    }	
    public String getNvlTERM() {
    	if(getTERM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM();
    	}
    }
    public void setTERM(String TERM) {
    	if(TERM == null) {
    		this.TERM = null;
    	} else {
    		this.TERM = TERM;
    	}
    	this.TERM_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME
     * Comments    : 
     */	
    private String TIME = null;
    
    private transient boolean TIME_nullable = true;
    
    public boolean isNullableTIME() {
    	return this.TIME_nullable;
    }
    
    private transient boolean TIME_invalidation = false;
    	
    public void setInvalidationTIME(boolean invalidation) { 
    	this.TIME_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME() {
    	return this.TIME_invalidation;
    }
    	
    private transient boolean TIME_modified = false;
    
    public boolean isModifiedTIME() {
    	return this.TIME_modified;
    }
    	
    public void unModifiedTIME() {
    	this.TIME_modified = false;
    }
    public FieldProperty getTIMEFieldProperty() {
    	return fieldPropertyMap.get("TIME");
    }
    
    public String getTIME() {
    	return TIME;
    }	
    public String getNvlTIME() {
    	if(getTIME() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME();
    	}
    }
    public void setTIME(String TIME) {
    	if(TIME == null) {
    		this.TIME = null;
    	} else {
    		this.TIME = TIME;
    	}
    	this.TIME_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN
     * Comments    : 
     */	
    private String IDEN = null;
    
    private transient boolean IDEN_nullable = true;
    
    public boolean isNullableIDEN() {
    	return this.IDEN_nullable;
    }
    
    private transient boolean IDEN_invalidation = false;
    	
    public void setInvalidationIDEN(boolean invalidation) { 
    	this.IDEN_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN() {
    	return this.IDEN_invalidation;
    }
    	
    private transient boolean IDEN_modified = false;
    
    public boolean isModifiedIDEN() {
    	return this.IDEN_modified;
    }
    	
    public void unModifiedIDEN() {
    	this.IDEN_modified = false;
    }
    public FieldProperty getIDENFieldProperty() {
    	return fieldPropertyMap.get("IDEN");
    }
    
    public String getIDEN() {
    	return IDEN;
    }	
    public String getNvlIDEN() {
    	if(getIDEN() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN();
    	}
    }
    public void setIDEN(String IDEN) {
    	if(IDEN == null) {
    		this.IDEN = null;
    	} else {
    		this.IDEN = IDEN;
    	}
    	this.IDEN_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME
     * Comments    : 
     */	
    private String NAME = null;
    
    private transient boolean NAME_nullable = true;
    
    public boolean isNullableNAME() {
    	return this.NAME_nullable;
    }
    
    private transient boolean NAME_invalidation = false;
    	
    public void setInvalidationNAME(boolean invalidation) { 
    	this.NAME_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME() {
    	return this.NAME_invalidation;
    }
    	
    private transient boolean NAME_modified = false;
    
    public boolean isModifiedNAME() {
    	return this.NAME_modified;
    }
    	
    public void unModifiedNAME() {
    	this.NAME_modified = false;
    }
    public FieldProperty getNAMEFieldProperty() {
    	return fieldPropertyMap.get("NAME");
    }
    
    public String getNAME() {
    	return NAME;
    }	
    public String getNvlNAME() {
    	if(getNAME() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME();
    	}
    }
    public void setNAME(String NAME) {
    	if(NAME == null) {
    		this.NAME = null;
    	} else {
    		this.NAME = NAME;
    	}
    	this.NAME_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT
     * Comments    : 
     */	
    private String DEPT = null;
    
    private transient boolean DEPT_nullable = true;
    
    public boolean isNullableDEPT() {
    	return this.DEPT_nullable;
    }
    
    private transient boolean DEPT_invalidation = false;
    	
    public void setInvalidationDEPT(boolean invalidation) { 
    	this.DEPT_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT() {
    	return this.DEPT_invalidation;
    }
    	
    private transient boolean DEPT_modified = false;
    
    public boolean isModifiedDEPT() {
    	return this.DEPT_modified;
    }
    	
    public void unModifiedDEPT() {
    	this.DEPT_modified = false;
    }
    public FieldProperty getDEPTFieldProperty() {
    	return fieldPropertyMap.get("DEPT");
    }
    
    public String getDEPT() {
    	return DEPT;
    }	
    public String getNvlDEPT() {
    	if(getDEPT() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT();
    	}
    }
    public void setDEPT(String DEPT) {
    	if(DEPT == null) {
    		this.DEPT = null;
    	} else {
    		this.DEPT = DEPT;
    	}
    	this.DEPT_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON
     * Comments    : 
     */	
    private String PHON = null;
    
    private transient boolean PHON_nullable = true;
    
    public boolean isNullablePHON() {
    	return this.PHON_nullable;
    }
    
    private transient boolean PHON_invalidation = false;
    	
    public void setInvalidationPHON(boolean invalidation) { 
    	this.PHON_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON() {
    	return this.PHON_invalidation;
    }
    	
    private transient boolean PHON_modified = false;
    
    public boolean isModifiedPHON() {
    	return this.PHON_modified;
    }
    	
    public void unModifiedPHON() {
    	this.PHON_modified = false;
    }
    public FieldProperty getPHONFieldProperty() {
    	return fieldPropertyMap.get("PHON");
    }
    
    public String getPHON() {
    	return PHON;
    }	
    public String getNvlPHON() {
    	if(getPHON() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON();
    	}
    }
    public void setPHON(String PHON) {
    	if(PHON == null) {
    		this.PHON = null;
    	} else {
    		this.PHON = PHON;
    	}
    	this.PHON_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : EMAL
     * Comments    : 
     */	
    private String EMAL = null;
    
    private transient boolean EMAL_nullable = true;
    
    public boolean isNullableEMAL() {
    	return this.EMAL_nullable;
    }
    
    private transient boolean EMAL_invalidation = false;
    	
    public void setInvalidationEMAL(boolean invalidation) { 
    	this.EMAL_invalidation = invalidation;
    }
    	
    public boolean isInvalidationEMAL() {
    	return this.EMAL_invalidation;
    }
    	
    private transient boolean EMAL_modified = false;
    
    public boolean isModifiedEMAL() {
    	return this.EMAL_modified;
    }
    	
    public void unModifiedEMAL() {
    	this.EMAL_modified = false;
    }
    public FieldProperty getEMALFieldProperty() {
    	return fieldPropertyMap.get("EMAL");
    }
    
    public String getEMAL() {
    	return EMAL;
    }	
    public String getNvlEMAL() {
    	if(getEMAL() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getEMAL();
    	}
    }
    public void setEMAL(String EMAL) {
    	if(EMAL == null) {
    		this.EMAL = null;
    	} else {
    		this.EMAL = EMAL;
    	}
    	this.EMAL_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ADDR
     * Comments    : 
     */	
    private String ADDR = null;
    
    private transient boolean ADDR_nullable = true;
    
    public boolean isNullableADDR() {
    	return this.ADDR_nullable;
    }
    
    private transient boolean ADDR_invalidation = false;
    	
    public void setInvalidationADDR(boolean invalidation) { 
    	this.ADDR_invalidation = invalidation;
    }
    	
    public boolean isInvalidationADDR() {
    	return this.ADDR_invalidation;
    }
    	
    private transient boolean ADDR_modified = false;
    
    public boolean isModifiedADDR() {
    	return this.ADDR_modified;
    }
    	
    public void unModifiedADDR() {
    	this.ADDR_modified = false;
    }
    public FieldProperty getADDRFieldProperty() {
    	return fieldPropertyMap.get("ADDR");
    }
    
    public String getADDR() {
    	return ADDR;
    }	
    public String getNvlADDR() {
    	if(getADDR() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getADDR();
    	}
    }
    public void setADDR(String ADDR) {
    	if(ADDR == null) {
    		this.ADDR = null;
    	} else {
    		this.ADDR = ADDR;
    	}
    	this.ADDR_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG
     * Comments    : 
     */	
    private String MSG = null;
    
    private transient boolean MSG_nullable = true;
    
    public boolean isNullableMSG() {
    	return this.MSG_nullable;
    }
    
    private transient boolean MSG_invalidation = false;
    	
    public void setInvalidationMSG(boolean invalidation) { 
    	this.MSG_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG() {
    	return this.MSG_invalidation;
    }
    	
    private transient boolean MSG_modified = false;
    
    public boolean isModifiedMSG() {
    	return this.MSG_modified;
    }
    	
    public void unModifiedMSG() {
    	this.MSG_modified = false;
    }
    public FieldProperty getMSGFieldProperty() {
    	return fieldPropertyMap.get("MSG");
    }
    
    public String getMSG() {
    	return MSG;
    }	
    public String getNvlMSG() {
    	if(getMSG() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG();
    	}
    }
    public void setMSG(String MSG) {
    	if(MSG == null) {
    		this.MSG = null;
    	} else {
    		this.MSG = MSG;
    	}
    	this.MSG_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_modified = false;
    	this.TERM_modified = false;
    	this.TIME_modified = false;
    	this.IDEN_modified = false;
    	this.NAME_modified = false;
    	this.DEPT_modified = false;
    	this.PHON_modified = false;
    	this.EMAL_modified = false;
    	this.ADDR_modified = false;
    	this.MSG_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_modified == true)
    		modifiedFields.add("DATE");
    	if(this.TERM_modified == true)
    		modifiedFields.add("TERM");
    	if(this.TIME_modified == true)
    		modifiedFields.add("TIME");
    	if(this.IDEN_modified == true)
    		modifiedFields.add("IDEN");
    	if(this.NAME_modified == true)
    		modifiedFields.add("NAME");
    	if(this.DEPT_modified == true)
    		modifiedFields.add("DEPT");
    	if(this.PHON_modified == true)
    		modifiedFields.add("PHON");
    	if(this.EMAL_modified == true)
    		modifiedFields.add("EMAL");
    	if(this.ADDR_modified == true)
    		modifiedFields.add("ADDR");
    	if(this.MSG_modified == true)
    		modifiedFields.add("MSG");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM03_OIVPM03_data copyObj = new OIVPM03_OIVPM03_data();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM03_OIVPM03_data) {
    	if(this == _oIVPM03_OIVPM03_data) return;
    	OIVPM03_OIVPM03_data __oIVPM03_OIVPM03_data = (OIVPM03_OIVPM03_data) _oIVPM03_OIVPM03_data;
    	this.setDATE(__oIVPM03_OIVPM03_data.getDATE());
    	this.setTERM(__oIVPM03_OIVPM03_data.getTERM());
    	this.setTIME(__oIVPM03_OIVPM03_data.getTIME());
    	this.setIDEN(__oIVPM03_OIVPM03_data.getIDEN());
    	this.setNAME(__oIVPM03_OIVPM03_data.getNAME());
    	this.setDEPT(__oIVPM03_OIVPM03_data.getDEPT());
    	this.setPHON(__oIVPM03_OIVPM03_data.getPHON());
    	this.setEMAL(__oIVPM03_OIVPM03_data.getEMAL());
    	this.setADDR(__oIVPM03_OIVPM03_data.getADDR());
    	this.setMSG(__oIVPM03_OIVPM03_data.getMSG());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE : ").append(DATE).append("\n");	
    	buffer.append("TERM : ").append(TERM).append("\n");	
    	buffer.append("TIME : ").append(TIME).append("\n");	
    	buffer.append("IDEN : ").append(IDEN).append("\n");	
    	buffer.append("NAME : ").append(NAME).append("\n");	
    	buffer.append("DEPT : ").append(DEPT).append("\n");	
    	buffer.append("PHON : ").append(PHON).append("\n");	
    	buffer.append("EMAL : ").append(EMAL).append("\n");	
    	buffer.append("ADDR : ").append(ADDR).append("\n");	
    	buffer.append("MSG : ").append(MSG).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(10);
    	fieldPropertyMap.put("DATE", FieldProperty.builder()
    	              .setPhysicalName("DATE")
    	              .setLogicalName("DATE")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM", FieldProperty.builder()
    	              .setPhysicalName("TERM")
    	              .setLogicalName("TERM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(4)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME", FieldProperty.builder()
    	              .setPhysicalName("TIME")
    	              .setLogicalName("TIME")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN", FieldProperty.builder()
    	              .setPhysicalName("IDEN")
    	              .setLogicalName("IDEN")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME", FieldProperty.builder()
    	              .setPhysicalName("NAME")
    	              .setLogicalName("NAME")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT", FieldProperty.builder()
    	              .setPhysicalName("DEPT")
    	              .setLogicalName("DEPT")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON", FieldProperty.builder()
    	              .setPhysicalName("PHON")
    	              .setLogicalName("PHON")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("EMAL", FieldProperty.builder()
    	              .setPhysicalName("EMAL")
    	              .setLogicalName("EMAL")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ADDR", FieldProperty.builder()
    	              .setPhysicalName("ADDR")
    	              .setLogicalName("ADDR")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG", FieldProperty.builder()
    	              .setPhysicalName("MSG")
    	              .setLogicalName("MSG")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(76)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : return getDATE();
    		case "TERM" : return getTERM();
    		case "TIME" : return getTIME();
    		case "IDEN" : return getIDEN();
    		case "NAME" : return getNAME();
    		case "DEPT" : return getDEPT();
    		case "PHON" : return getPHON();
    		case "EMAL" : return getEMAL();
    		case "ADDR" : return getADDR();
    		case "MSG" : return getMSG();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : setDATE((String)arg); break;
    		case "TERM" : setTERM((String)arg); break;
    		case "TIME" : setTIME((String)arg); break;
    		case "IDEN" : setIDEN((String)arg); break;
    		case "NAME" : setNAME((String)arg); break;
    		case "DEPT" : setDEPT((String)arg); break;
    		case "PHON" : setPHON((String)arg); break;
    		case "EMAL" : setEMAL((String)arg); break;
    		case "ADDR" : setADDR((String)arg); break;
    		case "MSG" : setMSG((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM03_OIVPM03_data _OIVPM03_OIVPM03_data = (OIVPM03_OIVPM03_data) obj;
    	if(this.DATE == null) {
    	if(_OIVPM03_OIVPM03_data.getDATE() != null)
    		return false;
    	} else if(!this.DATE.equals(_OIVPM03_OIVPM03_data.getDATE()))
    		return false;
    	if(this.TERM == null) {
    	if(_OIVPM03_OIVPM03_data.getTERM() != null)
    		return false;
    	} else if(!this.TERM.equals(_OIVPM03_OIVPM03_data.getTERM()))
    		return false;
    	if(this.TIME == null) {
    	if(_OIVPM03_OIVPM03_data.getTIME() != null)
    		return false;
    	} else if(!this.TIME.equals(_OIVPM03_OIVPM03_data.getTIME()))
    		return false;
    	if(this.IDEN == null) {
    	if(_OIVPM03_OIVPM03_data.getIDEN() != null)
    		return false;
    	} else if(!this.IDEN.equals(_OIVPM03_OIVPM03_data.getIDEN()))
    		return false;
    	if(this.NAME == null) {
    	if(_OIVPM03_OIVPM03_data.getNAME() != null)
    		return false;
    	} else if(!this.NAME.equals(_OIVPM03_OIVPM03_data.getNAME()))
    		return false;
    	if(this.DEPT == null) {
    	if(_OIVPM03_OIVPM03_data.getDEPT() != null)
    		return false;
    	} else if(!this.DEPT.equals(_OIVPM03_OIVPM03_data.getDEPT()))
    		return false;
    	if(this.PHON == null) {
    	if(_OIVPM03_OIVPM03_data.getPHON() != null)
    		return false;
    	} else if(!this.PHON.equals(_OIVPM03_OIVPM03_data.getPHON()))
    		return false;
    	if(this.EMAL == null) {
    	if(_OIVPM03_OIVPM03_data.getEMAL() != null)
    		return false;
    	} else if(!this.EMAL.equals(_OIVPM03_OIVPM03_data.getEMAL()))
    		return false;
    	if(this.ADDR == null) {
    	if(_OIVPM03_OIVPM03_data.getADDR() != null)
    		return false;
    	} else if(!this.ADDR.equals(_OIVPM03_OIVPM03_data.getADDR()))
    		return false;
    	if(this.MSG == null) {
    	if(_OIVPM03_OIVPM03_data.getMSG() != null)
    		return false;
    	} else if(!this.MSG.equals(_OIVPM03_OIVPM03_data.getMSG()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DATE == null) ? 0 : this.DATE.hashCode());
    	result = prime * result + ((this.TERM == null) ? 0 : this.TERM.hashCode());
    	result = prime * result + ((this.TIME == null) ? 0 : this.TIME.hashCode());
    	result = prime * result + ((this.IDEN == null) ? 0 : this.IDEN.hashCode());
    	result = prime * result + ((this.NAME == null) ? 0 : this.NAME.hashCode());
    	result = prime * result + ((this.DEPT == null) ? 0 : this.DEPT.hashCode());
    	result = prime * result + ((this.PHON == null) ? 0 : this.PHON.hashCode());
    	result = prime * result + ((this.EMAL == null) ? 0 : this.EMAL.hashCode());
    	result = prime * result + ((this.ADDR == null) ? 0 : this.ADDR.hashCode());
    	result = prime * result + ((this.MSG == null) ? 0 : this.MSG.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE = null;
    	TERM = null;
    	TIME = null;
    	IDEN = null;
    	NAME = null;
    	DEPT = null;
    	PHON = null;
    	EMAL = null;
    	ADDR = null;
    	MSG = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

