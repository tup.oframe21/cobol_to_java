package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPINFO_DCA_TIMEDto extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPINFO_DCA_TIMEDto";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPINFO_DCA_TIMEDto() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DCA_TIME_HRS
     * Comments    : 
     */	
    private String DCA_TIME_HRS = null;
    
    private transient boolean DCA_TIME_HRS_nullable = true;
    
    public boolean isNullableDCA_TIME_HRS() {
    	return this.DCA_TIME_HRS_nullable;
    }
    
    private transient boolean DCA_TIME_HRS_invalidation = false;
    	
    public void setInvalidationDCA_TIME_HRS(boolean invalidation) { 
    	this.DCA_TIME_HRS_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_TIME_HRS() {
    	return this.DCA_TIME_HRS_invalidation;
    }
    	
    private transient boolean DCA_TIME_HRS_modified = false;
    
    public boolean isModifiedDCA_TIME_HRS() {
    	return this.DCA_TIME_HRS_modified;
    }
    	
    public void unModifiedDCA_TIME_HRS() {
    	this.DCA_TIME_HRS_modified = false;
    }
    public FieldProperty getDCA_TIME_HRSFieldProperty() {
    	return fieldPropertyMap.get("DCA_TIME_HRS");
    }
    
    public String getDCA_TIME_HRS() {
    	return DCA_TIME_HRS;
    }	
    public String getNvlDCA_TIME_HRS() {
    	if(getDCA_TIME_HRS() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDCA_TIME_HRS();
    	}
    }
    public void setDCA_TIME_HRS(String DCA_TIME_HRS) {
    	if(DCA_TIME_HRS == null) {
    		this.DCA_TIME_HRS = null;
    	} else {
    		this.DCA_TIME_HRS = DCA_TIME_HRS;
    	}
    	this.DCA_TIME_HRS_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DCA_COLON
     * Comments    : 
     */	
    private String DCA_COLON = null;
    
    private transient boolean DCA_COLON_nullable = true;
    
    public boolean isNullableDCA_COLON() {
    	return this.DCA_COLON_nullable;
    }
    
    private transient boolean DCA_COLON_invalidation = false;
    	
    public void setInvalidationDCA_COLON(boolean invalidation) { 
    	this.DCA_COLON_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_COLON() {
    	return this.DCA_COLON_invalidation;
    }
    	
    private transient boolean DCA_COLON_modified = false;
    
    public boolean isModifiedDCA_COLON() {
    	return this.DCA_COLON_modified;
    }
    	
    public void unModifiedDCA_COLON() {
    	this.DCA_COLON_modified = false;
    }
    public FieldProperty getDCA_COLONFieldProperty() {
    	return fieldPropertyMap.get("DCA_COLON");
    }
    
    public String getDCA_COLON() {
    	return DCA_COLON;
    }	
    public String getNvlDCA_COLON() {
    	if(getDCA_COLON() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDCA_COLON();
    	}
    }
    public void setDCA_COLON(String DCA_COLON) {
    	if(DCA_COLON == null) {
    		this.DCA_COLON = null;
    	} else {
    		this.DCA_COLON = DCA_COLON;
    	}
    	this.DCA_COLON_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DCA_TIME_MIN
     * Comments    : 
     */	
    private String DCA_TIME_MIN = null;
    
    private transient boolean DCA_TIME_MIN_nullable = true;
    
    public boolean isNullableDCA_TIME_MIN() {
    	return this.DCA_TIME_MIN_nullable;
    }
    
    private transient boolean DCA_TIME_MIN_invalidation = false;
    	
    public void setInvalidationDCA_TIME_MIN(boolean invalidation) { 
    	this.DCA_TIME_MIN_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_TIME_MIN() {
    	return this.DCA_TIME_MIN_invalidation;
    }
    	
    private transient boolean DCA_TIME_MIN_modified = false;
    
    public boolean isModifiedDCA_TIME_MIN() {
    	return this.DCA_TIME_MIN_modified;
    }
    	
    public void unModifiedDCA_TIME_MIN() {
    	this.DCA_TIME_MIN_modified = false;
    }
    public FieldProperty getDCA_TIME_MINFieldProperty() {
    	return fieldPropertyMap.get("DCA_TIME_MIN");
    }
    
    public String getDCA_TIME_MIN() {
    	return DCA_TIME_MIN;
    }	
    public String getNvlDCA_TIME_MIN() {
    	if(getDCA_TIME_MIN() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDCA_TIME_MIN();
    	}
    }
    public void setDCA_TIME_MIN(String DCA_TIME_MIN) {
    	if(DCA_TIME_MIN == null) {
    		this.DCA_TIME_MIN = null;
    	} else {
    		this.DCA_TIME_MIN = DCA_TIME_MIN;
    	}
    	this.DCA_TIME_MIN_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DCA_TIME_AMPM
     * Comments    : 
     */	
    private String DCA_TIME_AMPM = null;
    
    private transient boolean DCA_TIME_AMPM_nullable = true;
    
    public boolean isNullableDCA_TIME_AMPM() {
    	return this.DCA_TIME_AMPM_nullable;
    }
    
    private transient boolean DCA_TIME_AMPM_invalidation = false;
    	
    public void setInvalidationDCA_TIME_AMPM(boolean invalidation) { 
    	this.DCA_TIME_AMPM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_TIME_AMPM() {
    	return this.DCA_TIME_AMPM_invalidation;
    }
    	
    private transient boolean DCA_TIME_AMPM_modified = false;
    
    public boolean isModifiedDCA_TIME_AMPM() {
    	return this.DCA_TIME_AMPM_modified;
    }
    	
    public void unModifiedDCA_TIME_AMPM() {
    	this.DCA_TIME_AMPM_modified = false;
    }
    public FieldProperty getDCA_TIME_AMPMFieldProperty() {
    	return fieldPropertyMap.get("DCA_TIME_AMPM");
    }
    
    public String getDCA_TIME_AMPM() {
    	return DCA_TIME_AMPM;
    }	
    public String getNvlDCA_TIME_AMPM() {
    	if(getDCA_TIME_AMPM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDCA_TIME_AMPM();
    	}
    }
    public void setDCA_TIME_AMPM(String DCA_TIME_AMPM) {
    	if(DCA_TIME_AMPM == null) {
    		this.DCA_TIME_AMPM = null;
    	} else {
    		this.DCA_TIME_AMPM = DCA_TIME_AMPM;
    	}
    	this.DCA_TIME_AMPM_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DCA_TIME_HRS_modified = false;
    	this.DCA_COLON_modified = false;
    	this.DCA_TIME_MIN_modified = false;
    	this.DCA_TIME_AMPM_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DCA_TIME_HRS_modified == true)
    		modifiedFields.add("DCA_TIME_HRS");
    	if(this.DCA_COLON_modified == true)
    		modifiedFields.add("DCA_COLON");
    	if(this.DCA_TIME_MIN_modified == true)
    		modifiedFields.add("DCA_TIME_MIN");
    	if(this.DCA_TIME_AMPM_modified == true)
    		modifiedFields.add("DCA_TIME_AMPM");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPINFO_DCA_TIMEDto copyObj = new OIVPINFO_DCA_TIMEDto();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPINFO_DCA_TIMEDto) {
    	if(this == _oIVPINFO_DCA_TIMEDto) return;
    	OIVPINFO_DCA_TIMEDto __oIVPINFO_DCA_TIMEDto = (OIVPINFO_DCA_TIMEDto) _oIVPINFO_DCA_TIMEDto;
    	this.setDCA_TIME_HRS(__oIVPINFO_DCA_TIMEDto.getDCA_TIME_HRS());
    	this.setDCA_COLON(__oIVPINFO_DCA_TIMEDto.getDCA_COLON());
    	this.setDCA_TIME_MIN(__oIVPINFO_DCA_TIMEDto.getDCA_TIME_MIN());
    	this.setDCA_TIME_AMPM(__oIVPINFO_DCA_TIMEDto.getDCA_TIME_AMPM());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DCA_TIME_HRS : ").append(DCA_TIME_HRS).append("\n");	
    	buffer.append("DCA_COLON : ").append(DCA_COLON).append("\n");	
    	buffer.append("DCA_TIME_MIN : ").append(DCA_TIME_MIN).append("\n");	
    	buffer.append("DCA_TIME_AMPM : ").append(DCA_TIME_AMPM).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(4);
    	fieldPropertyMap.put("DCA_TIME_HRS", FieldProperty.builder()
    	              .setPhysicalName("DCA_TIME_HRS")
    	              .setLogicalName("DCA_TIME_HRS")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(2)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("DCA_COLON", FieldProperty.builder()
    	              .setPhysicalName("DCA_COLON")
    	              .setLogicalName("DCA_COLON")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(1)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("DCA_TIME_MIN", FieldProperty.builder()
    	              .setPhysicalName("DCA_TIME_MIN")
    	              .setLogicalName("DCA_TIME_MIN")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(2)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("DCA_TIME_AMPM", FieldProperty.builder()
    	              .setPhysicalName("DCA_TIME_AMPM")
    	              .setLogicalName("DCA_TIME_AMPM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(3)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DCA_TIME_HRS" : return getDCA_TIME_HRS();
    		case "DCA_COLON" : return getDCA_COLON();
    		case "DCA_TIME_MIN" : return getDCA_TIME_MIN();
    		case "DCA_TIME_AMPM" : return getDCA_TIME_AMPM();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DCA_TIME_HRS" : setDCA_TIME_HRS((String)arg); break;
    		case "DCA_COLON" : setDCA_COLON((String)arg); break;
    		case "DCA_TIME_MIN" : setDCA_TIME_MIN((String)arg); break;
    		case "DCA_TIME_AMPM" : setDCA_TIME_AMPM((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPINFO_DCA_TIMEDto _OIVPINFO_DCA_TIMEDto = (OIVPINFO_DCA_TIMEDto) obj;
    	if(this.DCA_TIME_HRS == null) {
    	if(_OIVPINFO_DCA_TIMEDto.getDCA_TIME_HRS() != null)
    		return false;
    	} else if(!this.DCA_TIME_HRS.equals(_OIVPINFO_DCA_TIMEDto.getDCA_TIME_HRS()))
    		return false;
    	if(this.DCA_COLON == null) {
    	if(_OIVPINFO_DCA_TIMEDto.getDCA_COLON() != null)
    		return false;
    	} else if(!this.DCA_COLON.equals(_OIVPINFO_DCA_TIMEDto.getDCA_COLON()))
    		return false;
    	if(this.DCA_TIME_MIN == null) {
    	if(_OIVPINFO_DCA_TIMEDto.getDCA_TIME_MIN() != null)
    		return false;
    	} else if(!this.DCA_TIME_MIN.equals(_OIVPINFO_DCA_TIMEDto.getDCA_TIME_MIN()))
    		return false;
    	if(this.DCA_TIME_AMPM == null) {
    	if(_OIVPINFO_DCA_TIMEDto.getDCA_TIME_AMPM() != null)
    		return false;
    	} else if(!this.DCA_TIME_AMPM.equals(_OIVPINFO_DCA_TIMEDto.getDCA_TIME_AMPM()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DCA_TIME_HRS == null) ? 0 : this.DCA_TIME_HRS.hashCode());
    	result = prime * result + ((this.DCA_COLON == null) ? 0 : this.DCA_COLON.hashCode());
    	result = prime * result + ((this.DCA_TIME_MIN == null) ? 0 : this.DCA_TIME_MIN.hashCode());
    	result = prime * result + ((this.DCA_TIME_AMPM == null) ? 0 : this.DCA_TIME_AMPM.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DCA_TIME_HRS = null;
    	DCA_COLON = null;
    	DCA_TIME_MIN = null;
    	DCA_TIME_AMPM = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

