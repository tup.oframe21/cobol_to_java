package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPM05_OIVPM05_dataMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPM05_OIVPM05_data _OIVPM05_OIVPM05_data = (com.tmax.openframe.dto.OIVPM05_OIVPM05_data)obj;
    	
    	if(_OIVPM05_OIVPM05_data == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPM05_OIVPM05_data, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPM05_OIVPM05_data _OIVPM05_OIVPM05_data, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DATE"); 
    	if (_OIVPM05_OIVPM05_data.getDATE() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getDATE());
    		writer.value(_OIVPM05_OIVPM05_data.getDATE());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM"); 
    	if (_OIVPM05_OIVPM05_data.getTERM() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getTERM());
    		writer.value(_OIVPM05_OIVPM05_data.getTERM());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME"); 
    	if (_OIVPM05_OIVPM05_data.getTIME() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getTIME());
    		writer.value(_OIVPM05_OIVPM05_data.getTIME());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN"); 
    	if (_OIVPM05_OIVPM05_data.getIDEN() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getIDEN());
    		writer.value(_OIVPM05_OIVPM05_data.getIDEN());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("NAME"); 
    	if (_OIVPM05_OIVPM05_data.getNAME() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getNAME());
    		writer.value(_OIVPM05_OIVPM05_data.getNAME());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DEPT"); 
    	if (_OIVPM05_OIVPM05_data.getDEPT() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getDEPT());
    		writer.value(_OIVPM05_OIVPM05_data.getDEPT());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("PHON"); 
    	if (_OIVPM05_OIVPM05_data.getPHON() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getPHON());
    		writer.value(_OIVPM05_OIVPM05_data.getPHON());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("EMAL"); 
    	if (_OIVPM05_OIVPM05_data.getEMAL() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getEMAL());
    		writer.value(_OIVPM05_OIVPM05_data.getEMAL());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("ADDR"); 
    	if (_OIVPM05_OIVPM05_data.getADDR() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getADDR());
    		writer.value(_OIVPM05_OIVPM05_data.getADDR());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG"); 
    	if (_OIVPM05_OIVPM05_data.getMSG() != null) {
    		writer.value(_OIVPM05_OIVPM05_data.getMSG());
    		writer.value(_OIVPM05_OIVPM05_data.getMSG());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM05_OIVPM05_data _OIVPM05_OIVPM05_data = new com.tmax.openframe.dto.OIVPM05_OIVPM05_data();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM05_OIVPM05_data();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM05_OIVPM05_data = (com.tmax.openframe.dto.OIVPM05_OIVPM05_data)unmarshal( jr,  _OIVPM05_OIVPM05_data);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPM05_OIVPM05_data;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM05_OIVPM05_data _OIVPM05_OIVPM05_data = (com.tmax.openframe.dto.OIVPM05_OIVPM05_data) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM05_OIVPM05_data();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM05_OIVPM05_data = (com.tmax.openframe.dto.OIVPM05_OIVPM05_data)unmarshal( jr,  _OIVPM05_OIVPM05_data);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPM05_OIVPM05_data;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPM05_OIVPM05_data dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPM05_OIVPM05_data dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DATE" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "NAME" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setNAME( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DEPT" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDEPT( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "PHON" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setPHON( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "EMAL" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setEMAL( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "ADDR" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setADDR( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

