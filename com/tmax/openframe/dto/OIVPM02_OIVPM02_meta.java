package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM02_OIVPM02_meta extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM02_OIVPM02_meta";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM02_OIVPM02_meta() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE_length
     * Comments    : 
     */	
    private int DATE_length = 0;
    
    private transient boolean DATE_length_nullable = false;
    
    public boolean isNullableDATE_length() {
    	return this.DATE_length_nullable;
    }
    
    private transient boolean DATE_length_invalidation = false;
    	
    public void setInvalidationDATE_length(boolean invalidation) { 
    	this.DATE_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_length() {
    	return this.DATE_length_invalidation;
    }
    	
    private transient boolean DATE_length_modified = false;
    
    public boolean isModifiedDATE_length() {
    	return this.DATE_length_modified;
    }
    	
    public void unModifiedDATE_length() {
    	this.DATE_length_modified = false;
    }
    public FieldProperty getDATE_lengthFieldProperty() {
    	return fieldPropertyMap.get("DATE_length");
    }
    
    public int getDATE_length() {
    	return DATE_length;
    }	
    public void setDATE_length(int DATE_length) {
    	this.DATE_length = DATE_length;
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(Integer DATE_length) {
    	if( DATE_length == null) {
    		this.DATE_length = 0;
    	} else{
    		this.DATE_length = DATE_length.intValue();
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(String DATE_length) {
    	if  (DATE_length==null || DATE_length.length() == 0) {
    		this.DATE_length = 0;
    	} else {
    		this.DATE_length = Integer.parseInt(DATE_length);
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_color
     * Comments    : 
     */	
    private String DATE_color = null;
    
    private transient boolean DATE_color_nullable = true;
    
    public boolean isNullableDATE_color() {
    	return this.DATE_color_nullable;
    }
    
    private transient boolean DATE_color_invalidation = false;
    	
    public void setInvalidationDATE_color(boolean invalidation) { 
    	this.DATE_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_color() {
    	return this.DATE_color_invalidation;
    }
    	
    private transient boolean DATE_color_modified = false;
    
    public boolean isModifiedDATE_color() {
    	return this.DATE_color_modified;
    }
    	
    public void unModifiedDATE_color() {
    	this.DATE_color_modified = false;
    }
    public FieldProperty getDATE_colorFieldProperty() {
    	return fieldPropertyMap.get("DATE_color");
    }
    
    public String getDATE_color() {
    	return DATE_color;
    }	
    public String getNvlDATE_color() {
    	if(getDATE_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_color();
    	}
    }
    public void setDATE_color(String DATE_color) {
    	if(DATE_color == null) {
    		this.DATE_color = null;
    	} else {
    		this.DATE_color = DATE_color;
    	}
    	this.DATE_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_hilight
     * Comments    : 
     */	
    private String DATE_hilight = null;
    
    private transient boolean DATE_hilight_nullable = true;
    
    public boolean isNullableDATE_hilight() {
    	return this.DATE_hilight_nullable;
    }
    
    private transient boolean DATE_hilight_invalidation = false;
    	
    public void setInvalidationDATE_hilight(boolean invalidation) { 
    	this.DATE_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_hilight() {
    	return this.DATE_hilight_invalidation;
    }
    	
    private transient boolean DATE_hilight_modified = false;
    
    public boolean isModifiedDATE_hilight() {
    	return this.DATE_hilight_modified;
    }
    	
    public void unModifiedDATE_hilight() {
    	this.DATE_hilight_modified = false;
    }
    public FieldProperty getDATE_hilightFieldProperty() {
    	return fieldPropertyMap.get("DATE_hilight");
    }
    
    public String getDATE_hilight() {
    	return DATE_hilight;
    }	
    public String getNvlDATE_hilight() {
    	if(getDATE_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_hilight();
    	}
    }
    public void setDATE_hilight(String DATE_hilight) {
    	if(DATE_hilight == null) {
    		this.DATE_hilight = null;
    	} else {
    		this.DATE_hilight = DATE_hilight;
    	}
    	this.DATE_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_outline
     * Comments    : 
     */	
    private String DATE_outline = null;
    
    private transient boolean DATE_outline_nullable = true;
    
    public boolean isNullableDATE_outline() {
    	return this.DATE_outline_nullable;
    }
    
    private transient boolean DATE_outline_invalidation = false;
    	
    public void setInvalidationDATE_outline(boolean invalidation) { 
    	this.DATE_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_outline() {
    	return this.DATE_outline_invalidation;
    }
    	
    private transient boolean DATE_outline_modified = false;
    
    public boolean isModifiedDATE_outline() {
    	return this.DATE_outline_modified;
    }
    	
    public void unModifiedDATE_outline() {
    	this.DATE_outline_modified = false;
    }
    public FieldProperty getDATE_outlineFieldProperty() {
    	return fieldPropertyMap.get("DATE_outline");
    }
    
    public String getDATE_outline() {
    	return DATE_outline;
    }	
    public String getNvlDATE_outline() {
    	if(getDATE_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_outline();
    	}
    }
    public void setDATE_outline(String DATE_outline) {
    	if(DATE_outline == null) {
    		this.DATE_outline = null;
    	} else {
    		this.DATE_outline = DATE_outline;
    	}
    	this.DATE_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_transp
     * Comments    : 
     */	
    private String DATE_transp = null;
    
    private transient boolean DATE_transp_nullable = true;
    
    public boolean isNullableDATE_transp() {
    	return this.DATE_transp_nullable;
    }
    
    private transient boolean DATE_transp_invalidation = false;
    	
    public void setInvalidationDATE_transp(boolean invalidation) { 
    	this.DATE_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_transp() {
    	return this.DATE_transp_invalidation;
    }
    	
    private transient boolean DATE_transp_modified = false;
    
    public boolean isModifiedDATE_transp() {
    	return this.DATE_transp_modified;
    }
    	
    public void unModifiedDATE_transp() {
    	this.DATE_transp_modified = false;
    }
    public FieldProperty getDATE_transpFieldProperty() {
    	return fieldPropertyMap.get("DATE_transp");
    }
    
    public String getDATE_transp() {
    	return DATE_transp;
    }	
    public String getNvlDATE_transp() {
    	if(getDATE_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_transp();
    	}
    }
    public void setDATE_transp(String DATE_transp) {
    	if(DATE_transp == null) {
    		this.DATE_transp = null;
    	} else {
    		this.DATE_transp = DATE_transp;
    	}
    	this.DATE_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_validn
     * Comments    : 
     */	
    private String DATE_validn = null;
    
    private transient boolean DATE_validn_nullable = true;
    
    public boolean isNullableDATE_validn() {
    	return this.DATE_validn_nullable;
    }
    
    private transient boolean DATE_validn_invalidation = false;
    	
    public void setInvalidationDATE_validn(boolean invalidation) { 
    	this.DATE_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_validn() {
    	return this.DATE_validn_invalidation;
    }
    	
    private transient boolean DATE_validn_modified = false;
    
    public boolean isModifiedDATE_validn() {
    	return this.DATE_validn_modified;
    }
    	
    public void unModifiedDATE_validn() {
    	this.DATE_validn_modified = false;
    }
    public FieldProperty getDATE_validnFieldProperty() {
    	return fieldPropertyMap.get("DATE_validn");
    }
    
    public String getDATE_validn() {
    	return DATE_validn;
    }	
    public String getNvlDATE_validn() {
    	if(getDATE_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_validn();
    	}
    }
    public void setDATE_validn(String DATE_validn) {
    	if(DATE_validn == null) {
    		this.DATE_validn = null;
    	} else {
    		this.DATE_validn = DATE_validn;
    	}
    	this.DATE_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_sosi
     * Comments    : 
     */	
    private String DATE_sosi = null;
    
    private transient boolean DATE_sosi_nullable = true;
    
    public boolean isNullableDATE_sosi() {
    	return this.DATE_sosi_nullable;
    }
    
    private transient boolean DATE_sosi_invalidation = false;
    	
    public void setInvalidationDATE_sosi(boolean invalidation) { 
    	this.DATE_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_sosi() {
    	return this.DATE_sosi_invalidation;
    }
    	
    private transient boolean DATE_sosi_modified = false;
    
    public boolean isModifiedDATE_sosi() {
    	return this.DATE_sosi_modified;
    }
    	
    public void unModifiedDATE_sosi() {
    	this.DATE_sosi_modified = false;
    }
    public FieldProperty getDATE_sosiFieldProperty() {
    	return fieldPropertyMap.get("DATE_sosi");
    }
    
    public String getDATE_sosi() {
    	return DATE_sosi;
    }	
    public String getNvlDATE_sosi() {
    	if(getDATE_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_sosi();
    	}
    }
    public void setDATE_sosi(String DATE_sosi) {
    	if(DATE_sosi == null) {
    		this.DATE_sosi = null;
    	} else {
    		this.DATE_sosi = DATE_sosi;
    	}
    	this.DATE_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_ps
     * Comments    : 
     */	
    private String DATE_ps = null;
    
    private transient boolean DATE_ps_nullable = true;
    
    public boolean isNullableDATE_ps() {
    	return this.DATE_ps_nullable;
    }
    
    private transient boolean DATE_ps_invalidation = false;
    	
    public void setInvalidationDATE_ps(boolean invalidation) { 
    	this.DATE_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_ps() {
    	return this.DATE_ps_invalidation;
    }
    	
    private transient boolean DATE_ps_modified = false;
    
    public boolean isModifiedDATE_ps() {
    	return this.DATE_ps_modified;
    }
    	
    public void unModifiedDATE_ps() {
    	this.DATE_ps_modified = false;
    }
    public FieldProperty getDATE_psFieldProperty() {
    	return fieldPropertyMap.get("DATE_ps");
    }
    
    public String getDATE_ps() {
    	return DATE_ps;
    }	
    public String getNvlDATE_ps() {
    	if(getDATE_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_ps();
    	}
    }
    public void setDATE_ps(String DATE_ps) {
    	if(DATE_ps == null) {
    		this.DATE_ps = null;
    	} else {
    		this.DATE_ps = DATE_ps;
    	}
    	this.DATE_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_length
     * Comments    : 
     */	
    private int TERM_length = 0;
    
    private transient boolean TERM_length_nullable = false;
    
    public boolean isNullableTERM_length() {
    	return this.TERM_length_nullable;
    }
    
    private transient boolean TERM_length_invalidation = false;
    	
    public void setInvalidationTERM_length(boolean invalidation) { 
    	this.TERM_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_length() {
    	return this.TERM_length_invalidation;
    }
    	
    private transient boolean TERM_length_modified = false;
    
    public boolean isModifiedTERM_length() {
    	return this.TERM_length_modified;
    }
    	
    public void unModifiedTERM_length() {
    	this.TERM_length_modified = false;
    }
    public FieldProperty getTERM_lengthFieldProperty() {
    	return fieldPropertyMap.get("TERM_length");
    }
    
    public int getTERM_length() {
    	return TERM_length;
    }	
    public void setTERM_length(int TERM_length) {
    	this.TERM_length = TERM_length;
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(Integer TERM_length) {
    	if( TERM_length == null) {
    		this.TERM_length = 0;
    	} else{
    		this.TERM_length = TERM_length.intValue();
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(String TERM_length) {
    	if  (TERM_length==null || TERM_length.length() == 0) {
    		this.TERM_length = 0;
    	} else {
    		this.TERM_length = Integer.parseInt(TERM_length);
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_color
     * Comments    : 
     */	
    private String TERM_color = null;
    
    private transient boolean TERM_color_nullable = true;
    
    public boolean isNullableTERM_color() {
    	return this.TERM_color_nullable;
    }
    
    private transient boolean TERM_color_invalidation = false;
    	
    public void setInvalidationTERM_color(boolean invalidation) { 
    	this.TERM_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_color() {
    	return this.TERM_color_invalidation;
    }
    	
    private transient boolean TERM_color_modified = false;
    
    public boolean isModifiedTERM_color() {
    	return this.TERM_color_modified;
    }
    	
    public void unModifiedTERM_color() {
    	this.TERM_color_modified = false;
    }
    public FieldProperty getTERM_colorFieldProperty() {
    	return fieldPropertyMap.get("TERM_color");
    }
    
    public String getTERM_color() {
    	return TERM_color;
    }	
    public String getNvlTERM_color() {
    	if(getTERM_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_color();
    	}
    }
    public void setTERM_color(String TERM_color) {
    	if(TERM_color == null) {
    		this.TERM_color = null;
    	} else {
    		this.TERM_color = TERM_color;
    	}
    	this.TERM_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_hilight
     * Comments    : 
     */	
    private String TERM_hilight = null;
    
    private transient boolean TERM_hilight_nullable = true;
    
    public boolean isNullableTERM_hilight() {
    	return this.TERM_hilight_nullable;
    }
    
    private transient boolean TERM_hilight_invalidation = false;
    	
    public void setInvalidationTERM_hilight(boolean invalidation) { 
    	this.TERM_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_hilight() {
    	return this.TERM_hilight_invalidation;
    }
    	
    private transient boolean TERM_hilight_modified = false;
    
    public boolean isModifiedTERM_hilight() {
    	return this.TERM_hilight_modified;
    }
    	
    public void unModifiedTERM_hilight() {
    	this.TERM_hilight_modified = false;
    }
    public FieldProperty getTERM_hilightFieldProperty() {
    	return fieldPropertyMap.get("TERM_hilight");
    }
    
    public String getTERM_hilight() {
    	return TERM_hilight;
    }	
    public String getNvlTERM_hilight() {
    	if(getTERM_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_hilight();
    	}
    }
    public void setTERM_hilight(String TERM_hilight) {
    	if(TERM_hilight == null) {
    		this.TERM_hilight = null;
    	} else {
    		this.TERM_hilight = TERM_hilight;
    	}
    	this.TERM_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_outline
     * Comments    : 
     */	
    private String TERM_outline = null;
    
    private transient boolean TERM_outline_nullable = true;
    
    public boolean isNullableTERM_outline() {
    	return this.TERM_outline_nullable;
    }
    
    private transient boolean TERM_outline_invalidation = false;
    	
    public void setInvalidationTERM_outline(boolean invalidation) { 
    	this.TERM_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_outline() {
    	return this.TERM_outline_invalidation;
    }
    	
    private transient boolean TERM_outline_modified = false;
    
    public boolean isModifiedTERM_outline() {
    	return this.TERM_outline_modified;
    }
    	
    public void unModifiedTERM_outline() {
    	this.TERM_outline_modified = false;
    }
    public FieldProperty getTERM_outlineFieldProperty() {
    	return fieldPropertyMap.get("TERM_outline");
    }
    
    public String getTERM_outline() {
    	return TERM_outline;
    }	
    public String getNvlTERM_outline() {
    	if(getTERM_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_outline();
    	}
    }
    public void setTERM_outline(String TERM_outline) {
    	if(TERM_outline == null) {
    		this.TERM_outline = null;
    	} else {
    		this.TERM_outline = TERM_outline;
    	}
    	this.TERM_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_transp
     * Comments    : 
     */	
    private String TERM_transp = null;
    
    private transient boolean TERM_transp_nullable = true;
    
    public boolean isNullableTERM_transp() {
    	return this.TERM_transp_nullable;
    }
    
    private transient boolean TERM_transp_invalidation = false;
    	
    public void setInvalidationTERM_transp(boolean invalidation) { 
    	this.TERM_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_transp() {
    	return this.TERM_transp_invalidation;
    }
    	
    private transient boolean TERM_transp_modified = false;
    
    public boolean isModifiedTERM_transp() {
    	return this.TERM_transp_modified;
    }
    	
    public void unModifiedTERM_transp() {
    	this.TERM_transp_modified = false;
    }
    public FieldProperty getTERM_transpFieldProperty() {
    	return fieldPropertyMap.get("TERM_transp");
    }
    
    public String getTERM_transp() {
    	return TERM_transp;
    }	
    public String getNvlTERM_transp() {
    	if(getTERM_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_transp();
    	}
    }
    public void setTERM_transp(String TERM_transp) {
    	if(TERM_transp == null) {
    		this.TERM_transp = null;
    	} else {
    		this.TERM_transp = TERM_transp;
    	}
    	this.TERM_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_validn
     * Comments    : 
     */	
    private String TERM_validn = null;
    
    private transient boolean TERM_validn_nullable = true;
    
    public boolean isNullableTERM_validn() {
    	return this.TERM_validn_nullable;
    }
    
    private transient boolean TERM_validn_invalidation = false;
    	
    public void setInvalidationTERM_validn(boolean invalidation) { 
    	this.TERM_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_validn() {
    	return this.TERM_validn_invalidation;
    }
    	
    private transient boolean TERM_validn_modified = false;
    
    public boolean isModifiedTERM_validn() {
    	return this.TERM_validn_modified;
    }
    	
    public void unModifiedTERM_validn() {
    	this.TERM_validn_modified = false;
    }
    public FieldProperty getTERM_validnFieldProperty() {
    	return fieldPropertyMap.get("TERM_validn");
    }
    
    public String getTERM_validn() {
    	return TERM_validn;
    }	
    public String getNvlTERM_validn() {
    	if(getTERM_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_validn();
    	}
    }
    public void setTERM_validn(String TERM_validn) {
    	if(TERM_validn == null) {
    		this.TERM_validn = null;
    	} else {
    		this.TERM_validn = TERM_validn;
    	}
    	this.TERM_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_sosi
     * Comments    : 
     */	
    private String TERM_sosi = null;
    
    private transient boolean TERM_sosi_nullable = true;
    
    public boolean isNullableTERM_sosi() {
    	return this.TERM_sosi_nullable;
    }
    
    private transient boolean TERM_sosi_invalidation = false;
    	
    public void setInvalidationTERM_sosi(boolean invalidation) { 
    	this.TERM_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_sosi() {
    	return this.TERM_sosi_invalidation;
    }
    	
    private transient boolean TERM_sosi_modified = false;
    
    public boolean isModifiedTERM_sosi() {
    	return this.TERM_sosi_modified;
    }
    	
    public void unModifiedTERM_sosi() {
    	this.TERM_sosi_modified = false;
    }
    public FieldProperty getTERM_sosiFieldProperty() {
    	return fieldPropertyMap.get("TERM_sosi");
    }
    
    public String getTERM_sosi() {
    	return TERM_sosi;
    }	
    public String getNvlTERM_sosi() {
    	if(getTERM_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_sosi();
    	}
    }
    public void setTERM_sosi(String TERM_sosi) {
    	if(TERM_sosi == null) {
    		this.TERM_sosi = null;
    	} else {
    		this.TERM_sosi = TERM_sosi;
    	}
    	this.TERM_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_ps
     * Comments    : 
     */	
    private String TERM_ps = null;
    
    private transient boolean TERM_ps_nullable = true;
    
    public boolean isNullableTERM_ps() {
    	return this.TERM_ps_nullable;
    }
    
    private transient boolean TERM_ps_invalidation = false;
    	
    public void setInvalidationTERM_ps(boolean invalidation) { 
    	this.TERM_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_ps() {
    	return this.TERM_ps_invalidation;
    }
    	
    private transient boolean TERM_ps_modified = false;
    
    public boolean isModifiedTERM_ps() {
    	return this.TERM_ps_modified;
    }
    	
    public void unModifiedTERM_ps() {
    	this.TERM_ps_modified = false;
    }
    public FieldProperty getTERM_psFieldProperty() {
    	return fieldPropertyMap.get("TERM_ps");
    }
    
    public String getTERM_ps() {
    	return TERM_ps;
    }	
    public String getNvlTERM_ps() {
    	if(getTERM_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_ps();
    	}
    }
    public void setTERM_ps(String TERM_ps) {
    	if(TERM_ps == null) {
    		this.TERM_ps = null;
    	} else {
    		this.TERM_ps = TERM_ps;
    	}
    	this.TERM_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_length
     * Comments    : 
     */	
    private int TIME_length = 0;
    
    private transient boolean TIME_length_nullable = false;
    
    public boolean isNullableTIME_length() {
    	return this.TIME_length_nullable;
    }
    
    private transient boolean TIME_length_invalidation = false;
    	
    public void setInvalidationTIME_length(boolean invalidation) { 
    	this.TIME_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_length() {
    	return this.TIME_length_invalidation;
    }
    	
    private transient boolean TIME_length_modified = false;
    
    public boolean isModifiedTIME_length() {
    	return this.TIME_length_modified;
    }
    	
    public void unModifiedTIME_length() {
    	this.TIME_length_modified = false;
    }
    public FieldProperty getTIME_lengthFieldProperty() {
    	return fieldPropertyMap.get("TIME_length");
    }
    
    public int getTIME_length() {
    	return TIME_length;
    }	
    public void setTIME_length(int TIME_length) {
    	this.TIME_length = TIME_length;
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(Integer TIME_length) {
    	if( TIME_length == null) {
    		this.TIME_length = 0;
    	} else{
    		this.TIME_length = TIME_length.intValue();
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(String TIME_length) {
    	if  (TIME_length==null || TIME_length.length() == 0) {
    		this.TIME_length = 0;
    	} else {
    		this.TIME_length = Integer.parseInt(TIME_length);
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_color
     * Comments    : 
     */	
    private String TIME_color = null;
    
    private transient boolean TIME_color_nullable = true;
    
    public boolean isNullableTIME_color() {
    	return this.TIME_color_nullable;
    }
    
    private transient boolean TIME_color_invalidation = false;
    	
    public void setInvalidationTIME_color(boolean invalidation) { 
    	this.TIME_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_color() {
    	return this.TIME_color_invalidation;
    }
    	
    private transient boolean TIME_color_modified = false;
    
    public boolean isModifiedTIME_color() {
    	return this.TIME_color_modified;
    }
    	
    public void unModifiedTIME_color() {
    	this.TIME_color_modified = false;
    }
    public FieldProperty getTIME_colorFieldProperty() {
    	return fieldPropertyMap.get("TIME_color");
    }
    
    public String getTIME_color() {
    	return TIME_color;
    }	
    public String getNvlTIME_color() {
    	if(getTIME_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_color();
    	}
    }
    public void setTIME_color(String TIME_color) {
    	if(TIME_color == null) {
    		this.TIME_color = null;
    	} else {
    		this.TIME_color = TIME_color;
    	}
    	this.TIME_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_hilight
     * Comments    : 
     */	
    private String TIME_hilight = null;
    
    private transient boolean TIME_hilight_nullable = true;
    
    public boolean isNullableTIME_hilight() {
    	return this.TIME_hilight_nullable;
    }
    
    private transient boolean TIME_hilight_invalidation = false;
    	
    public void setInvalidationTIME_hilight(boolean invalidation) { 
    	this.TIME_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_hilight() {
    	return this.TIME_hilight_invalidation;
    }
    	
    private transient boolean TIME_hilight_modified = false;
    
    public boolean isModifiedTIME_hilight() {
    	return this.TIME_hilight_modified;
    }
    	
    public void unModifiedTIME_hilight() {
    	this.TIME_hilight_modified = false;
    }
    public FieldProperty getTIME_hilightFieldProperty() {
    	return fieldPropertyMap.get("TIME_hilight");
    }
    
    public String getTIME_hilight() {
    	return TIME_hilight;
    }	
    public String getNvlTIME_hilight() {
    	if(getTIME_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_hilight();
    	}
    }
    public void setTIME_hilight(String TIME_hilight) {
    	if(TIME_hilight == null) {
    		this.TIME_hilight = null;
    	} else {
    		this.TIME_hilight = TIME_hilight;
    	}
    	this.TIME_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_outline
     * Comments    : 
     */	
    private String TIME_outline = null;
    
    private transient boolean TIME_outline_nullable = true;
    
    public boolean isNullableTIME_outline() {
    	return this.TIME_outline_nullable;
    }
    
    private transient boolean TIME_outline_invalidation = false;
    	
    public void setInvalidationTIME_outline(boolean invalidation) { 
    	this.TIME_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_outline() {
    	return this.TIME_outline_invalidation;
    }
    	
    private transient boolean TIME_outline_modified = false;
    
    public boolean isModifiedTIME_outline() {
    	return this.TIME_outline_modified;
    }
    	
    public void unModifiedTIME_outline() {
    	this.TIME_outline_modified = false;
    }
    public FieldProperty getTIME_outlineFieldProperty() {
    	return fieldPropertyMap.get("TIME_outline");
    }
    
    public String getTIME_outline() {
    	return TIME_outline;
    }	
    public String getNvlTIME_outline() {
    	if(getTIME_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_outline();
    	}
    }
    public void setTIME_outline(String TIME_outline) {
    	if(TIME_outline == null) {
    		this.TIME_outline = null;
    	} else {
    		this.TIME_outline = TIME_outline;
    	}
    	this.TIME_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_transp
     * Comments    : 
     */	
    private String TIME_transp = null;
    
    private transient boolean TIME_transp_nullable = true;
    
    public boolean isNullableTIME_transp() {
    	return this.TIME_transp_nullable;
    }
    
    private transient boolean TIME_transp_invalidation = false;
    	
    public void setInvalidationTIME_transp(boolean invalidation) { 
    	this.TIME_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_transp() {
    	return this.TIME_transp_invalidation;
    }
    	
    private transient boolean TIME_transp_modified = false;
    
    public boolean isModifiedTIME_transp() {
    	return this.TIME_transp_modified;
    }
    	
    public void unModifiedTIME_transp() {
    	this.TIME_transp_modified = false;
    }
    public FieldProperty getTIME_transpFieldProperty() {
    	return fieldPropertyMap.get("TIME_transp");
    }
    
    public String getTIME_transp() {
    	return TIME_transp;
    }	
    public String getNvlTIME_transp() {
    	if(getTIME_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_transp();
    	}
    }
    public void setTIME_transp(String TIME_transp) {
    	if(TIME_transp == null) {
    		this.TIME_transp = null;
    	} else {
    		this.TIME_transp = TIME_transp;
    	}
    	this.TIME_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_validn
     * Comments    : 
     */	
    private String TIME_validn = null;
    
    private transient boolean TIME_validn_nullable = true;
    
    public boolean isNullableTIME_validn() {
    	return this.TIME_validn_nullable;
    }
    
    private transient boolean TIME_validn_invalidation = false;
    	
    public void setInvalidationTIME_validn(boolean invalidation) { 
    	this.TIME_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_validn() {
    	return this.TIME_validn_invalidation;
    }
    	
    private transient boolean TIME_validn_modified = false;
    
    public boolean isModifiedTIME_validn() {
    	return this.TIME_validn_modified;
    }
    	
    public void unModifiedTIME_validn() {
    	this.TIME_validn_modified = false;
    }
    public FieldProperty getTIME_validnFieldProperty() {
    	return fieldPropertyMap.get("TIME_validn");
    }
    
    public String getTIME_validn() {
    	return TIME_validn;
    }	
    public String getNvlTIME_validn() {
    	if(getTIME_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_validn();
    	}
    }
    public void setTIME_validn(String TIME_validn) {
    	if(TIME_validn == null) {
    		this.TIME_validn = null;
    	} else {
    		this.TIME_validn = TIME_validn;
    	}
    	this.TIME_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_sosi
     * Comments    : 
     */	
    private String TIME_sosi = null;
    
    private transient boolean TIME_sosi_nullable = true;
    
    public boolean isNullableTIME_sosi() {
    	return this.TIME_sosi_nullable;
    }
    
    private transient boolean TIME_sosi_invalidation = false;
    	
    public void setInvalidationTIME_sosi(boolean invalidation) { 
    	this.TIME_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_sosi() {
    	return this.TIME_sosi_invalidation;
    }
    	
    private transient boolean TIME_sosi_modified = false;
    
    public boolean isModifiedTIME_sosi() {
    	return this.TIME_sosi_modified;
    }
    	
    public void unModifiedTIME_sosi() {
    	this.TIME_sosi_modified = false;
    }
    public FieldProperty getTIME_sosiFieldProperty() {
    	return fieldPropertyMap.get("TIME_sosi");
    }
    
    public String getTIME_sosi() {
    	return TIME_sosi;
    }	
    public String getNvlTIME_sosi() {
    	if(getTIME_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_sosi();
    	}
    }
    public void setTIME_sosi(String TIME_sosi) {
    	if(TIME_sosi == null) {
    		this.TIME_sosi = null;
    	} else {
    		this.TIME_sosi = TIME_sosi;
    	}
    	this.TIME_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_ps
     * Comments    : 
     */	
    private String TIME_ps = null;
    
    private transient boolean TIME_ps_nullable = true;
    
    public boolean isNullableTIME_ps() {
    	return this.TIME_ps_nullable;
    }
    
    private transient boolean TIME_ps_invalidation = false;
    	
    public void setInvalidationTIME_ps(boolean invalidation) { 
    	this.TIME_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_ps() {
    	return this.TIME_ps_invalidation;
    }
    	
    private transient boolean TIME_ps_modified = false;
    
    public boolean isModifiedTIME_ps() {
    	return this.TIME_ps_modified;
    }
    	
    public void unModifiedTIME_ps() {
    	this.TIME_ps_modified = false;
    }
    public FieldProperty getTIME_psFieldProperty() {
    	return fieldPropertyMap.get("TIME_ps");
    }
    
    public String getTIME_ps() {
    	return TIME_ps;
    }	
    public String getNvlTIME_ps() {
    	if(getTIME_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_ps();
    	}
    }
    public void setTIME_ps(String TIME_ps) {
    	if(TIME_ps == null) {
    		this.TIME_ps = null;
    	} else {
    		this.TIME_ps = TIME_ps;
    	}
    	this.TIME_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_length
     * Comments    : 
     */	
    private int IDEN1_length = 0;
    
    private transient boolean IDEN1_length_nullable = false;
    
    public boolean isNullableIDEN1_length() {
    	return this.IDEN1_length_nullable;
    }
    
    private transient boolean IDEN1_length_invalidation = false;
    	
    public void setInvalidationIDEN1_length(boolean invalidation) { 
    	this.IDEN1_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_length() {
    	return this.IDEN1_length_invalidation;
    }
    	
    private transient boolean IDEN1_length_modified = false;
    
    public boolean isModifiedIDEN1_length() {
    	return this.IDEN1_length_modified;
    }
    	
    public void unModifiedIDEN1_length() {
    	this.IDEN1_length_modified = false;
    }
    public FieldProperty getIDEN1_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_length");
    }
    
    public int getIDEN1_length() {
    	return IDEN1_length;
    }	
    public void setIDEN1_length(int IDEN1_length) {
    	this.IDEN1_length = IDEN1_length;
    	this.IDEN1_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN1_length(Integer IDEN1_length) {
    	if( IDEN1_length == null) {
    		this.IDEN1_length = 0;
    	} else{
    		this.IDEN1_length = IDEN1_length.intValue();
    	}
    	this.IDEN1_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN1_length(String IDEN1_length) {
    	if  (IDEN1_length==null || IDEN1_length.length() == 0) {
    		this.IDEN1_length = 0;
    	} else {
    		this.IDEN1_length = Integer.parseInt(IDEN1_length);
    	}
    	this.IDEN1_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_color
     * Comments    : 
     */	
    private String IDEN1_color = null;
    
    private transient boolean IDEN1_color_nullable = true;
    
    public boolean isNullableIDEN1_color() {
    	return this.IDEN1_color_nullable;
    }
    
    private transient boolean IDEN1_color_invalidation = false;
    	
    public void setInvalidationIDEN1_color(boolean invalidation) { 
    	this.IDEN1_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_color() {
    	return this.IDEN1_color_invalidation;
    }
    	
    private transient boolean IDEN1_color_modified = false;
    
    public boolean isModifiedIDEN1_color() {
    	return this.IDEN1_color_modified;
    }
    	
    public void unModifiedIDEN1_color() {
    	this.IDEN1_color_modified = false;
    }
    public FieldProperty getIDEN1_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_color");
    }
    
    public String getIDEN1_color() {
    	return IDEN1_color;
    }	
    public String getNvlIDEN1_color() {
    	if(getIDEN1_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_color();
    	}
    }
    public void setIDEN1_color(String IDEN1_color) {
    	if(IDEN1_color == null) {
    		this.IDEN1_color = null;
    	} else {
    		this.IDEN1_color = IDEN1_color;
    	}
    	this.IDEN1_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_hilight
     * Comments    : 
     */	
    private String IDEN1_hilight = null;
    
    private transient boolean IDEN1_hilight_nullable = true;
    
    public boolean isNullableIDEN1_hilight() {
    	return this.IDEN1_hilight_nullable;
    }
    
    private transient boolean IDEN1_hilight_invalidation = false;
    	
    public void setInvalidationIDEN1_hilight(boolean invalidation) { 
    	this.IDEN1_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_hilight() {
    	return this.IDEN1_hilight_invalidation;
    }
    	
    private transient boolean IDEN1_hilight_modified = false;
    
    public boolean isModifiedIDEN1_hilight() {
    	return this.IDEN1_hilight_modified;
    }
    	
    public void unModifiedIDEN1_hilight() {
    	this.IDEN1_hilight_modified = false;
    }
    public FieldProperty getIDEN1_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_hilight");
    }
    
    public String getIDEN1_hilight() {
    	return IDEN1_hilight;
    }	
    public String getNvlIDEN1_hilight() {
    	if(getIDEN1_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_hilight();
    	}
    }
    public void setIDEN1_hilight(String IDEN1_hilight) {
    	if(IDEN1_hilight == null) {
    		this.IDEN1_hilight = null;
    	} else {
    		this.IDEN1_hilight = IDEN1_hilight;
    	}
    	this.IDEN1_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_outline
     * Comments    : 
     */	
    private String IDEN1_outline = null;
    
    private transient boolean IDEN1_outline_nullable = true;
    
    public boolean isNullableIDEN1_outline() {
    	return this.IDEN1_outline_nullable;
    }
    
    private transient boolean IDEN1_outline_invalidation = false;
    	
    public void setInvalidationIDEN1_outline(boolean invalidation) { 
    	this.IDEN1_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_outline() {
    	return this.IDEN1_outline_invalidation;
    }
    	
    private transient boolean IDEN1_outline_modified = false;
    
    public boolean isModifiedIDEN1_outline() {
    	return this.IDEN1_outline_modified;
    }
    	
    public void unModifiedIDEN1_outline() {
    	this.IDEN1_outline_modified = false;
    }
    public FieldProperty getIDEN1_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_outline");
    }
    
    public String getIDEN1_outline() {
    	return IDEN1_outline;
    }	
    public String getNvlIDEN1_outline() {
    	if(getIDEN1_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_outline();
    	}
    }
    public void setIDEN1_outline(String IDEN1_outline) {
    	if(IDEN1_outline == null) {
    		this.IDEN1_outline = null;
    	} else {
    		this.IDEN1_outline = IDEN1_outline;
    	}
    	this.IDEN1_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_transp
     * Comments    : 
     */	
    private String IDEN1_transp = null;
    
    private transient boolean IDEN1_transp_nullable = true;
    
    public boolean isNullableIDEN1_transp() {
    	return this.IDEN1_transp_nullable;
    }
    
    private transient boolean IDEN1_transp_invalidation = false;
    	
    public void setInvalidationIDEN1_transp(boolean invalidation) { 
    	this.IDEN1_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_transp() {
    	return this.IDEN1_transp_invalidation;
    }
    	
    private transient boolean IDEN1_transp_modified = false;
    
    public boolean isModifiedIDEN1_transp() {
    	return this.IDEN1_transp_modified;
    }
    	
    public void unModifiedIDEN1_transp() {
    	this.IDEN1_transp_modified = false;
    }
    public FieldProperty getIDEN1_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_transp");
    }
    
    public String getIDEN1_transp() {
    	return IDEN1_transp;
    }	
    public String getNvlIDEN1_transp() {
    	if(getIDEN1_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_transp();
    	}
    }
    public void setIDEN1_transp(String IDEN1_transp) {
    	if(IDEN1_transp == null) {
    		this.IDEN1_transp = null;
    	} else {
    		this.IDEN1_transp = IDEN1_transp;
    	}
    	this.IDEN1_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_validn
     * Comments    : 
     */	
    private String IDEN1_validn = null;
    
    private transient boolean IDEN1_validn_nullable = true;
    
    public boolean isNullableIDEN1_validn() {
    	return this.IDEN1_validn_nullable;
    }
    
    private transient boolean IDEN1_validn_invalidation = false;
    	
    public void setInvalidationIDEN1_validn(boolean invalidation) { 
    	this.IDEN1_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_validn() {
    	return this.IDEN1_validn_invalidation;
    }
    	
    private transient boolean IDEN1_validn_modified = false;
    
    public boolean isModifiedIDEN1_validn() {
    	return this.IDEN1_validn_modified;
    }
    	
    public void unModifiedIDEN1_validn() {
    	this.IDEN1_validn_modified = false;
    }
    public FieldProperty getIDEN1_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_validn");
    }
    
    public String getIDEN1_validn() {
    	return IDEN1_validn;
    }	
    public String getNvlIDEN1_validn() {
    	if(getIDEN1_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_validn();
    	}
    }
    public void setIDEN1_validn(String IDEN1_validn) {
    	if(IDEN1_validn == null) {
    		this.IDEN1_validn = null;
    	} else {
    		this.IDEN1_validn = IDEN1_validn;
    	}
    	this.IDEN1_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_sosi
     * Comments    : 
     */	
    private String IDEN1_sosi = null;
    
    private transient boolean IDEN1_sosi_nullable = true;
    
    public boolean isNullableIDEN1_sosi() {
    	return this.IDEN1_sosi_nullable;
    }
    
    private transient boolean IDEN1_sosi_invalidation = false;
    	
    public void setInvalidationIDEN1_sosi(boolean invalidation) { 
    	this.IDEN1_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_sosi() {
    	return this.IDEN1_sosi_invalidation;
    }
    	
    private transient boolean IDEN1_sosi_modified = false;
    
    public boolean isModifiedIDEN1_sosi() {
    	return this.IDEN1_sosi_modified;
    }
    	
    public void unModifiedIDEN1_sosi() {
    	this.IDEN1_sosi_modified = false;
    }
    public FieldProperty getIDEN1_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_sosi");
    }
    
    public String getIDEN1_sosi() {
    	return IDEN1_sosi;
    }	
    public String getNvlIDEN1_sosi() {
    	if(getIDEN1_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_sosi();
    	}
    }
    public void setIDEN1_sosi(String IDEN1_sosi) {
    	if(IDEN1_sosi == null) {
    		this.IDEN1_sosi = null;
    	} else {
    		this.IDEN1_sosi = IDEN1_sosi;
    	}
    	this.IDEN1_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN1_ps
     * Comments    : 
     */	
    private String IDEN1_ps = null;
    
    private transient boolean IDEN1_ps_nullable = true;
    
    public boolean isNullableIDEN1_ps() {
    	return this.IDEN1_ps_nullable;
    }
    
    private transient boolean IDEN1_ps_invalidation = false;
    	
    public void setInvalidationIDEN1_ps(boolean invalidation) { 
    	this.IDEN1_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN1_ps() {
    	return this.IDEN1_ps_invalidation;
    }
    	
    private transient boolean IDEN1_ps_modified = false;
    
    public boolean isModifiedIDEN1_ps() {
    	return this.IDEN1_ps_modified;
    }
    	
    public void unModifiedIDEN1_ps() {
    	this.IDEN1_ps_modified = false;
    }
    public FieldProperty getIDEN1_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN1_ps");
    }
    
    public String getIDEN1_ps() {
    	return IDEN1_ps;
    }	
    public String getNvlIDEN1_ps() {
    	if(getIDEN1_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN1_ps();
    	}
    }
    public void setIDEN1_ps(String IDEN1_ps) {
    	if(IDEN1_ps == null) {
    		this.IDEN1_ps = null;
    	} else {
    		this.IDEN1_ps = IDEN1_ps;
    	}
    	this.IDEN1_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_length
     * Comments    : 
     */	
    private int NAME1_length = 0;
    
    private transient boolean NAME1_length_nullable = false;
    
    public boolean isNullableNAME1_length() {
    	return this.NAME1_length_nullable;
    }
    
    private transient boolean NAME1_length_invalidation = false;
    	
    public void setInvalidationNAME1_length(boolean invalidation) { 
    	this.NAME1_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_length() {
    	return this.NAME1_length_invalidation;
    }
    	
    private transient boolean NAME1_length_modified = false;
    
    public boolean isModifiedNAME1_length() {
    	return this.NAME1_length_modified;
    }
    	
    public void unModifiedNAME1_length() {
    	this.NAME1_length_modified = false;
    }
    public FieldProperty getNAME1_lengthFieldProperty() {
    	return fieldPropertyMap.get("NAME1_length");
    }
    
    public int getNAME1_length() {
    	return NAME1_length;
    }	
    public void setNAME1_length(int NAME1_length) {
    	this.NAME1_length = NAME1_length;
    	this.NAME1_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME1_length(Integer NAME1_length) {
    	if( NAME1_length == null) {
    		this.NAME1_length = 0;
    	} else{
    		this.NAME1_length = NAME1_length.intValue();
    	}
    	this.NAME1_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME1_length(String NAME1_length) {
    	if  (NAME1_length==null || NAME1_length.length() == 0) {
    		this.NAME1_length = 0;
    	} else {
    		this.NAME1_length = Integer.parseInt(NAME1_length);
    	}
    	this.NAME1_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_color
     * Comments    : 
     */	
    private String NAME1_color = null;
    
    private transient boolean NAME1_color_nullable = true;
    
    public boolean isNullableNAME1_color() {
    	return this.NAME1_color_nullable;
    }
    
    private transient boolean NAME1_color_invalidation = false;
    	
    public void setInvalidationNAME1_color(boolean invalidation) { 
    	this.NAME1_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_color() {
    	return this.NAME1_color_invalidation;
    }
    	
    private transient boolean NAME1_color_modified = false;
    
    public boolean isModifiedNAME1_color() {
    	return this.NAME1_color_modified;
    }
    	
    public void unModifiedNAME1_color() {
    	this.NAME1_color_modified = false;
    }
    public FieldProperty getNAME1_colorFieldProperty() {
    	return fieldPropertyMap.get("NAME1_color");
    }
    
    public String getNAME1_color() {
    	return NAME1_color;
    }	
    public String getNvlNAME1_color() {
    	if(getNAME1_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_color();
    	}
    }
    public void setNAME1_color(String NAME1_color) {
    	if(NAME1_color == null) {
    		this.NAME1_color = null;
    	} else {
    		this.NAME1_color = NAME1_color;
    	}
    	this.NAME1_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_hilight
     * Comments    : 
     */	
    private String NAME1_hilight = null;
    
    private transient boolean NAME1_hilight_nullable = true;
    
    public boolean isNullableNAME1_hilight() {
    	return this.NAME1_hilight_nullable;
    }
    
    private transient boolean NAME1_hilight_invalidation = false;
    	
    public void setInvalidationNAME1_hilight(boolean invalidation) { 
    	this.NAME1_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_hilight() {
    	return this.NAME1_hilight_invalidation;
    }
    	
    private transient boolean NAME1_hilight_modified = false;
    
    public boolean isModifiedNAME1_hilight() {
    	return this.NAME1_hilight_modified;
    }
    	
    public void unModifiedNAME1_hilight() {
    	this.NAME1_hilight_modified = false;
    }
    public FieldProperty getNAME1_hilightFieldProperty() {
    	return fieldPropertyMap.get("NAME1_hilight");
    }
    
    public String getNAME1_hilight() {
    	return NAME1_hilight;
    }	
    public String getNvlNAME1_hilight() {
    	if(getNAME1_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_hilight();
    	}
    }
    public void setNAME1_hilight(String NAME1_hilight) {
    	if(NAME1_hilight == null) {
    		this.NAME1_hilight = null;
    	} else {
    		this.NAME1_hilight = NAME1_hilight;
    	}
    	this.NAME1_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_outline
     * Comments    : 
     */	
    private String NAME1_outline = null;
    
    private transient boolean NAME1_outline_nullable = true;
    
    public boolean isNullableNAME1_outline() {
    	return this.NAME1_outline_nullable;
    }
    
    private transient boolean NAME1_outline_invalidation = false;
    	
    public void setInvalidationNAME1_outline(boolean invalidation) { 
    	this.NAME1_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_outline() {
    	return this.NAME1_outline_invalidation;
    }
    	
    private transient boolean NAME1_outline_modified = false;
    
    public boolean isModifiedNAME1_outline() {
    	return this.NAME1_outline_modified;
    }
    	
    public void unModifiedNAME1_outline() {
    	this.NAME1_outline_modified = false;
    }
    public FieldProperty getNAME1_outlineFieldProperty() {
    	return fieldPropertyMap.get("NAME1_outline");
    }
    
    public String getNAME1_outline() {
    	return NAME1_outline;
    }	
    public String getNvlNAME1_outline() {
    	if(getNAME1_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_outline();
    	}
    }
    public void setNAME1_outline(String NAME1_outline) {
    	if(NAME1_outline == null) {
    		this.NAME1_outline = null;
    	} else {
    		this.NAME1_outline = NAME1_outline;
    	}
    	this.NAME1_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_transp
     * Comments    : 
     */	
    private String NAME1_transp = null;
    
    private transient boolean NAME1_transp_nullable = true;
    
    public boolean isNullableNAME1_transp() {
    	return this.NAME1_transp_nullable;
    }
    
    private transient boolean NAME1_transp_invalidation = false;
    	
    public void setInvalidationNAME1_transp(boolean invalidation) { 
    	this.NAME1_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_transp() {
    	return this.NAME1_transp_invalidation;
    }
    	
    private transient boolean NAME1_transp_modified = false;
    
    public boolean isModifiedNAME1_transp() {
    	return this.NAME1_transp_modified;
    }
    	
    public void unModifiedNAME1_transp() {
    	this.NAME1_transp_modified = false;
    }
    public FieldProperty getNAME1_transpFieldProperty() {
    	return fieldPropertyMap.get("NAME1_transp");
    }
    
    public String getNAME1_transp() {
    	return NAME1_transp;
    }	
    public String getNvlNAME1_transp() {
    	if(getNAME1_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_transp();
    	}
    }
    public void setNAME1_transp(String NAME1_transp) {
    	if(NAME1_transp == null) {
    		this.NAME1_transp = null;
    	} else {
    		this.NAME1_transp = NAME1_transp;
    	}
    	this.NAME1_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_validn
     * Comments    : 
     */	
    private String NAME1_validn = null;
    
    private transient boolean NAME1_validn_nullable = true;
    
    public boolean isNullableNAME1_validn() {
    	return this.NAME1_validn_nullable;
    }
    
    private transient boolean NAME1_validn_invalidation = false;
    	
    public void setInvalidationNAME1_validn(boolean invalidation) { 
    	this.NAME1_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_validn() {
    	return this.NAME1_validn_invalidation;
    }
    	
    private transient boolean NAME1_validn_modified = false;
    
    public boolean isModifiedNAME1_validn() {
    	return this.NAME1_validn_modified;
    }
    	
    public void unModifiedNAME1_validn() {
    	this.NAME1_validn_modified = false;
    }
    public FieldProperty getNAME1_validnFieldProperty() {
    	return fieldPropertyMap.get("NAME1_validn");
    }
    
    public String getNAME1_validn() {
    	return NAME1_validn;
    }	
    public String getNvlNAME1_validn() {
    	if(getNAME1_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_validn();
    	}
    }
    public void setNAME1_validn(String NAME1_validn) {
    	if(NAME1_validn == null) {
    		this.NAME1_validn = null;
    	} else {
    		this.NAME1_validn = NAME1_validn;
    	}
    	this.NAME1_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_sosi
     * Comments    : 
     */	
    private String NAME1_sosi = null;
    
    private transient boolean NAME1_sosi_nullable = true;
    
    public boolean isNullableNAME1_sosi() {
    	return this.NAME1_sosi_nullable;
    }
    
    private transient boolean NAME1_sosi_invalidation = false;
    	
    public void setInvalidationNAME1_sosi(boolean invalidation) { 
    	this.NAME1_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_sosi() {
    	return this.NAME1_sosi_invalidation;
    }
    	
    private transient boolean NAME1_sosi_modified = false;
    
    public boolean isModifiedNAME1_sosi() {
    	return this.NAME1_sosi_modified;
    }
    	
    public void unModifiedNAME1_sosi() {
    	this.NAME1_sosi_modified = false;
    }
    public FieldProperty getNAME1_sosiFieldProperty() {
    	return fieldPropertyMap.get("NAME1_sosi");
    }
    
    public String getNAME1_sosi() {
    	return NAME1_sosi;
    }	
    public String getNvlNAME1_sosi() {
    	if(getNAME1_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_sosi();
    	}
    }
    public void setNAME1_sosi(String NAME1_sosi) {
    	if(NAME1_sosi == null) {
    		this.NAME1_sosi = null;
    	} else {
    		this.NAME1_sosi = NAME1_sosi;
    	}
    	this.NAME1_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME1_ps
     * Comments    : 
     */	
    private String NAME1_ps = null;
    
    private transient boolean NAME1_ps_nullable = true;
    
    public boolean isNullableNAME1_ps() {
    	return this.NAME1_ps_nullable;
    }
    
    private transient boolean NAME1_ps_invalidation = false;
    	
    public void setInvalidationNAME1_ps(boolean invalidation) { 
    	this.NAME1_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME1_ps() {
    	return this.NAME1_ps_invalidation;
    }
    	
    private transient boolean NAME1_ps_modified = false;
    
    public boolean isModifiedNAME1_ps() {
    	return this.NAME1_ps_modified;
    }
    	
    public void unModifiedNAME1_ps() {
    	this.NAME1_ps_modified = false;
    }
    public FieldProperty getNAME1_psFieldProperty() {
    	return fieldPropertyMap.get("NAME1_ps");
    }
    
    public String getNAME1_ps() {
    	return NAME1_ps;
    }	
    public String getNvlNAME1_ps() {
    	if(getNAME1_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME1_ps();
    	}
    }
    public void setNAME1_ps(String NAME1_ps) {
    	if(NAME1_ps == null) {
    		this.NAME1_ps = null;
    	} else {
    		this.NAME1_ps = NAME1_ps;
    	}
    	this.NAME1_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_length
     * Comments    : 
     */	
    private int DEPT1_length = 0;
    
    private transient boolean DEPT1_length_nullable = false;
    
    public boolean isNullableDEPT1_length() {
    	return this.DEPT1_length_nullable;
    }
    
    private transient boolean DEPT1_length_invalidation = false;
    	
    public void setInvalidationDEPT1_length(boolean invalidation) { 
    	this.DEPT1_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_length() {
    	return this.DEPT1_length_invalidation;
    }
    	
    private transient boolean DEPT1_length_modified = false;
    
    public boolean isModifiedDEPT1_length() {
    	return this.DEPT1_length_modified;
    }
    	
    public void unModifiedDEPT1_length() {
    	this.DEPT1_length_modified = false;
    }
    public FieldProperty getDEPT1_lengthFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_length");
    }
    
    public int getDEPT1_length() {
    	return DEPT1_length;
    }	
    public void setDEPT1_length(int DEPT1_length) {
    	this.DEPT1_length = DEPT1_length;
    	this.DEPT1_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT1_length(Integer DEPT1_length) {
    	if( DEPT1_length == null) {
    		this.DEPT1_length = 0;
    	} else{
    		this.DEPT1_length = DEPT1_length.intValue();
    	}
    	this.DEPT1_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT1_length(String DEPT1_length) {
    	if  (DEPT1_length==null || DEPT1_length.length() == 0) {
    		this.DEPT1_length = 0;
    	} else {
    		this.DEPT1_length = Integer.parseInt(DEPT1_length);
    	}
    	this.DEPT1_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_color
     * Comments    : 
     */	
    private String DEPT1_color = null;
    
    private transient boolean DEPT1_color_nullable = true;
    
    public boolean isNullableDEPT1_color() {
    	return this.DEPT1_color_nullable;
    }
    
    private transient boolean DEPT1_color_invalidation = false;
    	
    public void setInvalidationDEPT1_color(boolean invalidation) { 
    	this.DEPT1_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_color() {
    	return this.DEPT1_color_invalidation;
    }
    	
    private transient boolean DEPT1_color_modified = false;
    
    public boolean isModifiedDEPT1_color() {
    	return this.DEPT1_color_modified;
    }
    	
    public void unModifiedDEPT1_color() {
    	this.DEPT1_color_modified = false;
    }
    public FieldProperty getDEPT1_colorFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_color");
    }
    
    public String getDEPT1_color() {
    	return DEPT1_color;
    }	
    public String getNvlDEPT1_color() {
    	if(getDEPT1_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_color();
    	}
    }
    public void setDEPT1_color(String DEPT1_color) {
    	if(DEPT1_color == null) {
    		this.DEPT1_color = null;
    	} else {
    		this.DEPT1_color = DEPT1_color;
    	}
    	this.DEPT1_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_hilight
     * Comments    : 
     */	
    private String DEPT1_hilight = null;
    
    private transient boolean DEPT1_hilight_nullable = true;
    
    public boolean isNullableDEPT1_hilight() {
    	return this.DEPT1_hilight_nullable;
    }
    
    private transient boolean DEPT1_hilight_invalidation = false;
    	
    public void setInvalidationDEPT1_hilight(boolean invalidation) { 
    	this.DEPT1_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_hilight() {
    	return this.DEPT1_hilight_invalidation;
    }
    	
    private transient boolean DEPT1_hilight_modified = false;
    
    public boolean isModifiedDEPT1_hilight() {
    	return this.DEPT1_hilight_modified;
    }
    	
    public void unModifiedDEPT1_hilight() {
    	this.DEPT1_hilight_modified = false;
    }
    public FieldProperty getDEPT1_hilightFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_hilight");
    }
    
    public String getDEPT1_hilight() {
    	return DEPT1_hilight;
    }	
    public String getNvlDEPT1_hilight() {
    	if(getDEPT1_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_hilight();
    	}
    }
    public void setDEPT1_hilight(String DEPT1_hilight) {
    	if(DEPT1_hilight == null) {
    		this.DEPT1_hilight = null;
    	} else {
    		this.DEPT1_hilight = DEPT1_hilight;
    	}
    	this.DEPT1_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_outline
     * Comments    : 
     */	
    private String DEPT1_outline = null;
    
    private transient boolean DEPT1_outline_nullable = true;
    
    public boolean isNullableDEPT1_outline() {
    	return this.DEPT1_outline_nullable;
    }
    
    private transient boolean DEPT1_outline_invalidation = false;
    	
    public void setInvalidationDEPT1_outline(boolean invalidation) { 
    	this.DEPT1_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_outline() {
    	return this.DEPT1_outline_invalidation;
    }
    	
    private transient boolean DEPT1_outline_modified = false;
    
    public boolean isModifiedDEPT1_outline() {
    	return this.DEPT1_outline_modified;
    }
    	
    public void unModifiedDEPT1_outline() {
    	this.DEPT1_outline_modified = false;
    }
    public FieldProperty getDEPT1_outlineFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_outline");
    }
    
    public String getDEPT1_outline() {
    	return DEPT1_outline;
    }	
    public String getNvlDEPT1_outline() {
    	if(getDEPT1_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_outline();
    	}
    }
    public void setDEPT1_outline(String DEPT1_outline) {
    	if(DEPT1_outline == null) {
    		this.DEPT1_outline = null;
    	} else {
    		this.DEPT1_outline = DEPT1_outline;
    	}
    	this.DEPT1_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_transp
     * Comments    : 
     */	
    private String DEPT1_transp = null;
    
    private transient boolean DEPT1_transp_nullable = true;
    
    public boolean isNullableDEPT1_transp() {
    	return this.DEPT1_transp_nullable;
    }
    
    private transient boolean DEPT1_transp_invalidation = false;
    	
    public void setInvalidationDEPT1_transp(boolean invalidation) { 
    	this.DEPT1_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_transp() {
    	return this.DEPT1_transp_invalidation;
    }
    	
    private transient boolean DEPT1_transp_modified = false;
    
    public boolean isModifiedDEPT1_transp() {
    	return this.DEPT1_transp_modified;
    }
    	
    public void unModifiedDEPT1_transp() {
    	this.DEPT1_transp_modified = false;
    }
    public FieldProperty getDEPT1_transpFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_transp");
    }
    
    public String getDEPT1_transp() {
    	return DEPT1_transp;
    }	
    public String getNvlDEPT1_transp() {
    	if(getDEPT1_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_transp();
    	}
    }
    public void setDEPT1_transp(String DEPT1_transp) {
    	if(DEPT1_transp == null) {
    		this.DEPT1_transp = null;
    	} else {
    		this.DEPT1_transp = DEPT1_transp;
    	}
    	this.DEPT1_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_validn
     * Comments    : 
     */	
    private String DEPT1_validn = null;
    
    private transient boolean DEPT1_validn_nullable = true;
    
    public boolean isNullableDEPT1_validn() {
    	return this.DEPT1_validn_nullable;
    }
    
    private transient boolean DEPT1_validn_invalidation = false;
    	
    public void setInvalidationDEPT1_validn(boolean invalidation) { 
    	this.DEPT1_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_validn() {
    	return this.DEPT1_validn_invalidation;
    }
    	
    private transient boolean DEPT1_validn_modified = false;
    
    public boolean isModifiedDEPT1_validn() {
    	return this.DEPT1_validn_modified;
    }
    	
    public void unModifiedDEPT1_validn() {
    	this.DEPT1_validn_modified = false;
    }
    public FieldProperty getDEPT1_validnFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_validn");
    }
    
    public String getDEPT1_validn() {
    	return DEPT1_validn;
    }	
    public String getNvlDEPT1_validn() {
    	if(getDEPT1_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_validn();
    	}
    }
    public void setDEPT1_validn(String DEPT1_validn) {
    	if(DEPT1_validn == null) {
    		this.DEPT1_validn = null;
    	} else {
    		this.DEPT1_validn = DEPT1_validn;
    	}
    	this.DEPT1_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_sosi
     * Comments    : 
     */	
    private String DEPT1_sosi = null;
    
    private transient boolean DEPT1_sosi_nullable = true;
    
    public boolean isNullableDEPT1_sosi() {
    	return this.DEPT1_sosi_nullable;
    }
    
    private transient boolean DEPT1_sosi_invalidation = false;
    	
    public void setInvalidationDEPT1_sosi(boolean invalidation) { 
    	this.DEPT1_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_sosi() {
    	return this.DEPT1_sosi_invalidation;
    }
    	
    private transient boolean DEPT1_sosi_modified = false;
    
    public boolean isModifiedDEPT1_sosi() {
    	return this.DEPT1_sosi_modified;
    }
    	
    public void unModifiedDEPT1_sosi() {
    	this.DEPT1_sosi_modified = false;
    }
    public FieldProperty getDEPT1_sosiFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_sosi");
    }
    
    public String getDEPT1_sosi() {
    	return DEPT1_sosi;
    }	
    public String getNvlDEPT1_sosi() {
    	if(getDEPT1_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_sosi();
    	}
    }
    public void setDEPT1_sosi(String DEPT1_sosi) {
    	if(DEPT1_sosi == null) {
    		this.DEPT1_sosi = null;
    	} else {
    		this.DEPT1_sosi = DEPT1_sosi;
    	}
    	this.DEPT1_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT1_ps
     * Comments    : 
     */	
    private String DEPT1_ps = null;
    
    private transient boolean DEPT1_ps_nullable = true;
    
    public boolean isNullableDEPT1_ps() {
    	return this.DEPT1_ps_nullable;
    }
    
    private transient boolean DEPT1_ps_invalidation = false;
    	
    public void setInvalidationDEPT1_ps(boolean invalidation) { 
    	this.DEPT1_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT1_ps() {
    	return this.DEPT1_ps_invalidation;
    }
    	
    private transient boolean DEPT1_ps_modified = false;
    
    public boolean isModifiedDEPT1_ps() {
    	return this.DEPT1_ps_modified;
    }
    	
    public void unModifiedDEPT1_ps() {
    	this.DEPT1_ps_modified = false;
    }
    public FieldProperty getDEPT1_psFieldProperty() {
    	return fieldPropertyMap.get("DEPT1_ps");
    }
    
    public String getDEPT1_ps() {
    	return DEPT1_ps;
    }	
    public String getNvlDEPT1_ps() {
    	if(getDEPT1_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT1_ps();
    	}
    }
    public void setDEPT1_ps(String DEPT1_ps) {
    	if(DEPT1_ps == null) {
    		this.DEPT1_ps = null;
    	} else {
    		this.DEPT1_ps = DEPT1_ps;
    	}
    	this.DEPT1_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_length
     * Comments    : 
     */	
    private int PHON1_length = 0;
    
    private transient boolean PHON1_length_nullable = false;
    
    public boolean isNullablePHON1_length() {
    	return this.PHON1_length_nullable;
    }
    
    private transient boolean PHON1_length_invalidation = false;
    	
    public void setInvalidationPHON1_length(boolean invalidation) { 
    	this.PHON1_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_length() {
    	return this.PHON1_length_invalidation;
    }
    	
    private transient boolean PHON1_length_modified = false;
    
    public boolean isModifiedPHON1_length() {
    	return this.PHON1_length_modified;
    }
    	
    public void unModifiedPHON1_length() {
    	this.PHON1_length_modified = false;
    }
    public FieldProperty getPHON1_lengthFieldProperty() {
    	return fieldPropertyMap.get("PHON1_length");
    }
    
    public int getPHON1_length() {
    	return PHON1_length;
    }	
    public void setPHON1_length(int PHON1_length) {
    	this.PHON1_length = PHON1_length;
    	this.PHON1_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON1_length(Integer PHON1_length) {
    	if( PHON1_length == null) {
    		this.PHON1_length = 0;
    	} else{
    		this.PHON1_length = PHON1_length.intValue();
    	}
    	this.PHON1_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON1_length(String PHON1_length) {
    	if  (PHON1_length==null || PHON1_length.length() == 0) {
    		this.PHON1_length = 0;
    	} else {
    		this.PHON1_length = Integer.parseInt(PHON1_length);
    	}
    	this.PHON1_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_color
     * Comments    : 
     */	
    private String PHON1_color = null;
    
    private transient boolean PHON1_color_nullable = true;
    
    public boolean isNullablePHON1_color() {
    	return this.PHON1_color_nullable;
    }
    
    private transient boolean PHON1_color_invalidation = false;
    	
    public void setInvalidationPHON1_color(boolean invalidation) { 
    	this.PHON1_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_color() {
    	return this.PHON1_color_invalidation;
    }
    	
    private transient boolean PHON1_color_modified = false;
    
    public boolean isModifiedPHON1_color() {
    	return this.PHON1_color_modified;
    }
    	
    public void unModifiedPHON1_color() {
    	this.PHON1_color_modified = false;
    }
    public FieldProperty getPHON1_colorFieldProperty() {
    	return fieldPropertyMap.get("PHON1_color");
    }
    
    public String getPHON1_color() {
    	return PHON1_color;
    }	
    public String getNvlPHON1_color() {
    	if(getPHON1_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_color();
    	}
    }
    public void setPHON1_color(String PHON1_color) {
    	if(PHON1_color == null) {
    		this.PHON1_color = null;
    	} else {
    		this.PHON1_color = PHON1_color;
    	}
    	this.PHON1_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_hilight
     * Comments    : 
     */	
    private String PHON1_hilight = null;
    
    private transient boolean PHON1_hilight_nullable = true;
    
    public boolean isNullablePHON1_hilight() {
    	return this.PHON1_hilight_nullable;
    }
    
    private transient boolean PHON1_hilight_invalidation = false;
    	
    public void setInvalidationPHON1_hilight(boolean invalidation) { 
    	this.PHON1_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_hilight() {
    	return this.PHON1_hilight_invalidation;
    }
    	
    private transient boolean PHON1_hilight_modified = false;
    
    public boolean isModifiedPHON1_hilight() {
    	return this.PHON1_hilight_modified;
    }
    	
    public void unModifiedPHON1_hilight() {
    	this.PHON1_hilight_modified = false;
    }
    public FieldProperty getPHON1_hilightFieldProperty() {
    	return fieldPropertyMap.get("PHON1_hilight");
    }
    
    public String getPHON1_hilight() {
    	return PHON1_hilight;
    }	
    public String getNvlPHON1_hilight() {
    	if(getPHON1_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_hilight();
    	}
    }
    public void setPHON1_hilight(String PHON1_hilight) {
    	if(PHON1_hilight == null) {
    		this.PHON1_hilight = null;
    	} else {
    		this.PHON1_hilight = PHON1_hilight;
    	}
    	this.PHON1_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_outline
     * Comments    : 
     */	
    private String PHON1_outline = null;
    
    private transient boolean PHON1_outline_nullable = true;
    
    public boolean isNullablePHON1_outline() {
    	return this.PHON1_outline_nullable;
    }
    
    private transient boolean PHON1_outline_invalidation = false;
    	
    public void setInvalidationPHON1_outline(boolean invalidation) { 
    	this.PHON1_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_outline() {
    	return this.PHON1_outline_invalidation;
    }
    	
    private transient boolean PHON1_outline_modified = false;
    
    public boolean isModifiedPHON1_outline() {
    	return this.PHON1_outline_modified;
    }
    	
    public void unModifiedPHON1_outline() {
    	this.PHON1_outline_modified = false;
    }
    public FieldProperty getPHON1_outlineFieldProperty() {
    	return fieldPropertyMap.get("PHON1_outline");
    }
    
    public String getPHON1_outline() {
    	return PHON1_outline;
    }	
    public String getNvlPHON1_outline() {
    	if(getPHON1_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_outline();
    	}
    }
    public void setPHON1_outline(String PHON1_outline) {
    	if(PHON1_outline == null) {
    		this.PHON1_outline = null;
    	} else {
    		this.PHON1_outline = PHON1_outline;
    	}
    	this.PHON1_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_transp
     * Comments    : 
     */	
    private String PHON1_transp = null;
    
    private transient boolean PHON1_transp_nullable = true;
    
    public boolean isNullablePHON1_transp() {
    	return this.PHON1_transp_nullable;
    }
    
    private transient boolean PHON1_transp_invalidation = false;
    	
    public void setInvalidationPHON1_transp(boolean invalidation) { 
    	this.PHON1_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_transp() {
    	return this.PHON1_transp_invalidation;
    }
    	
    private transient boolean PHON1_transp_modified = false;
    
    public boolean isModifiedPHON1_transp() {
    	return this.PHON1_transp_modified;
    }
    	
    public void unModifiedPHON1_transp() {
    	this.PHON1_transp_modified = false;
    }
    public FieldProperty getPHON1_transpFieldProperty() {
    	return fieldPropertyMap.get("PHON1_transp");
    }
    
    public String getPHON1_transp() {
    	return PHON1_transp;
    }	
    public String getNvlPHON1_transp() {
    	if(getPHON1_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_transp();
    	}
    }
    public void setPHON1_transp(String PHON1_transp) {
    	if(PHON1_transp == null) {
    		this.PHON1_transp = null;
    	} else {
    		this.PHON1_transp = PHON1_transp;
    	}
    	this.PHON1_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_validn
     * Comments    : 
     */	
    private String PHON1_validn = null;
    
    private transient boolean PHON1_validn_nullable = true;
    
    public boolean isNullablePHON1_validn() {
    	return this.PHON1_validn_nullable;
    }
    
    private transient boolean PHON1_validn_invalidation = false;
    	
    public void setInvalidationPHON1_validn(boolean invalidation) { 
    	this.PHON1_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_validn() {
    	return this.PHON1_validn_invalidation;
    }
    	
    private transient boolean PHON1_validn_modified = false;
    
    public boolean isModifiedPHON1_validn() {
    	return this.PHON1_validn_modified;
    }
    	
    public void unModifiedPHON1_validn() {
    	this.PHON1_validn_modified = false;
    }
    public FieldProperty getPHON1_validnFieldProperty() {
    	return fieldPropertyMap.get("PHON1_validn");
    }
    
    public String getPHON1_validn() {
    	return PHON1_validn;
    }	
    public String getNvlPHON1_validn() {
    	if(getPHON1_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_validn();
    	}
    }
    public void setPHON1_validn(String PHON1_validn) {
    	if(PHON1_validn == null) {
    		this.PHON1_validn = null;
    	} else {
    		this.PHON1_validn = PHON1_validn;
    	}
    	this.PHON1_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_sosi
     * Comments    : 
     */	
    private String PHON1_sosi = null;
    
    private transient boolean PHON1_sosi_nullable = true;
    
    public boolean isNullablePHON1_sosi() {
    	return this.PHON1_sosi_nullable;
    }
    
    private transient boolean PHON1_sosi_invalidation = false;
    	
    public void setInvalidationPHON1_sosi(boolean invalidation) { 
    	this.PHON1_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_sosi() {
    	return this.PHON1_sosi_invalidation;
    }
    	
    private transient boolean PHON1_sosi_modified = false;
    
    public boolean isModifiedPHON1_sosi() {
    	return this.PHON1_sosi_modified;
    }
    	
    public void unModifiedPHON1_sosi() {
    	this.PHON1_sosi_modified = false;
    }
    public FieldProperty getPHON1_sosiFieldProperty() {
    	return fieldPropertyMap.get("PHON1_sosi");
    }
    
    public String getPHON1_sosi() {
    	return PHON1_sosi;
    }	
    public String getNvlPHON1_sosi() {
    	if(getPHON1_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_sosi();
    	}
    }
    public void setPHON1_sosi(String PHON1_sosi) {
    	if(PHON1_sosi == null) {
    		this.PHON1_sosi = null;
    	} else {
    		this.PHON1_sosi = PHON1_sosi;
    	}
    	this.PHON1_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON1_ps
     * Comments    : 
     */	
    private String PHON1_ps = null;
    
    private transient boolean PHON1_ps_nullable = true;
    
    public boolean isNullablePHON1_ps() {
    	return this.PHON1_ps_nullable;
    }
    
    private transient boolean PHON1_ps_invalidation = false;
    	
    public void setInvalidationPHON1_ps(boolean invalidation) { 
    	this.PHON1_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON1_ps() {
    	return this.PHON1_ps_invalidation;
    }
    	
    private transient boolean PHON1_ps_modified = false;
    
    public boolean isModifiedPHON1_ps() {
    	return this.PHON1_ps_modified;
    }
    	
    public void unModifiedPHON1_ps() {
    	this.PHON1_ps_modified = false;
    }
    public FieldProperty getPHON1_psFieldProperty() {
    	return fieldPropertyMap.get("PHON1_ps");
    }
    
    public String getPHON1_ps() {
    	return PHON1_ps;
    }	
    public String getNvlPHON1_ps() {
    	if(getPHON1_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON1_ps();
    	}
    }
    public void setPHON1_ps(String PHON1_ps) {
    	if(PHON1_ps == null) {
    		this.PHON1_ps = null;
    	} else {
    		this.PHON1_ps = PHON1_ps;
    	}
    	this.PHON1_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_length
     * Comments    : 
     */	
    private int IDEN2_length = 0;
    
    private transient boolean IDEN2_length_nullable = false;
    
    public boolean isNullableIDEN2_length() {
    	return this.IDEN2_length_nullable;
    }
    
    private transient boolean IDEN2_length_invalidation = false;
    	
    public void setInvalidationIDEN2_length(boolean invalidation) { 
    	this.IDEN2_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_length() {
    	return this.IDEN2_length_invalidation;
    }
    	
    private transient boolean IDEN2_length_modified = false;
    
    public boolean isModifiedIDEN2_length() {
    	return this.IDEN2_length_modified;
    }
    	
    public void unModifiedIDEN2_length() {
    	this.IDEN2_length_modified = false;
    }
    public FieldProperty getIDEN2_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_length");
    }
    
    public int getIDEN2_length() {
    	return IDEN2_length;
    }	
    public void setIDEN2_length(int IDEN2_length) {
    	this.IDEN2_length = IDEN2_length;
    	this.IDEN2_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN2_length(Integer IDEN2_length) {
    	if( IDEN2_length == null) {
    		this.IDEN2_length = 0;
    	} else{
    		this.IDEN2_length = IDEN2_length.intValue();
    	}
    	this.IDEN2_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN2_length(String IDEN2_length) {
    	if  (IDEN2_length==null || IDEN2_length.length() == 0) {
    		this.IDEN2_length = 0;
    	} else {
    		this.IDEN2_length = Integer.parseInt(IDEN2_length);
    	}
    	this.IDEN2_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_color
     * Comments    : 
     */	
    private String IDEN2_color = null;
    
    private transient boolean IDEN2_color_nullable = true;
    
    public boolean isNullableIDEN2_color() {
    	return this.IDEN2_color_nullable;
    }
    
    private transient boolean IDEN2_color_invalidation = false;
    	
    public void setInvalidationIDEN2_color(boolean invalidation) { 
    	this.IDEN2_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_color() {
    	return this.IDEN2_color_invalidation;
    }
    	
    private transient boolean IDEN2_color_modified = false;
    
    public boolean isModifiedIDEN2_color() {
    	return this.IDEN2_color_modified;
    }
    	
    public void unModifiedIDEN2_color() {
    	this.IDEN2_color_modified = false;
    }
    public FieldProperty getIDEN2_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_color");
    }
    
    public String getIDEN2_color() {
    	return IDEN2_color;
    }	
    public String getNvlIDEN2_color() {
    	if(getIDEN2_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_color();
    	}
    }
    public void setIDEN2_color(String IDEN2_color) {
    	if(IDEN2_color == null) {
    		this.IDEN2_color = null;
    	} else {
    		this.IDEN2_color = IDEN2_color;
    	}
    	this.IDEN2_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_hilight
     * Comments    : 
     */	
    private String IDEN2_hilight = null;
    
    private transient boolean IDEN2_hilight_nullable = true;
    
    public boolean isNullableIDEN2_hilight() {
    	return this.IDEN2_hilight_nullable;
    }
    
    private transient boolean IDEN2_hilight_invalidation = false;
    	
    public void setInvalidationIDEN2_hilight(boolean invalidation) { 
    	this.IDEN2_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_hilight() {
    	return this.IDEN2_hilight_invalidation;
    }
    	
    private transient boolean IDEN2_hilight_modified = false;
    
    public boolean isModifiedIDEN2_hilight() {
    	return this.IDEN2_hilight_modified;
    }
    	
    public void unModifiedIDEN2_hilight() {
    	this.IDEN2_hilight_modified = false;
    }
    public FieldProperty getIDEN2_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_hilight");
    }
    
    public String getIDEN2_hilight() {
    	return IDEN2_hilight;
    }	
    public String getNvlIDEN2_hilight() {
    	if(getIDEN2_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_hilight();
    	}
    }
    public void setIDEN2_hilight(String IDEN2_hilight) {
    	if(IDEN2_hilight == null) {
    		this.IDEN2_hilight = null;
    	} else {
    		this.IDEN2_hilight = IDEN2_hilight;
    	}
    	this.IDEN2_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_outline
     * Comments    : 
     */	
    private String IDEN2_outline = null;
    
    private transient boolean IDEN2_outline_nullable = true;
    
    public boolean isNullableIDEN2_outline() {
    	return this.IDEN2_outline_nullable;
    }
    
    private transient boolean IDEN2_outline_invalidation = false;
    	
    public void setInvalidationIDEN2_outline(boolean invalidation) { 
    	this.IDEN2_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_outline() {
    	return this.IDEN2_outline_invalidation;
    }
    	
    private transient boolean IDEN2_outline_modified = false;
    
    public boolean isModifiedIDEN2_outline() {
    	return this.IDEN2_outline_modified;
    }
    	
    public void unModifiedIDEN2_outline() {
    	this.IDEN2_outline_modified = false;
    }
    public FieldProperty getIDEN2_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_outline");
    }
    
    public String getIDEN2_outline() {
    	return IDEN2_outline;
    }	
    public String getNvlIDEN2_outline() {
    	if(getIDEN2_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_outline();
    	}
    }
    public void setIDEN2_outline(String IDEN2_outline) {
    	if(IDEN2_outline == null) {
    		this.IDEN2_outline = null;
    	} else {
    		this.IDEN2_outline = IDEN2_outline;
    	}
    	this.IDEN2_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_transp
     * Comments    : 
     */	
    private String IDEN2_transp = null;
    
    private transient boolean IDEN2_transp_nullable = true;
    
    public boolean isNullableIDEN2_transp() {
    	return this.IDEN2_transp_nullable;
    }
    
    private transient boolean IDEN2_transp_invalidation = false;
    	
    public void setInvalidationIDEN2_transp(boolean invalidation) { 
    	this.IDEN2_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_transp() {
    	return this.IDEN2_transp_invalidation;
    }
    	
    private transient boolean IDEN2_transp_modified = false;
    
    public boolean isModifiedIDEN2_transp() {
    	return this.IDEN2_transp_modified;
    }
    	
    public void unModifiedIDEN2_transp() {
    	this.IDEN2_transp_modified = false;
    }
    public FieldProperty getIDEN2_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_transp");
    }
    
    public String getIDEN2_transp() {
    	return IDEN2_transp;
    }	
    public String getNvlIDEN2_transp() {
    	if(getIDEN2_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_transp();
    	}
    }
    public void setIDEN2_transp(String IDEN2_transp) {
    	if(IDEN2_transp == null) {
    		this.IDEN2_transp = null;
    	} else {
    		this.IDEN2_transp = IDEN2_transp;
    	}
    	this.IDEN2_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_validn
     * Comments    : 
     */	
    private String IDEN2_validn = null;
    
    private transient boolean IDEN2_validn_nullable = true;
    
    public boolean isNullableIDEN2_validn() {
    	return this.IDEN2_validn_nullable;
    }
    
    private transient boolean IDEN2_validn_invalidation = false;
    	
    public void setInvalidationIDEN2_validn(boolean invalidation) { 
    	this.IDEN2_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_validn() {
    	return this.IDEN2_validn_invalidation;
    }
    	
    private transient boolean IDEN2_validn_modified = false;
    
    public boolean isModifiedIDEN2_validn() {
    	return this.IDEN2_validn_modified;
    }
    	
    public void unModifiedIDEN2_validn() {
    	this.IDEN2_validn_modified = false;
    }
    public FieldProperty getIDEN2_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_validn");
    }
    
    public String getIDEN2_validn() {
    	return IDEN2_validn;
    }	
    public String getNvlIDEN2_validn() {
    	if(getIDEN2_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_validn();
    	}
    }
    public void setIDEN2_validn(String IDEN2_validn) {
    	if(IDEN2_validn == null) {
    		this.IDEN2_validn = null;
    	} else {
    		this.IDEN2_validn = IDEN2_validn;
    	}
    	this.IDEN2_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_sosi
     * Comments    : 
     */	
    private String IDEN2_sosi = null;
    
    private transient boolean IDEN2_sosi_nullable = true;
    
    public boolean isNullableIDEN2_sosi() {
    	return this.IDEN2_sosi_nullable;
    }
    
    private transient boolean IDEN2_sosi_invalidation = false;
    	
    public void setInvalidationIDEN2_sosi(boolean invalidation) { 
    	this.IDEN2_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_sosi() {
    	return this.IDEN2_sosi_invalidation;
    }
    	
    private transient boolean IDEN2_sosi_modified = false;
    
    public boolean isModifiedIDEN2_sosi() {
    	return this.IDEN2_sosi_modified;
    }
    	
    public void unModifiedIDEN2_sosi() {
    	this.IDEN2_sosi_modified = false;
    }
    public FieldProperty getIDEN2_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_sosi");
    }
    
    public String getIDEN2_sosi() {
    	return IDEN2_sosi;
    }	
    public String getNvlIDEN2_sosi() {
    	if(getIDEN2_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_sosi();
    	}
    }
    public void setIDEN2_sosi(String IDEN2_sosi) {
    	if(IDEN2_sosi == null) {
    		this.IDEN2_sosi = null;
    	} else {
    		this.IDEN2_sosi = IDEN2_sosi;
    	}
    	this.IDEN2_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN2_ps
     * Comments    : 
     */	
    private String IDEN2_ps = null;
    
    private transient boolean IDEN2_ps_nullable = true;
    
    public boolean isNullableIDEN2_ps() {
    	return this.IDEN2_ps_nullable;
    }
    
    private transient boolean IDEN2_ps_invalidation = false;
    	
    public void setInvalidationIDEN2_ps(boolean invalidation) { 
    	this.IDEN2_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN2_ps() {
    	return this.IDEN2_ps_invalidation;
    }
    	
    private transient boolean IDEN2_ps_modified = false;
    
    public boolean isModifiedIDEN2_ps() {
    	return this.IDEN2_ps_modified;
    }
    	
    public void unModifiedIDEN2_ps() {
    	this.IDEN2_ps_modified = false;
    }
    public FieldProperty getIDEN2_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN2_ps");
    }
    
    public String getIDEN2_ps() {
    	return IDEN2_ps;
    }	
    public String getNvlIDEN2_ps() {
    	if(getIDEN2_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN2_ps();
    	}
    }
    public void setIDEN2_ps(String IDEN2_ps) {
    	if(IDEN2_ps == null) {
    		this.IDEN2_ps = null;
    	} else {
    		this.IDEN2_ps = IDEN2_ps;
    	}
    	this.IDEN2_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_length
     * Comments    : 
     */	
    private int NAME2_length = 0;
    
    private transient boolean NAME2_length_nullable = false;
    
    public boolean isNullableNAME2_length() {
    	return this.NAME2_length_nullable;
    }
    
    private transient boolean NAME2_length_invalidation = false;
    	
    public void setInvalidationNAME2_length(boolean invalidation) { 
    	this.NAME2_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_length() {
    	return this.NAME2_length_invalidation;
    }
    	
    private transient boolean NAME2_length_modified = false;
    
    public boolean isModifiedNAME2_length() {
    	return this.NAME2_length_modified;
    }
    	
    public void unModifiedNAME2_length() {
    	this.NAME2_length_modified = false;
    }
    public FieldProperty getNAME2_lengthFieldProperty() {
    	return fieldPropertyMap.get("NAME2_length");
    }
    
    public int getNAME2_length() {
    	return NAME2_length;
    }	
    public void setNAME2_length(int NAME2_length) {
    	this.NAME2_length = NAME2_length;
    	this.NAME2_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME2_length(Integer NAME2_length) {
    	if( NAME2_length == null) {
    		this.NAME2_length = 0;
    	} else{
    		this.NAME2_length = NAME2_length.intValue();
    	}
    	this.NAME2_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME2_length(String NAME2_length) {
    	if  (NAME2_length==null || NAME2_length.length() == 0) {
    		this.NAME2_length = 0;
    	} else {
    		this.NAME2_length = Integer.parseInt(NAME2_length);
    	}
    	this.NAME2_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_color
     * Comments    : 
     */	
    private String NAME2_color = null;
    
    private transient boolean NAME2_color_nullable = true;
    
    public boolean isNullableNAME2_color() {
    	return this.NAME2_color_nullable;
    }
    
    private transient boolean NAME2_color_invalidation = false;
    	
    public void setInvalidationNAME2_color(boolean invalidation) { 
    	this.NAME2_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_color() {
    	return this.NAME2_color_invalidation;
    }
    	
    private transient boolean NAME2_color_modified = false;
    
    public boolean isModifiedNAME2_color() {
    	return this.NAME2_color_modified;
    }
    	
    public void unModifiedNAME2_color() {
    	this.NAME2_color_modified = false;
    }
    public FieldProperty getNAME2_colorFieldProperty() {
    	return fieldPropertyMap.get("NAME2_color");
    }
    
    public String getNAME2_color() {
    	return NAME2_color;
    }	
    public String getNvlNAME2_color() {
    	if(getNAME2_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_color();
    	}
    }
    public void setNAME2_color(String NAME2_color) {
    	if(NAME2_color == null) {
    		this.NAME2_color = null;
    	} else {
    		this.NAME2_color = NAME2_color;
    	}
    	this.NAME2_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_hilight
     * Comments    : 
     */	
    private String NAME2_hilight = null;
    
    private transient boolean NAME2_hilight_nullable = true;
    
    public boolean isNullableNAME2_hilight() {
    	return this.NAME2_hilight_nullable;
    }
    
    private transient boolean NAME2_hilight_invalidation = false;
    	
    public void setInvalidationNAME2_hilight(boolean invalidation) { 
    	this.NAME2_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_hilight() {
    	return this.NAME2_hilight_invalidation;
    }
    	
    private transient boolean NAME2_hilight_modified = false;
    
    public boolean isModifiedNAME2_hilight() {
    	return this.NAME2_hilight_modified;
    }
    	
    public void unModifiedNAME2_hilight() {
    	this.NAME2_hilight_modified = false;
    }
    public FieldProperty getNAME2_hilightFieldProperty() {
    	return fieldPropertyMap.get("NAME2_hilight");
    }
    
    public String getNAME2_hilight() {
    	return NAME2_hilight;
    }	
    public String getNvlNAME2_hilight() {
    	if(getNAME2_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_hilight();
    	}
    }
    public void setNAME2_hilight(String NAME2_hilight) {
    	if(NAME2_hilight == null) {
    		this.NAME2_hilight = null;
    	} else {
    		this.NAME2_hilight = NAME2_hilight;
    	}
    	this.NAME2_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_outline
     * Comments    : 
     */	
    private String NAME2_outline = null;
    
    private transient boolean NAME2_outline_nullable = true;
    
    public boolean isNullableNAME2_outline() {
    	return this.NAME2_outline_nullable;
    }
    
    private transient boolean NAME2_outline_invalidation = false;
    	
    public void setInvalidationNAME2_outline(boolean invalidation) { 
    	this.NAME2_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_outline() {
    	return this.NAME2_outline_invalidation;
    }
    	
    private transient boolean NAME2_outline_modified = false;
    
    public boolean isModifiedNAME2_outline() {
    	return this.NAME2_outline_modified;
    }
    	
    public void unModifiedNAME2_outline() {
    	this.NAME2_outline_modified = false;
    }
    public FieldProperty getNAME2_outlineFieldProperty() {
    	return fieldPropertyMap.get("NAME2_outline");
    }
    
    public String getNAME2_outline() {
    	return NAME2_outline;
    }	
    public String getNvlNAME2_outline() {
    	if(getNAME2_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_outline();
    	}
    }
    public void setNAME2_outline(String NAME2_outline) {
    	if(NAME2_outline == null) {
    		this.NAME2_outline = null;
    	} else {
    		this.NAME2_outline = NAME2_outline;
    	}
    	this.NAME2_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_transp
     * Comments    : 
     */	
    private String NAME2_transp = null;
    
    private transient boolean NAME2_transp_nullable = true;
    
    public boolean isNullableNAME2_transp() {
    	return this.NAME2_transp_nullable;
    }
    
    private transient boolean NAME2_transp_invalidation = false;
    	
    public void setInvalidationNAME2_transp(boolean invalidation) { 
    	this.NAME2_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_transp() {
    	return this.NAME2_transp_invalidation;
    }
    	
    private transient boolean NAME2_transp_modified = false;
    
    public boolean isModifiedNAME2_transp() {
    	return this.NAME2_transp_modified;
    }
    	
    public void unModifiedNAME2_transp() {
    	this.NAME2_transp_modified = false;
    }
    public FieldProperty getNAME2_transpFieldProperty() {
    	return fieldPropertyMap.get("NAME2_transp");
    }
    
    public String getNAME2_transp() {
    	return NAME2_transp;
    }	
    public String getNvlNAME2_transp() {
    	if(getNAME2_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_transp();
    	}
    }
    public void setNAME2_transp(String NAME2_transp) {
    	if(NAME2_transp == null) {
    		this.NAME2_transp = null;
    	} else {
    		this.NAME2_transp = NAME2_transp;
    	}
    	this.NAME2_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_validn
     * Comments    : 
     */	
    private String NAME2_validn = null;
    
    private transient boolean NAME2_validn_nullable = true;
    
    public boolean isNullableNAME2_validn() {
    	return this.NAME2_validn_nullable;
    }
    
    private transient boolean NAME2_validn_invalidation = false;
    	
    public void setInvalidationNAME2_validn(boolean invalidation) { 
    	this.NAME2_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_validn() {
    	return this.NAME2_validn_invalidation;
    }
    	
    private transient boolean NAME2_validn_modified = false;
    
    public boolean isModifiedNAME2_validn() {
    	return this.NAME2_validn_modified;
    }
    	
    public void unModifiedNAME2_validn() {
    	this.NAME2_validn_modified = false;
    }
    public FieldProperty getNAME2_validnFieldProperty() {
    	return fieldPropertyMap.get("NAME2_validn");
    }
    
    public String getNAME2_validn() {
    	return NAME2_validn;
    }	
    public String getNvlNAME2_validn() {
    	if(getNAME2_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_validn();
    	}
    }
    public void setNAME2_validn(String NAME2_validn) {
    	if(NAME2_validn == null) {
    		this.NAME2_validn = null;
    	} else {
    		this.NAME2_validn = NAME2_validn;
    	}
    	this.NAME2_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_sosi
     * Comments    : 
     */	
    private String NAME2_sosi = null;
    
    private transient boolean NAME2_sosi_nullable = true;
    
    public boolean isNullableNAME2_sosi() {
    	return this.NAME2_sosi_nullable;
    }
    
    private transient boolean NAME2_sosi_invalidation = false;
    	
    public void setInvalidationNAME2_sosi(boolean invalidation) { 
    	this.NAME2_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_sosi() {
    	return this.NAME2_sosi_invalidation;
    }
    	
    private transient boolean NAME2_sosi_modified = false;
    
    public boolean isModifiedNAME2_sosi() {
    	return this.NAME2_sosi_modified;
    }
    	
    public void unModifiedNAME2_sosi() {
    	this.NAME2_sosi_modified = false;
    }
    public FieldProperty getNAME2_sosiFieldProperty() {
    	return fieldPropertyMap.get("NAME2_sosi");
    }
    
    public String getNAME2_sosi() {
    	return NAME2_sosi;
    }	
    public String getNvlNAME2_sosi() {
    	if(getNAME2_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_sosi();
    	}
    }
    public void setNAME2_sosi(String NAME2_sosi) {
    	if(NAME2_sosi == null) {
    		this.NAME2_sosi = null;
    	} else {
    		this.NAME2_sosi = NAME2_sosi;
    	}
    	this.NAME2_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME2_ps
     * Comments    : 
     */	
    private String NAME2_ps = null;
    
    private transient boolean NAME2_ps_nullable = true;
    
    public boolean isNullableNAME2_ps() {
    	return this.NAME2_ps_nullable;
    }
    
    private transient boolean NAME2_ps_invalidation = false;
    	
    public void setInvalidationNAME2_ps(boolean invalidation) { 
    	this.NAME2_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME2_ps() {
    	return this.NAME2_ps_invalidation;
    }
    	
    private transient boolean NAME2_ps_modified = false;
    
    public boolean isModifiedNAME2_ps() {
    	return this.NAME2_ps_modified;
    }
    	
    public void unModifiedNAME2_ps() {
    	this.NAME2_ps_modified = false;
    }
    public FieldProperty getNAME2_psFieldProperty() {
    	return fieldPropertyMap.get("NAME2_ps");
    }
    
    public String getNAME2_ps() {
    	return NAME2_ps;
    }	
    public String getNvlNAME2_ps() {
    	if(getNAME2_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME2_ps();
    	}
    }
    public void setNAME2_ps(String NAME2_ps) {
    	if(NAME2_ps == null) {
    		this.NAME2_ps = null;
    	} else {
    		this.NAME2_ps = NAME2_ps;
    	}
    	this.NAME2_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_length
     * Comments    : 
     */	
    private int DEPT2_length = 0;
    
    private transient boolean DEPT2_length_nullable = false;
    
    public boolean isNullableDEPT2_length() {
    	return this.DEPT2_length_nullable;
    }
    
    private transient boolean DEPT2_length_invalidation = false;
    	
    public void setInvalidationDEPT2_length(boolean invalidation) { 
    	this.DEPT2_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_length() {
    	return this.DEPT2_length_invalidation;
    }
    	
    private transient boolean DEPT2_length_modified = false;
    
    public boolean isModifiedDEPT2_length() {
    	return this.DEPT2_length_modified;
    }
    	
    public void unModifiedDEPT2_length() {
    	this.DEPT2_length_modified = false;
    }
    public FieldProperty getDEPT2_lengthFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_length");
    }
    
    public int getDEPT2_length() {
    	return DEPT2_length;
    }	
    public void setDEPT2_length(int DEPT2_length) {
    	this.DEPT2_length = DEPT2_length;
    	this.DEPT2_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT2_length(Integer DEPT2_length) {
    	if( DEPT2_length == null) {
    		this.DEPT2_length = 0;
    	} else{
    		this.DEPT2_length = DEPT2_length.intValue();
    	}
    	this.DEPT2_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT2_length(String DEPT2_length) {
    	if  (DEPT2_length==null || DEPT2_length.length() == 0) {
    		this.DEPT2_length = 0;
    	} else {
    		this.DEPT2_length = Integer.parseInt(DEPT2_length);
    	}
    	this.DEPT2_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_color
     * Comments    : 
     */	
    private String DEPT2_color = null;
    
    private transient boolean DEPT2_color_nullable = true;
    
    public boolean isNullableDEPT2_color() {
    	return this.DEPT2_color_nullable;
    }
    
    private transient boolean DEPT2_color_invalidation = false;
    	
    public void setInvalidationDEPT2_color(boolean invalidation) { 
    	this.DEPT2_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_color() {
    	return this.DEPT2_color_invalidation;
    }
    	
    private transient boolean DEPT2_color_modified = false;
    
    public boolean isModifiedDEPT2_color() {
    	return this.DEPT2_color_modified;
    }
    	
    public void unModifiedDEPT2_color() {
    	this.DEPT2_color_modified = false;
    }
    public FieldProperty getDEPT2_colorFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_color");
    }
    
    public String getDEPT2_color() {
    	return DEPT2_color;
    }	
    public String getNvlDEPT2_color() {
    	if(getDEPT2_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_color();
    	}
    }
    public void setDEPT2_color(String DEPT2_color) {
    	if(DEPT2_color == null) {
    		this.DEPT2_color = null;
    	} else {
    		this.DEPT2_color = DEPT2_color;
    	}
    	this.DEPT2_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_hilight
     * Comments    : 
     */	
    private String DEPT2_hilight = null;
    
    private transient boolean DEPT2_hilight_nullable = true;
    
    public boolean isNullableDEPT2_hilight() {
    	return this.DEPT2_hilight_nullable;
    }
    
    private transient boolean DEPT2_hilight_invalidation = false;
    	
    public void setInvalidationDEPT2_hilight(boolean invalidation) { 
    	this.DEPT2_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_hilight() {
    	return this.DEPT2_hilight_invalidation;
    }
    	
    private transient boolean DEPT2_hilight_modified = false;
    
    public boolean isModifiedDEPT2_hilight() {
    	return this.DEPT2_hilight_modified;
    }
    	
    public void unModifiedDEPT2_hilight() {
    	this.DEPT2_hilight_modified = false;
    }
    public FieldProperty getDEPT2_hilightFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_hilight");
    }
    
    public String getDEPT2_hilight() {
    	return DEPT2_hilight;
    }	
    public String getNvlDEPT2_hilight() {
    	if(getDEPT2_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_hilight();
    	}
    }
    public void setDEPT2_hilight(String DEPT2_hilight) {
    	if(DEPT2_hilight == null) {
    		this.DEPT2_hilight = null;
    	} else {
    		this.DEPT2_hilight = DEPT2_hilight;
    	}
    	this.DEPT2_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_outline
     * Comments    : 
     */	
    private String DEPT2_outline = null;
    
    private transient boolean DEPT2_outline_nullable = true;
    
    public boolean isNullableDEPT2_outline() {
    	return this.DEPT2_outline_nullable;
    }
    
    private transient boolean DEPT2_outline_invalidation = false;
    	
    public void setInvalidationDEPT2_outline(boolean invalidation) { 
    	this.DEPT2_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_outline() {
    	return this.DEPT2_outline_invalidation;
    }
    	
    private transient boolean DEPT2_outline_modified = false;
    
    public boolean isModifiedDEPT2_outline() {
    	return this.DEPT2_outline_modified;
    }
    	
    public void unModifiedDEPT2_outline() {
    	this.DEPT2_outline_modified = false;
    }
    public FieldProperty getDEPT2_outlineFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_outline");
    }
    
    public String getDEPT2_outline() {
    	return DEPT2_outline;
    }	
    public String getNvlDEPT2_outline() {
    	if(getDEPT2_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_outline();
    	}
    }
    public void setDEPT2_outline(String DEPT2_outline) {
    	if(DEPT2_outline == null) {
    		this.DEPT2_outline = null;
    	} else {
    		this.DEPT2_outline = DEPT2_outline;
    	}
    	this.DEPT2_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_transp
     * Comments    : 
     */	
    private String DEPT2_transp = null;
    
    private transient boolean DEPT2_transp_nullable = true;
    
    public boolean isNullableDEPT2_transp() {
    	return this.DEPT2_transp_nullable;
    }
    
    private transient boolean DEPT2_transp_invalidation = false;
    	
    public void setInvalidationDEPT2_transp(boolean invalidation) { 
    	this.DEPT2_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_transp() {
    	return this.DEPT2_transp_invalidation;
    }
    	
    private transient boolean DEPT2_transp_modified = false;
    
    public boolean isModifiedDEPT2_transp() {
    	return this.DEPT2_transp_modified;
    }
    	
    public void unModifiedDEPT2_transp() {
    	this.DEPT2_transp_modified = false;
    }
    public FieldProperty getDEPT2_transpFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_transp");
    }
    
    public String getDEPT2_transp() {
    	return DEPT2_transp;
    }	
    public String getNvlDEPT2_transp() {
    	if(getDEPT2_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_transp();
    	}
    }
    public void setDEPT2_transp(String DEPT2_transp) {
    	if(DEPT2_transp == null) {
    		this.DEPT2_transp = null;
    	} else {
    		this.DEPT2_transp = DEPT2_transp;
    	}
    	this.DEPT2_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_validn
     * Comments    : 
     */	
    private String DEPT2_validn = null;
    
    private transient boolean DEPT2_validn_nullable = true;
    
    public boolean isNullableDEPT2_validn() {
    	return this.DEPT2_validn_nullable;
    }
    
    private transient boolean DEPT2_validn_invalidation = false;
    	
    public void setInvalidationDEPT2_validn(boolean invalidation) { 
    	this.DEPT2_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_validn() {
    	return this.DEPT2_validn_invalidation;
    }
    	
    private transient boolean DEPT2_validn_modified = false;
    
    public boolean isModifiedDEPT2_validn() {
    	return this.DEPT2_validn_modified;
    }
    	
    public void unModifiedDEPT2_validn() {
    	this.DEPT2_validn_modified = false;
    }
    public FieldProperty getDEPT2_validnFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_validn");
    }
    
    public String getDEPT2_validn() {
    	return DEPT2_validn;
    }	
    public String getNvlDEPT2_validn() {
    	if(getDEPT2_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_validn();
    	}
    }
    public void setDEPT2_validn(String DEPT2_validn) {
    	if(DEPT2_validn == null) {
    		this.DEPT2_validn = null;
    	} else {
    		this.DEPT2_validn = DEPT2_validn;
    	}
    	this.DEPT2_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_sosi
     * Comments    : 
     */	
    private String DEPT2_sosi = null;
    
    private transient boolean DEPT2_sosi_nullable = true;
    
    public boolean isNullableDEPT2_sosi() {
    	return this.DEPT2_sosi_nullable;
    }
    
    private transient boolean DEPT2_sosi_invalidation = false;
    	
    public void setInvalidationDEPT2_sosi(boolean invalidation) { 
    	this.DEPT2_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_sosi() {
    	return this.DEPT2_sosi_invalidation;
    }
    	
    private transient boolean DEPT2_sosi_modified = false;
    
    public boolean isModifiedDEPT2_sosi() {
    	return this.DEPT2_sosi_modified;
    }
    	
    public void unModifiedDEPT2_sosi() {
    	this.DEPT2_sosi_modified = false;
    }
    public FieldProperty getDEPT2_sosiFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_sosi");
    }
    
    public String getDEPT2_sosi() {
    	return DEPT2_sosi;
    }	
    public String getNvlDEPT2_sosi() {
    	if(getDEPT2_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_sosi();
    	}
    }
    public void setDEPT2_sosi(String DEPT2_sosi) {
    	if(DEPT2_sosi == null) {
    		this.DEPT2_sosi = null;
    	} else {
    		this.DEPT2_sosi = DEPT2_sosi;
    	}
    	this.DEPT2_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT2_ps
     * Comments    : 
     */	
    private String DEPT2_ps = null;
    
    private transient boolean DEPT2_ps_nullable = true;
    
    public boolean isNullableDEPT2_ps() {
    	return this.DEPT2_ps_nullable;
    }
    
    private transient boolean DEPT2_ps_invalidation = false;
    	
    public void setInvalidationDEPT2_ps(boolean invalidation) { 
    	this.DEPT2_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT2_ps() {
    	return this.DEPT2_ps_invalidation;
    }
    	
    private transient boolean DEPT2_ps_modified = false;
    
    public boolean isModifiedDEPT2_ps() {
    	return this.DEPT2_ps_modified;
    }
    	
    public void unModifiedDEPT2_ps() {
    	this.DEPT2_ps_modified = false;
    }
    public FieldProperty getDEPT2_psFieldProperty() {
    	return fieldPropertyMap.get("DEPT2_ps");
    }
    
    public String getDEPT2_ps() {
    	return DEPT2_ps;
    }	
    public String getNvlDEPT2_ps() {
    	if(getDEPT2_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT2_ps();
    	}
    }
    public void setDEPT2_ps(String DEPT2_ps) {
    	if(DEPT2_ps == null) {
    		this.DEPT2_ps = null;
    	} else {
    		this.DEPT2_ps = DEPT2_ps;
    	}
    	this.DEPT2_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_length
     * Comments    : 
     */	
    private int PHON2_length = 0;
    
    private transient boolean PHON2_length_nullable = false;
    
    public boolean isNullablePHON2_length() {
    	return this.PHON2_length_nullable;
    }
    
    private transient boolean PHON2_length_invalidation = false;
    	
    public void setInvalidationPHON2_length(boolean invalidation) { 
    	this.PHON2_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_length() {
    	return this.PHON2_length_invalidation;
    }
    	
    private transient boolean PHON2_length_modified = false;
    
    public boolean isModifiedPHON2_length() {
    	return this.PHON2_length_modified;
    }
    	
    public void unModifiedPHON2_length() {
    	this.PHON2_length_modified = false;
    }
    public FieldProperty getPHON2_lengthFieldProperty() {
    	return fieldPropertyMap.get("PHON2_length");
    }
    
    public int getPHON2_length() {
    	return PHON2_length;
    }	
    public void setPHON2_length(int PHON2_length) {
    	this.PHON2_length = PHON2_length;
    	this.PHON2_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON2_length(Integer PHON2_length) {
    	if( PHON2_length == null) {
    		this.PHON2_length = 0;
    	} else{
    		this.PHON2_length = PHON2_length.intValue();
    	}
    	this.PHON2_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON2_length(String PHON2_length) {
    	if  (PHON2_length==null || PHON2_length.length() == 0) {
    		this.PHON2_length = 0;
    	} else {
    		this.PHON2_length = Integer.parseInt(PHON2_length);
    	}
    	this.PHON2_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_color
     * Comments    : 
     */	
    private String PHON2_color = null;
    
    private transient boolean PHON2_color_nullable = true;
    
    public boolean isNullablePHON2_color() {
    	return this.PHON2_color_nullable;
    }
    
    private transient boolean PHON2_color_invalidation = false;
    	
    public void setInvalidationPHON2_color(boolean invalidation) { 
    	this.PHON2_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_color() {
    	return this.PHON2_color_invalidation;
    }
    	
    private transient boolean PHON2_color_modified = false;
    
    public boolean isModifiedPHON2_color() {
    	return this.PHON2_color_modified;
    }
    	
    public void unModifiedPHON2_color() {
    	this.PHON2_color_modified = false;
    }
    public FieldProperty getPHON2_colorFieldProperty() {
    	return fieldPropertyMap.get("PHON2_color");
    }
    
    public String getPHON2_color() {
    	return PHON2_color;
    }	
    public String getNvlPHON2_color() {
    	if(getPHON2_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_color();
    	}
    }
    public void setPHON2_color(String PHON2_color) {
    	if(PHON2_color == null) {
    		this.PHON2_color = null;
    	} else {
    		this.PHON2_color = PHON2_color;
    	}
    	this.PHON2_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_hilight
     * Comments    : 
     */	
    private String PHON2_hilight = null;
    
    private transient boolean PHON2_hilight_nullable = true;
    
    public boolean isNullablePHON2_hilight() {
    	return this.PHON2_hilight_nullable;
    }
    
    private transient boolean PHON2_hilight_invalidation = false;
    	
    public void setInvalidationPHON2_hilight(boolean invalidation) { 
    	this.PHON2_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_hilight() {
    	return this.PHON2_hilight_invalidation;
    }
    	
    private transient boolean PHON2_hilight_modified = false;
    
    public boolean isModifiedPHON2_hilight() {
    	return this.PHON2_hilight_modified;
    }
    	
    public void unModifiedPHON2_hilight() {
    	this.PHON2_hilight_modified = false;
    }
    public FieldProperty getPHON2_hilightFieldProperty() {
    	return fieldPropertyMap.get("PHON2_hilight");
    }
    
    public String getPHON2_hilight() {
    	return PHON2_hilight;
    }	
    public String getNvlPHON2_hilight() {
    	if(getPHON2_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_hilight();
    	}
    }
    public void setPHON2_hilight(String PHON2_hilight) {
    	if(PHON2_hilight == null) {
    		this.PHON2_hilight = null;
    	} else {
    		this.PHON2_hilight = PHON2_hilight;
    	}
    	this.PHON2_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_outline
     * Comments    : 
     */	
    private String PHON2_outline = null;
    
    private transient boolean PHON2_outline_nullable = true;
    
    public boolean isNullablePHON2_outline() {
    	return this.PHON2_outline_nullable;
    }
    
    private transient boolean PHON2_outline_invalidation = false;
    	
    public void setInvalidationPHON2_outline(boolean invalidation) { 
    	this.PHON2_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_outline() {
    	return this.PHON2_outline_invalidation;
    }
    	
    private transient boolean PHON2_outline_modified = false;
    
    public boolean isModifiedPHON2_outline() {
    	return this.PHON2_outline_modified;
    }
    	
    public void unModifiedPHON2_outline() {
    	this.PHON2_outline_modified = false;
    }
    public FieldProperty getPHON2_outlineFieldProperty() {
    	return fieldPropertyMap.get("PHON2_outline");
    }
    
    public String getPHON2_outline() {
    	return PHON2_outline;
    }	
    public String getNvlPHON2_outline() {
    	if(getPHON2_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_outline();
    	}
    }
    public void setPHON2_outline(String PHON2_outline) {
    	if(PHON2_outline == null) {
    		this.PHON2_outline = null;
    	} else {
    		this.PHON2_outline = PHON2_outline;
    	}
    	this.PHON2_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_transp
     * Comments    : 
     */	
    private String PHON2_transp = null;
    
    private transient boolean PHON2_transp_nullable = true;
    
    public boolean isNullablePHON2_transp() {
    	return this.PHON2_transp_nullable;
    }
    
    private transient boolean PHON2_transp_invalidation = false;
    	
    public void setInvalidationPHON2_transp(boolean invalidation) { 
    	this.PHON2_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_transp() {
    	return this.PHON2_transp_invalidation;
    }
    	
    private transient boolean PHON2_transp_modified = false;
    
    public boolean isModifiedPHON2_transp() {
    	return this.PHON2_transp_modified;
    }
    	
    public void unModifiedPHON2_transp() {
    	this.PHON2_transp_modified = false;
    }
    public FieldProperty getPHON2_transpFieldProperty() {
    	return fieldPropertyMap.get("PHON2_transp");
    }
    
    public String getPHON2_transp() {
    	return PHON2_transp;
    }	
    public String getNvlPHON2_transp() {
    	if(getPHON2_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_transp();
    	}
    }
    public void setPHON2_transp(String PHON2_transp) {
    	if(PHON2_transp == null) {
    		this.PHON2_transp = null;
    	} else {
    		this.PHON2_transp = PHON2_transp;
    	}
    	this.PHON2_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_validn
     * Comments    : 
     */	
    private String PHON2_validn = null;
    
    private transient boolean PHON2_validn_nullable = true;
    
    public boolean isNullablePHON2_validn() {
    	return this.PHON2_validn_nullable;
    }
    
    private transient boolean PHON2_validn_invalidation = false;
    	
    public void setInvalidationPHON2_validn(boolean invalidation) { 
    	this.PHON2_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_validn() {
    	return this.PHON2_validn_invalidation;
    }
    	
    private transient boolean PHON2_validn_modified = false;
    
    public boolean isModifiedPHON2_validn() {
    	return this.PHON2_validn_modified;
    }
    	
    public void unModifiedPHON2_validn() {
    	this.PHON2_validn_modified = false;
    }
    public FieldProperty getPHON2_validnFieldProperty() {
    	return fieldPropertyMap.get("PHON2_validn");
    }
    
    public String getPHON2_validn() {
    	return PHON2_validn;
    }	
    public String getNvlPHON2_validn() {
    	if(getPHON2_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_validn();
    	}
    }
    public void setPHON2_validn(String PHON2_validn) {
    	if(PHON2_validn == null) {
    		this.PHON2_validn = null;
    	} else {
    		this.PHON2_validn = PHON2_validn;
    	}
    	this.PHON2_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_sosi
     * Comments    : 
     */	
    private String PHON2_sosi = null;
    
    private transient boolean PHON2_sosi_nullable = true;
    
    public boolean isNullablePHON2_sosi() {
    	return this.PHON2_sosi_nullable;
    }
    
    private transient boolean PHON2_sosi_invalidation = false;
    	
    public void setInvalidationPHON2_sosi(boolean invalidation) { 
    	this.PHON2_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_sosi() {
    	return this.PHON2_sosi_invalidation;
    }
    	
    private transient boolean PHON2_sosi_modified = false;
    
    public boolean isModifiedPHON2_sosi() {
    	return this.PHON2_sosi_modified;
    }
    	
    public void unModifiedPHON2_sosi() {
    	this.PHON2_sosi_modified = false;
    }
    public FieldProperty getPHON2_sosiFieldProperty() {
    	return fieldPropertyMap.get("PHON2_sosi");
    }
    
    public String getPHON2_sosi() {
    	return PHON2_sosi;
    }	
    public String getNvlPHON2_sosi() {
    	if(getPHON2_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_sosi();
    	}
    }
    public void setPHON2_sosi(String PHON2_sosi) {
    	if(PHON2_sosi == null) {
    		this.PHON2_sosi = null;
    	} else {
    		this.PHON2_sosi = PHON2_sosi;
    	}
    	this.PHON2_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON2_ps
     * Comments    : 
     */	
    private String PHON2_ps = null;
    
    private transient boolean PHON2_ps_nullable = true;
    
    public boolean isNullablePHON2_ps() {
    	return this.PHON2_ps_nullable;
    }
    
    private transient boolean PHON2_ps_invalidation = false;
    	
    public void setInvalidationPHON2_ps(boolean invalidation) { 
    	this.PHON2_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON2_ps() {
    	return this.PHON2_ps_invalidation;
    }
    	
    private transient boolean PHON2_ps_modified = false;
    
    public boolean isModifiedPHON2_ps() {
    	return this.PHON2_ps_modified;
    }
    	
    public void unModifiedPHON2_ps() {
    	this.PHON2_ps_modified = false;
    }
    public FieldProperty getPHON2_psFieldProperty() {
    	return fieldPropertyMap.get("PHON2_ps");
    }
    
    public String getPHON2_ps() {
    	return PHON2_ps;
    }	
    public String getNvlPHON2_ps() {
    	if(getPHON2_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON2_ps();
    	}
    }
    public void setPHON2_ps(String PHON2_ps) {
    	if(PHON2_ps == null) {
    		this.PHON2_ps = null;
    	} else {
    		this.PHON2_ps = PHON2_ps;
    	}
    	this.PHON2_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_length
     * Comments    : 
     */	
    private int IDEN3_length = 0;
    
    private transient boolean IDEN3_length_nullable = false;
    
    public boolean isNullableIDEN3_length() {
    	return this.IDEN3_length_nullable;
    }
    
    private transient boolean IDEN3_length_invalidation = false;
    	
    public void setInvalidationIDEN3_length(boolean invalidation) { 
    	this.IDEN3_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_length() {
    	return this.IDEN3_length_invalidation;
    }
    	
    private transient boolean IDEN3_length_modified = false;
    
    public boolean isModifiedIDEN3_length() {
    	return this.IDEN3_length_modified;
    }
    	
    public void unModifiedIDEN3_length() {
    	this.IDEN3_length_modified = false;
    }
    public FieldProperty getIDEN3_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_length");
    }
    
    public int getIDEN3_length() {
    	return IDEN3_length;
    }	
    public void setIDEN3_length(int IDEN3_length) {
    	this.IDEN3_length = IDEN3_length;
    	this.IDEN3_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN3_length(Integer IDEN3_length) {
    	if( IDEN3_length == null) {
    		this.IDEN3_length = 0;
    	} else{
    		this.IDEN3_length = IDEN3_length.intValue();
    	}
    	this.IDEN3_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN3_length(String IDEN3_length) {
    	if  (IDEN3_length==null || IDEN3_length.length() == 0) {
    		this.IDEN3_length = 0;
    	} else {
    		this.IDEN3_length = Integer.parseInt(IDEN3_length);
    	}
    	this.IDEN3_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_color
     * Comments    : 
     */	
    private String IDEN3_color = null;
    
    private transient boolean IDEN3_color_nullable = true;
    
    public boolean isNullableIDEN3_color() {
    	return this.IDEN3_color_nullable;
    }
    
    private transient boolean IDEN3_color_invalidation = false;
    	
    public void setInvalidationIDEN3_color(boolean invalidation) { 
    	this.IDEN3_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_color() {
    	return this.IDEN3_color_invalidation;
    }
    	
    private transient boolean IDEN3_color_modified = false;
    
    public boolean isModifiedIDEN3_color() {
    	return this.IDEN3_color_modified;
    }
    	
    public void unModifiedIDEN3_color() {
    	this.IDEN3_color_modified = false;
    }
    public FieldProperty getIDEN3_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_color");
    }
    
    public String getIDEN3_color() {
    	return IDEN3_color;
    }	
    public String getNvlIDEN3_color() {
    	if(getIDEN3_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_color();
    	}
    }
    public void setIDEN3_color(String IDEN3_color) {
    	if(IDEN3_color == null) {
    		this.IDEN3_color = null;
    	} else {
    		this.IDEN3_color = IDEN3_color;
    	}
    	this.IDEN3_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_hilight
     * Comments    : 
     */	
    private String IDEN3_hilight = null;
    
    private transient boolean IDEN3_hilight_nullable = true;
    
    public boolean isNullableIDEN3_hilight() {
    	return this.IDEN3_hilight_nullable;
    }
    
    private transient boolean IDEN3_hilight_invalidation = false;
    	
    public void setInvalidationIDEN3_hilight(boolean invalidation) { 
    	this.IDEN3_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_hilight() {
    	return this.IDEN3_hilight_invalidation;
    }
    	
    private transient boolean IDEN3_hilight_modified = false;
    
    public boolean isModifiedIDEN3_hilight() {
    	return this.IDEN3_hilight_modified;
    }
    	
    public void unModifiedIDEN3_hilight() {
    	this.IDEN3_hilight_modified = false;
    }
    public FieldProperty getIDEN3_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_hilight");
    }
    
    public String getIDEN3_hilight() {
    	return IDEN3_hilight;
    }	
    public String getNvlIDEN3_hilight() {
    	if(getIDEN3_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_hilight();
    	}
    }
    public void setIDEN3_hilight(String IDEN3_hilight) {
    	if(IDEN3_hilight == null) {
    		this.IDEN3_hilight = null;
    	} else {
    		this.IDEN3_hilight = IDEN3_hilight;
    	}
    	this.IDEN3_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_outline
     * Comments    : 
     */	
    private String IDEN3_outline = null;
    
    private transient boolean IDEN3_outline_nullable = true;
    
    public boolean isNullableIDEN3_outline() {
    	return this.IDEN3_outline_nullable;
    }
    
    private transient boolean IDEN3_outline_invalidation = false;
    	
    public void setInvalidationIDEN3_outline(boolean invalidation) { 
    	this.IDEN3_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_outline() {
    	return this.IDEN3_outline_invalidation;
    }
    	
    private transient boolean IDEN3_outline_modified = false;
    
    public boolean isModifiedIDEN3_outline() {
    	return this.IDEN3_outline_modified;
    }
    	
    public void unModifiedIDEN3_outline() {
    	this.IDEN3_outline_modified = false;
    }
    public FieldProperty getIDEN3_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_outline");
    }
    
    public String getIDEN3_outline() {
    	return IDEN3_outline;
    }	
    public String getNvlIDEN3_outline() {
    	if(getIDEN3_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_outline();
    	}
    }
    public void setIDEN3_outline(String IDEN3_outline) {
    	if(IDEN3_outline == null) {
    		this.IDEN3_outline = null;
    	} else {
    		this.IDEN3_outline = IDEN3_outline;
    	}
    	this.IDEN3_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_transp
     * Comments    : 
     */	
    private String IDEN3_transp = null;
    
    private transient boolean IDEN3_transp_nullable = true;
    
    public boolean isNullableIDEN3_transp() {
    	return this.IDEN3_transp_nullable;
    }
    
    private transient boolean IDEN3_transp_invalidation = false;
    	
    public void setInvalidationIDEN3_transp(boolean invalidation) { 
    	this.IDEN3_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_transp() {
    	return this.IDEN3_transp_invalidation;
    }
    	
    private transient boolean IDEN3_transp_modified = false;
    
    public boolean isModifiedIDEN3_transp() {
    	return this.IDEN3_transp_modified;
    }
    	
    public void unModifiedIDEN3_transp() {
    	this.IDEN3_transp_modified = false;
    }
    public FieldProperty getIDEN3_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_transp");
    }
    
    public String getIDEN3_transp() {
    	return IDEN3_transp;
    }	
    public String getNvlIDEN3_transp() {
    	if(getIDEN3_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_transp();
    	}
    }
    public void setIDEN3_transp(String IDEN3_transp) {
    	if(IDEN3_transp == null) {
    		this.IDEN3_transp = null;
    	} else {
    		this.IDEN3_transp = IDEN3_transp;
    	}
    	this.IDEN3_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_validn
     * Comments    : 
     */	
    private String IDEN3_validn = null;
    
    private transient boolean IDEN3_validn_nullable = true;
    
    public boolean isNullableIDEN3_validn() {
    	return this.IDEN3_validn_nullable;
    }
    
    private transient boolean IDEN3_validn_invalidation = false;
    	
    public void setInvalidationIDEN3_validn(boolean invalidation) { 
    	this.IDEN3_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_validn() {
    	return this.IDEN3_validn_invalidation;
    }
    	
    private transient boolean IDEN3_validn_modified = false;
    
    public boolean isModifiedIDEN3_validn() {
    	return this.IDEN3_validn_modified;
    }
    	
    public void unModifiedIDEN3_validn() {
    	this.IDEN3_validn_modified = false;
    }
    public FieldProperty getIDEN3_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_validn");
    }
    
    public String getIDEN3_validn() {
    	return IDEN3_validn;
    }	
    public String getNvlIDEN3_validn() {
    	if(getIDEN3_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_validn();
    	}
    }
    public void setIDEN3_validn(String IDEN3_validn) {
    	if(IDEN3_validn == null) {
    		this.IDEN3_validn = null;
    	} else {
    		this.IDEN3_validn = IDEN3_validn;
    	}
    	this.IDEN3_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_sosi
     * Comments    : 
     */	
    private String IDEN3_sosi = null;
    
    private transient boolean IDEN3_sosi_nullable = true;
    
    public boolean isNullableIDEN3_sosi() {
    	return this.IDEN3_sosi_nullable;
    }
    
    private transient boolean IDEN3_sosi_invalidation = false;
    	
    public void setInvalidationIDEN3_sosi(boolean invalidation) { 
    	this.IDEN3_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_sosi() {
    	return this.IDEN3_sosi_invalidation;
    }
    	
    private transient boolean IDEN3_sosi_modified = false;
    
    public boolean isModifiedIDEN3_sosi() {
    	return this.IDEN3_sosi_modified;
    }
    	
    public void unModifiedIDEN3_sosi() {
    	this.IDEN3_sosi_modified = false;
    }
    public FieldProperty getIDEN3_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_sosi");
    }
    
    public String getIDEN3_sosi() {
    	return IDEN3_sosi;
    }	
    public String getNvlIDEN3_sosi() {
    	if(getIDEN3_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_sosi();
    	}
    }
    public void setIDEN3_sosi(String IDEN3_sosi) {
    	if(IDEN3_sosi == null) {
    		this.IDEN3_sosi = null;
    	} else {
    		this.IDEN3_sosi = IDEN3_sosi;
    	}
    	this.IDEN3_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN3_ps
     * Comments    : 
     */	
    private String IDEN3_ps = null;
    
    private transient boolean IDEN3_ps_nullable = true;
    
    public boolean isNullableIDEN3_ps() {
    	return this.IDEN3_ps_nullable;
    }
    
    private transient boolean IDEN3_ps_invalidation = false;
    	
    public void setInvalidationIDEN3_ps(boolean invalidation) { 
    	this.IDEN3_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN3_ps() {
    	return this.IDEN3_ps_invalidation;
    }
    	
    private transient boolean IDEN3_ps_modified = false;
    
    public boolean isModifiedIDEN3_ps() {
    	return this.IDEN3_ps_modified;
    }
    	
    public void unModifiedIDEN3_ps() {
    	this.IDEN3_ps_modified = false;
    }
    public FieldProperty getIDEN3_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN3_ps");
    }
    
    public String getIDEN3_ps() {
    	return IDEN3_ps;
    }	
    public String getNvlIDEN3_ps() {
    	if(getIDEN3_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN3_ps();
    	}
    }
    public void setIDEN3_ps(String IDEN3_ps) {
    	if(IDEN3_ps == null) {
    		this.IDEN3_ps = null;
    	} else {
    		this.IDEN3_ps = IDEN3_ps;
    	}
    	this.IDEN3_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_length
     * Comments    : 
     */	
    private int NAME3_length = 0;
    
    private transient boolean NAME3_length_nullable = false;
    
    public boolean isNullableNAME3_length() {
    	return this.NAME3_length_nullable;
    }
    
    private transient boolean NAME3_length_invalidation = false;
    	
    public void setInvalidationNAME3_length(boolean invalidation) { 
    	this.NAME3_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_length() {
    	return this.NAME3_length_invalidation;
    }
    	
    private transient boolean NAME3_length_modified = false;
    
    public boolean isModifiedNAME3_length() {
    	return this.NAME3_length_modified;
    }
    	
    public void unModifiedNAME3_length() {
    	this.NAME3_length_modified = false;
    }
    public FieldProperty getNAME3_lengthFieldProperty() {
    	return fieldPropertyMap.get("NAME3_length");
    }
    
    public int getNAME3_length() {
    	return NAME3_length;
    }	
    public void setNAME3_length(int NAME3_length) {
    	this.NAME3_length = NAME3_length;
    	this.NAME3_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME3_length(Integer NAME3_length) {
    	if( NAME3_length == null) {
    		this.NAME3_length = 0;
    	} else{
    		this.NAME3_length = NAME3_length.intValue();
    	}
    	this.NAME3_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME3_length(String NAME3_length) {
    	if  (NAME3_length==null || NAME3_length.length() == 0) {
    		this.NAME3_length = 0;
    	} else {
    		this.NAME3_length = Integer.parseInt(NAME3_length);
    	}
    	this.NAME3_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_color
     * Comments    : 
     */	
    private String NAME3_color = null;
    
    private transient boolean NAME3_color_nullable = true;
    
    public boolean isNullableNAME3_color() {
    	return this.NAME3_color_nullable;
    }
    
    private transient boolean NAME3_color_invalidation = false;
    	
    public void setInvalidationNAME3_color(boolean invalidation) { 
    	this.NAME3_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_color() {
    	return this.NAME3_color_invalidation;
    }
    	
    private transient boolean NAME3_color_modified = false;
    
    public boolean isModifiedNAME3_color() {
    	return this.NAME3_color_modified;
    }
    	
    public void unModifiedNAME3_color() {
    	this.NAME3_color_modified = false;
    }
    public FieldProperty getNAME3_colorFieldProperty() {
    	return fieldPropertyMap.get("NAME3_color");
    }
    
    public String getNAME3_color() {
    	return NAME3_color;
    }	
    public String getNvlNAME3_color() {
    	if(getNAME3_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_color();
    	}
    }
    public void setNAME3_color(String NAME3_color) {
    	if(NAME3_color == null) {
    		this.NAME3_color = null;
    	} else {
    		this.NAME3_color = NAME3_color;
    	}
    	this.NAME3_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_hilight
     * Comments    : 
     */	
    private String NAME3_hilight = null;
    
    private transient boolean NAME3_hilight_nullable = true;
    
    public boolean isNullableNAME3_hilight() {
    	return this.NAME3_hilight_nullable;
    }
    
    private transient boolean NAME3_hilight_invalidation = false;
    	
    public void setInvalidationNAME3_hilight(boolean invalidation) { 
    	this.NAME3_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_hilight() {
    	return this.NAME3_hilight_invalidation;
    }
    	
    private transient boolean NAME3_hilight_modified = false;
    
    public boolean isModifiedNAME3_hilight() {
    	return this.NAME3_hilight_modified;
    }
    	
    public void unModifiedNAME3_hilight() {
    	this.NAME3_hilight_modified = false;
    }
    public FieldProperty getNAME3_hilightFieldProperty() {
    	return fieldPropertyMap.get("NAME3_hilight");
    }
    
    public String getNAME3_hilight() {
    	return NAME3_hilight;
    }	
    public String getNvlNAME3_hilight() {
    	if(getNAME3_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_hilight();
    	}
    }
    public void setNAME3_hilight(String NAME3_hilight) {
    	if(NAME3_hilight == null) {
    		this.NAME3_hilight = null;
    	} else {
    		this.NAME3_hilight = NAME3_hilight;
    	}
    	this.NAME3_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_outline
     * Comments    : 
     */	
    private String NAME3_outline = null;
    
    private transient boolean NAME3_outline_nullable = true;
    
    public boolean isNullableNAME3_outline() {
    	return this.NAME3_outline_nullable;
    }
    
    private transient boolean NAME3_outline_invalidation = false;
    	
    public void setInvalidationNAME3_outline(boolean invalidation) { 
    	this.NAME3_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_outline() {
    	return this.NAME3_outline_invalidation;
    }
    	
    private transient boolean NAME3_outline_modified = false;
    
    public boolean isModifiedNAME3_outline() {
    	return this.NAME3_outline_modified;
    }
    	
    public void unModifiedNAME3_outline() {
    	this.NAME3_outline_modified = false;
    }
    public FieldProperty getNAME3_outlineFieldProperty() {
    	return fieldPropertyMap.get("NAME3_outline");
    }
    
    public String getNAME3_outline() {
    	return NAME3_outline;
    }	
    public String getNvlNAME3_outline() {
    	if(getNAME3_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_outline();
    	}
    }
    public void setNAME3_outline(String NAME3_outline) {
    	if(NAME3_outline == null) {
    		this.NAME3_outline = null;
    	} else {
    		this.NAME3_outline = NAME3_outline;
    	}
    	this.NAME3_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_transp
     * Comments    : 
     */	
    private String NAME3_transp = null;
    
    private transient boolean NAME3_transp_nullable = true;
    
    public boolean isNullableNAME3_transp() {
    	return this.NAME3_transp_nullable;
    }
    
    private transient boolean NAME3_transp_invalidation = false;
    	
    public void setInvalidationNAME3_transp(boolean invalidation) { 
    	this.NAME3_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_transp() {
    	return this.NAME3_transp_invalidation;
    }
    	
    private transient boolean NAME3_transp_modified = false;
    
    public boolean isModifiedNAME3_transp() {
    	return this.NAME3_transp_modified;
    }
    	
    public void unModifiedNAME3_transp() {
    	this.NAME3_transp_modified = false;
    }
    public FieldProperty getNAME3_transpFieldProperty() {
    	return fieldPropertyMap.get("NAME3_transp");
    }
    
    public String getNAME3_transp() {
    	return NAME3_transp;
    }	
    public String getNvlNAME3_transp() {
    	if(getNAME3_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_transp();
    	}
    }
    public void setNAME3_transp(String NAME3_transp) {
    	if(NAME3_transp == null) {
    		this.NAME3_transp = null;
    	} else {
    		this.NAME3_transp = NAME3_transp;
    	}
    	this.NAME3_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_validn
     * Comments    : 
     */	
    private String NAME3_validn = null;
    
    private transient boolean NAME3_validn_nullable = true;
    
    public boolean isNullableNAME3_validn() {
    	return this.NAME3_validn_nullable;
    }
    
    private transient boolean NAME3_validn_invalidation = false;
    	
    public void setInvalidationNAME3_validn(boolean invalidation) { 
    	this.NAME3_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_validn() {
    	return this.NAME3_validn_invalidation;
    }
    	
    private transient boolean NAME3_validn_modified = false;
    
    public boolean isModifiedNAME3_validn() {
    	return this.NAME3_validn_modified;
    }
    	
    public void unModifiedNAME3_validn() {
    	this.NAME3_validn_modified = false;
    }
    public FieldProperty getNAME3_validnFieldProperty() {
    	return fieldPropertyMap.get("NAME3_validn");
    }
    
    public String getNAME3_validn() {
    	return NAME3_validn;
    }	
    public String getNvlNAME3_validn() {
    	if(getNAME3_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_validn();
    	}
    }
    public void setNAME3_validn(String NAME3_validn) {
    	if(NAME3_validn == null) {
    		this.NAME3_validn = null;
    	} else {
    		this.NAME3_validn = NAME3_validn;
    	}
    	this.NAME3_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_sosi
     * Comments    : 
     */	
    private String NAME3_sosi = null;
    
    private transient boolean NAME3_sosi_nullable = true;
    
    public boolean isNullableNAME3_sosi() {
    	return this.NAME3_sosi_nullable;
    }
    
    private transient boolean NAME3_sosi_invalidation = false;
    	
    public void setInvalidationNAME3_sosi(boolean invalidation) { 
    	this.NAME3_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_sosi() {
    	return this.NAME3_sosi_invalidation;
    }
    	
    private transient boolean NAME3_sosi_modified = false;
    
    public boolean isModifiedNAME3_sosi() {
    	return this.NAME3_sosi_modified;
    }
    	
    public void unModifiedNAME3_sosi() {
    	this.NAME3_sosi_modified = false;
    }
    public FieldProperty getNAME3_sosiFieldProperty() {
    	return fieldPropertyMap.get("NAME3_sosi");
    }
    
    public String getNAME3_sosi() {
    	return NAME3_sosi;
    }	
    public String getNvlNAME3_sosi() {
    	if(getNAME3_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_sosi();
    	}
    }
    public void setNAME3_sosi(String NAME3_sosi) {
    	if(NAME3_sosi == null) {
    		this.NAME3_sosi = null;
    	} else {
    		this.NAME3_sosi = NAME3_sosi;
    	}
    	this.NAME3_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME3_ps
     * Comments    : 
     */	
    private String NAME3_ps = null;
    
    private transient boolean NAME3_ps_nullable = true;
    
    public boolean isNullableNAME3_ps() {
    	return this.NAME3_ps_nullable;
    }
    
    private transient boolean NAME3_ps_invalidation = false;
    	
    public void setInvalidationNAME3_ps(boolean invalidation) { 
    	this.NAME3_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME3_ps() {
    	return this.NAME3_ps_invalidation;
    }
    	
    private transient boolean NAME3_ps_modified = false;
    
    public boolean isModifiedNAME3_ps() {
    	return this.NAME3_ps_modified;
    }
    	
    public void unModifiedNAME3_ps() {
    	this.NAME3_ps_modified = false;
    }
    public FieldProperty getNAME3_psFieldProperty() {
    	return fieldPropertyMap.get("NAME3_ps");
    }
    
    public String getNAME3_ps() {
    	return NAME3_ps;
    }	
    public String getNvlNAME3_ps() {
    	if(getNAME3_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME3_ps();
    	}
    }
    public void setNAME3_ps(String NAME3_ps) {
    	if(NAME3_ps == null) {
    		this.NAME3_ps = null;
    	} else {
    		this.NAME3_ps = NAME3_ps;
    	}
    	this.NAME3_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_length
     * Comments    : 
     */	
    private int DEPT3_length = 0;
    
    private transient boolean DEPT3_length_nullable = false;
    
    public boolean isNullableDEPT3_length() {
    	return this.DEPT3_length_nullable;
    }
    
    private transient boolean DEPT3_length_invalidation = false;
    	
    public void setInvalidationDEPT3_length(boolean invalidation) { 
    	this.DEPT3_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_length() {
    	return this.DEPT3_length_invalidation;
    }
    	
    private transient boolean DEPT3_length_modified = false;
    
    public boolean isModifiedDEPT3_length() {
    	return this.DEPT3_length_modified;
    }
    	
    public void unModifiedDEPT3_length() {
    	this.DEPT3_length_modified = false;
    }
    public FieldProperty getDEPT3_lengthFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_length");
    }
    
    public int getDEPT3_length() {
    	return DEPT3_length;
    }	
    public void setDEPT3_length(int DEPT3_length) {
    	this.DEPT3_length = DEPT3_length;
    	this.DEPT3_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT3_length(Integer DEPT3_length) {
    	if( DEPT3_length == null) {
    		this.DEPT3_length = 0;
    	} else{
    		this.DEPT3_length = DEPT3_length.intValue();
    	}
    	this.DEPT3_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT3_length(String DEPT3_length) {
    	if  (DEPT3_length==null || DEPT3_length.length() == 0) {
    		this.DEPT3_length = 0;
    	} else {
    		this.DEPT3_length = Integer.parseInt(DEPT3_length);
    	}
    	this.DEPT3_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_color
     * Comments    : 
     */	
    private String DEPT3_color = null;
    
    private transient boolean DEPT3_color_nullable = true;
    
    public boolean isNullableDEPT3_color() {
    	return this.DEPT3_color_nullable;
    }
    
    private transient boolean DEPT3_color_invalidation = false;
    	
    public void setInvalidationDEPT3_color(boolean invalidation) { 
    	this.DEPT3_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_color() {
    	return this.DEPT3_color_invalidation;
    }
    	
    private transient boolean DEPT3_color_modified = false;
    
    public boolean isModifiedDEPT3_color() {
    	return this.DEPT3_color_modified;
    }
    	
    public void unModifiedDEPT3_color() {
    	this.DEPT3_color_modified = false;
    }
    public FieldProperty getDEPT3_colorFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_color");
    }
    
    public String getDEPT3_color() {
    	return DEPT3_color;
    }	
    public String getNvlDEPT3_color() {
    	if(getDEPT3_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_color();
    	}
    }
    public void setDEPT3_color(String DEPT3_color) {
    	if(DEPT3_color == null) {
    		this.DEPT3_color = null;
    	} else {
    		this.DEPT3_color = DEPT3_color;
    	}
    	this.DEPT3_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_hilight
     * Comments    : 
     */	
    private String DEPT3_hilight = null;
    
    private transient boolean DEPT3_hilight_nullable = true;
    
    public boolean isNullableDEPT3_hilight() {
    	return this.DEPT3_hilight_nullable;
    }
    
    private transient boolean DEPT3_hilight_invalidation = false;
    	
    public void setInvalidationDEPT3_hilight(boolean invalidation) { 
    	this.DEPT3_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_hilight() {
    	return this.DEPT3_hilight_invalidation;
    }
    	
    private transient boolean DEPT3_hilight_modified = false;
    
    public boolean isModifiedDEPT3_hilight() {
    	return this.DEPT3_hilight_modified;
    }
    	
    public void unModifiedDEPT3_hilight() {
    	this.DEPT3_hilight_modified = false;
    }
    public FieldProperty getDEPT3_hilightFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_hilight");
    }
    
    public String getDEPT3_hilight() {
    	return DEPT3_hilight;
    }	
    public String getNvlDEPT3_hilight() {
    	if(getDEPT3_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_hilight();
    	}
    }
    public void setDEPT3_hilight(String DEPT3_hilight) {
    	if(DEPT3_hilight == null) {
    		this.DEPT3_hilight = null;
    	} else {
    		this.DEPT3_hilight = DEPT3_hilight;
    	}
    	this.DEPT3_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_outline
     * Comments    : 
     */	
    private String DEPT3_outline = null;
    
    private transient boolean DEPT3_outline_nullable = true;
    
    public boolean isNullableDEPT3_outline() {
    	return this.DEPT3_outline_nullable;
    }
    
    private transient boolean DEPT3_outline_invalidation = false;
    	
    public void setInvalidationDEPT3_outline(boolean invalidation) { 
    	this.DEPT3_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_outline() {
    	return this.DEPT3_outline_invalidation;
    }
    	
    private transient boolean DEPT3_outline_modified = false;
    
    public boolean isModifiedDEPT3_outline() {
    	return this.DEPT3_outline_modified;
    }
    	
    public void unModifiedDEPT3_outline() {
    	this.DEPT3_outline_modified = false;
    }
    public FieldProperty getDEPT3_outlineFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_outline");
    }
    
    public String getDEPT3_outline() {
    	return DEPT3_outline;
    }	
    public String getNvlDEPT3_outline() {
    	if(getDEPT3_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_outline();
    	}
    }
    public void setDEPT3_outline(String DEPT3_outline) {
    	if(DEPT3_outline == null) {
    		this.DEPT3_outline = null;
    	} else {
    		this.DEPT3_outline = DEPT3_outline;
    	}
    	this.DEPT3_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_transp
     * Comments    : 
     */	
    private String DEPT3_transp = null;
    
    private transient boolean DEPT3_transp_nullable = true;
    
    public boolean isNullableDEPT3_transp() {
    	return this.DEPT3_transp_nullable;
    }
    
    private transient boolean DEPT3_transp_invalidation = false;
    	
    public void setInvalidationDEPT3_transp(boolean invalidation) { 
    	this.DEPT3_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_transp() {
    	return this.DEPT3_transp_invalidation;
    }
    	
    private transient boolean DEPT3_transp_modified = false;
    
    public boolean isModifiedDEPT3_transp() {
    	return this.DEPT3_transp_modified;
    }
    	
    public void unModifiedDEPT3_transp() {
    	this.DEPT3_transp_modified = false;
    }
    public FieldProperty getDEPT3_transpFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_transp");
    }
    
    public String getDEPT3_transp() {
    	return DEPT3_transp;
    }	
    public String getNvlDEPT3_transp() {
    	if(getDEPT3_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_transp();
    	}
    }
    public void setDEPT3_transp(String DEPT3_transp) {
    	if(DEPT3_transp == null) {
    		this.DEPT3_transp = null;
    	} else {
    		this.DEPT3_transp = DEPT3_transp;
    	}
    	this.DEPT3_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_validn
     * Comments    : 
     */	
    private String DEPT3_validn = null;
    
    private transient boolean DEPT3_validn_nullable = true;
    
    public boolean isNullableDEPT3_validn() {
    	return this.DEPT3_validn_nullable;
    }
    
    private transient boolean DEPT3_validn_invalidation = false;
    	
    public void setInvalidationDEPT3_validn(boolean invalidation) { 
    	this.DEPT3_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_validn() {
    	return this.DEPT3_validn_invalidation;
    }
    	
    private transient boolean DEPT3_validn_modified = false;
    
    public boolean isModifiedDEPT3_validn() {
    	return this.DEPT3_validn_modified;
    }
    	
    public void unModifiedDEPT3_validn() {
    	this.DEPT3_validn_modified = false;
    }
    public FieldProperty getDEPT3_validnFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_validn");
    }
    
    public String getDEPT3_validn() {
    	return DEPT3_validn;
    }	
    public String getNvlDEPT3_validn() {
    	if(getDEPT3_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_validn();
    	}
    }
    public void setDEPT3_validn(String DEPT3_validn) {
    	if(DEPT3_validn == null) {
    		this.DEPT3_validn = null;
    	} else {
    		this.DEPT3_validn = DEPT3_validn;
    	}
    	this.DEPT3_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_sosi
     * Comments    : 
     */	
    private String DEPT3_sosi = null;
    
    private transient boolean DEPT3_sosi_nullable = true;
    
    public boolean isNullableDEPT3_sosi() {
    	return this.DEPT3_sosi_nullable;
    }
    
    private transient boolean DEPT3_sosi_invalidation = false;
    	
    public void setInvalidationDEPT3_sosi(boolean invalidation) { 
    	this.DEPT3_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_sosi() {
    	return this.DEPT3_sosi_invalidation;
    }
    	
    private transient boolean DEPT3_sosi_modified = false;
    
    public boolean isModifiedDEPT3_sosi() {
    	return this.DEPT3_sosi_modified;
    }
    	
    public void unModifiedDEPT3_sosi() {
    	this.DEPT3_sosi_modified = false;
    }
    public FieldProperty getDEPT3_sosiFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_sosi");
    }
    
    public String getDEPT3_sosi() {
    	return DEPT3_sosi;
    }	
    public String getNvlDEPT3_sosi() {
    	if(getDEPT3_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_sosi();
    	}
    }
    public void setDEPT3_sosi(String DEPT3_sosi) {
    	if(DEPT3_sosi == null) {
    		this.DEPT3_sosi = null;
    	} else {
    		this.DEPT3_sosi = DEPT3_sosi;
    	}
    	this.DEPT3_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT3_ps
     * Comments    : 
     */	
    private String DEPT3_ps = null;
    
    private transient boolean DEPT3_ps_nullable = true;
    
    public boolean isNullableDEPT3_ps() {
    	return this.DEPT3_ps_nullable;
    }
    
    private transient boolean DEPT3_ps_invalidation = false;
    	
    public void setInvalidationDEPT3_ps(boolean invalidation) { 
    	this.DEPT3_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT3_ps() {
    	return this.DEPT3_ps_invalidation;
    }
    	
    private transient boolean DEPT3_ps_modified = false;
    
    public boolean isModifiedDEPT3_ps() {
    	return this.DEPT3_ps_modified;
    }
    	
    public void unModifiedDEPT3_ps() {
    	this.DEPT3_ps_modified = false;
    }
    public FieldProperty getDEPT3_psFieldProperty() {
    	return fieldPropertyMap.get("DEPT3_ps");
    }
    
    public String getDEPT3_ps() {
    	return DEPT3_ps;
    }	
    public String getNvlDEPT3_ps() {
    	if(getDEPT3_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT3_ps();
    	}
    }
    public void setDEPT3_ps(String DEPT3_ps) {
    	if(DEPT3_ps == null) {
    		this.DEPT3_ps = null;
    	} else {
    		this.DEPT3_ps = DEPT3_ps;
    	}
    	this.DEPT3_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_length
     * Comments    : 
     */	
    private int PHON3_length = 0;
    
    private transient boolean PHON3_length_nullable = false;
    
    public boolean isNullablePHON3_length() {
    	return this.PHON3_length_nullable;
    }
    
    private transient boolean PHON3_length_invalidation = false;
    	
    public void setInvalidationPHON3_length(boolean invalidation) { 
    	this.PHON3_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_length() {
    	return this.PHON3_length_invalidation;
    }
    	
    private transient boolean PHON3_length_modified = false;
    
    public boolean isModifiedPHON3_length() {
    	return this.PHON3_length_modified;
    }
    	
    public void unModifiedPHON3_length() {
    	this.PHON3_length_modified = false;
    }
    public FieldProperty getPHON3_lengthFieldProperty() {
    	return fieldPropertyMap.get("PHON3_length");
    }
    
    public int getPHON3_length() {
    	return PHON3_length;
    }	
    public void setPHON3_length(int PHON3_length) {
    	this.PHON3_length = PHON3_length;
    	this.PHON3_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON3_length(Integer PHON3_length) {
    	if( PHON3_length == null) {
    		this.PHON3_length = 0;
    	} else{
    		this.PHON3_length = PHON3_length.intValue();
    	}
    	this.PHON3_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON3_length(String PHON3_length) {
    	if  (PHON3_length==null || PHON3_length.length() == 0) {
    		this.PHON3_length = 0;
    	} else {
    		this.PHON3_length = Integer.parseInt(PHON3_length);
    	}
    	this.PHON3_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_color
     * Comments    : 
     */	
    private String PHON3_color = null;
    
    private transient boolean PHON3_color_nullable = true;
    
    public boolean isNullablePHON3_color() {
    	return this.PHON3_color_nullable;
    }
    
    private transient boolean PHON3_color_invalidation = false;
    	
    public void setInvalidationPHON3_color(boolean invalidation) { 
    	this.PHON3_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_color() {
    	return this.PHON3_color_invalidation;
    }
    	
    private transient boolean PHON3_color_modified = false;
    
    public boolean isModifiedPHON3_color() {
    	return this.PHON3_color_modified;
    }
    	
    public void unModifiedPHON3_color() {
    	this.PHON3_color_modified = false;
    }
    public FieldProperty getPHON3_colorFieldProperty() {
    	return fieldPropertyMap.get("PHON3_color");
    }
    
    public String getPHON3_color() {
    	return PHON3_color;
    }	
    public String getNvlPHON3_color() {
    	if(getPHON3_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_color();
    	}
    }
    public void setPHON3_color(String PHON3_color) {
    	if(PHON3_color == null) {
    		this.PHON3_color = null;
    	} else {
    		this.PHON3_color = PHON3_color;
    	}
    	this.PHON3_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_hilight
     * Comments    : 
     */	
    private String PHON3_hilight = null;
    
    private transient boolean PHON3_hilight_nullable = true;
    
    public boolean isNullablePHON3_hilight() {
    	return this.PHON3_hilight_nullable;
    }
    
    private transient boolean PHON3_hilight_invalidation = false;
    	
    public void setInvalidationPHON3_hilight(boolean invalidation) { 
    	this.PHON3_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_hilight() {
    	return this.PHON3_hilight_invalidation;
    }
    	
    private transient boolean PHON3_hilight_modified = false;
    
    public boolean isModifiedPHON3_hilight() {
    	return this.PHON3_hilight_modified;
    }
    	
    public void unModifiedPHON3_hilight() {
    	this.PHON3_hilight_modified = false;
    }
    public FieldProperty getPHON3_hilightFieldProperty() {
    	return fieldPropertyMap.get("PHON3_hilight");
    }
    
    public String getPHON3_hilight() {
    	return PHON3_hilight;
    }	
    public String getNvlPHON3_hilight() {
    	if(getPHON3_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_hilight();
    	}
    }
    public void setPHON3_hilight(String PHON3_hilight) {
    	if(PHON3_hilight == null) {
    		this.PHON3_hilight = null;
    	} else {
    		this.PHON3_hilight = PHON3_hilight;
    	}
    	this.PHON3_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_outline
     * Comments    : 
     */	
    private String PHON3_outline = null;
    
    private transient boolean PHON3_outline_nullable = true;
    
    public boolean isNullablePHON3_outline() {
    	return this.PHON3_outline_nullable;
    }
    
    private transient boolean PHON3_outline_invalidation = false;
    	
    public void setInvalidationPHON3_outline(boolean invalidation) { 
    	this.PHON3_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_outline() {
    	return this.PHON3_outline_invalidation;
    }
    	
    private transient boolean PHON3_outline_modified = false;
    
    public boolean isModifiedPHON3_outline() {
    	return this.PHON3_outline_modified;
    }
    	
    public void unModifiedPHON3_outline() {
    	this.PHON3_outline_modified = false;
    }
    public FieldProperty getPHON3_outlineFieldProperty() {
    	return fieldPropertyMap.get("PHON3_outline");
    }
    
    public String getPHON3_outline() {
    	return PHON3_outline;
    }	
    public String getNvlPHON3_outline() {
    	if(getPHON3_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_outline();
    	}
    }
    public void setPHON3_outline(String PHON3_outline) {
    	if(PHON3_outline == null) {
    		this.PHON3_outline = null;
    	} else {
    		this.PHON3_outline = PHON3_outline;
    	}
    	this.PHON3_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_transp
     * Comments    : 
     */	
    private String PHON3_transp = null;
    
    private transient boolean PHON3_transp_nullable = true;
    
    public boolean isNullablePHON3_transp() {
    	return this.PHON3_transp_nullable;
    }
    
    private transient boolean PHON3_transp_invalidation = false;
    	
    public void setInvalidationPHON3_transp(boolean invalidation) { 
    	this.PHON3_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_transp() {
    	return this.PHON3_transp_invalidation;
    }
    	
    private transient boolean PHON3_transp_modified = false;
    
    public boolean isModifiedPHON3_transp() {
    	return this.PHON3_transp_modified;
    }
    	
    public void unModifiedPHON3_transp() {
    	this.PHON3_transp_modified = false;
    }
    public FieldProperty getPHON3_transpFieldProperty() {
    	return fieldPropertyMap.get("PHON3_transp");
    }
    
    public String getPHON3_transp() {
    	return PHON3_transp;
    }	
    public String getNvlPHON3_transp() {
    	if(getPHON3_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_transp();
    	}
    }
    public void setPHON3_transp(String PHON3_transp) {
    	if(PHON3_transp == null) {
    		this.PHON3_transp = null;
    	} else {
    		this.PHON3_transp = PHON3_transp;
    	}
    	this.PHON3_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_validn
     * Comments    : 
     */	
    private String PHON3_validn = null;
    
    private transient boolean PHON3_validn_nullable = true;
    
    public boolean isNullablePHON3_validn() {
    	return this.PHON3_validn_nullable;
    }
    
    private transient boolean PHON3_validn_invalidation = false;
    	
    public void setInvalidationPHON3_validn(boolean invalidation) { 
    	this.PHON3_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_validn() {
    	return this.PHON3_validn_invalidation;
    }
    	
    private transient boolean PHON3_validn_modified = false;
    
    public boolean isModifiedPHON3_validn() {
    	return this.PHON3_validn_modified;
    }
    	
    public void unModifiedPHON3_validn() {
    	this.PHON3_validn_modified = false;
    }
    public FieldProperty getPHON3_validnFieldProperty() {
    	return fieldPropertyMap.get("PHON3_validn");
    }
    
    public String getPHON3_validn() {
    	return PHON3_validn;
    }	
    public String getNvlPHON3_validn() {
    	if(getPHON3_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_validn();
    	}
    }
    public void setPHON3_validn(String PHON3_validn) {
    	if(PHON3_validn == null) {
    		this.PHON3_validn = null;
    	} else {
    		this.PHON3_validn = PHON3_validn;
    	}
    	this.PHON3_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_sosi
     * Comments    : 
     */	
    private String PHON3_sosi = null;
    
    private transient boolean PHON3_sosi_nullable = true;
    
    public boolean isNullablePHON3_sosi() {
    	return this.PHON3_sosi_nullable;
    }
    
    private transient boolean PHON3_sosi_invalidation = false;
    	
    public void setInvalidationPHON3_sosi(boolean invalidation) { 
    	this.PHON3_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_sosi() {
    	return this.PHON3_sosi_invalidation;
    }
    	
    private transient boolean PHON3_sosi_modified = false;
    
    public boolean isModifiedPHON3_sosi() {
    	return this.PHON3_sosi_modified;
    }
    	
    public void unModifiedPHON3_sosi() {
    	this.PHON3_sosi_modified = false;
    }
    public FieldProperty getPHON3_sosiFieldProperty() {
    	return fieldPropertyMap.get("PHON3_sosi");
    }
    
    public String getPHON3_sosi() {
    	return PHON3_sosi;
    }	
    public String getNvlPHON3_sosi() {
    	if(getPHON3_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_sosi();
    	}
    }
    public void setPHON3_sosi(String PHON3_sosi) {
    	if(PHON3_sosi == null) {
    		this.PHON3_sosi = null;
    	} else {
    		this.PHON3_sosi = PHON3_sosi;
    	}
    	this.PHON3_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON3_ps
     * Comments    : 
     */	
    private String PHON3_ps = null;
    
    private transient boolean PHON3_ps_nullable = true;
    
    public boolean isNullablePHON3_ps() {
    	return this.PHON3_ps_nullable;
    }
    
    private transient boolean PHON3_ps_invalidation = false;
    	
    public void setInvalidationPHON3_ps(boolean invalidation) { 
    	this.PHON3_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON3_ps() {
    	return this.PHON3_ps_invalidation;
    }
    	
    private transient boolean PHON3_ps_modified = false;
    
    public boolean isModifiedPHON3_ps() {
    	return this.PHON3_ps_modified;
    }
    	
    public void unModifiedPHON3_ps() {
    	this.PHON3_ps_modified = false;
    }
    public FieldProperty getPHON3_psFieldProperty() {
    	return fieldPropertyMap.get("PHON3_ps");
    }
    
    public String getPHON3_ps() {
    	return PHON3_ps;
    }	
    public String getNvlPHON3_ps() {
    	if(getPHON3_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON3_ps();
    	}
    }
    public void setPHON3_ps(String PHON3_ps) {
    	if(PHON3_ps == null) {
    		this.PHON3_ps = null;
    	} else {
    		this.PHON3_ps = PHON3_ps;
    	}
    	this.PHON3_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_length
     * Comments    : 
     */	
    private int IDEN4_length = 0;
    
    private transient boolean IDEN4_length_nullable = false;
    
    public boolean isNullableIDEN4_length() {
    	return this.IDEN4_length_nullable;
    }
    
    private transient boolean IDEN4_length_invalidation = false;
    	
    public void setInvalidationIDEN4_length(boolean invalidation) { 
    	this.IDEN4_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_length() {
    	return this.IDEN4_length_invalidation;
    }
    	
    private transient boolean IDEN4_length_modified = false;
    
    public boolean isModifiedIDEN4_length() {
    	return this.IDEN4_length_modified;
    }
    	
    public void unModifiedIDEN4_length() {
    	this.IDEN4_length_modified = false;
    }
    public FieldProperty getIDEN4_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_length");
    }
    
    public int getIDEN4_length() {
    	return IDEN4_length;
    }	
    public void setIDEN4_length(int IDEN4_length) {
    	this.IDEN4_length = IDEN4_length;
    	this.IDEN4_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN4_length(Integer IDEN4_length) {
    	if( IDEN4_length == null) {
    		this.IDEN4_length = 0;
    	} else{
    		this.IDEN4_length = IDEN4_length.intValue();
    	}
    	this.IDEN4_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN4_length(String IDEN4_length) {
    	if  (IDEN4_length==null || IDEN4_length.length() == 0) {
    		this.IDEN4_length = 0;
    	} else {
    		this.IDEN4_length = Integer.parseInt(IDEN4_length);
    	}
    	this.IDEN4_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_color
     * Comments    : 
     */	
    private String IDEN4_color = null;
    
    private transient boolean IDEN4_color_nullable = true;
    
    public boolean isNullableIDEN4_color() {
    	return this.IDEN4_color_nullable;
    }
    
    private transient boolean IDEN4_color_invalidation = false;
    	
    public void setInvalidationIDEN4_color(boolean invalidation) { 
    	this.IDEN4_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_color() {
    	return this.IDEN4_color_invalidation;
    }
    	
    private transient boolean IDEN4_color_modified = false;
    
    public boolean isModifiedIDEN4_color() {
    	return this.IDEN4_color_modified;
    }
    	
    public void unModifiedIDEN4_color() {
    	this.IDEN4_color_modified = false;
    }
    public FieldProperty getIDEN4_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_color");
    }
    
    public String getIDEN4_color() {
    	return IDEN4_color;
    }	
    public String getNvlIDEN4_color() {
    	if(getIDEN4_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_color();
    	}
    }
    public void setIDEN4_color(String IDEN4_color) {
    	if(IDEN4_color == null) {
    		this.IDEN4_color = null;
    	} else {
    		this.IDEN4_color = IDEN4_color;
    	}
    	this.IDEN4_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_hilight
     * Comments    : 
     */	
    private String IDEN4_hilight = null;
    
    private transient boolean IDEN4_hilight_nullable = true;
    
    public boolean isNullableIDEN4_hilight() {
    	return this.IDEN4_hilight_nullable;
    }
    
    private transient boolean IDEN4_hilight_invalidation = false;
    	
    public void setInvalidationIDEN4_hilight(boolean invalidation) { 
    	this.IDEN4_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_hilight() {
    	return this.IDEN4_hilight_invalidation;
    }
    	
    private transient boolean IDEN4_hilight_modified = false;
    
    public boolean isModifiedIDEN4_hilight() {
    	return this.IDEN4_hilight_modified;
    }
    	
    public void unModifiedIDEN4_hilight() {
    	this.IDEN4_hilight_modified = false;
    }
    public FieldProperty getIDEN4_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_hilight");
    }
    
    public String getIDEN4_hilight() {
    	return IDEN4_hilight;
    }	
    public String getNvlIDEN4_hilight() {
    	if(getIDEN4_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_hilight();
    	}
    }
    public void setIDEN4_hilight(String IDEN4_hilight) {
    	if(IDEN4_hilight == null) {
    		this.IDEN4_hilight = null;
    	} else {
    		this.IDEN4_hilight = IDEN4_hilight;
    	}
    	this.IDEN4_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_outline
     * Comments    : 
     */	
    private String IDEN4_outline = null;
    
    private transient boolean IDEN4_outline_nullable = true;
    
    public boolean isNullableIDEN4_outline() {
    	return this.IDEN4_outline_nullable;
    }
    
    private transient boolean IDEN4_outline_invalidation = false;
    	
    public void setInvalidationIDEN4_outline(boolean invalidation) { 
    	this.IDEN4_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_outline() {
    	return this.IDEN4_outline_invalidation;
    }
    	
    private transient boolean IDEN4_outline_modified = false;
    
    public boolean isModifiedIDEN4_outline() {
    	return this.IDEN4_outline_modified;
    }
    	
    public void unModifiedIDEN4_outline() {
    	this.IDEN4_outline_modified = false;
    }
    public FieldProperty getIDEN4_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_outline");
    }
    
    public String getIDEN4_outline() {
    	return IDEN4_outline;
    }	
    public String getNvlIDEN4_outline() {
    	if(getIDEN4_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_outline();
    	}
    }
    public void setIDEN4_outline(String IDEN4_outline) {
    	if(IDEN4_outline == null) {
    		this.IDEN4_outline = null;
    	} else {
    		this.IDEN4_outline = IDEN4_outline;
    	}
    	this.IDEN4_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_transp
     * Comments    : 
     */	
    private String IDEN4_transp = null;
    
    private transient boolean IDEN4_transp_nullable = true;
    
    public boolean isNullableIDEN4_transp() {
    	return this.IDEN4_transp_nullable;
    }
    
    private transient boolean IDEN4_transp_invalidation = false;
    	
    public void setInvalidationIDEN4_transp(boolean invalidation) { 
    	this.IDEN4_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_transp() {
    	return this.IDEN4_transp_invalidation;
    }
    	
    private transient boolean IDEN4_transp_modified = false;
    
    public boolean isModifiedIDEN4_transp() {
    	return this.IDEN4_transp_modified;
    }
    	
    public void unModifiedIDEN4_transp() {
    	this.IDEN4_transp_modified = false;
    }
    public FieldProperty getIDEN4_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_transp");
    }
    
    public String getIDEN4_transp() {
    	return IDEN4_transp;
    }	
    public String getNvlIDEN4_transp() {
    	if(getIDEN4_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_transp();
    	}
    }
    public void setIDEN4_transp(String IDEN4_transp) {
    	if(IDEN4_transp == null) {
    		this.IDEN4_transp = null;
    	} else {
    		this.IDEN4_transp = IDEN4_transp;
    	}
    	this.IDEN4_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_validn
     * Comments    : 
     */	
    private String IDEN4_validn = null;
    
    private transient boolean IDEN4_validn_nullable = true;
    
    public boolean isNullableIDEN4_validn() {
    	return this.IDEN4_validn_nullable;
    }
    
    private transient boolean IDEN4_validn_invalidation = false;
    	
    public void setInvalidationIDEN4_validn(boolean invalidation) { 
    	this.IDEN4_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_validn() {
    	return this.IDEN4_validn_invalidation;
    }
    	
    private transient boolean IDEN4_validn_modified = false;
    
    public boolean isModifiedIDEN4_validn() {
    	return this.IDEN4_validn_modified;
    }
    	
    public void unModifiedIDEN4_validn() {
    	this.IDEN4_validn_modified = false;
    }
    public FieldProperty getIDEN4_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_validn");
    }
    
    public String getIDEN4_validn() {
    	return IDEN4_validn;
    }	
    public String getNvlIDEN4_validn() {
    	if(getIDEN4_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_validn();
    	}
    }
    public void setIDEN4_validn(String IDEN4_validn) {
    	if(IDEN4_validn == null) {
    		this.IDEN4_validn = null;
    	} else {
    		this.IDEN4_validn = IDEN4_validn;
    	}
    	this.IDEN4_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_sosi
     * Comments    : 
     */	
    private String IDEN4_sosi = null;
    
    private transient boolean IDEN4_sosi_nullable = true;
    
    public boolean isNullableIDEN4_sosi() {
    	return this.IDEN4_sosi_nullable;
    }
    
    private transient boolean IDEN4_sosi_invalidation = false;
    	
    public void setInvalidationIDEN4_sosi(boolean invalidation) { 
    	this.IDEN4_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_sosi() {
    	return this.IDEN4_sosi_invalidation;
    }
    	
    private transient boolean IDEN4_sosi_modified = false;
    
    public boolean isModifiedIDEN4_sosi() {
    	return this.IDEN4_sosi_modified;
    }
    	
    public void unModifiedIDEN4_sosi() {
    	this.IDEN4_sosi_modified = false;
    }
    public FieldProperty getIDEN4_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_sosi");
    }
    
    public String getIDEN4_sosi() {
    	return IDEN4_sosi;
    }	
    public String getNvlIDEN4_sosi() {
    	if(getIDEN4_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_sosi();
    	}
    }
    public void setIDEN4_sosi(String IDEN4_sosi) {
    	if(IDEN4_sosi == null) {
    		this.IDEN4_sosi = null;
    	} else {
    		this.IDEN4_sosi = IDEN4_sosi;
    	}
    	this.IDEN4_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN4_ps
     * Comments    : 
     */	
    private String IDEN4_ps = null;
    
    private transient boolean IDEN4_ps_nullable = true;
    
    public boolean isNullableIDEN4_ps() {
    	return this.IDEN4_ps_nullable;
    }
    
    private transient boolean IDEN4_ps_invalidation = false;
    	
    public void setInvalidationIDEN4_ps(boolean invalidation) { 
    	this.IDEN4_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN4_ps() {
    	return this.IDEN4_ps_invalidation;
    }
    	
    private transient boolean IDEN4_ps_modified = false;
    
    public boolean isModifiedIDEN4_ps() {
    	return this.IDEN4_ps_modified;
    }
    	
    public void unModifiedIDEN4_ps() {
    	this.IDEN4_ps_modified = false;
    }
    public FieldProperty getIDEN4_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN4_ps");
    }
    
    public String getIDEN4_ps() {
    	return IDEN4_ps;
    }	
    public String getNvlIDEN4_ps() {
    	if(getIDEN4_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN4_ps();
    	}
    }
    public void setIDEN4_ps(String IDEN4_ps) {
    	if(IDEN4_ps == null) {
    		this.IDEN4_ps = null;
    	} else {
    		this.IDEN4_ps = IDEN4_ps;
    	}
    	this.IDEN4_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_length
     * Comments    : 
     */	
    private int NAME4_length = 0;
    
    private transient boolean NAME4_length_nullable = false;
    
    public boolean isNullableNAME4_length() {
    	return this.NAME4_length_nullable;
    }
    
    private transient boolean NAME4_length_invalidation = false;
    	
    public void setInvalidationNAME4_length(boolean invalidation) { 
    	this.NAME4_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_length() {
    	return this.NAME4_length_invalidation;
    }
    	
    private transient boolean NAME4_length_modified = false;
    
    public boolean isModifiedNAME4_length() {
    	return this.NAME4_length_modified;
    }
    	
    public void unModifiedNAME4_length() {
    	this.NAME4_length_modified = false;
    }
    public FieldProperty getNAME4_lengthFieldProperty() {
    	return fieldPropertyMap.get("NAME4_length");
    }
    
    public int getNAME4_length() {
    	return NAME4_length;
    }	
    public void setNAME4_length(int NAME4_length) {
    	this.NAME4_length = NAME4_length;
    	this.NAME4_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME4_length(Integer NAME4_length) {
    	if( NAME4_length == null) {
    		this.NAME4_length = 0;
    	} else{
    		this.NAME4_length = NAME4_length.intValue();
    	}
    	this.NAME4_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME4_length(String NAME4_length) {
    	if  (NAME4_length==null || NAME4_length.length() == 0) {
    		this.NAME4_length = 0;
    	} else {
    		this.NAME4_length = Integer.parseInt(NAME4_length);
    	}
    	this.NAME4_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_color
     * Comments    : 
     */	
    private String NAME4_color = null;
    
    private transient boolean NAME4_color_nullable = true;
    
    public boolean isNullableNAME4_color() {
    	return this.NAME4_color_nullable;
    }
    
    private transient boolean NAME4_color_invalidation = false;
    	
    public void setInvalidationNAME4_color(boolean invalidation) { 
    	this.NAME4_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_color() {
    	return this.NAME4_color_invalidation;
    }
    	
    private transient boolean NAME4_color_modified = false;
    
    public boolean isModifiedNAME4_color() {
    	return this.NAME4_color_modified;
    }
    	
    public void unModifiedNAME4_color() {
    	this.NAME4_color_modified = false;
    }
    public FieldProperty getNAME4_colorFieldProperty() {
    	return fieldPropertyMap.get("NAME4_color");
    }
    
    public String getNAME4_color() {
    	return NAME4_color;
    }	
    public String getNvlNAME4_color() {
    	if(getNAME4_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_color();
    	}
    }
    public void setNAME4_color(String NAME4_color) {
    	if(NAME4_color == null) {
    		this.NAME4_color = null;
    	} else {
    		this.NAME4_color = NAME4_color;
    	}
    	this.NAME4_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_hilight
     * Comments    : 
     */	
    private String NAME4_hilight = null;
    
    private transient boolean NAME4_hilight_nullable = true;
    
    public boolean isNullableNAME4_hilight() {
    	return this.NAME4_hilight_nullable;
    }
    
    private transient boolean NAME4_hilight_invalidation = false;
    	
    public void setInvalidationNAME4_hilight(boolean invalidation) { 
    	this.NAME4_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_hilight() {
    	return this.NAME4_hilight_invalidation;
    }
    	
    private transient boolean NAME4_hilight_modified = false;
    
    public boolean isModifiedNAME4_hilight() {
    	return this.NAME4_hilight_modified;
    }
    	
    public void unModifiedNAME4_hilight() {
    	this.NAME4_hilight_modified = false;
    }
    public FieldProperty getNAME4_hilightFieldProperty() {
    	return fieldPropertyMap.get("NAME4_hilight");
    }
    
    public String getNAME4_hilight() {
    	return NAME4_hilight;
    }	
    public String getNvlNAME4_hilight() {
    	if(getNAME4_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_hilight();
    	}
    }
    public void setNAME4_hilight(String NAME4_hilight) {
    	if(NAME4_hilight == null) {
    		this.NAME4_hilight = null;
    	} else {
    		this.NAME4_hilight = NAME4_hilight;
    	}
    	this.NAME4_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_outline
     * Comments    : 
     */	
    private String NAME4_outline = null;
    
    private transient boolean NAME4_outline_nullable = true;
    
    public boolean isNullableNAME4_outline() {
    	return this.NAME4_outline_nullable;
    }
    
    private transient boolean NAME4_outline_invalidation = false;
    	
    public void setInvalidationNAME4_outline(boolean invalidation) { 
    	this.NAME4_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_outline() {
    	return this.NAME4_outline_invalidation;
    }
    	
    private transient boolean NAME4_outline_modified = false;
    
    public boolean isModifiedNAME4_outline() {
    	return this.NAME4_outline_modified;
    }
    	
    public void unModifiedNAME4_outline() {
    	this.NAME4_outline_modified = false;
    }
    public FieldProperty getNAME4_outlineFieldProperty() {
    	return fieldPropertyMap.get("NAME4_outline");
    }
    
    public String getNAME4_outline() {
    	return NAME4_outline;
    }	
    public String getNvlNAME4_outline() {
    	if(getNAME4_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_outline();
    	}
    }
    public void setNAME4_outline(String NAME4_outline) {
    	if(NAME4_outline == null) {
    		this.NAME4_outline = null;
    	} else {
    		this.NAME4_outline = NAME4_outline;
    	}
    	this.NAME4_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_transp
     * Comments    : 
     */	
    private String NAME4_transp = null;
    
    private transient boolean NAME4_transp_nullable = true;
    
    public boolean isNullableNAME4_transp() {
    	return this.NAME4_transp_nullable;
    }
    
    private transient boolean NAME4_transp_invalidation = false;
    	
    public void setInvalidationNAME4_transp(boolean invalidation) { 
    	this.NAME4_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_transp() {
    	return this.NAME4_transp_invalidation;
    }
    	
    private transient boolean NAME4_transp_modified = false;
    
    public boolean isModifiedNAME4_transp() {
    	return this.NAME4_transp_modified;
    }
    	
    public void unModifiedNAME4_transp() {
    	this.NAME4_transp_modified = false;
    }
    public FieldProperty getNAME4_transpFieldProperty() {
    	return fieldPropertyMap.get("NAME4_transp");
    }
    
    public String getNAME4_transp() {
    	return NAME4_transp;
    }	
    public String getNvlNAME4_transp() {
    	if(getNAME4_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_transp();
    	}
    }
    public void setNAME4_transp(String NAME4_transp) {
    	if(NAME4_transp == null) {
    		this.NAME4_transp = null;
    	} else {
    		this.NAME4_transp = NAME4_transp;
    	}
    	this.NAME4_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_validn
     * Comments    : 
     */	
    private String NAME4_validn = null;
    
    private transient boolean NAME4_validn_nullable = true;
    
    public boolean isNullableNAME4_validn() {
    	return this.NAME4_validn_nullable;
    }
    
    private transient boolean NAME4_validn_invalidation = false;
    	
    public void setInvalidationNAME4_validn(boolean invalidation) { 
    	this.NAME4_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_validn() {
    	return this.NAME4_validn_invalidation;
    }
    	
    private transient boolean NAME4_validn_modified = false;
    
    public boolean isModifiedNAME4_validn() {
    	return this.NAME4_validn_modified;
    }
    	
    public void unModifiedNAME4_validn() {
    	this.NAME4_validn_modified = false;
    }
    public FieldProperty getNAME4_validnFieldProperty() {
    	return fieldPropertyMap.get("NAME4_validn");
    }
    
    public String getNAME4_validn() {
    	return NAME4_validn;
    }	
    public String getNvlNAME4_validn() {
    	if(getNAME4_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_validn();
    	}
    }
    public void setNAME4_validn(String NAME4_validn) {
    	if(NAME4_validn == null) {
    		this.NAME4_validn = null;
    	} else {
    		this.NAME4_validn = NAME4_validn;
    	}
    	this.NAME4_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_sosi
     * Comments    : 
     */	
    private String NAME4_sosi = null;
    
    private transient boolean NAME4_sosi_nullable = true;
    
    public boolean isNullableNAME4_sosi() {
    	return this.NAME4_sosi_nullable;
    }
    
    private transient boolean NAME4_sosi_invalidation = false;
    	
    public void setInvalidationNAME4_sosi(boolean invalidation) { 
    	this.NAME4_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_sosi() {
    	return this.NAME4_sosi_invalidation;
    }
    	
    private transient boolean NAME4_sosi_modified = false;
    
    public boolean isModifiedNAME4_sosi() {
    	return this.NAME4_sosi_modified;
    }
    	
    public void unModifiedNAME4_sosi() {
    	this.NAME4_sosi_modified = false;
    }
    public FieldProperty getNAME4_sosiFieldProperty() {
    	return fieldPropertyMap.get("NAME4_sosi");
    }
    
    public String getNAME4_sosi() {
    	return NAME4_sosi;
    }	
    public String getNvlNAME4_sosi() {
    	if(getNAME4_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_sosi();
    	}
    }
    public void setNAME4_sosi(String NAME4_sosi) {
    	if(NAME4_sosi == null) {
    		this.NAME4_sosi = null;
    	} else {
    		this.NAME4_sosi = NAME4_sosi;
    	}
    	this.NAME4_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME4_ps
     * Comments    : 
     */	
    private String NAME4_ps = null;
    
    private transient boolean NAME4_ps_nullable = true;
    
    public boolean isNullableNAME4_ps() {
    	return this.NAME4_ps_nullable;
    }
    
    private transient boolean NAME4_ps_invalidation = false;
    	
    public void setInvalidationNAME4_ps(boolean invalidation) { 
    	this.NAME4_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME4_ps() {
    	return this.NAME4_ps_invalidation;
    }
    	
    private transient boolean NAME4_ps_modified = false;
    
    public boolean isModifiedNAME4_ps() {
    	return this.NAME4_ps_modified;
    }
    	
    public void unModifiedNAME4_ps() {
    	this.NAME4_ps_modified = false;
    }
    public FieldProperty getNAME4_psFieldProperty() {
    	return fieldPropertyMap.get("NAME4_ps");
    }
    
    public String getNAME4_ps() {
    	return NAME4_ps;
    }	
    public String getNvlNAME4_ps() {
    	if(getNAME4_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME4_ps();
    	}
    }
    public void setNAME4_ps(String NAME4_ps) {
    	if(NAME4_ps == null) {
    		this.NAME4_ps = null;
    	} else {
    		this.NAME4_ps = NAME4_ps;
    	}
    	this.NAME4_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_length
     * Comments    : 
     */	
    private int DEPT4_length = 0;
    
    private transient boolean DEPT4_length_nullable = false;
    
    public boolean isNullableDEPT4_length() {
    	return this.DEPT4_length_nullable;
    }
    
    private transient boolean DEPT4_length_invalidation = false;
    	
    public void setInvalidationDEPT4_length(boolean invalidation) { 
    	this.DEPT4_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_length() {
    	return this.DEPT4_length_invalidation;
    }
    	
    private transient boolean DEPT4_length_modified = false;
    
    public boolean isModifiedDEPT4_length() {
    	return this.DEPT4_length_modified;
    }
    	
    public void unModifiedDEPT4_length() {
    	this.DEPT4_length_modified = false;
    }
    public FieldProperty getDEPT4_lengthFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_length");
    }
    
    public int getDEPT4_length() {
    	return DEPT4_length;
    }	
    public void setDEPT4_length(int DEPT4_length) {
    	this.DEPT4_length = DEPT4_length;
    	this.DEPT4_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT4_length(Integer DEPT4_length) {
    	if( DEPT4_length == null) {
    		this.DEPT4_length = 0;
    	} else{
    		this.DEPT4_length = DEPT4_length.intValue();
    	}
    	this.DEPT4_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT4_length(String DEPT4_length) {
    	if  (DEPT4_length==null || DEPT4_length.length() == 0) {
    		this.DEPT4_length = 0;
    	} else {
    		this.DEPT4_length = Integer.parseInt(DEPT4_length);
    	}
    	this.DEPT4_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_color
     * Comments    : 
     */	
    private String DEPT4_color = null;
    
    private transient boolean DEPT4_color_nullable = true;
    
    public boolean isNullableDEPT4_color() {
    	return this.DEPT4_color_nullable;
    }
    
    private transient boolean DEPT4_color_invalidation = false;
    	
    public void setInvalidationDEPT4_color(boolean invalidation) { 
    	this.DEPT4_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_color() {
    	return this.DEPT4_color_invalidation;
    }
    	
    private transient boolean DEPT4_color_modified = false;
    
    public boolean isModifiedDEPT4_color() {
    	return this.DEPT4_color_modified;
    }
    	
    public void unModifiedDEPT4_color() {
    	this.DEPT4_color_modified = false;
    }
    public FieldProperty getDEPT4_colorFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_color");
    }
    
    public String getDEPT4_color() {
    	return DEPT4_color;
    }	
    public String getNvlDEPT4_color() {
    	if(getDEPT4_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_color();
    	}
    }
    public void setDEPT4_color(String DEPT4_color) {
    	if(DEPT4_color == null) {
    		this.DEPT4_color = null;
    	} else {
    		this.DEPT4_color = DEPT4_color;
    	}
    	this.DEPT4_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_hilight
     * Comments    : 
     */	
    private String DEPT4_hilight = null;
    
    private transient boolean DEPT4_hilight_nullable = true;
    
    public boolean isNullableDEPT4_hilight() {
    	return this.DEPT4_hilight_nullable;
    }
    
    private transient boolean DEPT4_hilight_invalidation = false;
    	
    public void setInvalidationDEPT4_hilight(boolean invalidation) { 
    	this.DEPT4_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_hilight() {
    	return this.DEPT4_hilight_invalidation;
    }
    	
    private transient boolean DEPT4_hilight_modified = false;
    
    public boolean isModifiedDEPT4_hilight() {
    	return this.DEPT4_hilight_modified;
    }
    	
    public void unModifiedDEPT4_hilight() {
    	this.DEPT4_hilight_modified = false;
    }
    public FieldProperty getDEPT4_hilightFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_hilight");
    }
    
    public String getDEPT4_hilight() {
    	return DEPT4_hilight;
    }	
    public String getNvlDEPT4_hilight() {
    	if(getDEPT4_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_hilight();
    	}
    }
    public void setDEPT4_hilight(String DEPT4_hilight) {
    	if(DEPT4_hilight == null) {
    		this.DEPT4_hilight = null;
    	} else {
    		this.DEPT4_hilight = DEPT4_hilight;
    	}
    	this.DEPT4_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_outline
     * Comments    : 
     */	
    private String DEPT4_outline = null;
    
    private transient boolean DEPT4_outline_nullable = true;
    
    public boolean isNullableDEPT4_outline() {
    	return this.DEPT4_outline_nullable;
    }
    
    private transient boolean DEPT4_outline_invalidation = false;
    	
    public void setInvalidationDEPT4_outline(boolean invalidation) { 
    	this.DEPT4_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_outline() {
    	return this.DEPT4_outline_invalidation;
    }
    	
    private transient boolean DEPT4_outline_modified = false;
    
    public boolean isModifiedDEPT4_outline() {
    	return this.DEPT4_outline_modified;
    }
    	
    public void unModifiedDEPT4_outline() {
    	this.DEPT4_outline_modified = false;
    }
    public FieldProperty getDEPT4_outlineFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_outline");
    }
    
    public String getDEPT4_outline() {
    	return DEPT4_outline;
    }	
    public String getNvlDEPT4_outline() {
    	if(getDEPT4_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_outline();
    	}
    }
    public void setDEPT4_outline(String DEPT4_outline) {
    	if(DEPT4_outline == null) {
    		this.DEPT4_outline = null;
    	} else {
    		this.DEPT4_outline = DEPT4_outline;
    	}
    	this.DEPT4_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_transp
     * Comments    : 
     */	
    private String DEPT4_transp = null;
    
    private transient boolean DEPT4_transp_nullable = true;
    
    public boolean isNullableDEPT4_transp() {
    	return this.DEPT4_transp_nullable;
    }
    
    private transient boolean DEPT4_transp_invalidation = false;
    	
    public void setInvalidationDEPT4_transp(boolean invalidation) { 
    	this.DEPT4_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_transp() {
    	return this.DEPT4_transp_invalidation;
    }
    	
    private transient boolean DEPT4_transp_modified = false;
    
    public boolean isModifiedDEPT4_transp() {
    	return this.DEPT4_transp_modified;
    }
    	
    public void unModifiedDEPT4_transp() {
    	this.DEPT4_transp_modified = false;
    }
    public FieldProperty getDEPT4_transpFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_transp");
    }
    
    public String getDEPT4_transp() {
    	return DEPT4_transp;
    }	
    public String getNvlDEPT4_transp() {
    	if(getDEPT4_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_transp();
    	}
    }
    public void setDEPT4_transp(String DEPT4_transp) {
    	if(DEPT4_transp == null) {
    		this.DEPT4_transp = null;
    	} else {
    		this.DEPT4_transp = DEPT4_transp;
    	}
    	this.DEPT4_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_validn
     * Comments    : 
     */	
    private String DEPT4_validn = null;
    
    private transient boolean DEPT4_validn_nullable = true;
    
    public boolean isNullableDEPT4_validn() {
    	return this.DEPT4_validn_nullable;
    }
    
    private transient boolean DEPT4_validn_invalidation = false;
    	
    public void setInvalidationDEPT4_validn(boolean invalidation) { 
    	this.DEPT4_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_validn() {
    	return this.DEPT4_validn_invalidation;
    }
    	
    private transient boolean DEPT4_validn_modified = false;
    
    public boolean isModifiedDEPT4_validn() {
    	return this.DEPT4_validn_modified;
    }
    	
    public void unModifiedDEPT4_validn() {
    	this.DEPT4_validn_modified = false;
    }
    public FieldProperty getDEPT4_validnFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_validn");
    }
    
    public String getDEPT4_validn() {
    	return DEPT4_validn;
    }	
    public String getNvlDEPT4_validn() {
    	if(getDEPT4_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_validn();
    	}
    }
    public void setDEPT4_validn(String DEPT4_validn) {
    	if(DEPT4_validn == null) {
    		this.DEPT4_validn = null;
    	} else {
    		this.DEPT4_validn = DEPT4_validn;
    	}
    	this.DEPT4_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_sosi
     * Comments    : 
     */	
    private String DEPT4_sosi = null;
    
    private transient boolean DEPT4_sosi_nullable = true;
    
    public boolean isNullableDEPT4_sosi() {
    	return this.DEPT4_sosi_nullable;
    }
    
    private transient boolean DEPT4_sosi_invalidation = false;
    	
    public void setInvalidationDEPT4_sosi(boolean invalidation) { 
    	this.DEPT4_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_sosi() {
    	return this.DEPT4_sosi_invalidation;
    }
    	
    private transient boolean DEPT4_sosi_modified = false;
    
    public boolean isModifiedDEPT4_sosi() {
    	return this.DEPT4_sosi_modified;
    }
    	
    public void unModifiedDEPT4_sosi() {
    	this.DEPT4_sosi_modified = false;
    }
    public FieldProperty getDEPT4_sosiFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_sosi");
    }
    
    public String getDEPT4_sosi() {
    	return DEPT4_sosi;
    }	
    public String getNvlDEPT4_sosi() {
    	if(getDEPT4_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_sosi();
    	}
    }
    public void setDEPT4_sosi(String DEPT4_sosi) {
    	if(DEPT4_sosi == null) {
    		this.DEPT4_sosi = null;
    	} else {
    		this.DEPT4_sosi = DEPT4_sosi;
    	}
    	this.DEPT4_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT4_ps
     * Comments    : 
     */	
    private String DEPT4_ps = null;
    
    private transient boolean DEPT4_ps_nullable = true;
    
    public boolean isNullableDEPT4_ps() {
    	return this.DEPT4_ps_nullable;
    }
    
    private transient boolean DEPT4_ps_invalidation = false;
    	
    public void setInvalidationDEPT4_ps(boolean invalidation) { 
    	this.DEPT4_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT4_ps() {
    	return this.DEPT4_ps_invalidation;
    }
    	
    private transient boolean DEPT4_ps_modified = false;
    
    public boolean isModifiedDEPT4_ps() {
    	return this.DEPT4_ps_modified;
    }
    	
    public void unModifiedDEPT4_ps() {
    	this.DEPT4_ps_modified = false;
    }
    public FieldProperty getDEPT4_psFieldProperty() {
    	return fieldPropertyMap.get("DEPT4_ps");
    }
    
    public String getDEPT4_ps() {
    	return DEPT4_ps;
    }	
    public String getNvlDEPT4_ps() {
    	if(getDEPT4_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT4_ps();
    	}
    }
    public void setDEPT4_ps(String DEPT4_ps) {
    	if(DEPT4_ps == null) {
    		this.DEPT4_ps = null;
    	} else {
    		this.DEPT4_ps = DEPT4_ps;
    	}
    	this.DEPT4_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_length
     * Comments    : 
     */	
    private int PHON4_length = 0;
    
    private transient boolean PHON4_length_nullable = false;
    
    public boolean isNullablePHON4_length() {
    	return this.PHON4_length_nullable;
    }
    
    private transient boolean PHON4_length_invalidation = false;
    	
    public void setInvalidationPHON4_length(boolean invalidation) { 
    	this.PHON4_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_length() {
    	return this.PHON4_length_invalidation;
    }
    	
    private transient boolean PHON4_length_modified = false;
    
    public boolean isModifiedPHON4_length() {
    	return this.PHON4_length_modified;
    }
    	
    public void unModifiedPHON4_length() {
    	this.PHON4_length_modified = false;
    }
    public FieldProperty getPHON4_lengthFieldProperty() {
    	return fieldPropertyMap.get("PHON4_length");
    }
    
    public int getPHON4_length() {
    	return PHON4_length;
    }	
    public void setPHON4_length(int PHON4_length) {
    	this.PHON4_length = PHON4_length;
    	this.PHON4_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON4_length(Integer PHON4_length) {
    	if( PHON4_length == null) {
    		this.PHON4_length = 0;
    	} else{
    		this.PHON4_length = PHON4_length.intValue();
    	}
    	this.PHON4_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON4_length(String PHON4_length) {
    	if  (PHON4_length==null || PHON4_length.length() == 0) {
    		this.PHON4_length = 0;
    	} else {
    		this.PHON4_length = Integer.parseInt(PHON4_length);
    	}
    	this.PHON4_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_color
     * Comments    : 
     */	
    private String PHON4_color = null;
    
    private transient boolean PHON4_color_nullable = true;
    
    public boolean isNullablePHON4_color() {
    	return this.PHON4_color_nullable;
    }
    
    private transient boolean PHON4_color_invalidation = false;
    	
    public void setInvalidationPHON4_color(boolean invalidation) { 
    	this.PHON4_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_color() {
    	return this.PHON4_color_invalidation;
    }
    	
    private transient boolean PHON4_color_modified = false;
    
    public boolean isModifiedPHON4_color() {
    	return this.PHON4_color_modified;
    }
    	
    public void unModifiedPHON4_color() {
    	this.PHON4_color_modified = false;
    }
    public FieldProperty getPHON4_colorFieldProperty() {
    	return fieldPropertyMap.get("PHON4_color");
    }
    
    public String getPHON4_color() {
    	return PHON4_color;
    }	
    public String getNvlPHON4_color() {
    	if(getPHON4_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_color();
    	}
    }
    public void setPHON4_color(String PHON4_color) {
    	if(PHON4_color == null) {
    		this.PHON4_color = null;
    	} else {
    		this.PHON4_color = PHON4_color;
    	}
    	this.PHON4_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_hilight
     * Comments    : 
     */	
    private String PHON4_hilight = null;
    
    private transient boolean PHON4_hilight_nullable = true;
    
    public boolean isNullablePHON4_hilight() {
    	return this.PHON4_hilight_nullable;
    }
    
    private transient boolean PHON4_hilight_invalidation = false;
    	
    public void setInvalidationPHON4_hilight(boolean invalidation) { 
    	this.PHON4_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_hilight() {
    	return this.PHON4_hilight_invalidation;
    }
    	
    private transient boolean PHON4_hilight_modified = false;
    
    public boolean isModifiedPHON4_hilight() {
    	return this.PHON4_hilight_modified;
    }
    	
    public void unModifiedPHON4_hilight() {
    	this.PHON4_hilight_modified = false;
    }
    public FieldProperty getPHON4_hilightFieldProperty() {
    	return fieldPropertyMap.get("PHON4_hilight");
    }
    
    public String getPHON4_hilight() {
    	return PHON4_hilight;
    }	
    public String getNvlPHON4_hilight() {
    	if(getPHON4_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_hilight();
    	}
    }
    public void setPHON4_hilight(String PHON4_hilight) {
    	if(PHON4_hilight == null) {
    		this.PHON4_hilight = null;
    	} else {
    		this.PHON4_hilight = PHON4_hilight;
    	}
    	this.PHON4_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_outline
     * Comments    : 
     */	
    private String PHON4_outline = null;
    
    private transient boolean PHON4_outline_nullable = true;
    
    public boolean isNullablePHON4_outline() {
    	return this.PHON4_outline_nullable;
    }
    
    private transient boolean PHON4_outline_invalidation = false;
    	
    public void setInvalidationPHON4_outline(boolean invalidation) { 
    	this.PHON4_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_outline() {
    	return this.PHON4_outline_invalidation;
    }
    	
    private transient boolean PHON4_outline_modified = false;
    
    public boolean isModifiedPHON4_outline() {
    	return this.PHON4_outline_modified;
    }
    	
    public void unModifiedPHON4_outline() {
    	this.PHON4_outline_modified = false;
    }
    public FieldProperty getPHON4_outlineFieldProperty() {
    	return fieldPropertyMap.get("PHON4_outline");
    }
    
    public String getPHON4_outline() {
    	return PHON4_outline;
    }	
    public String getNvlPHON4_outline() {
    	if(getPHON4_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_outline();
    	}
    }
    public void setPHON4_outline(String PHON4_outline) {
    	if(PHON4_outline == null) {
    		this.PHON4_outline = null;
    	} else {
    		this.PHON4_outline = PHON4_outline;
    	}
    	this.PHON4_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_transp
     * Comments    : 
     */	
    private String PHON4_transp = null;
    
    private transient boolean PHON4_transp_nullable = true;
    
    public boolean isNullablePHON4_transp() {
    	return this.PHON4_transp_nullable;
    }
    
    private transient boolean PHON4_transp_invalidation = false;
    	
    public void setInvalidationPHON4_transp(boolean invalidation) { 
    	this.PHON4_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_transp() {
    	return this.PHON4_transp_invalidation;
    }
    	
    private transient boolean PHON4_transp_modified = false;
    
    public boolean isModifiedPHON4_transp() {
    	return this.PHON4_transp_modified;
    }
    	
    public void unModifiedPHON4_transp() {
    	this.PHON4_transp_modified = false;
    }
    public FieldProperty getPHON4_transpFieldProperty() {
    	return fieldPropertyMap.get("PHON4_transp");
    }
    
    public String getPHON4_transp() {
    	return PHON4_transp;
    }	
    public String getNvlPHON4_transp() {
    	if(getPHON4_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_transp();
    	}
    }
    public void setPHON4_transp(String PHON4_transp) {
    	if(PHON4_transp == null) {
    		this.PHON4_transp = null;
    	} else {
    		this.PHON4_transp = PHON4_transp;
    	}
    	this.PHON4_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_validn
     * Comments    : 
     */	
    private String PHON4_validn = null;
    
    private transient boolean PHON4_validn_nullable = true;
    
    public boolean isNullablePHON4_validn() {
    	return this.PHON4_validn_nullable;
    }
    
    private transient boolean PHON4_validn_invalidation = false;
    	
    public void setInvalidationPHON4_validn(boolean invalidation) { 
    	this.PHON4_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_validn() {
    	return this.PHON4_validn_invalidation;
    }
    	
    private transient boolean PHON4_validn_modified = false;
    
    public boolean isModifiedPHON4_validn() {
    	return this.PHON4_validn_modified;
    }
    	
    public void unModifiedPHON4_validn() {
    	this.PHON4_validn_modified = false;
    }
    public FieldProperty getPHON4_validnFieldProperty() {
    	return fieldPropertyMap.get("PHON4_validn");
    }
    
    public String getPHON4_validn() {
    	return PHON4_validn;
    }	
    public String getNvlPHON4_validn() {
    	if(getPHON4_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_validn();
    	}
    }
    public void setPHON4_validn(String PHON4_validn) {
    	if(PHON4_validn == null) {
    		this.PHON4_validn = null;
    	} else {
    		this.PHON4_validn = PHON4_validn;
    	}
    	this.PHON4_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_sosi
     * Comments    : 
     */	
    private String PHON4_sosi = null;
    
    private transient boolean PHON4_sosi_nullable = true;
    
    public boolean isNullablePHON4_sosi() {
    	return this.PHON4_sosi_nullable;
    }
    
    private transient boolean PHON4_sosi_invalidation = false;
    	
    public void setInvalidationPHON4_sosi(boolean invalidation) { 
    	this.PHON4_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_sosi() {
    	return this.PHON4_sosi_invalidation;
    }
    	
    private transient boolean PHON4_sosi_modified = false;
    
    public boolean isModifiedPHON4_sosi() {
    	return this.PHON4_sosi_modified;
    }
    	
    public void unModifiedPHON4_sosi() {
    	this.PHON4_sosi_modified = false;
    }
    public FieldProperty getPHON4_sosiFieldProperty() {
    	return fieldPropertyMap.get("PHON4_sosi");
    }
    
    public String getPHON4_sosi() {
    	return PHON4_sosi;
    }	
    public String getNvlPHON4_sosi() {
    	if(getPHON4_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_sosi();
    	}
    }
    public void setPHON4_sosi(String PHON4_sosi) {
    	if(PHON4_sosi == null) {
    		this.PHON4_sosi = null;
    	} else {
    		this.PHON4_sosi = PHON4_sosi;
    	}
    	this.PHON4_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON4_ps
     * Comments    : 
     */	
    private String PHON4_ps = null;
    
    private transient boolean PHON4_ps_nullable = true;
    
    public boolean isNullablePHON4_ps() {
    	return this.PHON4_ps_nullable;
    }
    
    private transient boolean PHON4_ps_invalidation = false;
    	
    public void setInvalidationPHON4_ps(boolean invalidation) { 
    	this.PHON4_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON4_ps() {
    	return this.PHON4_ps_invalidation;
    }
    	
    private transient boolean PHON4_ps_modified = false;
    
    public boolean isModifiedPHON4_ps() {
    	return this.PHON4_ps_modified;
    }
    	
    public void unModifiedPHON4_ps() {
    	this.PHON4_ps_modified = false;
    }
    public FieldProperty getPHON4_psFieldProperty() {
    	return fieldPropertyMap.get("PHON4_ps");
    }
    
    public String getPHON4_ps() {
    	return PHON4_ps;
    }	
    public String getNvlPHON4_ps() {
    	if(getPHON4_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON4_ps();
    	}
    }
    public void setPHON4_ps(String PHON4_ps) {
    	if(PHON4_ps == null) {
    		this.PHON4_ps = null;
    	} else {
    		this.PHON4_ps = PHON4_ps;
    	}
    	this.PHON4_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_length
     * Comments    : 
     */	
    private int IDEN5_length = 0;
    
    private transient boolean IDEN5_length_nullable = false;
    
    public boolean isNullableIDEN5_length() {
    	return this.IDEN5_length_nullable;
    }
    
    private transient boolean IDEN5_length_invalidation = false;
    	
    public void setInvalidationIDEN5_length(boolean invalidation) { 
    	this.IDEN5_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_length() {
    	return this.IDEN5_length_invalidation;
    }
    	
    private transient boolean IDEN5_length_modified = false;
    
    public boolean isModifiedIDEN5_length() {
    	return this.IDEN5_length_modified;
    }
    	
    public void unModifiedIDEN5_length() {
    	this.IDEN5_length_modified = false;
    }
    public FieldProperty getIDEN5_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_length");
    }
    
    public int getIDEN5_length() {
    	return IDEN5_length;
    }	
    public void setIDEN5_length(int IDEN5_length) {
    	this.IDEN5_length = IDEN5_length;
    	this.IDEN5_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN5_length(Integer IDEN5_length) {
    	if( IDEN5_length == null) {
    		this.IDEN5_length = 0;
    	} else{
    		this.IDEN5_length = IDEN5_length.intValue();
    	}
    	this.IDEN5_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN5_length(String IDEN5_length) {
    	if  (IDEN5_length==null || IDEN5_length.length() == 0) {
    		this.IDEN5_length = 0;
    	} else {
    		this.IDEN5_length = Integer.parseInt(IDEN5_length);
    	}
    	this.IDEN5_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_color
     * Comments    : 
     */	
    private String IDEN5_color = null;
    
    private transient boolean IDEN5_color_nullable = true;
    
    public boolean isNullableIDEN5_color() {
    	return this.IDEN5_color_nullable;
    }
    
    private transient boolean IDEN5_color_invalidation = false;
    	
    public void setInvalidationIDEN5_color(boolean invalidation) { 
    	this.IDEN5_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_color() {
    	return this.IDEN5_color_invalidation;
    }
    	
    private transient boolean IDEN5_color_modified = false;
    
    public boolean isModifiedIDEN5_color() {
    	return this.IDEN5_color_modified;
    }
    	
    public void unModifiedIDEN5_color() {
    	this.IDEN5_color_modified = false;
    }
    public FieldProperty getIDEN5_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_color");
    }
    
    public String getIDEN5_color() {
    	return IDEN5_color;
    }	
    public String getNvlIDEN5_color() {
    	if(getIDEN5_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_color();
    	}
    }
    public void setIDEN5_color(String IDEN5_color) {
    	if(IDEN5_color == null) {
    		this.IDEN5_color = null;
    	} else {
    		this.IDEN5_color = IDEN5_color;
    	}
    	this.IDEN5_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_hilight
     * Comments    : 
     */	
    private String IDEN5_hilight = null;
    
    private transient boolean IDEN5_hilight_nullable = true;
    
    public boolean isNullableIDEN5_hilight() {
    	return this.IDEN5_hilight_nullable;
    }
    
    private transient boolean IDEN5_hilight_invalidation = false;
    	
    public void setInvalidationIDEN5_hilight(boolean invalidation) { 
    	this.IDEN5_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_hilight() {
    	return this.IDEN5_hilight_invalidation;
    }
    	
    private transient boolean IDEN5_hilight_modified = false;
    
    public boolean isModifiedIDEN5_hilight() {
    	return this.IDEN5_hilight_modified;
    }
    	
    public void unModifiedIDEN5_hilight() {
    	this.IDEN5_hilight_modified = false;
    }
    public FieldProperty getIDEN5_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_hilight");
    }
    
    public String getIDEN5_hilight() {
    	return IDEN5_hilight;
    }	
    public String getNvlIDEN5_hilight() {
    	if(getIDEN5_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_hilight();
    	}
    }
    public void setIDEN5_hilight(String IDEN5_hilight) {
    	if(IDEN5_hilight == null) {
    		this.IDEN5_hilight = null;
    	} else {
    		this.IDEN5_hilight = IDEN5_hilight;
    	}
    	this.IDEN5_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_outline
     * Comments    : 
     */	
    private String IDEN5_outline = null;
    
    private transient boolean IDEN5_outline_nullable = true;
    
    public boolean isNullableIDEN5_outline() {
    	return this.IDEN5_outline_nullable;
    }
    
    private transient boolean IDEN5_outline_invalidation = false;
    	
    public void setInvalidationIDEN5_outline(boolean invalidation) { 
    	this.IDEN5_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_outline() {
    	return this.IDEN5_outline_invalidation;
    }
    	
    private transient boolean IDEN5_outline_modified = false;
    
    public boolean isModifiedIDEN5_outline() {
    	return this.IDEN5_outline_modified;
    }
    	
    public void unModifiedIDEN5_outline() {
    	this.IDEN5_outline_modified = false;
    }
    public FieldProperty getIDEN5_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_outline");
    }
    
    public String getIDEN5_outline() {
    	return IDEN5_outline;
    }	
    public String getNvlIDEN5_outline() {
    	if(getIDEN5_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_outline();
    	}
    }
    public void setIDEN5_outline(String IDEN5_outline) {
    	if(IDEN5_outline == null) {
    		this.IDEN5_outline = null;
    	} else {
    		this.IDEN5_outline = IDEN5_outline;
    	}
    	this.IDEN5_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_transp
     * Comments    : 
     */	
    private String IDEN5_transp = null;
    
    private transient boolean IDEN5_transp_nullable = true;
    
    public boolean isNullableIDEN5_transp() {
    	return this.IDEN5_transp_nullable;
    }
    
    private transient boolean IDEN5_transp_invalidation = false;
    	
    public void setInvalidationIDEN5_transp(boolean invalidation) { 
    	this.IDEN5_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_transp() {
    	return this.IDEN5_transp_invalidation;
    }
    	
    private transient boolean IDEN5_transp_modified = false;
    
    public boolean isModifiedIDEN5_transp() {
    	return this.IDEN5_transp_modified;
    }
    	
    public void unModifiedIDEN5_transp() {
    	this.IDEN5_transp_modified = false;
    }
    public FieldProperty getIDEN5_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_transp");
    }
    
    public String getIDEN5_transp() {
    	return IDEN5_transp;
    }	
    public String getNvlIDEN5_transp() {
    	if(getIDEN5_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_transp();
    	}
    }
    public void setIDEN5_transp(String IDEN5_transp) {
    	if(IDEN5_transp == null) {
    		this.IDEN5_transp = null;
    	} else {
    		this.IDEN5_transp = IDEN5_transp;
    	}
    	this.IDEN5_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_validn
     * Comments    : 
     */	
    private String IDEN5_validn = null;
    
    private transient boolean IDEN5_validn_nullable = true;
    
    public boolean isNullableIDEN5_validn() {
    	return this.IDEN5_validn_nullable;
    }
    
    private transient boolean IDEN5_validn_invalidation = false;
    	
    public void setInvalidationIDEN5_validn(boolean invalidation) { 
    	this.IDEN5_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_validn() {
    	return this.IDEN5_validn_invalidation;
    }
    	
    private transient boolean IDEN5_validn_modified = false;
    
    public boolean isModifiedIDEN5_validn() {
    	return this.IDEN5_validn_modified;
    }
    	
    public void unModifiedIDEN5_validn() {
    	this.IDEN5_validn_modified = false;
    }
    public FieldProperty getIDEN5_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_validn");
    }
    
    public String getIDEN5_validn() {
    	return IDEN5_validn;
    }	
    public String getNvlIDEN5_validn() {
    	if(getIDEN5_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_validn();
    	}
    }
    public void setIDEN5_validn(String IDEN5_validn) {
    	if(IDEN5_validn == null) {
    		this.IDEN5_validn = null;
    	} else {
    		this.IDEN5_validn = IDEN5_validn;
    	}
    	this.IDEN5_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_sosi
     * Comments    : 
     */	
    private String IDEN5_sosi = null;
    
    private transient boolean IDEN5_sosi_nullable = true;
    
    public boolean isNullableIDEN5_sosi() {
    	return this.IDEN5_sosi_nullable;
    }
    
    private transient boolean IDEN5_sosi_invalidation = false;
    	
    public void setInvalidationIDEN5_sosi(boolean invalidation) { 
    	this.IDEN5_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_sosi() {
    	return this.IDEN5_sosi_invalidation;
    }
    	
    private transient boolean IDEN5_sosi_modified = false;
    
    public boolean isModifiedIDEN5_sosi() {
    	return this.IDEN5_sosi_modified;
    }
    	
    public void unModifiedIDEN5_sosi() {
    	this.IDEN5_sosi_modified = false;
    }
    public FieldProperty getIDEN5_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_sosi");
    }
    
    public String getIDEN5_sosi() {
    	return IDEN5_sosi;
    }	
    public String getNvlIDEN5_sosi() {
    	if(getIDEN5_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_sosi();
    	}
    }
    public void setIDEN5_sosi(String IDEN5_sosi) {
    	if(IDEN5_sosi == null) {
    		this.IDEN5_sosi = null;
    	} else {
    		this.IDEN5_sosi = IDEN5_sosi;
    	}
    	this.IDEN5_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN5_ps
     * Comments    : 
     */	
    private String IDEN5_ps = null;
    
    private transient boolean IDEN5_ps_nullable = true;
    
    public boolean isNullableIDEN5_ps() {
    	return this.IDEN5_ps_nullable;
    }
    
    private transient boolean IDEN5_ps_invalidation = false;
    	
    public void setInvalidationIDEN5_ps(boolean invalidation) { 
    	this.IDEN5_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN5_ps() {
    	return this.IDEN5_ps_invalidation;
    }
    	
    private transient boolean IDEN5_ps_modified = false;
    
    public boolean isModifiedIDEN5_ps() {
    	return this.IDEN5_ps_modified;
    }
    	
    public void unModifiedIDEN5_ps() {
    	this.IDEN5_ps_modified = false;
    }
    public FieldProperty getIDEN5_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN5_ps");
    }
    
    public String getIDEN5_ps() {
    	return IDEN5_ps;
    }	
    public String getNvlIDEN5_ps() {
    	if(getIDEN5_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN5_ps();
    	}
    }
    public void setIDEN5_ps(String IDEN5_ps) {
    	if(IDEN5_ps == null) {
    		this.IDEN5_ps = null;
    	} else {
    		this.IDEN5_ps = IDEN5_ps;
    	}
    	this.IDEN5_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_length
     * Comments    : 
     */	
    private int NAME5_length = 0;
    
    private transient boolean NAME5_length_nullable = false;
    
    public boolean isNullableNAME5_length() {
    	return this.NAME5_length_nullable;
    }
    
    private transient boolean NAME5_length_invalidation = false;
    	
    public void setInvalidationNAME5_length(boolean invalidation) { 
    	this.NAME5_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_length() {
    	return this.NAME5_length_invalidation;
    }
    	
    private transient boolean NAME5_length_modified = false;
    
    public boolean isModifiedNAME5_length() {
    	return this.NAME5_length_modified;
    }
    	
    public void unModifiedNAME5_length() {
    	this.NAME5_length_modified = false;
    }
    public FieldProperty getNAME5_lengthFieldProperty() {
    	return fieldPropertyMap.get("NAME5_length");
    }
    
    public int getNAME5_length() {
    	return NAME5_length;
    }	
    public void setNAME5_length(int NAME5_length) {
    	this.NAME5_length = NAME5_length;
    	this.NAME5_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME5_length(Integer NAME5_length) {
    	if( NAME5_length == null) {
    		this.NAME5_length = 0;
    	} else{
    		this.NAME5_length = NAME5_length.intValue();
    	}
    	this.NAME5_length_modified = true;
    	this.isModified = true;
    }
    public void setNAME5_length(String NAME5_length) {
    	if  (NAME5_length==null || NAME5_length.length() == 0) {
    		this.NAME5_length = 0;
    	} else {
    		this.NAME5_length = Integer.parseInt(NAME5_length);
    	}
    	this.NAME5_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_color
     * Comments    : 
     */	
    private String NAME5_color = null;
    
    private transient boolean NAME5_color_nullable = true;
    
    public boolean isNullableNAME5_color() {
    	return this.NAME5_color_nullable;
    }
    
    private transient boolean NAME5_color_invalidation = false;
    	
    public void setInvalidationNAME5_color(boolean invalidation) { 
    	this.NAME5_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_color() {
    	return this.NAME5_color_invalidation;
    }
    	
    private transient boolean NAME5_color_modified = false;
    
    public boolean isModifiedNAME5_color() {
    	return this.NAME5_color_modified;
    }
    	
    public void unModifiedNAME5_color() {
    	this.NAME5_color_modified = false;
    }
    public FieldProperty getNAME5_colorFieldProperty() {
    	return fieldPropertyMap.get("NAME5_color");
    }
    
    public String getNAME5_color() {
    	return NAME5_color;
    }	
    public String getNvlNAME5_color() {
    	if(getNAME5_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_color();
    	}
    }
    public void setNAME5_color(String NAME5_color) {
    	if(NAME5_color == null) {
    		this.NAME5_color = null;
    	} else {
    		this.NAME5_color = NAME5_color;
    	}
    	this.NAME5_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_hilight
     * Comments    : 
     */	
    private String NAME5_hilight = null;
    
    private transient boolean NAME5_hilight_nullable = true;
    
    public boolean isNullableNAME5_hilight() {
    	return this.NAME5_hilight_nullable;
    }
    
    private transient boolean NAME5_hilight_invalidation = false;
    	
    public void setInvalidationNAME5_hilight(boolean invalidation) { 
    	this.NAME5_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_hilight() {
    	return this.NAME5_hilight_invalidation;
    }
    	
    private transient boolean NAME5_hilight_modified = false;
    
    public boolean isModifiedNAME5_hilight() {
    	return this.NAME5_hilight_modified;
    }
    	
    public void unModifiedNAME5_hilight() {
    	this.NAME5_hilight_modified = false;
    }
    public FieldProperty getNAME5_hilightFieldProperty() {
    	return fieldPropertyMap.get("NAME5_hilight");
    }
    
    public String getNAME5_hilight() {
    	return NAME5_hilight;
    }	
    public String getNvlNAME5_hilight() {
    	if(getNAME5_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_hilight();
    	}
    }
    public void setNAME5_hilight(String NAME5_hilight) {
    	if(NAME5_hilight == null) {
    		this.NAME5_hilight = null;
    	} else {
    		this.NAME5_hilight = NAME5_hilight;
    	}
    	this.NAME5_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_outline
     * Comments    : 
     */	
    private String NAME5_outline = null;
    
    private transient boolean NAME5_outline_nullable = true;
    
    public boolean isNullableNAME5_outline() {
    	return this.NAME5_outline_nullable;
    }
    
    private transient boolean NAME5_outline_invalidation = false;
    	
    public void setInvalidationNAME5_outline(boolean invalidation) { 
    	this.NAME5_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_outline() {
    	return this.NAME5_outline_invalidation;
    }
    	
    private transient boolean NAME5_outline_modified = false;
    
    public boolean isModifiedNAME5_outline() {
    	return this.NAME5_outline_modified;
    }
    	
    public void unModifiedNAME5_outline() {
    	this.NAME5_outline_modified = false;
    }
    public FieldProperty getNAME5_outlineFieldProperty() {
    	return fieldPropertyMap.get("NAME5_outline");
    }
    
    public String getNAME5_outline() {
    	return NAME5_outline;
    }	
    public String getNvlNAME5_outline() {
    	if(getNAME5_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_outline();
    	}
    }
    public void setNAME5_outline(String NAME5_outline) {
    	if(NAME5_outline == null) {
    		this.NAME5_outline = null;
    	} else {
    		this.NAME5_outline = NAME5_outline;
    	}
    	this.NAME5_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_transp
     * Comments    : 
     */	
    private String NAME5_transp = null;
    
    private transient boolean NAME5_transp_nullable = true;
    
    public boolean isNullableNAME5_transp() {
    	return this.NAME5_transp_nullable;
    }
    
    private transient boolean NAME5_transp_invalidation = false;
    	
    public void setInvalidationNAME5_transp(boolean invalidation) { 
    	this.NAME5_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_transp() {
    	return this.NAME5_transp_invalidation;
    }
    	
    private transient boolean NAME5_transp_modified = false;
    
    public boolean isModifiedNAME5_transp() {
    	return this.NAME5_transp_modified;
    }
    	
    public void unModifiedNAME5_transp() {
    	this.NAME5_transp_modified = false;
    }
    public FieldProperty getNAME5_transpFieldProperty() {
    	return fieldPropertyMap.get("NAME5_transp");
    }
    
    public String getNAME5_transp() {
    	return NAME5_transp;
    }	
    public String getNvlNAME5_transp() {
    	if(getNAME5_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_transp();
    	}
    }
    public void setNAME5_transp(String NAME5_transp) {
    	if(NAME5_transp == null) {
    		this.NAME5_transp = null;
    	} else {
    		this.NAME5_transp = NAME5_transp;
    	}
    	this.NAME5_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_validn
     * Comments    : 
     */	
    private String NAME5_validn = null;
    
    private transient boolean NAME5_validn_nullable = true;
    
    public boolean isNullableNAME5_validn() {
    	return this.NAME5_validn_nullable;
    }
    
    private transient boolean NAME5_validn_invalidation = false;
    	
    public void setInvalidationNAME5_validn(boolean invalidation) { 
    	this.NAME5_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_validn() {
    	return this.NAME5_validn_invalidation;
    }
    	
    private transient boolean NAME5_validn_modified = false;
    
    public boolean isModifiedNAME5_validn() {
    	return this.NAME5_validn_modified;
    }
    	
    public void unModifiedNAME5_validn() {
    	this.NAME5_validn_modified = false;
    }
    public FieldProperty getNAME5_validnFieldProperty() {
    	return fieldPropertyMap.get("NAME5_validn");
    }
    
    public String getNAME5_validn() {
    	return NAME5_validn;
    }	
    public String getNvlNAME5_validn() {
    	if(getNAME5_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_validn();
    	}
    }
    public void setNAME5_validn(String NAME5_validn) {
    	if(NAME5_validn == null) {
    		this.NAME5_validn = null;
    	} else {
    		this.NAME5_validn = NAME5_validn;
    	}
    	this.NAME5_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_sosi
     * Comments    : 
     */	
    private String NAME5_sosi = null;
    
    private transient boolean NAME5_sosi_nullable = true;
    
    public boolean isNullableNAME5_sosi() {
    	return this.NAME5_sosi_nullable;
    }
    
    private transient boolean NAME5_sosi_invalidation = false;
    	
    public void setInvalidationNAME5_sosi(boolean invalidation) { 
    	this.NAME5_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_sosi() {
    	return this.NAME5_sosi_invalidation;
    }
    	
    private transient boolean NAME5_sosi_modified = false;
    
    public boolean isModifiedNAME5_sosi() {
    	return this.NAME5_sosi_modified;
    }
    	
    public void unModifiedNAME5_sosi() {
    	this.NAME5_sosi_modified = false;
    }
    public FieldProperty getNAME5_sosiFieldProperty() {
    	return fieldPropertyMap.get("NAME5_sosi");
    }
    
    public String getNAME5_sosi() {
    	return NAME5_sosi;
    }	
    public String getNvlNAME5_sosi() {
    	if(getNAME5_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_sosi();
    	}
    }
    public void setNAME5_sosi(String NAME5_sosi) {
    	if(NAME5_sosi == null) {
    		this.NAME5_sosi = null;
    	} else {
    		this.NAME5_sosi = NAME5_sosi;
    	}
    	this.NAME5_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : NAME5_ps
     * Comments    : 
     */	
    private String NAME5_ps = null;
    
    private transient boolean NAME5_ps_nullable = true;
    
    public boolean isNullableNAME5_ps() {
    	return this.NAME5_ps_nullable;
    }
    
    private transient boolean NAME5_ps_invalidation = false;
    	
    public void setInvalidationNAME5_ps(boolean invalidation) { 
    	this.NAME5_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationNAME5_ps() {
    	return this.NAME5_ps_invalidation;
    }
    	
    private transient boolean NAME5_ps_modified = false;
    
    public boolean isModifiedNAME5_ps() {
    	return this.NAME5_ps_modified;
    }
    	
    public void unModifiedNAME5_ps() {
    	this.NAME5_ps_modified = false;
    }
    public FieldProperty getNAME5_psFieldProperty() {
    	return fieldPropertyMap.get("NAME5_ps");
    }
    
    public String getNAME5_ps() {
    	return NAME5_ps;
    }	
    public String getNvlNAME5_ps() {
    	if(getNAME5_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getNAME5_ps();
    	}
    }
    public void setNAME5_ps(String NAME5_ps) {
    	if(NAME5_ps == null) {
    		this.NAME5_ps = null;
    	} else {
    		this.NAME5_ps = NAME5_ps;
    	}
    	this.NAME5_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_length
     * Comments    : 
     */	
    private int DEPT5_length = 0;
    
    private transient boolean DEPT5_length_nullable = false;
    
    public boolean isNullableDEPT5_length() {
    	return this.DEPT5_length_nullable;
    }
    
    private transient boolean DEPT5_length_invalidation = false;
    	
    public void setInvalidationDEPT5_length(boolean invalidation) { 
    	this.DEPT5_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_length() {
    	return this.DEPT5_length_invalidation;
    }
    	
    private transient boolean DEPT5_length_modified = false;
    
    public boolean isModifiedDEPT5_length() {
    	return this.DEPT5_length_modified;
    }
    	
    public void unModifiedDEPT5_length() {
    	this.DEPT5_length_modified = false;
    }
    public FieldProperty getDEPT5_lengthFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_length");
    }
    
    public int getDEPT5_length() {
    	return DEPT5_length;
    }	
    public void setDEPT5_length(int DEPT5_length) {
    	this.DEPT5_length = DEPT5_length;
    	this.DEPT5_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT5_length(Integer DEPT5_length) {
    	if( DEPT5_length == null) {
    		this.DEPT5_length = 0;
    	} else{
    		this.DEPT5_length = DEPT5_length.intValue();
    	}
    	this.DEPT5_length_modified = true;
    	this.isModified = true;
    }
    public void setDEPT5_length(String DEPT5_length) {
    	if  (DEPT5_length==null || DEPT5_length.length() == 0) {
    		this.DEPT5_length = 0;
    	} else {
    		this.DEPT5_length = Integer.parseInt(DEPT5_length);
    	}
    	this.DEPT5_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_color
     * Comments    : 
     */	
    private String DEPT5_color = null;
    
    private transient boolean DEPT5_color_nullable = true;
    
    public boolean isNullableDEPT5_color() {
    	return this.DEPT5_color_nullable;
    }
    
    private transient boolean DEPT5_color_invalidation = false;
    	
    public void setInvalidationDEPT5_color(boolean invalidation) { 
    	this.DEPT5_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_color() {
    	return this.DEPT5_color_invalidation;
    }
    	
    private transient boolean DEPT5_color_modified = false;
    
    public boolean isModifiedDEPT5_color() {
    	return this.DEPT5_color_modified;
    }
    	
    public void unModifiedDEPT5_color() {
    	this.DEPT5_color_modified = false;
    }
    public FieldProperty getDEPT5_colorFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_color");
    }
    
    public String getDEPT5_color() {
    	return DEPT5_color;
    }	
    public String getNvlDEPT5_color() {
    	if(getDEPT5_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_color();
    	}
    }
    public void setDEPT5_color(String DEPT5_color) {
    	if(DEPT5_color == null) {
    		this.DEPT5_color = null;
    	} else {
    		this.DEPT5_color = DEPT5_color;
    	}
    	this.DEPT5_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_hilight
     * Comments    : 
     */	
    private String DEPT5_hilight = null;
    
    private transient boolean DEPT5_hilight_nullable = true;
    
    public boolean isNullableDEPT5_hilight() {
    	return this.DEPT5_hilight_nullable;
    }
    
    private transient boolean DEPT5_hilight_invalidation = false;
    	
    public void setInvalidationDEPT5_hilight(boolean invalidation) { 
    	this.DEPT5_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_hilight() {
    	return this.DEPT5_hilight_invalidation;
    }
    	
    private transient boolean DEPT5_hilight_modified = false;
    
    public boolean isModifiedDEPT5_hilight() {
    	return this.DEPT5_hilight_modified;
    }
    	
    public void unModifiedDEPT5_hilight() {
    	this.DEPT5_hilight_modified = false;
    }
    public FieldProperty getDEPT5_hilightFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_hilight");
    }
    
    public String getDEPT5_hilight() {
    	return DEPT5_hilight;
    }	
    public String getNvlDEPT5_hilight() {
    	if(getDEPT5_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_hilight();
    	}
    }
    public void setDEPT5_hilight(String DEPT5_hilight) {
    	if(DEPT5_hilight == null) {
    		this.DEPT5_hilight = null;
    	} else {
    		this.DEPT5_hilight = DEPT5_hilight;
    	}
    	this.DEPT5_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_outline
     * Comments    : 
     */	
    private String DEPT5_outline = null;
    
    private transient boolean DEPT5_outline_nullable = true;
    
    public boolean isNullableDEPT5_outline() {
    	return this.DEPT5_outline_nullable;
    }
    
    private transient boolean DEPT5_outline_invalidation = false;
    	
    public void setInvalidationDEPT5_outline(boolean invalidation) { 
    	this.DEPT5_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_outline() {
    	return this.DEPT5_outline_invalidation;
    }
    	
    private transient boolean DEPT5_outline_modified = false;
    
    public boolean isModifiedDEPT5_outline() {
    	return this.DEPT5_outline_modified;
    }
    	
    public void unModifiedDEPT5_outline() {
    	this.DEPT5_outline_modified = false;
    }
    public FieldProperty getDEPT5_outlineFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_outline");
    }
    
    public String getDEPT5_outline() {
    	return DEPT5_outline;
    }	
    public String getNvlDEPT5_outline() {
    	if(getDEPT5_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_outline();
    	}
    }
    public void setDEPT5_outline(String DEPT5_outline) {
    	if(DEPT5_outline == null) {
    		this.DEPT5_outline = null;
    	} else {
    		this.DEPT5_outline = DEPT5_outline;
    	}
    	this.DEPT5_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_transp
     * Comments    : 
     */	
    private String DEPT5_transp = null;
    
    private transient boolean DEPT5_transp_nullable = true;
    
    public boolean isNullableDEPT5_transp() {
    	return this.DEPT5_transp_nullable;
    }
    
    private transient boolean DEPT5_transp_invalidation = false;
    	
    public void setInvalidationDEPT5_transp(boolean invalidation) { 
    	this.DEPT5_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_transp() {
    	return this.DEPT5_transp_invalidation;
    }
    	
    private transient boolean DEPT5_transp_modified = false;
    
    public boolean isModifiedDEPT5_transp() {
    	return this.DEPT5_transp_modified;
    }
    	
    public void unModifiedDEPT5_transp() {
    	this.DEPT5_transp_modified = false;
    }
    public FieldProperty getDEPT5_transpFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_transp");
    }
    
    public String getDEPT5_transp() {
    	return DEPT5_transp;
    }	
    public String getNvlDEPT5_transp() {
    	if(getDEPT5_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_transp();
    	}
    }
    public void setDEPT5_transp(String DEPT5_transp) {
    	if(DEPT5_transp == null) {
    		this.DEPT5_transp = null;
    	} else {
    		this.DEPT5_transp = DEPT5_transp;
    	}
    	this.DEPT5_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_validn
     * Comments    : 
     */	
    private String DEPT5_validn = null;
    
    private transient boolean DEPT5_validn_nullable = true;
    
    public boolean isNullableDEPT5_validn() {
    	return this.DEPT5_validn_nullable;
    }
    
    private transient boolean DEPT5_validn_invalidation = false;
    	
    public void setInvalidationDEPT5_validn(boolean invalidation) { 
    	this.DEPT5_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_validn() {
    	return this.DEPT5_validn_invalidation;
    }
    	
    private transient boolean DEPT5_validn_modified = false;
    
    public boolean isModifiedDEPT5_validn() {
    	return this.DEPT5_validn_modified;
    }
    	
    public void unModifiedDEPT5_validn() {
    	this.DEPT5_validn_modified = false;
    }
    public FieldProperty getDEPT5_validnFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_validn");
    }
    
    public String getDEPT5_validn() {
    	return DEPT5_validn;
    }	
    public String getNvlDEPT5_validn() {
    	if(getDEPT5_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_validn();
    	}
    }
    public void setDEPT5_validn(String DEPT5_validn) {
    	if(DEPT5_validn == null) {
    		this.DEPT5_validn = null;
    	} else {
    		this.DEPT5_validn = DEPT5_validn;
    	}
    	this.DEPT5_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_sosi
     * Comments    : 
     */	
    private String DEPT5_sosi = null;
    
    private transient boolean DEPT5_sosi_nullable = true;
    
    public boolean isNullableDEPT5_sosi() {
    	return this.DEPT5_sosi_nullable;
    }
    
    private transient boolean DEPT5_sosi_invalidation = false;
    	
    public void setInvalidationDEPT5_sosi(boolean invalidation) { 
    	this.DEPT5_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_sosi() {
    	return this.DEPT5_sosi_invalidation;
    }
    	
    private transient boolean DEPT5_sosi_modified = false;
    
    public boolean isModifiedDEPT5_sosi() {
    	return this.DEPT5_sosi_modified;
    }
    	
    public void unModifiedDEPT5_sosi() {
    	this.DEPT5_sosi_modified = false;
    }
    public FieldProperty getDEPT5_sosiFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_sosi");
    }
    
    public String getDEPT5_sosi() {
    	return DEPT5_sosi;
    }	
    public String getNvlDEPT5_sosi() {
    	if(getDEPT5_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_sosi();
    	}
    }
    public void setDEPT5_sosi(String DEPT5_sosi) {
    	if(DEPT5_sosi == null) {
    		this.DEPT5_sosi = null;
    	} else {
    		this.DEPT5_sosi = DEPT5_sosi;
    	}
    	this.DEPT5_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DEPT5_ps
     * Comments    : 
     */	
    private String DEPT5_ps = null;
    
    private transient boolean DEPT5_ps_nullable = true;
    
    public boolean isNullableDEPT5_ps() {
    	return this.DEPT5_ps_nullable;
    }
    
    private transient boolean DEPT5_ps_invalidation = false;
    	
    public void setInvalidationDEPT5_ps(boolean invalidation) { 
    	this.DEPT5_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDEPT5_ps() {
    	return this.DEPT5_ps_invalidation;
    }
    	
    private transient boolean DEPT5_ps_modified = false;
    
    public boolean isModifiedDEPT5_ps() {
    	return this.DEPT5_ps_modified;
    }
    	
    public void unModifiedDEPT5_ps() {
    	this.DEPT5_ps_modified = false;
    }
    public FieldProperty getDEPT5_psFieldProperty() {
    	return fieldPropertyMap.get("DEPT5_ps");
    }
    
    public String getDEPT5_ps() {
    	return DEPT5_ps;
    }	
    public String getNvlDEPT5_ps() {
    	if(getDEPT5_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDEPT5_ps();
    	}
    }
    public void setDEPT5_ps(String DEPT5_ps) {
    	if(DEPT5_ps == null) {
    		this.DEPT5_ps = null;
    	} else {
    		this.DEPT5_ps = DEPT5_ps;
    	}
    	this.DEPT5_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_length
     * Comments    : 
     */	
    private int PHON5_length = 0;
    
    private transient boolean PHON5_length_nullable = false;
    
    public boolean isNullablePHON5_length() {
    	return this.PHON5_length_nullable;
    }
    
    private transient boolean PHON5_length_invalidation = false;
    	
    public void setInvalidationPHON5_length(boolean invalidation) { 
    	this.PHON5_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_length() {
    	return this.PHON5_length_invalidation;
    }
    	
    private transient boolean PHON5_length_modified = false;
    
    public boolean isModifiedPHON5_length() {
    	return this.PHON5_length_modified;
    }
    	
    public void unModifiedPHON5_length() {
    	this.PHON5_length_modified = false;
    }
    public FieldProperty getPHON5_lengthFieldProperty() {
    	return fieldPropertyMap.get("PHON5_length");
    }
    
    public int getPHON5_length() {
    	return PHON5_length;
    }	
    public void setPHON5_length(int PHON5_length) {
    	this.PHON5_length = PHON5_length;
    	this.PHON5_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON5_length(Integer PHON5_length) {
    	if( PHON5_length == null) {
    		this.PHON5_length = 0;
    	} else{
    		this.PHON5_length = PHON5_length.intValue();
    	}
    	this.PHON5_length_modified = true;
    	this.isModified = true;
    }
    public void setPHON5_length(String PHON5_length) {
    	if  (PHON5_length==null || PHON5_length.length() == 0) {
    		this.PHON5_length = 0;
    	} else {
    		this.PHON5_length = Integer.parseInt(PHON5_length);
    	}
    	this.PHON5_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_color
     * Comments    : 
     */	
    private String PHON5_color = null;
    
    private transient boolean PHON5_color_nullable = true;
    
    public boolean isNullablePHON5_color() {
    	return this.PHON5_color_nullable;
    }
    
    private transient boolean PHON5_color_invalidation = false;
    	
    public void setInvalidationPHON5_color(boolean invalidation) { 
    	this.PHON5_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_color() {
    	return this.PHON5_color_invalidation;
    }
    	
    private transient boolean PHON5_color_modified = false;
    
    public boolean isModifiedPHON5_color() {
    	return this.PHON5_color_modified;
    }
    	
    public void unModifiedPHON5_color() {
    	this.PHON5_color_modified = false;
    }
    public FieldProperty getPHON5_colorFieldProperty() {
    	return fieldPropertyMap.get("PHON5_color");
    }
    
    public String getPHON5_color() {
    	return PHON5_color;
    }	
    public String getNvlPHON5_color() {
    	if(getPHON5_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_color();
    	}
    }
    public void setPHON5_color(String PHON5_color) {
    	if(PHON5_color == null) {
    		this.PHON5_color = null;
    	} else {
    		this.PHON5_color = PHON5_color;
    	}
    	this.PHON5_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_hilight
     * Comments    : 
     */	
    private String PHON5_hilight = null;
    
    private transient boolean PHON5_hilight_nullable = true;
    
    public boolean isNullablePHON5_hilight() {
    	return this.PHON5_hilight_nullable;
    }
    
    private transient boolean PHON5_hilight_invalidation = false;
    	
    public void setInvalidationPHON5_hilight(boolean invalidation) { 
    	this.PHON5_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_hilight() {
    	return this.PHON5_hilight_invalidation;
    }
    	
    private transient boolean PHON5_hilight_modified = false;
    
    public boolean isModifiedPHON5_hilight() {
    	return this.PHON5_hilight_modified;
    }
    	
    public void unModifiedPHON5_hilight() {
    	this.PHON5_hilight_modified = false;
    }
    public FieldProperty getPHON5_hilightFieldProperty() {
    	return fieldPropertyMap.get("PHON5_hilight");
    }
    
    public String getPHON5_hilight() {
    	return PHON5_hilight;
    }	
    public String getNvlPHON5_hilight() {
    	if(getPHON5_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_hilight();
    	}
    }
    public void setPHON5_hilight(String PHON5_hilight) {
    	if(PHON5_hilight == null) {
    		this.PHON5_hilight = null;
    	} else {
    		this.PHON5_hilight = PHON5_hilight;
    	}
    	this.PHON5_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_outline
     * Comments    : 
     */	
    private String PHON5_outline = null;
    
    private transient boolean PHON5_outline_nullable = true;
    
    public boolean isNullablePHON5_outline() {
    	return this.PHON5_outline_nullable;
    }
    
    private transient boolean PHON5_outline_invalidation = false;
    	
    public void setInvalidationPHON5_outline(boolean invalidation) { 
    	this.PHON5_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_outline() {
    	return this.PHON5_outline_invalidation;
    }
    	
    private transient boolean PHON5_outline_modified = false;
    
    public boolean isModifiedPHON5_outline() {
    	return this.PHON5_outline_modified;
    }
    	
    public void unModifiedPHON5_outline() {
    	this.PHON5_outline_modified = false;
    }
    public FieldProperty getPHON5_outlineFieldProperty() {
    	return fieldPropertyMap.get("PHON5_outline");
    }
    
    public String getPHON5_outline() {
    	return PHON5_outline;
    }	
    public String getNvlPHON5_outline() {
    	if(getPHON5_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_outline();
    	}
    }
    public void setPHON5_outline(String PHON5_outline) {
    	if(PHON5_outline == null) {
    		this.PHON5_outline = null;
    	} else {
    		this.PHON5_outline = PHON5_outline;
    	}
    	this.PHON5_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_transp
     * Comments    : 
     */	
    private String PHON5_transp = null;
    
    private transient boolean PHON5_transp_nullable = true;
    
    public boolean isNullablePHON5_transp() {
    	return this.PHON5_transp_nullable;
    }
    
    private transient boolean PHON5_transp_invalidation = false;
    	
    public void setInvalidationPHON5_transp(boolean invalidation) { 
    	this.PHON5_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_transp() {
    	return this.PHON5_transp_invalidation;
    }
    	
    private transient boolean PHON5_transp_modified = false;
    
    public boolean isModifiedPHON5_transp() {
    	return this.PHON5_transp_modified;
    }
    	
    public void unModifiedPHON5_transp() {
    	this.PHON5_transp_modified = false;
    }
    public FieldProperty getPHON5_transpFieldProperty() {
    	return fieldPropertyMap.get("PHON5_transp");
    }
    
    public String getPHON5_transp() {
    	return PHON5_transp;
    }	
    public String getNvlPHON5_transp() {
    	if(getPHON5_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_transp();
    	}
    }
    public void setPHON5_transp(String PHON5_transp) {
    	if(PHON5_transp == null) {
    		this.PHON5_transp = null;
    	} else {
    		this.PHON5_transp = PHON5_transp;
    	}
    	this.PHON5_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_validn
     * Comments    : 
     */	
    private String PHON5_validn = null;
    
    private transient boolean PHON5_validn_nullable = true;
    
    public boolean isNullablePHON5_validn() {
    	return this.PHON5_validn_nullable;
    }
    
    private transient boolean PHON5_validn_invalidation = false;
    	
    public void setInvalidationPHON5_validn(boolean invalidation) { 
    	this.PHON5_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_validn() {
    	return this.PHON5_validn_invalidation;
    }
    	
    private transient boolean PHON5_validn_modified = false;
    
    public boolean isModifiedPHON5_validn() {
    	return this.PHON5_validn_modified;
    }
    	
    public void unModifiedPHON5_validn() {
    	this.PHON5_validn_modified = false;
    }
    public FieldProperty getPHON5_validnFieldProperty() {
    	return fieldPropertyMap.get("PHON5_validn");
    }
    
    public String getPHON5_validn() {
    	return PHON5_validn;
    }	
    public String getNvlPHON5_validn() {
    	if(getPHON5_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_validn();
    	}
    }
    public void setPHON5_validn(String PHON5_validn) {
    	if(PHON5_validn == null) {
    		this.PHON5_validn = null;
    	} else {
    		this.PHON5_validn = PHON5_validn;
    	}
    	this.PHON5_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_sosi
     * Comments    : 
     */	
    private String PHON5_sosi = null;
    
    private transient boolean PHON5_sosi_nullable = true;
    
    public boolean isNullablePHON5_sosi() {
    	return this.PHON5_sosi_nullable;
    }
    
    private transient boolean PHON5_sosi_invalidation = false;
    	
    public void setInvalidationPHON5_sosi(boolean invalidation) { 
    	this.PHON5_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_sosi() {
    	return this.PHON5_sosi_invalidation;
    }
    	
    private transient boolean PHON5_sosi_modified = false;
    
    public boolean isModifiedPHON5_sosi() {
    	return this.PHON5_sosi_modified;
    }
    	
    public void unModifiedPHON5_sosi() {
    	this.PHON5_sosi_modified = false;
    }
    public FieldProperty getPHON5_sosiFieldProperty() {
    	return fieldPropertyMap.get("PHON5_sosi");
    }
    
    public String getPHON5_sosi() {
    	return PHON5_sosi;
    }	
    public String getNvlPHON5_sosi() {
    	if(getPHON5_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_sosi();
    	}
    }
    public void setPHON5_sosi(String PHON5_sosi) {
    	if(PHON5_sosi == null) {
    		this.PHON5_sosi = null;
    	} else {
    		this.PHON5_sosi = PHON5_sosi;
    	}
    	this.PHON5_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : PHON5_ps
     * Comments    : 
     */	
    private String PHON5_ps = null;
    
    private transient boolean PHON5_ps_nullable = true;
    
    public boolean isNullablePHON5_ps() {
    	return this.PHON5_ps_nullable;
    }
    
    private transient boolean PHON5_ps_invalidation = false;
    	
    public void setInvalidationPHON5_ps(boolean invalidation) { 
    	this.PHON5_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationPHON5_ps() {
    	return this.PHON5_ps_invalidation;
    }
    	
    private transient boolean PHON5_ps_modified = false;
    
    public boolean isModifiedPHON5_ps() {
    	return this.PHON5_ps_modified;
    }
    	
    public void unModifiedPHON5_ps() {
    	this.PHON5_ps_modified = false;
    }
    public FieldProperty getPHON5_psFieldProperty() {
    	return fieldPropertyMap.get("PHON5_ps");
    }
    
    public String getPHON5_ps() {
    	return PHON5_ps;
    }	
    public String getNvlPHON5_ps() {
    	if(getPHON5_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getPHON5_ps();
    	}
    }
    public void setPHON5_ps(String PHON5_ps) {
    	if(PHON5_ps == null) {
    		this.PHON5_ps = null;
    	} else {
    		this.PHON5_ps = PHON5_ps;
    	}
    	this.PHON5_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_length
     * Comments    : 
     */	
    private int BIDX_length = 0;
    
    private transient boolean BIDX_length_nullable = false;
    
    public boolean isNullableBIDX_length() {
    	return this.BIDX_length_nullable;
    }
    
    private transient boolean BIDX_length_invalidation = false;
    	
    public void setInvalidationBIDX_length(boolean invalidation) { 
    	this.BIDX_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_length() {
    	return this.BIDX_length_invalidation;
    }
    	
    private transient boolean BIDX_length_modified = false;
    
    public boolean isModifiedBIDX_length() {
    	return this.BIDX_length_modified;
    }
    	
    public void unModifiedBIDX_length() {
    	this.BIDX_length_modified = false;
    }
    public FieldProperty getBIDX_lengthFieldProperty() {
    	return fieldPropertyMap.get("BIDX_length");
    }
    
    public int getBIDX_length() {
    	return BIDX_length;
    }	
    public void setBIDX_length(int BIDX_length) {
    	this.BIDX_length = BIDX_length;
    	this.BIDX_length_modified = true;
    	this.isModified = true;
    }
    public void setBIDX_length(Integer BIDX_length) {
    	if( BIDX_length == null) {
    		this.BIDX_length = 0;
    	} else{
    		this.BIDX_length = BIDX_length.intValue();
    	}
    	this.BIDX_length_modified = true;
    	this.isModified = true;
    }
    public void setBIDX_length(String BIDX_length) {
    	if  (BIDX_length==null || BIDX_length.length() == 0) {
    		this.BIDX_length = 0;
    	} else {
    		this.BIDX_length = Integer.parseInt(BIDX_length);
    	}
    	this.BIDX_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_color
     * Comments    : 
     */	
    private String BIDX_color = null;
    
    private transient boolean BIDX_color_nullable = true;
    
    public boolean isNullableBIDX_color() {
    	return this.BIDX_color_nullable;
    }
    
    private transient boolean BIDX_color_invalidation = false;
    	
    public void setInvalidationBIDX_color(boolean invalidation) { 
    	this.BIDX_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_color() {
    	return this.BIDX_color_invalidation;
    }
    	
    private transient boolean BIDX_color_modified = false;
    
    public boolean isModifiedBIDX_color() {
    	return this.BIDX_color_modified;
    }
    	
    public void unModifiedBIDX_color() {
    	this.BIDX_color_modified = false;
    }
    public FieldProperty getBIDX_colorFieldProperty() {
    	return fieldPropertyMap.get("BIDX_color");
    }
    
    public String getBIDX_color() {
    	return BIDX_color;
    }	
    public String getNvlBIDX_color() {
    	if(getBIDX_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_color();
    	}
    }
    public void setBIDX_color(String BIDX_color) {
    	if(BIDX_color == null) {
    		this.BIDX_color = null;
    	} else {
    		this.BIDX_color = BIDX_color;
    	}
    	this.BIDX_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_hilight
     * Comments    : 
     */	
    private String BIDX_hilight = null;
    
    private transient boolean BIDX_hilight_nullable = true;
    
    public boolean isNullableBIDX_hilight() {
    	return this.BIDX_hilight_nullable;
    }
    
    private transient boolean BIDX_hilight_invalidation = false;
    	
    public void setInvalidationBIDX_hilight(boolean invalidation) { 
    	this.BIDX_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_hilight() {
    	return this.BIDX_hilight_invalidation;
    }
    	
    private transient boolean BIDX_hilight_modified = false;
    
    public boolean isModifiedBIDX_hilight() {
    	return this.BIDX_hilight_modified;
    }
    	
    public void unModifiedBIDX_hilight() {
    	this.BIDX_hilight_modified = false;
    }
    public FieldProperty getBIDX_hilightFieldProperty() {
    	return fieldPropertyMap.get("BIDX_hilight");
    }
    
    public String getBIDX_hilight() {
    	return BIDX_hilight;
    }	
    public String getNvlBIDX_hilight() {
    	if(getBIDX_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_hilight();
    	}
    }
    public void setBIDX_hilight(String BIDX_hilight) {
    	if(BIDX_hilight == null) {
    		this.BIDX_hilight = null;
    	} else {
    		this.BIDX_hilight = BIDX_hilight;
    	}
    	this.BIDX_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_outline
     * Comments    : 
     */	
    private String BIDX_outline = null;
    
    private transient boolean BIDX_outline_nullable = true;
    
    public boolean isNullableBIDX_outline() {
    	return this.BIDX_outline_nullable;
    }
    
    private transient boolean BIDX_outline_invalidation = false;
    	
    public void setInvalidationBIDX_outline(boolean invalidation) { 
    	this.BIDX_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_outline() {
    	return this.BIDX_outline_invalidation;
    }
    	
    private transient boolean BIDX_outline_modified = false;
    
    public boolean isModifiedBIDX_outline() {
    	return this.BIDX_outline_modified;
    }
    	
    public void unModifiedBIDX_outline() {
    	this.BIDX_outline_modified = false;
    }
    public FieldProperty getBIDX_outlineFieldProperty() {
    	return fieldPropertyMap.get("BIDX_outline");
    }
    
    public String getBIDX_outline() {
    	return BIDX_outline;
    }	
    public String getNvlBIDX_outline() {
    	if(getBIDX_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_outline();
    	}
    }
    public void setBIDX_outline(String BIDX_outline) {
    	if(BIDX_outline == null) {
    		this.BIDX_outline = null;
    	} else {
    		this.BIDX_outline = BIDX_outline;
    	}
    	this.BIDX_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_transp
     * Comments    : 
     */	
    private String BIDX_transp = null;
    
    private transient boolean BIDX_transp_nullable = true;
    
    public boolean isNullableBIDX_transp() {
    	return this.BIDX_transp_nullable;
    }
    
    private transient boolean BIDX_transp_invalidation = false;
    	
    public void setInvalidationBIDX_transp(boolean invalidation) { 
    	this.BIDX_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_transp() {
    	return this.BIDX_transp_invalidation;
    }
    	
    private transient boolean BIDX_transp_modified = false;
    
    public boolean isModifiedBIDX_transp() {
    	return this.BIDX_transp_modified;
    }
    	
    public void unModifiedBIDX_transp() {
    	this.BIDX_transp_modified = false;
    }
    public FieldProperty getBIDX_transpFieldProperty() {
    	return fieldPropertyMap.get("BIDX_transp");
    }
    
    public String getBIDX_transp() {
    	return BIDX_transp;
    }	
    public String getNvlBIDX_transp() {
    	if(getBIDX_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_transp();
    	}
    }
    public void setBIDX_transp(String BIDX_transp) {
    	if(BIDX_transp == null) {
    		this.BIDX_transp = null;
    	} else {
    		this.BIDX_transp = BIDX_transp;
    	}
    	this.BIDX_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_validn
     * Comments    : 
     */	
    private String BIDX_validn = null;
    
    private transient boolean BIDX_validn_nullable = true;
    
    public boolean isNullableBIDX_validn() {
    	return this.BIDX_validn_nullable;
    }
    
    private transient boolean BIDX_validn_invalidation = false;
    	
    public void setInvalidationBIDX_validn(boolean invalidation) { 
    	this.BIDX_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_validn() {
    	return this.BIDX_validn_invalidation;
    }
    	
    private transient boolean BIDX_validn_modified = false;
    
    public boolean isModifiedBIDX_validn() {
    	return this.BIDX_validn_modified;
    }
    	
    public void unModifiedBIDX_validn() {
    	this.BIDX_validn_modified = false;
    }
    public FieldProperty getBIDX_validnFieldProperty() {
    	return fieldPropertyMap.get("BIDX_validn");
    }
    
    public String getBIDX_validn() {
    	return BIDX_validn;
    }	
    public String getNvlBIDX_validn() {
    	if(getBIDX_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_validn();
    	}
    }
    public void setBIDX_validn(String BIDX_validn) {
    	if(BIDX_validn == null) {
    		this.BIDX_validn = null;
    	} else {
    		this.BIDX_validn = BIDX_validn;
    	}
    	this.BIDX_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_sosi
     * Comments    : 
     */	
    private String BIDX_sosi = null;
    
    private transient boolean BIDX_sosi_nullable = true;
    
    public boolean isNullableBIDX_sosi() {
    	return this.BIDX_sosi_nullable;
    }
    
    private transient boolean BIDX_sosi_invalidation = false;
    	
    public void setInvalidationBIDX_sosi(boolean invalidation) { 
    	this.BIDX_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_sosi() {
    	return this.BIDX_sosi_invalidation;
    }
    	
    private transient boolean BIDX_sosi_modified = false;
    
    public boolean isModifiedBIDX_sosi() {
    	return this.BIDX_sosi_modified;
    }
    	
    public void unModifiedBIDX_sosi() {
    	this.BIDX_sosi_modified = false;
    }
    public FieldProperty getBIDX_sosiFieldProperty() {
    	return fieldPropertyMap.get("BIDX_sosi");
    }
    
    public String getBIDX_sosi() {
    	return BIDX_sosi;
    }	
    public String getNvlBIDX_sosi() {
    	if(getBIDX_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_sosi();
    	}
    }
    public void setBIDX_sosi(String BIDX_sosi) {
    	if(BIDX_sosi == null) {
    		this.BIDX_sosi = null;
    	} else {
    		this.BIDX_sosi = BIDX_sosi;
    	}
    	this.BIDX_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : BIDX_ps
     * Comments    : 
     */	
    private String BIDX_ps = null;
    
    private transient boolean BIDX_ps_nullable = true;
    
    public boolean isNullableBIDX_ps() {
    	return this.BIDX_ps_nullable;
    }
    
    private transient boolean BIDX_ps_invalidation = false;
    	
    public void setInvalidationBIDX_ps(boolean invalidation) { 
    	this.BIDX_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationBIDX_ps() {
    	return this.BIDX_ps_invalidation;
    }
    	
    private transient boolean BIDX_ps_modified = false;
    
    public boolean isModifiedBIDX_ps() {
    	return this.BIDX_ps_modified;
    }
    	
    public void unModifiedBIDX_ps() {
    	this.BIDX_ps_modified = false;
    }
    public FieldProperty getBIDX_psFieldProperty() {
    	return fieldPropertyMap.get("BIDX_ps");
    }
    
    public String getBIDX_ps() {
    	return BIDX_ps;
    }	
    public String getNvlBIDX_ps() {
    	if(getBIDX_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getBIDX_ps();
    	}
    }
    public void setBIDX_ps(String BIDX_ps) {
    	if(BIDX_ps == null) {
    		this.BIDX_ps = null;
    	} else {
    		this.BIDX_ps = BIDX_ps;
    	}
    	this.BIDX_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_length
     * Comments    : 
     */	
    private int FIDX_length = 0;
    
    private transient boolean FIDX_length_nullable = false;
    
    public boolean isNullableFIDX_length() {
    	return this.FIDX_length_nullable;
    }
    
    private transient boolean FIDX_length_invalidation = false;
    	
    public void setInvalidationFIDX_length(boolean invalidation) { 
    	this.FIDX_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_length() {
    	return this.FIDX_length_invalidation;
    }
    	
    private transient boolean FIDX_length_modified = false;
    
    public boolean isModifiedFIDX_length() {
    	return this.FIDX_length_modified;
    }
    	
    public void unModifiedFIDX_length() {
    	this.FIDX_length_modified = false;
    }
    public FieldProperty getFIDX_lengthFieldProperty() {
    	return fieldPropertyMap.get("FIDX_length");
    }
    
    public int getFIDX_length() {
    	return FIDX_length;
    }	
    public void setFIDX_length(int FIDX_length) {
    	this.FIDX_length = FIDX_length;
    	this.FIDX_length_modified = true;
    	this.isModified = true;
    }
    public void setFIDX_length(Integer FIDX_length) {
    	if( FIDX_length == null) {
    		this.FIDX_length = 0;
    	} else{
    		this.FIDX_length = FIDX_length.intValue();
    	}
    	this.FIDX_length_modified = true;
    	this.isModified = true;
    }
    public void setFIDX_length(String FIDX_length) {
    	if  (FIDX_length==null || FIDX_length.length() == 0) {
    		this.FIDX_length = 0;
    	} else {
    		this.FIDX_length = Integer.parseInt(FIDX_length);
    	}
    	this.FIDX_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_color
     * Comments    : 
     */	
    private String FIDX_color = null;
    
    private transient boolean FIDX_color_nullable = true;
    
    public boolean isNullableFIDX_color() {
    	return this.FIDX_color_nullable;
    }
    
    private transient boolean FIDX_color_invalidation = false;
    	
    public void setInvalidationFIDX_color(boolean invalidation) { 
    	this.FIDX_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_color() {
    	return this.FIDX_color_invalidation;
    }
    	
    private transient boolean FIDX_color_modified = false;
    
    public boolean isModifiedFIDX_color() {
    	return this.FIDX_color_modified;
    }
    	
    public void unModifiedFIDX_color() {
    	this.FIDX_color_modified = false;
    }
    public FieldProperty getFIDX_colorFieldProperty() {
    	return fieldPropertyMap.get("FIDX_color");
    }
    
    public String getFIDX_color() {
    	return FIDX_color;
    }	
    public String getNvlFIDX_color() {
    	if(getFIDX_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_color();
    	}
    }
    public void setFIDX_color(String FIDX_color) {
    	if(FIDX_color == null) {
    		this.FIDX_color = null;
    	} else {
    		this.FIDX_color = FIDX_color;
    	}
    	this.FIDX_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_hilight
     * Comments    : 
     */	
    private String FIDX_hilight = null;
    
    private transient boolean FIDX_hilight_nullable = true;
    
    public boolean isNullableFIDX_hilight() {
    	return this.FIDX_hilight_nullable;
    }
    
    private transient boolean FIDX_hilight_invalidation = false;
    	
    public void setInvalidationFIDX_hilight(boolean invalidation) { 
    	this.FIDX_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_hilight() {
    	return this.FIDX_hilight_invalidation;
    }
    	
    private transient boolean FIDX_hilight_modified = false;
    
    public boolean isModifiedFIDX_hilight() {
    	return this.FIDX_hilight_modified;
    }
    	
    public void unModifiedFIDX_hilight() {
    	this.FIDX_hilight_modified = false;
    }
    public FieldProperty getFIDX_hilightFieldProperty() {
    	return fieldPropertyMap.get("FIDX_hilight");
    }
    
    public String getFIDX_hilight() {
    	return FIDX_hilight;
    }	
    public String getNvlFIDX_hilight() {
    	if(getFIDX_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_hilight();
    	}
    }
    public void setFIDX_hilight(String FIDX_hilight) {
    	if(FIDX_hilight == null) {
    		this.FIDX_hilight = null;
    	} else {
    		this.FIDX_hilight = FIDX_hilight;
    	}
    	this.FIDX_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_outline
     * Comments    : 
     */	
    private String FIDX_outline = null;
    
    private transient boolean FIDX_outline_nullable = true;
    
    public boolean isNullableFIDX_outline() {
    	return this.FIDX_outline_nullable;
    }
    
    private transient boolean FIDX_outline_invalidation = false;
    	
    public void setInvalidationFIDX_outline(boolean invalidation) { 
    	this.FIDX_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_outline() {
    	return this.FIDX_outline_invalidation;
    }
    	
    private transient boolean FIDX_outline_modified = false;
    
    public boolean isModifiedFIDX_outline() {
    	return this.FIDX_outline_modified;
    }
    	
    public void unModifiedFIDX_outline() {
    	this.FIDX_outline_modified = false;
    }
    public FieldProperty getFIDX_outlineFieldProperty() {
    	return fieldPropertyMap.get("FIDX_outline");
    }
    
    public String getFIDX_outline() {
    	return FIDX_outline;
    }	
    public String getNvlFIDX_outline() {
    	if(getFIDX_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_outline();
    	}
    }
    public void setFIDX_outline(String FIDX_outline) {
    	if(FIDX_outline == null) {
    		this.FIDX_outline = null;
    	} else {
    		this.FIDX_outline = FIDX_outline;
    	}
    	this.FIDX_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_transp
     * Comments    : 
     */	
    private String FIDX_transp = null;
    
    private transient boolean FIDX_transp_nullable = true;
    
    public boolean isNullableFIDX_transp() {
    	return this.FIDX_transp_nullable;
    }
    
    private transient boolean FIDX_transp_invalidation = false;
    	
    public void setInvalidationFIDX_transp(boolean invalidation) { 
    	this.FIDX_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_transp() {
    	return this.FIDX_transp_invalidation;
    }
    	
    private transient boolean FIDX_transp_modified = false;
    
    public boolean isModifiedFIDX_transp() {
    	return this.FIDX_transp_modified;
    }
    	
    public void unModifiedFIDX_transp() {
    	this.FIDX_transp_modified = false;
    }
    public FieldProperty getFIDX_transpFieldProperty() {
    	return fieldPropertyMap.get("FIDX_transp");
    }
    
    public String getFIDX_transp() {
    	return FIDX_transp;
    }	
    public String getNvlFIDX_transp() {
    	if(getFIDX_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_transp();
    	}
    }
    public void setFIDX_transp(String FIDX_transp) {
    	if(FIDX_transp == null) {
    		this.FIDX_transp = null;
    	} else {
    		this.FIDX_transp = FIDX_transp;
    	}
    	this.FIDX_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_validn
     * Comments    : 
     */	
    private String FIDX_validn = null;
    
    private transient boolean FIDX_validn_nullable = true;
    
    public boolean isNullableFIDX_validn() {
    	return this.FIDX_validn_nullable;
    }
    
    private transient boolean FIDX_validn_invalidation = false;
    	
    public void setInvalidationFIDX_validn(boolean invalidation) { 
    	this.FIDX_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_validn() {
    	return this.FIDX_validn_invalidation;
    }
    	
    private transient boolean FIDX_validn_modified = false;
    
    public boolean isModifiedFIDX_validn() {
    	return this.FIDX_validn_modified;
    }
    	
    public void unModifiedFIDX_validn() {
    	this.FIDX_validn_modified = false;
    }
    public FieldProperty getFIDX_validnFieldProperty() {
    	return fieldPropertyMap.get("FIDX_validn");
    }
    
    public String getFIDX_validn() {
    	return FIDX_validn;
    }	
    public String getNvlFIDX_validn() {
    	if(getFIDX_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_validn();
    	}
    }
    public void setFIDX_validn(String FIDX_validn) {
    	if(FIDX_validn == null) {
    		this.FIDX_validn = null;
    	} else {
    		this.FIDX_validn = FIDX_validn;
    	}
    	this.FIDX_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_sosi
     * Comments    : 
     */	
    private String FIDX_sosi = null;
    
    private transient boolean FIDX_sosi_nullable = true;
    
    public boolean isNullableFIDX_sosi() {
    	return this.FIDX_sosi_nullable;
    }
    
    private transient boolean FIDX_sosi_invalidation = false;
    	
    public void setInvalidationFIDX_sosi(boolean invalidation) { 
    	this.FIDX_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_sosi() {
    	return this.FIDX_sosi_invalidation;
    }
    	
    private transient boolean FIDX_sosi_modified = false;
    
    public boolean isModifiedFIDX_sosi() {
    	return this.FIDX_sosi_modified;
    }
    	
    public void unModifiedFIDX_sosi() {
    	this.FIDX_sosi_modified = false;
    }
    public FieldProperty getFIDX_sosiFieldProperty() {
    	return fieldPropertyMap.get("FIDX_sosi");
    }
    
    public String getFIDX_sosi() {
    	return FIDX_sosi;
    }	
    public String getNvlFIDX_sosi() {
    	if(getFIDX_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_sosi();
    	}
    }
    public void setFIDX_sosi(String FIDX_sosi) {
    	if(FIDX_sosi == null) {
    		this.FIDX_sosi = null;
    	} else {
    		this.FIDX_sosi = FIDX_sosi;
    	}
    	this.FIDX_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : FIDX_ps
     * Comments    : 
     */	
    private String FIDX_ps = null;
    
    private transient boolean FIDX_ps_nullable = true;
    
    public boolean isNullableFIDX_ps() {
    	return this.FIDX_ps_nullable;
    }
    
    private transient boolean FIDX_ps_invalidation = false;
    	
    public void setInvalidationFIDX_ps(boolean invalidation) { 
    	this.FIDX_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationFIDX_ps() {
    	return this.FIDX_ps_invalidation;
    }
    	
    private transient boolean FIDX_ps_modified = false;
    
    public boolean isModifiedFIDX_ps() {
    	return this.FIDX_ps_modified;
    }
    	
    public void unModifiedFIDX_ps() {
    	this.FIDX_ps_modified = false;
    }
    public FieldProperty getFIDX_psFieldProperty() {
    	return fieldPropertyMap.get("FIDX_ps");
    }
    
    public String getFIDX_ps() {
    	return FIDX_ps;
    }	
    public String getNvlFIDX_ps() {
    	if(getFIDX_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getFIDX_ps();
    	}
    }
    public void setFIDX_ps(String FIDX_ps) {
    	if(FIDX_ps == null) {
    		this.FIDX_ps = null;
    	} else {
    		this.FIDX_ps = FIDX_ps;
    	}
    	this.FIDX_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_length
     * Comments    : 
     */	
    private int MSG_length = 0;
    
    private transient boolean MSG_length_nullable = false;
    
    public boolean isNullableMSG_length() {
    	return this.MSG_length_nullable;
    }
    
    private transient boolean MSG_length_invalidation = false;
    	
    public void setInvalidationMSG_length(boolean invalidation) { 
    	this.MSG_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_length() {
    	return this.MSG_length_invalidation;
    }
    	
    private transient boolean MSG_length_modified = false;
    
    public boolean isModifiedMSG_length() {
    	return this.MSG_length_modified;
    }
    	
    public void unModifiedMSG_length() {
    	this.MSG_length_modified = false;
    }
    public FieldProperty getMSG_lengthFieldProperty() {
    	return fieldPropertyMap.get("MSG_length");
    }
    
    public int getMSG_length() {
    	return MSG_length;
    }	
    public void setMSG_length(int MSG_length) {
    	this.MSG_length = MSG_length;
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(Integer MSG_length) {
    	if( MSG_length == null) {
    		this.MSG_length = 0;
    	} else{
    		this.MSG_length = MSG_length.intValue();
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(String MSG_length) {
    	if  (MSG_length==null || MSG_length.length() == 0) {
    		this.MSG_length = 0;
    	} else {
    		this.MSG_length = Integer.parseInt(MSG_length);
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_color
     * Comments    : 
     */	
    private String MSG_color = null;
    
    private transient boolean MSG_color_nullable = true;
    
    public boolean isNullableMSG_color() {
    	return this.MSG_color_nullable;
    }
    
    private transient boolean MSG_color_invalidation = false;
    	
    public void setInvalidationMSG_color(boolean invalidation) { 
    	this.MSG_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_color() {
    	return this.MSG_color_invalidation;
    }
    	
    private transient boolean MSG_color_modified = false;
    
    public boolean isModifiedMSG_color() {
    	return this.MSG_color_modified;
    }
    	
    public void unModifiedMSG_color() {
    	this.MSG_color_modified = false;
    }
    public FieldProperty getMSG_colorFieldProperty() {
    	return fieldPropertyMap.get("MSG_color");
    }
    
    public String getMSG_color() {
    	return MSG_color;
    }	
    public String getNvlMSG_color() {
    	if(getMSG_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_color();
    	}
    }
    public void setMSG_color(String MSG_color) {
    	if(MSG_color == null) {
    		this.MSG_color = null;
    	} else {
    		this.MSG_color = MSG_color;
    	}
    	this.MSG_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_hilight
     * Comments    : 
     */	
    private String MSG_hilight = null;
    
    private transient boolean MSG_hilight_nullable = true;
    
    public boolean isNullableMSG_hilight() {
    	return this.MSG_hilight_nullable;
    }
    
    private transient boolean MSG_hilight_invalidation = false;
    	
    public void setInvalidationMSG_hilight(boolean invalidation) { 
    	this.MSG_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_hilight() {
    	return this.MSG_hilight_invalidation;
    }
    	
    private transient boolean MSG_hilight_modified = false;
    
    public boolean isModifiedMSG_hilight() {
    	return this.MSG_hilight_modified;
    }
    	
    public void unModifiedMSG_hilight() {
    	this.MSG_hilight_modified = false;
    }
    public FieldProperty getMSG_hilightFieldProperty() {
    	return fieldPropertyMap.get("MSG_hilight");
    }
    
    public String getMSG_hilight() {
    	return MSG_hilight;
    }	
    public String getNvlMSG_hilight() {
    	if(getMSG_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_hilight();
    	}
    }
    public void setMSG_hilight(String MSG_hilight) {
    	if(MSG_hilight == null) {
    		this.MSG_hilight = null;
    	} else {
    		this.MSG_hilight = MSG_hilight;
    	}
    	this.MSG_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_outline
     * Comments    : 
     */	
    private String MSG_outline = null;
    
    private transient boolean MSG_outline_nullable = true;
    
    public boolean isNullableMSG_outline() {
    	return this.MSG_outline_nullable;
    }
    
    private transient boolean MSG_outline_invalidation = false;
    	
    public void setInvalidationMSG_outline(boolean invalidation) { 
    	this.MSG_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_outline() {
    	return this.MSG_outline_invalidation;
    }
    	
    private transient boolean MSG_outline_modified = false;
    
    public boolean isModifiedMSG_outline() {
    	return this.MSG_outline_modified;
    }
    	
    public void unModifiedMSG_outline() {
    	this.MSG_outline_modified = false;
    }
    public FieldProperty getMSG_outlineFieldProperty() {
    	return fieldPropertyMap.get("MSG_outline");
    }
    
    public String getMSG_outline() {
    	return MSG_outline;
    }	
    public String getNvlMSG_outline() {
    	if(getMSG_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_outline();
    	}
    }
    public void setMSG_outline(String MSG_outline) {
    	if(MSG_outline == null) {
    		this.MSG_outline = null;
    	} else {
    		this.MSG_outline = MSG_outline;
    	}
    	this.MSG_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_transp
     * Comments    : 
     */	
    private String MSG_transp = null;
    
    private transient boolean MSG_transp_nullable = true;
    
    public boolean isNullableMSG_transp() {
    	return this.MSG_transp_nullable;
    }
    
    private transient boolean MSG_transp_invalidation = false;
    	
    public void setInvalidationMSG_transp(boolean invalidation) { 
    	this.MSG_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_transp() {
    	return this.MSG_transp_invalidation;
    }
    	
    private transient boolean MSG_transp_modified = false;
    
    public boolean isModifiedMSG_transp() {
    	return this.MSG_transp_modified;
    }
    	
    public void unModifiedMSG_transp() {
    	this.MSG_transp_modified = false;
    }
    public FieldProperty getMSG_transpFieldProperty() {
    	return fieldPropertyMap.get("MSG_transp");
    }
    
    public String getMSG_transp() {
    	return MSG_transp;
    }	
    public String getNvlMSG_transp() {
    	if(getMSG_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_transp();
    	}
    }
    public void setMSG_transp(String MSG_transp) {
    	if(MSG_transp == null) {
    		this.MSG_transp = null;
    	} else {
    		this.MSG_transp = MSG_transp;
    	}
    	this.MSG_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_validn
     * Comments    : 
     */	
    private String MSG_validn = null;
    
    private transient boolean MSG_validn_nullable = true;
    
    public boolean isNullableMSG_validn() {
    	return this.MSG_validn_nullable;
    }
    
    private transient boolean MSG_validn_invalidation = false;
    	
    public void setInvalidationMSG_validn(boolean invalidation) { 
    	this.MSG_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_validn() {
    	return this.MSG_validn_invalidation;
    }
    	
    private transient boolean MSG_validn_modified = false;
    
    public boolean isModifiedMSG_validn() {
    	return this.MSG_validn_modified;
    }
    	
    public void unModifiedMSG_validn() {
    	this.MSG_validn_modified = false;
    }
    public FieldProperty getMSG_validnFieldProperty() {
    	return fieldPropertyMap.get("MSG_validn");
    }
    
    public String getMSG_validn() {
    	return MSG_validn;
    }	
    public String getNvlMSG_validn() {
    	if(getMSG_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_validn();
    	}
    }
    public void setMSG_validn(String MSG_validn) {
    	if(MSG_validn == null) {
    		this.MSG_validn = null;
    	} else {
    		this.MSG_validn = MSG_validn;
    	}
    	this.MSG_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_sosi
     * Comments    : 
     */	
    private String MSG_sosi = null;
    
    private transient boolean MSG_sosi_nullable = true;
    
    public boolean isNullableMSG_sosi() {
    	return this.MSG_sosi_nullable;
    }
    
    private transient boolean MSG_sosi_invalidation = false;
    	
    public void setInvalidationMSG_sosi(boolean invalidation) { 
    	this.MSG_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_sosi() {
    	return this.MSG_sosi_invalidation;
    }
    	
    private transient boolean MSG_sosi_modified = false;
    
    public boolean isModifiedMSG_sosi() {
    	return this.MSG_sosi_modified;
    }
    	
    public void unModifiedMSG_sosi() {
    	this.MSG_sosi_modified = false;
    }
    public FieldProperty getMSG_sosiFieldProperty() {
    	return fieldPropertyMap.get("MSG_sosi");
    }
    
    public String getMSG_sosi() {
    	return MSG_sosi;
    }	
    public String getNvlMSG_sosi() {
    	if(getMSG_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_sosi();
    	}
    }
    public void setMSG_sosi(String MSG_sosi) {
    	if(MSG_sosi == null) {
    		this.MSG_sosi = null;
    	} else {
    		this.MSG_sosi = MSG_sosi;
    	}
    	this.MSG_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_ps
     * Comments    : 
     */	
    private String MSG_ps = null;
    
    private transient boolean MSG_ps_nullable = true;
    
    public boolean isNullableMSG_ps() {
    	return this.MSG_ps_nullable;
    }
    
    private transient boolean MSG_ps_invalidation = false;
    	
    public void setInvalidationMSG_ps(boolean invalidation) { 
    	this.MSG_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_ps() {
    	return this.MSG_ps_invalidation;
    }
    	
    private transient boolean MSG_ps_modified = false;
    
    public boolean isModifiedMSG_ps() {
    	return this.MSG_ps_modified;
    }
    	
    public void unModifiedMSG_ps() {
    	this.MSG_ps_modified = false;
    }
    public FieldProperty getMSG_psFieldProperty() {
    	return fieldPropertyMap.get("MSG_ps");
    }
    
    public String getMSG_ps() {
    	return MSG_ps;
    }	
    public String getNvlMSG_ps() {
    	if(getMSG_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_ps();
    	}
    }
    public void setMSG_ps(String MSG_ps) {
    	if(MSG_ps == null) {
    		this.MSG_ps = null;
    	} else {
    		this.MSG_ps = MSG_ps;
    	}
    	this.MSG_ps_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_length_modified = false;
    	this.DATE_color_modified = false;
    	this.DATE_hilight_modified = false;
    	this.DATE_outline_modified = false;
    	this.DATE_transp_modified = false;
    	this.DATE_validn_modified = false;
    	this.DATE_sosi_modified = false;
    	this.DATE_ps_modified = false;
    	this.TERM_length_modified = false;
    	this.TERM_color_modified = false;
    	this.TERM_hilight_modified = false;
    	this.TERM_outline_modified = false;
    	this.TERM_transp_modified = false;
    	this.TERM_validn_modified = false;
    	this.TERM_sosi_modified = false;
    	this.TERM_ps_modified = false;
    	this.TIME_length_modified = false;
    	this.TIME_color_modified = false;
    	this.TIME_hilight_modified = false;
    	this.TIME_outline_modified = false;
    	this.TIME_transp_modified = false;
    	this.TIME_validn_modified = false;
    	this.TIME_sosi_modified = false;
    	this.TIME_ps_modified = false;
    	this.IDEN1_length_modified = false;
    	this.IDEN1_color_modified = false;
    	this.IDEN1_hilight_modified = false;
    	this.IDEN1_outline_modified = false;
    	this.IDEN1_transp_modified = false;
    	this.IDEN1_validn_modified = false;
    	this.IDEN1_sosi_modified = false;
    	this.IDEN1_ps_modified = false;
    	this.NAME1_length_modified = false;
    	this.NAME1_color_modified = false;
    	this.NAME1_hilight_modified = false;
    	this.NAME1_outline_modified = false;
    	this.NAME1_transp_modified = false;
    	this.NAME1_validn_modified = false;
    	this.NAME1_sosi_modified = false;
    	this.NAME1_ps_modified = false;
    	this.DEPT1_length_modified = false;
    	this.DEPT1_color_modified = false;
    	this.DEPT1_hilight_modified = false;
    	this.DEPT1_outline_modified = false;
    	this.DEPT1_transp_modified = false;
    	this.DEPT1_validn_modified = false;
    	this.DEPT1_sosi_modified = false;
    	this.DEPT1_ps_modified = false;
    	this.PHON1_length_modified = false;
    	this.PHON1_color_modified = false;
    	this.PHON1_hilight_modified = false;
    	this.PHON1_outline_modified = false;
    	this.PHON1_transp_modified = false;
    	this.PHON1_validn_modified = false;
    	this.PHON1_sosi_modified = false;
    	this.PHON1_ps_modified = false;
    	this.IDEN2_length_modified = false;
    	this.IDEN2_color_modified = false;
    	this.IDEN2_hilight_modified = false;
    	this.IDEN2_outline_modified = false;
    	this.IDEN2_transp_modified = false;
    	this.IDEN2_validn_modified = false;
    	this.IDEN2_sosi_modified = false;
    	this.IDEN2_ps_modified = false;
    	this.NAME2_length_modified = false;
    	this.NAME2_color_modified = false;
    	this.NAME2_hilight_modified = false;
    	this.NAME2_outline_modified = false;
    	this.NAME2_transp_modified = false;
    	this.NAME2_validn_modified = false;
    	this.NAME2_sosi_modified = false;
    	this.NAME2_ps_modified = false;
    	this.DEPT2_length_modified = false;
    	this.DEPT2_color_modified = false;
    	this.DEPT2_hilight_modified = false;
    	this.DEPT2_outline_modified = false;
    	this.DEPT2_transp_modified = false;
    	this.DEPT2_validn_modified = false;
    	this.DEPT2_sosi_modified = false;
    	this.DEPT2_ps_modified = false;
    	this.PHON2_length_modified = false;
    	this.PHON2_color_modified = false;
    	this.PHON2_hilight_modified = false;
    	this.PHON2_outline_modified = false;
    	this.PHON2_transp_modified = false;
    	this.PHON2_validn_modified = false;
    	this.PHON2_sosi_modified = false;
    	this.PHON2_ps_modified = false;
    	this.IDEN3_length_modified = false;
    	this.IDEN3_color_modified = false;
    	this.IDEN3_hilight_modified = false;
    	this.IDEN3_outline_modified = false;
    	this.IDEN3_transp_modified = false;
    	this.IDEN3_validn_modified = false;
    	this.IDEN3_sosi_modified = false;
    	this.IDEN3_ps_modified = false;
    	this.NAME3_length_modified = false;
    	this.NAME3_color_modified = false;
    	this.NAME3_hilight_modified = false;
    	this.NAME3_outline_modified = false;
    	this.NAME3_transp_modified = false;
    	this.NAME3_validn_modified = false;
    	this.NAME3_sosi_modified = false;
    	this.NAME3_ps_modified = false;
    	this.DEPT3_length_modified = false;
    	this.DEPT3_color_modified = false;
    	this.DEPT3_hilight_modified = false;
    	this.DEPT3_outline_modified = false;
    	this.DEPT3_transp_modified = false;
    	this.DEPT3_validn_modified = false;
    	this.DEPT3_sosi_modified = false;
    	this.DEPT3_ps_modified = false;
    	this.PHON3_length_modified = false;
    	this.PHON3_color_modified = false;
    	this.PHON3_hilight_modified = false;
    	this.PHON3_outline_modified = false;
    	this.PHON3_transp_modified = false;
    	this.PHON3_validn_modified = false;
    	this.PHON3_sosi_modified = false;
    	this.PHON3_ps_modified = false;
    	this.IDEN4_length_modified = false;
    	this.IDEN4_color_modified = false;
    	this.IDEN4_hilight_modified = false;
    	this.IDEN4_outline_modified = false;
    	this.IDEN4_transp_modified = false;
    	this.IDEN4_validn_modified = false;
    	this.IDEN4_sosi_modified = false;
    	this.IDEN4_ps_modified = false;
    	this.NAME4_length_modified = false;
    	this.NAME4_color_modified = false;
    	this.NAME4_hilight_modified = false;
    	this.NAME4_outline_modified = false;
    	this.NAME4_transp_modified = false;
    	this.NAME4_validn_modified = false;
    	this.NAME4_sosi_modified = false;
    	this.NAME4_ps_modified = false;
    	this.DEPT4_length_modified = false;
    	this.DEPT4_color_modified = false;
    	this.DEPT4_hilight_modified = false;
    	this.DEPT4_outline_modified = false;
    	this.DEPT4_transp_modified = false;
    	this.DEPT4_validn_modified = false;
    	this.DEPT4_sosi_modified = false;
    	this.DEPT4_ps_modified = false;
    	this.PHON4_length_modified = false;
    	this.PHON4_color_modified = false;
    	this.PHON4_hilight_modified = false;
    	this.PHON4_outline_modified = false;
    	this.PHON4_transp_modified = false;
    	this.PHON4_validn_modified = false;
    	this.PHON4_sosi_modified = false;
    	this.PHON4_ps_modified = false;
    	this.IDEN5_length_modified = false;
    	this.IDEN5_color_modified = false;
    	this.IDEN5_hilight_modified = false;
    	this.IDEN5_outline_modified = false;
    	this.IDEN5_transp_modified = false;
    	this.IDEN5_validn_modified = false;
    	this.IDEN5_sosi_modified = false;
    	this.IDEN5_ps_modified = false;
    	this.NAME5_length_modified = false;
    	this.NAME5_color_modified = false;
    	this.NAME5_hilight_modified = false;
    	this.NAME5_outline_modified = false;
    	this.NAME5_transp_modified = false;
    	this.NAME5_validn_modified = false;
    	this.NAME5_sosi_modified = false;
    	this.NAME5_ps_modified = false;
    	this.DEPT5_length_modified = false;
    	this.DEPT5_color_modified = false;
    	this.DEPT5_hilight_modified = false;
    	this.DEPT5_outline_modified = false;
    	this.DEPT5_transp_modified = false;
    	this.DEPT5_validn_modified = false;
    	this.DEPT5_sosi_modified = false;
    	this.DEPT5_ps_modified = false;
    	this.PHON5_length_modified = false;
    	this.PHON5_color_modified = false;
    	this.PHON5_hilight_modified = false;
    	this.PHON5_outline_modified = false;
    	this.PHON5_transp_modified = false;
    	this.PHON5_validn_modified = false;
    	this.PHON5_sosi_modified = false;
    	this.PHON5_ps_modified = false;
    	this.BIDX_length_modified = false;
    	this.BIDX_color_modified = false;
    	this.BIDX_hilight_modified = false;
    	this.BIDX_outline_modified = false;
    	this.BIDX_transp_modified = false;
    	this.BIDX_validn_modified = false;
    	this.BIDX_sosi_modified = false;
    	this.BIDX_ps_modified = false;
    	this.FIDX_length_modified = false;
    	this.FIDX_color_modified = false;
    	this.FIDX_hilight_modified = false;
    	this.FIDX_outline_modified = false;
    	this.FIDX_transp_modified = false;
    	this.FIDX_validn_modified = false;
    	this.FIDX_sosi_modified = false;
    	this.FIDX_ps_modified = false;
    	this.MSG_length_modified = false;
    	this.MSG_color_modified = false;
    	this.MSG_hilight_modified = false;
    	this.MSG_outline_modified = false;
    	this.MSG_transp_modified = false;
    	this.MSG_validn_modified = false;
    	this.MSG_sosi_modified = false;
    	this.MSG_ps_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_length_modified == true)
    		modifiedFields.add("DATE_length");
    	if(this.DATE_color_modified == true)
    		modifiedFields.add("DATE_color");
    	if(this.DATE_hilight_modified == true)
    		modifiedFields.add("DATE_hilight");
    	if(this.DATE_outline_modified == true)
    		modifiedFields.add("DATE_outline");
    	if(this.DATE_transp_modified == true)
    		modifiedFields.add("DATE_transp");
    	if(this.DATE_validn_modified == true)
    		modifiedFields.add("DATE_validn");
    	if(this.DATE_sosi_modified == true)
    		modifiedFields.add("DATE_sosi");
    	if(this.DATE_ps_modified == true)
    		modifiedFields.add("DATE_ps");
    	if(this.TERM_length_modified == true)
    		modifiedFields.add("TERM_length");
    	if(this.TERM_color_modified == true)
    		modifiedFields.add("TERM_color");
    	if(this.TERM_hilight_modified == true)
    		modifiedFields.add("TERM_hilight");
    	if(this.TERM_outline_modified == true)
    		modifiedFields.add("TERM_outline");
    	if(this.TERM_transp_modified == true)
    		modifiedFields.add("TERM_transp");
    	if(this.TERM_validn_modified == true)
    		modifiedFields.add("TERM_validn");
    	if(this.TERM_sosi_modified == true)
    		modifiedFields.add("TERM_sosi");
    	if(this.TERM_ps_modified == true)
    		modifiedFields.add("TERM_ps");
    	if(this.TIME_length_modified == true)
    		modifiedFields.add("TIME_length");
    	if(this.TIME_color_modified == true)
    		modifiedFields.add("TIME_color");
    	if(this.TIME_hilight_modified == true)
    		modifiedFields.add("TIME_hilight");
    	if(this.TIME_outline_modified == true)
    		modifiedFields.add("TIME_outline");
    	if(this.TIME_transp_modified == true)
    		modifiedFields.add("TIME_transp");
    	if(this.TIME_validn_modified == true)
    		modifiedFields.add("TIME_validn");
    	if(this.TIME_sosi_modified == true)
    		modifiedFields.add("TIME_sosi");
    	if(this.TIME_ps_modified == true)
    		modifiedFields.add("TIME_ps");
    	if(this.IDEN1_length_modified == true)
    		modifiedFields.add("IDEN1_length");
    	if(this.IDEN1_color_modified == true)
    		modifiedFields.add("IDEN1_color");
    	if(this.IDEN1_hilight_modified == true)
    		modifiedFields.add("IDEN1_hilight");
    	if(this.IDEN1_outline_modified == true)
    		modifiedFields.add("IDEN1_outline");
    	if(this.IDEN1_transp_modified == true)
    		modifiedFields.add("IDEN1_transp");
    	if(this.IDEN1_validn_modified == true)
    		modifiedFields.add("IDEN1_validn");
    	if(this.IDEN1_sosi_modified == true)
    		modifiedFields.add("IDEN1_sosi");
    	if(this.IDEN1_ps_modified == true)
    		modifiedFields.add("IDEN1_ps");
    	if(this.NAME1_length_modified == true)
    		modifiedFields.add("NAME1_length");
    	if(this.NAME1_color_modified == true)
    		modifiedFields.add("NAME1_color");
    	if(this.NAME1_hilight_modified == true)
    		modifiedFields.add("NAME1_hilight");
    	if(this.NAME1_outline_modified == true)
    		modifiedFields.add("NAME1_outline");
    	if(this.NAME1_transp_modified == true)
    		modifiedFields.add("NAME1_transp");
    	if(this.NAME1_validn_modified == true)
    		modifiedFields.add("NAME1_validn");
    	if(this.NAME1_sosi_modified == true)
    		modifiedFields.add("NAME1_sosi");
    	if(this.NAME1_ps_modified == true)
    		modifiedFields.add("NAME1_ps");
    	if(this.DEPT1_length_modified == true)
    		modifiedFields.add("DEPT1_length");
    	if(this.DEPT1_color_modified == true)
    		modifiedFields.add("DEPT1_color");
    	if(this.DEPT1_hilight_modified == true)
    		modifiedFields.add("DEPT1_hilight");
    	if(this.DEPT1_outline_modified == true)
    		modifiedFields.add("DEPT1_outline");
    	if(this.DEPT1_transp_modified == true)
    		modifiedFields.add("DEPT1_transp");
    	if(this.DEPT1_validn_modified == true)
    		modifiedFields.add("DEPT1_validn");
    	if(this.DEPT1_sosi_modified == true)
    		modifiedFields.add("DEPT1_sosi");
    	if(this.DEPT1_ps_modified == true)
    		modifiedFields.add("DEPT1_ps");
    	if(this.PHON1_length_modified == true)
    		modifiedFields.add("PHON1_length");
    	if(this.PHON1_color_modified == true)
    		modifiedFields.add("PHON1_color");
    	if(this.PHON1_hilight_modified == true)
    		modifiedFields.add("PHON1_hilight");
    	if(this.PHON1_outline_modified == true)
    		modifiedFields.add("PHON1_outline");
    	if(this.PHON1_transp_modified == true)
    		modifiedFields.add("PHON1_transp");
    	if(this.PHON1_validn_modified == true)
    		modifiedFields.add("PHON1_validn");
    	if(this.PHON1_sosi_modified == true)
    		modifiedFields.add("PHON1_sosi");
    	if(this.PHON1_ps_modified == true)
    		modifiedFields.add("PHON1_ps");
    	if(this.IDEN2_length_modified == true)
    		modifiedFields.add("IDEN2_length");
    	if(this.IDEN2_color_modified == true)
    		modifiedFields.add("IDEN2_color");
    	if(this.IDEN2_hilight_modified == true)
    		modifiedFields.add("IDEN2_hilight");
    	if(this.IDEN2_outline_modified == true)
    		modifiedFields.add("IDEN2_outline");
    	if(this.IDEN2_transp_modified == true)
    		modifiedFields.add("IDEN2_transp");
    	if(this.IDEN2_validn_modified == true)
    		modifiedFields.add("IDEN2_validn");
    	if(this.IDEN2_sosi_modified == true)
    		modifiedFields.add("IDEN2_sosi");
    	if(this.IDEN2_ps_modified == true)
    		modifiedFields.add("IDEN2_ps");
    	if(this.NAME2_length_modified == true)
    		modifiedFields.add("NAME2_length");
    	if(this.NAME2_color_modified == true)
    		modifiedFields.add("NAME2_color");
    	if(this.NAME2_hilight_modified == true)
    		modifiedFields.add("NAME2_hilight");
    	if(this.NAME2_outline_modified == true)
    		modifiedFields.add("NAME2_outline");
    	if(this.NAME2_transp_modified == true)
    		modifiedFields.add("NAME2_transp");
    	if(this.NAME2_validn_modified == true)
    		modifiedFields.add("NAME2_validn");
    	if(this.NAME2_sosi_modified == true)
    		modifiedFields.add("NAME2_sosi");
    	if(this.NAME2_ps_modified == true)
    		modifiedFields.add("NAME2_ps");
    	if(this.DEPT2_length_modified == true)
    		modifiedFields.add("DEPT2_length");
    	if(this.DEPT2_color_modified == true)
    		modifiedFields.add("DEPT2_color");
    	if(this.DEPT2_hilight_modified == true)
    		modifiedFields.add("DEPT2_hilight");
    	if(this.DEPT2_outline_modified == true)
    		modifiedFields.add("DEPT2_outline");
    	if(this.DEPT2_transp_modified == true)
    		modifiedFields.add("DEPT2_transp");
    	if(this.DEPT2_validn_modified == true)
    		modifiedFields.add("DEPT2_validn");
    	if(this.DEPT2_sosi_modified == true)
    		modifiedFields.add("DEPT2_sosi");
    	if(this.DEPT2_ps_modified == true)
    		modifiedFields.add("DEPT2_ps");
    	if(this.PHON2_length_modified == true)
    		modifiedFields.add("PHON2_length");
    	if(this.PHON2_color_modified == true)
    		modifiedFields.add("PHON2_color");
    	if(this.PHON2_hilight_modified == true)
    		modifiedFields.add("PHON2_hilight");
    	if(this.PHON2_outline_modified == true)
    		modifiedFields.add("PHON2_outline");
    	if(this.PHON2_transp_modified == true)
    		modifiedFields.add("PHON2_transp");
    	if(this.PHON2_validn_modified == true)
    		modifiedFields.add("PHON2_validn");
    	if(this.PHON2_sosi_modified == true)
    		modifiedFields.add("PHON2_sosi");
    	if(this.PHON2_ps_modified == true)
    		modifiedFields.add("PHON2_ps");
    	if(this.IDEN3_length_modified == true)
    		modifiedFields.add("IDEN3_length");
    	if(this.IDEN3_color_modified == true)
    		modifiedFields.add("IDEN3_color");
    	if(this.IDEN3_hilight_modified == true)
    		modifiedFields.add("IDEN3_hilight");
    	if(this.IDEN3_outline_modified == true)
    		modifiedFields.add("IDEN3_outline");
    	if(this.IDEN3_transp_modified == true)
    		modifiedFields.add("IDEN3_transp");
    	if(this.IDEN3_validn_modified == true)
    		modifiedFields.add("IDEN3_validn");
    	if(this.IDEN3_sosi_modified == true)
    		modifiedFields.add("IDEN3_sosi");
    	if(this.IDEN3_ps_modified == true)
    		modifiedFields.add("IDEN3_ps");
    	if(this.NAME3_length_modified == true)
    		modifiedFields.add("NAME3_length");
    	if(this.NAME3_color_modified == true)
    		modifiedFields.add("NAME3_color");
    	if(this.NAME3_hilight_modified == true)
    		modifiedFields.add("NAME3_hilight");
    	if(this.NAME3_outline_modified == true)
    		modifiedFields.add("NAME3_outline");
    	if(this.NAME3_transp_modified == true)
    		modifiedFields.add("NAME3_transp");
    	if(this.NAME3_validn_modified == true)
    		modifiedFields.add("NAME3_validn");
    	if(this.NAME3_sosi_modified == true)
    		modifiedFields.add("NAME3_sosi");
    	if(this.NAME3_ps_modified == true)
    		modifiedFields.add("NAME3_ps");
    	if(this.DEPT3_length_modified == true)
    		modifiedFields.add("DEPT3_length");
    	if(this.DEPT3_color_modified == true)
    		modifiedFields.add("DEPT3_color");
    	if(this.DEPT3_hilight_modified == true)
    		modifiedFields.add("DEPT3_hilight");
    	if(this.DEPT3_outline_modified == true)
    		modifiedFields.add("DEPT3_outline");
    	if(this.DEPT3_transp_modified == true)
    		modifiedFields.add("DEPT3_transp");
    	if(this.DEPT3_validn_modified == true)
    		modifiedFields.add("DEPT3_validn");
    	if(this.DEPT3_sosi_modified == true)
    		modifiedFields.add("DEPT3_sosi");
    	if(this.DEPT3_ps_modified == true)
    		modifiedFields.add("DEPT3_ps");
    	if(this.PHON3_length_modified == true)
    		modifiedFields.add("PHON3_length");
    	if(this.PHON3_color_modified == true)
    		modifiedFields.add("PHON3_color");
    	if(this.PHON3_hilight_modified == true)
    		modifiedFields.add("PHON3_hilight");
    	if(this.PHON3_outline_modified == true)
    		modifiedFields.add("PHON3_outline");
    	if(this.PHON3_transp_modified == true)
    		modifiedFields.add("PHON3_transp");
    	if(this.PHON3_validn_modified == true)
    		modifiedFields.add("PHON3_validn");
    	if(this.PHON3_sosi_modified == true)
    		modifiedFields.add("PHON3_sosi");
    	if(this.PHON3_ps_modified == true)
    		modifiedFields.add("PHON3_ps");
    	if(this.IDEN4_length_modified == true)
    		modifiedFields.add("IDEN4_length");
    	if(this.IDEN4_color_modified == true)
    		modifiedFields.add("IDEN4_color");
    	if(this.IDEN4_hilight_modified == true)
    		modifiedFields.add("IDEN4_hilight");
    	if(this.IDEN4_outline_modified == true)
    		modifiedFields.add("IDEN4_outline");
    	if(this.IDEN4_transp_modified == true)
    		modifiedFields.add("IDEN4_transp");
    	if(this.IDEN4_validn_modified == true)
    		modifiedFields.add("IDEN4_validn");
    	if(this.IDEN4_sosi_modified == true)
    		modifiedFields.add("IDEN4_sosi");
    	if(this.IDEN4_ps_modified == true)
    		modifiedFields.add("IDEN4_ps");
    	if(this.NAME4_length_modified == true)
    		modifiedFields.add("NAME4_length");
    	if(this.NAME4_color_modified == true)
    		modifiedFields.add("NAME4_color");
    	if(this.NAME4_hilight_modified == true)
    		modifiedFields.add("NAME4_hilight");
    	if(this.NAME4_outline_modified == true)
    		modifiedFields.add("NAME4_outline");
    	if(this.NAME4_transp_modified == true)
    		modifiedFields.add("NAME4_transp");
    	if(this.NAME4_validn_modified == true)
    		modifiedFields.add("NAME4_validn");
    	if(this.NAME4_sosi_modified == true)
    		modifiedFields.add("NAME4_sosi");
    	if(this.NAME4_ps_modified == true)
    		modifiedFields.add("NAME4_ps");
    	if(this.DEPT4_length_modified == true)
    		modifiedFields.add("DEPT4_length");
    	if(this.DEPT4_color_modified == true)
    		modifiedFields.add("DEPT4_color");
    	if(this.DEPT4_hilight_modified == true)
    		modifiedFields.add("DEPT4_hilight");
    	if(this.DEPT4_outline_modified == true)
    		modifiedFields.add("DEPT4_outline");
    	if(this.DEPT4_transp_modified == true)
    		modifiedFields.add("DEPT4_transp");
    	if(this.DEPT4_validn_modified == true)
    		modifiedFields.add("DEPT4_validn");
    	if(this.DEPT4_sosi_modified == true)
    		modifiedFields.add("DEPT4_sosi");
    	if(this.DEPT4_ps_modified == true)
    		modifiedFields.add("DEPT4_ps");
    	if(this.PHON4_length_modified == true)
    		modifiedFields.add("PHON4_length");
    	if(this.PHON4_color_modified == true)
    		modifiedFields.add("PHON4_color");
    	if(this.PHON4_hilight_modified == true)
    		modifiedFields.add("PHON4_hilight");
    	if(this.PHON4_outline_modified == true)
    		modifiedFields.add("PHON4_outline");
    	if(this.PHON4_transp_modified == true)
    		modifiedFields.add("PHON4_transp");
    	if(this.PHON4_validn_modified == true)
    		modifiedFields.add("PHON4_validn");
    	if(this.PHON4_sosi_modified == true)
    		modifiedFields.add("PHON4_sosi");
    	if(this.PHON4_ps_modified == true)
    		modifiedFields.add("PHON4_ps");
    	if(this.IDEN5_length_modified == true)
    		modifiedFields.add("IDEN5_length");
    	if(this.IDEN5_color_modified == true)
    		modifiedFields.add("IDEN5_color");
    	if(this.IDEN5_hilight_modified == true)
    		modifiedFields.add("IDEN5_hilight");
    	if(this.IDEN5_outline_modified == true)
    		modifiedFields.add("IDEN5_outline");
    	if(this.IDEN5_transp_modified == true)
    		modifiedFields.add("IDEN5_transp");
    	if(this.IDEN5_validn_modified == true)
    		modifiedFields.add("IDEN5_validn");
    	if(this.IDEN5_sosi_modified == true)
    		modifiedFields.add("IDEN5_sosi");
    	if(this.IDEN5_ps_modified == true)
    		modifiedFields.add("IDEN5_ps");
    	if(this.NAME5_length_modified == true)
    		modifiedFields.add("NAME5_length");
    	if(this.NAME5_color_modified == true)
    		modifiedFields.add("NAME5_color");
    	if(this.NAME5_hilight_modified == true)
    		modifiedFields.add("NAME5_hilight");
    	if(this.NAME5_outline_modified == true)
    		modifiedFields.add("NAME5_outline");
    	if(this.NAME5_transp_modified == true)
    		modifiedFields.add("NAME5_transp");
    	if(this.NAME5_validn_modified == true)
    		modifiedFields.add("NAME5_validn");
    	if(this.NAME5_sosi_modified == true)
    		modifiedFields.add("NAME5_sosi");
    	if(this.NAME5_ps_modified == true)
    		modifiedFields.add("NAME5_ps");
    	if(this.DEPT5_length_modified == true)
    		modifiedFields.add("DEPT5_length");
    	if(this.DEPT5_color_modified == true)
    		modifiedFields.add("DEPT5_color");
    	if(this.DEPT5_hilight_modified == true)
    		modifiedFields.add("DEPT5_hilight");
    	if(this.DEPT5_outline_modified == true)
    		modifiedFields.add("DEPT5_outline");
    	if(this.DEPT5_transp_modified == true)
    		modifiedFields.add("DEPT5_transp");
    	if(this.DEPT5_validn_modified == true)
    		modifiedFields.add("DEPT5_validn");
    	if(this.DEPT5_sosi_modified == true)
    		modifiedFields.add("DEPT5_sosi");
    	if(this.DEPT5_ps_modified == true)
    		modifiedFields.add("DEPT5_ps");
    	if(this.PHON5_length_modified == true)
    		modifiedFields.add("PHON5_length");
    	if(this.PHON5_color_modified == true)
    		modifiedFields.add("PHON5_color");
    	if(this.PHON5_hilight_modified == true)
    		modifiedFields.add("PHON5_hilight");
    	if(this.PHON5_outline_modified == true)
    		modifiedFields.add("PHON5_outline");
    	if(this.PHON5_transp_modified == true)
    		modifiedFields.add("PHON5_transp");
    	if(this.PHON5_validn_modified == true)
    		modifiedFields.add("PHON5_validn");
    	if(this.PHON5_sosi_modified == true)
    		modifiedFields.add("PHON5_sosi");
    	if(this.PHON5_ps_modified == true)
    		modifiedFields.add("PHON5_ps");
    	if(this.BIDX_length_modified == true)
    		modifiedFields.add("BIDX_length");
    	if(this.BIDX_color_modified == true)
    		modifiedFields.add("BIDX_color");
    	if(this.BIDX_hilight_modified == true)
    		modifiedFields.add("BIDX_hilight");
    	if(this.BIDX_outline_modified == true)
    		modifiedFields.add("BIDX_outline");
    	if(this.BIDX_transp_modified == true)
    		modifiedFields.add("BIDX_transp");
    	if(this.BIDX_validn_modified == true)
    		modifiedFields.add("BIDX_validn");
    	if(this.BIDX_sosi_modified == true)
    		modifiedFields.add("BIDX_sosi");
    	if(this.BIDX_ps_modified == true)
    		modifiedFields.add("BIDX_ps");
    	if(this.FIDX_length_modified == true)
    		modifiedFields.add("FIDX_length");
    	if(this.FIDX_color_modified == true)
    		modifiedFields.add("FIDX_color");
    	if(this.FIDX_hilight_modified == true)
    		modifiedFields.add("FIDX_hilight");
    	if(this.FIDX_outline_modified == true)
    		modifiedFields.add("FIDX_outline");
    	if(this.FIDX_transp_modified == true)
    		modifiedFields.add("FIDX_transp");
    	if(this.FIDX_validn_modified == true)
    		modifiedFields.add("FIDX_validn");
    	if(this.FIDX_sosi_modified == true)
    		modifiedFields.add("FIDX_sosi");
    	if(this.FIDX_ps_modified == true)
    		modifiedFields.add("FIDX_ps");
    	if(this.MSG_length_modified == true)
    		modifiedFields.add("MSG_length");
    	if(this.MSG_color_modified == true)
    		modifiedFields.add("MSG_color");
    	if(this.MSG_hilight_modified == true)
    		modifiedFields.add("MSG_hilight");
    	if(this.MSG_outline_modified == true)
    		modifiedFields.add("MSG_outline");
    	if(this.MSG_transp_modified == true)
    		modifiedFields.add("MSG_transp");
    	if(this.MSG_validn_modified == true)
    		modifiedFields.add("MSG_validn");
    	if(this.MSG_sosi_modified == true)
    		modifiedFields.add("MSG_sosi");
    	if(this.MSG_ps_modified == true)
    		modifiedFields.add("MSG_ps");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM02_OIVPM02_meta copyObj = new OIVPM02_OIVPM02_meta();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM02_OIVPM02_meta) {
    	if(this == _oIVPM02_OIVPM02_meta) return;
    	OIVPM02_OIVPM02_meta __oIVPM02_OIVPM02_meta = (OIVPM02_OIVPM02_meta) _oIVPM02_OIVPM02_meta;
    	this.setDATE_length(__oIVPM02_OIVPM02_meta.getDATE_length());
    	this.setDATE_color(__oIVPM02_OIVPM02_meta.getDATE_color());
    	this.setDATE_hilight(__oIVPM02_OIVPM02_meta.getDATE_hilight());
    	this.setDATE_outline(__oIVPM02_OIVPM02_meta.getDATE_outline());
    	this.setDATE_transp(__oIVPM02_OIVPM02_meta.getDATE_transp());
    	this.setDATE_validn(__oIVPM02_OIVPM02_meta.getDATE_validn());
    	this.setDATE_sosi(__oIVPM02_OIVPM02_meta.getDATE_sosi());
    	this.setDATE_ps(__oIVPM02_OIVPM02_meta.getDATE_ps());
    	this.setTERM_length(__oIVPM02_OIVPM02_meta.getTERM_length());
    	this.setTERM_color(__oIVPM02_OIVPM02_meta.getTERM_color());
    	this.setTERM_hilight(__oIVPM02_OIVPM02_meta.getTERM_hilight());
    	this.setTERM_outline(__oIVPM02_OIVPM02_meta.getTERM_outline());
    	this.setTERM_transp(__oIVPM02_OIVPM02_meta.getTERM_transp());
    	this.setTERM_validn(__oIVPM02_OIVPM02_meta.getTERM_validn());
    	this.setTERM_sosi(__oIVPM02_OIVPM02_meta.getTERM_sosi());
    	this.setTERM_ps(__oIVPM02_OIVPM02_meta.getTERM_ps());
    	this.setTIME_length(__oIVPM02_OIVPM02_meta.getTIME_length());
    	this.setTIME_color(__oIVPM02_OIVPM02_meta.getTIME_color());
    	this.setTIME_hilight(__oIVPM02_OIVPM02_meta.getTIME_hilight());
    	this.setTIME_outline(__oIVPM02_OIVPM02_meta.getTIME_outline());
    	this.setTIME_transp(__oIVPM02_OIVPM02_meta.getTIME_transp());
    	this.setTIME_validn(__oIVPM02_OIVPM02_meta.getTIME_validn());
    	this.setTIME_sosi(__oIVPM02_OIVPM02_meta.getTIME_sosi());
    	this.setTIME_ps(__oIVPM02_OIVPM02_meta.getTIME_ps());
    	this.setIDEN1_length(__oIVPM02_OIVPM02_meta.getIDEN1_length());
    	this.setIDEN1_color(__oIVPM02_OIVPM02_meta.getIDEN1_color());
    	this.setIDEN1_hilight(__oIVPM02_OIVPM02_meta.getIDEN1_hilight());
    	this.setIDEN1_outline(__oIVPM02_OIVPM02_meta.getIDEN1_outline());
    	this.setIDEN1_transp(__oIVPM02_OIVPM02_meta.getIDEN1_transp());
    	this.setIDEN1_validn(__oIVPM02_OIVPM02_meta.getIDEN1_validn());
    	this.setIDEN1_sosi(__oIVPM02_OIVPM02_meta.getIDEN1_sosi());
    	this.setIDEN1_ps(__oIVPM02_OIVPM02_meta.getIDEN1_ps());
    	this.setNAME1_length(__oIVPM02_OIVPM02_meta.getNAME1_length());
    	this.setNAME1_color(__oIVPM02_OIVPM02_meta.getNAME1_color());
    	this.setNAME1_hilight(__oIVPM02_OIVPM02_meta.getNAME1_hilight());
    	this.setNAME1_outline(__oIVPM02_OIVPM02_meta.getNAME1_outline());
    	this.setNAME1_transp(__oIVPM02_OIVPM02_meta.getNAME1_transp());
    	this.setNAME1_validn(__oIVPM02_OIVPM02_meta.getNAME1_validn());
    	this.setNAME1_sosi(__oIVPM02_OIVPM02_meta.getNAME1_sosi());
    	this.setNAME1_ps(__oIVPM02_OIVPM02_meta.getNAME1_ps());
    	this.setDEPT1_length(__oIVPM02_OIVPM02_meta.getDEPT1_length());
    	this.setDEPT1_color(__oIVPM02_OIVPM02_meta.getDEPT1_color());
    	this.setDEPT1_hilight(__oIVPM02_OIVPM02_meta.getDEPT1_hilight());
    	this.setDEPT1_outline(__oIVPM02_OIVPM02_meta.getDEPT1_outline());
    	this.setDEPT1_transp(__oIVPM02_OIVPM02_meta.getDEPT1_transp());
    	this.setDEPT1_validn(__oIVPM02_OIVPM02_meta.getDEPT1_validn());
    	this.setDEPT1_sosi(__oIVPM02_OIVPM02_meta.getDEPT1_sosi());
    	this.setDEPT1_ps(__oIVPM02_OIVPM02_meta.getDEPT1_ps());
    	this.setPHON1_length(__oIVPM02_OIVPM02_meta.getPHON1_length());
    	this.setPHON1_color(__oIVPM02_OIVPM02_meta.getPHON1_color());
    	this.setPHON1_hilight(__oIVPM02_OIVPM02_meta.getPHON1_hilight());
    	this.setPHON1_outline(__oIVPM02_OIVPM02_meta.getPHON1_outline());
    	this.setPHON1_transp(__oIVPM02_OIVPM02_meta.getPHON1_transp());
    	this.setPHON1_validn(__oIVPM02_OIVPM02_meta.getPHON1_validn());
    	this.setPHON1_sosi(__oIVPM02_OIVPM02_meta.getPHON1_sosi());
    	this.setPHON1_ps(__oIVPM02_OIVPM02_meta.getPHON1_ps());
    	this.setIDEN2_length(__oIVPM02_OIVPM02_meta.getIDEN2_length());
    	this.setIDEN2_color(__oIVPM02_OIVPM02_meta.getIDEN2_color());
    	this.setIDEN2_hilight(__oIVPM02_OIVPM02_meta.getIDEN2_hilight());
    	this.setIDEN2_outline(__oIVPM02_OIVPM02_meta.getIDEN2_outline());
    	this.setIDEN2_transp(__oIVPM02_OIVPM02_meta.getIDEN2_transp());
    	this.setIDEN2_validn(__oIVPM02_OIVPM02_meta.getIDEN2_validn());
    	this.setIDEN2_sosi(__oIVPM02_OIVPM02_meta.getIDEN2_sosi());
    	this.setIDEN2_ps(__oIVPM02_OIVPM02_meta.getIDEN2_ps());
    	this.setNAME2_length(__oIVPM02_OIVPM02_meta.getNAME2_length());
    	this.setNAME2_color(__oIVPM02_OIVPM02_meta.getNAME2_color());
    	this.setNAME2_hilight(__oIVPM02_OIVPM02_meta.getNAME2_hilight());
    	this.setNAME2_outline(__oIVPM02_OIVPM02_meta.getNAME2_outline());
    	this.setNAME2_transp(__oIVPM02_OIVPM02_meta.getNAME2_transp());
    	this.setNAME2_validn(__oIVPM02_OIVPM02_meta.getNAME2_validn());
    	this.setNAME2_sosi(__oIVPM02_OIVPM02_meta.getNAME2_sosi());
    	this.setNAME2_ps(__oIVPM02_OIVPM02_meta.getNAME2_ps());
    	this.setDEPT2_length(__oIVPM02_OIVPM02_meta.getDEPT2_length());
    	this.setDEPT2_color(__oIVPM02_OIVPM02_meta.getDEPT2_color());
    	this.setDEPT2_hilight(__oIVPM02_OIVPM02_meta.getDEPT2_hilight());
    	this.setDEPT2_outline(__oIVPM02_OIVPM02_meta.getDEPT2_outline());
    	this.setDEPT2_transp(__oIVPM02_OIVPM02_meta.getDEPT2_transp());
    	this.setDEPT2_validn(__oIVPM02_OIVPM02_meta.getDEPT2_validn());
    	this.setDEPT2_sosi(__oIVPM02_OIVPM02_meta.getDEPT2_sosi());
    	this.setDEPT2_ps(__oIVPM02_OIVPM02_meta.getDEPT2_ps());
    	this.setPHON2_length(__oIVPM02_OIVPM02_meta.getPHON2_length());
    	this.setPHON2_color(__oIVPM02_OIVPM02_meta.getPHON2_color());
    	this.setPHON2_hilight(__oIVPM02_OIVPM02_meta.getPHON2_hilight());
    	this.setPHON2_outline(__oIVPM02_OIVPM02_meta.getPHON2_outline());
    	this.setPHON2_transp(__oIVPM02_OIVPM02_meta.getPHON2_transp());
    	this.setPHON2_validn(__oIVPM02_OIVPM02_meta.getPHON2_validn());
    	this.setPHON2_sosi(__oIVPM02_OIVPM02_meta.getPHON2_sosi());
    	this.setPHON2_ps(__oIVPM02_OIVPM02_meta.getPHON2_ps());
    	this.setIDEN3_length(__oIVPM02_OIVPM02_meta.getIDEN3_length());
    	this.setIDEN3_color(__oIVPM02_OIVPM02_meta.getIDEN3_color());
    	this.setIDEN3_hilight(__oIVPM02_OIVPM02_meta.getIDEN3_hilight());
    	this.setIDEN3_outline(__oIVPM02_OIVPM02_meta.getIDEN3_outline());
    	this.setIDEN3_transp(__oIVPM02_OIVPM02_meta.getIDEN3_transp());
    	this.setIDEN3_validn(__oIVPM02_OIVPM02_meta.getIDEN3_validn());
    	this.setIDEN3_sosi(__oIVPM02_OIVPM02_meta.getIDEN3_sosi());
    	this.setIDEN3_ps(__oIVPM02_OIVPM02_meta.getIDEN3_ps());
    	this.setNAME3_length(__oIVPM02_OIVPM02_meta.getNAME3_length());
    	this.setNAME3_color(__oIVPM02_OIVPM02_meta.getNAME3_color());
    	this.setNAME3_hilight(__oIVPM02_OIVPM02_meta.getNAME3_hilight());
    	this.setNAME3_outline(__oIVPM02_OIVPM02_meta.getNAME3_outline());
    	this.setNAME3_transp(__oIVPM02_OIVPM02_meta.getNAME3_transp());
    	this.setNAME3_validn(__oIVPM02_OIVPM02_meta.getNAME3_validn());
    	this.setNAME3_sosi(__oIVPM02_OIVPM02_meta.getNAME3_sosi());
    	this.setNAME3_ps(__oIVPM02_OIVPM02_meta.getNAME3_ps());
    	this.setDEPT3_length(__oIVPM02_OIVPM02_meta.getDEPT3_length());
    	this.setDEPT3_color(__oIVPM02_OIVPM02_meta.getDEPT3_color());
    	this.setDEPT3_hilight(__oIVPM02_OIVPM02_meta.getDEPT3_hilight());
    	this.setDEPT3_outline(__oIVPM02_OIVPM02_meta.getDEPT3_outline());
    	this.setDEPT3_transp(__oIVPM02_OIVPM02_meta.getDEPT3_transp());
    	this.setDEPT3_validn(__oIVPM02_OIVPM02_meta.getDEPT3_validn());
    	this.setDEPT3_sosi(__oIVPM02_OIVPM02_meta.getDEPT3_sosi());
    	this.setDEPT3_ps(__oIVPM02_OIVPM02_meta.getDEPT3_ps());
    	this.setPHON3_length(__oIVPM02_OIVPM02_meta.getPHON3_length());
    	this.setPHON3_color(__oIVPM02_OIVPM02_meta.getPHON3_color());
    	this.setPHON3_hilight(__oIVPM02_OIVPM02_meta.getPHON3_hilight());
    	this.setPHON3_outline(__oIVPM02_OIVPM02_meta.getPHON3_outline());
    	this.setPHON3_transp(__oIVPM02_OIVPM02_meta.getPHON3_transp());
    	this.setPHON3_validn(__oIVPM02_OIVPM02_meta.getPHON3_validn());
    	this.setPHON3_sosi(__oIVPM02_OIVPM02_meta.getPHON3_sosi());
    	this.setPHON3_ps(__oIVPM02_OIVPM02_meta.getPHON3_ps());
    	this.setIDEN4_length(__oIVPM02_OIVPM02_meta.getIDEN4_length());
    	this.setIDEN4_color(__oIVPM02_OIVPM02_meta.getIDEN4_color());
    	this.setIDEN4_hilight(__oIVPM02_OIVPM02_meta.getIDEN4_hilight());
    	this.setIDEN4_outline(__oIVPM02_OIVPM02_meta.getIDEN4_outline());
    	this.setIDEN4_transp(__oIVPM02_OIVPM02_meta.getIDEN4_transp());
    	this.setIDEN4_validn(__oIVPM02_OIVPM02_meta.getIDEN4_validn());
    	this.setIDEN4_sosi(__oIVPM02_OIVPM02_meta.getIDEN4_sosi());
    	this.setIDEN4_ps(__oIVPM02_OIVPM02_meta.getIDEN4_ps());
    	this.setNAME4_length(__oIVPM02_OIVPM02_meta.getNAME4_length());
    	this.setNAME4_color(__oIVPM02_OIVPM02_meta.getNAME4_color());
    	this.setNAME4_hilight(__oIVPM02_OIVPM02_meta.getNAME4_hilight());
    	this.setNAME4_outline(__oIVPM02_OIVPM02_meta.getNAME4_outline());
    	this.setNAME4_transp(__oIVPM02_OIVPM02_meta.getNAME4_transp());
    	this.setNAME4_validn(__oIVPM02_OIVPM02_meta.getNAME4_validn());
    	this.setNAME4_sosi(__oIVPM02_OIVPM02_meta.getNAME4_sosi());
    	this.setNAME4_ps(__oIVPM02_OIVPM02_meta.getNAME4_ps());
    	this.setDEPT4_length(__oIVPM02_OIVPM02_meta.getDEPT4_length());
    	this.setDEPT4_color(__oIVPM02_OIVPM02_meta.getDEPT4_color());
    	this.setDEPT4_hilight(__oIVPM02_OIVPM02_meta.getDEPT4_hilight());
    	this.setDEPT4_outline(__oIVPM02_OIVPM02_meta.getDEPT4_outline());
    	this.setDEPT4_transp(__oIVPM02_OIVPM02_meta.getDEPT4_transp());
    	this.setDEPT4_validn(__oIVPM02_OIVPM02_meta.getDEPT4_validn());
    	this.setDEPT4_sosi(__oIVPM02_OIVPM02_meta.getDEPT4_sosi());
    	this.setDEPT4_ps(__oIVPM02_OIVPM02_meta.getDEPT4_ps());
    	this.setPHON4_length(__oIVPM02_OIVPM02_meta.getPHON4_length());
    	this.setPHON4_color(__oIVPM02_OIVPM02_meta.getPHON4_color());
    	this.setPHON4_hilight(__oIVPM02_OIVPM02_meta.getPHON4_hilight());
    	this.setPHON4_outline(__oIVPM02_OIVPM02_meta.getPHON4_outline());
    	this.setPHON4_transp(__oIVPM02_OIVPM02_meta.getPHON4_transp());
    	this.setPHON4_validn(__oIVPM02_OIVPM02_meta.getPHON4_validn());
    	this.setPHON4_sosi(__oIVPM02_OIVPM02_meta.getPHON4_sosi());
    	this.setPHON4_ps(__oIVPM02_OIVPM02_meta.getPHON4_ps());
    	this.setIDEN5_length(__oIVPM02_OIVPM02_meta.getIDEN5_length());
    	this.setIDEN5_color(__oIVPM02_OIVPM02_meta.getIDEN5_color());
    	this.setIDEN5_hilight(__oIVPM02_OIVPM02_meta.getIDEN5_hilight());
    	this.setIDEN5_outline(__oIVPM02_OIVPM02_meta.getIDEN5_outline());
    	this.setIDEN5_transp(__oIVPM02_OIVPM02_meta.getIDEN5_transp());
    	this.setIDEN5_validn(__oIVPM02_OIVPM02_meta.getIDEN5_validn());
    	this.setIDEN5_sosi(__oIVPM02_OIVPM02_meta.getIDEN5_sosi());
    	this.setIDEN5_ps(__oIVPM02_OIVPM02_meta.getIDEN5_ps());
    	this.setNAME5_length(__oIVPM02_OIVPM02_meta.getNAME5_length());
    	this.setNAME5_color(__oIVPM02_OIVPM02_meta.getNAME5_color());
    	this.setNAME5_hilight(__oIVPM02_OIVPM02_meta.getNAME5_hilight());
    	this.setNAME5_outline(__oIVPM02_OIVPM02_meta.getNAME5_outline());
    	this.setNAME5_transp(__oIVPM02_OIVPM02_meta.getNAME5_transp());
    	this.setNAME5_validn(__oIVPM02_OIVPM02_meta.getNAME5_validn());
    	this.setNAME5_sosi(__oIVPM02_OIVPM02_meta.getNAME5_sosi());
    	this.setNAME5_ps(__oIVPM02_OIVPM02_meta.getNAME5_ps());
    	this.setDEPT5_length(__oIVPM02_OIVPM02_meta.getDEPT5_length());
    	this.setDEPT5_color(__oIVPM02_OIVPM02_meta.getDEPT5_color());
    	this.setDEPT5_hilight(__oIVPM02_OIVPM02_meta.getDEPT5_hilight());
    	this.setDEPT5_outline(__oIVPM02_OIVPM02_meta.getDEPT5_outline());
    	this.setDEPT5_transp(__oIVPM02_OIVPM02_meta.getDEPT5_transp());
    	this.setDEPT5_validn(__oIVPM02_OIVPM02_meta.getDEPT5_validn());
    	this.setDEPT5_sosi(__oIVPM02_OIVPM02_meta.getDEPT5_sosi());
    	this.setDEPT5_ps(__oIVPM02_OIVPM02_meta.getDEPT5_ps());
    	this.setPHON5_length(__oIVPM02_OIVPM02_meta.getPHON5_length());
    	this.setPHON5_color(__oIVPM02_OIVPM02_meta.getPHON5_color());
    	this.setPHON5_hilight(__oIVPM02_OIVPM02_meta.getPHON5_hilight());
    	this.setPHON5_outline(__oIVPM02_OIVPM02_meta.getPHON5_outline());
    	this.setPHON5_transp(__oIVPM02_OIVPM02_meta.getPHON5_transp());
    	this.setPHON5_validn(__oIVPM02_OIVPM02_meta.getPHON5_validn());
    	this.setPHON5_sosi(__oIVPM02_OIVPM02_meta.getPHON5_sosi());
    	this.setPHON5_ps(__oIVPM02_OIVPM02_meta.getPHON5_ps());
    	this.setBIDX_length(__oIVPM02_OIVPM02_meta.getBIDX_length());
    	this.setBIDX_color(__oIVPM02_OIVPM02_meta.getBIDX_color());
    	this.setBIDX_hilight(__oIVPM02_OIVPM02_meta.getBIDX_hilight());
    	this.setBIDX_outline(__oIVPM02_OIVPM02_meta.getBIDX_outline());
    	this.setBIDX_transp(__oIVPM02_OIVPM02_meta.getBIDX_transp());
    	this.setBIDX_validn(__oIVPM02_OIVPM02_meta.getBIDX_validn());
    	this.setBIDX_sosi(__oIVPM02_OIVPM02_meta.getBIDX_sosi());
    	this.setBIDX_ps(__oIVPM02_OIVPM02_meta.getBIDX_ps());
    	this.setFIDX_length(__oIVPM02_OIVPM02_meta.getFIDX_length());
    	this.setFIDX_color(__oIVPM02_OIVPM02_meta.getFIDX_color());
    	this.setFIDX_hilight(__oIVPM02_OIVPM02_meta.getFIDX_hilight());
    	this.setFIDX_outline(__oIVPM02_OIVPM02_meta.getFIDX_outline());
    	this.setFIDX_transp(__oIVPM02_OIVPM02_meta.getFIDX_transp());
    	this.setFIDX_validn(__oIVPM02_OIVPM02_meta.getFIDX_validn());
    	this.setFIDX_sosi(__oIVPM02_OIVPM02_meta.getFIDX_sosi());
    	this.setFIDX_ps(__oIVPM02_OIVPM02_meta.getFIDX_ps());
    	this.setMSG_length(__oIVPM02_OIVPM02_meta.getMSG_length());
    	this.setMSG_color(__oIVPM02_OIVPM02_meta.getMSG_color());
    	this.setMSG_hilight(__oIVPM02_OIVPM02_meta.getMSG_hilight());
    	this.setMSG_outline(__oIVPM02_OIVPM02_meta.getMSG_outline());
    	this.setMSG_transp(__oIVPM02_OIVPM02_meta.getMSG_transp());
    	this.setMSG_validn(__oIVPM02_OIVPM02_meta.getMSG_validn());
    	this.setMSG_sosi(__oIVPM02_OIVPM02_meta.getMSG_sosi());
    	this.setMSG_ps(__oIVPM02_OIVPM02_meta.getMSG_ps());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE_length : ").append(DATE_length).append("\n");	
    	buffer.append("DATE_color : ").append(DATE_color).append("\n");	
    	buffer.append("DATE_hilight : ").append(DATE_hilight).append("\n");	
    	buffer.append("DATE_outline : ").append(DATE_outline).append("\n");	
    	buffer.append("DATE_transp : ").append(DATE_transp).append("\n");	
    	buffer.append("DATE_validn : ").append(DATE_validn).append("\n");	
    	buffer.append("DATE_sosi : ").append(DATE_sosi).append("\n");	
    	buffer.append("DATE_ps : ").append(DATE_ps).append("\n");	
    	buffer.append("TERM_length : ").append(TERM_length).append("\n");	
    	buffer.append("TERM_color : ").append(TERM_color).append("\n");	
    	buffer.append("TERM_hilight : ").append(TERM_hilight).append("\n");	
    	buffer.append("TERM_outline : ").append(TERM_outline).append("\n");	
    	buffer.append("TERM_transp : ").append(TERM_transp).append("\n");	
    	buffer.append("TERM_validn : ").append(TERM_validn).append("\n");	
    	buffer.append("TERM_sosi : ").append(TERM_sosi).append("\n");	
    	buffer.append("TERM_ps : ").append(TERM_ps).append("\n");	
    	buffer.append("TIME_length : ").append(TIME_length).append("\n");	
    	buffer.append("TIME_color : ").append(TIME_color).append("\n");	
    	buffer.append("TIME_hilight : ").append(TIME_hilight).append("\n");	
    	buffer.append("TIME_outline : ").append(TIME_outline).append("\n");	
    	buffer.append("TIME_transp : ").append(TIME_transp).append("\n");	
    	buffer.append("TIME_validn : ").append(TIME_validn).append("\n");	
    	buffer.append("TIME_sosi : ").append(TIME_sosi).append("\n");	
    	buffer.append("TIME_ps : ").append(TIME_ps).append("\n");	
    	buffer.append("IDEN1_length : ").append(IDEN1_length).append("\n");	
    	buffer.append("IDEN1_color : ").append(IDEN1_color).append("\n");	
    	buffer.append("IDEN1_hilight : ").append(IDEN1_hilight).append("\n");	
    	buffer.append("IDEN1_outline : ").append(IDEN1_outline).append("\n");	
    	buffer.append("IDEN1_transp : ").append(IDEN1_transp).append("\n");	
    	buffer.append("IDEN1_validn : ").append(IDEN1_validn).append("\n");	
    	buffer.append("IDEN1_sosi : ").append(IDEN1_sosi).append("\n");	
    	buffer.append("IDEN1_ps : ").append(IDEN1_ps).append("\n");	
    	buffer.append("NAME1_length : ").append(NAME1_length).append("\n");	
    	buffer.append("NAME1_color : ").append(NAME1_color).append("\n");	
    	buffer.append("NAME1_hilight : ").append(NAME1_hilight).append("\n");	
    	buffer.append("NAME1_outline : ").append(NAME1_outline).append("\n");	
    	buffer.append("NAME1_transp : ").append(NAME1_transp).append("\n");	
    	buffer.append("NAME1_validn : ").append(NAME1_validn).append("\n");	
    	buffer.append("NAME1_sosi : ").append(NAME1_sosi).append("\n");	
    	buffer.append("NAME1_ps : ").append(NAME1_ps).append("\n");	
    	buffer.append("DEPT1_length : ").append(DEPT1_length).append("\n");	
    	buffer.append("DEPT1_color : ").append(DEPT1_color).append("\n");	
    	buffer.append("DEPT1_hilight : ").append(DEPT1_hilight).append("\n");	
    	buffer.append("DEPT1_outline : ").append(DEPT1_outline).append("\n");	
    	buffer.append("DEPT1_transp : ").append(DEPT1_transp).append("\n");	
    	buffer.append("DEPT1_validn : ").append(DEPT1_validn).append("\n");	
    	buffer.append("DEPT1_sosi : ").append(DEPT1_sosi).append("\n");	
    	buffer.append("DEPT1_ps : ").append(DEPT1_ps).append("\n");	
    	buffer.append("PHON1_length : ").append(PHON1_length).append("\n");	
    	buffer.append("PHON1_color : ").append(PHON1_color).append("\n");	
    	buffer.append("PHON1_hilight : ").append(PHON1_hilight).append("\n");	
    	buffer.append("PHON1_outline : ").append(PHON1_outline).append("\n");	
    	buffer.append("PHON1_transp : ").append(PHON1_transp).append("\n");	
    	buffer.append("PHON1_validn : ").append(PHON1_validn).append("\n");	
    	buffer.append("PHON1_sosi : ").append(PHON1_sosi).append("\n");	
    	buffer.append("PHON1_ps : ").append(PHON1_ps).append("\n");	
    	buffer.append("IDEN2_length : ").append(IDEN2_length).append("\n");	
    	buffer.append("IDEN2_color : ").append(IDEN2_color).append("\n");	
    	buffer.append("IDEN2_hilight : ").append(IDEN2_hilight).append("\n");	
    	buffer.append("IDEN2_outline : ").append(IDEN2_outline).append("\n");	
    	buffer.append("IDEN2_transp : ").append(IDEN2_transp).append("\n");	
    	buffer.append("IDEN2_validn : ").append(IDEN2_validn).append("\n");	
    	buffer.append("IDEN2_sosi : ").append(IDEN2_sosi).append("\n");	
    	buffer.append("IDEN2_ps : ").append(IDEN2_ps).append("\n");	
    	buffer.append("NAME2_length : ").append(NAME2_length).append("\n");	
    	buffer.append("NAME2_color : ").append(NAME2_color).append("\n");	
    	buffer.append("NAME2_hilight : ").append(NAME2_hilight).append("\n");	
    	buffer.append("NAME2_outline : ").append(NAME2_outline).append("\n");	
    	buffer.append("NAME2_transp : ").append(NAME2_transp).append("\n");	
    	buffer.append("NAME2_validn : ").append(NAME2_validn).append("\n");	
    	buffer.append("NAME2_sosi : ").append(NAME2_sosi).append("\n");	
    	buffer.append("NAME2_ps : ").append(NAME2_ps).append("\n");	
    	buffer.append("DEPT2_length : ").append(DEPT2_length).append("\n");	
    	buffer.append("DEPT2_color : ").append(DEPT2_color).append("\n");	
    	buffer.append("DEPT2_hilight : ").append(DEPT2_hilight).append("\n");	
    	buffer.append("DEPT2_outline : ").append(DEPT2_outline).append("\n");	
    	buffer.append("DEPT2_transp : ").append(DEPT2_transp).append("\n");	
    	buffer.append("DEPT2_validn : ").append(DEPT2_validn).append("\n");	
    	buffer.append("DEPT2_sosi : ").append(DEPT2_sosi).append("\n");	
    	buffer.append("DEPT2_ps : ").append(DEPT2_ps).append("\n");	
    	buffer.append("PHON2_length : ").append(PHON2_length).append("\n");	
    	buffer.append("PHON2_color : ").append(PHON2_color).append("\n");	
    	buffer.append("PHON2_hilight : ").append(PHON2_hilight).append("\n");	
    	buffer.append("PHON2_outline : ").append(PHON2_outline).append("\n");	
    	buffer.append("PHON2_transp : ").append(PHON2_transp).append("\n");	
    	buffer.append("PHON2_validn : ").append(PHON2_validn).append("\n");	
    	buffer.append("PHON2_sosi : ").append(PHON2_sosi).append("\n");	
    	buffer.append("PHON2_ps : ").append(PHON2_ps).append("\n");	
    	buffer.append("IDEN3_length : ").append(IDEN3_length).append("\n");	
    	buffer.append("IDEN3_color : ").append(IDEN3_color).append("\n");	
    	buffer.append("IDEN3_hilight : ").append(IDEN3_hilight).append("\n");	
    	buffer.append("IDEN3_outline : ").append(IDEN3_outline).append("\n");	
    	buffer.append("IDEN3_transp : ").append(IDEN3_transp).append("\n");	
    	buffer.append("IDEN3_validn : ").append(IDEN3_validn).append("\n");	
    	buffer.append("IDEN3_sosi : ").append(IDEN3_sosi).append("\n");	
    	buffer.append("IDEN3_ps : ").append(IDEN3_ps).append("\n");	
    	buffer.append("NAME3_length : ").append(NAME3_length).append("\n");	
    	buffer.append("NAME3_color : ").append(NAME3_color).append("\n");	
    	buffer.append("NAME3_hilight : ").append(NAME3_hilight).append("\n");	
    	buffer.append("NAME3_outline : ").append(NAME3_outline).append("\n");	
    	buffer.append("NAME3_transp : ").append(NAME3_transp).append("\n");	
    	buffer.append("NAME3_validn : ").append(NAME3_validn).append("\n");	
    	buffer.append("NAME3_sosi : ").append(NAME3_sosi).append("\n");	
    	buffer.append("NAME3_ps : ").append(NAME3_ps).append("\n");	
    	buffer.append("DEPT3_length : ").append(DEPT3_length).append("\n");	
    	buffer.append("DEPT3_color : ").append(DEPT3_color).append("\n");	
    	buffer.append("DEPT3_hilight : ").append(DEPT3_hilight).append("\n");	
    	buffer.append("DEPT3_outline : ").append(DEPT3_outline).append("\n");	
    	buffer.append("DEPT3_transp : ").append(DEPT3_transp).append("\n");	
    	buffer.append("DEPT3_validn : ").append(DEPT3_validn).append("\n");	
    	buffer.append("DEPT3_sosi : ").append(DEPT3_sosi).append("\n");	
    	buffer.append("DEPT3_ps : ").append(DEPT3_ps).append("\n");	
    	buffer.append("PHON3_length : ").append(PHON3_length).append("\n");	
    	buffer.append("PHON3_color : ").append(PHON3_color).append("\n");	
    	buffer.append("PHON3_hilight : ").append(PHON3_hilight).append("\n");	
    	buffer.append("PHON3_outline : ").append(PHON3_outline).append("\n");	
    	buffer.append("PHON3_transp : ").append(PHON3_transp).append("\n");	
    	buffer.append("PHON3_validn : ").append(PHON3_validn).append("\n");	
    	buffer.append("PHON3_sosi : ").append(PHON3_sosi).append("\n");	
    	buffer.append("PHON3_ps : ").append(PHON3_ps).append("\n");	
    	buffer.append("IDEN4_length : ").append(IDEN4_length).append("\n");	
    	buffer.append("IDEN4_color : ").append(IDEN4_color).append("\n");	
    	buffer.append("IDEN4_hilight : ").append(IDEN4_hilight).append("\n");	
    	buffer.append("IDEN4_outline : ").append(IDEN4_outline).append("\n");	
    	buffer.append("IDEN4_transp : ").append(IDEN4_transp).append("\n");	
    	buffer.append("IDEN4_validn : ").append(IDEN4_validn).append("\n");	
    	buffer.append("IDEN4_sosi : ").append(IDEN4_sosi).append("\n");	
    	buffer.append("IDEN4_ps : ").append(IDEN4_ps).append("\n");	
    	buffer.append("NAME4_length : ").append(NAME4_length).append("\n");	
    	buffer.append("NAME4_color : ").append(NAME4_color).append("\n");	
    	buffer.append("NAME4_hilight : ").append(NAME4_hilight).append("\n");	
    	buffer.append("NAME4_outline : ").append(NAME4_outline).append("\n");	
    	buffer.append("NAME4_transp : ").append(NAME4_transp).append("\n");	
    	buffer.append("NAME4_validn : ").append(NAME4_validn).append("\n");	
    	buffer.append("NAME4_sosi : ").append(NAME4_sosi).append("\n");	
    	buffer.append("NAME4_ps : ").append(NAME4_ps).append("\n");	
    	buffer.append("DEPT4_length : ").append(DEPT4_length).append("\n");	
    	buffer.append("DEPT4_color : ").append(DEPT4_color).append("\n");	
    	buffer.append("DEPT4_hilight : ").append(DEPT4_hilight).append("\n");	
    	buffer.append("DEPT4_outline : ").append(DEPT4_outline).append("\n");	
    	buffer.append("DEPT4_transp : ").append(DEPT4_transp).append("\n");	
    	buffer.append("DEPT4_validn : ").append(DEPT4_validn).append("\n");	
    	buffer.append("DEPT4_sosi : ").append(DEPT4_sosi).append("\n");	
    	buffer.append("DEPT4_ps : ").append(DEPT4_ps).append("\n");	
    	buffer.append("PHON4_length : ").append(PHON4_length).append("\n");	
    	buffer.append("PHON4_color : ").append(PHON4_color).append("\n");	
    	buffer.append("PHON4_hilight : ").append(PHON4_hilight).append("\n");	
    	buffer.append("PHON4_outline : ").append(PHON4_outline).append("\n");	
    	buffer.append("PHON4_transp : ").append(PHON4_transp).append("\n");	
    	buffer.append("PHON4_validn : ").append(PHON4_validn).append("\n");	
    	buffer.append("PHON4_sosi : ").append(PHON4_sosi).append("\n");	
    	buffer.append("PHON4_ps : ").append(PHON4_ps).append("\n");	
    	buffer.append("IDEN5_length : ").append(IDEN5_length).append("\n");	
    	buffer.append("IDEN5_color : ").append(IDEN5_color).append("\n");	
    	buffer.append("IDEN5_hilight : ").append(IDEN5_hilight).append("\n");	
    	buffer.append("IDEN5_outline : ").append(IDEN5_outline).append("\n");	
    	buffer.append("IDEN5_transp : ").append(IDEN5_transp).append("\n");	
    	buffer.append("IDEN5_validn : ").append(IDEN5_validn).append("\n");	
    	buffer.append("IDEN5_sosi : ").append(IDEN5_sosi).append("\n");	
    	buffer.append("IDEN5_ps : ").append(IDEN5_ps).append("\n");	
    	buffer.append("NAME5_length : ").append(NAME5_length).append("\n");	
    	buffer.append("NAME5_color : ").append(NAME5_color).append("\n");	
    	buffer.append("NAME5_hilight : ").append(NAME5_hilight).append("\n");	
    	buffer.append("NAME5_outline : ").append(NAME5_outline).append("\n");	
    	buffer.append("NAME5_transp : ").append(NAME5_transp).append("\n");	
    	buffer.append("NAME5_validn : ").append(NAME5_validn).append("\n");	
    	buffer.append("NAME5_sosi : ").append(NAME5_sosi).append("\n");	
    	buffer.append("NAME5_ps : ").append(NAME5_ps).append("\n");	
    	buffer.append("DEPT5_length : ").append(DEPT5_length).append("\n");	
    	buffer.append("DEPT5_color : ").append(DEPT5_color).append("\n");	
    	buffer.append("DEPT5_hilight : ").append(DEPT5_hilight).append("\n");	
    	buffer.append("DEPT5_outline : ").append(DEPT5_outline).append("\n");	
    	buffer.append("DEPT5_transp : ").append(DEPT5_transp).append("\n");	
    	buffer.append("DEPT5_validn : ").append(DEPT5_validn).append("\n");	
    	buffer.append("DEPT5_sosi : ").append(DEPT5_sosi).append("\n");	
    	buffer.append("DEPT5_ps : ").append(DEPT5_ps).append("\n");	
    	buffer.append("PHON5_length : ").append(PHON5_length).append("\n");	
    	buffer.append("PHON5_color : ").append(PHON5_color).append("\n");	
    	buffer.append("PHON5_hilight : ").append(PHON5_hilight).append("\n");	
    	buffer.append("PHON5_outline : ").append(PHON5_outline).append("\n");	
    	buffer.append("PHON5_transp : ").append(PHON5_transp).append("\n");	
    	buffer.append("PHON5_validn : ").append(PHON5_validn).append("\n");	
    	buffer.append("PHON5_sosi : ").append(PHON5_sosi).append("\n");	
    	buffer.append("PHON5_ps : ").append(PHON5_ps).append("\n");	
    	buffer.append("BIDX_length : ").append(BIDX_length).append("\n");	
    	buffer.append("BIDX_color : ").append(BIDX_color).append("\n");	
    	buffer.append("BIDX_hilight : ").append(BIDX_hilight).append("\n");	
    	buffer.append("BIDX_outline : ").append(BIDX_outline).append("\n");	
    	buffer.append("BIDX_transp : ").append(BIDX_transp).append("\n");	
    	buffer.append("BIDX_validn : ").append(BIDX_validn).append("\n");	
    	buffer.append("BIDX_sosi : ").append(BIDX_sosi).append("\n");	
    	buffer.append("BIDX_ps : ").append(BIDX_ps).append("\n");	
    	buffer.append("FIDX_length : ").append(FIDX_length).append("\n");	
    	buffer.append("FIDX_color : ").append(FIDX_color).append("\n");	
    	buffer.append("FIDX_hilight : ").append(FIDX_hilight).append("\n");	
    	buffer.append("FIDX_outline : ").append(FIDX_outline).append("\n");	
    	buffer.append("FIDX_transp : ").append(FIDX_transp).append("\n");	
    	buffer.append("FIDX_validn : ").append(FIDX_validn).append("\n");	
    	buffer.append("FIDX_sosi : ").append(FIDX_sosi).append("\n");	
    	buffer.append("FIDX_ps : ").append(FIDX_ps).append("\n");	
    	buffer.append("MSG_length : ").append(MSG_length).append("\n");	
    	buffer.append("MSG_color : ").append(MSG_color).append("\n");	
    	buffer.append("MSG_hilight : ").append(MSG_hilight).append("\n");	
    	buffer.append("MSG_outline : ").append(MSG_outline).append("\n");	
    	buffer.append("MSG_transp : ").append(MSG_transp).append("\n");	
    	buffer.append("MSG_validn : ").append(MSG_validn).append("\n");	
    	buffer.append("MSG_sosi : ").append(MSG_sosi).append("\n");	
    	buffer.append("MSG_ps : ").append(MSG_ps).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(208);
    	fieldPropertyMap.put("DATE_length", FieldProperty.builder()
    	              .setPhysicalName("DATE_length")
    	              .setLogicalName("DATE_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DATE_color", FieldProperty.builder()
    	              .setPhysicalName("DATE_color")
    	              .setLogicalName("DATE_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_hilight", FieldProperty.builder()
    	              .setPhysicalName("DATE_hilight")
    	              .setLogicalName("DATE_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_outline", FieldProperty.builder()
    	              .setPhysicalName("DATE_outline")
    	              .setLogicalName("DATE_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_transp", FieldProperty.builder()
    	              .setPhysicalName("DATE_transp")
    	              .setLogicalName("DATE_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_validn", FieldProperty.builder()
    	              .setPhysicalName("DATE_validn")
    	              .setLogicalName("DATE_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_sosi", FieldProperty.builder()
    	              .setPhysicalName("DATE_sosi")
    	              .setLogicalName("DATE_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_ps", FieldProperty.builder()
    	              .setPhysicalName("DATE_ps")
    	              .setLogicalName("DATE_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_length", FieldProperty.builder()
    	              .setPhysicalName("TERM_length")
    	              .setLogicalName("TERM_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TERM_color", FieldProperty.builder()
    	              .setPhysicalName("TERM_color")
    	              .setLogicalName("TERM_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_hilight", FieldProperty.builder()
    	              .setPhysicalName("TERM_hilight")
    	              .setLogicalName("TERM_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_outline", FieldProperty.builder()
    	              .setPhysicalName("TERM_outline")
    	              .setLogicalName("TERM_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_transp", FieldProperty.builder()
    	              .setPhysicalName("TERM_transp")
    	              .setLogicalName("TERM_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_validn", FieldProperty.builder()
    	              .setPhysicalName("TERM_validn")
    	              .setLogicalName("TERM_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_sosi", FieldProperty.builder()
    	              .setPhysicalName("TERM_sosi")
    	              .setLogicalName("TERM_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_ps", FieldProperty.builder()
    	              .setPhysicalName("TERM_ps")
    	              .setLogicalName("TERM_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_length", FieldProperty.builder()
    	              .setPhysicalName("TIME_length")
    	              .setLogicalName("TIME_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TIME_color", FieldProperty.builder()
    	              .setPhysicalName("TIME_color")
    	              .setLogicalName("TIME_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_hilight", FieldProperty.builder()
    	              .setPhysicalName("TIME_hilight")
    	              .setLogicalName("TIME_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_outline", FieldProperty.builder()
    	              .setPhysicalName("TIME_outline")
    	              .setLogicalName("TIME_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_transp", FieldProperty.builder()
    	              .setPhysicalName("TIME_transp")
    	              .setLogicalName("TIME_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_validn", FieldProperty.builder()
    	              .setPhysicalName("TIME_validn")
    	              .setLogicalName("TIME_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_sosi", FieldProperty.builder()
    	              .setPhysicalName("TIME_sosi")
    	              .setLogicalName("TIME_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_ps", FieldProperty.builder()
    	              .setPhysicalName("TIME_ps")
    	              .setLogicalName("TIME_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_length")
    	              .setLogicalName("IDEN1_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN1_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_color")
    	              .setLogicalName("IDEN1_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_hilight")
    	              .setLogicalName("IDEN1_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_outline")
    	              .setLogicalName("IDEN1_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_transp")
    	              .setLogicalName("IDEN1_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_validn")
    	              .setLogicalName("IDEN1_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_sosi")
    	              .setLogicalName("IDEN1_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN1_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN1_ps")
    	              .setLogicalName("IDEN1_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_length", FieldProperty.builder()
    	              .setPhysicalName("NAME1_length")
    	              .setLogicalName("NAME1_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("NAME1_color", FieldProperty.builder()
    	              .setPhysicalName("NAME1_color")
    	              .setLogicalName("NAME1_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_hilight", FieldProperty.builder()
    	              .setPhysicalName("NAME1_hilight")
    	              .setLogicalName("NAME1_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_outline", FieldProperty.builder()
    	              .setPhysicalName("NAME1_outline")
    	              .setLogicalName("NAME1_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_transp", FieldProperty.builder()
    	              .setPhysicalName("NAME1_transp")
    	              .setLogicalName("NAME1_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_validn", FieldProperty.builder()
    	              .setPhysicalName("NAME1_validn")
    	              .setLogicalName("NAME1_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_sosi", FieldProperty.builder()
    	              .setPhysicalName("NAME1_sosi")
    	              .setLogicalName("NAME1_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME1_ps", FieldProperty.builder()
    	              .setPhysicalName("NAME1_ps")
    	              .setLogicalName("NAME1_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_length", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_length")
    	              .setLogicalName("DEPT1_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DEPT1_color", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_color")
    	              .setLogicalName("DEPT1_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_hilight", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_hilight")
    	              .setLogicalName("DEPT1_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_outline", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_outline")
    	              .setLogicalName("DEPT1_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_transp", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_transp")
    	              .setLogicalName("DEPT1_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_validn", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_validn")
    	              .setLogicalName("DEPT1_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_sosi", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_sosi")
    	              .setLogicalName("DEPT1_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT1_ps", FieldProperty.builder()
    	              .setPhysicalName("DEPT1_ps")
    	              .setLogicalName("DEPT1_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_length", FieldProperty.builder()
    	              .setPhysicalName("PHON1_length")
    	              .setLogicalName("PHON1_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("PHON1_color", FieldProperty.builder()
    	              .setPhysicalName("PHON1_color")
    	              .setLogicalName("PHON1_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_hilight", FieldProperty.builder()
    	              .setPhysicalName("PHON1_hilight")
    	              .setLogicalName("PHON1_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_outline", FieldProperty.builder()
    	              .setPhysicalName("PHON1_outline")
    	              .setLogicalName("PHON1_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_transp", FieldProperty.builder()
    	              .setPhysicalName("PHON1_transp")
    	              .setLogicalName("PHON1_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_validn", FieldProperty.builder()
    	              .setPhysicalName("PHON1_validn")
    	              .setLogicalName("PHON1_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_sosi", FieldProperty.builder()
    	              .setPhysicalName("PHON1_sosi")
    	              .setLogicalName("PHON1_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON1_ps", FieldProperty.builder()
    	              .setPhysicalName("PHON1_ps")
    	              .setLogicalName("PHON1_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_length")
    	              .setLogicalName("IDEN2_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN2_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_color")
    	              .setLogicalName("IDEN2_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_hilight")
    	              .setLogicalName("IDEN2_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_outline")
    	              .setLogicalName("IDEN2_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_transp")
    	              .setLogicalName("IDEN2_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_validn")
    	              .setLogicalName("IDEN2_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_sosi")
    	              .setLogicalName("IDEN2_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN2_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN2_ps")
    	              .setLogicalName("IDEN2_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_length", FieldProperty.builder()
    	              .setPhysicalName("NAME2_length")
    	              .setLogicalName("NAME2_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("NAME2_color", FieldProperty.builder()
    	              .setPhysicalName("NAME2_color")
    	              .setLogicalName("NAME2_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_hilight", FieldProperty.builder()
    	              .setPhysicalName("NAME2_hilight")
    	              .setLogicalName("NAME2_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_outline", FieldProperty.builder()
    	              .setPhysicalName("NAME2_outline")
    	              .setLogicalName("NAME2_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_transp", FieldProperty.builder()
    	              .setPhysicalName("NAME2_transp")
    	              .setLogicalName("NAME2_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_validn", FieldProperty.builder()
    	              .setPhysicalName("NAME2_validn")
    	              .setLogicalName("NAME2_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_sosi", FieldProperty.builder()
    	              .setPhysicalName("NAME2_sosi")
    	              .setLogicalName("NAME2_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME2_ps", FieldProperty.builder()
    	              .setPhysicalName("NAME2_ps")
    	              .setLogicalName("NAME2_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_length", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_length")
    	              .setLogicalName("DEPT2_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DEPT2_color", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_color")
    	              .setLogicalName("DEPT2_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_hilight", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_hilight")
    	              .setLogicalName("DEPT2_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_outline", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_outline")
    	              .setLogicalName("DEPT2_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_transp", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_transp")
    	              .setLogicalName("DEPT2_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_validn", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_validn")
    	              .setLogicalName("DEPT2_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_sosi", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_sosi")
    	              .setLogicalName("DEPT2_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT2_ps", FieldProperty.builder()
    	              .setPhysicalName("DEPT2_ps")
    	              .setLogicalName("DEPT2_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_length", FieldProperty.builder()
    	              .setPhysicalName("PHON2_length")
    	              .setLogicalName("PHON2_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("PHON2_color", FieldProperty.builder()
    	              .setPhysicalName("PHON2_color")
    	              .setLogicalName("PHON2_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_hilight", FieldProperty.builder()
    	              .setPhysicalName("PHON2_hilight")
    	              .setLogicalName("PHON2_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_outline", FieldProperty.builder()
    	              .setPhysicalName("PHON2_outline")
    	              .setLogicalName("PHON2_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_transp", FieldProperty.builder()
    	              .setPhysicalName("PHON2_transp")
    	              .setLogicalName("PHON2_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_validn", FieldProperty.builder()
    	              .setPhysicalName("PHON2_validn")
    	              .setLogicalName("PHON2_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_sosi", FieldProperty.builder()
    	              .setPhysicalName("PHON2_sosi")
    	              .setLogicalName("PHON2_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON2_ps", FieldProperty.builder()
    	              .setPhysicalName("PHON2_ps")
    	              .setLogicalName("PHON2_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_length")
    	              .setLogicalName("IDEN3_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN3_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_color")
    	              .setLogicalName("IDEN3_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_hilight")
    	              .setLogicalName("IDEN3_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_outline")
    	              .setLogicalName("IDEN3_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_transp")
    	              .setLogicalName("IDEN3_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_validn")
    	              .setLogicalName("IDEN3_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_sosi")
    	              .setLogicalName("IDEN3_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN3_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN3_ps")
    	              .setLogicalName("IDEN3_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_length", FieldProperty.builder()
    	              .setPhysicalName("NAME3_length")
    	              .setLogicalName("NAME3_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("NAME3_color", FieldProperty.builder()
    	              .setPhysicalName("NAME3_color")
    	              .setLogicalName("NAME3_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_hilight", FieldProperty.builder()
    	              .setPhysicalName("NAME3_hilight")
    	              .setLogicalName("NAME3_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_outline", FieldProperty.builder()
    	              .setPhysicalName("NAME3_outline")
    	              .setLogicalName("NAME3_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_transp", FieldProperty.builder()
    	              .setPhysicalName("NAME3_transp")
    	              .setLogicalName("NAME3_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_validn", FieldProperty.builder()
    	              .setPhysicalName("NAME3_validn")
    	              .setLogicalName("NAME3_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_sosi", FieldProperty.builder()
    	              .setPhysicalName("NAME3_sosi")
    	              .setLogicalName("NAME3_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME3_ps", FieldProperty.builder()
    	              .setPhysicalName("NAME3_ps")
    	              .setLogicalName("NAME3_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_length", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_length")
    	              .setLogicalName("DEPT3_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DEPT3_color", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_color")
    	              .setLogicalName("DEPT3_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_hilight", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_hilight")
    	              .setLogicalName("DEPT3_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_outline", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_outline")
    	              .setLogicalName("DEPT3_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_transp", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_transp")
    	              .setLogicalName("DEPT3_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_validn", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_validn")
    	              .setLogicalName("DEPT3_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_sosi", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_sosi")
    	              .setLogicalName("DEPT3_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT3_ps", FieldProperty.builder()
    	              .setPhysicalName("DEPT3_ps")
    	              .setLogicalName("DEPT3_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_length", FieldProperty.builder()
    	              .setPhysicalName("PHON3_length")
    	              .setLogicalName("PHON3_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("PHON3_color", FieldProperty.builder()
    	              .setPhysicalName("PHON3_color")
    	              .setLogicalName("PHON3_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_hilight", FieldProperty.builder()
    	              .setPhysicalName("PHON3_hilight")
    	              .setLogicalName("PHON3_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_outline", FieldProperty.builder()
    	              .setPhysicalName("PHON3_outline")
    	              .setLogicalName("PHON3_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_transp", FieldProperty.builder()
    	              .setPhysicalName("PHON3_transp")
    	              .setLogicalName("PHON3_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_validn", FieldProperty.builder()
    	              .setPhysicalName("PHON3_validn")
    	              .setLogicalName("PHON3_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_sosi", FieldProperty.builder()
    	              .setPhysicalName("PHON3_sosi")
    	              .setLogicalName("PHON3_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON3_ps", FieldProperty.builder()
    	              .setPhysicalName("PHON3_ps")
    	              .setLogicalName("PHON3_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_length")
    	              .setLogicalName("IDEN4_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN4_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_color")
    	              .setLogicalName("IDEN4_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_hilight")
    	              .setLogicalName("IDEN4_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_outline")
    	              .setLogicalName("IDEN4_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_transp")
    	              .setLogicalName("IDEN4_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_validn")
    	              .setLogicalName("IDEN4_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_sosi")
    	              .setLogicalName("IDEN4_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN4_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN4_ps")
    	              .setLogicalName("IDEN4_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_length", FieldProperty.builder()
    	              .setPhysicalName("NAME4_length")
    	              .setLogicalName("NAME4_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("NAME4_color", FieldProperty.builder()
    	              .setPhysicalName("NAME4_color")
    	              .setLogicalName("NAME4_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_hilight", FieldProperty.builder()
    	              .setPhysicalName("NAME4_hilight")
    	              .setLogicalName("NAME4_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_outline", FieldProperty.builder()
    	              .setPhysicalName("NAME4_outline")
    	              .setLogicalName("NAME4_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_transp", FieldProperty.builder()
    	              .setPhysicalName("NAME4_transp")
    	              .setLogicalName("NAME4_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_validn", FieldProperty.builder()
    	              .setPhysicalName("NAME4_validn")
    	              .setLogicalName("NAME4_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_sosi", FieldProperty.builder()
    	              .setPhysicalName("NAME4_sosi")
    	              .setLogicalName("NAME4_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME4_ps", FieldProperty.builder()
    	              .setPhysicalName("NAME4_ps")
    	              .setLogicalName("NAME4_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_length", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_length")
    	              .setLogicalName("DEPT4_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DEPT4_color", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_color")
    	              .setLogicalName("DEPT4_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_hilight", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_hilight")
    	              .setLogicalName("DEPT4_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_outline", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_outline")
    	              .setLogicalName("DEPT4_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_transp", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_transp")
    	              .setLogicalName("DEPT4_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_validn", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_validn")
    	              .setLogicalName("DEPT4_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_sosi", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_sosi")
    	              .setLogicalName("DEPT4_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT4_ps", FieldProperty.builder()
    	              .setPhysicalName("DEPT4_ps")
    	              .setLogicalName("DEPT4_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_length", FieldProperty.builder()
    	              .setPhysicalName("PHON4_length")
    	              .setLogicalName("PHON4_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("PHON4_color", FieldProperty.builder()
    	              .setPhysicalName("PHON4_color")
    	              .setLogicalName("PHON4_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_hilight", FieldProperty.builder()
    	              .setPhysicalName("PHON4_hilight")
    	              .setLogicalName("PHON4_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_outline", FieldProperty.builder()
    	              .setPhysicalName("PHON4_outline")
    	              .setLogicalName("PHON4_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_transp", FieldProperty.builder()
    	              .setPhysicalName("PHON4_transp")
    	              .setLogicalName("PHON4_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_validn", FieldProperty.builder()
    	              .setPhysicalName("PHON4_validn")
    	              .setLogicalName("PHON4_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_sosi", FieldProperty.builder()
    	              .setPhysicalName("PHON4_sosi")
    	              .setLogicalName("PHON4_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON4_ps", FieldProperty.builder()
    	              .setPhysicalName("PHON4_ps")
    	              .setLogicalName("PHON4_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_length")
    	              .setLogicalName("IDEN5_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN5_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_color")
    	              .setLogicalName("IDEN5_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_hilight")
    	              .setLogicalName("IDEN5_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_outline")
    	              .setLogicalName("IDEN5_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_transp")
    	              .setLogicalName("IDEN5_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_validn")
    	              .setLogicalName("IDEN5_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_sosi")
    	              .setLogicalName("IDEN5_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN5_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN5_ps")
    	              .setLogicalName("IDEN5_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_length", FieldProperty.builder()
    	              .setPhysicalName("NAME5_length")
    	              .setLogicalName("NAME5_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("NAME5_color", FieldProperty.builder()
    	              .setPhysicalName("NAME5_color")
    	              .setLogicalName("NAME5_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_hilight", FieldProperty.builder()
    	              .setPhysicalName("NAME5_hilight")
    	              .setLogicalName("NAME5_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_outline", FieldProperty.builder()
    	              .setPhysicalName("NAME5_outline")
    	              .setLogicalName("NAME5_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_transp", FieldProperty.builder()
    	              .setPhysicalName("NAME5_transp")
    	              .setLogicalName("NAME5_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_validn", FieldProperty.builder()
    	              .setPhysicalName("NAME5_validn")
    	              .setLogicalName("NAME5_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_sosi", FieldProperty.builder()
    	              .setPhysicalName("NAME5_sosi")
    	              .setLogicalName("NAME5_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("NAME5_ps", FieldProperty.builder()
    	              .setPhysicalName("NAME5_ps")
    	              .setLogicalName("NAME5_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_length", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_length")
    	              .setLogicalName("DEPT5_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DEPT5_color", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_color")
    	              .setLogicalName("DEPT5_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_hilight", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_hilight")
    	              .setLogicalName("DEPT5_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_outline", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_outline")
    	              .setLogicalName("DEPT5_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_transp", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_transp")
    	              .setLogicalName("DEPT5_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_validn", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_validn")
    	              .setLogicalName("DEPT5_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_sosi", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_sosi")
    	              .setLogicalName("DEPT5_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DEPT5_ps", FieldProperty.builder()
    	              .setPhysicalName("DEPT5_ps")
    	              .setLogicalName("DEPT5_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_length", FieldProperty.builder()
    	              .setPhysicalName("PHON5_length")
    	              .setLogicalName("PHON5_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("PHON5_color", FieldProperty.builder()
    	              .setPhysicalName("PHON5_color")
    	              .setLogicalName("PHON5_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_hilight", FieldProperty.builder()
    	              .setPhysicalName("PHON5_hilight")
    	              .setLogicalName("PHON5_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_outline", FieldProperty.builder()
    	              .setPhysicalName("PHON5_outline")
    	              .setLogicalName("PHON5_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_transp", FieldProperty.builder()
    	              .setPhysicalName("PHON5_transp")
    	              .setLogicalName("PHON5_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_validn", FieldProperty.builder()
    	              .setPhysicalName("PHON5_validn")
    	              .setLogicalName("PHON5_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_sosi", FieldProperty.builder()
    	              .setPhysicalName("PHON5_sosi")
    	              .setLogicalName("PHON5_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("PHON5_ps", FieldProperty.builder()
    	              .setPhysicalName("PHON5_ps")
    	              .setLogicalName("PHON5_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_length", FieldProperty.builder()
    	              .setPhysicalName("BIDX_length")
    	              .setLogicalName("BIDX_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("BIDX_color", FieldProperty.builder()
    	              .setPhysicalName("BIDX_color")
    	              .setLogicalName("BIDX_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_hilight", FieldProperty.builder()
    	              .setPhysicalName("BIDX_hilight")
    	              .setLogicalName("BIDX_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_outline", FieldProperty.builder()
    	              .setPhysicalName("BIDX_outline")
    	              .setLogicalName("BIDX_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_transp", FieldProperty.builder()
    	              .setPhysicalName("BIDX_transp")
    	              .setLogicalName("BIDX_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_validn", FieldProperty.builder()
    	              .setPhysicalName("BIDX_validn")
    	              .setLogicalName("BIDX_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_sosi", FieldProperty.builder()
    	              .setPhysicalName("BIDX_sosi")
    	              .setLogicalName("BIDX_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("BIDX_ps", FieldProperty.builder()
    	              .setPhysicalName("BIDX_ps")
    	              .setLogicalName("BIDX_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_length", FieldProperty.builder()
    	              .setPhysicalName("FIDX_length")
    	              .setLogicalName("FIDX_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("FIDX_color", FieldProperty.builder()
    	              .setPhysicalName("FIDX_color")
    	              .setLogicalName("FIDX_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_hilight", FieldProperty.builder()
    	              .setPhysicalName("FIDX_hilight")
    	              .setLogicalName("FIDX_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_outline", FieldProperty.builder()
    	              .setPhysicalName("FIDX_outline")
    	              .setLogicalName("FIDX_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_transp", FieldProperty.builder()
    	              .setPhysicalName("FIDX_transp")
    	              .setLogicalName("FIDX_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_validn", FieldProperty.builder()
    	              .setPhysicalName("FIDX_validn")
    	              .setLogicalName("FIDX_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_sosi", FieldProperty.builder()
    	              .setPhysicalName("FIDX_sosi")
    	              .setLogicalName("FIDX_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("FIDX_ps", FieldProperty.builder()
    	              .setPhysicalName("FIDX_ps")
    	              .setLogicalName("FIDX_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_length", FieldProperty.builder()
    	              .setPhysicalName("MSG_length")
    	              .setLogicalName("MSG_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("MSG_color", FieldProperty.builder()
    	              .setPhysicalName("MSG_color")
    	              .setLogicalName("MSG_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_hilight", FieldProperty.builder()
    	              .setPhysicalName("MSG_hilight")
    	              .setLogicalName("MSG_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_outline", FieldProperty.builder()
    	              .setPhysicalName("MSG_outline")
    	              .setLogicalName("MSG_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_transp", FieldProperty.builder()
    	              .setPhysicalName("MSG_transp")
    	              .setLogicalName("MSG_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_validn", FieldProperty.builder()
    	              .setPhysicalName("MSG_validn")
    	              .setLogicalName("MSG_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_sosi", FieldProperty.builder()
    	              .setPhysicalName("MSG_sosi")
    	              .setLogicalName("MSG_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_ps", FieldProperty.builder()
    	              .setPhysicalName("MSG_ps")
    	              .setLogicalName("MSG_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : return getDATE_length();
    		case "DATE_color" : return getDATE_color();
    		case "DATE_hilight" : return getDATE_hilight();
    		case "DATE_outline" : return getDATE_outline();
    		case "DATE_transp" : return getDATE_transp();
    		case "DATE_validn" : return getDATE_validn();
    		case "DATE_sosi" : return getDATE_sosi();
    		case "DATE_ps" : return getDATE_ps();
    		case "TERM_length" : return getTERM_length();
    		case "TERM_color" : return getTERM_color();
    		case "TERM_hilight" : return getTERM_hilight();
    		case "TERM_outline" : return getTERM_outline();
    		case "TERM_transp" : return getTERM_transp();
    		case "TERM_validn" : return getTERM_validn();
    		case "TERM_sosi" : return getTERM_sosi();
    		case "TERM_ps" : return getTERM_ps();
    		case "TIME_length" : return getTIME_length();
    		case "TIME_color" : return getTIME_color();
    		case "TIME_hilight" : return getTIME_hilight();
    		case "TIME_outline" : return getTIME_outline();
    		case "TIME_transp" : return getTIME_transp();
    		case "TIME_validn" : return getTIME_validn();
    		case "TIME_sosi" : return getTIME_sosi();
    		case "TIME_ps" : return getTIME_ps();
    		case "IDEN1_length" : return getIDEN1_length();
    		case "IDEN1_color" : return getIDEN1_color();
    		case "IDEN1_hilight" : return getIDEN1_hilight();
    		case "IDEN1_outline" : return getIDEN1_outline();
    		case "IDEN1_transp" : return getIDEN1_transp();
    		case "IDEN1_validn" : return getIDEN1_validn();
    		case "IDEN1_sosi" : return getIDEN1_sosi();
    		case "IDEN1_ps" : return getIDEN1_ps();
    		case "NAME1_length" : return getNAME1_length();
    		case "NAME1_color" : return getNAME1_color();
    		case "NAME1_hilight" : return getNAME1_hilight();
    		case "NAME1_outline" : return getNAME1_outline();
    		case "NAME1_transp" : return getNAME1_transp();
    		case "NAME1_validn" : return getNAME1_validn();
    		case "NAME1_sosi" : return getNAME1_sosi();
    		case "NAME1_ps" : return getNAME1_ps();
    		case "DEPT1_length" : return getDEPT1_length();
    		case "DEPT1_color" : return getDEPT1_color();
    		case "DEPT1_hilight" : return getDEPT1_hilight();
    		case "DEPT1_outline" : return getDEPT1_outline();
    		case "DEPT1_transp" : return getDEPT1_transp();
    		case "DEPT1_validn" : return getDEPT1_validn();
    		case "DEPT1_sosi" : return getDEPT1_sosi();
    		case "DEPT1_ps" : return getDEPT1_ps();
    		case "PHON1_length" : return getPHON1_length();
    		case "PHON1_color" : return getPHON1_color();
    		case "PHON1_hilight" : return getPHON1_hilight();
    		case "PHON1_outline" : return getPHON1_outline();
    		case "PHON1_transp" : return getPHON1_transp();
    		case "PHON1_validn" : return getPHON1_validn();
    		case "PHON1_sosi" : return getPHON1_sosi();
    		case "PHON1_ps" : return getPHON1_ps();
    		case "IDEN2_length" : return getIDEN2_length();
    		case "IDEN2_color" : return getIDEN2_color();
    		case "IDEN2_hilight" : return getIDEN2_hilight();
    		case "IDEN2_outline" : return getIDEN2_outline();
    		case "IDEN2_transp" : return getIDEN2_transp();
    		case "IDEN2_validn" : return getIDEN2_validn();
    		case "IDEN2_sosi" : return getIDEN2_sosi();
    		case "IDEN2_ps" : return getIDEN2_ps();
    		case "NAME2_length" : return getNAME2_length();
    		case "NAME2_color" : return getNAME2_color();
    		case "NAME2_hilight" : return getNAME2_hilight();
    		case "NAME2_outline" : return getNAME2_outline();
    		case "NAME2_transp" : return getNAME2_transp();
    		case "NAME2_validn" : return getNAME2_validn();
    		case "NAME2_sosi" : return getNAME2_sosi();
    		case "NAME2_ps" : return getNAME2_ps();
    		case "DEPT2_length" : return getDEPT2_length();
    		case "DEPT2_color" : return getDEPT2_color();
    		case "DEPT2_hilight" : return getDEPT2_hilight();
    		case "DEPT2_outline" : return getDEPT2_outline();
    		case "DEPT2_transp" : return getDEPT2_transp();
    		case "DEPT2_validn" : return getDEPT2_validn();
    		case "DEPT2_sosi" : return getDEPT2_sosi();
    		case "DEPT2_ps" : return getDEPT2_ps();
    		case "PHON2_length" : return getPHON2_length();
    		case "PHON2_color" : return getPHON2_color();
    		case "PHON2_hilight" : return getPHON2_hilight();
    		case "PHON2_outline" : return getPHON2_outline();
    		case "PHON2_transp" : return getPHON2_transp();
    		case "PHON2_validn" : return getPHON2_validn();
    		case "PHON2_sosi" : return getPHON2_sosi();
    		case "PHON2_ps" : return getPHON2_ps();
    		case "IDEN3_length" : return getIDEN3_length();
    		case "IDEN3_color" : return getIDEN3_color();
    		case "IDEN3_hilight" : return getIDEN3_hilight();
    		case "IDEN3_outline" : return getIDEN3_outline();
    		case "IDEN3_transp" : return getIDEN3_transp();
    		case "IDEN3_validn" : return getIDEN3_validn();
    		case "IDEN3_sosi" : return getIDEN3_sosi();
    		case "IDEN3_ps" : return getIDEN3_ps();
    		case "NAME3_length" : return getNAME3_length();
    		case "NAME3_color" : return getNAME3_color();
    		case "NAME3_hilight" : return getNAME3_hilight();
    		case "NAME3_outline" : return getNAME3_outline();
    		case "NAME3_transp" : return getNAME3_transp();
    		case "NAME3_validn" : return getNAME3_validn();
    		case "NAME3_sosi" : return getNAME3_sosi();
    		case "NAME3_ps" : return getNAME3_ps();
    		case "DEPT3_length" : return getDEPT3_length();
    		case "DEPT3_color" : return getDEPT3_color();
    		case "DEPT3_hilight" : return getDEPT3_hilight();
    		case "DEPT3_outline" : return getDEPT3_outline();
    		case "DEPT3_transp" : return getDEPT3_transp();
    		case "DEPT3_validn" : return getDEPT3_validn();
    		case "DEPT3_sosi" : return getDEPT3_sosi();
    		case "DEPT3_ps" : return getDEPT3_ps();
    		case "PHON3_length" : return getPHON3_length();
    		case "PHON3_color" : return getPHON3_color();
    		case "PHON3_hilight" : return getPHON3_hilight();
    		case "PHON3_outline" : return getPHON3_outline();
    		case "PHON3_transp" : return getPHON3_transp();
    		case "PHON3_validn" : return getPHON3_validn();
    		case "PHON3_sosi" : return getPHON3_sosi();
    		case "PHON3_ps" : return getPHON3_ps();
    		case "IDEN4_length" : return getIDEN4_length();
    		case "IDEN4_color" : return getIDEN4_color();
    		case "IDEN4_hilight" : return getIDEN4_hilight();
    		case "IDEN4_outline" : return getIDEN4_outline();
    		case "IDEN4_transp" : return getIDEN4_transp();
    		case "IDEN4_validn" : return getIDEN4_validn();
    		case "IDEN4_sosi" : return getIDEN4_sosi();
    		case "IDEN4_ps" : return getIDEN4_ps();
    		case "NAME4_length" : return getNAME4_length();
    		case "NAME4_color" : return getNAME4_color();
    		case "NAME4_hilight" : return getNAME4_hilight();
    		case "NAME4_outline" : return getNAME4_outline();
    		case "NAME4_transp" : return getNAME4_transp();
    		case "NAME4_validn" : return getNAME4_validn();
    		case "NAME4_sosi" : return getNAME4_sosi();
    		case "NAME4_ps" : return getNAME4_ps();
    		case "DEPT4_length" : return getDEPT4_length();
    		case "DEPT4_color" : return getDEPT4_color();
    		case "DEPT4_hilight" : return getDEPT4_hilight();
    		case "DEPT4_outline" : return getDEPT4_outline();
    		case "DEPT4_transp" : return getDEPT4_transp();
    		case "DEPT4_validn" : return getDEPT4_validn();
    		case "DEPT4_sosi" : return getDEPT4_sosi();
    		case "DEPT4_ps" : return getDEPT4_ps();
    		case "PHON4_length" : return getPHON4_length();
    		case "PHON4_color" : return getPHON4_color();
    		case "PHON4_hilight" : return getPHON4_hilight();
    		case "PHON4_outline" : return getPHON4_outline();
    		case "PHON4_transp" : return getPHON4_transp();
    		case "PHON4_validn" : return getPHON4_validn();
    		case "PHON4_sosi" : return getPHON4_sosi();
    		case "PHON4_ps" : return getPHON4_ps();
    		case "IDEN5_length" : return getIDEN5_length();
    		case "IDEN5_color" : return getIDEN5_color();
    		case "IDEN5_hilight" : return getIDEN5_hilight();
    		case "IDEN5_outline" : return getIDEN5_outline();
    		case "IDEN5_transp" : return getIDEN5_transp();
    		case "IDEN5_validn" : return getIDEN5_validn();
    		case "IDEN5_sosi" : return getIDEN5_sosi();
    		case "IDEN5_ps" : return getIDEN5_ps();
    		case "NAME5_length" : return getNAME5_length();
    		case "NAME5_color" : return getNAME5_color();
    		case "NAME5_hilight" : return getNAME5_hilight();
    		case "NAME5_outline" : return getNAME5_outline();
    		case "NAME5_transp" : return getNAME5_transp();
    		case "NAME5_validn" : return getNAME5_validn();
    		case "NAME5_sosi" : return getNAME5_sosi();
    		case "NAME5_ps" : return getNAME5_ps();
    		case "DEPT5_length" : return getDEPT5_length();
    		case "DEPT5_color" : return getDEPT5_color();
    		case "DEPT5_hilight" : return getDEPT5_hilight();
    		case "DEPT5_outline" : return getDEPT5_outline();
    		case "DEPT5_transp" : return getDEPT5_transp();
    		case "DEPT5_validn" : return getDEPT5_validn();
    		case "DEPT5_sosi" : return getDEPT5_sosi();
    		case "DEPT5_ps" : return getDEPT5_ps();
    		case "PHON5_length" : return getPHON5_length();
    		case "PHON5_color" : return getPHON5_color();
    		case "PHON5_hilight" : return getPHON5_hilight();
    		case "PHON5_outline" : return getPHON5_outline();
    		case "PHON5_transp" : return getPHON5_transp();
    		case "PHON5_validn" : return getPHON5_validn();
    		case "PHON5_sosi" : return getPHON5_sosi();
    		case "PHON5_ps" : return getPHON5_ps();
    		case "BIDX_length" : return getBIDX_length();
    		case "BIDX_color" : return getBIDX_color();
    		case "BIDX_hilight" : return getBIDX_hilight();
    		case "BIDX_outline" : return getBIDX_outline();
    		case "BIDX_transp" : return getBIDX_transp();
    		case "BIDX_validn" : return getBIDX_validn();
    		case "BIDX_sosi" : return getBIDX_sosi();
    		case "BIDX_ps" : return getBIDX_ps();
    		case "FIDX_length" : return getFIDX_length();
    		case "FIDX_color" : return getFIDX_color();
    		case "FIDX_hilight" : return getFIDX_hilight();
    		case "FIDX_outline" : return getFIDX_outline();
    		case "FIDX_transp" : return getFIDX_transp();
    		case "FIDX_validn" : return getFIDX_validn();
    		case "FIDX_sosi" : return getFIDX_sosi();
    		case "FIDX_ps" : return getFIDX_ps();
    		case "MSG_length" : return getMSG_length();
    		case "MSG_color" : return getMSG_color();
    		case "MSG_hilight" : return getMSG_hilight();
    		case "MSG_outline" : return getMSG_outline();
    		case "MSG_transp" : return getMSG_transp();
    		case "MSG_validn" : return getMSG_validn();
    		case "MSG_sosi" : return getMSG_sosi();
    		case "MSG_ps" : return getMSG_ps();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : setDATE_length((Integer)arg);break;
    		case "DATE_color" : setDATE_color((String)arg); break;
    		case "DATE_hilight" : setDATE_hilight((String)arg); break;
    		case "DATE_outline" : setDATE_outline((String)arg); break;
    		case "DATE_transp" : setDATE_transp((String)arg); break;
    		case "DATE_validn" : setDATE_validn((String)arg); break;
    		case "DATE_sosi" : setDATE_sosi((String)arg); break;
    		case "DATE_ps" : setDATE_ps((String)arg); break;
    		case "TERM_length" : setTERM_length((Integer)arg);break;
    		case "TERM_color" : setTERM_color((String)arg); break;
    		case "TERM_hilight" : setTERM_hilight((String)arg); break;
    		case "TERM_outline" : setTERM_outline((String)arg); break;
    		case "TERM_transp" : setTERM_transp((String)arg); break;
    		case "TERM_validn" : setTERM_validn((String)arg); break;
    		case "TERM_sosi" : setTERM_sosi((String)arg); break;
    		case "TERM_ps" : setTERM_ps((String)arg); break;
    		case "TIME_length" : setTIME_length((Integer)arg);break;
    		case "TIME_color" : setTIME_color((String)arg); break;
    		case "TIME_hilight" : setTIME_hilight((String)arg); break;
    		case "TIME_outline" : setTIME_outline((String)arg); break;
    		case "TIME_transp" : setTIME_transp((String)arg); break;
    		case "TIME_validn" : setTIME_validn((String)arg); break;
    		case "TIME_sosi" : setTIME_sosi((String)arg); break;
    		case "TIME_ps" : setTIME_ps((String)arg); break;
    		case "IDEN1_length" : setIDEN1_length((Integer)arg);break;
    		case "IDEN1_color" : setIDEN1_color((String)arg); break;
    		case "IDEN1_hilight" : setIDEN1_hilight((String)arg); break;
    		case "IDEN1_outline" : setIDEN1_outline((String)arg); break;
    		case "IDEN1_transp" : setIDEN1_transp((String)arg); break;
    		case "IDEN1_validn" : setIDEN1_validn((String)arg); break;
    		case "IDEN1_sosi" : setIDEN1_sosi((String)arg); break;
    		case "IDEN1_ps" : setIDEN1_ps((String)arg); break;
    		case "NAME1_length" : setNAME1_length((Integer)arg);break;
    		case "NAME1_color" : setNAME1_color((String)arg); break;
    		case "NAME1_hilight" : setNAME1_hilight((String)arg); break;
    		case "NAME1_outline" : setNAME1_outline((String)arg); break;
    		case "NAME1_transp" : setNAME1_transp((String)arg); break;
    		case "NAME1_validn" : setNAME1_validn((String)arg); break;
    		case "NAME1_sosi" : setNAME1_sosi((String)arg); break;
    		case "NAME1_ps" : setNAME1_ps((String)arg); break;
    		case "DEPT1_length" : setDEPT1_length((Integer)arg);break;
    		case "DEPT1_color" : setDEPT1_color((String)arg); break;
    		case "DEPT1_hilight" : setDEPT1_hilight((String)arg); break;
    		case "DEPT1_outline" : setDEPT1_outline((String)arg); break;
    		case "DEPT1_transp" : setDEPT1_transp((String)arg); break;
    		case "DEPT1_validn" : setDEPT1_validn((String)arg); break;
    		case "DEPT1_sosi" : setDEPT1_sosi((String)arg); break;
    		case "DEPT1_ps" : setDEPT1_ps((String)arg); break;
    		case "PHON1_length" : setPHON1_length((Integer)arg);break;
    		case "PHON1_color" : setPHON1_color((String)arg); break;
    		case "PHON1_hilight" : setPHON1_hilight((String)arg); break;
    		case "PHON1_outline" : setPHON1_outline((String)arg); break;
    		case "PHON1_transp" : setPHON1_transp((String)arg); break;
    		case "PHON1_validn" : setPHON1_validn((String)arg); break;
    		case "PHON1_sosi" : setPHON1_sosi((String)arg); break;
    		case "PHON1_ps" : setPHON1_ps((String)arg); break;
    		case "IDEN2_length" : setIDEN2_length((Integer)arg);break;
    		case "IDEN2_color" : setIDEN2_color((String)arg); break;
    		case "IDEN2_hilight" : setIDEN2_hilight((String)arg); break;
    		case "IDEN2_outline" : setIDEN2_outline((String)arg); break;
    		case "IDEN2_transp" : setIDEN2_transp((String)arg); break;
    		case "IDEN2_validn" : setIDEN2_validn((String)arg); break;
    		case "IDEN2_sosi" : setIDEN2_sosi((String)arg); break;
    		case "IDEN2_ps" : setIDEN2_ps((String)arg); break;
    		case "NAME2_length" : setNAME2_length((Integer)arg);break;
    		case "NAME2_color" : setNAME2_color((String)arg); break;
    		case "NAME2_hilight" : setNAME2_hilight((String)arg); break;
    		case "NAME2_outline" : setNAME2_outline((String)arg); break;
    		case "NAME2_transp" : setNAME2_transp((String)arg); break;
    		case "NAME2_validn" : setNAME2_validn((String)arg); break;
    		case "NAME2_sosi" : setNAME2_sosi((String)arg); break;
    		case "NAME2_ps" : setNAME2_ps((String)arg); break;
    		case "DEPT2_length" : setDEPT2_length((Integer)arg);break;
    		case "DEPT2_color" : setDEPT2_color((String)arg); break;
    		case "DEPT2_hilight" : setDEPT2_hilight((String)arg); break;
    		case "DEPT2_outline" : setDEPT2_outline((String)arg); break;
    		case "DEPT2_transp" : setDEPT2_transp((String)arg); break;
    		case "DEPT2_validn" : setDEPT2_validn((String)arg); break;
    		case "DEPT2_sosi" : setDEPT2_sosi((String)arg); break;
    		case "DEPT2_ps" : setDEPT2_ps((String)arg); break;
    		case "PHON2_length" : setPHON2_length((Integer)arg);break;
    		case "PHON2_color" : setPHON2_color((String)arg); break;
    		case "PHON2_hilight" : setPHON2_hilight((String)arg); break;
    		case "PHON2_outline" : setPHON2_outline((String)arg); break;
    		case "PHON2_transp" : setPHON2_transp((String)arg); break;
    		case "PHON2_validn" : setPHON2_validn((String)arg); break;
    		case "PHON2_sosi" : setPHON2_sosi((String)arg); break;
    		case "PHON2_ps" : setPHON2_ps((String)arg); break;
    		case "IDEN3_length" : setIDEN3_length((Integer)arg);break;
    		case "IDEN3_color" : setIDEN3_color((String)arg); break;
    		case "IDEN3_hilight" : setIDEN3_hilight((String)arg); break;
    		case "IDEN3_outline" : setIDEN3_outline((String)arg); break;
    		case "IDEN3_transp" : setIDEN3_transp((String)arg); break;
    		case "IDEN3_validn" : setIDEN3_validn((String)arg); break;
    		case "IDEN3_sosi" : setIDEN3_sosi((String)arg); break;
    		case "IDEN3_ps" : setIDEN3_ps((String)arg); break;
    		case "NAME3_length" : setNAME3_length((Integer)arg);break;
    		case "NAME3_color" : setNAME3_color((String)arg); break;
    		case "NAME3_hilight" : setNAME3_hilight((String)arg); break;
    		case "NAME3_outline" : setNAME3_outline((String)arg); break;
    		case "NAME3_transp" : setNAME3_transp((String)arg); break;
    		case "NAME3_validn" : setNAME3_validn((String)arg); break;
    		case "NAME3_sosi" : setNAME3_sosi((String)arg); break;
    		case "NAME3_ps" : setNAME3_ps((String)arg); break;
    		case "DEPT3_length" : setDEPT3_length((Integer)arg);break;
    		case "DEPT3_color" : setDEPT3_color((String)arg); break;
    		case "DEPT3_hilight" : setDEPT3_hilight((String)arg); break;
    		case "DEPT3_outline" : setDEPT3_outline((String)arg); break;
    		case "DEPT3_transp" : setDEPT3_transp((String)arg); break;
    		case "DEPT3_validn" : setDEPT3_validn((String)arg); break;
    		case "DEPT3_sosi" : setDEPT3_sosi((String)arg); break;
    		case "DEPT3_ps" : setDEPT3_ps((String)arg); break;
    		case "PHON3_length" : setPHON3_length((Integer)arg);break;
    		case "PHON3_color" : setPHON3_color((String)arg); break;
    		case "PHON3_hilight" : setPHON3_hilight((String)arg); break;
    		case "PHON3_outline" : setPHON3_outline((String)arg); break;
    		case "PHON3_transp" : setPHON3_transp((String)arg); break;
    		case "PHON3_validn" : setPHON3_validn((String)arg); break;
    		case "PHON3_sosi" : setPHON3_sosi((String)arg); break;
    		case "PHON3_ps" : setPHON3_ps((String)arg); break;
    		case "IDEN4_length" : setIDEN4_length((Integer)arg);break;
    		case "IDEN4_color" : setIDEN4_color((String)arg); break;
    		case "IDEN4_hilight" : setIDEN4_hilight((String)arg); break;
    		case "IDEN4_outline" : setIDEN4_outline((String)arg); break;
    		case "IDEN4_transp" : setIDEN4_transp((String)arg); break;
    		case "IDEN4_validn" : setIDEN4_validn((String)arg); break;
    		case "IDEN4_sosi" : setIDEN4_sosi((String)arg); break;
    		case "IDEN4_ps" : setIDEN4_ps((String)arg); break;
    		case "NAME4_length" : setNAME4_length((Integer)arg);break;
    		case "NAME4_color" : setNAME4_color((String)arg); break;
    		case "NAME4_hilight" : setNAME4_hilight((String)arg); break;
    		case "NAME4_outline" : setNAME4_outline((String)arg); break;
    		case "NAME4_transp" : setNAME4_transp((String)arg); break;
    		case "NAME4_validn" : setNAME4_validn((String)arg); break;
    		case "NAME4_sosi" : setNAME4_sosi((String)arg); break;
    		case "NAME4_ps" : setNAME4_ps((String)arg); break;
    		case "DEPT4_length" : setDEPT4_length((Integer)arg);break;
    		case "DEPT4_color" : setDEPT4_color((String)arg); break;
    		case "DEPT4_hilight" : setDEPT4_hilight((String)arg); break;
    		case "DEPT4_outline" : setDEPT4_outline((String)arg); break;
    		case "DEPT4_transp" : setDEPT4_transp((String)arg); break;
    		case "DEPT4_validn" : setDEPT4_validn((String)arg); break;
    		case "DEPT4_sosi" : setDEPT4_sosi((String)arg); break;
    		case "DEPT4_ps" : setDEPT4_ps((String)arg); break;
    		case "PHON4_length" : setPHON4_length((Integer)arg);break;
    		case "PHON4_color" : setPHON4_color((String)arg); break;
    		case "PHON4_hilight" : setPHON4_hilight((String)arg); break;
    		case "PHON4_outline" : setPHON4_outline((String)arg); break;
    		case "PHON4_transp" : setPHON4_transp((String)arg); break;
    		case "PHON4_validn" : setPHON4_validn((String)arg); break;
    		case "PHON4_sosi" : setPHON4_sosi((String)arg); break;
    		case "PHON4_ps" : setPHON4_ps((String)arg); break;
    		case "IDEN5_length" : setIDEN5_length((Integer)arg);break;
    		case "IDEN5_color" : setIDEN5_color((String)arg); break;
    		case "IDEN5_hilight" : setIDEN5_hilight((String)arg); break;
    		case "IDEN5_outline" : setIDEN5_outline((String)arg); break;
    		case "IDEN5_transp" : setIDEN5_transp((String)arg); break;
    		case "IDEN5_validn" : setIDEN5_validn((String)arg); break;
    		case "IDEN5_sosi" : setIDEN5_sosi((String)arg); break;
    		case "IDEN5_ps" : setIDEN5_ps((String)arg); break;
    		case "NAME5_length" : setNAME5_length((Integer)arg);break;
    		case "NAME5_color" : setNAME5_color((String)arg); break;
    		case "NAME5_hilight" : setNAME5_hilight((String)arg); break;
    		case "NAME5_outline" : setNAME5_outline((String)arg); break;
    		case "NAME5_transp" : setNAME5_transp((String)arg); break;
    		case "NAME5_validn" : setNAME5_validn((String)arg); break;
    		case "NAME5_sosi" : setNAME5_sosi((String)arg); break;
    		case "NAME5_ps" : setNAME5_ps((String)arg); break;
    		case "DEPT5_length" : setDEPT5_length((Integer)arg);break;
    		case "DEPT5_color" : setDEPT5_color((String)arg); break;
    		case "DEPT5_hilight" : setDEPT5_hilight((String)arg); break;
    		case "DEPT5_outline" : setDEPT5_outline((String)arg); break;
    		case "DEPT5_transp" : setDEPT5_transp((String)arg); break;
    		case "DEPT5_validn" : setDEPT5_validn((String)arg); break;
    		case "DEPT5_sosi" : setDEPT5_sosi((String)arg); break;
    		case "DEPT5_ps" : setDEPT5_ps((String)arg); break;
    		case "PHON5_length" : setPHON5_length((Integer)arg);break;
    		case "PHON5_color" : setPHON5_color((String)arg); break;
    		case "PHON5_hilight" : setPHON5_hilight((String)arg); break;
    		case "PHON5_outline" : setPHON5_outline((String)arg); break;
    		case "PHON5_transp" : setPHON5_transp((String)arg); break;
    		case "PHON5_validn" : setPHON5_validn((String)arg); break;
    		case "PHON5_sosi" : setPHON5_sosi((String)arg); break;
    		case "PHON5_ps" : setPHON5_ps((String)arg); break;
    		case "BIDX_length" : setBIDX_length((Integer)arg);break;
    		case "BIDX_color" : setBIDX_color((String)arg); break;
    		case "BIDX_hilight" : setBIDX_hilight((String)arg); break;
    		case "BIDX_outline" : setBIDX_outline((String)arg); break;
    		case "BIDX_transp" : setBIDX_transp((String)arg); break;
    		case "BIDX_validn" : setBIDX_validn((String)arg); break;
    		case "BIDX_sosi" : setBIDX_sosi((String)arg); break;
    		case "BIDX_ps" : setBIDX_ps((String)arg); break;
    		case "FIDX_length" : setFIDX_length((Integer)arg);break;
    		case "FIDX_color" : setFIDX_color((String)arg); break;
    		case "FIDX_hilight" : setFIDX_hilight((String)arg); break;
    		case "FIDX_outline" : setFIDX_outline((String)arg); break;
    		case "FIDX_transp" : setFIDX_transp((String)arg); break;
    		case "FIDX_validn" : setFIDX_validn((String)arg); break;
    		case "FIDX_sosi" : setFIDX_sosi((String)arg); break;
    		case "FIDX_ps" : setFIDX_ps((String)arg); break;
    		case "MSG_length" : setMSG_length((Integer)arg);break;
    		case "MSG_color" : setMSG_color((String)arg); break;
    		case "MSG_hilight" : setMSG_hilight((String)arg); break;
    		case "MSG_outline" : setMSG_outline((String)arg); break;
    		case "MSG_transp" : setMSG_transp((String)arg); break;
    		case "MSG_validn" : setMSG_validn((String)arg); break;
    		case "MSG_sosi" : setMSG_sosi((String)arg); break;
    		case "MSG_ps" : setMSG_ps((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM02_OIVPM02_meta _OIVPM02_OIVPM02_meta = (OIVPM02_OIVPM02_meta) obj;
    	if(this.DATE_length != _OIVPM02_OIVPM02_meta.getDATE_length()) return false;
    	if(this.DATE_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_color() != null)
    		return false;
    	} else if(!this.DATE_color.equals(_OIVPM02_OIVPM02_meta.getDATE_color()))
    		return false;
    	if(this.DATE_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_hilight() != null)
    		return false;
    	} else if(!this.DATE_hilight.equals(_OIVPM02_OIVPM02_meta.getDATE_hilight()))
    		return false;
    	if(this.DATE_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_outline() != null)
    		return false;
    	} else if(!this.DATE_outline.equals(_OIVPM02_OIVPM02_meta.getDATE_outline()))
    		return false;
    	if(this.DATE_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_transp() != null)
    		return false;
    	} else if(!this.DATE_transp.equals(_OIVPM02_OIVPM02_meta.getDATE_transp()))
    		return false;
    	if(this.DATE_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_validn() != null)
    		return false;
    	} else if(!this.DATE_validn.equals(_OIVPM02_OIVPM02_meta.getDATE_validn()))
    		return false;
    	if(this.DATE_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_sosi() != null)
    		return false;
    	} else if(!this.DATE_sosi.equals(_OIVPM02_OIVPM02_meta.getDATE_sosi()))
    		return false;
    	if(this.DATE_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getDATE_ps() != null)
    		return false;
    	} else if(!this.DATE_ps.equals(_OIVPM02_OIVPM02_meta.getDATE_ps()))
    		return false;
    	if(this.TERM_length != _OIVPM02_OIVPM02_meta.getTERM_length()) return false;
    	if(this.TERM_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_color() != null)
    		return false;
    	} else if(!this.TERM_color.equals(_OIVPM02_OIVPM02_meta.getTERM_color()))
    		return false;
    	if(this.TERM_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_hilight() != null)
    		return false;
    	} else if(!this.TERM_hilight.equals(_OIVPM02_OIVPM02_meta.getTERM_hilight()))
    		return false;
    	if(this.TERM_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_outline() != null)
    		return false;
    	} else if(!this.TERM_outline.equals(_OIVPM02_OIVPM02_meta.getTERM_outline()))
    		return false;
    	if(this.TERM_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_transp() != null)
    		return false;
    	} else if(!this.TERM_transp.equals(_OIVPM02_OIVPM02_meta.getTERM_transp()))
    		return false;
    	if(this.TERM_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_validn() != null)
    		return false;
    	} else if(!this.TERM_validn.equals(_OIVPM02_OIVPM02_meta.getTERM_validn()))
    		return false;
    	if(this.TERM_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_sosi() != null)
    		return false;
    	} else if(!this.TERM_sosi.equals(_OIVPM02_OIVPM02_meta.getTERM_sosi()))
    		return false;
    	if(this.TERM_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getTERM_ps() != null)
    		return false;
    	} else if(!this.TERM_ps.equals(_OIVPM02_OIVPM02_meta.getTERM_ps()))
    		return false;
    	if(this.TIME_length != _OIVPM02_OIVPM02_meta.getTIME_length()) return false;
    	if(this.TIME_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_color() != null)
    		return false;
    	} else if(!this.TIME_color.equals(_OIVPM02_OIVPM02_meta.getTIME_color()))
    		return false;
    	if(this.TIME_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_hilight() != null)
    		return false;
    	} else if(!this.TIME_hilight.equals(_OIVPM02_OIVPM02_meta.getTIME_hilight()))
    		return false;
    	if(this.TIME_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_outline() != null)
    		return false;
    	} else if(!this.TIME_outline.equals(_OIVPM02_OIVPM02_meta.getTIME_outline()))
    		return false;
    	if(this.TIME_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_transp() != null)
    		return false;
    	} else if(!this.TIME_transp.equals(_OIVPM02_OIVPM02_meta.getTIME_transp()))
    		return false;
    	if(this.TIME_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_validn() != null)
    		return false;
    	} else if(!this.TIME_validn.equals(_OIVPM02_OIVPM02_meta.getTIME_validn()))
    		return false;
    	if(this.TIME_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_sosi() != null)
    		return false;
    	} else if(!this.TIME_sosi.equals(_OIVPM02_OIVPM02_meta.getTIME_sosi()))
    		return false;
    	if(this.TIME_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getTIME_ps() != null)
    		return false;
    	} else if(!this.TIME_ps.equals(_OIVPM02_OIVPM02_meta.getTIME_ps()))
    		return false;
    	if(this.IDEN1_length != _OIVPM02_OIVPM02_meta.getIDEN1_length()) return false;
    	if(this.IDEN1_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_color() != null)
    		return false;
    	} else if(!this.IDEN1_color.equals(_OIVPM02_OIVPM02_meta.getIDEN1_color()))
    		return false;
    	if(this.IDEN1_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_hilight() != null)
    		return false;
    	} else if(!this.IDEN1_hilight.equals(_OIVPM02_OIVPM02_meta.getIDEN1_hilight()))
    		return false;
    	if(this.IDEN1_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_outline() != null)
    		return false;
    	} else if(!this.IDEN1_outline.equals(_OIVPM02_OIVPM02_meta.getIDEN1_outline()))
    		return false;
    	if(this.IDEN1_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_transp() != null)
    		return false;
    	} else if(!this.IDEN1_transp.equals(_OIVPM02_OIVPM02_meta.getIDEN1_transp()))
    		return false;
    	if(this.IDEN1_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_validn() != null)
    		return false;
    	} else if(!this.IDEN1_validn.equals(_OIVPM02_OIVPM02_meta.getIDEN1_validn()))
    		return false;
    	if(this.IDEN1_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_sosi() != null)
    		return false;
    	} else if(!this.IDEN1_sosi.equals(_OIVPM02_OIVPM02_meta.getIDEN1_sosi()))
    		return false;
    	if(this.IDEN1_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN1_ps() != null)
    		return false;
    	} else if(!this.IDEN1_ps.equals(_OIVPM02_OIVPM02_meta.getIDEN1_ps()))
    		return false;
    	if(this.NAME1_length != _OIVPM02_OIVPM02_meta.getNAME1_length()) return false;
    	if(this.NAME1_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_color() != null)
    		return false;
    	} else if(!this.NAME1_color.equals(_OIVPM02_OIVPM02_meta.getNAME1_color()))
    		return false;
    	if(this.NAME1_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_hilight() != null)
    		return false;
    	} else if(!this.NAME1_hilight.equals(_OIVPM02_OIVPM02_meta.getNAME1_hilight()))
    		return false;
    	if(this.NAME1_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_outline() != null)
    		return false;
    	} else if(!this.NAME1_outline.equals(_OIVPM02_OIVPM02_meta.getNAME1_outline()))
    		return false;
    	if(this.NAME1_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_transp() != null)
    		return false;
    	} else if(!this.NAME1_transp.equals(_OIVPM02_OIVPM02_meta.getNAME1_transp()))
    		return false;
    	if(this.NAME1_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_validn() != null)
    		return false;
    	} else if(!this.NAME1_validn.equals(_OIVPM02_OIVPM02_meta.getNAME1_validn()))
    		return false;
    	if(this.NAME1_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_sosi() != null)
    		return false;
    	} else if(!this.NAME1_sosi.equals(_OIVPM02_OIVPM02_meta.getNAME1_sosi()))
    		return false;
    	if(this.NAME1_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME1_ps() != null)
    		return false;
    	} else if(!this.NAME1_ps.equals(_OIVPM02_OIVPM02_meta.getNAME1_ps()))
    		return false;
    	if(this.DEPT1_length != _OIVPM02_OIVPM02_meta.getDEPT1_length()) return false;
    	if(this.DEPT1_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_color() != null)
    		return false;
    	} else if(!this.DEPT1_color.equals(_OIVPM02_OIVPM02_meta.getDEPT1_color()))
    		return false;
    	if(this.DEPT1_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_hilight() != null)
    		return false;
    	} else if(!this.DEPT1_hilight.equals(_OIVPM02_OIVPM02_meta.getDEPT1_hilight()))
    		return false;
    	if(this.DEPT1_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_outline() != null)
    		return false;
    	} else if(!this.DEPT1_outline.equals(_OIVPM02_OIVPM02_meta.getDEPT1_outline()))
    		return false;
    	if(this.DEPT1_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_transp() != null)
    		return false;
    	} else if(!this.DEPT1_transp.equals(_OIVPM02_OIVPM02_meta.getDEPT1_transp()))
    		return false;
    	if(this.DEPT1_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_validn() != null)
    		return false;
    	} else if(!this.DEPT1_validn.equals(_OIVPM02_OIVPM02_meta.getDEPT1_validn()))
    		return false;
    	if(this.DEPT1_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_sosi() != null)
    		return false;
    	} else if(!this.DEPT1_sosi.equals(_OIVPM02_OIVPM02_meta.getDEPT1_sosi()))
    		return false;
    	if(this.DEPT1_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT1_ps() != null)
    		return false;
    	} else if(!this.DEPT1_ps.equals(_OIVPM02_OIVPM02_meta.getDEPT1_ps()))
    		return false;
    	if(this.PHON1_length != _OIVPM02_OIVPM02_meta.getPHON1_length()) return false;
    	if(this.PHON1_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_color() != null)
    		return false;
    	} else if(!this.PHON1_color.equals(_OIVPM02_OIVPM02_meta.getPHON1_color()))
    		return false;
    	if(this.PHON1_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_hilight() != null)
    		return false;
    	} else if(!this.PHON1_hilight.equals(_OIVPM02_OIVPM02_meta.getPHON1_hilight()))
    		return false;
    	if(this.PHON1_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_outline() != null)
    		return false;
    	} else if(!this.PHON1_outline.equals(_OIVPM02_OIVPM02_meta.getPHON1_outline()))
    		return false;
    	if(this.PHON1_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_transp() != null)
    		return false;
    	} else if(!this.PHON1_transp.equals(_OIVPM02_OIVPM02_meta.getPHON1_transp()))
    		return false;
    	if(this.PHON1_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_validn() != null)
    		return false;
    	} else if(!this.PHON1_validn.equals(_OIVPM02_OIVPM02_meta.getPHON1_validn()))
    		return false;
    	if(this.PHON1_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_sosi() != null)
    		return false;
    	} else if(!this.PHON1_sosi.equals(_OIVPM02_OIVPM02_meta.getPHON1_sosi()))
    		return false;
    	if(this.PHON1_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON1_ps() != null)
    		return false;
    	} else if(!this.PHON1_ps.equals(_OIVPM02_OIVPM02_meta.getPHON1_ps()))
    		return false;
    	if(this.IDEN2_length != _OIVPM02_OIVPM02_meta.getIDEN2_length()) return false;
    	if(this.IDEN2_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_color() != null)
    		return false;
    	} else if(!this.IDEN2_color.equals(_OIVPM02_OIVPM02_meta.getIDEN2_color()))
    		return false;
    	if(this.IDEN2_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_hilight() != null)
    		return false;
    	} else if(!this.IDEN2_hilight.equals(_OIVPM02_OIVPM02_meta.getIDEN2_hilight()))
    		return false;
    	if(this.IDEN2_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_outline() != null)
    		return false;
    	} else if(!this.IDEN2_outline.equals(_OIVPM02_OIVPM02_meta.getIDEN2_outline()))
    		return false;
    	if(this.IDEN2_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_transp() != null)
    		return false;
    	} else if(!this.IDEN2_transp.equals(_OIVPM02_OIVPM02_meta.getIDEN2_transp()))
    		return false;
    	if(this.IDEN2_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_validn() != null)
    		return false;
    	} else if(!this.IDEN2_validn.equals(_OIVPM02_OIVPM02_meta.getIDEN2_validn()))
    		return false;
    	if(this.IDEN2_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_sosi() != null)
    		return false;
    	} else if(!this.IDEN2_sosi.equals(_OIVPM02_OIVPM02_meta.getIDEN2_sosi()))
    		return false;
    	if(this.IDEN2_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN2_ps() != null)
    		return false;
    	} else if(!this.IDEN2_ps.equals(_OIVPM02_OIVPM02_meta.getIDEN2_ps()))
    		return false;
    	if(this.NAME2_length != _OIVPM02_OIVPM02_meta.getNAME2_length()) return false;
    	if(this.NAME2_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_color() != null)
    		return false;
    	} else if(!this.NAME2_color.equals(_OIVPM02_OIVPM02_meta.getNAME2_color()))
    		return false;
    	if(this.NAME2_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_hilight() != null)
    		return false;
    	} else if(!this.NAME2_hilight.equals(_OIVPM02_OIVPM02_meta.getNAME2_hilight()))
    		return false;
    	if(this.NAME2_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_outline() != null)
    		return false;
    	} else if(!this.NAME2_outline.equals(_OIVPM02_OIVPM02_meta.getNAME2_outline()))
    		return false;
    	if(this.NAME2_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_transp() != null)
    		return false;
    	} else if(!this.NAME2_transp.equals(_OIVPM02_OIVPM02_meta.getNAME2_transp()))
    		return false;
    	if(this.NAME2_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_validn() != null)
    		return false;
    	} else if(!this.NAME2_validn.equals(_OIVPM02_OIVPM02_meta.getNAME2_validn()))
    		return false;
    	if(this.NAME2_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_sosi() != null)
    		return false;
    	} else if(!this.NAME2_sosi.equals(_OIVPM02_OIVPM02_meta.getNAME2_sosi()))
    		return false;
    	if(this.NAME2_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME2_ps() != null)
    		return false;
    	} else if(!this.NAME2_ps.equals(_OIVPM02_OIVPM02_meta.getNAME2_ps()))
    		return false;
    	if(this.DEPT2_length != _OIVPM02_OIVPM02_meta.getDEPT2_length()) return false;
    	if(this.DEPT2_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_color() != null)
    		return false;
    	} else if(!this.DEPT2_color.equals(_OIVPM02_OIVPM02_meta.getDEPT2_color()))
    		return false;
    	if(this.DEPT2_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_hilight() != null)
    		return false;
    	} else if(!this.DEPT2_hilight.equals(_OIVPM02_OIVPM02_meta.getDEPT2_hilight()))
    		return false;
    	if(this.DEPT2_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_outline() != null)
    		return false;
    	} else if(!this.DEPT2_outline.equals(_OIVPM02_OIVPM02_meta.getDEPT2_outline()))
    		return false;
    	if(this.DEPT2_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_transp() != null)
    		return false;
    	} else if(!this.DEPT2_transp.equals(_OIVPM02_OIVPM02_meta.getDEPT2_transp()))
    		return false;
    	if(this.DEPT2_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_validn() != null)
    		return false;
    	} else if(!this.DEPT2_validn.equals(_OIVPM02_OIVPM02_meta.getDEPT2_validn()))
    		return false;
    	if(this.DEPT2_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_sosi() != null)
    		return false;
    	} else if(!this.DEPT2_sosi.equals(_OIVPM02_OIVPM02_meta.getDEPT2_sosi()))
    		return false;
    	if(this.DEPT2_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT2_ps() != null)
    		return false;
    	} else if(!this.DEPT2_ps.equals(_OIVPM02_OIVPM02_meta.getDEPT2_ps()))
    		return false;
    	if(this.PHON2_length != _OIVPM02_OIVPM02_meta.getPHON2_length()) return false;
    	if(this.PHON2_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_color() != null)
    		return false;
    	} else if(!this.PHON2_color.equals(_OIVPM02_OIVPM02_meta.getPHON2_color()))
    		return false;
    	if(this.PHON2_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_hilight() != null)
    		return false;
    	} else if(!this.PHON2_hilight.equals(_OIVPM02_OIVPM02_meta.getPHON2_hilight()))
    		return false;
    	if(this.PHON2_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_outline() != null)
    		return false;
    	} else if(!this.PHON2_outline.equals(_OIVPM02_OIVPM02_meta.getPHON2_outline()))
    		return false;
    	if(this.PHON2_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_transp() != null)
    		return false;
    	} else if(!this.PHON2_transp.equals(_OIVPM02_OIVPM02_meta.getPHON2_transp()))
    		return false;
    	if(this.PHON2_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_validn() != null)
    		return false;
    	} else if(!this.PHON2_validn.equals(_OIVPM02_OIVPM02_meta.getPHON2_validn()))
    		return false;
    	if(this.PHON2_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_sosi() != null)
    		return false;
    	} else if(!this.PHON2_sosi.equals(_OIVPM02_OIVPM02_meta.getPHON2_sosi()))
    		return false;
    	if(this.PHON2_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON2_ps() != null)
    		return false;
    	} else if(!this.PHON2_ps.equals(_OIVPM02_OIVPM02_meta.getPHON2_ps()))
    		return false;
    	if(this.IDEN3_length != _OIVPM02_OIVPM02_meta.getIDEN3_length()) return false;
    	if(this.IDEN3_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_color() != null)
    		return false;
    	} else if(!this.IDEN3_color.equals(_OIVPM02_OIVPM02_meta.getIDEN3_color()))
    		return false;
    	if(this.IDEN3_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_hilight() != null)
    		return false;
    	} else if(!this.IDEN3_hilight.equals(_OIVPM02_OIVPM02_meta.getIDEN3_hilight()))
    		return false;
    	if(this.IDEN3_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_outline() != null)
    		return false;
    	} else if(!this.IDEN3_outline.equals(_OIVPM02_OIVPM02_meta.getIDEN3_outline()))
    		return false;
    	if(this.IDEN3_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_transp() != null)
    		return false;
    	} else if(!this.IDEN3_transp.equals(_OIVPM02_OIVPM02_meta.getIDEN3_transp()))
    		return false;
    	if(this.IDEN3_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_validn() != null)
    		return false;
    	} else if(!this.IDEN3_validn.equals(_OIVPM02_OIVPM02_meta.getIDEN3_validn()))
    		return false;
    	if(this.IDEN3_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_sosi() != null)
    		return false;
    	} else if(!this.IDEN3_sosi.equals(_OIVPM02_OIVPM02_meta.getIDEN3_sosi()))
    		return false;
    	if(this.IDEN3_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN3_ps() != null)
    		return false;
    	} else if(!this.IDEN3_ps.equals(_OIVPM02_OIVPM02_meta.getIDEN3_ps()))
    		return false;
    	if(this.NAME3_length != _OIVPM02_OIVPM02_meta.getNAME3_length()) return false;
    	if(this.NAME3_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_color() != null)
    		return false;
    	} else if(!this.NAME3_color.equals(_OIVPM02_OIVPM02_meta.getNAME3_color()))
    		return false;
    	if(this.NAME3_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_hilight() != null)
    		return false;
    	} else if(!this.NAME3_hilight.equals(_OIVPM02_OIVPM02_meta.getNAME3_hilight()))
    		return false;
    	if(this.NAME3_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_outline() != null)
    		return false;
    	} else if(!this.NAME3_outline.equals(_OIVPM02_OIVPM02_meta.getNAME3_outline()))
    		return false;
    	if(this.NAME3_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_transp() != null)
    		return false;
    	} else if(!this.NAME3_transp.equals(_OIVPM02_OIVPM02_meta.getNAME3_transp()))
    		return false;
    	if(this.NAME3_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_validn() != null)
    		return false;
    	} else if(!this.NAME3_validn.equals(_OIVPM02_OIVPM02_meta.getNAME3_validn()))
    		return false;
    	if(this.NAME3_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_sosi() != null)
    		return false;
    	} else if(!this.NAME3_sosi.equals(_OIVPM02_OIVPM02_meta.getNAME3_sosi()))
    		return false;
    	if(this.NAME3_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME3_ps() != null)
    		return false;
    	} else if(!this.NAME3_ps.equals(_OIVPM02_OIVPM02_meta.getNAME3_ps()))
    		return false;
    	if(this.DEPT3_length != _OIVPM02_OIVPM02_meta.getDEPT3_length()) return false;
    	if(this.DEPT3_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_color() != null)
    		return false;
    	} else if(!this.DEPT3_color.equals(_OIVPM02_OIVPM02_meta.getDEPT3_color()))
    		return false;
    	if(this.DEPT3_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_hilight() != null)
    		return false;
    	} else if(!this.DEPT3_hilight.equals(_OIVPM02_OIVPM02_meta.getDEPT3_hilight()))
    		return false;
    	if(this.DEPT3_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_outline() != null)
    		return false;
    	} else if(!this.DEPT3_outline.equals(_OIVPM02_OIVPM02_meta.getDEPT3_outline()))
    		return false;
    	if(this.DEPT3_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_transp() != null)
    		return false;
    	} else if(!this.DEPT3_transp.equals(_OIVPM02_OIVPM02_meta.getDEPT3_transp()))
    		return false;
    	if(this.DEPT3_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_validn() != null)
    		return false;
    	} else if(!this.DEPT3_validn.equals(_OIVPM02_OIVPM02_meta.getDEPT3_validn()))
    		return false;
    	if(this.DEPT3_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_sosi() != null)
    		return false;
    	} else if(!this.DEPT3_sosi.equals(_OIVPM02_OIVPM02_meta.getDEPT3_sosi()))
    		return false;
    	if(this.DEPT3_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT3_ps() != null)
    		return false;
    	} else if(!this.DEPT3_ps.equals(_OIVPM02_OIVPM02_meta.getDEPT3_ps()))
    		return false;
    	if(this.PHON3_length != _OIVPM02_OIVPM02_meta.getPHON3_length()) return false;
    	if(this.PHON3_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_color() != null)
    		return false;
    	} else if(!this.PHON3_color.equals(_OIVPM02_OIVPM02_meta.getPHON3_color()))
    		return false;
    	if(this.PHON3_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_hilight() != null)
    		return false;
    	} else if(!this.PHON3_hilight.equals(_OIVPM02_OIVPM02_meta.getPHON3_hilight()))
    		return false;
    	if(this.PHON3_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_outline() != null)
    		return false;
    	} else if(!this.PHON3_outline.equals(_OIVPM02_OIVPM02_meta.getPHON3_outline()))
    		return false;
    	if(this.PHON3_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_transp() != null)
    		return false;
    	} else if(!this.PHON3_transp.equals(_OIVPM02_OIVPM02_meta.getPHON3_transp()))
    		return false;
    	if(this.PHON3_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_validn() != null)
    		return false;
    	} else if(!this.PHON3_validn.equals(_OIVPM02_OIVPM02_meta.getPHON3_validn()))
    		return false;
    	if(this.PHON3_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_sosi() != null)
    		return false;
    	} else if(!this.PHON3_sosi.equals(_OIVPM02_OIVPM02_meta.getPHON3_sosi()))
    		return false;
    	if(this.PHON3_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON3_ps() != null)
    		return false;
    	} else if(!this.PHON3_ps.equals(_OIVPM02_OIVPM02_meta.getPHON3_ps()))
    		return false;
    	if(this.IDEN4_length != _OIVPM02_OIVPM02_meta.getIDEN4_length()) return false;
    	if(this.IDEN4_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_color() != null)
    		return false;
    	} else if(!this.IDEN4_color.equals(_OIVPM02_OIVPM02_meta.getIDEN4_color()))
    		return false;
    	if(this.IDEN4_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_hilight() != null)
    		return false;
    	} else if(!this.IDEN4_hilight.equals(_OIVPM02_OIVPM02_meta.getIDEN4_hilight()))
    		return false;
    	if(this.IDEN4_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_outline() != null)
    		return false;
    	} else if(!this.IDEN4_outline.equals(_OIVPM02_OIVPM02_meta.getIDEN4_outline()))
    		return false;
    	if(this.IDEN4_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_transp() != null)
    		return false;
    	} else if(!this.IDEN4_transp.equals(_OIVPM02_OIVPM02_meta.getIDEN4_transp()))
    		return false;
    	if(this.IDEN4_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_validn() != null)
    		return false;
    	} else if(!this.IDEN4_validn.equals(_OIVPM02_OIVPM02_meta.getIDEN4_validn()))
    		return false;
    	if(this.IDEN4_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_sosi() != null)
    		return false;
    	} else if(!this.IDEN4_sosi.equals(_OIVPM02_OIVPM02_meta.getIDEN4_sosi()))
    		return false;
    	if(this.IDEN4_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN4_ps() != null)
    		return false;
    	} else if(!this.IDEN4_ps.equals(_OIVPM02_OIVPM02_meta.getIDEN4_ps()))
    		return false;
    	if(this.NAME4_length != _OIVPM02_OIVPM02_meta.getNAME4_length()) return false;
    	if(this.NAME4_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_color() != null)
    		return false;
    	} else if(!this.NAME4_color.equals(_OIVPM02_OIVPM02_meta.getNAME4_color()))
    		return false;
    	if(this.NAME4_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_hilight() != null)
    		return false;
    	} else if(!this.NAME4_hilight.equals(_OIVPM02_OIVPM02_meta.getNAME4_hilight()))
    		return false;
    	if(this.NAME4_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_outline() != null)
    		return false;
    	} else if(!this.NAME4_outline.equals(_OIVPM02_OIVPM02_meta.getNAME4_outline()))
    		return false;
    	if(this.NAME4_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_transp() != null)
    		return false;
    	} else if(!this.NAME4_transp.equals(_OIVPM02_OIVPM02_meta.getNAME4_transp()))
    		return false;
    	if(this.NAME4_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_validn() != null)
    		return false;
    	} else if(!this.NAME4_validn.equals(_OIVPM02_OIVPM02_meta.getNAME4_validn()))
    		return false;
    	if(this.NAME4_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_sosi() != null)
    		return false;
    	} else if(!this.NAME4_sosi.equals(_OIVPM02_OIVPM02_meta.getNAME4_sosi()))
    		return false;
    	if(this.NAME4_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME4_ps() != null)
    		return false;
    	} else if(!this.NAME4_ps.equals(_OIVPM02_OIVPM02_meta.getNAME4_ps()))
    		return false;
    	if(this.DEPT4_length != _OIVPM02_OIVPM02_meta.getDEPT4_length()) return false;
    	if(this.DEPT4_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_color() != null)
    		return false;
    	} else if(!this.DEPT4_color.equals(_OIVPM02_OIVPM02_meta.getDEPT4_color()))
    		return false;
    	if(this.DEPT4_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_hilight() != null)
    		return false;
    	} else if(!this.DEPT4_hilight.equals(_OIVPM02_OIVPM02_meta.getDEPT4_hilight()))
    		return false;
    	if(this.DEPT4_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_outline() != null)
    		return false;
    	} else if(!this.DEPT4_outline.equals(_OIVPM02_OIVPM02_meta.getDEPT4_outline()))
    		return false;
    	if(this.DEPT4_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_transp() != null)
    		return false;
    	} else if(!this.DEPT4_transp.equals(_OIVPM02_OIVPM02_meta.getDEPT4_transp()))
    		return false;
    	if(this.DEPT4_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_validn() != null)
    		return false;
    	} else if(!this.DEPT4_validn.equals(_OIVPM02_OIVPM02_meta.getDEPT4_validn()))
    		return false;
    	if(this.DEPT4_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_sosi() != null)
    		return false;
    	} else if(!this.DEPT4_sosi.equals(_OIVPM02_OIVPM02_meta.getDEPT4_sosi()))
    		return false;
    	if(this.DEPT4_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT4_ps() != null)
    		return false;
    	} else if(!this.DEPT4_ps.equals(_OIVPM02_OIVPM02_meta.getDEPT4_ps()))
    		return false;
    	if(this.PHON4_length != _OIVPM02_OIVPM02_meta.getPHON4_length()) return false;
    	if(this.PHON4_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_color() != null)
    		return false;
    	} else if(!this.PHON4_color.equals(_OIVPM02_OIVPM02_meta.getPHON4_color()))
    		return false;
    	if(this.PHON4_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_hilight() != null)
    		return false;
    	} else if(!this.PHON4_hilight.equals(_OIVPM02_OIVPM02_meta.getPHON4_hilight()))
    		return false;
    	if(this.PHON4_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_outline() != null)
    		return false;
    	} else if(!this.PHON4_outline.equals(_OIVPM02_OIVPM02_meta.getPHON4_outline()))
    		return false;
    	if(this.PHON4_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_transp() != null)
    		return false;
    	} else if(!this.PHON4_transp.equals(_OIVPM02_OIVPM02_meta.getPHON4_transp()))
    		return false;
    	if(this.PHON4_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_validn() != null)
    		return false;
    	} else if(!this.PHON4_validn.equals(_OIVPM02_OIVPM02_meta.getPHON4_validn()))
    		return false;
    	if(this.PHON4_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_sosi() != null)
    		return false;
    	} else if(!this.PHON4_sosi.equals(_OIVPM02_OIVPM02_meta.getPHON4_sosi()))
    		return false;
    	if(this.PHON4_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON4_ps() != null)
    		return false;
    	} else if(!this.PHON4_ps.equals(_OIVPM02_OIVPM02_meta.getPHON4_ps()))
    		return false;
    	if(this.IDEN5_length != _OIVPM02_OIVPM02_meta.getIDEN5_length()) return false;
    	if(this.IDEN5_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_color() != null)
    		return false;
    	} else if(!this.IDEN5_color.equals(_OIVPM02_OIVPM02_meta.getIDEN5_color()))
    		return false;
    	if(this.IDEN5_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_hilight() != null)
    		return false;
    	} else if(!this.IDEN5_hilight.equals(_OIVPM02_OIVPM02_meta.getIDEN5_hilight()))
    		return false;
    	if(this.IDEN5_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_outline() != null)
    		return false;
    	} else if(!this.IDEN5_outline.equals(_OIVPM02_OIVPM02_meta.getIDEN5_outline()))
    		return false;
    	if(this.IDEN5_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_transp() != null)
    		return false;
    	} else if(!this.IDEN5_transp.equals(_OIVPM02_OIVPM02_meta.getIDEN5_transp()))
    		return false;
    	if(this.IDEN5_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_validn() != null)
    		return false;
    	} else if(!this.IDEN5_validn.equals(_OIVPM02_OIVPM02_meta.getIDEN5_validn()))
    		return false;
    	if(this.IDEN5_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_sosi() != null)
    		return false;
    	} else if(!this.IDEN5_sosi.equals(_OIVPM02_OIVPM02_meta.getIDEN5_sosi()))
    		return false;
    	if(this.IDEN5_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getIDEN5_ps() != null)
    		return false;
    	} else if(!this.IDEN5_ps.equals(_OIVPM02_OIVPM02_meta.getIDEN5_ps()))
    		return false;
    	if(this.NAME5_length != _OIVPM02_OIVPM02_meta.getNAME5_length()) return false;
    	if(this.NAME5_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_color() != null)
    		return false;
    	} else if(!this.NAME5_color.equals(_OIVPM02_OIVPM02_meta.getNAME5_color()))
    		return false;
    	if(this.NAME5_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_hilight() != null)
    		return false;
    	} else if(!this.NAME5_hilight.equals(_OIVPM02_OIVPM02_meta.getNAME5_hilight()))
    		return false;
    	if(this.NAME5_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_outline() != null)
    		return false;
    	} else if(!this.NAME5_outline.equals(_OIVPM02_OIVPM02_meta.getNAME5_outline()))
    		return false;
    	if(this.NAME5_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_transp() != null)
    		return false;
    	} else if(!this.NAME5_transp.equals(_OIVPM02_OIVPM02_meta.getNAME5_transp()))
    		return false;
    	if(this.NAME5_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_validn() != null)
    		return false;
    	} else if(!this.NAME5_validn.equals(_OIVPM02_OIVPM02_meta.getNAME5_validn()))
    		return false;
    	if(this.NAME5_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_sosi() != null)
    		return false;
    	} else if(!this.NAME5_sosi.equals(_OIVPM02_OIVPM02_meta.getNAME5_sosi()))
    		return false;
    	if(this.NAME5_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getNAME5_ps() != null)
    		return false;
    	} else if(!this.NAME5_ps.equals(_OIVPM02_OIVPM02_meta.getNAME5_ps()))
    		return false;
    	if(this.DEPT5_length != _OIVPM02_OIVPM02_meta.getDEPT5_length()) return false;
    	if(this.DEPT5_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_color() != null)
    		return false;
    	} else if(!this.DEPT5_color.equals(_OIVPM02_OIVPM02_meta.getDEPT5_color()))
    		return false;
    	if(this.DEPT5_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_hilight() != null)
    		return false;
    	} else if(!this.DEPT5_hilight.equals(_OIVPM02_OIVPM02_meta.getDEPT5_hilight()))
    		return false;
    	if(this.DEPT5_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_outline() != null)
    		return false;
    	} else if(!this.DEPT5_outline.equals(_OIVPM02_OIVPM02_meta.getDEPT5_outline()))
    		return false;
    	if(this.DEPT5_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_transp() != null)
    		return false;
    	} else if(!this.DEPT5_transp.equals(_OIVPM02_OIVPM02_meta.getDEPT5_transp()))
    		return false;
    	if(this.DEPT5_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_validn() != null)
    		return false;
    	} else if(!this.DEPT5_validn.equals(_OIVPM02_OIVPM02_meta.getDEPT5_validn()))
    		return false;
    	if(this.DEPT5_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_sosi() != null)
    		return false;
    	} else if(!this.DEPT5_sosi.equals(_OIVPM02_OIVPM02_meta.getDEPT5_sosi()))
    		return false;
    	if(this.DEPT5_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getDEPT5_ps() != null)
    		return false;
    	} else if(!this.DEPT5_ps.equals(_OIVPM02_OIVPM02_meta.getDEPT5_ps()))
    		return false;
    	if(this.PHON5_length != _OIVPM02_OIVPM02_meta.getPHON5_length()) return false;
    	if(this.PHON5_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_color() != null)
    		return false;
    	} else if(!this.PHON5_color.equals(_OIVPM02_OIVPM02_meta.getPHON5_color()))
    		return false;
    	if(this.PHON5_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_hilight() != null)
    		return false;
    	} else if(!this.PHON5_hilight.equals(_OIVPM02_OIVPM02_meta.getPHON5_hilight()))
    		return false;
    	if(this.PHON5_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_outline() != null)
    		return false;
    	} else if(!this.PHON5_outline.equals(_OIVPM02_OIVPM02_meta.getPHON5_outline()))
    		return false;
    	if(this.PHON5_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_transp() != null)
    		return false;
    	} else if(!this.PHON5_transp.equals(_OIVPM02_OIVPM02_meta.getPHON5_transp()))
    		return false;
    	if(this.PHON5_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_validn() != null)
    		return false;
    	} else if(!this.PHON5_validn.equals(_OIVPM02_OIVPM02_meta.getPHON5_validn()))
    		return false;
    	if(this.PHON5_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_sosi() != null)
    		return false;
    	} else if(!this.PHON5_sosi.equals(_OIVPM02_OIVPM02_meta.getPHON5_sosi()))
    		return false;
    	if(this.PHON5_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getPHON5_ps() != null)
    		return false;
    	} else if(!this.PHON5_ps.equals(_OIVPM02_OIVPM02_meta.getPHON5_ps()))
    		return false;
    	if(this.BIDX_length != _OIVPM02_OIVPM02_meta.getBIDX_length()) return false;
    	if(this.BIDX_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_color() != null)
    		return false;
    	} else if(!this.BIDX_color.equals(_OIVPM02_OIVPM02_meta.getBIDX_color()))
    		return false;
    	if(this.BIDX_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_hilight() != null)
    		return false;
    	} else if(!this.BIDX_hilight.equals(_OIVPM02_OIVPM02_meta.getBIDX_hilight()))
    		return false;
    	if(this.BIDX_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_outline() != null)
    		return false;
    	} else if(!this.BIDX_outline.equals(_OIVPM02_OIVPM02_meta.getBIDX_outline()))
    		return false;
    	if(this.BIDX_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_transp() != null)
    		return false;
    	} else if(!this.BIDX_transp.equals(_OIVPM02_OIVPM02_meta.getBIDX_transp()))
    		return false;
    	if(this.BIDX_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_validn() != null)
    		return false;
    	} else if(!this.BIDX_validn.equals(_OIVPM02_OIVPM02_meta.getBIDX_validn()))
    		return false;
    	if(this.BIDX_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_sosi() != null)
    		return false;
    	} else if(!this.BIDX_sosi.equals(_OIVPM02_OIVPM02_meta.getBIDX_sosi()))
    		return false;
    	if(this.BIDX_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getBIDX_ps() != null)
    		return false;
    	} else if(!this.BIDX_ps.equals(_OIVPM02_OIVPM02_meta.getBIDX_ps()))
    		return false;
    	if(this.FIDX_length != _OIVPM02_OIVPM02_meta.getFIDX_length()) return false;
    	if(this.FIDX_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_color() != null)
    		return false;
    	} else if(!this.FIDX_color.equals(_OIVPM02_OIVPM02_meta.getFIDX_color()))
    		return false;
    	if(this.FIDX_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_hilight() != null)
    		return false;
    	} else if(!this.FIDX_hilight.equals(_OIVPM02_OIVPM02_meta.getFIDX_hilight()))
    		return false;
    	if(this.FIDX_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_outline() != null)
    		return false;
    	} else if(!this.FIDX_outline.equals(_OIVPM02_OIVPM02_meta.getFIDX_outline()))
    		return false;
    	if(this.FIDX_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_transp() != null)
    		return false;
    	} else if(!this.FIDX_transp.equals(_OIVPM02_OIVPM02_meta.getFIDX_transp()))
    		return false;
    	if(this.FIDX_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_validn() != null)
    		return false;
    	} else if(!this.FIDX_validn.equals(_OIVPM02_OIVPM02_meta.getFIDX_validn()))
    		return false;
    	if(this.FIDX_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_sosi() != null)
    		return false;
    	} else if(!this.FIDX_sosi.equals(_OIVPM02_OIVPM02_meta.getFIDX_sosi()))
    		return false;
    	if(this.FIDX_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getFIDX_ps() != null)
    		return false;
    	} else if(!this.FIDX_ps.equals(_OIVPM02_OIVPM02_meta.getFIDX_ps()))
    		return false;
    	if(this.MSG_length != _OIVPM02_OIVPM02_meta.getMSG_length()) return false;
    	if(this.MSG_color == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_color() != null)
    		return false;
    	} else if(!this.MSG_color.equals(_OIVPM02_OIVPM02_meta.getMSG_color()))
    		return false;
    	if(this.MSG_hilight == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_hilight() != null)
    		return false;
    	} else if(!this.MSG_hilight.equals(_OIVPM02_OIVPM02_meta.getMSG_hilight()))
    		return false;
    	if(this.MSG_outline == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_outline() != null)
    		return false;
    	} else if(!this.MSG_outline.equals(_OIVPM02_OIVPM02_meta.getMSG_outline()))
    		return false;
    	if(this.MSG_transp == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_transp() != null)
    		return false;
    	} else if(!this.MSG_transp.equals(_OIVPM02_OIVPM02_meta.getMSG_transp()))
    		return false;
    	if(this.MSG_validn == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_validn() != null)
    		return false;
    	} else if(!this.MSG_validn.equals(_OIVPM02_OIVPM02_meta.getMSG_validn()))
    		return false;
    	if(this.MSG_sosi == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_sosi() != null)
    		return false;
    	} else if(!this.MSG_sosi.equals(_OIVPM02_OIVPM02_meta.getMSG_sosi()))
    		return false;
    	if(this.MSG_ps == null) {
    	if(_OIVPM02_OIVPM02_meta.getMSG_ps() != null)
    		return false;
    	} else if(!this.MSG_ps.equals(_OIVPM02_OIVPM02_meta.getMSG_ps()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + this.DATE_length;
    	result = prime * result + ((this.DATE_color == null) ? 0 : this.DATE_color.hashCode());
    	result = prime * result + ((this.DATE_hilight == null) ? 0 : this.DATE_hilight.hashCode());
    	result = prime * result + ((this.DATE_outline == null) ? 0 : this.DATE_outline.hashCode());
    	result = prime * result + ((this.DATE_transp == null) ? 0 : this.DATE_transp.hashCode());
    	result = prime * result + ((this.DATE_validn == null) ? 0 : this.DATE_validn.hashCode());
    	result = prime * result + ((this.DATE_sosi == null) ? 0 : this.DATE_sosi.hashCode());
    	result = prime * result + ((this.DATE_ps == null) ? 0 : this.DATE_ps.hashCode());
    	result = prime * result + this.TERM_length;
    	result = prime * result + ((this.TERM_color == null) ? 0 : this.TERM_color.hashCode());
    	result = prime * result + ((this.TERM_hilight == null) ? 0 : this.TERM_hilight.hashCode());
    	result = prime * result + ((this.TERM_outline == null) ? 0 : this.TERM_outline.hashCode());
    	result = prime * result + ((this.TERM_transp == null) ? 0 : this.TERM_transp.hashCode());
    	result = prime * result + ((this.TERM_validn == null) ? 0 : this.TERM_validn.hashCode());
    	result = prime * result + ((this.TERM_sosi == null) ? 0 : this.TERM_sosi.hashCode());
    	result = prime * result + ((this.TERM_ps == null) ? 0 : this.TERM_ps.hashCode());
    	result = prime * result + this.TIME_length;
    	result = prime * result + ((this.TIME_color == null) ? 0 : this.TIME_color.hashCode());
    	result = prime * result + ((this.TIME_hilight == null) ? 0 : this.TIME_hilight.hashCode());
    	result = prime * result + ((this.TIME_outline == null) ? 0 : this.TIME_outline.hashCode());
    	result = prime * result + ((this.TIME_transp == null) ? 0 : this.TIME_transp.hashCode());
    	result = prime * result + ((this.TIME_validn == null) ? 0 : this.TIME_validn.hashCode());
    	result = prime * result + ((this.TIME_sosi == null) ? 0 : this.TIME_sosi.hashCode());
    	result = prime * result + ((this.TIME_ps == null) ? 0 : this.TIME_ps.hashCode());
    	result = prime * result + this.IDEN1_length;
    	result = prime * result + ((this.IDEN1_color == null) ? 0 : this.IDEN1_color.hashCode());
    	result = prime * result + ((this.IDEN1_hilight == null) ? 0 : this.IDEN1_hilight.hashCode());
    	result = prime * result + ((this.IDEN1_outline == null) ? 0 : this.IDEN1_outline.hashCode());
    	result = prime * result + ((this.IDEN1_transp == null) ? 0 : this.IDEN1_transp.hashCode());
    	result = prime * result + ((this.IDEN1_validn == null) ? 0 : this.IDEN1_validn.hashCode());
    	result = prime * result + ((this.IDEN1_sosi == null) ? 0 : this.IDEN1_sosi.hashCode());
    	result = prime * result + ((this.IDEN1_ps == null) ? 0 : this.IDEN1_ps.hashCode());
    	result = prime * result + this.NAME1_length;
    	result = prime * result + ((this.NAME1_color == null) ? 0 : this.NAME1_color.hashCode());
    	result = prime * result + ((this.NAME1_hilight == null) ? 0 : this.NAME1_hilight.hashCode());
    	result = prime * result + ((this.NAME1_outline == null) ? 0 : this.NAME1_outline.hashCode());
    	result = prime * result + ((this.NAME1_transp == null) ? 0 : this.NAME1_transp.hashCode());
    	result = prime * result + ((this.NAME1_validn == null) ? 0 : this.NAME1_validn.hashCode());
    	result = prime * result + ((this.NAME1_sosi == null) ? 0 : this.NAME1_sosi.hashCode());
    	result = prime * result + ((this.NAME1_ps == null) ? 0 : this.NAME1_ps.hashCode());
    	result = prime * result + this.DEPT1_length;
    	result = prime * result + ((this.DEPT1_color == null) ? 0 : this.DEPT1_color.hashCode());
    	result = prime * result + ((this.DEPT1_hilight == null) ? 0 : this.DEPT1_hilight.hashCode());
    	result = prime * result + ((this.DEPT1_outline == null) ? 0 : this.DEPT1_outline.hashCode());
    	result = prime * result + ((this.DEPT1_transp == null) ? 0 : this.DEPT1_transp.hashCode());
    	result = prime * result + ((this.DEPT1_validn == null) ? 0 : this.DEPT1_validn.hashCode());
    	result = prime * result + ((this.DEPT1_sosi == null) ? 0 : this.DEPT1_sosi.hashCode());
    	result = prime * result + ((this.DEPT1_ps == null) ? 0 : this.DEPT1_ps.hashCode());
    	result = prime * result + this.PHON1_length;
    	result = prime * result + ((this.PHON1_color == null) ? 0 : this.PHON1_color.hashCode());
    	result = prime * result + ((this.PHON1_hilight == null) ? 0 : this.PHON1_hilight.hashCode());
    	result = prime * result + ((this.PHON1_outline == null) ? 0 : this.PHON1_outline.hashCode());
    	result = prime * result + ((this.PHON1_transp == null) ? 0 : this.PHON1_transp.hashCode());
    	result = prime * result + ((this.PHON1_validn == null) ? 0 : this.PHON1_validn.hashCode());
    	result = prime * result + ((this.PHON1_sosi == null) ? 0 : this.PHON1_sosi.hashCode());
    	result = prime * result + ((this.PHON1_ps == null) ? 0 : this.PHON1_ps.hashCode());
    	result = prime * result + this.IDEN2_length;
    	result = prime * result + ((this.IDEN2_color == null) ? 0 : this.IDEN2_color.hashCode());
    	result = prime * result + ((this.IDEN2_hilight == null) ? 0 : this.IDEN2_hilight.hashCode());
    	result = prime * result + ((this.IDEN2_outline == null) ? 0 : this.IDEN2_outline.hashCode());
    	result = prime * result + ((this.IDEN2_transp == null) ? 0 : this.IDEN2_transp.hashCode());
    	result = prime * result + ((this.IDEN2_validn == null) ? 0 : this.IDEN2_validn.hashCode());
    	result = prime * result + ((this.IDEN2_sosi == null) ? 0 : this.IDEN2_sosi.hashCode());
    	result = prime * result + ((this.IDEN2_ps == null) ? 0 : this.IDEN2_ps.hashCode());
    	result = prime * result + this.NAME2_length;
    	result = prime * result + ((this.NAME2_color == null) ? 0 : this.NAME2_color.hashCode());
    	result = prime * result + ((this.NAME2_hilight == null) ? 0 : this.NAME2_hilight.hashCode());
    	result = prime * result + ((this.NAME2_outline == null) ? 0 : this.NAME2_outline.hashCode());
    	result = prime * result + ((this.NAME2_transp == null) ? 0 : this.NAME2_transp.hashCode());
    	result = prime * result + ((this.NAME2_validn == null) ? 0 : this.NAME2_validn.hashCode());
    	result = prime * result + ((this.NAME2_sosi == null) ? 0 : this.NAME2_sosi.hashCode());
    	result = prime * result + ((this.NAME2_ps == null) ? 0 : this.NAME2_ps.hashCode());
    	result = prime * result + this.DEPT2_length;
    	result = prime * result + ((this.DEPT2_color == null) ? 0 : this.DEPT2_color.hashCode());
    	result = prime * result + ((this.DEPT2_hilight == null) ? 0 : this.DEPT2_hilight.hashCode());
    	result = prime * result + ((this.DEPT2_outline == null) ? 0 : this.DEPT2_outline.hashCode());
    	result = prime * result + ((this.DEPT2_transp == null) ? 0 : this.DEPT2_transp.hashCode());
    	result = prime * result + ((this.DEPT2_validn == null) ? 0 : this.DEPT2_validn.hashCode());
    	result = prime * result + ((this.DEPT2_sosi == null) ? 0 : this.DEPT2_sosi.hashCode());
    	result = prime * result + ((this.DEPT2_ps == null) ? 0 : this.DEPT2_ps.hashCode());
    	result = prime * result + this.PHON2_length;
    	result = prime * result + ((this.PHON2_color == null) ? 0 : this.PHON2_color.hashCode());
    	result = prime * result + ((this.PHON2_hilight == null) ? 0 : this.PHON2_hilight.hashCode());
    	result = prime * result + ((this.PHON2_outline == null) ? 0 : this.PHON2_outline.hashCode());
    	result = prime * result + ((this.PHON2_transp == null) ? 0 : this.PHON2_transp.hashCode());
    	result = prime * result + ((this.PHON2_validn == null) ? 0 : this.PHON2_validn.hashCode());
    	result = prime * result + ((this.PHON2_sosi == null) ? 0 : this.PHON2_sosi.hashCode());
    	result = prime * result + ((this.PHON2_ps == null) ? 0 : this.PHON2_ps.hashCode());
    	result = prime * result + this.IDEN3_length;
    	result = prime * result + ((this.IDEN3_color == null) ? 0 : this.IDEN3_color.hashCode());
    	result = prime * result + ((this.IDEN3_hilight == null) ? 0 : this.IDEN3_hilight.hashCode());
    	result = prime * result + ((this.IDEN3_outline == null) ? 0 : this.IDEN3_outline.hashCode());
    	result = prime * result + ((this.IDEN3_transp == null) ? 0 : this.IDEN3_transp.hashCode());
    	result = prime * result + ((this.IDEN3_validn == null) ? 0 : this.IDEN3_validn.hashCode());
    	result = prime * result + ((this.IDEN3_sosi == null) ? 0 : this.IDEN3_sosi.hashCode());
    	result = prime * result + ((this.IDEN3_ps == null) ? 0 : this.IDEN3_ps.hashCode());
    	result = prime * result + this.NAME3_length;
    	result = prime * result + ((this.NAME3_color == null) ? 0 : this.NAME3_color.hashCode());
    	result = prime * result + ((this.NAME3_hilight == null) ? 0 : this.NAME3_hilight.hashCode());
    	result = prime * result + ((this.NAME3_outline == null) ? 0 : this.NAME3_outline.hashCode());
    	result = prime * result + ((this.NAME3_transp == null) ? 0 : this.NAME3_transp.hashCode());
    	result = prime * result + ((this.NAME3_validn == null) ? 0 : this.NAME3_validn.hashCode());
    	result = prime * result + ((this.NAME3_sosi == null) ? 0 : this.NAME3_sosi.hashCode());
    	result = prime * result + ((this.NAME3_ps == null) ? 0 : this.NAME3_ps.hashCode());
    	result = prime * result + this.DEPT3_length;
    	result = prime * result + ((this.DEPT3_color == null) ? 0 : this.DEPT3_color.hashCode());
    	result = prime * result + ((this.DEPT3_hilight == null) ? 0 : this.DEPT3_hilight.hashCode());
    	result = prime * result + ((this.DEPT3_outline == null) ? 0 : this.DEPT3_outline.hashCode());
    	result = prime * result + ((this.DEPT3_transp == null) ? 0 : this.DEPT3_transp.hashCode());
    	result = prime * result + ((this.DEPT3_validn == null) ? 0 : this.DEPT3_validn.hashCode());
    	result = prime * result + ((this.DEPT3_sosi == null) ? 0 : this.DEPT3_sosi.hashCode());
    	result = prime * result + ((this.DEPT3_ps == null) ? 0 : this.DEPT3_ps.hashCode());
    	result = prime * result + this.PHON3_length;
    	result = prime * result + ((this.PHON3_color == null) ? 0 : this.PHON3_color.hashCode());
    	result = prime * result + ((this.PHON3_hilight == null) ? 0 : this.PHON3_hilight.hashCode());
    	result = prime * result + ((this.PHON3_outline == null) ? 0 : this.PHON3_outline.hashCode());
    	result = prime * result + ((this.PHON3_transp == null) ? 0 : this.PHON3_transp.hashCode());
    	result = prime * result + ((this.PHON3_validn == null) ? 0 : this.PHON3_validn.hashCode());
    	result = prime * result + ((this.PHON3_sosi == null) ? 0 : this.PHON3_sosi.hashCode());
    	result = prime * result + ((this.PHON3_ps == null) ? 0 : this.PHON3_ps.hashCode());
    	result = prime * result + this.IDEN4_length;
    	result = prime * result + ((this.IDEN4_color == null) ? 0 : this.IDEN4_color.hashCode());
    	result = prime * result + ((this.IDEN4_hilight == null) ? 0 : this.IDEN4_hilight.hashCode());
    	result = prime * result + ((this.IDEN4_outline == null) ? 0 : this.IDEN4_outline.hashCode());
    	result = prime * result + ((this.IDEN4_transp == null) ? 0 : this.IDEN4_transp.hashCode());
    	result = prime * result + ((this.IDEN4_validn == null) ? 0 : this.IDEN4_validn.hashCode());
    	result = prime * result + ((this.IDEN4_sosi == null) ? 0 : this.IDEN4_sosi.hashCode());
    	result = prime * result + ((this.IDEN4_ps == null) ? 0 : this.IDEN4_ps.hashCode());
    	result = prime * result + this.NAME4_length;
    	result = prime * result + ((this.NAME4_color == null) ? 0 : this.NAME4_color.hashCode());
    	result = prime * result + ((this.NAME4_hilight == null) ? 0 : this.NAME4_hilight.hashCode());
    	result = prime * result + ((this.NAME4_outline == null) ? 0 : this.NAME4_outline.hashCode());
    	result = prime * result + ((this.NAME4_transp == null) ? 0 : this.NAME4_transp.hashCode());
    	result = prime * result + ((this.NAME4_validn == null) ? 0 : this.NAME4_validn.hashCode());
    	result = prime * result + ((this.NAME4_sosi == null) ? 0 : this.NAME4_sosi.hashCode());
    	result = prime * result + ((this.NAME4_ps == null) ? 0 : this.NAME4_ps.hashCode());
    	result = prime * result + this.DEPT4_length;
    	result = prime * result + ((this.DEPT4_color == null) ? 0 : this.DEPT4_color.hashCode());
    	result = prime * result + ((this.DEPT4_hilight == null) ? 0 : this.DEPT4_hilight.hashCode());
    	result = prime * result + ((this.DEPT4_outline == null) ? 0 : this.DEPT4_outline.hashCode());
    	result = prime * result + ((this.DEPT4_transp == null) ? 0 : this.DEPT4_transp.hashCode());
    	result = prime * result + ((this.DEPT4_validn == null) ? 0 : this.DEPT4_validn.hashCode());
    	result = prime * result + ((this.DEPT4_sosi == null) ? 0 : this.DEPT4_sosi.hashCode());
    	result = prime * result + ((this.DEPT4_ps == null) ? 0 : this.DEPT4_ps.hashCode());
    	result = prime * result + this.PHON4_length;
    	result = prime * result + ((this.PHON4_color == null) ? 0 : this.PHON4_color.hashCode());
    	result = prime * result + ((this.PHON4_hilight == null) ? 0 : this.PHON4_hilight.hashCode());
    	result = prime * result + ((this.PHON4_outline == null) ? 0 : this.PHON4_outline.hashCode());
    	result = prime * result + ((this.PHON4_transp == null) ? 0 : this.PHON4_transp.hashCode());
    	result = prime * result + ((this.PHON4_validn == null) ? 0 : this.PHON4_validn.hashCode());
    	result = prime * result + ((this.PHON4_sosi == null) ? 0 : this.PHON4_sosi.hashCode());
    	result = prime * result + ((this.PHON4_ps == null) ? 0 : this.PHON4_ps.hashCode());
    	result = prime * result + this.IDEN5_length;
    	result = prime * result + ((this.IDEN5_color == null) ? 0 : this.IDEN5_color.hashCode());
    	result = prime * result + ((this.IDEN5_hilight == null) ? 0 : this.IDEN5_hilight.hashCode());
    	result = prime * result + ((this.IDEN5_outline == null) ? 0 : this.IDEN5_outline.hashCode());
    	result = prime * result + ((this.IDEN5_transp == null) ? 0 : this.IDEN5_transp.hashCode());
    	result = prime * result + ((this.IDEN5_validn == null) ? 0 : this.IDEN5_validn.hashCode());
    	result = prime * result + ((this.IDEN5_sosi == null) ? 0 : this.IDEN5_sosi.hashCode());
    	result = prime * result + ((this.IDEN5_ps == null) ? 0 : this.IDEN5_ps.hashCode());
    	result = prime * result + this.NAME5_length;
    	result = prime * result + ((this.NAME5_color == null) ? 0 : this.NAME5_color.hashCode());
    	result = prime * result + ((this.NAME5_hilight == null) ? 0 : this.NAME5_hilight.hashCode());
    	result = prime * result + ((this.NAME5_outline == null) ? 0 : this.NAME5_outline.hashCode());
    	result = prime * result + ((this.NAME5_transp == null) ? 0 : this.NAME5_transp.hashCode());
    	result = prime * result + ((this.NAME5_validn == null) ? 0 : this.NAME5_validn.hashCode());
    	result = prime * result + ((this.NAME5_sosi == null) ? 0 : this.NAME5_sosi.hashCode());
    	result = prime * result + ((this.NAME5_ps == null) ? 0 : this.NAME5_ps.hashCode());
    	result = prime * result + this.DEPT5_length;
    	result = prime * result + ((this.DEPT5_color == null) ? 0 : this.DEPT5_color.hashCode());
    	result = prime * result + ((this.DEPT5_hilight == null) ? 0 : this.DEPT5_hilight.hashCode());
    	result = prime * result + ((this.DEPT5_outline == null) ? 0 : this.DEPT5_outline.hashCode());
    	result = prime * result + ((this.DEPT5_transp == null) ? 0 : this.DEPT5_transp.hashCode());
    	result = prime * result + ((this.DEPT5_validn == null) ? 0 : this.DEPT5_validn.hashCode());
    	result = prime * result + ((this.DEPT5_sosi == null) ? 0 : this.DEPT5_sosi.hashCode());
    	result = prime * result + ((this.DEPT5_ps == null) ? 0 : this.DEPT5_ps.hashCode());
    	result = prime * result + this.PHON5_length;
    	result = prime * result + ((this.PHON5_color == null) ? 0 : this.PHON5_color.hashCode());
    	result = prime * result + ((this.PHON5_hilight == null) ? 0 : this.PHON5_hilight.hashCode());
    	result = prime * result + ((this.PHON5_outline == null) ? 0 : this.PHON5_outline.hashCode());
    	result = prime * result + ((this.PHON5_transp == null) ? 0 : this.PHON5_transp.hashCode());
    	result = prime * result + ((this.PHON5_validn == null) ? 0 : this.PHON5_validn.hashCode());
    	result = prime * result + ((this.PHON5_sosi == null) ? 0 : this.PHON5_sosi.hashCode());
    	result = prime * result + ((this.PHON5_ps == null) ? 0 : this.PHON5_ps.hashCode());
    	result = prime * result + this.BIDX_length;
    	result = prime * result + ((this.BIDX_color == null) ? 0 : this.BIDX_color.hashCode());
    	result = prime * result + ((this.BIDX_hilight == null) ? 0 : this.BIDX_hilight.hashCode());
    	result = prime * result + ((this.BIDX_outline == null) ? 0 : this.BIDX_outline.hashCode());
    	result = prime * result + ((this.BIDX_transp == null) ? 0 : this.BIDX_transp.hashCode());
    	result = prime * result + ((this.BIDX_validn == null) ? 0 : this.BIDX_validn.hashCode());
    	result = prime * result + ((this.BIDX_sosi == null) ? 0 : this.BIDX_sosi.hashCode());
    	result = prime * result + ((this.BIDX_ps == null) ? 0 : this.BIDX_ps.hashCode());
    	result = prime * result + this.FIDX_length;
    	result = prime * result + ((this.FIDX_color == null) ? 0 : this.FIDX_color.hashCode());
    	result = prime * result + ((this.FIDX_hilight == null) ? 0 : this.FIDX_hilight.hashCode());
    	result = prime * result + ((this.FIDX_outline == null) ? 0 : this.FIDX_outline.hashCode());
    	result = prime * result + ((this.FIDX_transp == null) ? 0 : this.FIDX_transp.hashCode());
    	result = prime * result + ((this.FIDX_validn == null) ? 0 : this.FIDX_validn.hashCode());
    	result = prime * result + ((this.FIDX_sosi == null) ? 0 : this.FIDX_sosi.hashCode());
    	result = prime * result + ((this.FIDX_ps == null) ? 0 : this.FIDX_ps.hashCode());
    	result = prime * result + this.MSG_length;
    	result = prime * result + ((this.MSG_color == null) ? 0 : this.MSG_color.hashCode());
    	result = prime * result + ((this.MSG_hilight == null) ? 0 : this.MSG_hilight.hashCode());
    	result = prime * result + ((this.MSG_outline == null) ? 0 : this.MSG_outline.hashCode());
    	result = prime * result + ((this.MSG_transp == null) ? 0 : this.MSG_transp.hashCode());
    	result = prime * result + ((this.MSG_validn == null) ? 0 : this.MSG_validn.hashCode());
    	result = prime * result + ((this.MSG_sosi == null) ? 0 : this.MSG_sosi.hashCode());
    	result = prime * result + ((this.MSG_ps == null) ? 0 : this.MSG_ps.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE_length = 0;
    	DATE_color = null;
    	DATE_hilight = null;
    	DATE_outline = null;
    	DATE_transp = null;
    	DATE_validn = null;
    	DATE_sosi = null;
    	DATE_ps = null;
    	TERM_length = 0;
    	TERM_color = null;
    	TERM_hilight = null;
    	TERM_outline = null;
    	TERM_transp = null;
    	TERM_validn = null;
    	TERM_sosi = null;
    	TERM_ps = null;
    	TIME_length = 0;
    	TIME_color = null;
    	TIME_hilight = null;
    	TIME_outline = null;
    	TIME_transp = null;
    	TIME_validn = null;
    	TIME_sosi = null;
    	TIME_ps = null;
    	IDEN1_length = 0;
    	IDEN1_color = null;
    	IDEN1_hilight = null;
    	IDEN1_outline = null;
    	IDEN1_transp = null;
    	IDEN1_validn = null;
    	IDEN1_sosi = null;
    	IDEN1_ps = null;
    	NAME1_length = 0;
    	NAME1_color = null;
    	NAME1_hilight = null;
    	NAME1_outline = null;
    	NAME1_transp = null;
    	NAME1_validn = null;
    	NAME1_sosi = null;
    	NAME1_ps = null;
    	DEPT1_length = 0;
    	DEPT1_color = null;
    	DEPT1_hilight = null;
    	DEPT1_outline = null;
    	DEPT1_transp = null;
    	DEPT1_validn = null;
    	DEPT1_sosi = null;
    	DEPT1_ps = null;
    	PHON1_length = 0;
    	PHON1_color = null;
    	PHON1_hilight = null;
    	PHON1_outline = null;
    	PHON1_transp = null;
    	PHON1_validn = null;
    	PHON1_sosi = null;
    	PHON1_ps = null;
    	IDEN2_length = 0;
    	IDEN2_color = null;
    	IDEN2_hilight = null;
    	IDEN2_outline = null;
    	IDEN2_transp = null;
    	IDEN2_validn = null;
    	IDEN2_sosi = null;
    	IDEN2_ps = null;
    	NAME2_length = 0;
    	NAME2_color = null;
    	NAME2_hilight = null;
    	NAME2_outline = null;
    	NAME2_transp = null;
    	NAME2_validn = null;
    	NAME2_sosi = null;
    	NAME2_ps = null;
    	DEPT2_length = 0;
    	DEPT2_color = null;
    	DEPT2_hilight = null;
    	DEPT2_outline = null;
    	DEPT2_transp = null;
    	DEPT2_validn = null;
    	DEPT2_sosi = null;
    	DEPT2_ps = null;
    	PHON2_length = 0;
    	PHON2_color = null;
    	PHON2_hilight = null;
    	PHON2_outline = null;
    	PHON2_transp = null;
    	PHON2_validn = null;
    	PHON2_sosi = null;
    	PHON2_ps = null;
    	IDEN3_length = 0;
    	IDEN3_color = null;
    	IDEN3_hilight = null;
    	IDEN3_outline = null;
    	IDEN3_transp = null;
    	IDEN3_validn = null;
    	IDEN3_sosi = null;
    	IDEN3_ps = null;
    	NAME3_length = 0;
    	NAME3_color = null;
    	NAME3_hilight = null;
    	NAME3_outline = null;
    	NAME3_transp = null;
    	NAME3_validn = null;
    	NAME3_sosi = null;
    	NAME3_ps = null;
    	DEPT3_length = 0;
    	DEPT3_color = null;
    	DEPT3_hilight = null;
    	DEPT3_outline = null;
    	DEPT3_transp = null;
    	DEPT3_validn = null;
    	DEPT3_sosi = null;
    	DEPT3_ps = null;
    	PHON3_length = 0;
    	PHON3_color = null;
    	PHON3_hilight = null;
    	PHON3_outline = null;
    	PHON3_transp = null;
    	PHON3_validn = null;
    	PHON3_sosi = null;
    	PHON3_ps = null;
    	IDEN4_length = 0;
    	IDEN4_color = null;
    	IDEN4_hilight = null;
    	IDEN4_outline = null;
    	IDEN4_transp = null;
    	IDEN4_validn = null;
    	IDEN4_sosi = null;
    	IDEN4_ps = null;
    	NAME4_length = 0;
    	NAME4_color = null;
    	NAME4_hilight = null;
    	NAME4_outline = null;
    	NAME4_transp = null;
    	NAME4_validn = null;
    	NAME4_sosi = null;
    	NAME4_ps = null;
    	DEPT4_length = 0;
    	DEPT4_color = null;
    	DEPT4_hilight = null;
    	DEPT4_outline = null;
    	DEPT4_transp = null;
    	DEPT4_validn = null;
    	DEPT4_sosi = null;
    	DEPT4_ps = null;
    	PHON4_length = 0;
    	PHON4_color = null;
    	PHON4_hilight = null;
    	PHON4_outline = null;
    	PHON4_transp = null;
    	PHON4_validn = null;
    	PHON4_sosi = null;
    	PHON4_ps = null;
    	IDEN5_length = 0;
    	IDEN5_color = null;
    	IDEN5_hilight = null;
    	IDEN5_outline = null;
    	IDEN5_transp = null;
    	IDEN5_validn = null;
    	IDEN5_sosi = null;
    	IDEN5_ps = null;
    	NAME5_length = 0;
    	NAME5_color = null;
    	NAME5_hilight = null;
    	NAME5_outline = null;
    	NAME5_transp = null;
    	NAME5_validn = null;
    	NAME5_sosi = null;
    	NAME5_ps = null;
    	DEPT5_length = 0;
    	DEPT5_color = null;
    	DEPT5_hilight = null;
    	DEPT5_outline = null;
    	DEPT5_transp = null;
    	DEPT5_validn = null;
    	DEPT5_sosi = null;
    	DEPT5_ps = null;
    	PHON5_length = 0;
    	PHON5_color = null;
    	PHON5_hilight = null;
    	PHON5_outline = null;
    	PHON5_transp = null;
    	PHON5_validn = null;
    	PHON5_sosi = null;
    	PHON5_ps = null;
    	BIDX_length = 0;
    	BIDX_color = null;
    	BIDX_hilight = null;
    	BIDX_outline = null;
    	BIDX_transp = null;
    	BIDX_validn = null;
    	BIDX_sosi = null;
    	BIDX_ps = null;
    	FIDX_length = 0;
    	FIDX_color = null;
    	FIDX_hilight = null;
    	FIDX_outline = null;
    	FIDX_transp = null;
    	FIDX_validn = null;
    	FIDX_sosi = null;
    	FIDX_ps = null;
    	MSG_length = 0;
    	MSG_color = null;
    	MSG_hilight = null;
    	MSG_outline = null;
    	MSG_transp = null;
    	MSG_validn = null;
    	MSG_sosi = null;
    	MSG_ps = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

