package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPM01_OIVPM01_metaMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPM01_OIVPM01_meta _OIVPM01_OIVPM01_meta = (com.tmax.openframe.dto.OIVPM01_OIVPM01_meta)obj;
    	
    	if(_OIVPM01_OIVPM01_meta == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPM01_OIVPM01_meta, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPM01_OIVPM01_meta _OIVPM01_OIVPM01_meta, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	writer.name("DATE_length");
    	writer.value(_OIVPM01_OIVPM01_meta.getDATE_length());
    	writer.name("DATE_color"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_color() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_color());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_hilight"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_hilight() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_hilight());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_outline"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_outline() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_outline());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_transp"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_transp() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_transp());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_validn"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_validn() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_validn());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_sosi"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_sosi() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_sosi());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("DATE_ps"); 
    	if (_OIVPM01_OIVPM01_meta.getDATE_ps() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_ps());
    		writer.value(_OIVPM01_OIVPM01_meta.getDATE_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_length");
    	writer.value(_OIVPM01_OIVPM01_meta.getTERM_length());
    	writer.name("TERM_color"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_color() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_color());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_hilight"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_hilight() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_hilight());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_outline"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_outline() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_outline());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_transp"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_transp() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_transp());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_validn"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_validn() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_validn());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_sosi"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_sosi() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_sosi());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TERM_ps"); 
    	if (_OIVPM01_OIVPM01_meta.getTERM_ps() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_ps());
    		writer.value(_OIVPM01_OIVPM01_meta.getTERM_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_length");
    	writer.value(_OIVPM01_OIVPM01_meta.getTIME_length());
    	writer.name("TIME_color"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_color() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_color());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_hilight"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_hilight() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_hilight());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_outline"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_outline() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_outline());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_transp"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_transp() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_transp());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_validn"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_validn() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_validn());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_sosi"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_sosi() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_sosi());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("TIME_ps"); 
    	if (_OIVPM01_OIVPM01_meta.getTIME_ps() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_ps());
    		writer.value(_OIVPM01_OIVPM01_meta.getTIME_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_length");
    	writer.value(_OIVPM01_OIVPM01_meta.getCODE_length());
    	writer.name("CODE_color"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_color() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_color());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_hilight"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_hilight() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_hilight());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_outline"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_outline() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_outline());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_transp"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_transp() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_transp());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_validn"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_validn() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_validn());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_sosi"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_sosi() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_sosi());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("CODE_ps"); 
    	if (_OIVPM01_OIVPM01_meta.getCODE_ps() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_ps());
    		writer.value(_OIVPM01_OIVPM01_meta.getCODE_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_length");
    	writer.value(_OIVPM01_OIVPM01_meta.getIDEN_length());
    	writer.name("IDEN_color"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_color() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_color());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_hilight"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_hilight() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_hilight());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_outline"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_outline() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_outline());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_transp"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_transp() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_transp());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_validn"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_validn() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_validn());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_sosi"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_sosi() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_sosi());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("IDEN_ps"); 
    	if (_OIVPM01_OIVPM01_meta.getIDEN_ps() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_ps());
    		writer.value(_OIVPM01_OIVPM01_meta.getIDEN_ps());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_length");
    	writer.value(_OIVPM01_OIVPM01_meta.getMSG_length());
    	writer.name("MSG_color"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_color() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_color());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_color());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_hilight"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_hilight() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_hilight());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_hilight());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_outline"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_outline() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_outline());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_outline());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_transp"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_transp() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_transp());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_transp());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_validn"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_validn() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_validn());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_validn());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_sosi"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_sosi() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_sosi());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_sosi());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("MSG_ps"); 
    	if (_OIVPM01_OIVPM01_meta.getMSG_ps() != null) {
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_ps());
    		writer.value(_OIVPM01_OIVPM01_meta.getMSG_ps());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM01_OIVPM01_meta _OIVPM01_OIVPM01_meta = new com.tmax.openframe.dto.OIVPM01_OIVPM01_meta();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM01_OIVPM01_meta();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM01_OIVPM01_meta = (com.tmax.openframe.dto.OIVPM01_OIVPM01_meta)unmarshal( jr,  _OIVPM01_OIVPM01_meta);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPM01_OIVPM01_meta;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM01_OIVPM01_meta _OIVPM01_OIVPM01_meta = (com.tmax.openframe.dto.OIVPM01_OIVPM01_meta) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPM01_OIVPM01_meta();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPM01_OIVPM01_meta = (com.tmax.openframe.dto.OIVPM01_OIVPM01_meta)unmarshal( jr,  _OIVPM01_OIVPM01_meta);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPM01_OIVPM01_meta;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPM01_OIVPM01_meta dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPM01_OIVPM01_meta dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "DATE_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "DATE_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setDATE_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TERM_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTERM_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "TIME_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setTIME_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "CODE_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setCODE_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "IDEN_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setIDEN_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_length" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_length( reader.nextInt());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_color" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_color( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_hilight" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_hilight( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_outline" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_outline( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_transp" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_transp( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_validn" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_validn( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_sosi" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_sosi( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "MSG_ps" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMSG_ps( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

