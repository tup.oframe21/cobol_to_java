package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM07_OIVPM07_meta extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM07_OIVPM07_meta";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM07_OIVPM07_meta() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE_length
     * Comments    : 
     */	
    private int DATE_length = 0;
    
    private transient boolean DATE_length_nullable = false;
    
    public boolean isNullableDATE_length() {
    	return this.DATE_length_nullable;
    }
    
    private transient boolean DATE_length_invalidation = false;
    	
    public void setInvalidationDATE_length(boolean invalidation) { 
    	this.DATE_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_length() {
    	return this.DATE_length_invalidation;
    }
    	
    private transient boolean DATE_length_modified = false;
    
    public boolean isModifiedDATE_length() {
    	return this.DATE_length_modified;
    }
    	
    public void unModifiedDATE_length() {
    	this.DATE_length_modified = false;
    }
    public FieldProperty getDATE_lengthFieldProperty() {
    	return fieldPropertyMap.get("DATE_length");
    }
    
    public int getDATE_length() {
    	return DATE_length;
    }	
    public void setDATE_length(int DATE_length) {
    	this.DATE_length = DATE_length;
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(Integer DATE_length) {
    	if( DATE_length == null) {
    		this.DATE_length = 0;
    	} else{
    		this.DATE_length = DATE_length.intValue();
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(String DATE_length) {
    	if  (DATE_length==null || DATE_length.length() == 0) {
    		this.DATE_length = 0;
    	} else {
    		this.DATE_length = Integer.parseInt(DATE_length);
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_color
     * Comments    : 
     */	
    private String DATE_color = null;
    
    private transient boolean DATE_color_nullable = true;
    
    public boolean isNullableDATE_color() {
    	return this.DATE_color_nullable;
    }
    
    private transient boolean DATE_color_invalidation = false;
    	
    public void setInvalidationDATE_color(boolean invalidation) { 
    	this.DATE_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_color() {
    	return this.DATE_color_invalidation;
    }
    	
    private transient boolean DATE_color_modified = false;
    
    public boolean isModifiedDATE_color() {
    	return this.DATE_color_modified;
    }
    	
    public void unModifiedDATE_color() {
    	this.DATE_color_modified = false;
    }
    public FieldProperty getDATE_colorFieldProperty() {
    	return fieldPropertyMap.get("DATE_color");
    }
    
    public String getDATE_color() {
    	return DATE_color;
    }	
    public String getNvlDATE_color() {
    	if(getDATE_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_color();
    	}
    }
    public void setDATE_color(String DATE_color) {
    	if(DATE_color == null) {
    		this.DATE_color = null;
    	} else {
    		this.DATE_color = DATE_color;
    	}
    	this.DATE_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_hilight
     * Comments    : 
     */	
    private String DATE_hilight = null;
    
    private transient boolean DATE_hilight_nullable = true;
    
    public boolean isNullableDATE_hilight() {
    	return this.DATE_hilight_nullable;
    }
    
    private transient boolean DATE_hilight_invalidation = false;
    	
    public void setInvalidationDATE_hilight(boolean invalidation) { 
    	this.DATE_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_hilight() {
    	return this.DATE_hilight_invalidation;
    }
    	
    private transient boolean DATE_hilight_modified = false;
    
    public boolean isModifiedDATE_hilight() {
    	return this.DATE_hilight_modified;
    }
    	
    public void unModifiedDATE_hilight() {
    	this.DATE_hilight_modified = false;
    }
    public FieldProperty getDATE_hilightFieldProperty() {
    	return fieldPropertyMap.get("DATE_hilight");
    }
    
    public String getDATE_hilight() {
    	return DATE_hilight;
    }	
    public String getNvlDATE_hilight() {
    	if(getDATE_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_hilight();
    	}
    }
    public void setDATE_hilight(String DATE_hilight) {
    	if(DATE_hilight == null) {
    		this.DATE_hilight = null;
    	} else {
    		this.DATE_hilight = DATE_hilight;
    	}
    	this.DATE_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_outline
     * Comments    : 
     */	
    private String DATE_outline = null;
    
    private transient boolean DATE_outline_nullable = true;
    
    public boolean isNullableDATE_outline() {
    	return this.DATE_outline_nullable;
    }
    
    private transient boolean DATE_outline_invalidation = false;
    	
    public void setInvalidationDATE_outline(boolean invalidation) { 
    	this.DATE_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_outline() {
    	return this.DATE_outline_invalidation;
    }
    	
    private transient boolean DATE_outline_modified = false;
    
    public boolean isModifiedDATE_outline() {
    	return this.DATE_outline_modified;
    }
    	
    public void unModifiedDATE_outline() {
    	this.DATE_outline_modified = false;
    }
    public FieldProperty getDATE_outlineFieldProperty() {
    	return fieldPropertyMap.get("DATE_outline");
    }
    
    public String getDATE_outline() {
    	return DATE_outline;
    }	
    public String getNvlDATE_outline() {
    	if(getDATE_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_outline();
    	}
    }
    public void setDATE_outline(String DATE_outline) {
    	if(DATE_outline == null) {
    		this.DATE_outline = null;
    	} else {
    		this.DATE_outline = DATE_outline;
    	}
    	this.DATE_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_transp
     * Comments    : 
     */	
    private String DATE_transp = null;
    
    private transient boolean DATE_transp_nullable = true;
    
    public boolean isNullableDATE_transp() {
    	return this.DATE_transp_nullable;
    }
    
    private transient boolean DATE_transp_invalidation = false;
    	
    public void setInvalidationDATE_transp(boolean invalidation) { 
    	this.DATE_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_transp() {
    	return this.DATE_transp_invalidation;
    }
    	
    private transient boolean DATE_transp_modified = false;
    
    public boolean isModifiedDATE_transp() {
    	return this.DATE_transp_modified;
    }
    	
    public void unModifiedDATE_transp() {
    	this.DATE_transp_modified = false;
    }
    public FieldProperty getDATE_transpFieldProperty() {
    	return fieldPropertyMap.get("DATE_transp");
    }
    
    public String getDATE_transp() {
    	return DATE_transp;
    }	
    public String getNvlDATE_transp() {
    	if(getDATE_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_transp();
    	}
    }
    public void setDATE_transp(String DATE_transp) {
    	if(DATE_transp == null) {
    		this.DATE_transp = null;
    	} else {
    		this.DATE_transp = DATE_transp;
    	}
    	this.DATE_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_validn
     * Comments    : 
     */	
    private String DATE_validn = null;
    
    private transient boolean DATE_validn_nullable = true;
    
    public boolean isNullableDATE_validn() {
    	return this.DATE_validn_nullable;
    }
    
    private transient boolean DATE_validn_invalidation = false;
    	
    public void setInvalidationDATE_validn(boolean invalidation) { 
    	this.DATE_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_validn() {
    	return this.DATE_validn_invalidation;
    }
    	
    private transient boolean DATE_validn_modified = false;
    
    public boolean isModifiedDATE_validn() {
    	return this.DATE_validn_modified;
    }
    	
    public void unModifiedDATE_validn() {
    	this.DATE_validn_modified = false;
    }
    public FieldProperty getDATE_validnFieldProperty() {
    	return fieldPropertyMap.get("DATE_validn");
    }
    
    public String getDATE_validn() {
    	return DATE_validn;
    }	
    public String getNvlDATE_validn() {
    	if(getDATE_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_validn();
    	}
    }
    public void setDATE_validn(String DATE_validn) {
    	if(DATE_validn == null) {
    		this.DATE_validn = null;
    	} else {
    		this.DATE_validn = DATE_validn;
    	}
    	this.DATE_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_sosi
     * Comments    : 
     */	
    private String DATE_sosi = null;
    
    private transient boolean DATE_sosi_nullable = true;
    
    public boolean isNullableDATE_sosi() {
    	return this.DATE_sosi_nullable;
    }
    
    private transient boolean DATE_sosi_invalidation = false;
    	
    public void setInvalidationDATE_sosi(boolean invalidation) { 
    	this.DATE_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_sosi() {
    	return this.DATE_sosi_invalidation;
    }
    	
    private transient boolean DATE_sosi_modified = false;
    
    public boolean isModifiedDATE_sosi() {
    	return this.DATE_sosi_modified;
    }
    	
    public void unModifiedDATE_sosi() {
    	this.DATE_sosi_modified = false;
    }
    public FieldProperty getDATE_sosiFieldProperty() {
    	return fieldPropertyMap.get("DATE_sosi");
    }
    
    public String getDATE_sosi() {
    	return DATE_sosi;
    }	
    public String getNvlDATE_sosi() {
    	if(getDATE_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_sosi();
    	}
    }
    public void setDATE_sosi(String DATE_sosi) {
    	if(DATE_sosi == null) {
    		this.DATE_sosi = null;
    	} else {
    		this.DATE_sosi = DATE_sosi;
    	}
    	this.DATE_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_ps
     * Comments    : 
     */	
    private String DATE_ps = null;
    
    private transient boolean DATE_ps_nullable = true;
    
    public boolean isNullableDATE_ps() {
    	return this.DATE_ps_nullable;
    }
    
    private transient boolean DATE_ps_invalidation = false;
    	
    public void setInvalidationDATE_ps(boolean invalidation) { 
    	this.DATE_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_ps() {
    	return this.DATE_ps_invalidation;
    }
    	
    private transient boolean DATE_ps_modified = false;
    
    public boolean isModifiedDATE_ps() {
    	return this.DATE_ps_modified;
    }
    	
    public void unModifiedDATE_ps() {
    	this.DATE_ps_modified = false;
    }
    public FieldProperty getDATE_psFieldProperty() {
    	return fieldPropertyMap.get("DATE_ps");
    }
    
    public String getDATE_ps() {
    	return DATE_ps;
    }	
    public String getNvlDATE_ps() {
    	if(getDATE_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_ps();
    	}
    }
    public void setDATE_ps(String DATE_ps) {
    	if(DATE_ps == null) {
    		this.DATE_ps = null;
    	} else {
    		this.DATE_ps = DATE_ps;
    	}
    	this.DATE_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_length
     * Comments    : 
     */	
    private int TERM_length = 0;
    
    private transient boolean TERM_length_nullable = false;
    
    public boolean isNullableTERM_length() {
    	return this.TERM_length_nullable;
    }
    
    private transient boolean TERM_length_invalidation = false;
    	
    public void setInvalidationTERM_length(boolean invalidation) { 
    	this.TERM_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_length() {
    	return this.TERM_length_invalidation;
    }
    	
    private transient boolean TERM_length_modified = false;
    
    public boolean isModifiedTERM_length() {
    	return this.TERM_length_modified;
    }
    	
    public void unModifiedTERM_length() {
    	this.TERM_length_modified = false;
    }
    public FieldProperty getTERM_lengthFieldProperty() {
    	return fieldPropertyMap.get("TERM_length");
    }
    
    public int getTERM_length() {
    	return TERM_length;
    }	
    public void setTERM_length(int TERM_length) {
    	this.TERM_length = TERM_length;
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(Integer TERM_length) {
    	if( TERM_length == null) {
    		this.TERM_length = 0;
    	} else{
    		this.TERM_length = TERM_length.intValue();
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(String TERM_length) {
    	if  (TERM_length==null || TERM_length.length() == 0) {
    		this.TERM_length = 0;
    	} else {
    		this.TERM_length = Integer.parseInt(TERM_length);
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_color
     * Comments    : 
     */	
    private String TERM_color = null;
    
    private transient boolean TERM_color_nullable = true;
    
    public boolean isNullableTERM_color() {
    	return this.TERM_color_nullable;
    }
    
    private transient boolean TERM_color_invalidation = false;
    	
    public void setInvalidationTERM_color(boolean invalidation) { 
    	this.TERM_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_color() {
    	return this.TERM_color_invalidation;
    }
    	
    private transient boolean TERM_color_modified = false;
    
    public boolean isModifiedTERM_color() {
    	return this.TERM_color_modified;
    }
    	
    public void unModifiedTERM_color() {
    	this.TERM_color_modified = false;
    }
    public FieldProperty getTERM_colorFieldProperty() {
    	return fieldPropertyMap.get("TERM_color");
    }
    
    public String getTERM_color() {
    	return TERM_color;
    }	
    public String getNvlTERM_color() {
    	if(getTERM_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_color();
    	}
    }
    public void setTERM_color(String TERM_color) {
    	if(TERM_color == null) {
    		this.TERM_color = null;
    	} else {
    		this.TERM_color = TERM_color;
    	}
    	this.TERM_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_hilight
     * Comments    : 
     */	
    private String TERM_hilight = null;
    
    private transient boolean TERM_hilight_nullable = true;
    
    public boolean isNullableTERM_hilight() {
    	return this.TERM_hilight_nullable;
    }
    
    private transient boolean TERM_hilight_invalidation = false;
    	
    public void setInvalidationTERM_hilight(boolean invalidation) { 
    	this.TERM_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_hilight() {
    	return this.TERM_hilight_invalidation;
    }
    	
    private transient boolean TERM_hilight_modified = false;
    
    public boolean isModifiedTERM_hilight() {
    	return this.TERM_hilight_modified;
    }
    	
    public void unModifiedTERM_hilight() {
    	this.TERM_hilight_modified = false;
    }
    public FieldProperty getTERM_hilightFieldProperty() {
    	return fieldPropertyMap.get("TERM_hilight");
    }
    
    public String getTERM_hilight() {
    	return TERM_hilight;
    }	
    public String getNvlTERM_hilight() {
    	if(getTERM_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_hilight();
    	}
    }
    public void setTERM_hilight(String TERM_hilight) {
    	if(TERM_hilight == null) {
    		this.TERM_hilight = null;
    	} else {
    		this.TERM_hilight = TERM_hilight;
    	}
    	this.TERM_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_outline
     * Comments    : 
     */	
    private String TERM_outline = null;
    
    private transient boolean TERM_outline_nullable = true;
    
    public boolean isNullableTERM_outline() {
    	return this.TERM_outline_nullable;
    }
    
    private transient boolean TERM_outline_invalidation = false;
    	
    public void setInvalidationTERM_outline(boolean invalidation) { 
    	this.TERM_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_outline() {
    	return this.TERM_outline_invalidation;
    }
    	
    private transient boolean TERM_outline_modified = false;
    
    public boolean isModifiedTERM_outline() {
    	return this.TERM_outline_modified;
    }
    	
    public void unModifiedTERM_outline() {
    	this.TERM_outline_modified = false;
    }
    public FieldProperty getTERM_outlineFieldProperty() {
    	return fieldPropertyMap.get("TERM_outline");
    }
    
    public String getTERM_outline() {
    	return TERM_outline;
    }	
    public String getNvlTERM_outline() {
    	if(getTERM_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_outline();
    	}
    }
    public void setTERM_outline(String TERM_outline) {
    	if(TERM_outline == null) {
    		this.TERM_outline = null;
    	} else {
    		this.TERM_outline = TERM_outline;
    	}
    	this.TERM_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_transp
     * Comments    : 
     */	
    private String TERM_transp = null;
    
    private transient boolean TERM_transp_nullable = true;
    
    public boolean isNullableTERM_transp() {
    	return this.TERM_transp_nullable;
    }
    
    private transient boolean TERM_transp_invalidation = false;
    	
    public void setInvalidationTERM_transp(boolean invalidation) { 
    	this.TERM_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_transp() {
    	return this.TERM_transp_invalidation;
    }
    	
    private transient boolean TERM_transp_modified = false;
    
    public boolean isModifiedTERM_transp() {
    	return this.TERM_transp_modified;
    }
    	
    public void unModifiedTERM_transp() {
    	this.TERM_transp_modified = false;
    }
    public FieldProperty getTERM_transpFieldProperty() {
    	return fieldPropertyMap.get("TERM_transp");
    }
    
    public String getTERM_transp() {
    	return TERM_transp;
    }	
    public String getNvlTERM_transp() {
    	if(getTERM_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_transp();
    	}
    }
    public void setTERM_transp(String TERM_transp) {
    	if(TERM_transp == null) {
    		this.TERM_transp = null;
    	} else {
    		this.TERM_transp = TERM_transp;
    	}
    	this.TERM_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_validn
     * Comments    : 
     */	
    private String TERM_validn = null;
    
    private transient boolean TERM_validn_nullable = true;
    
    public boolean isNullableTERM_validn() {
    	return this.TERM_validn_nullable;
    }
    
    private transient boolean TERM_validn_invalidation = false;
    	
    public void setInvalidationTERM_validn(boolean invalidation) { 
    	this.TERM_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_validn() {
    	return this.TERM_validn_invalidation;
    }
    	
    private transient boolean TERM_validn_modified = false;
    
    public boolean isModifiedTERM_validn() {
    	return this.TERM_validn_modified;
    }
    	
    public void unModifiedTERM_validn() {
    	this.TERM_validn_modified = false;
    }
    public FieldProperty getTERM_validnFieldProperty() {
    	return fieldPropertyMap.get("TERM_validn");
    }
    
    public String getTERM_validn() {
    	return TERM_validn;
    }	
    public String getNvlTERM_validn() {
    	if(getTERM_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_validn();
    	}
    }
    public void setTERM_validn(String TERM_validn) {
    	if(TERM_validn == null) {
    		this.TERM_validn = null;
    	} else {
    		this.TERM_validn = TERM_validn;
    	}
    	this.TERM_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_sosi
     * Comments    : 
     */	
    private String TERM_sosi = null;
    
    private transient boolean TERM_sosi_nullable = true;
    
    public boolean isNullableTERM_sosi() {
    	return this.TERM_sosi_nullable;
    }
    
    private transient boolean TERM_sosi_invalidation = false;
    	
    public void setInvalidationTERM_sosi(boolean invalidation) { 
    	this.TERM_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_sosi() {
    	return this.TERM_sosi_invalidation;
    }
    	
    private transient boolean TERM_sosi_modified = false;
    
    public boolean isModifiedTERM_sosi() {
    	return this.TERM_sosi_modified;
    }
    	
    public void unModifiedTERM_sosi() {
    	this.TERM_sosi_modified = false;
    }
    public FieldProperty getTERM_sosiFieldProperty() {
    	return fieldPropertyMap.get("TERM_sosi");
    }
    
    public String getTERM_sosi() {
    	return TERM_sosi;
    }	
    public String getNvlTERM_sosi() {
    	if(getTERM_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_sosi();
    	}
    }
    public void setTERM_sosi(String TERM_sosi) {
    	if(TERM_sosi == null) {
    		this.TERM_sosi = null;
    	} else {
    		this.TERM_sosi = TERM_sosi;
    	}
    	this.TERM_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_ps
     * Comments    : 
     */	
    private String TERM_ps = null;
    
    private transient boolean TERM_ps_nullable = true;
    
    public boolean isNullableTERM_ps() {
    	return this.TERM_ps_nullable;
    }
    
    private transient boolean TERM_ps_invalidation = false;
    	
    public void setInvalidationTERM_ps(boolean invalidation) { 
    	this.TERM_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_ps() {
    	return this.TERM_ps_invalidation;
    }
    	
    private transient boolean TERM_ps_modified = false;
    
    public boolean isModifiedTERM_ps() {
    	return this.TERM_ps_modified;
    }
    	
    public void unModifiedTERM_ps() {
    	this.TERM_ps_modified = false;
    }
    public FieldProperty getTERM_psFieldProperty() {
    	return fieldPropertyMap.get("TERM_ps");
    }
    
    public String getTERM_ps() {
    	return TERM_ps;
    }	
    public String getNvlTERM_ps() {
    	if(getTERM_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_ps();
    	}
    }
    public void setTERM_ps(String TERM_ps) {
    	if(TERM_ps == null) {
    		this.TERM_ps = null;
    	} else {
    		this.TERM_ps = TERM_ps;
    	}
    	this.TERM_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_length
     * Comments    : 
     */	
    private int TIME_length = 0;
    
    private transient boolean TIME_length_nullable = false;
    
    public boolean isNullableTIME_length() {
    	return this.TIME_length_nullable;
    }
    
    private transient boolean TIME_length_invalidation = false;
    	
    public void setInvalidationTIME_length(boolean invalidation) { 
    	this.TIME_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_length() {
    	return this.TIME_length_invalidation;
    }
    	
    private transient boolean TIME_length_modified = false;
    
    public boolean isModifiedTIME_length() {
    	return this.TIME_length_modified;
    }
    	
    public void unModifiedTIME_length() {
    	this.TIME_length_modified = false;
    }
    public FieldProperty getTIME_lengthFieldProperty() {
    	return fieldPropertyMap.get("TIME_length");
    }
    
    public int getTIME_length() {
    	return TIME_length;
    }	
    public void setTIME_length(int TIME_length) {
    	this.TIME_length = TIME_length;
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(Integer TIME_length) {
    	if( TIME_length == null) {
    		this.TIME_length = 0;
    	} else{
    		this.TIME_length = TIME_length.intValue();
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(String TIME_length) {
    	if  (TIME_length==null || TIME_length.length() == 0) {
    		this.TIME_length = 0;
    	} else {
    		this.TIME_length = Integer.parseInt(TIME_length);
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_color
     * Comments    : 
     */	
    private String TIME_color = null;
    
    private transient boolean TIME_color_nullable = true;
    
    public boolean isNullableTIME_color() {
    	return this.TIME_color_nullable;
    }
    
    private transient boolean TIME_color_invalidation = false;
    	
    public void setInvalidationTIME_color(boolean invalidation) { 
    	this.TIME_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_color() {
    	return this.TIME_color_invalidation;
    }
    	
    private transient boolean TIME_color_modified = false;
    
    public boolean isModifiedTIME_color() {
    	return this.TIME_color_modified;
    }
    	
    public void unModifiedTIME_color() {
    	this.TIME_color_modified = false;
    }
    public FieldProperty getTIME_colorFieldProperty() {
    	return fieldPropertyMap.get("TIME_color");
    }
    
    public String getTIME_color() {
    	return TIME_color;
    }	
    public String getNvlTIME_color() {
    	if(getTIME_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_color();
    	}
    }
    public void setTIME_color(String TIME_color) {
    	if(TIME_color == null) {
    		this.TIME_color = null;
    	} else {
    		this.TIME_color = TIME_color;
    	}
    	this.TIME_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_hilight
     * Comments    : 
     */	
    private String TIME_hilight = null;
    
    private transient boolean TIME_hilight_nullable = true;
    
    public boolean isNullableTIME_hilight() {
    	return this.TIME_hilight_nullable;
    }
    
    private transient boolean TIME_hilight_invalidation = false;
    	
    public void setInvalidationTIME_hilight(boolean invalidation) { 
    	this.TIME_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_hilight() {
    	return this.TIME_hilight_invalidation;
    }
    	
    private transient boolean TIME_hilight_modified = false;
    
    public boolean isModifiedTIME_hilight() {
    	return this.TIME_hilight_modified;
    }
    	
    public void unModifiedTIME_hilight() {
    	this.TIME_hilight_modified = false;
    }
    public FieldProperty getTIME_hilightFieldProperty() {
    	return fieldPropertyMap.get("TIME_hilight");
    }
    
    public String getTIME_hilight() {
    	return TIME_hilight;
    }	
    public String getNvlTIME_hilight() {
    	if(getTIME_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_hilight();
    	}
    }
    public void setTIME_hilight(String TIME_hilight) {
    	if(TIME_hilight == null) {
    		this.TIME_hilight = null;
    	} else {
    		this.TIME_hilight = TIME_hilight;
    	}
    	this.TIME_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_outline
     * Comments    : 
     */	
    private String TIME_outline = null;
    
    private transient boolean TIME_outline_nullable = true;
    
    public boolean isNullableTIME_outline() {
    	return this.TIME_outline_nullable;
    }
    
    private transient boolean TIME_outline_invalidation = false;
    	
    public void setInvalidationTIME_outline(boolean invalidation) { 
    	this.TIME_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_outline() {
    	return this.TIME_outline_invalidation;
    }
    	
    private transient boolean TIME_outline_modified = false;
    
    public boolean isModifiedTIME_outline() {
    	return this.TIME_outline_modified;
    }
    	
    public void unModifiedTIME_outline() {
    	this.TIME_outline_modified = false;
    }
    public FieldProperty getTIME_outlineFieldProperty() {
    	return fieldPropertyMap.get("TIME_outline");
    }
    
    public String getTIME_outline() {
    	return TIME_outline;
    }	
    public String getNvlTIME_outline() {
    	if(getTIME_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_outline();
    	}
    }
    public void setTIME_outline(String TIME_outline) {
    	if(TIME_outline == null) {
    		this.TIME_outline = null;
    	} else {
    		this.TIME_outline = TIME_outline;
    	}
    	this.TIME_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_transp
     * Comments    : 
     */	
    private String TIME_transp = null;
    
    private transient boolean TIME_transp_nullable = true;
    
    public boolean isNullableTIME_transp() {
    	return this.TIME_transp_nullable;
    }
    
    private transient boolean TIME_transp_invalidation = false;
    	
    public void setInvalidationTIME_transp(boolean invalidation) { 
    	this.TIME_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_transp() {
    	return this.TIME_transp_invalidation;
    }
    	
    private transient boolean TIME_transp_modified = false;
    
    public boolean isModifiedTIME_transp() {
    	return this.TIME_transp_modified;
    }
    	
    public void unModifiedTIME_transp() {
    	this.TIME_transp_modified = false;
    }
    public FieldProperty getTIME_transpFieldProperty() {
    	return fieldPropertyMap.get("TIME_transp");
    }
    
    public String getTIME_transp() {
    	return TIME_transp;
    }	
    public String getNvlTIME_transp() {
    	if(getTIME_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_transp();
    	}
    }
    public void setTIME_transp(String TIME_transp) {
    	if(TIME_transp == null) {
    		this.TIME_transp = null;
    	} else {
    		this.TIME_transp = TIME_transp;
    	}
    	this.TIME_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_validn
     * Comments    : 
     */	
    private String TIME_validn = null;
    
    private transient boolean TIME_validn_nullable = true;
    
    public boolean isNullableTIME_validn() {
    	return this.TIME_validn_nullable;
    }
    
    private transient boolean TIME_validn_invalidation = false;
    	
    public void setInvalidationTIME_validn(boolean invalidation) { 
    	this.TIME_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_validn() {
    	return this.TIME_validn_invalidation;
    }
    	
    private transient boolean TIME_validn_modified = false;
    
    public boolean isModifiedTIME_validn() {
    	return this.TIME_validn_modified;
    }
    	
    public void unModifiedTIME_validn() {
    	this.TIME_validn_modified = false;
    }
    public FieldProperty getTIME_validnFieldProperty() {
    	return fieldPropertyMap.get("TIME_validn");
    }
    
    public String getTIME_validn() {
    	return TIME_validn;
    }	
    public String getNvlTIME_validn() {
    	if(getTIME_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_validn();
    	}
    }
    public void setTIME_validn(String TIME_validn) {
    	if(TIME_validn == null) {
    		this.TIME_validn = null;
    	} else {
    		this.TIME_validn = TIME_validn;
    	}
    	this.TIME_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_sosi
     * Comments    : 
     */	
    private String TIME_sosi = null;
    
    private transient boolean TIME_sosi_nullable = true;
    
    public boolean isNullableTIME_sosi() {
    	return this.TIME_sosi_nullable;
    }
    
    private transient boolean TIME_sosi_invalidation = false;
    	
    public void setInvalidationTIME_sosi(boolean invalidation) { 
    	this.TIME_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_sosi() {
    	return this.TIME_sosi_invalidation;
    }
    	
    private transient boolean TIME_sosi_modified = false;
    
    public boolean isModifiedTIME_sosi() {
    	return this.TIME_sosi_modified;
    }
    	
    public void unModifiedTIME_sosi() {
    	this.TIME_sosi_modified = false;
    }
    public FieldProperty getTIME_sosiFieldProperty() {
    	return fieldPropertyMap.get("TIME_sosi");
    }
    
    public String getTIME_sosi() {
    	return TIME_sosi;
    }	
    public String getNvlTIME_sosi() {
    	if(getTIME_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_sosi();
    	}
    }
    public void setTIME_sosi(String TIME_sosi) {
    	if(TIME_sosi == null) {
    		this.TIME_sosi = null;
    	} else {
    		this.TIME_sosi = TIME_sosi;
    	}
    	this.TIME_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_ps
     * Comments    : 
     */	
    private String TIME_ps = null;
    
    private transient boolean TIME_ps_nullable = true;
    
    public boolean isNullableTIME_ps() {
    	return this.TIME_ps_nullable;
    }
    
    private transient boolean TIME_ps_invalidation = false;
    	
    public void setInvalidationTIME_ps(boolean invalidation) { 
    	this.TIME_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_ps() {
    	return this.TIME_ps_invalidation;
    }
    	
    private transient boolean TIME_ps_modified = false;
    
    public boolean isModifiedTIME_ps() {
    	return this.TIME_ps_modified;
    }
    	
    public void unModifiedTIME_ps() {
    	this.TIME_ps_modified = false;
    }
    public FieldProperty getTIME_psFieldProperty() {
    	return fieldPropertyMap.get("TIME_ps");
    }
    
    public String getTIME_ps() {
    	return TIME_ps;
    }	
    public String getNvlTIME_ps() {
    	if(getTIME_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_ps();
    	}
    }
    public void setTIME_ps(String TIME_ps) {
    	if(TIME_ps == null) {
    		this.TIME_ps = null;
    	} else {
    		this.TIME_ps = TIME_ps;
    	}
    	this.TIME_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_length
     * Comments    : 
     */	
    private int ENDM_length = 0;
    
    private transient boolean ENDM_length_nullable = false;
    
    public boolean isNullableENDM_length() {
    	return this.ENDM_length_nullable;
    }
    
    private transient boolean ENDM_length_invalidation = false;
    	
    public void setInvalidationENDM_length(boolean invalidation) { 
    	this.ENDM_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_length() {
    	return this.ENDM_length_invalidation;
    }
    	
    private transient boolean ENDM_length_modified = false;
    
    public boolean isModifiedENDM_length() {
    	return this.ENDM_length_modified;
    }
    	
    public void unModifiedENDM_length() {
    	this.ENDM_length_modified = false;
    }
    public FieldProperty getENDM_lengthFieldProperty() {
    	return fieldPropertyMap.get("ENDM_length");
    }
    
    public int getENDM_length() {
    	return ENDM_length;
    }	
    public void setENDM_length(int ENDM_length) {
    	this.ENDM_length = ENDM_length;
    	this.ENDM_length_modified = true;
    	this.isModified = true;
    }
    public void setENDM_length(Integer ENDM_length) {
    	if( ENDM_length == null) {
    		this.ENDM_length = 0;
    	} else{
    		this.ENDM_length = ENDM_length.intValue();
    	}
    	this.ENDM_length_modified = true;
    	this.isModified = true;
    }
    public void setENDM_length(String ENDM_length) {
    	if  (ENDM_length==null || ENDM_length.length() == 0) {
    		this.ENDM_length = 0;
    	} else {
    		this.ENDM_length = Integer.parseInt(ENDM_length);
    	}
    	this.ENDM_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_color
     * Comments    : 
     */	
    private String ENDM_color = null;
    
    private transient boolean ENDM_color_nullable = true;
    
    public boolean isNullableENDM_color() {
    	return this.ENDM_color_nullable;
    }
    
    private transient boolean ENDM_color_invalidation = false;
    	
    public void setInvalidationENDM_color(boolean invalidation) { 
    	this.ENDM_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_color() {
    	return this.ENDM_color_invalidation;
    }
    	
    private transient boolean ENDM_color_modified = false;
    
    public boolean isModifiedENDM_color() {
    	return this.ENDM_color_modified;
    }
    	
    public void unModifiedENDM_color() {
    	this.ENDM_color_modified = false;
    }
    public FieldProperty getENDM_colorFieldProperty() {
    	return fieldPropertyMap.get("ENDM_color");
    }
    
    public String getENDM_color() {
    	return ENDM_color;
    }	
    public String getNvlENDM_color() {
    	if(getENDM_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_color();
    	}
    }
    public void setENDM_color(String ENDM_color) {
    	if(ENDM_color == null) {
    		this.ENDM_color = null;
    	} else {
    		this.ENDM_color = ENDM_color;
    	}
    	this.ENDM_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_hilight
     * Comments    : 
     */	
    private String ENDM_hilight = null;
    
    private transient boolean ENDM_hilight_nullable = true;
    
    public boolean isNullableENDM_hilight() {
    	return this.ENDM_hilight_nullable;
    }
    
    private transient boolean ENDM_hilight_invalidation = false;
    	
    public void setInvalidationENDM_hilight(boolean invalidation) { 
    	this.ENDM_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_hilight() {
    	return this.ENDM_hilight_invalidation;
    }
    	
    private transient boolean ENDM_hilight_modified = false;
    
    public boolean isModifiedENDM_hilight() {
    	return this.ENDM_hilight_modified;
    }
    	
    public void unModifiedENDM_hilight() {
    	this.ENDM_hilight_modified = false;
    }
    public FieldProperty getENDM_hilightFieldProperty() {
    	return fieldPropertyMap.get("ENDM_hilight");
    }
    
    public String getENDM_hilight() {
    	return ENDM_hilight;
    }	
    public String getNvlENDM_hilight() {
    	if(getENDM_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_hilight();
    	}
    }
    public void setENDM_hilight(String ENDM_hilight) {
    	if(ENDM_hilight == null) {
    		this.ENDM_hilight = null;
    	} else {
    		this.ENDM_hilight = ENDM_hilight;
    	}
    	this.ENDM_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_outline
     * Comments    : 
     */	
    private String ENDM_outline = null;
    
    private transient boolean ENDM_outline_nullable = true;
    
    public boolean isNullableENDM_outline() {
    	return this.ENDM_outline_nullable;
    }
    
    private transient boolean ENDM_outline_invalidation = false;
    	
    public void setInvalidationENDM_outline(boolean invalidation) { 
    	this.ENDM_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_outline() {
    	return this.ENDM_outline_invalidation;
    }
    	
    private transient boolean ENDM_outline_modified = false;
    
    public boolean isModifiedENDM_outline() {
    	return this.ENDM_outline_modified;
    }
    	
    public void unModifiedENDM_outline() {
    	this.ENDM_outline_modified = false;
    }
    public FieldProperty getENDM_outlineFieldProperty() {
    	return fieldPropertyMap.get("ENDM_outline");
    }
    
    public String getENDM_outline() {
    	return ENDM_outline;
    }	
    public String getNvlENDM_outline() {
    	if(getENDM_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_outline();
    	}
    }
    public void setENDM_outline(String ENDM_outline) {
    	if(ENDM_outline == null) {
    		this.ENDM_outline = null;
    	} else {
    		this.ENDM_outline = ENDM_outline;
    	}
    	this.ENDM_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_transp
     * Comments    : 
     */	
    private String ENDM_transp = null;
    
    private transient boolean ENDM_transp_nullable = true;
    
    public boolean isNullableENDM_transp() {
    	return this.ENDM_transp_nullable;
    }
    
    private transient boolean ENDM_transp_invalidation = false;
    	
    public void setInvalidationENDM_transp(boolean invalidation) { 
    	this.ENDM_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_transp() {
    	return this.ENDM_transp_invalidation;
    }
    	
    private transient boolean ENDM_transp_modified = false;
    
    public boolean isModifiedENDM_transp() {
    	return this.ENDM_transp_modified;
    }
    	
    public void unModifiedENDM_transp() {
    	this.ENDM_transp_modified = false;
    }
    public FieldProperty getENDM_transpFieldProperty() {
    	return fieldPropertyMap.get("ENDM_transp");
    }
    
    public String getENDM_transp() {
    	return ENDM_transp;
    }	
    public String getNvlENDM_transp() {
    	if(getENDM_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_transp();
    	}
    }
    public void setENDM_transp(String ENDM_transp) {
    	if(ENDM_transp == null) {
    		this.ENDM_transp = null;
    	} else {
    		this.ENDM_transp = ENDM_transp;
    	}
    	this.ENDM_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_validn
     * Comments    : 
     */	
    private String ENDM_validn = null;
    
    private transient boolean ENDM_validn_nullable = true;
    
    public boolean isNullableENDM_validn() {
    	return this.ENDM_validn_nullable;
    }
    
    private transient boolean ENDM_validn_invalidation = false;
    	
    public void setInvalidationENDM_validn(boolean invalidation) { 
    	this.ENDM_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_validn() {
    	return this.ENDM_validn_invalidation;
    }
    	
    private transient boolean ENDM_validn_modified = false;
    
    public boolean isModifiedENDM_validn() {
    	return this.ENDM_validn_modified;
    }
    	
    public void unModifiedENDM_validn() {
    	this.ENDM_validn_modified = false;
    }
    public FieldProperty getENDM_validnFieldProperty() {
    	return fieldPropertyMap.get("ENDM_validn");
    }
    
    public String getENDM_validn() {
    	return ENDM_validn;
    }	
    public String getNvlENDM_validn() {
    	if(getENDM_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_validn();
    	}
    }
    public void setENDM_validn(String ENDM_validn) {
    	if(ENDM_validn == null) {
    		this.ENDM_validn = null;
    	} else {
    		this.ENDM_validn = ENDM_validn;
    	}
    	this.ENDM_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_sosi
     * Comments    : 
     */	
    private String ENDM_sosi = null;
    
    private transient boolean ENDM_sosi_nullable = true;
    
    public boolean isNullableENDM_sosi() {
    	return this.ENDM_sosi_nullable;
    }
    
    private transient boolean ENDM_sosi_invalidation = false;
    	
    public void setInvalidationENDM_sosi(boolean invalidation) { 
    	this.ENDM_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_sosi() {
    	return this.ENDM_sosi_invalidation;
    }
    	
    private transient boolean ENDM_sosi_modified = false;
    
    public boolean isModifiedENDM_sosi() {
    	return this.ENDM_sosi_modified;
    }
    	
    public void unModifiedENDM_sosi() {
    	this.ENDM_sosi_modified = false;
    }
    public FieldProperty getENDM_sosiFieldProperty() {
    	return fieldPropertyMap.get("ENDM_sosi");
    }
    
    public String getENDM_sosi() {
    	return ENDM_sosi;
    }	
    public String getNvlENDM_sosi() {
    	if(getENDM_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_sosi();
    	}
    }
    public void setENDM_sosi(String ENDM_sosi) {
    	if(ENDM_sosi == null) {
    		this.ENDM_sosi = null;
    	} else {
    		this.ENDM_sosi = ENDM_sosi;
    	}
    	this.ENDM_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM_ps
     * Comments    : 
     */	
    private String ENDM_ps = null;
    
    private transient boolean ENDM_ps_nullable = true;
    
    public boolean isNullableENDM_ps() {
    	return this.ENDM_ps_nullable;
    }
    
    private transient boolean ENDM_ps_invalidation = false;
    	
    public void setInvalidationENDM_ps(boolean invalidation) { 
    	this.ENDM_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM_ps() {
    	return this.ENDM_ps_invalidation;
    }
    	
    private transient boolean ENDM_ps_modified = false;
    
    public boolean isModifiedENDM_ps() {
    	return this.ENDM_ps_modified;
    }
    	
    public void unModifiedENDM_ps() {
    	this.ENDM_ps_modified = false;
    }
    public FieldProperty getENDM_psFieldProperty() {
    	return fieldPropertyMap.get("ENDM_ps");
    }
    
    public String getENDM_ps() {
    	return ENDM_ps;
    }	
    public String getNvlENDM_ps() {
    	if(getENDM_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM_ps();
    	}
    }
    public void setENDM_ps(String ENDM_ps) {
    	if(ENDM_ps == null) {
    		this.ENDM_ps = null;
    	} else {
    		this.ENDM_ps = ENDM_ps;
    	}
    	this.ENDM_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_length
     * Comments    : 
     */	
    private int MSG_length = 0;
    
    private transient boolean MSG_length_nullable = false;
    
    public boolean isNullableMSG_length() {
    	return this.MSG_length_nullable;
    }
    
    private transient boolean MSG_length_invalidation = false;
    	
    public void setInvalidationMSG_length(boolean invalidation) { 
    	this.MSG_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_length() {
    	return this.MSG_length_invalidation;
    }
    	
    private transient boolean MSG_length_modified = false;
    
    public boolean isModifiedMSG_length() {
    	return this.MSG_length_modified;
    }
    	
    public void unModifiedMSG_length() {
    	this.MSG_length_modified = false;
    }
    public FieldProperty getMSG_lengthFieldProperty() {
    	return fieldPropertyMap.get("MSG_length");
    }
    
    public int getMSG_length() {
    	return MSG_length;
    }	
    public void setMSG_length(int MSG_length) {
    	this.MSG_length = MSG_length;
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(Integer MSG_length) {
    	if( MSG_length == null) {
    		this.MSG_length = 0;
    	} else{
    		this.MSG_length = MSG_length.intValue();
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(String MSG_length) {
    	if  (MSG_length==null || MSG_length.length() == 0) {
    		this.MSG_length = 0;
    	} else {
    		this.MSG_length = Integer.parseInt(MSG_length);
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_color
     * Comments    : 
     */	
    private String MSG_color = null;
    
    private transient boolean MSG_color_nullable = true;
    
    public boolean isNullableMSG_color() {
    	return this.MSG_color_nullable;
    }
    
    private transient boolean MSG_color_invalidation = false;
    	
    public void setInvalidationMSG_color(boolean invalidation) { 
    	this.MSG_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_color() {
    	return this.MSG_color_invalidation;
    }
    	
    private transient boolean MSG_color_modified = false;
    
    public boolean isModifiedMSG_color() {
    	return this.MSG_color_modified;
    }
    	
    public void unModifiedMSG_color() {
    	this.MSG_color_modified = false;
    }
    public FieldProperty getMSG_colorFieldProperty() {
    	return fieldPropertyMap.get("MSG_color");
    }
    
    public String getMSG_color() {
    	return MSG_color;
    }	
    public String getNvlMSG_color() {
    	if(getMSG_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_color();
    	}
    }
    public void setMSG_color(String MSG_color) {
    	if(MSG_color == null) {
    		this.MSG_color = null;
    	} else {
    		this.MSG_color = MSG_color;
    	}
    	this.MSG_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_hilight
     * Comments    : 
     */	
    private String MSG_hilight = null;
    
    private transient boolean MSG_hilight_nullable = true;
    
    public boolean isNullableMSG_hilight() {
    	return this.MSG_hilight_nullable;
    }
    
    private transient boolean MSG_hilight_invalidation = false;
    	
    public void setInvalidationMSG_hilight(boolean invalidation) { 
    	this.MSG_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_hilight() {
    	return this.MSG_hilight_invalidation;
    }
    	
    private transient boolean MSG_hilight_modified = false;
    
    public boolean isModifiedMSG_hilight() {
    	return this.MSG_hilight_modified;
    }
    	
    public void unModifiedMSG_hilight() {
    	this.MSG_hilight_modified = false;
    }
    public FieldProperty getMSG_hilightFieldProperty() {
    	return fieldPropertyMap.get("MSG_hilight");
    }
    
    public String getMSG_hilight() {
    	return MSG_hilight;
    }	
    public String getNvlMSG_hilight() {
    	if(getMSG_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_hilight();
    	}
    }
    public void setMSG_hilight(String MSG_hilight) {
    	if(MSG_hilight == null) {
    		this.MSG_hilight = null;
    	} else {
    		this.MSG_hilight = MSG_hilight;
    	}
    	this.MSG_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_outline
     * Comments    : 
     */	
    private String MSG_outline = null;
    
    private transient boolean MSG_outline_nullable = true;
    
    public boolean isNullableMSG_outline() {
    	return this.MSG_outline_nullable;
    }
    
    private transient boolean MSG_outline_invalidation = false;
    	
    public void setInvalidationMSG_outline(boolean invalidation) { 
    	this.MSG_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_outline() {
    	return this.MSG_outline_invalidation;
    }
    	
    private transient boolean MSG_outline_modified = false;
    
    public boolean isModifiedMSG_outline() {
    	return this.MSG_outline_modified;
    }
    	
    public void unModifiedMSG_outline() {
    	this.MSG_outline_modified = false;
    }
    public FieldProperty getMSG_outlineFieldProperty() {
    	return fieldPropertyMap.get("MSG_outline");
    }
    
    public String getMSG_outline() {
    	return MSG_outline;
    }	
    public String getNvlMSG_outline() {
    	if(getMSG_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_outline();
    	}
    }
    public void setMSG_outline(String MSG_outline) {
    	if(MSG_outline == null) {
    		this.MSG_outline = null;
    	} else {
    		this.MSG_outline = MSG_outline;
    	}
    	this.MSG_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_transp
     * Comments    : 
     */	
    private String MSG_transp = null;
    
    private transient boolean MSG_transp_nullable = true;
    
    public boolean isNullableMSG_transp() {
    	return this.MSG_transp_nullable;
    }
    
    private transient boolean MSG_transp_invalidation = false;
    	
    public void setInvalidationMSG_transp(boolean invalidation) { 
    	this.MSG_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_transp() {
    	return this.MSG_transp_invalidation;
    }
    	
    private transient boolean MSG_transp_modified = false;
    
    public boolean isModifiedMSG_transp() {
    	return this.MSG_transp_modified;
    }
    	
    public void unModifiedMSG_transp() {
    	this.MSG_transp_modified = false;
    }
    public FieldProperty getMSG_transpFieldProperty() {
    	return fieldPropertyMap.get("MSG_transp");
    }
    
    public String getMSG_transp() {
    	return MSG_transp;
    }	
    public String getNvlMSG_transp() {
    	if(getMSG_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_transp();
    	}
    }
    public void setMSG_transp(String MSG_transp) {
    	if(MSG_transp == null) {
    		this.MSG_transp = null;
    	} else {
    		this.MSG_transp = MSG_transp;
    	}
    	this.MSG_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_validn
     * Comments    : 
     */	
    private String MSG_validn = null;
    
    private transient boolean MSG_validn_nullable = true;
    
    public boolean isNullableMSG_validn() {
    	return this.MSG_validn_nullable;
    }
    
    private transient boolean MSG_validn_invalidation = false;
    	
    public void setInvalidationMSG_validn(boolean invalidation) { 
    	this.MSG_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_validn() {
    	return this.MSG_validn_invalidation;
    }
    	
    private transient boolean MSG_validn_modified = false;
    
    public boolean isModifiedMSG_validn() {
    	return this.MSG_validn_modified;
    }
    	
    public void unModifiedMSG_validn() {
    	this.MSG_validn_modified = false;
    }
    public FieldProperty getMSG_validnFieldProperty() {
    	return fieldPropertyMap.get("MSG_validn");
    }
    
    public String getMSG_validn() {
    	return MSG_validn;
    }	
    public String getNvlMSG_validn() {
    	if(getMSG_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_validn();
    	}
    }
    public void setMSG_validn(String MSG_validn) {
    	if(MSG_validn == null) {
    		this.MSG_validn = null;
    	} else {
    		this.MSG_validn = MSG_validn;
    	}
    	this.MSG_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_sosi
     * Comments    : 
     */	
    private String MSG_sosi = null;
    
    private transient boolean MSG_sosi_nullable = true;
    
    public boolean isNullableMSG_sosi() {
    	return this.MSG_sosi_nullable;
    }
    
    private transient boolean MSG_sosi_invalidation = false;
    	
    public void setInvalidationMSG_sosi(boolean invalidation) { 
    	this.MSG_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_sosi() {
    	return this.MSG_sosi_invalidation;
    }
    	
    private transient boolean MSG_sosi_modified = false;
    
    public boolean isModifiedMSG_sosi() {
    	return this.MSG_sosi_modified;
    }
    	
    public void unModifiedMSG_sosi() {
    	this.MSG_sosi_modified = false;
    }
    public FieldProperty getMSG_sosiFieldProperty() {
    	return fieldPropertyMap.get("MSG_sosi");
    }
    
    public String getMSG_sosi() {
    	return MSG_sosi;
    }	
    public String getNvlMSG_sosi() {
    	if(getMSG_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_sosi();
    	}
    }
    public void setMSG_sosi(String MSG_sosi) {
    	if(MSG_sosi == null) {
    		this.MSG_sosi = null;
    	} else {
    		this.MSG_sosi = MSG_sosi;
    	}
    	this.MSG_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_ps
     * Comments    : 
     */	
    private String MSG_ps = null;
    
    private transient boolean MSG_ps_nullable = true;
    
    public boolean isNullableMSG_ps() {
    	return this.MSG_ps_nullable;
    }
    
    private transient boolean MSG_ps_invalidation = false;
    	
    public void setInvalidationMSG_ps(boolean invalidation) { 
    	this.MSG_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_ps() {
    	return this.MSG_ps_invalidation;
    }
    	
    private transient boolean MSG_ps_modified = false;
    
    public boolean isModifiedMSG_ps() {
    	return this.MSG_ps_modified;
    }
    	
    public void unModifiedMSG_ps() {
    	this.MSG_ps_modified = false;
    }
    public FieldProperty getMSG_psFieldProperty() {
    	return fieldPropertyMap.get("MSG_ps");
    }
    
    public String getMSG_ps() {
    	return MSG_ps;
    }	
    public String getNvlMSG_ps() {
    	if(getMSG_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_ps();
    	}
    }
    public void setMSG_ps(String MSG_ps) {
    	if(MSG_ps == null) {
    		this.MSG_ps = null;
    	} else {
    		this.MSG_ps = MSG_ps;
    	}
    	this.MSG_ps_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_length_modified = false;
    	this.DATE_color_modified = false;
    	this.DATE_hilight_modified = false;
    	this.DATE_outline_modified = false;
    	this.DATE_transp_modified = false;
    	this.DATE_validn_modified = false;
    	this.DATE_sosi_modified = false;
    	this.DATE_ps_modified = false;
    	this.TERM_length_modified = false;
    	this.TERM_color_modified = false;
    	this.TERM_hilight_modified = false;
    	this.TERM_outline_modified = false;
    	this.TERM_transp_modified = false;
    	this.TERM_validn_modified = false;
    	this.TERM_sosi_modified = false;
    	this.TERM_ps_modified = false;
    	this.TIME_length_modified = false;
    	this.TIME_color_modified = false;
    	this.TIME_hilight_modified = false;
    	this.TIME_outline_modified = false;
    	this.TIME_transp_modified = false;
    	this.TIME_validn_modified = false;
    	this.TIME_sosi_modified = false;
    	this.TIME_ps_modified = false;
    	this.ENDM_length_modified = false;
    	this.ENDM_color_modified = false;
    	this.ENDM_hilight_modified = false;
    	this.ENDM_outline_modified = false;
    	this.ENDM_transp_modified = false;
    	this.ENDM_validn_modified = false;
    	this.ENDM_sosi_modified = false;
    	this.ENDM_ps_modified = false;
    	this.MSG_length_modified = false;
    	this.MSG_color_modified = false;
    	this.MSG_hilight_modified = false;
    	this.MSG_outline_modified = false;
    	this.MSG_transp_modified = false;
    	this.MSG_validn_modified = false;
    	this.MSG_sosi_modified = false;
    	this.MSG_ps_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_length_modified == true)
    		modifiedFields.add("DATE_length");
    	if(this.DATE_color_modified == true)
    		modifiedFields.add("DATE_color");
    	if(this.DATE_hilight_modified == true)
    		modifiedFields.add("DATE_hilight");
    	if(this.DATE_outline_modified == true)
    		modifiedFields.add("DATE_outline");
    	if(this.DATE_transp_modified == true)
    		modifiedFields.add("DATE_transp");
    	if(this.DATE_validn_modified == true)
    		modifiedFields.add("DATE_validn");
    	if(this.DATE_sosi_modified == true)
    		modifiedFields.add("DATE_sosi");
    	if(this.DATE_ps_modified == true)
    		modifiedFields.add("DATE_ps");
    	if(this.TERM_length_modified == true)
    		modifiedFields.add("TERM_length");
    	if(this.TERM_color_modified == true)
    		modifiedFields.add("TERM_color");
    	if(this.TERM_hilight_modified == true)
    		modifiedFields.add("TERM_hilight");
    	if(this.TERM_outline_modified == true)
    		modifiedFields.add("TERM_outline");
    	if(this.TERM_transp_modified == true)
    		modifiedFields.add("TERM_transp");
    	if(this.TERM_validn_modified == true)
    		modifiedFields.add("TERM_validn");
    	if(this.TERM_sosi_modified == true)
    		modifiedFields.add("TERM_sosi");
    	if(this.TERM_ps_modified == true)
    		modifiedFields.add("TERM_ps");
    	if(this.TIME_length_modified == true)
    		modifiedFields.add("TIME_length");
    	if(this.TIME_color_modified == true)
    		modifiedFields.add("TIME_color");
    	if(this.TIME_hilight_modified == true)
    		modifiedFields.add("TIME_hilight");
    	if(this.TIME_outline_modified == true)
    		modifiedFields.add("TIME_outline");
    	if(this.TIME_transp_modified == true)
    		modifiedFields.add("TIME_transp");
    	if(this.TIME_validn_modified == true)
    		modifiedFields.add("TIME_validn");
    	if(this.TIME_sosi_modified == true)
    		modifiedFields.add("TIME_sosi");
    	if(this.TIME_ps_modified == true)
    		modifiedFields.add("TIME_ps");
    	if(this.ENDM_length_modified == true)
    		modifiedFields.add("ENDM_length");
    	if(this.ENDM_color_modified == true)
    		modifiedFields.add("ENDM_color");
    	if(this.ENDM_hilight_modified == true)
    		modifiedFields.add("ENDM_hilight");
    	if(this.ENDM_outline_modified == true)
    		modifiedFields.add("ENDM_outline");
    	if(this.ENDM_transp_modified == true)
    		modifiedFields.add("ENDM_transp");
    	if(this.ENDM_validn_modified == true)
    		modifiedFields.add("ENDM_validn");
    	if(this.ENDM_sosi_modified == true)
    		modifiedFields.add("ENDM_sosi");
    	if(this.ENDM_ps_modified == true)
    		modifiedFields.add("ENDM_ps");
    	if(this.MSG_length_modified == true)
    		modifiedFields.add("MSG_length");
    	if(this.MSG_color_modified == true)
    		modifiedFields.add("MSG_color");
    	if(this.MSG_hilight_modified == true)
    		modifiedFields.add("MSG_hilight");
    	if(this.MSG_outline_modified == true)
    		modifiedFields.add("MSG_outline");
    	if(this.MSG_transp_modified == true)
    		modifiedFields.add("MSG_transp");
    	if(this.MSG_validn_modified == true)
    		modifiedFields.add("MSG_validn");
    	if(this.MSG_sosi_modified == true)
    		modifiedFields.add("MSG_sosi");
    	if(this.MSG_ps_modified == true)
    		modifiedFields.add("MSG_ps");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM07_OIVPM07_meta copyObj = new OIVPM07_OIVPM07_meta();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM07_OIVPM07_meta) {
    	if(this == _oIVPM07_OIVPM07_meta) return;
    	OIVPM07_OIVPM07_meta __oIVPM07_OIVPM07_meta = (OIVPM07_OIVPM07_meta) _oIVPM07_OIVPM07_meta;
    	this.setDATE_length(__oIVPM07_OIVPM07_meta.getDATE_length());
    	this.setDATE_color(__oIVPM07_OIVPM07_meta.getDATE_color());
    	this.setDATE_hilight(__oIVPM07_OIVPM07_meta.getDATE_hilight());
    	this.setDATE_outline(__oIVPM07_OIVPM07_meta.getDATE_outline());
    	this.setDATE_transp(__oIVPM07_OIVPM07_meta.getDATE_transp());
    	this.setDATE_validn(__oIVPM07_OIVPM07_meta.getDATE_validn());
    	this.setDATE_sosi(__oIVPM07_OIVPM07_meta.getDATE_sosi());
    	this.setDATE_ps(__oIVPM07_OIVPM07_meta.getDATE_ps());
    	this.setTERM_length(__oIVPM07_OIVPM07_meta.getTERM_length());
    	this.setTERM_color(__oIVPM07_OIVPM07_meta.getTERM_color());
    	this.setTERM_hilight(__oIVPM07_OIVPM07_meta.getTERM_hilight());
    	this.setTERM_outline(__oIVPM07_OIVPM07_meta.getTERM_outline());
    	this.setTERM_transp(__oIVPM07_OIVPM07_meta.getTERM_transp());
    	this.setTERM_validn(__oIVPM07_OIVPM07_meta.getTERM_validn());
    	this.setTERM_sosi(__oIVPM07_OIVPM07_meta.getTERM_sosi());
    	this.setTERM_ps(__oIVPM07_OIVPM07_meta.getTERM_ps());
    	this.setTIME_length(__oIVPM07_OIVPM07_meta.getTIME_length());
    	this.setTIME_color(__oIVPM07_OIVPM07_meta.getTIME_color());
    	this.setTIME_hilight(__oIVPM07_OIVPM07_meta.getTIME_hilight());
    	this.setTIME_outline(__oIVPM07_OIVPM07_meta.getTIME_outline());
    	this.setTIME_transp(__oIVPM07_OIVPM07_meta.getTIME_transp());
    	this.setTIME_validn(__oIVPM07_OIVPM07_meta.getTIME_validn());
    	this.setTIME_sosi(__oIVPM07_OIVPM07_meta.getTIME_sosi());
    	this.setTIME_ps(__oIVPM07_OIVPM07_meta.getTIME_ps());
    	this.setENDM_length(__oIVPM07_OIVPM07_meta.getENDM_length());
    	this.setENDM_color(__oIVPM07_OIVPM07_meta.getENDM_color());
    	this.setENDM_hilight(__oIVPM07_OIVPM07_meta.getENDM_hilight());
    	this.setENDM_outline(__oIVPM07_OIVPM07_meta.getENDM_outline());
    	this.setENDM_transp(__oIVPM07_OIVPM07_meta.getENDM_transp());
    	this.setENDM_validn(__oIVPM07_OIVPM07_meta.getENDM_validn());
    	this.setENDM_sosi(__oIVPM07_OIVPM07_meta.getENDM_sosi());
    	this.setENDM_ps(__oIVPM07_OIVPM07_meta.getENDM_ps());
    	this.setMSG_length(__oIVPM07_OIVPM07_meta.getMSG_length());
    	this.setMSG_color(__oIVPM07_OIVPM07_meta.getMSG_color());
    	this.setMSG_hilight(__oIVPM07_OIVPM07_meta.getMSG_hilight());
    	this.setMSG_outline(__oIVPM07_OIVPM07_meta.getMSG_outline());
    	this.setMSG_transp(__oIVPM07_OIVPM07_meta.getMSG_transp());
    	this.setMSG_validn(__oIVPM07_OIVPM07_meta.getMSG_validn());
    	this.setMSG_sosi(__oIVPM07_OIVPM07_meta.getMSG_sosi());
    	this.setMSG_ps(__oIVPM07_OIVPM07_meta.getMSG_ps());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE_length : ").append(DATE_length).append("\n");	
    	buffer.append("DATE_color : ").append(DATE_color).append("\n");	
    	buffer.append("DATE_hilight : ").append(DATE_hilight).append("\n");	
    	buffer.append("DATE_outline : ").append(DATE_outline).append("\n");	
    	buffer.append("DATE_transp : ").append(DATE_transp).append("\n");	
    	buffer.append("DATE_validn : ").append(DATE_validn).append("\n");	
    	buffer.append("DATE_sosi : ").append(DATE_sosi).append("\n");	
    	buffer.append("DATE_ps : ").append(DATE_ps).append("\n");	
    	buffer.append("TERM_length : ").append(TERM_length).append("\n");	
    	buffer.append("TERM_color : ").append(TERM_color).append("\n");	
    	buffer.append("TERM_hilight : ").append(TERM_hilight).append("\n");	
    	buffer.append("TERM_outline : ").append(TERM_outline).append("\n");	
    	buffer.append("TERM_transp : ").append(TERM_transp).append("\n");	
    	buffer.append("TERM_validn : ").append(TERM_validn).append("\n");	
    	buffer.append("TERM_sosi : ").append(TERM_sosi).append("\n");	
    	buffer.append("TERM_ps : ").append(TERM_ps).append("\n");	
    	buffer.append("TIME_length : ").append(TIME_length).append("\n");	
    	buffer.append("TIME_color : ").append(TIME_color).append("\n");	
    	buffer.append("TIME_hilight : ").append(TIME_hilight).append("\n");	
    	buffer.append("TIME_outline : ").append(TIME_outline).append("\n");	
    	buffer.append("TIME_transp : ").append(TIME_transp).append("\n");	
    	buffer.append("TIME_validn : ").append(TIME_validn).append("\n");	
    	buffer.append("TIME_sosi : ").append(TIME_sosi).append("\n");	
    	buffer.append("TIME_ps : ").append(TIME_ps).append("\n");	
    	buffer.append("ENDM_length : ").append(ENDM_length).append("\n");	
    	buffer.append("ENDM_color : ").append(ENDM_color).append("\n");	
    	buffer.append("ENDM_hilight : ").append(ENDM_hilight).append("\n");	
    	buffer.append("ENDM_outline : ").append(ENDM_outline).append("\n");	
    	buffer.append("ENDM_transp : ").append(ENDM_transp).append("\n");	
    	buffer.append("ENDM_validn : ").append(ENDM_validn).append("\n");	
    	buffer.append("ENDM_sosi : ").append(ENDM_sosi).append("\n");	
    	buffer.append("ENDM_ps : ").append(ENDM_ps).append("\n");	
    	buffer.append("MSG_length : ").append(MSG_length).append("\n");	
    	buffer.append("MSG_color : ").append(MSG_color).append("\n");	
    	buffer.append("MSG_hilight : ").append(MSG_hilight).append("\n");	
    	buffer.append("MSG_outline : ").append(MSG_outline).append("\n");	
    	buffer.append("MSG_transp : ").append(MSG_transp).append("\n");	
    	buffer.append("MSG_validn : ").append(MSG_validn).append("\n");	
    	buffer.append("MSG_sosi : ").append(MSG_sosi).append("\n");	
    	buffer.append("MSG_ps : ").append(MSG_ps).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(40);
    	fieldPropertyMap.put("DATE_length", FieldProperty.builder()
    	              .setPhysicalName("DATE_length")
    	              .setLogicalName("DATE_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DATE_color", FieldProperty.builder()
    	              .setPhysicalName("DATE_color")
    	              .setLogicalName("DATE_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_hilight", FieldProperty.builder()
    	              .setPhysicalName("DATE_hilight")
    	              .setLogicalName("DATE_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_outline", FieldProperty.builder()
    	              .setPhysicalName("DATE_outline")
    	              .setLogicalName("DATE_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_transp", FieldProperty.builder()
    	              .setPhysicalName("DATE_transp")
    	              .setLogicalName("DATE_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_validn", FieldProperty.builder()
    	              .setPhysicalName("DATE_validn")
    	              .setLogicalName("DATE_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_sosi", FieldProperty.builder()
    	              .setPhysicalName("DATE_sosi")
    	              .setLogicalName("DATE_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_ps", FieldProperty.builder()
    	              .setPhysicalName("DATE_ps")
    	              .setLogicalName("DATE_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_length", FieldProperty.builder()
    	              .setPhysicalName("TERM_length")
    	              .setLogicalName("TERM_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TERM_color", FieldProperty.builder()
    	              .setPhysicalName("TERM_color")
    	              .setLogicalName("TERM_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_hilight", FieldProperty.builder()
    	              .setPhysicalName("TERM_hilight")
    	              .setLogicalName("TERM_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_outline", FieldProperty.builder()
    	              .setPhysicalName("TERM_outline")
    	              .setLogicalName("TERM_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_transp", FieldProperty.builder()
    	              .setPhysicalName("TERM_transp")
    	              .setLogicalName("TERM_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_validn", FieldProperty.builder()
    	              .setPhysicalName("TERM_validn")
    	              .setLogicalName("TERM_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_sosi", FieldProperty.builder()
    	              .setPhysicalName("TERM_sosi")
    	              .setLogicalName("TERM_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_ps", FieldProperty.builder()
    	              .setPhysicalName("TERM_ps")
    	              .setLogicalName("TERM_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_length", FieldProperty.builder()
    	              .setPhysicalName("TIME_length")
    	              .setLogicalName("TIME_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TIME_color", FieldProperty.builder()
    	              .setPhysicalName("TIME_color")
    	              .setLogicalName("TIME_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_hilight", FieldProperty.builder()
    	              .setPhysicalName("TIME_hilight")
    	              .setLogicalName("TIME_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_outline", FieldProperty.builder()
    	              .setPhysicalName("TIME_outline")
    	              .setLogicalName("TIME_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_transp", FieldProperty.builder()
    	              .setPhysicalName("TIME_transp")
    	              .setLogicalName("TIME_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_validn", FieldProperty.builder()
    	              .setPhysicalName("TIME_validn")
    	              .setLogicalName("TIME_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_sosi", FieldProperty.builder()
    	              .setPhysicalName("TIME_sosi")
    	              .setLogicalName("TIME_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_ps", FieldProperty.builder()
    	              .setPhysicalName("TIME_ps")
    	              .setLogicalName("TIME_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_length", FieldProperty.builder()
    	              .setPhysicalName("ENDM_length")
    	              .setLogicalName("ENDM_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("ENDM_color", FieldProperty.builder()
    	              .setPhysicalName("ENDM_color")
    	              .setLogicalName("ENDM_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_hilight", FieldProperty.builder()
    	              .setPhysicalName("ENDM_hilight")
    	              .setLogicalName("ENDM_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_outline", FieldProperty.builder()
    	              .setPhysicalName("ENDM_outline")
    	              .setLogicalName("ENDM_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_transp", FieldProperty.builder()
    	              .setPhysicalName("ENDM_transp")
    	              .setLogicalName("ENDM_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_validn", FieldProperty.builder()
    	              .setPhysicalName("ENDM_validn")
    	              .setLogicalName("ENDM_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_sosi", FieldProperty.builder()
    	              .setPhysicalName("ENDM_sosi")
    	              .setLogicalName("ENDM_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM_ps", FieldProperty.builder()
    	              .setPhysicalName("ENDM_ps")
    	              .setLogicalName("ENDM_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_length", FieldProperty.builder()
    	              .setPhysicalName("MSG_length")
    	              .setLogicalName("MSG_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("MSG_color", FieldProperty.builder()
    	              .setPhysicalName("MSG_color")
    	              .setLogicalName("MSG_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_hilight", FieldProperty.builder()
    	              .setPhysicalName("MSG_hilight")
    	              .setLogicalName("MSG_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_outline", FieldProperty.builder()
    	              .setPhysicalName("MSG_outline")
    	              .setLogicalName("MSG_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_transp", FieldProperty.builder()
    	              .setPhysicalName("MSG_transp")
    	              .setLogicalName("MSG_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_validn", FieldProperty.builder()
    	              .setPhysicalName("MSG_validn")
    	              .setLogicalName("MSG_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_sosi", FieldProperty.builder()
    	              .setPhysicalName("MSG_sosi")
    	              .setLogicalName("MSG_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_ps", FieldProperty.builder()
    	              .setPhysicalName("MSG_ps")
    	              .setLogicalName("MSG_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : return getDATE_length();
    		case "DATE_color" : return getDATE_color();
    		case "DATE_hilight" : return getDATE_hilight();
    		case "DATE_outline" : return getDATE_outline();
    		case "DATE_transp" : return getDATE_transp();
    		case "DATE_validn" : return getDATE_validn();
    		case "DATE_sosi" : return getDATE_sosi();
    		case "DATE_ps" : return getDATE_ps();
    		case "TERM_length" : return getTERM_length();
    		case "TERM_color" : return getTERM_color();
    		case "TERM_hilight" : return getTERM_hilight();
    		case "TERM_outline" : return getTERM_outline();
    		case "TERM_transp" : return getTERM_transp();
    		case "TERM_validn" : return getTERM_validn();
    		case "TERM_sosi" : return getTERM_sosi();
    		case "TERM_ps" : return getTERM_ps();
    		case "TIME_length" : return getTIME_length();
    		case "TIME_color" : return getTIME_color();
    		case "TIME_hilight" : return getTIME_hilight();
    		case "TIME_outline" : return getTIME_outline();
    		case "TIME_transp" : return getTIME_transp();
    		case "TIME_validn" : return getTIME_validn();
    		case "TIME_sosi" : return getTIME_sosi();
    		case "TIME_ps" : return getTIME_ps();
    		case "ENDM_length" : return getENDM_length();
    		case "ENDM_color" : return getENDM_color();
    		case "ENDM_hilight" : return getENDM_hilight();
    		case "ENDM_outline" : return getENDM_outline();
    		case "ENDM_transp" : return getENDM_transp();
    		case "ENDM_validn" : return getENDM_validn();
    		case "ENDM_sosi" : return getENDM_sosi();
    		case "ENDM_ps" : return getENDM_ps();
    		case "MSG_length" : return getMSG_length();
    		case "MSG_color" : return getMSG_color();
    		case "MSG_hilight" : return getMSG_hilight();
    		case "MSG_outline" : return getMSG_outline();
    		case "MSG_transp" : return getMSG_transp();
    		case "MSG_validn" : return getMSG_validn();
    		case "MSG_sosi" : return getMSG_sosi();
    		case "MSG_ps" : return getMSG_ps();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : setDATE_length((Integer)arg);break;
    		case "DATE_color" : setDATE_color((String)arg); break;
    		case "DATE_hilight" : setDATE_hilight((String)arg); break;
    		case "DATE_outline" : setDATE_outline((String)arg); break;
    		case "DATE_transp" : setDATE_transp((String)arg); break;
    		case "DATE_validn" : setDATE_validn((String)arg); break;
    		case "DATE_sosi" : setDATE_sosi((String)arg); break;
    		case "DATE_ps" : setDATE_ps((String)arg); break;
    		case "TERM_length" : setTERM_length((Integer)arg);break;
    		case "TERM_color" : setTERM_color((String)arg); break;
    		case "TERM_hilight" : setTERM_hilight((String)arg); break;
    		case "TERM_outline" : setTERM_outline((String)arg); break;
    		case "TERM_transp" : setTERM_transp((String)arg); break;
    		case "TERM_validn" : setTERM_validn((String)arg); break;
    		case "TERM_sosi" : setTERM_sosi((String)arg); break;
    		case "TERM_ps" : setTERM_ps((String)arg); break;
    		case "TIME_length" : setTIME_length((Integer)arg);break;
    		case "TIME_color" : setTIME_color((String)arg); break;
    		case "TIME_hilight" : setTIME_hilight((String)arg); break;
    		case "TIME_outline" : setTIME_outline((String)arg); break;
    		case "TIME_transp" : setTIME_transp((String)arg); break;
    		case "TIME_validn" : setTIME_validn((String)arg); break;
    		case "TIME_sosi" : setTIME_sosi((String)arg); break;
    		case "TIME_ps" : setTIME_ps((String)arg); break;
    		case "ENDM_length" : setENDM_length((Integer)arg);break;
    		case "ENDM_color" : setENDM_color((String)arg); break;
    		case "ENDM_hilight" : setENDM_hilight((String)arg); break;
    		case "ENDM_outline" : setENDM_outline((String)arg); break;
    		case "ENDM_transp" : setENDM_transp((String)arg); break;
    		case "ENDM_validn" : setENDM_validn((String)arg); break;
    		case "ENDM_sosi" : setENDM_sosi((String)arg); break;
    		case "ENDM_ps" : setENDM_ps((String)arg); break;
    		case "MSG_length" : setMSG_length((Integer)arg);break;
    		case "MSG_color" : setMSG_color((String)arg); break;
    		case "MSG_hilight" : setMSG_hilight((String)arg); break;
    		case "MSG_outline" : setMSG_outline((String)arg); break;
    		case "MSG_transp" : setMSG_transp((String)arg); break;
    		case "MSG_validn" : setMSG_validn((String)arg); break;
    		case "MSG_sosi" : setMSG_sosi((String)arg); break;
    		case "MSG_ps" : setMSG_ps((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM07_OIVPM07_meta _OIVPM07_OIVPM07_meta = (OIVPM07_OIVPM07_meta) obj;
    	if(this.DATE_length != _OIVPM07_OIVPM07_meta.getDATE_length()) return false;
    	if(this.DATE_color == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_color() != null)
    		return false;
    	} else if(!this.DATE_color.equals(_OIVPM07_OIVPM07_meta.getDATE_color()))
    		return false;
    	if(this.DATE_hilight == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_hilight() != null)
    		return false;
    	} else if(!this.DATE_hilight.equals(_OIVPM07_OIVPM07_meta.getDATE_hilight()))
    		return false;
    	if(this.DATE_outline == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_outline() != null)
    		return false;
    	} else if(!this.DATE_outline.equals(_OIVPM07_OIVPM07_meta.getDATE_outline()))
    		return false;
    	if(this.DATE_transp == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_transp() != null)
    		return false;
    	} else if(!this.DATE_transp.equals(_OIVPM07_OIVPM07_meta.getDATE_transp()))
    		return false;
    	if(this.DATE_validn == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_validn() != null)
    		return false;
    	} else if(!this.DATE_validn.equals(_OIVPM07_OIVPM07_meta.getDATE_validn()))
    		return false;
    	if(this.DATE_sosi == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_sosi() != null)
    		return false;
    	} else if(!this.DATE_sosi.equals(_OIVPM07_OIVPM07_meta.getDATE_sosi()))
    		return false;
    	if(this.DATE_ps == null) {
    	if(_OIVPM07_OIVPM07_meta.getDATE_ps() != null)
    		return false;
    	} else if(!this.DATE_ps.equals(_OIVPM07_OIVPM07_meta.getDATE_ps()))
    		return false;
    	if(this.TERM_length != _OIVPM07_OIVPM07_meta.getTERM_length()) return false;
    	if(this.TERM_color == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_color() != null)
    		return false;
    	} else if(!this.TERM_color.equals(_OIVPM07_OIVPM07_meta.getTERM_color()))
    		return false;
    	if(this.TERM_hilight == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_hilight() != null)
    		return false;
    	} else if(!this.TERM_hilight.equals(_OIVPM07_OIVPM07_meta.getTERM_hilight()))
    		return false;
    	if(this.TERM_outline == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_outline() != null)
    		return false;
    	} else if(!this.TERM_outline.equals(_OIVPM07_OIVPM07_meta.getTERM_outline()))
    		return false;
    	if(this.TERM_transp == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_transp() != null)
    		return false;
    	} else if(!this.TERM_transp.equals(_OIVPM07_OIVPM07_meta.getTERM_transp()))
    		return false;
    	if(this.TERM_validn == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_validn() != null)
    		return false;
    	} else if(!this.TERM_validn.equals(_OIVPM07_OIVPM07_meta.getTERM_validn()))
    		return false;
    	if(this.TERM_sosi == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_sosi() != null)
    		return false;
    	} else if(!this.TERM_sosi.equals(_OIVPM07_OIVPM07_meta.getTERM_sosi()))
    		return false;
    	if(this.TERM_ps == null) {
    	if(_OIVPM07_OIVPM07_meta.getTERM_ps() != null)
    		return false;
    	} else if(!this.TERM_ps.equals(_OIVPM07_OIVPM07_meta.getTERM_ps()))
    		return false;
    	if(this.TIME_length != _OIVPM07_OIVPM07_meta.getTIME_length()) return false;
    	if(this.TIME_color == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_color() != null)
    		return false;
    	} else if(!this.TIME_color.equals(_OIVPM07_OIVPM07_meta.getTIME_color()))
    		return false;
    	if(this.TIME_hilight == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_hilight() != null)
    		return false;
    	} else if(!this.TIME_hilight.equals(_OIVPM07_OIVPM07_meta.getTIME_hilight()))
    		return false;
    	if(this.TIME_outline == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_outline() != null)
    		return false;
    	} else if(!this.TIME_outline.equals(_OIVPM07_OIVPM07_meta.getTIME_outline()))
    		return false;
    	if(this.TIME_transp == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_transp() != null)
    		return false;
    	} else if(!this.TIME_transp.equals(_OIVPM07_OIVPM07_meta.getTIME_transp()))
    		return false;
    	if(this.TIME_validn == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_validn() != null)
    		return false;
    	} else if(!this.TIME_validn.equals(_OIVPM07_OIVPM07_meta.getTIME_validn()))
    		return false;
    	if(this.TIME_sosi == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_sosi() != null)
    		return false;
    	} else if(!this.TIME_sosi.equals(_OIVPM07_OIVPM07_meta.getTIME_sosi()))
    		return false;
    	if(this.TIME_ps == null) {
    	if(_OIVPM07_OIVPM07_meta.getTIME_ps() != null)
    		return false;
    	} else if(!this.TIME_ps.equals(_OIVPM07_OIVPM07_meta.getTIME_ps()))
    		return false;
    	if(this.ENDM_length != _OIVPM07_OIVPM07_meta.getENDM_length()) return false;
    	if(this.ENDM_color == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_color() != null)
    		return false;
    	} else if(!this.ENDM_color.equals(_OIVPM07_OIVPM07_meta.getENDM_color()))
    		return false;
    	if(this.ENDM_hilight == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_hilight() != null)
    		return false;
    	} else if(!this.ENDM_hilight.equals(_OIVPM07_OIVPM07_meta.getENDM_hilight()))
    		return false;
    	if(this.ENDM_outline == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_outline() != null)
    		return false;
    	} else if(!this.ENDM_outline.equals(_OIVPM07_OIVPM07_meta.getENDM_outline()))
    		return false;
    	if(this.ENDM_transp == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_transp() != null)
    		return false;
    	} else if(!this.ENDM_transp.equals(_OIVPM07_OIVPM07_meta.getENDM_transp()))
    		return false;
    	if(this.ENDM_validn == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_validn() != null)
    		return false;
    	} else if(!this.ENDM_validn.equals(_OIVPM07_OIVPM07_meta.getENDM_validn()))
    		return false;
    	if(this.ENDM_sosi == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_sosi() != null)
    		return false;
    	} else if(!this.ENDM_sosi.equals(_OIVPM07_OIVPM07_meta.getENDM_sosi()))
    		return false;
    	if(this.ENDM_ps == null) {
    	if(_OIVPM07_OIVPM07_meta.getENDM_ps() != null)
    		return false;
    	} else if(!this.ENDM_ps.equals(_OIVPM07_OIVPM07_meta.getENDM_ps()))
    		return false;
    	if(this.MSG_length != _OIVPM07_OIVPM07_meta.getMSG_length()) return false;
    	if(this.MSG_color == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_color() != null)
    		return false;
    	} else if(!this.MSG_color.equals(_OIVPM07_OIVPM07_meta.getMSG_color()))
    		return false;
    	if(this.MSG_hilight == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_hilight() != null)
    		return false;
    	} else if(!this.MSG_hilight.equals(_OIVPM07_OIVPM07_meta.getMSG_hilight()))
    		return false;
    	if(this.MSG_outline == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_outline() != null)
    		return false;
    	} else if(!this.MSG_outline.equals(_OIVPM07_OIVPM07_meta.getMSG_outline()))
    		return false;
    	if(this.MSG_transp == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_transp() != null)
    		return false;
    	} else if(!this.MSG_transp.equals(_OIVPM07_OIVPM07_meta.getMSG_transp()))
    		return false;
    	if(this.MSG_validn == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_validn() != null)
    		return false;
    	} else if(!this.MSG_validn.equals(_OIVPM07_OIVPM07_meta.getMSG_validn()))
    		return false;
    	if(this.MSG_sosi == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_sosi() != null)
    		return false;
    	} else if(!this.MSG_sosi.equals(_OIVPM07_OIVPM07_meta.getMSG_sosi()))
    		return false;
    	if(this.MSG_ps == null) {
    	if(_OIVPM07_OIVPM07_meta.getMSG_ps() != null)
    		return false;
    	} else if(!this.MSG_ps.equals(_OIVPM07_OIVPM07_meta.getMSG_ps()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + this.DATE_length;
    	result = prime * result + ((this.DATE_color == null) ? 0 : this.DATE_color.hashCode());
    	result = prime * result + ((this.DATE_hilight == null) ? 0 : this.DATE_hilight.hashCode());
    	result = prime * result + ((this.DATE_outline == null) ? 0 : this.DATE_outline.hashCode());
    	result = prime * result + ((this.DATE_transp == null) ? 0 : this.DATE_transp.hashCode());
    	result = prime * result + ((this.DATE_validn == null) ? 0 : this.DATE_validn.hashCode());
    	result = prime * result + ((this.DATE_sosi == null) ? 0 : this.DATE_sosi.hashCode());
    	result = prime * result + ((this.DATE_ps == null) ? 0 : this.DATE_ps.hashCode());
    	result = prime * result + this.TERM_length;
    	result = prime * result + ((this.TERM_color == null) ? 0 : this.TERM_color.hashCode());
    	result = prime * result + ((this.TERM_hilight == null) ? 0 : this.TERM_hilight.hashCode());
    	result = prime * result + ((this.TERM_outline == null) ? 0 : this.TERM_outline.hashCode());
    	result = prime * result + ((this.TERM_transp == null) ? 0 : this.TERM_transp.hashCode());
    	result = prime * result + ((this.TERM_validn == null) ? 0 : this.TERM_validn.hashCode());
    	result = prime * result + ((this.TERM_sosi == null) ? 0 : this.TERM_sosi.hashCode());
    	result = prime * result + ((this.TERM_ps == null) ? 0 : this.TERM_ps.hashCode());
    	result = prime * result + this.TIME_length;
    	result = prime * result + ((this.TIME_color == null) ? 0 : this.TIME_color.hashCode());
    	result = prime * result + ((this.TIME_hilight == null) ? 0 : this.TIME_hilight.hashCode());
    	result = prime * result + ((this.TIME_outline == null) ? 0 : this.TIME_outline.hashCode());
    	result = prime * result + ((this.TIME_transp == null) ? 0 : this.TIME_transp.hashCode());
    	result = prime * result + ((this.TIME_validn == null) ? 0 : this.TIME_validn.hashCode());
    	result = prime * result + ((this.TIME_sosi == null) ? 0 : this.TIME_sosi.hashCode());
    	result = prime * result + ((this.TIME_ps == null) ? 0 : this.TIME_ps.hashCode());
    	result = prime * result + this.ENDM_length;
    	result = prime * result + ((this.ENDM_color == null) ? 0 : this.ENDM_color.hashCode());
    	result = prime * result + ((this.ENDM_hilight == null) ? 0 : this.ENDM_hilight.hashCode());
    	result = prime * result + ((this.ENDM_outline == null) ? 0 : this.ENDM_outline.hashCode());
    	result = prime * result + ((this.ENDM_transp == null) ? 0 : this.ENDM_transp.hashCode());
    	result = prime * result + ((this.ENDM_validn == null) ? 0 : this.ENDM_validn.hashCode());
    	result = prime * result + ((this.ENDM_sosi == null) ? 0 : this.ENDM_sosi.hashCode());
    	result = prime * result + ((this.ENDM_ps == null) ? 0 : this.ENDM_ps.hashCode());
    	result = prime * result + this.MSG_length;
    	result = prime * result + ((this.MSG_color == null) ? 0 : this.MSG_color.hashCode());
    	result = prime * result + ((this.MSG_hilight == null) ? 0 : this.MSG_hilight.hashCode());
    	result = prime * result + ((this.MSG_outline == null) ? 0 : this.MSG_outline.hashCode());
    	result = prime * result + ((this.MSG_transp == null) ? 0 : this.MSG_transp.hashCode());
    	result = prime * result + ((this.MSG_validn == null) ? 0 : this.MSG_validn.hashCode());
    	result = prime * result + ((this.MSG_sosi == null) ? 0 : this.MSG_sosi.hashCode());
    	result = prime * result + ((this.MSG_ps == null) ? 0 : this.MSG_ps.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE_length = 0;
    	DATE_color = null;
    	DATE_hilight = null;
    	DATE_outline = null;
    	DATE_transp = null;
    	DATE_validn = null;
    	DATE_sosi = null;
    	DATE_ps = null;
    	TERM_length = 0;
    	TERM_color = null;
    	TERM_hilight = null;
    	TERM_outline = null;
    	TERM_transp = null;
    	TERM_validn = null;
    	TERM_sosi = null;
    	TERM_ps = null;
    	TIME_length = 0;
    	TIME_color = null;
    	TIME_hilight = null;
    	TIME_outline = null;
    	TIME_transp = null;
    	TIME_validn = null;
    	TIME_sosi = null;
    	TIME_ps = null;
    	ENDM_length = 0;
    	ENDM_color = null;
    	ENDM_hilight = null;
    	ENDM_outline = null;
    	ENDM_transp = null;
    	ENDM_validn = null;
    	ENDM_sosi = null;
    	ENDM_ps = null;
    	MSG_length = 0;
    	MSG_color = null;
    	MSG_hilight = null;
    	MSG_outline = null;
    	MSG_transp = null;
    	MSG_validn = null;
    	MSG_sosi = null;
    	MSG_ps = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

