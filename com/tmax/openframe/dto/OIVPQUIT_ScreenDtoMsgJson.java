package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPQUIT_ScreenDtoMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPQUIT_ScreenDto _OIVPQUIT_ScreenDto = (com.tmax.openframe.dto.OIVPQUIT_ScreenDto)obj;
    	
    	if(_OIVPQUIT_ScreenDto == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPQUIT_ScreenDto, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPQUIT_ScreenDto _OIVPQUIT_ScreenDto, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM07_OIVPM07_dataMsgJson __OIVPM07_OIVPM07_data = new com.tmax.openframe.dto.OIVPM07_OIVPM07_dataMsgJson();	
    	writer.name("OIVPM07_OIVPM07_data");
    	if(_OIVPQUIT_ScreenDto.getOIVPM07_OIVPM07_data() != null) {
    	writer.beginObject();
    	__OIVPM07_OIVPM07_data.marshal((com.tmax.openframe.dto.OIVPM07_OIVPM07_data)_OIVPQUIT_ScreenDto.getOIVPM07_OIVPM07_data(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.OIVPM07_OIVPM07_metaMsgJson __OIVPM07_OIVPM07_meta = new com.tmax.openframe.dto.OIVPM07_OIVPM07_metaMsgJson();	
    	writer.name("OIVPM07_OIVPM07_meta");
    	if(_OIVPQUIT_ScreenDto.getOIVPM07_OIVPM07_meta() != null) {
    	writer.beginObject();
    	__OIVPM07_OIVPM07_meta.marshal((com.tmax.openframe.dto.OIVPM07_OIVPM07_meta)_OIVPQUIT_ScreenDto.getOIVPM07_OIVPM07_meta(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapName"); 
    	if (_OIVPQUIT_ScreenDto.getMapName() != null) {
    		writer.value(_OIVPQUIT_ScreenDto.getMapName());
    		writer.value(_OIVPQUIT_ScreenDto.getMapName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapSetName"); 
    	if (_OIVPQUIT_ScreenDto.getMapSetName() != null) {
    		writer.value(_OIVPQUIT_ScreenDto.getMapSetName());
    		writer.value(_OIVPQUIT_ScreenDto.getMapSetName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("text"); 
    	if (_OIVPQUIT_ScreenDto.getText() != null) {
    		writer.value(_OIVPQUIT_ScreenDto.getText());
    		writer.value(_OIVPQUIT_ScreenDto.getText());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPQUIT_ScreenDto _OIVPQUIT_ScreenDto = new com.tmax.openframe.dto.OIVPQUIT_ScreenDto();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPQUIT_ScreenDto();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPQUIT_ScreenDto = (com.tmax.openframe.dto.OIVPQUIT_ScreenDto)unmarshal( jr,  _OIVPQUIT_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPQUIT_ScreenDto;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPQUIT_ScreenDto _OIVPQUIT_ScreenDto = (com.tmax.openframe.dto.OIVPQUIT_ScreenDto) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPQUIT_ScreenDto();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPQUIT_ScreenDto = (com.tmax.openframe.dto.OIVPQUIT_ScreenDto)unmarshal( jr,  _OIVPQUIT_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPQUIT_ScreenDto;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPQUIT_ScreenDto dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPQUIT_ScreenDto dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "OIVPM07_OIVPM07_data" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM07_OIVPM07_dataMsgJson __OIVPM07_OIVPM07_data = new com.tmax.openframe.dto.OIVPM07_OIVPM07_dataMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM07_OIVPM07_data ___OIVPM07_OIVPM07_data = new com.tmax.openframe.dto.OIVPM07_OIVPM07_data();
    				reader.beginObject();
    				dto.setOIVPM07_OIVPM07_data((com.tmax.openframe.dto.OIVPM07_OIVPM07_data)__OIVPM07_OIVPM07_data.unmarshal( reader, ___OIVPM07_OIVPM07_data ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "OIVPM07_OIVPM07_meta" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM07_OIVPM07_metaMsgJson __OIVPM07_OIVPM07_meta = new com.tmax.openframe.dto.OIVPM07_OIVPM07_metaMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM07_OIVPM07_meta ___OIVPM07_OIVPM07_meta = new com.tmax.openframe.dto.OIVPM07_OIVPM07_meta();
    				reader.beginObject();
    				dto.setOIVPM07_OIVPM07_meta((com.tmax.openframe.dto.OIVPM07_OIVPM07_meta)__OIVPM07_OIVPM07_meta.unmarshal( reader, ___OIVPM07_OIVPM07_meta ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "mapName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "mapSetName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapSetName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "text" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setText( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

