package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPINSR_InputDto extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPINSR_InputDto";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPINSR_InputDto() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : id
     * Comments    : 
     */	
    private String id = null;
    
    private transient boolean id_nullable = true;
    
    public boolean isNullableId() {
    	return this.id_nullable;
    }
    
    private transient boolean id_invalidation = false;
    	
    public void setInvalidationId(boolean invalidation) { 
    	this.id_invalidation = invalidation;
    }
    	
    public boolean isInvalidationId() {
    	return this.id_invalidation;
    }
    	
    private transient boolean id_modified = false;
    
    public boolean isModifiedId() {
    	return this.id_modified;
    }
    	
    public void unModifiedId() {
    	this.id_modified = false;
    }
    public FieldProperty getIdFieldProperty() {
    	return fieldPropertyMap.get("id");
    }
    
    public String getId() {
    	return id;
    }	
    public String getNvlId() {
    	if(getId() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getId();
    	}
    }
    public void setId(String id) {
    	if(id == null) {
    		this.id = null;
    	} else {
    		this.id = id;
    	}
    	this.id_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : tabId
     * Comments    : 
     */	
    private String tabId = null;
    
    private transient boolean tabId_nullable = true;
    
    public boolean isNullableTabId() {
    	return this.tabId_nullable;
    }
    
    private transient boolean tabId_invalidation = false;
    	
    public void setInvalidationTabId(boolean invalidation) { 
    	this.tabId_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTabId() {
    	return this.tabId_invalidation;
    }
    	
    private transient boolean tabId_modified = false;
    
    public boolean isModifiedTabId() {
    	return this.tabId_modified;
    }
    	
    public void unModifiedTabId() {
    	this.tabId_modified = false;
    }
    public FieldProperty getTabIdFieldProperty() {
    	return fieldPropertyMap.get("tabId");
    }
    
    public String getTabId() {
    	return tabId;
    }	
    public String getNvlTabId() {
    	if(getTabId() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTabId();
    	}
    }
    public void setTabId(String tabId) {
    	if(tabId == null) {
    		this.tabId = null;
    	} else {
    		this.tabId = tabId;
    	}
    	this.tabId_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : termId
     * Comments    : 
     */	
    private String termId = null;
    
    private transient boolean termId_nullable = true;
    
    public boolean isNullableTermId() {
    	return this.termId_nullable;
    }
    
    private transient boolean termId_invalidation = false;
    	
    public void setInvalidationTermId(boolean invalidation) { 
    	this.termId_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTermId() {
    	return this.termId_invalidation;
    }
    	
    private transient boolean termId_modified = false;
    
    public boolean isModifiedTermId() {
    	return this.termId_modified;
    }
    	
    public void unModifiedTermId() {
    	this.termId_modified = false;
    }
    public FieldProperty getTermIdFieldProperty() {
    	return fieldPropertyMap.get("termId");
    }
    
    public String getTermId() {
    	return termId;
    }	
    public String getNvlTermId() {
    	if(getTermId() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTermId();
    	}
    }
    public void setTermId(String termId) {
    	if(termId == null) {
    		this.termId = null;
    	} else {
    		this.termId = termId;
    	}
    	this.termId_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : systemDto
     * Comments    : 
     */	
    private com.tmax.openframe.dto.SystemDto systemDto = null;
    
    private transient boolean systemDto_nullable = true;
    
    public boolean isNullableSystemDto() {
    	return this.systemDto_nullable;
    }
    
    private transient boolean systemDto_invalidation = false;
    	
    public void setInvalidationSystemDto(boolean invalidation) { 
    	this.systemDto_invalidation = invalidation;
    }
    	
    public boolean isInvalidationSystemDto() {
    	return this.systemDto_invalidation;
    }
    	
    private transient boolean systemDto_modified = false;
    
    public boolean isModifiedSystemDto() {
    	if(this.systemDto_modified) return true;
    	if(systemDto.isModified()) return true;
    	return false;
    }
    	
    public void unModifiedSystemDto() {
    	this.systemDto_modified = false;
    }
    public FieldProperty getSystemDtoFieldProperty() {
    	return fieldPropertyMap.get("systemDto");
    }
    
    public com.tmax.openframe.dto.SystemDto getSystemDto() {
    	return systemDto;
    }	
     	public void setSystemDto(com.tmax.openframe.dto.SystemDto systemDto) {
    	if(systemDto == null) {
    		this.systemDto = null;
    	} else {
    		this.systemDto = systemDto;
    	}
    	this.systemDto_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : screenDto
     * Comments    : 
     */	
    private com.tmax.openframe.dto.OIVPINSR_ScreenDto screenDto = null;
    
    private transient boolean screenDto_nullable = true;
    
    public boolean isNullableScreenDto() {
    	return this.screenDto_nullable;
    }
    
    private transient boolean screenDto_invalidation = false;
    	
    public void setInvalidationScreenDto(boolean invalidation) { 
    	this.screenDto_invalidation = invalidation;
    }
    	
    public boolean isInvalidationScreenDto() {
    	return this.screenDto_invalidation;
    }
    	
    private transient boolean screenDto_modified = false;
    
    public boolean isModifiedScreenDto() {
    	if(this.screenDto_modified) return true;
    	if(screenDto.isModified()) return true;
    	return false;
    }
    	
    public void unModifiedScreenDto() {
    	this.screenDto_modified = false;
    }
    public FieldProperty getScreenDtoFieldProperty() {
    	return fieldPropertyMap.get("screenDto");
    }
    
    public com.tmax.openframe.dto.OIVPINSR_ScreenDto getScreenDto() {
    	return screenDto;
    }	
     	public void setScreenDto(com.tmax.openframe.dto.OIVPINSR_ScreenDto screenDto) {
    	if(screenDto == null) {
    		this.screenDto = null;
    	} else {
    		this.screenDto = screenDto;
    	}
    	this.screenDto_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.id_modified = false;
    	this.tabId_modified = false;
    	this.termId_modified = false;
    	this.systemDto_modified = false;
    	if(this.systemDto != null)
    		this.systemDto.clearAllIsModified();
    	this.screenDto_modified = false;
    	if(this.screenDto != null)
    		this.screenDto.clearAllIsModified();
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.id_modified == true)
    		modifiedFields.add("id");
    	if(this.tabId_modified == true)
    		modifiedFields.add("tabId");
    	if(this.termId_modified == true)
    		modifiedFields.add("termId");
    	if(this.systemDto_modified == true)
    		modifiedFields.add("systemDto");
    	if(this.screenDto_modified == true)
    		modifiedFields.add("screenDto");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPINSR_InputDto copyObj = new OIVPINSR_InputDto();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPINSR_InputDto) {
    	if(this == _oIVPINSR_InputDto) return;
    	OIVPINSR_InputDto __oIVPINSR_InputDto = (OIVPINSR_InputDto) _oIVPINSR_InputDto;
    	this.setId(__oIVPINSR_InputDto.getId());
    	this.setTabId(__oIVPINSR_InputDto.getTabId());
    	this.setTermId(__oIVPINSR_InputDto.getTermId());
    	com.tmax.openframe.dto.SystemDto _value3 = __oIVPINSR_InputDto.getSystemDto();
    		if(_value3 == null) {
    			this.setSystemDto(null);
    		} else {
    			this.setSystemDto((com.tmax.openframe.dto.SystemDto)_value3.clone());
    		}
    	com.tmax.openframe.dto.OIVPINSR_ScreenDto _value4 = __oIVPINSR_InputDto.getScreenDto();
    		if(_value4 == null) {
    			this.setScreenDto(null);
    		} else {
    			this.setScreenDto((com.tmax.openframe.dto.OIVPINSR_ScreenDto)_value4.clone());
    		}
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("id : ").append(id).append("\n");	
    	buffer.append("tabId : ").append(tabId).append("\n");	
    	buffer.append("termId : ").append(termId).append("\n");	
    	buffer.append("systemDto : ").append(systemDto).append("\n");	
    	buffer.append("screenDto : ").append(screenDto).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(5);
    	fieldPropertyMap.put("id", FieldProperty.builder()
    	              .setPhysicalName("id")
    	              .setLogicalName("id")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("tabId", FieldProperty.builder()
    	              .setPhysicalName("tabId")
    	              .setLogicalName("tabId")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(128)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("termId", FieldProperty.builder()
    	              .setPhysicalName("termId")
    	              .setLogicalName("termId")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(128)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("systemDto", FieldProperty.builder()
    	              .setPhysicalName("systemDto")
    	              .setLogicalName("systemDto")
    	              .setType(FieldProperty.TYPE_ABSTRACT_INCLUDE)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("com.tmax.openframe.dto.SystemDto")
    	              .build());
    	fieldPropertyMap.put("screenDto", FieldProperty.builder()
    	              .setPhysicalName("screenDto")
    	              .setLogicalName("screenDto")
    	              .setType(FieldProperty.TYPE_ABSTRACT_INCLUDE)
    	              .setLength(40)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("com.tmax.openframe.dto.OIVPINSR_ScreenDto")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "id" : return getId();
    		case "tabId" : return getTabId();
    		case "termId" : return getTermId();
    		case "systemDto" : return getSystemDto();
    		case "screenDto" : return getScreenDto();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "id" : setId((String)arg); break;
    		case "tabId" : setTabId((String)arg); break;
    		case "termId" : setTermId((String)arg); break;
    		case "systemDto" : setSystemDto((com.tmax.openframe.dto.SystemDto)arg); break;
    		case "screenDto" : setScreenDto((com.tmax.openframe.dto.OIVPINSR_ScreenDto)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPINSR_InputDto _OIVPINSR_InputDto = (OIVPINSR_InputDto) obj;
    	if(this.id == null) {
    	if(_OIVPINSR_InputDto.getId() != null)
    		return false;
    	} else if(!this.id.equals(_OIVPINSR_InputDto.getId()))
    		return false;
    	if(this.tabId == null) {
    	if(_OIVPINSR_InputDto.getTabId() != null)
    		return false;
    	} else if(!this.tabId.equals(_OIVPINSR_InputDto.getTabId()))
    		return false;
    	if(this.termId == null) {
    	if(_OIVPINSR_InputDto.getTermId() != null)
    		return false;
    	} else if(!this.termId.equals(_OIVPINSR_InputDto.getTermId()))
    		return false;
    	if(this.systemDto == null) {
    	if(_OIVPINSR_InputDto.getSystemDto() != null)
    		return false;
    	} else if(!this.systemDto.equals(_OIVPINSR_InputDto.getSystemDto()))
    		return false;
    	if(this.screenDto == null) {
    	if(_OIVPINSR_InputDto.getScreenDto() != null)
    		return false;
    	} else if(!this.screenDto.equals(_OIVPINSR_InputDto.getScreenDto()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
    	result = prime * result + ((this.tabId == null) ? 0 : this.tabId.hashCode());
    	result = prime * result + ((this.termId == null) ? 0 : this.termId.hashCode());
    	result = prime * result + ((this.systemDto == null) ? 0 : this.systemDto.hashCode());
    	result = prime * result + ((this.screenDto == null) ? 0 : this.screenDto.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	id = null;
    	tabId = null;
    	termId = null;
    	systemDto = null;
    	screenDto = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

