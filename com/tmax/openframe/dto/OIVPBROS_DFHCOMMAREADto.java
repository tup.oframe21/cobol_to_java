package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPBROS_DFHCOMMAREADto extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPBROS_DFHCOMMAREADto";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPBROS_DFHCOMMAREADto() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DFHCOMMAREA
     * Comments    : 
     */	
    private String DFHCOMMAREA = null;
    
    private transient boolean DFHCOMMAREA_nullable = true;
    
    public boolean isNullableDFHCOMMAREA() {
    	return this.DFHCOMMAREA_nullable;
    }
    
    private transient boolean DFHCOMMAREA_invalidation = false;
    	
    public void setInvalidationDFHCOMMAREA(boolean invalidation) { 
    	this.DFHCOMMAREA_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDFHCOMMAREA() {
    	return this.DFHCOMMAREA_invalidation;
    }
    	
    private transient boolean DFHCOMMAREA_modified = false;
    
    public boolean isModifiedDFHCOMMAREA() {
    	return this.DFHCOMMAREA_modified;
    }
    	
    public void unModifiedDFHCOMMAREA() {
    	this.DFHCOMMAREA_modified = false;
    }
    public FieldProperty getDFHCOMMAREAFieldProperty() {
    	return fieldPropertyMap.get("DFHCOMMAREA");
    }
    
    public String getDFHCOMMAREA() {
    	return DFHCOMMAREA;
    }	
    public String getNvlDFHCOMMAREA() {
    	if(getDFHCOMMAREA() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDFHCOMMAREA();
    	}
    }
    public void setDFHCOMMAREA(String DFHCOMMAREA) {
    	if(DFHCOMMAREA == null) {
    		this.DFHCOMMAREA = null;
    	} else {
    		this.DFHCOMMAREA = DFHCOMMAREA;
    	}
    	this.DFHCOMMAREA_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DFHCOMMAREA_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DFHCOMMAREA_modified == true)
    		modifiedFields.add("DFHCOMMAREA");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPBROS_DFHCOMMAREADto copyObj = new OIVPBROS_DFHCOMMAREADto();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPBROS_DFHCOMMAREADto) {
    	if(this == _oIVPBROS_DFHCOMMAREADto) return;
    	OIVPBROS_DFHCOMMAREADto __oIVPBROS_DFHCOMMAREADto = (OIVPBROS_DFHCOMMAREADto) _oIVPBROS_DFHCOMMAREADto;
    	this.setDFHCOMMAREA(__oIVPBROS_DFHCOMMAREADto.getDFHCOMMAREA());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DFHCOMMAREA : ").append(DFHCOMMAREA).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(1);
    	fieldPropertyMap.put("DFHCOMMAREA", FieldProperty.builder()
    	              .setPhysicalName("DFHCOMMAREA")
    	              .setLogicalName("DFHCOMMAREA")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(1)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DFHCOMMAREA" : return getDFHCOMMAREA();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DFHCOMMAREA" : setDFHCOMMAREA((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPBROS_DFHCOMMAREADto _OIVPBROS_DFHCOMMAREADto = (OIVPBROS_DFHCOMMAREADto) obj;
    	if(this.DFHCOMMAREA == null) {
    	if(_OIVPBROS_DFHCOMMAREADto.getDFHCOMMAREA() != null)
    		return false;
    	} else if(!this.DFHCOMMAREA.equals(_OIVPBROS_DFHCOMMAREADto.getDFHCOMMAREA()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DFHCOMMAREA == null) ? 0 : this.DFHCOMMAREA.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DFHCOMMAREA = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

