package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM07_OIVPM07_data extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM07_OIVPM07_data";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM07_OIVPM07_data() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE
     * Comments    : 
     */	
    private String DATE = null;
    
    private transient boolean DATE_nullable = true;
    
    public boolean isNullableDATE() {
    	return this.DATE_nullable;
    }
    
    private transient boolean DATE_invalidation = false;
    	
    public void setInvalidationDATE(boolean invalidation) { 
    	this.DATE_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE() {
    	return this.DATE_invalidation;
    }
    	
    private transient boolean DATE_modified = false;
    
    public boolean isModifiedDATE() {
    	return this.DATE_modified;
    }
    	
    public void unModifiedDATE() {
    	this.DATE_modified = false;
    }
    public FieldProperty getDATEFieldProperty() {
    	return fieldPropertyMap.get("DATE");
    }
    
    public String getDATE() {
    	return DATE;
    }	
    public String getNvlDATE() {
    	if(getDATE() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE();
    	}
    }
    public void setDATE(String DATE) {
    	if(DATE == null) {
    		this.DATE = null;
    	} else {
    		this.DATE = DATE;
    	}
    	this.DATE_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM
     * Comments    : 
     */	
    private String TERM = null;
    
    private transient boolean TERM_nullable = true;
    
    public boolean isNullableTERM() {
    	return this.TERM_nullable;
    }
    
    private transient boolean TERM_invalidation = false;
    	
    public void setInvalidationTERM(boolean invalidation) { 
    	this.TERM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM() {
    	return this.TERM_invalidation;
    }
    	
    private transient boolean TERM_modified = false;
    
    public boolean isModifiedTERM() {
    	return this.TERM_modified;
    }
    	
    public void unModifiedTERM() {
    	this.TERM_modified = false;
    }
    public FieldProperty getTERMFieldProperty() {
    	return fieldPropertyMap.get("TERM");
    }
    
    public String getTERM() {
    	return TERM;
    }	
    public String getNvlTERM() {
    	if(getTERM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM();
    	}
    }
    public void setTERM(String TERM) {
    	if(TERM == null) {
    		this.TERM = null;
    	} else {
    		this.TERM = TERM;
    	}
    	this.TERM_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME
     * Comments    : 
     */	
    private String TIME = null;
    
    private transient boolean TIME_nullable = true;
    
    public boolean isNullableTIME() {
    	return this.TIME_nullable;
    }
    
    private transient boolean TIME_invalidation = false;
    	
    public void setInvalidationTIME(boolean invalidation) { 
    	this.TIME_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME() {
    	return this.TIME_invalidation;
    }
    	
    private transient boolean TIME_modified = false;
    
    public boolean isModifiedTIME() {
    	return this.TIME_modified;
    }
    	
    public void unModifiedTIME() {
    	this.TIME_modified = false;
    }
    public FieldProperty getTIMEFieldProperty() {
    	return fieldPropertyMap.get("TIME");
    }
    
    public String getTIME() {
    	return TIME;
    }	
    public String getNvlTIME() {
    	if(getTIME() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME();
    	}
    }
    public void setTIME(String TIME) {
    	if(TIME == null) {
    		this.TIME = null;
    	} else {
    		this.TIME = TIME;
    	}
    	this.TIME_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : ENDM
     * Comments    : 
     */	
    private String ENDM = null;
    
    private transient boolean ENDM_nullable = true;
    
    public boolean isNullableENDM() {
    	return this.ENDM_nullable;
    }
    
    private transient boolean ENDM_invalidation = false;
    	
    public void setInvalidationENDM(boolean invalidation) { 
    	this.ENDM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationENDM() {
    	return this.ENDM_invalidation;
    }
    	
    private transient boolean ENDM_modified = false;
    
    public boolean isModifiedENDM() {
    	return this.ENDM_modified;
    }
    	
    public void unModifiedENDM() {
    	this.ENDM_modified = false;
    }
    public FieldProperty getENDMFieldProperty() {
    	return fieldPropertyMap.get("ENDM");
    }
    
    public String getENDM() {
    	return ENDM;
    }	
    public String getNvlENDM() {
    	if(getENDM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getENDM();
    	}
    }
    public void setENDM(String ENDM) {
    	if(ENDM == null) {
    		this.ENDM = null;
    	} else {
    		this.ENDM = ENDM;
    	}
    	this.ENDM_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG
     * Comments    : 
     */	
    private String MSG = null;
    
    private transient boolean MSG_nullable = true;
    
    public boolean isNullableMSG() {
    	return this.MSG_nullable;
    }
    
    private transient boolean MSG_invalidation = false;
    	
    public void setInvalidationMSG(boolean invalidation) { 
    	this.MSG_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG() {
    	return this.MSG_invalidation;
    }
    	
    private transient boolean MSG_modified = false;
    
    public boolean isModifiedMSG() {
    	return this.MSG_modified;
    }
    	
    public void unModifiedMSG() {
    	this.MSG_modified = false;
    }
    public FieldProperty getMSGFieldProperty() {
    	return fieldPropertyMap.get("MSG");
    }
    
    public String getMSG() {
    	return MSG;
    }	
    public String getNvlMSG() {
    	if(getMSG() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG();
    	}
    }
    public void setMSG(String MSG) {
    	if(MSG == null) {
    		this.MSG = null;
    	} else {
    		this.MSG = MSG;
    	}
    	this.MSG_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_modified = false;
    	this.TERM_modified = false;
    	this.TIME_modified = false;
    	this.ENDM_modified = false;
    	this.MSG_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_modified == true)
    		modifiedFields.add("DATE");
    	if(this.TERM_modified == true)
    		modifiedFields.add("TERM");
    	if(this.TIME_modified == true)
    		modifiedFields.add("TIME");
    	if(this.ENDM_modified == true)
    		modifiedFields.add("ENDM");
    	if(this.MSG_modified == true)
    		modifiedFields.add("MSG");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM07_OIVPM07_data copyObj = new OIVPM07_OIVPM07_data();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM07_OIVPM07_data) {
    	if(this == _oIVPM07_OIVPM07_data) return;
    	OIVPM07_OIVPM07_data __oIVPM07_OIVPM07_data = (OIVPM07_OIVPM07_data) _oIVPM07_OIVPM07_data;
    	this.setDATE(__oIVPM07_OIVPM07_data.getDATE());
    	this.setTERM(__oIVPM07_OIVPM07_data.getTERM());
    	this.setTIME(__oIVPM07_OIVPM07_data.getTIME());
    	this.setENDM(__oIVPM07_OIVPM07_data.getENDM());
    	this.setMSG(__oIVPM07_OIVPM07_data.getMSG());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE : ").append(DATE).append("\n");	
    	buffer.append("TERM : ").append(TERM).append("\n");	
    	buffer.append("TIME : ").append(TIME).append("\n");	
    	buffer.append("ENDM : ").append(ENDM).append("\n");	
    	buffer.append("MSG : ").append(MSG).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(5);
    	fieldPropertyMap.put("DATE", FieldProperty.builder()
    	              .setPhysicalName("DATE")
    	              .setLogicalName("DATE")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM", FieldProperty.builder()
    	              .setPhysicalName("TERM")
    	              .setLogicalName("TERM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(4)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME", FieldProperty.builder()
    	              .setPhysicalName("TIME")
    	              .setLogicalName("TIME")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("ENDM", FieldProperty.builder()
    	              .setPhysicalName("ENDM")
    	              .setLogicalName("ENDM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(36)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG", FieldProperty.builder()
    	              .setPhysicalName("MSG")
    	              .setLogicalName("MSG")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(76)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : return getDATE();
    		case "TERM" : return getTERM();
    		case "TIME" : return getTIME();
    		case "ENDM" : return getENDM();
    		case "MSG" : return getMSG();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE" : setDATE((String)arg); break;
    		case "TERM" : setTERM((String)arg); break;
    		case "TIME" : setTIME((String)arg); break;
    		case "ENDM" : setENDM((String)arg); break;
    		case "MSG" : setMSG((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM07_OIVPM07_data _OIVPM07_OIVPM07_data = (OIVPM07_OIVPM07_data) obj;
    	if(this.DATE == null) {
    	if(_OIVPM07_OIVPM07_data.getDATE() != null)
    		return false;
    	} else if(!this.DATE.equals(_OIVPM07_OIVPM07_data.getDATE()))
    		return false;
    	if(this.TERM == null) {
    	if(_OIVPM07_OIVPM07_data.getTERM() != null)
    		return false;
    	} else if(!this.TERM.equals(_OIVPM07_OIVPM07_data.getTERM()))
    		return false;
    	if(this.TIME == null) {
    	if(_OIVPM07_OIVPM07_data.getTIME() != null)
    		return false;
    	} else if(!this.TIME.equals(_OIVPM07_OIVPM07_data.getTIME()))
    		return false;
    	if(this.ENDM == null) {
    	if(_OIVPM07_OIVPM07_data.getENDM() != null)
    		return false;
    	} else if(!this.ENDM.equals(_OIVPM07_OIVPM07_data.getENDM()))
    		return false;
    	if(this.MSG == null) {
    	if(_OIVPM07_OIVPM07_data.getMSG() != null)
    		return false;
    	} else if(!this.MSG.equals(_OIVPM07_OIVPM07_data.getMSG()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DATE == null) ? 0 : this.DATE.hashCode());
    	result = prime * result + ((this.TERM == null) ? 0 : this.TERM.hashCode());
    	result = prime * result + ((this.TIME == null) ? 0 : this.TIME.hashCode());
    	result = prime * result + ((this.ENDM == null) ? 0 : this.ENDM.hashCode());
    	result = prime * result + ((this.MSG == null) ? 0 : this.MSG.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE = null;
    	TERM = null;
    	TIME = null;
    	ENDM = null;
    	MSG = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

