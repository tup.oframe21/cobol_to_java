package com.tmax.openframe.dto;

import com.tmax.promapper.engine.base.Message;
import com.tmax.proobject.model.dataobject.DataObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.w3c.dom.Node;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.gson.stream.JsonToken;




import java.lang.IllegalArgumentException;
import java.lang.NullPointerException;
import java.io.UnsupportedEncodingException;
import java.io.IOException;
import com.google.gson.stream.MalformedJsonException;

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.message.MessageGenerator",
	comments = "https://www.tmaxsoft.com"
)
public class OIVPDELT_ScreenDtoMsgJson extends Message
{
    public byte[] marshal(DataObject obj) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    com.tmax.openframe.dto.OIVPDELT_ScreenDto _OIVPDELT_ScreenDto = (com.tmax.openframe.dto.OIVPDELT_ScreenDto)obj;
    	
    	if(_OIVPDELT_ScreenDto == null)
    		return null;
    	
    	BufferedWriter bw = null;
    	JsonWriter jw = null;
    	
    	try{
    
    		ByteArrayOutputStream out = new ByteArrayOutputStream(); 
    		bw = new BufferedWriter( new OutputStreamWriter( out , this.encoding ) );        
    		jw = new JsonWriter( bw );
    		jw.beginObject();
    
    		marshal( _OIVPDELT_ScreenDto, jw);
    		
    		jw.endObject();
    		jw.close();
    		return out.toByteArray();
       		    	    		
    	} finally{
    		try {
    			if(jw != null) jw.close();
    		} finally {
    			if(bw != null) bw.close();
    		}
    	}
    }
    
    
    public void marshal(com.tmax.openframe.dto.OIVPDELT_ScreenDto _OIVPDELT_ScreenDto, JsonWriter writer )throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPM06_OIVPM06_dataMsgJson __OIVPM06_OIVPM06_data = new com.tmax.openframe.dto.OIVPM06_OIVPM06_dataMsgJson();	
    	writer.name("OIVPM06_OIVPM06_data");
    	if(_OIVPDELT_ScreenDto.getOIVPM06_OIVPM06_data() != null) {
    	writer.beginObject();
    	__OIVPM06_OIVPM06_data.marshal((com.tmax.openframe.dto.OIVPM06_OIVPM06_data)_OIVPDELT_ScreenDto.getOIVPM06_OIVPM06_data(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	com.tmax.openframe.dto.OIVPM06_OIVPM06_metaMsgJson __OIVPM06_OIVPM06_meta = new com.tmax.openframe.dto.OIVPM06_OIVPM06_metaMsgJson();	
    	writer.name("OIVPM06_OIVPM06_meta");
    	if(_OIVPDELT_ScreenDto.getOIVPM06_OIVPM06_meta() != null) {
    	writer.beginObject();
    	__OIVPM06_OIVPM06_meta.marshal((com.tmax.openframe.dto.OIVPM06_OIVPM06_meta)_OIVPDELT_ScreenDto.getOIVPM06_OIVPM06_meta(), writer);
    	writer.endObject();
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapName"); 
    	if (_OIVPDELT_ScreenDto.getMapName() != null) {
    		writer.value(_OIVPDELT_ScreenDto.getMapName());
    		writer.value(_OIVPDELT_ScreenDto.getMapName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("mapSetName"); 
    	if (_OIVPDELT_ScreenDto.getMapSetName() != null) {
    		writer.value(_OIVPDELT_ScreenDto.getMapSetName());
    		writer.value(_OIVPDELT_ScreenDto.getMapSetName());
    	} else {
    		writer.nullValue();
    	}
    	writer.name("text"); 
    	if (_OIVPDELT_ScreenDto.getText() != null) {
    		writer.value(_OIVPDELT_ScreenDto.getText());
    		writer.value(_OIVPDELT_ScreenDto.getText());
    	} else {
    		writer.nullValue();
    	}
    }
    
    /**
     * do not use
     */
    public void marshal(DataObject dataobject, Node node) throws IOException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException  {
    }
    
    public String removeNullChar(String charString) {
    	if( charString == null )
        	return "";
        
    	StringBuffer sb = new StringBuffer();
    	for (int i = 0 ; i<charString.length(); i++) {
    		if(charString.charAt(i) == (char)0) {
    			sb.append("");
    		} else {
    			sb.append(charString.charAt(i));
    		}
    	}
    	return sb.toString();
    }
    
    public DataObject unmarshal(byte[] bytes, int i) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPDELT_ScreenDto _OIVPDELT_ScreenDto = new com.tmax.openframe.dto.OIVPDELT_ScreenDto();
    	BufferedReader reader = null;
    	JsonReader jr = null;
    
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPDELT_ScreenDto();
    
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPDELT_ScreenDto = (com.tmax.openframe.dto.OIVPDELT_ScreenDto)unmarshal( jr,  _OIVPDELT_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	return _OIVPDELT_ScreenDto;
    }
    
    public DataObject unmarshal(byte[] bytes, DataObject dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	com.tmax.openframe.dto.OIVPDELT_ScreenDto _OIVPDELT_ScreenDto = (com.tmax.openframe.dto.OIVPDELT_ScreenDto) dto;
    	BufferedReader reader = null;
    	JsonReader jr = null;
    	
    	if( bytes.length <= 0)
    		return new com.tmax.openframe.dto.OIVPDELT_ScreenDto();
    	
    	try{
    		reader = new BufferedReader( new InputStreamReader( new ByteArrayInputStream(bytes), this.encoding));		       
    		jr = new JsonReader( reader );                
    		jr.beginObject();
    
    		_OIVPDELT_ScreenDto = (com.tmax.openframe.dto.OIVPDELT_ScreenDto)unmarshal( jr,  _OIVPDELT_ScreenDto);
    
    		jr.endObject();
    		jr.close();
    			
    	}finally{
    		if( jr != null ) jr.close();
    		if( reader != null ) reader.close();
    	}
    	                       
        return _OIVPDELT_ScreenDto;
    }
    
    public DataObject unmarshal(JsonReader reader, com.tmax.openframe.dto.OIVPDELT_ScreenDto dto) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	while( reader.hasNext() ){
    		String name = reader.nextName();			
    		setField(dto, reader, name);
    	}
    	 
    	dto.clearAllIsModified();
    	 
    	return dto;
    }
    	 
    protected void setField(com.tmax.openframe.dto.OIVPDELT_ScreenDto dto, JsonReader reader, String name) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	
    	switch(name) {
    		case "OIVPM06_OIVPM06_data" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM06_OIVPM06_dataMsgJson __OIVPM06_OIVPM06_data = new com.tmax.openframe.dto.OIVPM06_OIVPM06_dataMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM06_OIVPM06_data ___OIVPM06_OIVPM06_data = new com.tmax.openframe.dto.OIVPM06_OIVPM06_data();
    				reader.beginObject();
    				dto.setOIVPM06_OIVPM06_data((com.tmax.openframe.dto.OIVPM06_OIVPM06_data)__OIVPM06_OIVPM06_data.unmarshal( reader, ___OIVPM06_OIVPM06_data ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "OIVPM06_OIVPM06_meta" :
    		{	
    			if(reader.peek() == JsonToken.NULL) {
    				reader.nextNull();
    			} else {
    				com.tmax.openframe.dto.OIVPM06_OIVPM06_metaMsgJson __OIVPM06_OIVPM06_meta = new com.tmax.openframe.dto.OIVPM06_OIVPM06_metaMsgJson();
    		
    				com.tmax.openframe.dto.OIVPM06_OIVPM06_meta ___OIVPM06_OIVPM06_meta = new com.tmax.openframe.dto.OIVPM06_OIVPM06_meta();
    				reader.beginObject();
    				dto.setOIVPM06_OIVPM06_meta((com.tmax.openframe.dto.OIVPM06_OIVPM06_meta)__OIVPM06_OIVPM06_meta.unmarshal( reader, ___OIVPM06_OIVPM06_meta ));
    				reader.endObject();
    			}
    			break;
    		}
    		case "mapName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "mapSetName" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setMapSetName( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		case "text" :
    		{	
    			if(reader.peek() != JsonToken.NULL) {
    				dto.setText( reader.nextString());
    			} else {
    				reader.nextNull();
    			}
    			break;
    		}	
    		default :
    		reader.skipValue();
    		break;
    	}
    }
    
    /**
      * do not use
      */
    public int unmarshal(byte[] bytes, int i, DataObject dataobject){
    	return -1;
    }
    
    /**
      * do not use
      */
    public DataObject unmarshal(Node node) throws IOException, MalformedJsonException, IllegalArgumentException, NullPointerException, UnsupportedEncodingException {
    	return null;
    }
}

