package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPM01_OIVPM01_meta extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPM01_OIVPM01_meta";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPM01_OIVPM01_meta() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DATE_length
     * Comments    : 
     */	
    private int DATE_length = 0;
    
    private transient boolean DATE_length_nullable = false;
    
    public boolean isNullableDATE_length() {
    	return this.DATE_length_nullable;
    }
    
    private transient boolean DATE_length_invalidation = false;
    	
    public void setInvalidationDATE_length(boolean invalidation) { 
    	this.DATE_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_length() {
    	return this.DATE_length_invalidation;
    }
    	
    private transient boolean DATE_length_modified = false;
    
    public boolean isModifiedDATE_length() {
    	return this.DATE_length_modified;
    }
    	
    public void unModifiedDATE_length() {
    	this.DATE_length_modified = false;
    }
    public FieldProperty getDATE_lengthFieldProperty() {
    	return fieldPropertyMap.get("DATE_length");
    }
    
    public int getDATE_length() {
    	return DATE_length;
    }	
    public void setDATE_length(int DATE_length) {
    	this.DATE_length = DATE_length;
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(Integer DATE_length) {
    	if( DATE_length == null) {
    		this.DATE_length = 0;
    	} else{
    		this.DATE_length = DATE_length.intValue();
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    public void setDATE_length(String DATE_length) {
    	if  (DATE_length==null || DATE_length.length() == 0) {
    		this.DATE_length = 0;
    	} else {
    		this.DATE_length = Integer.parseInt(DATE_length);
    	}
    	this.DATE_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_color
     * Comments    : 
     */	
    private String DATE_color = null;
    
    private transient boolean DATE_color_nullable = true;
    
    public boolean isNullableDATE_color() {
    	return this.DATE_color_nullable;
    }
    
    private transient boolean DATE_color_invalidation = false;
    	
    public void setInvalidationDATE_color(boolean invalidation) { 
    	this.DATE_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_color() {
    	return this.DATE_color_invalidation;
    }
    	
    private transient boolean DATE_color_modified = false;
    
    public boolean isModifiedDATE_color() {
    	return this.DATE_color_modified;
    }
    	
    public void unModifiedDATE_color() {
    	this.DATE_color_modified = false;
    }
    public FieldProperty getDATE_colorFieldProperty() {
    	return fieldPropertyMap.get("DATE_color");
    }
    
    public String getDATE_color() {
    	return DATE_color;
    }	
    public String getNvlDATE_color() {
    	if(getDATE_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_color();
    	}
    }
    public void setDATE_color(String DATE_color) {
    	if(DATE_color == null) {
    		this.DATE_color = null;
    	} else {
    		this.DATE_color = DATE_color;
    	}
    	this.DATE_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_hilight
     * Comments    : 
     */	
    private String DATE_hilight = null;
    
    private transient boolean DATE_hilight_nullable = true;
    
    public boolean isNullableDATE_hilight() {
    	return this.DATE_hilight_nullable;
    }
    
    private transient boolean DATE_hilight_invalidation = false;
    	
    public void setInvalidationDATE_hilight(boolean invalidation) { 
    	this.DATE_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_hilight() {
    	return this.DATE_hilight_invalidation;
    }
    	
    private transient boolean DATE_hilight_modified = false;
    
    public boolean isModifiedDATE_hilight() {
    	return this.DATE_hilight_modified;
    }
    	
    public void unModifiedDATE_hilight() {
    	this.DATE_hilight_modified = false;
    }
    public FieldProperty getDATE_hilightFieldProperty() {
    	return fieldPropertyMap.get("DATE_hilight");
    }
    
    public String getDATE_hilight() {
    	return DATE_hilight;
    }	
    public String getNvlDATE_hilight() {
    	if(getDATE_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_hilight();
    	}
    }
    public void setDATE_hilight(String DATE_hilight) {
    	if(DATE_hilight == null) {
    		this.DATE_hilight = null;
    	} else {
    		this.DATE_hilight = DATE_hilight;
    	}
    	this.DATE_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_outline
     * Comments    : 
     */	
    private String DATE_outline = null;
    
    private transient boolean DATE_outline_nullable = true;
    
    public boolean isNullableDATE_outline() {
    	return this.DATE_outline_nullable;
    }
    
    private transient boolean DATE_outline_invalidation = false;
    	
    public void setInvalidationDATE_outline(boolean invalidation) { 
    	this.DATE_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_outline() {
    	return this.DATE_outline_invalidation;
    }
    	
    private transient boolean DATE_outline_modified = false;
    
    public boolean isModifiedDATE_outline() {
    	return this.DATE_outline_modified;
    }
    	
    public void unModifiedDATE_outline() {
    	this.DATE_outline_modified = false;
    }
    public FieldProperty getDATE_outlineFieldProperty() {
    	return fieldPropertyMap.get("DATE_outline");
    }
    
    public String getDATE_outline() {
    	return DATE_outline;
    }	
    public String getNvlDATE_outline() {
    	if(getDATE_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_outline();
    	}
    }
    public void setDATE_outline(String DATE_outline) {
    	if(DATE_outline == null) {
    		this.DATE_outline = null;
    	} else {
    		this.DATE_outline = DATE_outline;
    	}
    	this.DATE_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_transp
     * Comments    : 
     */	
    private String DATE_transp = null;
    
    private transient boolean DATE_transp_nullable = true;
    
    public boolean isNullableDATE_transp() {
    	return this.DATE_transp_nullable;
    }
    
    private transient boolean DATE_transp_invalidation = false;
    	
    public void setInvalidationDATE_transp(boolean invalidation) { 
    	this.DATE_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_transp() {
    	return this.DATE_transp_invalidation;
    }
    	
    private transient boolean DATE_transp_modified = false;
    
    public boolean isModifiedDATE_transp() {
    	return this.DATE_transp_modified;
    }
    	
    public void unModifiedDATE_transp() {
    	this.DATE_transp_modified = false;
    }
    public FieldProperty getDATE_transpFieldProperty() {
    	return fieldPropertyMap.get("DATE_transp");
    }
    
    public String getDATE_transp() {
    	return DATE_transp;
    }	
    public String getNvlDATE_transp() {
    	if(getDATE_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_transp();
    	}
    }
    public void setDATE_transp(String DATE_transp) {
    	if(DATE_transp == null) {
    		this.DATE_transp = null;
    	} else {
    		this.DATE_transp = DATE_transp;
    	}
    	this.DATE_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_validn
     * Comments    : 
     */	
    private String DATE_validn = null;
    
    private transient boolean DATE_validn_nullable = true;
    
    public boolean isNullableDATE_validn() {
    	return this.DATE_validn_nullable;
    }
    
    private transient boolean DATE_validn_invalidation = false;
    	
    public void setInvalidationDATE_validn(boolean invalidation) { 
    	this.DATE_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_validn() {
    	return this.DATE_validn_invalidation;
    }
    	
    private transient boolean DATE_validn_modified = false;
    
    public boolean isModifiedDATE_validn() {
    	return this.DATE_validn_modified;
    }
    	
    public void unModifiedDATE_validn() {
    	this.DATE_validn_modified = false;
    }
    public FieldProperty getDATE_validnFieldProperty() {
    	return fieldPropertyMap.get("DATE_validn");
    }
    
    public String getDATE_validn() {
    	return DATE_validn;
    }	
    public String getNvlDATE_validn() {
    	if(getDATE_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_validn();
    	}
    }
    public void setDATE_validn(String DATE_validn) {
    	if(DATE_validn == null) {
    		this.DATE_validn = null;
    	} else {
    		this.DATE_validn = DATE_validn;
    	}
    	this.DATE_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_sosi
     * Comments    : 
     */	
    private String DATE_sosi = null;
    
    private transient boolean DATE_sosi_nullable = true;
    
    public boolean isNullableDATE_sosi() {
    	return this.DATE_sosi_nullable;
    }
    
    private transient boolean DATE_sosi_invalidation = false;
    	
    public void setInvalidationDATE_sosi(boolean invalidation) { 
    	this.DATE_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_sosi() {
    	return this.DATE_sosi_invalidation;
    }
    	
    private transient boolean DATE_sosi_modified = false;
    
    public boolean isModifiedDATE_sosi() {
    	return this.DATE_sosi_modified;
    }
    	
    public void unModifiedDATE_sosi() {
    	this.DATE_sosi_modified = false;
    }
    public FieldProperty getDATE_sosiFieldProperty() {
    	return fieldPropertyMap.get("DATE_sosi");
    }
    
    public String getDATE_sosi() {
    	return DATE_sosi;
    }	
    public String getNvlDATE_sosi() {
    	if(getDATE_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_sosi();
    	}
    }
    public void setDATE_sosi(String DATE_sosi) {
    	if(DATE_sosi == null) {
    		this.DATE_sosi = null;
    	} else {
    		this.DATE_sosi = DATE_sosi;
    	}
    	this.DATE_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DATE_ps
     * Comments    : 
     */	
    private String DATE_ps = null;
    
    private transient boolean DATE_ps_nullable = true;
    
    public boolean isNullableDATE_ps() {
    	return this.DATE_ps_nullable;
    }
    
    private transient boolean DATE_ps_invalidation = false;
    	
    public void setInvalidationDATE_ps(boolean invalidation) { 
    	this.DATE_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDATE_ps() {
    	return this.DATE_ps_invalidation;
    }
    	
    private transient boolean DATE_ps_modified = false;
    
    public boolean isModifiedDATE_ps() {
    	return this.DATE_ps_modified;
    }
    	
    public void unModifiedDATE_ps() {
    	this.DATE_ps_modified = false;
    }
    public FieldProperty getDATE_psFieldProperty() {
    	return fieldPropertyMap.get("DATE_ps");
    }
    
    public String getDATE_ps() {
    	return DATE_ps;
    }	
    public String getNvlDATE_ps() {
    	if(getDATE_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDATE_ps();
    	}
    }
    public void setDATE_ps(String DATE_ps) {
    	if(DATE_ps == null) {
    		this.DATE_ps = null;
    	} else {
    		this.DATE_ps = DATE_ps;
    	}
    	this.DATE_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_length
     * Comments    : 
     */	
    private int TERM_length = 0;
    
    private transient boolean TERM_length_nullable = false;
    
    public boolean isNullableTERM_length() {
    	return this.TERM_length_nullable;
    }
    
    private transient boolean TERM_length_invalidation = false;
    	
    public void setInvalidationTERM_length(boolean invalidation) { 
    	this.TERM_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_length() {
    	return this.TERM_length_invalidation;
    }
    	
    private transient boolean TERM_length_modified = false;
    
    public boolean isModifiedTERM_length() {
    	return this.TERM_length_modified;
    }
    	
    public void unModifiedTERM_length() {
    	this.TERM_length_modified = false;
    }
    public FieldProperty getTERM_lengthFieldProperty() {
    	return fieldPropertyMap.get("TERM_length");
    }
    
    public int getTERM_length() {
    	return TERM_length;
    }	
    public void setTERM_length(int TERM_length) {
    	this.TERM_length = TERM_length;
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(Integer TERM_length) {
    	if( TERM_length == null) {
    		this.TERM_length = 0;
    	} else{
    		this.TERM_length = TERM_length.intValue();
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    public void setTERM_length(String TERM_length) {
    	if  (TERM_length==null || TERM_length.length() == 0) {
    		this.TERM_length = 0;
    	} else {
    		this.TERM_length = Integer.parseInt(TERM_length);
    	}
    	this.TERM_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_color
     * Comments    : 
     */	
    private String TERM_color = null;
    
    private transient boolean TERM_color_nullable = true;
    
    public boolean isNullableTERM_color() {
    	return this.TERM_color_nullable;
    }
    
    private transient boolean TERM_color_invalidation = false;
    	
    public void setInvalidationTERM_color(boolean invalidation) { 
    	this.TERM_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_color() {
    	return this.TERM_color_invalidation;
    }
    	
    private transient boolean TERM_color_modified = false;
    
    public boolean isModifiedTERM_color() {
    	return this.TERM_color_modified;
    }
    	
    public void unModifiedTERM_color() {
    	this.TERM_color_modified = false;
    }
    public FieldProperty getTERM_colorFieldProperty() {
    	return fieldPropertyMap.get("TERM_color");
    }
    
    public String getTERM_color() {
    	return TERM_color;
    }	
    public String getNvlTERM_color() {
    	if(getTERM_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_color();
    	}
    }
    public void setTERM_color(String TERM_color) {
    	if(TERM_color == null) {
    		this.TERM_color = null;
    	} else {
    		this.TERM_color = TERM_color;
    	}
    	this.TERM_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_hilight
     * Comments    : 
     */	
    private String TERM_hilight = null;
    
    private transient boolean TERM_hilight_nullable = true;
    
    public boolean isNullableTERM_hilight() {
    	return this.TERM_hilight_nullable;
    }
    
    private transient boolean TERM_hilight_invalidation = false;
    	
    public void setInvalidationTERM_hilight(boolean invalidation) { 
    	this.TERM_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_hilight() {
    	return this.TERM_hilight_invalidation;
    }
    	
    private transient boolean TERM_hilight_modified = false;
    
    public boolean isModifiedTERM_hilight() {
    	return this.TERM_hilight_modified;
    }
    	
    public void unModifiedTERM_hilight() {
    	this.TERM_hilight_modified = false;
    }
    public FieldProperty getTERM_hilightFieldProperty() {
    	return fieldPropertyMap.get("TERM_hilight");
    }
    
    public String getTERM_hilight() {
    	return TERM_hilight;
    }	
    public String getNvlTERM_hilight() {
    	if(getTERM_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_hilight();
    	}
    }
    public void setTERM_hilight(String TERM_hilight) {
    	if(TERM_hilight == null) {
    		this.TERM_hilight = null;
    	} else {
    		this.TERM_hilight = TERM_hilight;
    	}
    	this.TERM_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_outline
     * Comments    : 
     */	
    private String TERM_outline = null;
    
    private transient boolean TERM_outline_nullable = true;
    
    public boolean isNullableTERM_outline() {
    	return this.TERM_outline_nullable;
    }
    
    private transient boolean TERM_outline_invalidation = false;
    	
    public void setInvalidationTERM_outline(boolean invalidation) { 
    	this.TERM_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_outline() {
    	return this.TERM_outline_invalidation;
    }
    	
    private transient boolean TERM_outline_modified = false;
    
    public boolean isModifiedTERM_outline() {
    	return this.TERM_outline_modified;
    }
    	
    public void unModifiedTERM_outline() {
    	this.TERM_outline_modified = false;
    }
    public FieldProperty getTERM_outlineFieldProperty() {
    	return fieldPropertyMap.get("TERM_outline");
    }
    
    public String getTERM_outline() {
    	return TERM_outline;
    }	
    public String getNvlTERM_outline() {
    	if(getTERM_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_outline();
    	}
    }
    public void setTERM_outline(String TERM_outline) {
    	if(TERM_outline == null) {
    		this.TERM_outline = null;
    	} else {
    		this.TERM_outline = TERM_outline;
    	}
    	this.TERM_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_transp
     * Comments    : 
     */	
    private String TERM_transp = null;
    
    private transient boolean TERM_transp_nullable = true;
    
    public boolean isNullableTERM_transp() {
    	return this.TERM_transp_nullable;
    }
    
    private transient boolean TERM_transp_invalidation = false;
    	
    public void setInvalidationTERM_transp(boolean invalidation) { 
    	this.TERM_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_transp() {
    	return this.TERM_transp_invalidation;
    }
    	
    private transient boolean TERM_transp_modified = false;
    
    public boolean isModifiedTERM_transp() {
    	return this.TERM_transp_modified;
    }
    	
    public void unModifiedTERM_transp() {
    	this.TERM_transp_modified = false;
    }
    public FieldProperty getTERM_transpFieldProperty() {
    	return fieldPropertyMap.get("TERM_transp");
    }
    
    public String getTERM_transp() {
    	return TERM_transp;
    }	
    public String getNvlTERM_transp() {
    	if(getTERM_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_transp();
    	}
    }
    public void setTERM_transp(String TERM_transp) {
    	if(TERM_transp == null) {
    		this.TERM_transp = null;
    	} else {
    		this.TERM_transp = TERM_transp;
    	}
    	this.TERM_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_validn
     * Comments    : 
     */	
    private String TERM_validn = null;
    
    private transient boolean TERM_validn_nullable = true;
    
    public boolean isNullableTERM_validn() {
    	return this.TERM_validn_nullable;
    }
    
    private transient boolean TERM_validn_invalidation = false;
    	
    public void setInvalidationTERM_validn(boolean invalidation) { 
    	this.TERM_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_validn() {
    	return this.TERM_validn_invalidation;
    }
    	
    private transient boolean TERM_validn_modified = false;
    
    public boolean isModifiedTERM_validn() {
    	return this.TERM_validn_modified;
    }
    	
    public void unModifiedTERM_validn() {
    	this.TERM_validn_modified = false;
    }
    public FieldProperty getTERM_validnFieldProperty() {
    	return fieldPropertyMap.get("TERM_validn");
    }
    
    public String getTERM_validn() {
    	return TERM_validn;
    }	
    public String getNvlTERM_validn() {
    	if(getTERM_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_validn();
    	}
    }
    public void setTERM_validn(String TERM_validn) {
    	if(TERM_validn == null) {
    		this.TERM_validn = null;
    	} else {
    		this.TERM_validn = TERM_validn;
    	}
    	this.TERM_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_sosi
     * Comments    : 
     */	
    private String TERM_sosi = null;
    
    private transient boolean TERM_sosi_nullable = true;
    
    public boolean isNullableTERM_sosi() {
    	return this.TERM_sosi_nullable;
    }
    
    private transient boolean TERM_sosi_invalidation = false;
    	
    public void setInvalidationTERM_sosi(boolean invalidation) { 
    	this.TERM_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_sosi() {
    	return this.TERM_sosi_invalidation;
    }
    	
    private transient boolean TERM_sosi_modified = false;
    
    public boolean isModifiedTERM_sosi() {
    	return this.TERM_sosi_modified;
    }
    	
    public void unModifiedTERM_sosi() {
    	this.TERM_sosi_modified = false;
    }
    public FieldProperty getTERM_sosiFieldProperty() {
    	return fieldPropertyMap.get("TERM_sosi");
    }
    
    public String getTERM_sosi() {
    	return TERM_sosi;
    }	
    public String getNvlTERM_sosi() {
    	if(getTERM_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_sosi();
    	}
    }
    public void setTERM_sosi(String TERM_sosi) {
    	if(TERM_sosi == null) {
    		this.TERM_sosi = null;
    	} else {
    		this.TERM_sosi = TERM_sosi;
    	}
    	this.TERM_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TERM_ps
     * Comments    : 
     */	
    private String TERM_ps = null;
    
    private transient boolean TERM_ps_nullable = true;
    
    public boolean isNullableTERM_ps() {
    	return this.TERM_ps_nullable;
    }
    
    private transient boolean TERM_ps_invalidation = false;
    	
    public void setInvalidationTERM_ps(boolean invalidation) { 
    	this.TERM_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTERM_ps() {
    	return this.TERM_ps_invalidation;
    }
    	
    private transient boolean TERM_ps_modified = false;
    
    public boolean isModifiedTERM_ps() {
    	return this.TERM_ps_modified;
    }
    	
    public void unModifiedTERM_ps() {
    	this.TERM_ps_modified = false;
    }
    public FieldProperty getTERM_psFieldProperty() {
    	return fieldPropertyMap.get("TERM_ps");
    }
    
    public String getTERM_ps() {
    	return TERM_ps;
    }	
    public String getNvlTERM_ps() {
    	if(getTERM_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTERM_ps();
    	}
    }
    public void setTERM_ps(String TERM_ps) {
    	if(TERM_ps == null) {
    		this.TERM_ps = null;
    	} else {
    		this.TERM_ps = TERM_ps;
    	}
    	this.TERM_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_length
     * Comments    : 
     */	
    private int TIME_length = 0;
    
    private transient boolean TIME_length_nullable = false;
    
    public boolean isNullableTIME_length() {
    	return this.TIME_length_nullable;
    }
    
    private transient boolean TIME_length_invalidation = false;
    	
    public void setInvalidationTIME_length(boolean invalidation) { 
    	this.TIME_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_length() {
    	return this.TIME_length_invalidation;
    }
    	
    private transient boolean TIME_length_modified = false;
    
    public boolean isModifiedTIME_length() {
    	return this.TIME_length_modified;
    }
    	
    public void unModifiedTIME_length() {
    	this.TIME_length_modified = false;
    }
    public FieldProperty getTIME_lengthFieldProperty() {
    	return fieldPropertyMap.get("TIME_length");
    }
    
    public int getTIME_length() {
    	return TIME_length;
    }	
    public void setTIME_length(int TIME_length) {
    	this.TIME_length = TIME_length;
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(Integer TIME_length) {
    	if( TIME_length == null) {
    		this.TIME_length = 0;
    	} else{
    		this.TIME_length = TIME_length.intValue();
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    public void setTIME_length(String TIME_length) {
    	if  (TIME_length==null || TIME_length.length() == 0) {
    		this.TIME_length = 0;
    	} else {
    		this.TIME_length = Integer.parseInt(TIME_length);
    	}
    	this.TIME_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_color
     * Comments    : 
     */	
    private String TIME_color = null;
    
    private transient boolean TIME_color_nullable = true;
    
    public boolean isNullableTIME_color() {
    	return this.TIME_color_nullable;
    }
    
    private transient boolean TIME_color_invalidation = false;
    	
    public void setInvalidationTIME_color(boolean invalidation) { 
    	this.TIME_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_color() {
    	return this.TIME_color_invalidation;
    }
    	
    private transient boolean TIME_color_modified = false;
    
    public boolean isModifiedTIME_color() {
    	return this.TIME_color_modified;
    }
    	
    public void unModifiedTIME_color() {
    	this.TIME_color_modified = false;
    }
    public FieldProperty getTIME_colorFieldProperty() {
    	return fieldPropertyMap.get("TIME_color");
    }
    
    public String getTIME_color() {
    	return TIME_color;
    }	
    public String getNvlTIME_color() {
    	if(getTIME_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_color();
    	}
    }
    public void setTIME_color(String TIME_color) {
    	if(TIME_color == null) {
    		this.TIME_color = null;
    	} else {
    		this.TIME_color = TIME_color;
    	}
    	this.TIME_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_hilight
     * Comments    : 
     */	
    private String TIME_hilight = null;
    
    private transient boolean TIME_hilight_nullable = true;
    
    public boolean isNullableTIME_hilight() {
    	return this.TIME_hilight_nullable;
    }
    
    private transient boolean TIME_hilight_invalidation = false;
    	
    public void setInvalidationTIME_hilight(boolean invalidation) { 
    	this.TIME_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_hilight() {
    	return this.TIME_hilight_invalidation;
    }
    	
    private transient boolean TIME_hilight_modified = false;
    
    public boolean isModifiedTIME_hilight() {
    	return this.TIME_hilight_modified;
    }
    	
    public void unModifiedTIME_hilight() {
    	this.TIME_hilight_modified = false;
    }
    public FieldProperty getTIME_hilightFieldProperty() {
    	return fieldPropertyMap.get("TIME_hilight");
    }
    
    public String getTIME_hilight() {
    	return TIME_hilight;
    }	
    public String getNvlTIME_hilight() {
    	if(getTIME_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_hilight();
    	}
    }
    public void setTIME_hilight(String TIME_hilight) {
    	if(TIME_hilight == null) {
    		this.TIME_hilight = null;
    	} else {
    		this.TIME_hilight = TIME_hilight;
    	}
    	this.TIME_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_outline
     * Comments    : 
     */	
    private String TIME_outline = null;
    
    private transient boolean TIME_outline_nullable = true;
    
    public boolean isNullableTIME_outline() {
    	return this.TIME_outline_nullable;
    }
    
    private transient boolean TIME_outline_invalidation = false;
    	
    public void setInvalidationTIME_outline(boolean invalidation) { 
    	this.TIME_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_outline() {
    	return this.TIME_outline_invalidation;
    }
    	
    private transient boolean TIME_outline_modified = false;
    
    public boolean isModifiedTIME_outline() {
    	return this.TIME_outline_modified;
    }
    	
    public void unModifiedTIME_outline() {
    	this.TIME_outline_modified = false;
    }
    public FieldProperty getTIME_outlineFieldProperty() {
    	return fieldPropertyMap.get("TIME_outline");
    }
    
    public String getTIME_outline() {
    	return TIME_outline;
    }	
    public String getNvlTIME_outline() {
    	if(getTIME_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_outline();
    	}
    }
    public void setTIME_outline(String TIME_outline) {
    	if(TIME_outline == null) {
    		this.TIME_outline = null;
    	} else {
    		this.TIME_outline = TIME_outline;
    	}
    	this.TIME_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_transp
     * Comments    : 
     */	
    private String TIME_transp = null;
    
    private transient boolean TIME_transp_nullable = true;
    
    public boolean isNullableTIME_transp() {
    	return this.TIME_transp_nullable;
    }
    
    private transient boolean TIME_transp_invalidation = false;
    	
    public void setInvalidationTIME_transp(boolean invalidation) { 
    	this.TIME_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_transp() {
    	return this.TIME_transp_invalidation;
    }
    	
    private transient boolean TIME_transp_modified = false;
    
    public boolean isModifiedTIME_transp() {
    	return this.TIME_transp_modified;
    }
    	
    public void unModifiedTIME_transp() {
    	this.TIME_transp_modified = false;
    }
    public FieldProperty getTIME_transpFieldProperty() {
    	return fieldPropertyMap.get("TIME_transp");
    }
    
    public String getTIME_transp() {
    	return TIME_transp;
    }	
    public String getNvlTIME_transp() {
    	if(getTIME_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_transp();
    	}
    }
    public void setTIME_transp(String TIME_transp) {
    	if(TIME_transp == null) {
    		this.TIME_transp = null;
    	} else {
    		this.TIME_transp = TIME_transp;
    	}
    	this.TIME_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_validn
     * Comments    : 
     */	
    private String TIME_validn = null;
    
    private transient boolean TIME_validn_nullable = true;
    
    public boolean isNullableTIME_validn() {
    	return this.TIME_validn_nullable;
    }
    
    private transient boolean TIME_validn_invalidation = false;
    	
    public void setInvalidationTIME_validn(boolean invalidation) { 
    	this.TIME_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_validn() {
    	return this.TIME_validn_invalidation;
    }
    	
    private transient boolean TIME_validn_modified = false;
    
    public boolean isModifiedTIME_validn() {
    	return this.TIME_validn_modified;
    }
    	
    public void unModifiedTIME_validn() {
    	this.TIME_validn_modified = false;
    }
    public FieldProperty getTIME_validnFieldProperty() {
    	return fieldPropertyMap.get("TIME_validn");
    }
    
    public String getTIME_validn() {
    	return TIME_validn;
    }	
    public String getNvlTIME_validn() {
    	if(getTIME_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_validn();
    	}
    }
    public void setTIME_validn(String TIME_validn) {
    	if(TIME_validn == null) {
    		this.TIME_validn = null;
    	} else {
    		this.TIME_validn = TIME_validn;
    	}
    	this.TIME_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_sosi
     * Comments    : 
     */	
    private String TIME_sosi = null;
    
    private transient boolean TIME_sosi_nullable = true;
    
    public boolean isNullableTIME_sosi() {
    	return this.TIME_sosi_nullable;
    }
    
    private transient boolean TIME_sosi_invalidation = false;
    	
    public void setInvalidationTIME_sosi(boolean invalidation) { 
    	this.TIME_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_sosi() {
    	return this.TIME_sosi_invalidation;
    }
    	
    private transient boolean TIME_sosi_modified = false;
    
    public boolean isModifiedTIME_sosi() {
    	return this.TIME_sosi_modified;
    }
    	
    public void unModifiedTIME_sosi() {
    	this.TIME_sosi_modified = false;
    }
    public FieldProperty getTIME_sosiFieldProperty() {
    	return fieldPropertyMap.get("TIME_sosi");
    }
    
    public String getTIME_sosi() {
    	return TIME_sosi;
    }	
    public String getNvlTIME_sosi() {
    	if(getTIME_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_sosi();
    	}
    }
    public void setTIME_sosi(String TIME_sosi) {
    	if(TIME_sosi == null) {
    		this.TIME_sosi = null;
    	} else {
    		this.TIME_sosi = TIME_sosi;
    	}
    	this.TIME_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : TIME_ps
     * Comments    : 
     */	
    private String TIME_ps = null;
    
    private transient boolean TIME_ps_nullable = true;
    
    public boolean isNullableTIME_ps() {
    	return this.TIME_ps_nullable;
    }
    
    private transient boolean TIME_ps_invalidation = false;
    	
    public void setInvalidationTIME_ps(boolean invalidation) { 
    	this.TIME_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationTIME_ps() {
    	return this.TIME_ps_invalidation;
    }
    	
    private transient boolean TIME_ps_modified = false;
    
    public boolean isModifiedTIME_ps() {
    	return this.TIME_ps_modified;
    }
    	
    public void unModifiedTIME_ps() {
    	this.TIME_ps_modified = false;
    }
    public FieldProperty getTIME_psFieldProperty() {
    	return fieldPropertyMap.get("TIME_ps");
    }
    
    public String getTIME_ps() {
    	return TIME_ps;
    }	
    public String getNvlTIME_ps() {
    	if(getTIME_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getTIME_ps();
    	}
    }
    public void setTIME_ps(String TIME_ps) {
    	if(TIME_ps == null) {
    		this.TIME_ps = null;
    	} else {
    		this.TIME_ps = TIME_ps;
    	}
    	this.TIME_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_length
     * Comments    : 
     */	
    private int CODE_length = 0;
    
    private transient boolean CODE_length_nullable = false;
    
    public boolean isNullableCODE_length() {
    	return this.CODE_length_nullable;
    }
    
    private transient boolean CODE_length_invalidation = false;
    	
    public void setInvalidationCODE_length(boolean invalidation) { 
    	this.CODE_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_length() {
    	return this.CODE_length_invalidation;
    }
    	
    private transient boolean CODE_length_modified = false;
    
    public boolean isModifiedCODE_length() {
    	return this.CODE_length_modified;
    }
    	
    public void unModifiedCODE_length() {
    	this.CODE_length_modified = false;
    }
    public FieldProperty getCODE_lengthFieldProperty() {
    	return fieldPropertyMap.get("CODE_length");
    }
    
    public int getCODE_length() {
    	return CODE_length;
    }	
    public void setCODE_length(int CODE_length) {
    	this.CODE_length = CODE_length;
    	this.CODE_length_modified = true;
    	this.isModified = true;
    }
    public void setCODE_length(Integer CODE_length) {
    	if( CODE_length == null) {
    		this.CODE_length = 0;
    	} else{
    		this.CODE_length = CODE_length.intValue();
    	}
    	this.CODE_length_modified = true;
    	this.isModified = true;
    }
    public void setCODE_length(String CODE_length) {
    	if  (CODE_length==null || CODE_length.length() == 0) {
    		this.CODE_length = 0;
    	} else {
    		this.CODE_length = Integer.parseInt(CODE_length);
    	}
    	this.CODE_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_color
     * Comments    : 
     */	
    private String CODE_color = null;
    
    private transient boolean CODE_color_nullable = true;
    
    public boolean isNullableCODE_color() {
    	return this.CODE_color_nullable;
    }
    
    private transient boolean CODE_color_invalidation = false;
    	
    public void setInvalidationCODE_color(boolean invalidation) { 
    	this.CODE_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_color() {
    	return this.CODE_color_invalidation;
    }
    	
    private transient boolean CODE_color_modified = false;
    
    public boolean isModifiedCODE_color() {
    	return this.CODE_color_modified;
    }
    	
    public void unModifiedCODE_color() {
    	this.CODE_color_modified = false;
    }
    public FieldProperty getCODE_colorFieldProperty() {
    	return fieldPropertyMap.get("CODE_color");
    }
    
    public String getCODE_color() {
    	return CODE_color;
    }	
    public String getNvlCODE_color() {
    	if(getCODE_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_color();
    	}
    }
    public void setCODE_color(String CODE_color) {
    	if(CODE_color == null) {
    		this.CODE_color = null;
    	} else {
    		this.CODE_color = CODE_color;
    	}
    	this.CODE_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_hilight
     * Comments    : 
     */	
    private String CODE_hilight = null;
    
    private transient boolean CODE_hilight_nullable = true;
    
    public boolean isNullableCODE_hilight() {
    	return this.CODE_hilight_nullable;
    }
    
    private transient boolean CODE_hilight_invalidation = false;
    	
    public void setInvalidationCODE_hilight(boolean invalidation) { 
    	this.CODE_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_hilight() {
    	return this.CODE_hilight_invalidation;
    }
    	
    private transient boolean CODE_hilight_modified = false;
    
    public boolean isModifiedCODE_hilight() {
    	return this.CODE_hilight_modified;
    }
    	
    public void unModifiedCODE_hilight() {
    	this.CODE_hilight_modified = false;
    }
    public FieldProperty getCODE_hilightFieldProperty() {
    	return fieldPropertyMap.get("CODE_hilight");
    }
    
    public String getCODE_hilight() {
    	return CODE_hilight;
    }	
    public String getNvlCODE_hilight() {
    	if(getCODE_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_hilight();
    	}
    }
    public void setCODE_hilight(String CODE_hilight) {
    	if(CODE_hilight == null) {
    		this.CODE_hilight = null;
    	} else {
    		this.CODE_hilight = CODE_hilight;
    	}
    	this.CODE_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_outline
     * Comments    : 
     */	
    private String CODE_outline = null;
    
    private transient boolean CODE_outline_nullable = true;
    
    public boolean isNullableCODE_outline() {
    	return this.CODE_outline_nullable;
    }
    
    private transient boolean CODE_outline_invalidation = false;
    	
    public void setInvalidationCODE_outline(boolean invalidation) { 
    	this.CODE_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_outline() {
    	return this.CODE_outline_invalidation;
    }
    	
    private transient boolean CODE_outline_modified = false;
    
    public boolean isModifiedCODE_outline() {
    	return this.CODE_outline_modified;
    }
    	
    public void unModifiedCODE_outline() {
    	this.CODE_outline_modified = false;
    }
    public FieldProperty getCODE_outlineFieldProperty() {
    	return fieldPropertyMap.get("CODE_outline");
    }
    
    public String getCODE_outline() {
    	return CODE_outline;
    }	
    public String getNvlCODE_outline() {
    	if(getCODE_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_outline();
    	}
    }
    public void setCODE_outline(String CODE_outline) {
    	if(CODE_outline == null) {
    		this.CODE_outline = null;
    	} else {
    		this.CODE_outline = CODE_outline;
    	}
    	this.CODE_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_transp
     * Comments    : 
     */	
    private String CODE_transp = null;
    
    private transient boolean CODE_transp_nullable = true;
    
    public boolean isNullableCODE_transp() {
    	return this.CODE_transp_nullable;
    }
    
    private transient boolean CODE_transp_invalidation = false;
    	
    public void setInvalidationCODE_transp(boolean invalidation) { 
    	this.CODE_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_transp() {
    	return this.CODE_transp_invalidation;
    }
    	
    private transient boolean CODE_transp_modified = false;
    
    public boolean isModifiedCODE_transp() {
    	return this.CODE_transp_modified;
    }
    	
    public void unModifiedCODE_transp() {
    	this.CODE_transp_modified = false;
    }
    public FieldProperty getCODE_transpFieldProperty() {
    	return fieldPropertyMap.get("CODE_transp");
    }
    
    public String getCODE_transp() {
    	return CODE_transp;
    }	
    public String getNvlCODE_transp() {
    	if(getCODE_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_transp();
    	}
    }
    public void setCODE_transp(String CODE_transp) {
    	if(CODE_transp == null) {
    		this.CODE_transp = null;
    	} else {
    		this.CODE_transp = CODE_transp;
    	}
    	this.CODE_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_validn
     * Comments    : 
     */	
    private String CODE_validn = null;
    
    private transient boolean CODE_validn_nullable = true;
    
    public boolean isNullableCODE_validn() {
    	return this.CODE_validn_nullable;
    }
    
    private transient boolean CODE_validn_invalidation = false;
    	
    public void setInvalidationCODE_validn(boolean invalidation) { 
    	this.CODE_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_validn() {
    	return this.CODE_validn_invalidation;
    }
    	
    private transient boolean CODE_validn_modified = false;
    
    public boolean isModifiedCODE_validn() {
    	return this.CODE_validn_modified;
    }
    	
    public void unModifiedCODE_validn() {
    	this.CODE_validn_modified = false;
    }
    public FieldProperty getCODE_validnFieldProperty() {
    	return fieldPropertyMap.get("CODE_validn");
    }
    
    public String getCODE_validn() {
    	return CODE_validn;
    }	
    public String getNvlCODE_validn() {
    	if(getCODE_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_validn();
    	}
    }
    public void setCODE_validn(String CODE_validn) {
    	if(CODE_validn == null) {
    		this.CODE_validn = null;
    	} else {
    		this.CODE_validn = CODE_validn;
    	}
    	this.CODE_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_sosi
     * Comments    : 
     */	
    private String CODE_sosi = null;
    
    private transient boolean CODE_sosi_nullable = true;
    
    public boolean isNullableCODE_sosi() {
    	return this.CODE_sosi_nullable;
    }
    
    private transient boolean CODE_sosi_invalidation = false;
    	
    public void setInvalidationCODE_sosi(boolean invalidation) { 
    	this.CODE_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_sosi() {
    	return this.CODE_sosi_invalidation;
    }
    	
    private transient boolean CODE_sosi_modified = false;
    
    public boolean isModifiedCODE_sosi() {
    	return this.CODE_sosi_modified;
    }
    	
    public void unModifiedCODE_sosi() {
    	this.CODE_sosi_modified = false;
    }
    public FieldProperty getCODE_sosiFieldProperty() {
    	return fieldPropertyMap.get("CODE_sosi");
    }
    
    public String getCODE_sosi() {
    	return CODE_sosi;
    }	
    public String getNvlCODE_sosi() {
    	if(getCODE_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_sosi();
    	}
    }
    public void setCODE_sosi(String CODE_sosi) {
    	if(CODE_sosi == null) {
    		this.CODE_sosi = null;
    	} else {
    		this.CODE_sosi = CODE_sosi;
    	}
    	this.CODE_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : CODE_ps
     * Comments    : 
     */	
    private String CODE_ps = null;
    
    private transient boolean CODE_ps_nullable = true;
    
    public boolean isNullableCODE_ps() {
    	return this.CODE_ps_nullable;
    }
    
    private transient boolean CODE_ps_invalidation = false;
    	
    public void setInvalidationCODE_ps(boolean invalidation) { 
    	this.CODE_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationCODE_ps() {
    	return this.CODE_ps_invalidation;
    }
    	
    private transient boolean CODE_ps_modified = false;
    
    public boolean isModifiedCODE_ps() {
    	return this.CODE_ps_modified;
    }
    	
    public void unModifiedCODE_ps() {
    	this.CODE_ps_modified = false;
    }
    public FieldProperty getCODE_psFieldProperty() {
    	return fieldPropertyMap.get("CODE_ps");
    }
    
    public String getCODE_ps() {
    	return CODE_ps;
    }	
    public String getNvlCODE_ps() {
    	if(getCODE_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getCODE_ps();
    	}
    }
    public void setCODE_ps(String CODE_ps) {
    	if(CODE_ps == null) {
    		this.CODE_ps = null;
    	} else {
    		this.CODE_ps = CODE_ps;
    	}
    	this.CODE_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_length
     * Comments    : 
     */	
    private int IDEN_length = 0;
    
    private transient boolean IDEN_length_nullable = false;
    
    public boolean isNullableIDEN_length() {
    	return this.IDEN_length_nullable;
    }
    
    private transient boolean IDEN_length_invalidation = false;
    	
    public void setInvalidationIDEN_length(boolean invalidation) { 
    	this.IDEN_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_length() {
    	return this.IDEN_length_invalidation;
    }
    	
    private transient boolean IDEN_length_modified = false;
    
    public boolean isModifiedIDEN_length() {
    	return this.IDEN_length_modified;
    }
    	
    public void unModifiedIDEN_length() {
    	this.IDEN_length_modified = false;
    }
    public FieldProperty getIDEN_lengthFieldProperty() {
    	return fieldPropertyMap.get("IDEN_length");
    }
    
    public int getIDEN_length() {
    	return IDEN_length;
    }	
    public void setIDEN_length(int IDEN_length) {
    	this.IDEN_length = IDEN_length;
    	this.IDEN_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN_length(Integer IDEN_length) {
    	if( IDEN_length == null) {
    		this.IDEN_length = 0;
    	} else{
    		this.IDEN_length = IDEN_length.intValue();
    	}
    	this.IDEN_length_modified = true;
    	this.isModified = true;
    }
    public void setIDEN_length(String IDEN_length) {
    	if  (IDEN_length==null || IDEN_length.length() == 0) {
    		this.IDEN_length = 0;
    	} else {
    		this.IDEN_length = Integer.parseInt(IDEN_length);
    	}
    	this.IDEN_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_color
     * Comments    : 
     */	
    private String IDEN_color = null;
    
    private transient boolean IDEN_color_nullable = true;
    
    public boolean isNullableIDEN_color() {
    	return this.IDEN_color_nullable;
    }
    
    private transient boolean IDEN_color_invalidation = false;
    	
    public void setInvalidationIDEN_color(boolean invalidation) { 
    	this.IDEN_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_color() {
    	return this.IDEN_color_invalidation;
    }
    	
    private transient boolean IDEN_color_modified = false;
    
    public boolean isModifiedIDEN_color() {
    	return this.IDEN_color_modified;
    }
    	
    public void unModifiedIDEN_color() {
    	this.IDEN_color_modified = false;
    }
    public FieldProperty getIDEN_colorFieldProperty() {
    	return fieldPropertyMap.get("IDEN_color");
    }
    
    public String getIDEN_color() {
    	return IDEN_color;
    }	
    public String getNvlIDEN_color() {
    	if(getIDEN_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_color();
    	}
    }
    public void setIDEN_color(String IDEN_color) {
    	if(IDEN_color == null) {
    		this.IDEN_color = null;
    	} else {
    		this.IDEN_color = IDEN_color;
    	}
    	this.IDEN_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_hilight
     * Comments    : 
     */	
    private String IDEN_hilight = null;
    
    private transient boolean IDEN_hilight_nullable = true;
    
    public boolean isNullableIDEN_hilight() {
    	return this.IDEN_hilight_nullable;
    }
    
    private transient boolean IDEN_hilight_invalidation = false;
    	
    public void setInvalidationIDEN_hilight(boolean invalidation) { 
    	this.IDEN_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_hilight() {
    	return this.IDEN_hilight_invalidation;
    }
    	
    private transient boolean IDEN_hilight_modified = false;
    
    public boolean isModifiedIDEN_hilight() {
    	return this.IDEN_hilight_modified;
    }
    	
    public void unModifiedIDEN_hilight() {
    	this.IDEN_hilight_modified = false;
    }
    public FieldProperty getIDEN_hilightFieldProperty() {
    	return fieldPropertyMap.get("IDEN_hilight");
    }
    
    public String getIDEN_hilight() {
    	return IDEN_hilight;
    }	
    public String getNvlIDEN_hilight() {
    	if(getIDEN_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_hilight();
    	}
    }
    public void setIDEN_hilight(String IDEN_hilight) {
    	if(IDEN_hilight == null) {
    		this.IDEN_hilight = null;
    	} else {
    		this.IDEN_hilight = IDEN_hilight;
    	}
    	this.IDEN_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_outline
     * Comments    : 
     */	
    private String IDEN_outline = null;
    
    private transient boolean IDEN_outline_nullable = true;
    
    public boolean isNullableIDEN_outline() {
    	return this.IDEN_outline_nullable;
    }
    
    private transient boolean IDEN_outline_invalidation = false;
    	
    public void setInvalidationIDEN_outline(boolean invalidation) { 
    	this.IDEN_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_outline() {
    	return this.IDEN_outline_invalidation;
    }
    	
    private transient boolean IDEN_outline_modified = false;
    
    public boolean isModifiedIDEN_outline() {
    	return this.IDEN_outline_modified;
    }
    	
    public void unModifiedIDEN_outline() {
    	this.IDEN_outline_modified = false;
    }
    public FieldProperty getIDEN_outlineFieldProperty() {
    	return fieldPropertyMap.get("IDEN_outline");
    }
    
    public String getIDEN_outline() {
    	return IDEN_outline;
    }	
    public String getNvlIDEN_outline() {
    	if(getIDEN_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_outline();
    	}
    }
    public void setIDEN_outline(String IDEN_outline) {
    	if(IDEN_outline == null) {
    		this.IDEN_outline = null;
    	} else {
    		this.IDEN_outline = IDEN_outline;
    	}
    	this.IDEN_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_transp
     * Comments    : 
     */	
    private String IDEN_transp = null;
    
    private transient boolean IDEN_transp_nullable = true;
    
    public boolean isNullableIDEN_transp() {
    	return this.IDEN_transp_nullable;
    }
    
    private transient boolean IDEN_transp_invalidation = false;
    	
    public void setInvalidationIDEN_transp(boolean invalidation) { 
    	this.IDEN_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_transp() {
    	return this.IDEN_transp_invalidation;
    }
    	
    private transient boolean IDEN_transp_modified = false;
    
    public boolean isModifiedIDEN_transp() {
    	return this.IDEN_transp_modified;
    }
    	
    public void unModifiedIDEN_transp() {
    	this.IDEN_transp_modified = false;
    }
    public FieldProperty getIDEN_transpFieldProperty() {
    	return fieldPropertyMap.get("IDEN_transp");
    }
    
    public String getIDEN_transp() {
    	return IDEN_transp;
    }	
    public String getNvlIDEN_transp() {
    	if(getIDEN_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_transp();
    	}
    }
    public void setIDEN_transp(String IDEN_transp) {
    	if(IDEN_transp == null) {
    		this.IDEN_transp = null;
    	} else {
    		this.IDEN_transp = IDEN_transp;
    	}
    	this.IDEN_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_validn
     * Comments    : 
     */	
    private String IDEN_validn = null;
    
    private transient boolean IDEN_validn_nullable = true;
    
    public boolean isNullableIDEN_validn() {
    	return this.IDEN_validn_nullable;
    }
    
    private transient boolean IDEN_validn_invalidation = false;
    	
    public void setInvalidationIDEN_validn(boolean invalidation) { 
    	this.IDEN_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_validn() {
    	return this.IDEN_validn_invalidation;
    }
    	
    private transient boolean IDEN_validn_modified = false;
    
    public boolean isModifiedIDEN_validn() {
    	return this.IDEN_validn_modified;
    }
    	
    public void unModifiedIDEN_validn() {
    	this.IDEN_validn_modified = false;
    }
    public FieldProperty getIDEN_validnFieldProperty() {
    	return fieldPropertyMap.get("IDEN_validn");
    }
    
    public String getIDEN_validn() {
    	return IDEN_validn;
    }	
    public String getNvlIDEN_validn() {
    	if(getIDEN_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_validn();
    	}
    }
    public void setIDEN_validn(String IDEN_validn) {
    	if(IDEN_validn == null) {
    		this.IDEN_validn = null;
    	} else {
    		this.IDEN_validn = IDEN_validn;
    	}
    	this.IDEN_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_sosi
     * Comments    : 
     */	
    private String IDEN_sosi = null;
    
    private transient boolean IDEN_sosi_nullable = true;
    
    public boolean isNullableIDEN_sosi() {
    	return this.IDEN_sosi_nullable;
    }
    
    private transient boolean IDEN_sosi_invalidation = false;
    	
    public void setInvalidationIDEN_sosi(boolean invalidation) { 
    	this.IDEN_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_sosi() {
    	return this.IDEN_sosi_invalidation;
    }
    	
    private transient boolean IDEN_sosi_modified = false;
    
    public boolean isModifiedIDEN_sosi() {
    	return this.IDEN_sosi_modified;
    }
    	
    public void unModifiedIDEN_sosi() {
    	this.IDEN_sosi_modified = false;
    }
    public FieldProperty getIDEN_sosiFieldProperty() {
    	return fieldPropertyMap.get("IDEN_sosi");
    }
    
    public String getIDEN_sosi() {
    	return IDEN_sosi;
    }	
    public String getNvlIDEN_sosi() {
    	if(getIDEN_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_sosi();
    	}
    }
    public void setIDEN_sosi(String IDEN_sosi) {
    	if(IDEN_sosi == null) {
    		this.IDEN_sosi = null;
    	} else {
    		this.IDEN_sosi = IDEN_sosi;
    	}
    	this.IDEN_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : IDEN_ps
     * Comments    : 
     */	
    private String IDEN_ps = null;
    
    private transient boolean IDEN_ps_nullable = true;
    
    public boolean isNullableIDEN_ps() {
    	return this.IDEN_ps_nullable;
    }
    
    private transient boolean IDEN_ps_invalidation = false;
    	
    public void setInvalidationIDEN_ps(boolean invalidation) { 
    	this.IDEN_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationIDEN_ps() {
    	return this.IDEN_ps_invalidation;
    }
    	
    private transient boolean IDEN_ps_modified = false;
    
    public boolean isModifiedIDEN_ps() {
    	return this.IDEN_ps_modified;
    }
    	
    public void unModifiedIDEN_ps() {
    	this.IDEN_ps_modified = false;
    }
    public FieldProperty getIDEN_psFieldProperty() {
    	return fieldPropertyMap.get("IDEN_ps");
    }
    
    public String getIDEN_ps() {
    	return IDEN_ps;
    }	
    public String getNvlIDEN_ps() {
    	if(getIDEN_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getIDEN_ps();
    	}
    }
    public void setIDEN_ps(String IDEN_ps) {
    	if(IDEN_ps == null) {
    		this.IDEN_ps = null;
    	} else {
    		this.IDEN_ps = IDEN_ps;
    	}
    	this.IDEN_ps_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_length
     * Comments    : 
     */	
    private int MSG_length = 0;
    
    private transient boolean MSG_length_nullable = false;
    
    public boolean isNullableMSG_length() {
    	return this.MSG_length_nullable;
    }
    
    private transient boolean MSG_length_invalidation = false;
    	
    public void setInvalidationMSG_length(boolean invalidation) { 
    	this.MSG_length_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_length() {
    	return this.MSG_length_invalidation;
    }
    	
    private transient boolean MSG_length_modified = false;
    
    public boolean isModifiedMSG_length() {
    	return this.MSG_length_modified;
    }
    	
    public void unModifiedMSG_length() {
    	this.MSG_length_modified = false;
    }
    public FieldProperty getMSG_lengthFieldProperty() {
    	return fieldPropertyMap.get("MSG_length");
    }
    
    public int getMSG_length() {
    	return MSG_length;
    }	
    public void setMSG_length(int MSG_length) {
    	this.MSG_length = MSG_length;
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(Integer MSG_length) {
    	if( MSG_length == null) {
    		this.MSG_length = 0;
    	} else{
    		this.MSG_length = MSG_length.intValue();
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    public void setMSG_length(String MSG_length) {
    	if  (MSG_length==null || MSG_length.length() == 0) {
    		this.MSG_length = 0;
    	} else {
    		this.MSG_length = Integer.parseInt(MSG_length);
    	}
    	this.MSG_length_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_color
     * Comments    : 
     */	
    private String MSG_color = null;
    
    private transient boolean MSG_color_nullable = true;
    
    public boolean isNullableMSG_color() {
    	return this.MSG_color_nullable;
    }
    
    private transient boolean MSG_color_invalidation = false;
    	
    public void setInvalidationMSG_color(boolean invalidation) { 
    	this.MSG_color_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_color() {
    	return this.MSG_color_invalidation;
    }
    	
    private transient boolean MSG_color_modified = false;
    
    public boolean isModifiedMSG_color() {
    	return this.MSG_color_modified;
    }
    	
    public void unModifiedMSG_color() {
    	this.MSG_color_modified = false;
    }
    public FieldProperty getMSG_colorFieldProperty() {
    	return fieldPropertyMap.get("MSG_color");
    }
    
    public String getMSG_color() {
    	return MSG_color;
    }	
    public String getNvlMSG_color() {
    	if(getMSG_color() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_color();
    	}
    }
    public void setMSG_color(String MSG_color) {
    	if(MSG_color == null) {
    		this.MSG_color = null;
    	} else {
    		this.MSG_color = MSG_color;
    	}
    	this.MSG_color_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_hilight
     * Comments    : 
     */	
    private String MSG_hilight = null;
    
    private transient boolean MSG_hilight_nullable = true;
    
    public boolean isNullableMSG_hilight() {
    	return this.MSG_hilight_nullable;
    }
    
    private transient boolean MSG_hilight_invalidation = false;
    	
    public void setInvalidationMSG_hilight(boolean invalidation) { 
    	this.MSG_hilight_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_hilight() {
    	return this.MSG_hilight_invalidation;
    }
    	
    private transient boolean MSG_hilight_modified = false;
    
    public boolean isModifiedMSG_hilight() {
    	return this.MSG_hilight_modified;
    }
    	
    public void unModifiedMSG_hilight() {
    	this.MSG_hilight_modified = false;
    }
    public FieldProperty getMSG_hilightFieldProperty() {
    	return fieldPropertyMap.get("MSG_hilight");
    }
    
    public String getMSG_hilight() {
    	return MSG_hilight;
    }	
    public String getNvlMSG_hilight() {
    	if(getMSG_hilight() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_hilight();
    	}
    }
    public void setMSG_hilight(String MSG_hilight) {
    	if(MSG_hilight == null) {
    		this.MSG_hilight = null;
    	} else {
    		this.MSG_hilight = MSG_hilight;
    	}
    	this.MSG_hilight_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_outline
     * Comments    : 
     */	
    private String MSG_outline = null;
    
    private transient boolean MSG_outline_nullable = true;
    
    public boolean isNullableMSG_outline() {
    	return this.MSG_outline_nullable;
    }
    
    private transient boolean MSG_outline_invalidation = false;
    	
    public void setInvalidationMSG_outline(boolean invalidation) { 
    	this.MSG_outline_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_outline() {
    	return this.MSG_outline_invalidation;
    }
    	
    private transient boolean MSG_outline_modified = false;
    
    public boolean isModifiedMSG_outline() {
    	return this.MSG_outline_modified;
    }
    	
    public void unModifiedMSG_outline() {
    	this.MSG_outline_modified = false;
    }
    public FieldProperty getMSG_outlineFieldProperty() {
    	return fieldPropertyMap.get("MSG_outline");
    }
    
    public String getMSG_outline() {
    	return MSG_outline;
    }	
    public String getNvlMSG_outline() {
    	if(getMSG_outline() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_outline();
    	}
    }
    public void setMSG_outline(String MSG_outline) {
    	if(MSG_outline == null) {
    		this.MSG_outline = null;
    	} else {
    		this.MSG_outline = MSG_outline;
    	}
    	this.MSG_outline_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_transp
     * Comments    : 
     */	
    private String MSG_transp = null;
    
    private transient boolean MSG_transp_nullable = true;
    
    public boolean isNullableMSG_transp() {
    	return this.MSG_transp_nullable;
    }
    
    private transient boolean MSG_transp_invalidation = false;
    	
    public void setInvalidationMSG_transp(boolean invalidation) { 
    	this.MSG_transp_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_transp() {
    	return this.MSG_transp_invalidation;
    }
    	
    private transient boolean MSG_transp_modified = false;
    
    public boolean isModifiedMSG_transp() {
    	return this.MSG_transp_modified;
    }
    	
    public void unModifiedMSG_transp() {
    	this.MSG_transp_modified = false;
    }
    public FieldProperty getMSG_transpFieldProperty() {
    	return fieldPropertyMap.get("MSG_transp");
    }
    
    public String getMSG_transp() {
    	return MSG_transp;
    }	
    public String getNvlMSG_transp() {
    	if(getMSG_transp() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_transp();
    	}
    }
    public void setMSG_transp(String MSG_transp) {
    	if(MSG_transp == null) {
    		this.MSG_transp = null;
    	} else {
    		this.MSG_transp = MSG_transp;
    	}
    	this.MSG_transp_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_validn
     * Comments    : 
     */	
    private String MSG_validn = null;
    
    private transient boolean MSG_validn_nullable = true;
    
    public boolean isNullableMSG_validn() {
    	return this.MSG_validn_nullable;
    }
    
    private transient boolean MSG_validn_invalidation = false;
    	
    public void setInvalidationMSG_validn(boolean invalidation) { 
    	this.MSG_validn_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_validn() {
    	return this.MSG_validn_invalidation;
    }
    	
    private transient boolean MSG_validn_modified = false;
    
    public boolean isModifiedMSG_validn() {
    	return this.MSG_validn_modified;
    }
    	
    public void unModifiedMSG_validn() {
    	this.MSG_validn_modified = false;
    }
    public FieldProperty getMSG_validnFieldProperty() {
    	return fieldPropertyMap.get("MSG_validn");
    }
    
    public String getMSG_validn() {
    	return MSG_validn;
    }	
    public String getNvlMSG_validn() {
    	if(getMSG_validn() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_validn();
    	}
    }
    public void setMSG_validn(String MSG_validn) {
    	if(MSG_validn == null) {
    		this.MSG_validn = null;
    	} else {
    		this.MSG_validn = MSG_validn;
    	}
    	this.MSG_validn_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_sosi
     * Comments    : 
     */	
    private String MSG_sosi = null;
    
    private transient boolean MSG_sosi_nullable = true;
    
    public boolean isNullableMSG_sosi() {
    	return this.MSG_sosi_nullable;
    }
    
    private transient boolean MSG_sosi_invalidation = false;
    	
    public void setInvalidationMSG_sosi(boolean invalidation) { 
    	this.MSG_sosi_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_sosi() {
    	return this.MSG_sosi_invalidation;
    }
    	
    private transient boolean MSG_sosi_modified = false;
    
    public boolean isModifiedMSG_sosi() {
    	return this.MSG_sosi_modified;
    }
    	
    public void unModifiedMSG_sosi() {
    	this.MSG_sosi_modified = false;
    }
    public FieldProperty getMSG_sosiFieldProperty() {
    	return fieldPropertyMap.get("MSG_sosi");
    }
    
    public String getMSG_sosi() {
    	return MSG_sosi;
    }	
    public String getNvlMSG_sosi() {
    	if(getMSG_sosi() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_sosi();
    	}
    }
    public void setMSG_sosi(String MSG_sosi) {
    	if(MSG_sosi == null) {
    		this.MSG_sosi = null;
    	} else {
    		this.MSG_sosi = MSG_sosi;
    	}
    	this.MSG_sosi_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : MSG_ps
     * Comments    : 
     */	
    private String MSG_ps = null;
    
    private transient boolean MSG_ps_nullable = true;
    
    public boolean isNullableMSG_ps() {
    	return this.MSG_ps_nullable;
    }
    
    private transient boolean MSG_ps_invalidation = false;
    	
    public void setInvalidationMSG_ps(boolean invalidation) { 
    	this.MSG_ps_invalidation = invalidation;
    }
    	
    public boolean isInvalidationMSG_ps() {
    	return this.MSG_ps_invalidation;
    }
    	
    private transient boolean MSG_ps_modified = false;
    
    public boolean isModifiedMSG_ps() {
    	return this.MSG_ps_modified;
    }
    	
    public void unModifiedMSG_ps() {
    	this.MSG_ps_modified = false;
    }
    public FieldProperty getMSG_psFieldProperty() {
    	return fieldPropertyMap.get("MSG_ps");
    }
    
    public String getMSG_ps() {
    	return MSG_ps;
    }	
    public String getNvlMSG_ps() {
    	if(getMSG_ps() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getMSG_ps();
    	}
    }
    public void setMSG_ps(String MSG_ps) {
    	if(MSG_ps == null) {
    		this.MSG_ps = null;
    	} else {
    		this.MSG_ps = MSG_ps;
    	}
    	this.MSG_ps_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DATE_length_modified = false;
    	this.DATE_color_modified = false;
    	this.DATE_hilight_modified = false;
    	this.DATE_outline_modified = false;
    	this.DATE_transp_modified = false;
    	this.DATE_validn_modified = false;
    	this.DATE_sosi_modified = false;
    	this.DATE_ps_modified = false;
    	this.TERM_length_modified = false;
    	this.TERM_color_modified = false;
    	this.TERM_hilight_modified = false;
    	this.TERM_outline_modified = false;
    	this.TERM_transp_modified = false;
    	this.TERM_validn_modified = false;
    	this.TERM_sosi_modified = false;
    	this.TERM_ps_modified = false;
    	this.TIME_length_modified = false;
    	this.TIME_color_modified = false;
    	this.TIME_hilight_modified = false;
    	this.TIME_outline_modified = false;
    	this.TIME_transp_modified = false;
    	this.TIME_validn_modified = false;
    	this.TIME_sosi_modified = false;
    	this.TIME_ps_modified = false;
    	this.CODE_length_modified = false;
    	this.CODE_color_modified = false;
    	this.CODE_hilight_modified = false;
    	this.CODE_outline_modified = false;
    	this.CODE_transp_modified = false;
    	this.CODE_validn_modified = false;
    	this.CODE_sosi_modified = false;
    	this.CODE_ps_modified = false;
    	this.IDEN_length_modified = false;
    	this.IDEN_color_modified = false;
    	this.IDEN_hilight_modified = false;
    	this.IDEN_outline_modified = false;
    	this.IDEN_transp_modified = false;
    	this.IDEN_validn_modified = false;
    	this.IDEN_sosi_modified = false;
    	this.IDEN_ps_modified = false;
    	this.MSG_length_modified = false;
    	this.MSG_color_modified = false;
    	this.MSG_hilight_modified = false;
    	this.MSG_outline_modified = false;
    	this.MSG_transp_modified = false;
    	this.MSG_validn_modified = false;
    	this.MSG_sosi_modified = false;
    	this.MSG_ps_modified = false;
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DATE_length_modified == true)
    		modifiedFields.add("DATE_length");
    	if(this.DATE_color_modified == true)
    		modifiedFields.add("DATE_color");
    	if(this.DATE_hilight_modified == true)
    		modifiedFields.add("DATE_hilight");
    	if(this.DATE_outline_modified == true)
    		modifiedFields.add("DATE_outline");
    	if(this.DATE_transp_modified == true)
    		modifiedFields.add("DATE_transp");
    	if(this.DATE_validn_modified == true)
    		modifiedFields.add("DATE_validn");
    	if(this.DATE_sosi_modified == true)
    		modifiedFields.add("DATE_sosi");
    	if(this.DATE_ps_modified == true)
    		modifiedFields.add("DATE_ps");
    	if(this.TERM_length_modified == true)
    		modifiedFields.add("TERM_length");
    	if(this.TERM_color_modified == true)
    		modifiedFields.add("TERM_color");
    	if(this.TERM_hilight_modified == true)
    		modifiedFields.add("TERM_hilight");
    	if(this.TERM_outline_modified == true)
    		modifiedFields.add("TERM_outline");
    	if(this.TERM_transp_modified == true)
    		modifiedFields.add("TERM_transp");
    	if(this.TERM_validn_modified == true)
    		modifiedFields.add("TERM_validn");
    	if(this.TERM_sosi_modified == true)
    		modifiedFields.add("TERM_sosi");
    	if(this.TERM_ps_modified == true)
    		modifiedFields.add("TERM_ps");
    	if(this.TIME_length_modified == true)
    		modifiedFields.add("TIME_length");
    	if(this.TIME_color_modified == true)
    		modifiedFields.add("TIME_color");
    	if(this.TIME_hilight_modified == true)
    		modifiedFields.add("TIME_hilight");
    	if(this.TIME_outline_modified == true)
    		modifiedFields.add("TIME_outline");
    	if(this.TIME_transp_modified == true)
    		modifiedFields.add("TIME_transp");
    	if(this.TIME_validn_modified == true)
    		modifiedFields.add("TIME_validn");
    	if(this.TIME_sosi_modified == true)
    		modifiedFields.add("TIME_sosi");
    	if(this.TIME_ps_modified == true)
    		modifiedFields.add("TIME_ps");
    	if(this.CODE_length_modified == true)
    		modifiedFields.add("CODE_length");
    	if(this.CODE_color_modified == true)
    		modifiedFields.add("CODE_color");
    	if(this.CODE_hilight_modified == true)
    		modifiedFields.add("CODE_hilight");
    	if(this.CODE_outline_modified == true)
    		modifiedFields.add("CODE_outline");
    	if(this.CODE_transp_modified == true)
    		modifiedFields.add("CODE_transp");
    	if(this.CODE_validn_modified == true)
    		modifiedFields.add("CODE_validn");
    	if(this.CODE_sosi_modified == true)
    		modifiedFields.add("CODE_sosi");
    	if(this.CODE_ps_modified == true)
    		modifiedFields.add("CODE_ps");
    	if(this.IDEN_length_modified == true)
    		modifiedFields.add("IDEN_length");
    	if(this.IDEN_color_modified == true)
    		modifiedFields.add("IDEN_color");
    	if(this.IDEN_hilight_modified == true)
    		modifiedFields.add("IDEN_hilight");
    	if(this.IDEN_outline_modified == true)
    		modifiedFields.add("IDEN_outline");
    	if(this.IDEN_transp_modified == true)
    		modifiedFields.add("IDEN_transp");
    	if(this.IDEN_validn_modified == true)
    		modifiedFields.add("IDEN_validn");
    	if(this.IDEN_sosi_modified == true)
    		modifiedFields.add("IDEN_sosi");
    	if(this.IDEN_ps_modified == true)
    		modifiedFields.add("IDEN_ps");
    	if(this.MSG_length_modified == true)
    		modifiedFields.add("MSG_length");
    	if(this.MSG_color_modified == true)
    		modifiedFields.add("MSG_color");
    	if(this.MSG_hilight_modified == true)
    		modifiedFields.add("MSG_hilight");
    	if(this.MSG_outline_modified == true)
    		modifiedFields.add("MSG_outline");
    	if(this.MSG_transp_modified == true)
    		modifiedFields.add("MSG_transp");
    	if(this.MSG_validn_modified == true)
    		modifiedFields.add("MSG_validn");
    	if(this.MSG_sosi_modified == true)
    		modifiedFields.add("MSG_sosi");
    	if(this.MSG_ps_modified == true)
    		modifiedFields.add("MSG_ps");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPM01_OIVPM01_meta copyObj = new OIVPM01_OIVPM01_meta();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPM01_OIVPM01_meta) {
    	if(this == _oIVPM01_OIVPM01_meta) return;
    	OIVPM01_OIVPM01_meta __oIVPM01_OIVPM01_meta = (OIVPM01_OIVPM01_meta) _oIVPM01_OIVPM01_meta;
    	this.setDATE_length(__oIVPM01_OIVPM01_meta.getDATE_length());
    	this.setDATE_color(__oIVPM01_OIVPM01_meta.getDATE_color());
    	this.setDATE_hilight(__oIVPM01_OIVPM01_meta.getDATE_hilight());
    	this.setDATE_outline(__oIVPM01_OIVPM01_meta.getDATE_outline());
    	this.setDATE_transp(__oIVPM01_OIVPM01_meta.getDATE_transp());
    	this.setDATE_validn(__oIVPM01_OIVPM01_meta.getDATE_validn());
    	this.setDATE_sosi(__oIVPM01_OIVPM01_meta.getDATE_sosi());
    	this.setDATE_ps(__oIVPM01_OIVPM01_meta.getDATE_ps());
    	this.setTERM_length(__oIVPM01_OIVPM01_meta.getTERM_length());
    	this.setTERM_color(__oIVPM01_OIVPM01_meta.getTERM_color());
    	this.setTERM_hilight(__oIVPM01_OIVPM01_meta.getTERM_hilight());
    	this.setTERM_outline(__oIVPM01_OIVPM01_meta.getTERM_outline());
    	this.setTERM_transp(__oIVPM01_OIVPM01_meta.getTERM_transp());
    	this.setTERM_validn(__oIVPM01_OIVPM01_meta.getTERM_validn());
    	this.setTERM_sosi(__oIVPM01_OIVPM01_meta.getTERM_sosi());
    	this.setTERM_ps(__oIVPM01_OIVPM01_meta.getTERM_ps());
    	this.setTIME_length(__oIVPM01_OIVPM01_meta.getTIME_length());
    	this.setTIME_color(__oIVPM01_OIVPM01_meta.getTIME_color());
    	this.setTIME_hilight(__oIVPM01_OIVPM01_meta.getTIME_hilight());
    	this.setTIME_outline(__oIVPM01_OIVPM01_meta.getTIME_outline());
    	this.setTIME_transp(__oIVPM01_OIVPM01_meta.getTIME_transp());
    	this.setTIME_validn(__oIVPM01_OIVPM01_meta.getTIME_validn());
    	this.setTIME_sosi(__oIVPM01_OIVPM01_meta.getTIME_sosi());
    	this.setTIME_ps(__oIVPM01_OIVPM01_meta.getTIME_ps());
    	this.setCODE_length(__oIVPM01_OIVPM01_meta.getCODE_length());
    	this.setCODE_color(__oIVPM01_OIVPM01_meta.getCODE_color());
    	this.setCODE_hilight(__oIVPM01_OIVPM01_meta.getCODE_hilight());
    	this.setCODE_outline(__oIVPM01_OIVPM01_meta.getCODE_outline());
    	this.setCODE_transp(__oIVPM01_OIVPM01_meta.getCODE_transp());
    	this.setCODE_validn(__oIVPM01_OIVPM01_meta.getCODE_validn());
    	this.setCODE_sosi(__oIVPM01_OIVPM01_meta.getCODE_sosi());
    	this.setCODE_ps(__oIVPM01_OIVPM01_meta.getCODE_ps());
    	this.setIDEN_length(__oIVPM01_OIVPM01_meta.getIDEN_length());
    	this.setIDEN_color(__oIVPM01_OIVPM01_meta.getIDEN_color());
    	this.setIDEN_hilight(__oIVPM01_OIVPM01_meta.getIDEN_hilight());
    	this.setIDEN_outline(__oIVPM01_OIVPM01_meta.getIDEN_outline());
    	this.setIDEN_transp(__oIVPM01_OIVPM01_meta.getIDEN_transp());
    	this.setIDEN_validn(__oIVPM01_OIVPM01_meta.getIDEN_validn());
    	this.setIDEN_sosi(__oIVPM01_OIVPM01_meta.getIDEN_sosi());
    	this.setIDEN_ps(__oIVPM01_OIVPM01_meta.getIDEN_ps());
    	this.setMSG_length(__oIVPM01_OIVPM01_meta.getMSG_length());
    	this.setMSG_color(__oIVPM01_OIVPM01_meta.getMSG_color());
    	this.setMSG_hilight(__oIVPM01_OIVPM01_meta.getMSG_hilight());
    	this.setMSG_outline(__oIVPM01_OIVPM01_meta.getMSG_outline());
    	this.setMSG_transp(__oIVPM01_OIVPM01_meta.getMSG_transp());
    	this.setMSG_validn(__oIVPM01_OIVPM01_meta.getMSG_validn());
    	this.setMSG_sosi(__oIVPM01_OIVPM01_meta.getMSG_sosi());
    	this.setMSG_ps(__oIVPM01_OIVPM01_meta.getMSG_ps());
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DATE_length : ").append(DATE_length).append("\n");	
    	buffer.append("DATE_color : ").append(DATE_color).append("\n");	
    	buffer.append("DATE_hilight : ").append(DATE_hilight).append("\n");	
    	buffer.append("DATE_outline : ").append(DATE_outline).append("\n");	
    	buffer.append("DATE_transp : ").append(DATE_transp).append("\n");	
    	buffer.append("DATE_validn : ").append(DATE_validn).append("\n");	
    	buffer.append("DATE_sosi : ").append(DATE_sosi).append("\n");	
    	buffer.append("DATE_ps : ").append(DATE_ps).append("\n");	
    	buffer.append("TERM_length : ").append(TERM_length).append("\n");	
    	buffer.append("TERM_color : ").append(TERM_color).append("\n");	
    	buffer.append("TERM_hilight : ").append(TERM_hilight).append("\n");	
    	buffer.append("TERM_outline : ").append(TERM_outline).append("\n");	
    	buffer.append("TERM_transp : ").append(TERM_transp).append("\n");	
    	buffer.append("TERM_validn : ").append(TERM_validn).append("\n");	
    	buffer.append("TERM_sosi : ").append(TERM_sosi).append("\n");	
    	buffer.append("TERM_ps : ").append(TERM_ps).append("\n");	
    	buffer.append("TIME_length : ").append(TIME_length).append("\n");	
    	buffer.append("TIME_color : ").append(TIME_color).append("\n");	
    	buffer.append("TIME_hilight : ").append(TIME_hilight).append("\n");	
    	buffer.append("TIME_outline : ").append(TIME_outline).append("\n");	
    	buffer.append("TIME_transp : ").append(TIME_transp).append("\n");	
    	buffer.append("TIME_validn : ").append(TIME_validn).append("\n");	
    	buffer.append("TIME_sosi : ").append(TIME_sosi).append("\n");	
    	buffer.append("TIME_ps : ").append(TIME_ps).append("\n");	
    	buffer.append("CODE_length : ").append(CODE_length).append("\n");	
    	buffer.append("CODE_color : ").append(CODE_color).append("\n");	
    	buffer.append("CODE_hilight : ").append(CODE_hilight).append("\n");	
    	buffer.append("CODE_outline : ").append(CODE_outline).append("\n");	
    	buffer.append("CODE_transp : ").append(CODE_transp).append("\n");	
    	buffer.append("CODE_validn : ").append(CODE_validn).append("\n");	
    	buffer.append("CODE_sosi : ").append(CODE_sosi).append("\n");	
    	buffer.append("CODE_ps : ").append(CODE_ps).append("\n");	
    	buffer.append("IDEN_length : ").append(IDEN_length).append("\n");	
    	buffer.append("IDEN_color : ").append(IDEN_color).append("\n");	
    	buffer.append("IDEN_hilight : ").append(IDEN_hilight).append("\n");	
    	buffer.append("IDEN_outline : ").append(IDEN_outline).append("\n");	
    	buffer.append("IDEN_transp : ").append(IDEN_transp).append("\n");	
    	buffer.append("IDEN_validn : ").append(IDEN_validn).append("\n");	
    	buffer.append("IDEN_sosi : ").append(IDEN_sosi).append("\n");	
    	buffer.append("IDEN_ps : ").append(IDEN_ps).append("\n");	
    	buffer.append("MSG_length : ").append(MSG_length).append("\n");	
    	buffer.append("MSG_color : ").append(MSG_color).append("\n");	
    	buffer.append("MSG_hilight : ").append(MSG_hilight).append("\n");	
    	buffer.append("MSG_outline : ").append(MSG_outline).append("\n");	
    	buffer.append("MSG_transp : ").append(MSG_transp).append("\n");	
    	buffer.append("MSG_validn : ").append(MSG_validn).append("\n");	
    	buffer.append("MSG_sosi : ").append(MSG_sosi).append("\n");	
    	buffer.append("MSG_ps : ").append(MSG_ps).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(48);
    	fieldPropertyMap.put("DATE_length", FieldProperty.builder()
    	              .setPhysicalName("DATE_length")
    	              .setLogicalName("DATE_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("DATE_color", FieldProperty.builder()
    	              .setPhysicalName("DATE_color")
    	              .setLogicalName("DATE_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_hilight", FieldProperty.builder()
    	              .setPhysicalName("DATE_hilight")
    	              .setLogicalName("DATE_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_outline", FieldProperty.builder()
    	              .setPhysicalName("DATE_outline")
    	              .setLogicalName("DATE_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_transp", FieldProperty.builder()
    	              .setPhysicalName("DATE_transp")
    	              .setLogicalName("DATE_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_validn", FieldProperty.builder()
    	              .setPhysicalName("DATE_validn")
    	              .setLogicalName("DATE_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_sosi", FieldProperty.builder()
    	              .setPhysicalName("DATE_sosi")
    	              .setLogicalName("DATE_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("DATE_ps", FieldProperty.builder()
    	              .setPhysicalName("DATE_ps")
    	              .setLogicalName("DATE_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_length", FieldProperty.builder()
    	              .setPhysicalName("TERM_length")
    	              .setLogicalName("TERM_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TERM_color", FieldProperty.builder()
    	              .setPhysicalName("TERM_color")
    	              .setLogicalName("TERM_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_hilight", FieldProperty.builder()
    	              .setPhysicalName("TERM_hilight")
    	              .setLogicalName("TERM_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_outline", FieldProperty.builder()
    	              .setPhysicalName("TERM_outline")
    	              .setLogicalName("TERM_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_transp", FieldProperty.builder()
    	              .setPhysicalName("TERM_transp")
    	              .setLogicalName("TERM_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_validn", FieldProperty.builder()
    	              .setPhysicalName("TERM_validn")
    	              .setLogicalName("TERM_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_sosi", FieldProperty.builder()
    	              .setPhysicalName("TERM_sosi")
    	              .setLogicalName("TERM_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TERM_ps", FieldProperty.builder()
    	              .setPhysicalName("TERM_ps")
    	              .setLogicalName("TERM_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_length", FieldProperty.builder()
    	              .setPhysicalName("TIME_length")
    	              .setLogicalName("TIME_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("TIME_color", FieldProperty.builder()
    	              .setPhysicalName("TIME_color")
    	              .setLogicalName("TIME_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_hilight", FieldProperty.builder()
    	              .setPhysicalName("TIME_hilight")
    	              .setLogicalName("TIME_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_outline", FieldProperty.builder()
    	              .setPhysicalName("TIME_outline")
    	              .setLogicalName("TIME_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_transp", FieldProperty.builder()
    	              .setPhysicalName("TIME_transp")
    	              .setLogicalName("TIME_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_validn", FieldProperty.builder()
    	              .setPhysicalName("TIME_validn")
    	              .setLogicalName("TIME_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_sosi", FieldProperty.builder()
    	              .setPhysicalName("TIME_sosi")
    	              .setLogicalName("TIME_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("TIME_ps", FieldProperty.builder()
    	              .setPhysicalName("TIME_ps")
    	              .setLogicalName("TIME_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_length", FieldProperty.builder()
    	              .setPhysicalName("CODE_length")
    	              .setLogicalName("CODE_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("CODE_color", FieldProperty.builder()
    	              .setPhysicalName("CODE_color")
    	              .setLogicalName("CODE_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_hilight", FieldProperty.builder()
    	              .setPhysicalName("CODE_hilight")
    	              .setLogicalName("CODE_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_outline", FieldProperty.builder()
    	              .setPhysicalName("CODE_outline")
    	              .setLogicalName("CODE_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_transp", FieldProperty.builder()
    	              .setPhysicalName("CODE_transp")
    	              .setLogicalName("CODE_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_validn", FieldProperty.builder()
    	              .setPhysicalName("CODE_validn")
    	              .setLogicalName("CODE_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_sosi", FieldProperty.builder()
    	              .setPhysicalName("CODE_sosi")
    	              .setLogicalName("CODE_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("CODE_ps", FieldProperty.builder()
    	              .setPhysicalName("CODE_ps")
    	              .setLogicalName("CODE_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_length", FieldProperty.builder()
    	              .setPhysicalName("IDEN_length")
    	              .setLogicalName("IDEN_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("IDEN_color", FieldProperty.builder()
    	              .setPhysicalName("IDEN_color")
    	              .setLogicalName("IDEN_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_hilight", FieldProperty.builder()
    	              .setPhysicalName("IDEN_hilight")
    	              .setLogicalName("IDEN_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_outline", FieldProperty.builder()
    	              .setPhysicalName("IDEN_outline")
    	              .setLogicalName("IDEN_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_transp", FieldProperty.builder()
    	              .setPhysicalName("IDEN_transp")
    	              .setLogicalName("IDEN_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_validn", FieldProperty.builder()
    	              .setPhysicalName("IDEN_validn")
    	              .setLogicalName("IDEN_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_sosi", FieldProperty.builder()
    	              .setPhysicalName("IDEN_sosi")
    	              .setLogicalName("IDEN_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("IDEN_ps", FieldProperty.builder()
    	              .setPhysicalName("IDEN_ps")
    	              .setLogicalName("IDEN_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_length", FieldProperty.builder()
    	              .setPhysicalName("MSG_length")
    	              .setLogicalName("MSG_length")
    	              .setType(FieldProperty.TYPE_PRIMITIVE_INT)
    	              .setLength(5)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("int")
    	              .build());
    	fieldPropertyMap.put("MSG_color", FieldProperty.builder()
    	              .setPhysicalName("MSG_color")
    	              .setLogicalName("MSG_color")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_hilight", FieldProperty.builder()
    	              .setPhysicalName("MSG_hilight")
    	              .setLogicalName("MSG_hilight")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_outline", FieldProperty.builder()
    	              .setPhysicalName("MSG_outline")
    	              .setLogicalName("MSG_outline")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_transp", FieldProperty.builder()
    	              .setPhysicalName("MSG_transp")
    	              .setLogicalName("MSG_transp")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_validn", FieldProperty.builder()
    	              .setPhysicalName("MSG_validn")
    	              .setLogicalName("MSG_validn")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_sosi", FieldProperty.builder()
    	              .setPhysicalName("MSG_sosi")
    	              .setLogicalName("MSG_sosi")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    	fieldPropertyMap.put("MSG_ps", FieldProperty.builder()
    	              .setPhysicalName("MSG_ps")
    	              .setLogicalName("MSG_ps")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(10)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("String")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : return getDATE_length();
    		case "DATE_color" : return getDATE_color();
    		case "DATE_hilight" : return getDATE_hilight();
    		case "DATE_outline" : return getDATE_outline();
    		case "DATE_transp" : return getDATE_transp();
    		case "DATE_validn" : return getDATE_validn();
    		case "DATE_sosi" : return getDATE_sosi();
    		case "DATE_ps" : return getDATE_ps();
    		case "TERM_length" : return getTERM_length();
    		case "TERM_color" : return getTERM_color();
    		case "TERM_hilight" : return getTERM_hilight();
    		case "TERM_outline" : return getTERM_outline();
    		case "TERM_transp" : return getTERM_transp();
    		case "TERM_validn" : return getTERM_validn();
    		case "TERM_sosi" : return getTERM_sosi();
    		case "TERM_ps" : return getTERM_ps();
    		case "TIME_length" : return getTIME_length();
    		case "TIME_color" : return getTIME_color();
    		case "TIME_hilight" : return getTIME_hilight();
    		case "TIME_outline" : return getTIME_outline();
    		case "TIME_transp" : return getTIME_transp();
    		case "TIME_validn" : return getTIME_validn();
    		case "TIME_sosi" : return getTIME_sosi();
    		case "TIME_ps" : return getTIME_ps();
    		case "CODE_length" : return getCODE_length();
    		case "CODE_color" : return getCODE_color();
    		case "CODE_hilight" : return getCODE_hilight();
    		case "CODE_outline" : return getCODE_outline();
    		case "CODE_transp" : return getCODE_transp();
    		case "CODE_validn" : return getCODE_validn();
    		case "CODE_sosi" : return getCODE_sosi();
    		case "CODE_ps" : return getCODE_ps();
    		case "IDEN_length" : return getIDEN_length();
    		case "IDEN_color" : return getIDEN_color();
    		case "IDEN_hilight" : return getIDEN_hilight();
    		case "IDEN_outline" : return getIDEN_outline();
    		case "IDEN_transp" : return getIDEN_transp();
    		case "IDEN_validn" : return getIDEN_validn();
    		case "IDEN_sosi" : return getIDEN_sosi();
    		case "IDEN_ps" : return getIDEN_ps();
    		case "MSG_length" : return getMSG_length();
    		case "MSG_color" : return getMSG_color();
    		case "MSG_hilight" : return getMSG_hilight();
    		case "MSG_outline" : return getMSG_outline();
    		case "MSG_transp" : return getMSG_transp();
    		case "MSG_validn" : return getMSG_validn();
    		case "MSG_sosi" : return getMSG_sosi();
    		case "MSG_ps" : return getMSG_ps();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DATE_length" : setDATE_length((Integer)arg);break;
    		case "DATE_color" : setDATE_color((String)arg); break;
    		case "DATE_hilight" : setDATE_hilight((String)arg); break;
    		case "DATE_outline" : setDATE_outline((String)arg); break;
    		case "DATE_transp" : setDATE_transp((String)arg); break;
    		case "DATE_validn" : setDATE_validn((String)arg); break;
    		case "DATE_sosi" : setDATE_sosi((String)arg); break;
    		case "DATE_ps" : setDATE_ps((String)arg); break;
    		case "TERM_length" : setTERM_length((Integer)arg);break;
    		case "TERM_color" : setTERM_color((String)arg); break;
    		case "TERM_hilight" : setTERM_hilight((String)arg); break;
    		case "TERM_outline" : setTERM_outline((String)arg); break;
    		case "TERM_transp" : setTERM_transp((String)arg); break;
    		case "TERM_validn" : setTERM_validn((String)arg); break;
    		case "TERM_sosi" : setTERM_sosi((String)arg); break;
    		case "TERM_ps" : setTERM_ps((String)arg); break;
    		case "TIME_length" : setTIME_length((Integer)arg);break;
    		case "TIME_color" : setTIME_color((String)arg); break;
    		case "TIME_hilight" : setTIME_hilight((String)arg); break;
    		case "TIME_outline" : setTIME_outline((String)arg); break;
    		case "TIME_transp" : setTIME_transp((String)arg); break;
    		case "TIME_validn" : setTIME_validn((String)arg); break;
    		case "TIME_sosi" : setTIME_sosi((String)arg); break;
    		case "TIME_ps" : setTIME_ps((String)arg); break;
    		case "CODE_length" : setCODE_length((Integer)arg);break;
    		case "CODE_color" : setCODE_color((String)arg); break;
    		case "CODE_hilight" : setCODE_hilight((String)arg); break;
    		case "CODE_outline" : setCODE_outline((String)arg); break;
    		case "CODE_transp" : setCODE_transp((String)arg); break;
    		case "CODE_validn" : setCODE_validn((String)arg); break;
    		case "CODE_sosi" : setCODE_sosi((String)arg); break;
    		case "CODE_ps" : setCODE_ps((String)arg); break;
    		case "IDEN_length" : setIDEN_length((Integer)arg);break;
    		case "IDEN_color" : setIDEN_color((String)arg); break;
    		case "IDEN_hilight" : setIDEN_hilight((String)arg); break;
    		case "IDEN_outline" : setIDEN_outline((String)arg); break;
    		case "IDEN_transp" : setIDEN_transp((String)arg); break;
    		case "IDEN_validn" : setIDEN_validn((String)arg); break;
    		case "IDEN_sosi" : setIDEN_sosi((String)arg); break;
    		case "IDEN_ps" : setIDEN_ps((String)arg); break;
    		case "MSG_length" : setMSG_length((Integer)arg);break;
    		case "MSG_color" : setMSG_color((String)arg); break;
    		case "MSG_hilight" : setMSG_hilight((String)arg); break;
    		case "MSG_outline" : setMSG_outline((String)arg); break;
    		case "MSG_transp" : setMSG_transp((String)arg); break;
    		case "MSG_validn" : setMSG_validn((String)arg); break;
    		case "MSG_sosi" : setMSG_sosi((String)arg); break;
    		case "MSG_ps" : setMSG_ps((String)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPM01_OIVPM01_meta _OIVPM01_OIVPM01_meta = (OIVPM01_OIVPM01_meta) obj;
    	if(this.DATE_length != _OIVPM01_OIVPM01_meta.getDATE_length()) return false;
    	if(this.DATE_color == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_color() != null)
    		return false;
    	} else if(!this.DATE_color.equals(_OIVPM01_OIVPM01_meta.getDATE_color()))
    		return false;
    	if(this.DATE_hilight == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_hilight() != null)
    		return false;
    	} else if(!this.DATE_hilight.equals(_OIVPM01_OIVPM01_meta.getDATE_hilight()))
    		return false;
    	if(this.DATE_outline == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_outline() != null)
    		return false;
    	} else if(!this.DATE_outline.equals(_OIVPM01_OIVPM01_meta.getDATE_outline()))
    		return false;
    	if(this.DATE_transp == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_transp() != null)
    		return false;
    	} else if(!this.DATE_transp.equals(_OIVPM01_OIVPM01_meta.getDATE_transp()))
    		return false;
    	if(this.DATE_validn == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_validn() != null)
    		return false;
    	} else if(!this.DATE_validn.equals(_OIVPM01_OIVPM01_meta.getDATE_validn()))
    		return false;
    	if(this.DATE_sosi == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_sosi() != null)
    		return false;
    	} else if(!this.DATE_sosi.equals(_OIVPM01_OIVPM01_meta.getDATE_sosi()))
    		return false;
    	if(this.DATE_ps == null) {
    	if(_OIVPM01_OIVPM01_meta.getDATE_ps() != null)
    		return false;
    	} else if(!this.DATE_ps.equals(_OIVPM01_OIVPM01_meta.getDATE_ps()))
    		return false;
    	if(this.TERM_length != _OIVPM01_OIVPM01_meta.getTERM_length()) return false;
    	if(this.TERM_color == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_color() != null)
    		return false;
    	} else if(!this.TERM_color.equals(_OIVPM01_OIVPM01_meta.getTERM_color()))
    		return false;
    	if(this.TERM_hilight == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_hilight() != null)
    		return false;
    	} else if(!this.TERM_hilight.equals(_OIVPM01_OIVPM01_meta.getTERM_hilight()))
    		return false;
    	if(this.TERM_outline == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_outline() != null)
    		return false;
    	} else if(!this.TERM_outline.equals(_OIVPM01_OIVPM01_meta.getTERM_outline()))
    		return false;
    	if(this.TERM_transp == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_transp() != null)
    		return false;
    	} else if(!this.TERM_transp.equals(_OIVPM01_OIVPM01_meta.getTERM_transp()))
    		return false;
    	if(this.TERM_validn == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_validn() != null)
    		return false;
    	} else if(!this.TERM_validn.equals(_OIVPM01_OIVPM01_meta.getTERM_validn()))
    		return false;
    	if(this.TERM_sosi == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_sosi() != null)
    		return false;
    	} else if(!this.TERM_sosi.equals(_OIVPM01_OIVPM01_meta.getTERM_sosi()))
    		return false;
    	if(this.TERM_ps == null) {
    	if(_OIVPM01_OIVPM01_meta.getTERM_ps() != null)
    		return false;
    	} else if(!this.TERM_ps.equals(_OIVPM01_OIVPM01_meta.getTERM_ps()))
    		return false;
    	if(this.TIME_length != _OIVPM01_OIVPM01_meta.getTIME_length()) return false;
    	if(this.TIME_color == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_color() != null)
    		return false;
    	} else if(!this.TIME_color.equals(_OIVPM01_OIVPM01_meta.getTIME_color()))
    		return false;
    	if(this.TIME_hilight == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_hilight() != null)
    		return false;
    	} else if(!this.TIME_hilight.equals(_OIVPM01_OIVPM01_meta.getTIME_hilight()))
    		return false;
    	if(this.TIME_outline == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_outline() != null)
    		return false;
    	} else if(!this.TIME_outline.equals(_OIVPM01_OIVPM01_meta.getTIME_outline()))
    		return false;
    	if(this.TIME_transp == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_transp() != null)
    		return false;
    	} else if(!this.TIME_transp.equals(_OIVPM01_OIVPM01_meta.getTIME_transp()))
    		return false;
    	if(this.TIME_validn == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_validn() != null)
    		return false;
    	} else if(!this.TIME_validn.equals(_OIVPM01_OIVPM01_meta.getTIME_validn()))
    		return false;
    	if(this.TIME_sosi == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_sosi() != null)
    		return false;
    	} else if(!this.TIME_sosi.equals(_OIVPM01_OIVPM01_meta.getTIME_sosi()))
    		return false;
    	if(this.TIME_ps == null) {
    	if(_OIVPM01_OIVPM01_meta.getTIME_ps() != null)
    		return false;
    	} else if(!this.TIME_ps.equals(_OIVPM01_OIVPM01_meta.getTIME_ps()))
    		return false;
    	if(this.CODE_length != _OIVPM01_OIVPM01_meta.getCODE_length()) return false;
    	if(this.CODE_color == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_color() != null)
    		return false;
    	} else if(!this.CODE_color.equals(_OIVPM01_OIVPM01_meta.getCODE_color()))
    		return false;
    	if(this.CODE_hilight == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_hilight() != null)
    		return false;
    	} else if(!this.CODE_hilight.equals(_OIVPM01_OIVPM01_meta.getCODE_hilight()))
    		return false;
    	if(this.CODE_outline == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_outline() != null)
    		return false;
    	} else if(!this.CODE_outline.equals(_OIVPM01_OIVPM01_meta.getCODE_outline()))
    		return false;
    	if(this.CODE_transp == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_transp() != null)
    		return false;
    	} else if(!this.CODE_transp.equals(_OIVPM01_OIVPM01_meta.getCODE_transp()))
    		return false;
    	if(this.CODE_validn == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_validn() != null)
    		return false;
    	} else if(!this.CODE_validn.equals(_OIVPM01_OIVPM01_meta.getCODE_validn()))
    		return false;
    	if(this.CODE_sosi == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_sosi() != null)
    		return false;
    	} else if(!this.CODE_sosi.equals(_OIVPM01_OIVPM01_meta.getCODE_sosi()))
    		return false;
    	if(this.CODE_ps == null) {
    	if(_OIVPM01_OIVPM01_meta.getCODE_ps() != null)
    		return false;
    	} else if(!this.CODE_ps.equals(_OIVPM01_OIVPM01_meta.getCODE_ps()))
    		return false;
    	if(this.IDEN_length != _OIVPM01_OIVPM01_meta.getIDEN_length()) return false;
    	if(this.IDEN_color == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_color() != null)
    		return false;
    	} else if(!this.IDEN_color.equals(_OIVPM01_OIVPM01_meta.getIDEN_color()))
    		return false;
    	if(this.IDEN_hilight == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_hilight() != null)
    		return false;
    	} else if(!this.IDEN_hilight.equals(_OIVPM01_OIVPM01_meta.getIDEN_hilight()))
    		return false;
    	if(this.IDEN_outline == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_outline() != null)
    		return false;
    	} else if(!this.IDEN_outline.equals(_OIVPM01_OIVPM01_meta.getIDEN_outline()))
    		return false;
    	if(this.IDEN_transp == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_transp() != null)
    		return false;
    	} else if(!this.IDEN_transp.equals(_OIVPM01_OIVPM01_meta.getIDEN_transp()))
    		return false;
    	if(this.IDEN_validn == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_validn() != null)
    		return false;
    	} else if(!this.IDEN_validn.equals(_OIVPM01_OIVPM01_meta.getIDEN_validn()))
    		return false;
    	if(this.IDEN_sosi == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_sosi() != null)
    		return false;
    	} else if(!this.IDEN_sosi.equals(_OIVPM01_OIVPM01_meta.getIDEN_sosi()))
    		return false;
    	if(this.IDEN_ps == null) {
    	if(_OIVPM01_OIVPM01_meta.getIDEN_ps() != null)
    		return false;
    	} else if(!this.IDEN_ps.equals(_OIVPM01_OIVPM01_meta.getIDEN_ps()))
    		return false;
    	if(this.MSG_length != _OIVPM01_OIVPM01_meta.getMSG_length()) return false;
    	if(this.MSG_color == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_color() != null)
    		return false;
    	} else if(!this.MSG_color.equals(_OIVPM01_OIVPM01_meta.getMSG_color()))
    		return false;
    	if(this.MSG_hilight == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_hilight() != null)
    		return false;
    	} else if(!this.MSG_hilight.equals(_OIVPM01_OIVPM01_meta.getMSG_hilight()))
    		return false;
    	if(this.MSG_outline == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_outline() != null)
    		return false;
    	} else if(!this.MSG_outline.equals(_OIVPM01_OIVPM01_meta.getMSG_outline()))
    		return false;
    	if(this.MSG_transp == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_transp() != null)
    		return false;
    	} else if(!this.MSG_transp.equals(_OIVPM01_OIVPM01_meta.getMSG_transp()))
    		return false;
    	if(this.MSG_validn == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_validn() != null)
    		return false;
    	} else if(!this.MSG_validn.equals(_OIVPM01_OIVPM01_meta.getMSG_validn()))
    		return false;
    	if(this.MSG_sosi == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_sosi() != null)
    		return false;
    	} else if(!this.MSG_sosi.equals(_OIVPM01_OIVPM01_meta.getMSG_sosi()))
    		return false;
    	if(this.MSG_ps == null) {
    	if(_OIVPM01_OIVPM01_meta.getMSG_ps() != null)
    		return false;
    	} else if(!this.MSG_ps.equals(_OIVPM01_OIVPM01_meta.getMSG_ps()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + this.DATE_length;
    	result = prime * result + ((this.DATE_color == null) ? 0 : this.DATE_color.hashCode());
    	result = prime * result + ((this.DATE_hilight == null) ? 0 : this.DATE_hilight.hashCode());
    	result = prime * result + ((this.DATE_outline == null) ? 0 : this.DATE_outline.hashCode());
    	result = prime * result + ((this.DATE_transp == null) ? 0 : this.DATE_transp.hashCode());
    	result = prime * result + ((this.DATE_validn == null) ? 0 : this.DATE_validn.hashCode());
    	result = prime * result + ((this.DATE_sosi == null) ? 0 : this.DATE_sosi.hashCode());
    	result = prime * result + ((this.DATE_ps == null) ? 0 : this.DATE_ps.hashCode());
    	result = prime * result + this.TERM_length;
    	result = prime * result + ((this.TERM_color == null) ? 0 : this.TERM_color.hashCode());
    	result = prime * result + ((this.TERM_hilight == null) ? 0 : this.TERM_hilight.hashCode());
    	result = prime * result + ((this.TERM_outline == null) ? 0 : this.TERM_outline.hashCode());
    	result = prime * result + ((this.TERM_transp == null) ? 0 : this.TERM_transp.hashCode());
    	result = prime * result + ((this.TERM_validn == null) ? 0 : this.TERM_validn.hashCode());
    	result = prime * result + ((this.TERM_sosi == null) ? 0 : this.TERM_sosi.hashCode());
    	result = prime * result + ((this.TERM_ps == null) ? 0 : this.TERM_ps.hashCode());
    	result = prime * result + this.TIME_length;
    	result = prime * result + ((this.TIME_color == null) ? 0 : this.TIME_color.hashCode());
    	result = prime * result + ((this.TIME_hilight == null) ? 0 : this.TIME_hilight.hashCode());
    	result = prime * result + ((this.TIME_outline == null) ? 0 : this.TIME_outline.hashCode());
    	result = prime * result + ((this.TIME_transp == null) ? 0 : this.TIME_transp.hashCode());
    	result = prime * result + ((this.TIME_validn == null) ? 0 : this.TIME_validn.hashCode());
    	result = prime * result + ((this.TIME_sosi == null) ? 0 : this.TIME_sosi.hashCode());
    	result = prime * result + ((this.TIME_ps == null) ? 0 : this.TIME_ps.hashCode());
    	result = prime * result + this.CODE_length;
    	result = prime * result + ((this.CODE_color == null) ? 0 : this.CODE_color.hashCode());
    	result = prime * result + ((this.CODE_hilight == null) ? 0 : this.CODE_hilight.hashCode());
    	result = prime * result + ((this.CODE_outline == null) ? 0 : this.CODE_outline.hashCode());
    	result = prime * result + ((this.CODE_transp == null) ? 0 : this.CODE_transp.hashCode());
    	result = prime * result + ((this.CODE_validn == null) ? 0 : this.CODE_validn.hashCode());
    	result = prime * result + ((this.CODE_sosi == null) ? 0 : this.CODE_sosi.hashCode());
    	result = prime * result + ((this.CODE_ps == null) ? 0 : this.CODE_ps.hashCode());
    	result = prime * result + this.IDEN_length;
    	result = prime * result + ((this.IDEN_color == null) ? 0 : this.IDEN_color.hashCode());
    	result = prime * result + ((this.IDEN_hilight == null) ? 0 : this.IDEN_hilight.hashCode());
    	result = prime * result + ((this.IDEN_outline == null) ? 0 : this.IDEN_outline.hashCode());
    	result = prime * result + ((this.IDEN_transp == null) ? 0 : this.IDEN_transp.hashCode());
    	result = prime * result + ((this.IDEN_validn == null) ? 0 : this.IDEN_validn.hashCode());
    	result = prime * result + ((this.IDEN_sosi == null) ? 0 : this.IDEN_sosi.hashCode());
    	result = prime * result + ((this.IDEN_ps == null) ? 0 : this.IDEN_ps.hashCode());
    	result = prime * result + this.MSG_length;
    	result = prime * result + ((this.MSG_color == null) ? 0 : this.MSG_color.hashCode());
    	result = prime * result + ((this.MSG_hilight == null) ? 0 : this.MSG_hilight.hashCode());
    	result = prime * result + ((this.MSG_outline == null) ? 0 : this.MSG_outline.hashCode());
    	result = prime * result + ((this.MSG_transp == null) ? 0 : this.MSG_transp.hashCode());
    	result = prime * result + ((this.MSG_validn == null) ? 0 : this.MSG_validn.hashCode());
    	result = prime * result + ((this.MSG_sosi == null) ? 0 : this.MSG_sosi.hashCode());
    	result = prime * result + ((this.MSG_ps == null) ? 0 : this.MSG_ps.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DATE_length = 0;
    	DATE_color = null;
    	DATE_hilight = null;
    	DATE_outline = null;
    	DATE_transp = null;
    	DATE_validn = null;
    	DATE_sosi = null;
    	DATE_ps = null;
    	TERM_length = 0;
    	TERM_color = null;
    	TERM_hilight = null;
    	TERM_outline = null;
    	TERM_transp = null;
    	TERM_validn = null;
    	TERM_sosi = null;
    	TERM_ps = null;
    	TIME_length = 0;
    	TIME_color = null;
    	TIME_hilight = null;
    	TIME_outline = null;
    	TIME_transp = null;
    	TIME_validn = null;
    	TIME_sosi = null;
    	TIME_ps = null;
    	CODE_length = 0;
    	CODE_color = null;
    	CODE_hilight = null;
    	CODE_outline = null;
    	CODE_transp = null;
    	CODE_validn = null;
    	CODE_sosi = null;
    	CODE_ps = null;
    	IDEN_length = 0;
    	IDEN_color = null;
    	IDEN_hilight = null;
    	IDEN_outline = null;
    	IDEN_transp = null;
    	IDEN_validn = null;
    	IDEN_sosi = null;
    	IDEN_ps = null;
    	MSG_length = 0;
    	MSG_color = null;
    	MSG_hilight = null;
    	MSG_outline = null;
    	MSG_transp = null;
    	MSG_validn = null;
    	MSG_sosi = null;
    	MSG_ps = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

