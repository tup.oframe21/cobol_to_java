package com.tmax.openframe.dto;

import java.io.IOException;
import java.util.List;

import java.util.ArrayList;

import java.util.Map;
import java.util.Collections;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.model.exception.FieldNotFoundException;

import com.tmax.proobject.model.dataobject.DataObject;

	

@javax.annotation.Generated(
	value = "com.tmax.proobject.srcgen.dataobject.DataObjectGenerator",
	comments = "https://www.tmaxsoft.com"
)
@com.tmax.proobject.core.DataObject
public class OIVPINFO_DFHCOMMAREADto extends DataObject
{
    private static final String DTO_LOGICAL_NAME = "OIVPINFO_DFHCOMMAREADto";
    
    public String getDtoLogicalName() {
    	return DTO_LOGICAL_NAME;
    }
    
    private static final long serialVersionUID= 1L;
    
    public OIVPINFO_DFHCOMMAREADto() {
    	super();
    }
    
    private transient boolean isModified = false;
    
    /**
     * LogicalName : DCA_DATE
     * Comments    : 
     */	
    private String DCA_DATE = null;
    
    private transient boolean DCA_DATE_nullable = true;
    
    public boolean isNullableDCA_DATE() {
    	return this.DCA_DATE_nullable;
    }
    
    private transient boolean DCA_DATE_invalidation = false;
    	
    public void setInvalidationDCA_DATE(boolean invalidation) { 
    	this.DCA_DATE_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_DATE() {
    	return this.DCA_DATE_invalidation;
    }
    	
    private transient boolean DCA_DATE_modified = false;
    
    public boolean isModifiedDCA_DATE() {
    	return this.DCA_DATE_modified;
    }
    	
    public void unModifiedDCA_DATE() {
    	this.DCA_DATE_modified = false;
    }
    public FieldProperty getDCA_DATEFieldProperty() {
    	return fieldPropertyMap.get("DCA_DATE");
    }
    
    public String getDCA_DATE() {
    	return DCA_DATE;
    }	
    public String getNvlDCA_DATE() {
    	if(getDCA_DATE() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDCA_DATE();
    	}
    }
    public void setDCA_DATE(String DCA_DATE) {
    	if(DCA_DATE == null) {
    		this.DCA_DATE = null;
    	} else {
    		this.DCA_DATE = DCA_DATE;
    	}
    	this.DCA_DATE_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DCA_TERM
     * Comments    : 
     */	
    private String DCA_TERM = null;
    
    private transient boolean DCA_TERM_nullable = true;
    
    public boolean isNullableDCA_TERM() {
    	return this.DCA_TERM_nullable;
    }
    
    private transient boolean DCA_TERM_invalidation = false;
    	
    public void setInvalidationDCA_TERM(boolean invalidation) { 
    	this.DCA_TERM_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_TERM() {
    	return this.DCA_TERM_invalidation;
    }
    	
    private transient boolean DCA_TERM_modified = false;
    
    public boolean isModifiedDCA_TERM() {
    	return this.DCA_TERM_modified;
    }
    	
    public void unModifiedDCA_TERM() {
    	this.DCA_TERM_modified = false;
    }
    public FieldProperty getDCA_TERMFieldProperty() {
    	return fieldPropertyMap.get("DCA_TERM");
    }
    
    public String getDCA_TERM() {
    	return DCA_TERM;
    }	
    public String getNvlDCA_TERM() {
    	if(getDCA_TERM() == null) {
    		return EMPTY_STRING;
    	} else {
    		return getDCA_TERM();
    	}
    }
    public void setDCA_TERM(String DCA_TERM) {
    	if(DCA_TERM == null) {
    		this.DCA_TERM = null;
    	} else {
    		this.DCA_TERM = DCA_TERM;
    	}
    	this.DCA_TERM_modified = true;
    	this.isModified = true;
    }
    /**
     * LogicalName : DCA_TIME
     * Comments    : 
     */	
    private com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto DCA_TIME = null;
    
    private transient boolean DCA_TIME_nullable = true;
    
    public boolean isNullableDCA_TIME() {
    	return this.DCA_TIME_nullable;
    }
    
    private transient boolean DCA_TIME_invalidation = false;
    	
    public void setInvalidationDCA_TIME(boolean invalidation) { 
    	this.DCA_TIME_invalidation = invalidation;
    }
    	
    public boolean isInvalidationDCA_TIME() {
    	return this.DCA_TIME_invalidation;
    }
    	
    private transient boolean DCA_TIME_modified = false;
    
    public boolean isModifiedDCA_TIME() {
    	if(this.DCA_TIME_modified) return true;
    	if(DCA_TIME.isModified()) return true;
    	return false;
    }
    	
    public void unModifiedDCA_TIME() {
    	this.DCA_TIME_modified = false;
    }
    public FieldProperty getDCA_TIMEFieldProperty() {
    	return fieldPropertyMap.get("DCA_TIME");
    }
    
    public com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto getDCA_TIME() {
    	return DCA_TIME;
    }	
     	public void setDCA_TIME(com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto DCA_TIME) {
    	if(DCA_TIME == null) {
    		this.DCA_TIME = null;
    	} else {
    		this.DCA_TIME = DCA_TIME;
    	}
    	this.DCA_TIME_modified = true;
    	this.isModified = true;
    }
    @Override
    public void clearAllIsModified() {
    	this.DCA_DATE_modified = false;
    	this.DCA_TERM_modified = false;
    	this.DCA_TIME_modified = false;
    	if(this.DCA_TIME != null)
    		this.DCA_TIME.clearAllIsModified();
    	this.isModified = false;
    }
    
    @Override
    public List<String> getIsModifiedField() {
    	List<String> modifiedFields = new ArrayList<>();
    	if(this.DCA_DATE_modified == true)
    		modifiedFields.add("DCA_DATE");
    	if(this.DCA_TERM_modified == true)
    		modifiedFields.add("DCA_TERM");
    	if(this.DCA_TIME_modified == true)
    		modifiedFields.add("DCA_TIME");
    	return modifiedFields;
    }
    
    @Override
    public boolean isModified() {
    	return isModified;
    }
    
    
    public Object clone() {
    	OIVPINFO_DFHCOMMAREADto copyObj = new OIVPINFO_DFHCOMMAREADto();	
    	copyObj.clone(this);
    	return copyObj;
    }
    
    public void clone(DataObject _oIVPINFO_DFHCOMMAREADto) {
    	if(this == _oIVPINFO_DFHCOMMAREADto) return;
    	OIVPINFO_DFHCOMMAREADto __oIVPINFO_DFHCOMMAREADto = (OIVPINFO_DFHCOMMAREADto) _oIVPINFO_DFHCOMMAREADto;
    	this.setDCA_DATE(__oIVPINFO_DFHCOMMAREADto.getDCA_DATE());
    	this.setDCA_TERM(__oIVPINFO_DFHCOMMAREADto.getDCA_TERM());
    	com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto _value2 = __oIVPINFO_DFHCOMMAREADto.getDCA_TIME();
    		if(_value2 == null) {
    			this.setDCA_TIME(null);
    		} else {
    			this.setDCA_TIME((com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto)_value2.clone());
    		}
    }
    
    public String toString() {
    	StringBuilder buffer = new StringBuilder();
    	buffer.append("DCA_DATE : ").append(DCA_DATE).append("\n");	
    	buffer.append("DCA_TERM : ").append(DCA_TERM).append("\n");	
    	buffer.append("DCA_TIME : ").append(DCA_TIME).append("\n");	
    	return buffer.toString();
    }
    
    private static final Map<String,FieldProperty> fieldPropertyMap;
    
    static {
    	fieldPropertyMap = new java.util.LinkedHashMap<String,FieldProperty>(3);
    	fieldPropertyMap.put("DCA_DATE", FieldProperty.builder()
    	              .setPhysicalName("DCA_DATE")
    	              .setLogicalName("DCA_DATE")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("DCA_TERM", FieldProperty.builder()
    	              .setPhysicalName("DCA_TERM")
    	              .setLogicalName("DCA_TERM")
    	              .setType(FieldProperty.TYPE_OBJECT_STRING)
    	              .setLength(4)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .build());
    	fieldPropertyMap.put("DCA_TIME", FieldProperty.builder()
    	              .setPhysicalName("DCA_TIME")
    	              .setLogicalName("DCA_TIME")
    	              .setType(FieldProperty.TYPE_ABSTRACT_INCLUDE)
    	              .setLength(8)
    	              .setDecimal(-1)
    	              .setArray("0")
    	              .setReference("com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto")
    	              .build());
    }
    
    public Map<String,FieldProperty> getFieldPropertyMap() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    public static Map<String,FieldProperty> getFieldPropertyMapByStatic() {
    	return Collections.unmodifiableMap(fieldPropertyMap);
    }
    
    @SuppressWarnings("unchecked")
    public Object get(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DCA_DATE" : return getDCA_DATE();
    		case "DCA_TERM" : return getDCA_TERM();
    		case "DCA_TIME" : return getDCA_TIME();
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }	
    
    
    @Override
    public long getLobLength(String fieldName) throws FieldNotFoundException {
    	switch(fieldName) {
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    public void set(String fieldName, Object arg) throws FieldNotFoundException {
    	switch(fieldName) {
    		case "DCA_DATE" : setDCA_DATE((String)arg); break;
    		case "DCA_TERM" : setDCA_TERM((String)arg); break;
    		case "DCA_TIME" : setDCA_TIME((com.tmax.openframe.dto.OIVPINFO_DCA_TIMEDto)arg); break;
    		default : throw new FieldNotFoundException(fieldName);
    	}
    }
    
    @Override
    public boolean equals(Object obj) {
    	if (this == obj) return true;
    	if (obj == null) return false;
    	if (getClass() != obj.getClass()) return false;
    	OIVPINFO_DFHCOMMAREADto _OIVPINFO_DFHCOMMAREADto = (OIVPINFO_DFHCOMMAREADto) obj;
    	if(this.DCA_DATE == null) {
    	if(_OIVPINFO_DFHCOMMAREADto.getDCA_DATE() != null)
    		return false;
    	} else if(!this.DCA_DATE.equals(_OIVPINFO_DFHCOMMAREADto.getDCA_DATE()))
    		return false;
    	if(this.DCA_TERM == null) {
    	if(_OIVPINFO_DFHCOMMAREADto.getDCA_TERM() != null)
    		return false;
    	} else if(!this.DCA_TERM.equals(_OIVPINFO_DFHCOMMAREADto.getDCA_TERM()))
    		return false;
    	if(this.DCA_TIME == null) {
    	if(_OIVPINFO_DFHCOMMAREADto.getDCA_TIME() != null)
    		return false;
    	} else if(!this.DCA_TIME.equals(_OIVPINFO_DFHCOMMAREADto.getDCA_TIME()))
    		return false;
    	return true;
    }
    
    @Override
    public int hashCode() {
    	int prime  = 31;
    	int result = 1;
    	result = prime * result + ((this.DCA_DATE == null) ? 0 : this.DCA_DATE.hashCode());
    	result = prime * result + ((this.DCA_TERM == null) ? 0 : this.DCA_TERM.hashCode());
    	result = prime * result + ((this.DCA_TIME == null) ? 0 : this.DCA_TIME.hashCode());
    	return result;
    }
    
    @Override
    public void clear() {
    	DCA_DATE = null;
    	DCA_TERM = null;
    	DCA_TIME = null;
    	clearAllIsModified();
    }
    
    private void writeObject(java.io.ObjectOutputStream stream)throws IOException {	
    	stream.defaultWriteObject();
    }
}

